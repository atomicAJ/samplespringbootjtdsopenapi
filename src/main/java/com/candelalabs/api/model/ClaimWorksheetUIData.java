package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsWSResponse;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheet;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIData   {
  @JsonProperty("WSResponse")
  private ClaimDataUIClaimDetailsWSResponse wsResponse = null;

  @JsonProperty("claimWorksheet")
  private ClaimWorksheetUIDataClaimWorksheet claimWorksheet = null;

  public ClaimWorksheetUIData wsResponse(ClaimDataUIClaimDetailsWSResponse wsResponse) {
    this.wsResponse = wsResponse;
    return this;
  }

  /**
   * Get wsResponse
   * @return wsResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ClaimDataUIClaimDetailsWSResponse getWsResponse() {
    return wsResponse;
  }

  public void setWsResponse(ClaimDataUIClaimDetailsWSResponse wsResponse) {
    this.wsResponse = wsResponse;
  }

  public ClaimWorksheetUIData claimWorksheet(ClaimWorksheetUIDataClaimWorksheet claimWorksheet) {
    this.claimWorksheet = claimWorksheet;
    return this;
  }

  /**
   * Get claimWorksheet
   * @return claimWorksheet
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ClaimWorksheetUIDataClaimWorksheet getClaimWorksheet() {
    return claimWorksheet;
  }

  public void setClaimWorksheet(ClaimWorksheetUIDataClaimWorksheet claimWorksheet) {
    this.claimWorksheet = claimWorksheet;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIData claimWorksheetUIData = (ClaimWorksheetUIData) o;
    return Objects.equals(this.wsResponse, claimWorksheetUIData.wsResponse) &&
        Objects.equals(this.claimWorksheet, claimWorksheetUIData.claimWorksheet);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsResponse, claimWorksheet);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIData {\n");
    
    sb.append("    wsResponse: ").append(toIndentedString(wsResponse)).append("\n");
    sb.append("    claimWorksheet: ").append(toIndentedString(claimWorksheet)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

