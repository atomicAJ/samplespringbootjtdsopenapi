package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSDNULAdjClaimAmtUserDecisionDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSDNULAdjClaimAmtUserDecisionDataIn   {
  @JsonProperty("BOIDEN")
  private String BOIDEN = null;

  @JsonProperty("CLAIMNO")
  private String CLAIMNO = null;

  @JsonProperty("CHDRSEL")
  private String CHDRSEL = null;

  @JsonProperty("ACTIVITYID")
  private String ACTIVITYID = null;

  public WSDNULAdjClaimAmtUserDecisionDataIn BOIDEN(String BOIDEN) {
    this.BOIDEN = BOIDEN;
    return this;
  }

  /**
   * BO Identifier
   * @return BOIDEN
  **/
  @ApiModelProperty(example = "CHNSJ", required = true, value = "BO Identifier")
  @NotNull


  public String getBOIDEN() {
    return BOIDEN;
  }

  public void setBOIDEN(String BOIDEN) {
    this.BOIDEN = BOIDEN;
  }

  public WSDNULAdjClaimAmtUserDecisionDataIn CLAIMNO(String CLAIMNO) {
    this.CLAIMNO = CLAIMNO;
    return this;
  }

  /**
   * Get CLAIMNO
   * @return CLAIMNO
  **/
  @ApiModelProperty(example = "[claim Number]", required = true, value = "")
  @NotNull


  public String getCLAIMNO() {
    return CLAIMNO;
  }

  public void setCLAIMNO(String CLAIMNO) {
    this.CLAIMNO = CLAIMNO;
  }

  public WSDNULAdjClaimAmtUserDecisionDataIn CHDRSEL(String CHDRSEL) {
    this.CHDRSEL = CHDRSEL;
    return this;
  }

  /**
   * Get CHDRSEL
   * @return CHDRSEL
  **/
  @ApiModelProperty(example = "[chdrsel]", required = true, value = "")
  @NotNull


  public String getCHDRSEL() {
    return CHDRSEL;
  }

  public void setCHDRSEL(String CHDRSEL) {
    this.CHDRSEL = CHDRSEL;
  }

  public WSDNULAdjClaimAmtUserDecisionDataIn ACTIVITYID(String ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
    return this;
  }

  /**
   * Get ACTIVITYID
   * @return ACTIVITYID
  **/
  @ApiModelProperty(example = "[activityId]", required = true, value = "")
  @NotNull


  public String getACTIVITYID() {
    return ACTIVITYID;
  }

  public void setACTIVITYID(String ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSDNULAdjClaimAmtUserDecisionDataIn wsDNULAdjClaimAmtUserDecisionDataIn = (WSDNULAdjClaimAmtUserDecisionDataIn) o;
    return Objects.equals(this.BOIDEN, wsDNULAdjClaimAmtUserDecisionDataIn.BOIDEN) &&
        Objects.equals(this.CLAIMNO, wsDNULAdjClaimAmtUserDecisionDataIn.CLAIMNO) &&
        Objects.equals(this.CHDRSEL, wsDNULAdjClaimAmtUserDecisionDataIn.CHDRSEL) &&
        Objects.equals(this.ACTIVITYID, wsDNULAdjClaimAmtUserDecisionDataIn.ACTIVITYID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(BOIDEN, CLAIMNO, CHDRSEL, ACTIVITYID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSDNULAdjClaimAmtUserDecisionDataIn {\n");
    
    sb.append("    BOIDEN: ").append(toIndentedString(BOIDEN)).append("\n");
    sb.append("    CLAIMNO: ").append(toIndentedString(CLAIMNO)).append("\n");
    sb.append("    CHDRSEL: ").append(toIndentedString(CHDRSEL)).append("\n");
    sb.append("    ACTIVITYID: ").append(toIndentedString(ACTIVITYID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

