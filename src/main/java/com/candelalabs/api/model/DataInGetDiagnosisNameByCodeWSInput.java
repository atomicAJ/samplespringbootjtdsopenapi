package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetDiagnosisNameByCodeWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetDiagnosisNameByCodeWSInput   {
  @JsonProperty("strClaimTypeUI")
  private String strClaimTypeUI = null;

  @JsonProperty("strDiagnosisCode")
  private String strDiagnosisCode = null;

  @JsonProperty("iClaimSumAssuredPercent")
  private BigDecimal iClaimSumAssuredPercent = null;

  public DataInGetDiagnosisNameByCodeWSInput strClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
    return this;
  }

  /**
   * Get strClaimTypeUI
   * @return strClaimTypeUI
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrClaimTypeUI() {
    return strClaimTypeUI;
  }

  public void setStrClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
  }

  public DataInGetDiagnosisNameByCodeWSInput strDiagnosisCode(String strDiagnosisCode) {
    this.strDiagnosisCode = strDiagnosisCode;
    return this;
  }

  /**
   * Get strDiagnosisCode
   * @return strDiagnosisCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrDiagnosisCode() {
    return strDiagnosisCode;
  }

  public void setStrDiagnosisCode(String strDiagnosisCode) {
    this.strDiagnosisCode = strDiagnosisCode;
  }

  public DataInGetDiagnosisNameByCodeWSInput iClaimSumAssuredPercent(BigDecimal iClaimSumAssuredPercent) {
    this.iClaimSumAssuredPercent = iClaimSumAssuredPercent;
    return this;
  }

  /**
   * Get iClaimSumAssuredPercent
   * @return iClaimSumAssuredPercent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIClaimSumAssuredPercent() {
    return iClaimSumAssuredPercent;
  }

  public void setIClaimSumAssuredPercent(BigDecimal iClaimSumAssuredPercent) {
    this.iClaimSumAssuredPercent = iClaimSumAssuredPercent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetDiagnosisNameByCodeWSInput dataInGetDiagnosisNameByCodeWSInput = (DataInGetDiagnosisNameByCodeWSInput) o;
    return Objects.equals(this.strClaimTypeUI, dataInGetDiagnosisNameByCodeWSInput.strClaimTypeUI) &&
        Objects.equals(this.strDiagnosisCode, dataInGetDiagnosisNameByCodeWSInput.strDiagnosisCode) &&
        Objects.equals(this.iClaimSumAssuredPercent, dataInGetDiagnosisNameByCodeWSInput.iClaimSumAssuredPercent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimTypeUI, strDiagnosisCode, iClaimSumAssuredPercent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetDiagnosisNameByCodeWSInput {\n");
    
    sb.append("    strClaimTypeUI: ").append(toIndentedString(strClaimTypeUI)).append("\n");
    sb.append("    strDiagnosisCode: ").append(toIndentedString(strDiagnosisCode)).append("\n");
    sb.append("    iClaimSumAssuredPercent: ").append(toIndentedString(iClaimSumAssuredPercent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

