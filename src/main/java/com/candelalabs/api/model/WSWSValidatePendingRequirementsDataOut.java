package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSWSValidatePendingRequirementsDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSWSValidatePendingRequirementsDataOut   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strPendingRequirementStatus")
  private Boolean strPendingRequirementStatus = null;

  public WSWSValidatePendingRequirementsDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSWSValidatePendingRequirementsDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSWSValidatePendingRequirementsDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public WSWSValidatePendingRequirementsDataOut strPendingRequirementStatus(Boolean strPendingRequirementStatus) {
    this.strPendingRequirementStatus = strPendingRequirementStatus;
    return this;
  }

  /**
   * Get strPendingRequirementStatus
   * @return strPendingRequirementStatus
  **/
  @ApiModelProperty(example = "true", value = "")


  public Boolean isStrPendingRequirementStatus() {
    return strPendingRequirementStatus;
  }

  public void setStrPendingRequirementStatus(Boolean strPendingRequirementStatus) {
    this.strPendingRequirementStatus = strPendingRequirementStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSWSValidatePendingRequirementsDataOut wsWSValidatePendingRequirementsDataOut = (WSWSValidatePendingRequirementsDataOut) o;
    return Objects.equals(this.wsProcessingStatus, wsWSValidatePendingRequirementsDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsWSValidatePendingRequirementsDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsWSValidatePendingRequirementsDataOut.wsSuccessMessage) &&
        Objects.equals(this.strPendingRequirementStatus, wsWSValidatePendingRequirementsDataOut.strPendingRequirementStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strPendingRequirementStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSWSValidatePendingRequirementsDataOut {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strPendingRequirementStatus: ").append(toIndentedString(strPendingRequirementStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

