package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSGenerateFilesDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSGenerateFilesDataIn   {
  @JsonProperty("claimNo")
  private String claimNo = null;

  @JsonProperty("generateFile")
  private String generateFile = null;

  public JsonWSGenerateFilesDataIn claimNo(String claimNo) {
    this.claimNo = claimNo;
    return this;
  }

  /**
   * claimNo
   * @return claimNo
  **/
  @ApiModelProperty(example = "7", required = true, value = "claimNo")
  @NotNull


  public String getClaimNo() {
    return claimNo;
  }

  public void setClaimNo(String claimNo) {
    this.claimNo = claimNo;
  }

  public JsonWSGenerateFilesDataIn generateFile(String generateFile) {
    this.generateFile = generateFile;
    return this;
  }

  /**
   * Get generateFile
   * @return generateFile
  **/
  @ApiModelProperty(example = "TPADUPCHK", required = true, value = "")
  @NotNull


  public String getGenerateFile() {
    return generateFile;
  }

  public void setGenerateFile(String generateFile) {
    this.generateFile = generateFile;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSGenerateFilesDataIn jsonWSGenerateFilesDataIn = (JsonWSGenerateFilesDataIn) o;
    return Objects.equals(this.claimNo, jsonWSGenerateFilesDataIn.claimNo) &&
        Objects.equals(this.generateFile, jsonWSGenerateFilesDataIn.generateFile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNo, generateFile);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSGenerateFilesDataIn {\n");
    
    sb.append("    claimNo: ").append(toIndentedString(claimNo)).append("\n");
    sb.append("    generateFile: ").append(toIndentedString(generateFile)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

