package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.WSReportRequestDataInWSInputReportInputParameters;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSReportRequestDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSReportRequestDataInWSInput   {
  @JsonProperty("strReportName")
  private String strReportName = null;

  @JsonProperty("strReportType")
  private String strReportType = null;

  @JsonProperty("strReportOutuputFormat")
  private String strReportOutuputFormat = null;

  @JsonProperty("strReportRequestUser")
  private String strReportRequestUser = null;

  @JsonProperty("reportInputParameters")
  private WSReportRequestDataInWSInputReportInputParameters reportInputParameters = null;

  public WSReportRequestDataInWSInput strReportName(String strReportName) {
    this.strReportName = strReportName;
    return this;
  }

  /**
   * Get strReportName
   * @return strReportName
  **/
  @ApiModelProperty(example = "STP", value = "")


  public String getStrReportName() {
    return strReportName;
  }

  public void setStrReportName(String strReportName) {
    this.strReportName = strReportName;
  }

  public WSReportRequestDataInWSInput strReportType(String strReportType) {
    this.strReportType = strReportType;
    return this;
  }

  /**
   * Get strReportType
   * @return strReportType
  **/
  @ApiModelProperty(example = "Claims", value = "")


  public String getStrReportType() {
    return strReportType;
  }

  public void setStrReportType(String strReportType) {
    this.strReportType = strReportType;
  }

  public WSReportRequestDataInWSInput strReportOutuputFormat(String strReportOutuputFormat) {
    this.strReportOutuputFormat = strReportOutuputFormat;
    return this;
  }

  /**
   * Get strReportOutuputFormat
   * @return strReportOutuputFormat
  **/
  @ApiModelProperty(example = "xls", value = "")


  public String getStrReportOutuputFormat() {
    return strReportOutuputFormat;
  }

  public void setStrReportOutuputFormat(String strReportOutuputFormat) {
    this.strReportOutuputFormat = strReportOutuputFormat;
  }

  public WSReportRequestDataInWSInput strReportRequestUser(String strReportRequestUser) {
    this.strReportRequestUser = strReportRequestUser;
    return this;
  }

  /**
   * Get strReportRequestUser
   * @return strReportRequestUser
  **/
  @ApiModelProperty(example = "idntest2", value = "")


  public String getStrReportRequestUser() {
    return strReportRequestUser;
  }

  public void setStrReportRequestUser(String strReportRequestUser) {
    this.strReportRequestUser = strReportRequestUser;
  }

  public WSReportRequestDataInWSInput reportInputParameters(WSReportRequestDataInWSInputReportInputParameters reportInputParameters) {
    this.reportInputParameters = reportInputParameters;
    return this;
  }

  /**
   * Get reportInputParameters
   * @return reportInputParameters
  **/
  @ApiModelProperty(value = "")

  @Valid

  public WSReportRequestDataInWSInputReportInputParameters getReportInputParameters() {
    return reportInputParameters;
  }

  public void setReportInputParameters(WSReportRequestDataInWSInputReportInputParameters reportInputParameters) {
    this.reportInputParameters = reportInputParameters;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSReportRequestDataInWSInput wsReportRequestDataInWSInput = (WSReportRequestDataInWSInput) o;
    return Objects.equals(this.strReportName, wsReportRequestDataInWSInput.strReportName) &&
        Objects.equals(this.strReportType, wsReportRequestDataInWSInput.strReportType) &&
        Objects.equals(this.strReportOutuputFormat, wsReportRequestDataInWSInput.strReportOutuputFormat) &&
        Objects.equals(this.strReportRequestUser, wsReportRequestDataInWSInput.strReportRequestUser) &&
        Objects.equals(this.reportInputParameters, wsReportRequestDataInWSInput.reportInputParameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strReportName, strReportType, strReportOutuputFormat, strReportRequestUser, reportInputParameters);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSReportRequestDataInWSInput {\n");
    
    sb.append("    strReportName: ").append(toIndentedString(strReportName)).append("\n");
    sb.append("    strReportType: ").append(toIndentedString(strReportType)).append("\n");
    sb.append("    strReportOutuputFormat: ").append(toIndentedString(strReportOutuputFormat)).append("\n");
    sb.append("    strReportRequestUser: ").append(toIndentedString(strReportRequestUser)).append("\n");
    sb.append("    reportInputParameters: ").append(toIndentedString(reportInputParameters)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

