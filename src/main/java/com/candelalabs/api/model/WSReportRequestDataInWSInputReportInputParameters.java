package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSReportRequestDataInWSInputReportInputParameters
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSReportRequestDataInWSInputReportInputParameters   {
  @JsonProperty("dtDate")
  private String dtDate = null;

  @JsonProperty("dtFromDate")
  private String dtFromDate = null;

  @JsonProperty("dtToDate")
  private String dtToDate = null;

  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPOSRequestNumber")
  private String strPOSRequestNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strOperator")
  private String strOperator = null;

  @JsonProperty("strProductName")
  private String strProductName = null;

  @JsonProperty("dtReceiveDateFrom")
  private String dtReceiveDateFrom = null;

  @JsonProperty("dtReceiveDateTo")
  private String dtReceiveDateTo = null;

  @JsonProperty("dtRegisterDateFrom")
  private String dtRegisterDateFrom = null;

  @JsonProperty("dtRegisterDateTo")
  private String dtRegisterDateTo = null;

  public WSReportRequestDataInWSInputReportInputParameters dtDate(String dtDate) {
    this.dtDate = dtDate;
    return this;
  }

  /**
   * Get dtDate
   * @return dtDate
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtDate() {
    return dtDate;
  }

  public void setDtDate(String dtDate) {
    this.dtDate = dtDate;
  }

  public WSReportRequestDataInWSInputReportInputParameters dtFromDate(String dtFromDate) {
    this.dtFromDate = dtFromDate;
    return this;
  }

  /**
   * Get dtFromDate
   * @return dtFromDate
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtFromDate() {
    return dtFromDate;
  }

  public void setDtFromDate(String dtFromDate) {
    this.dtFromDate = dtFromDate;
  }

  public WSReportRequestDataInWSInputReportInputParameters dtToDate(String dtToDate) {
    this.dtToDate = dtToDate;
    return this;
  }

  /**
   * Get dtToDate
   * @return dtToDate
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtToDate() {
    return dtToDate;
  }

  public void setDtToDate(String dtToDate) {
    this.dtToDate = dtToDate;
  }

  public WSReportRequestDataInWSInputReportInputParameters strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(example = "1903DH00001", value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public WSReportRequestDataInWSInputReportInputParameters strPOSRequestNumber(String strPOSRequestNumber) {
    this.strPOSRequestNumber = strPOSRequestNumber;
    return this;
  }

  /**
   * Get strPOSRequestNumber
   * @return strPOSRequestNumber
  **/
  @ApiModelProperty(example = "1903DH00001", value = "")


  public String getStrPOSRequestNumber() {
    return strPOSRequestNumber;
  }

  public void setStrPOSRequestNumber(String strPOSRequestNumber) {
    this.strPOSRequestNumber = strPOSRequestNumber;
  }

  public WSReportRequestDataInWSInputReportInputParameters strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(example = "60134000", value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public WSReportRequestDataInWSInputReportInputParameters strOperator(String strOperator) {
    this.strOperator = strOperator;
    return this;
  }

  /**
   * Get strOperator
   * @return strOperator
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrOperator() {
    return strOperator;
  }

  public void setStrOperator(String strOperator) {
    this.strOperator = strOperator;
  }

  public WSReportRequestDataInWSInputReportInputParameters strProductName(String strProductName) {
    this.strProductName = strProductName;
    return this;
  }

  /**
   * Get strProductName
   * @return strProductName
  **/
  @ApiModelProperty(example = "FW4", value = "")


  public String getStrProductName() {
    return strProductName;
  }

  public void setStrProductName(String strProductName) {
    this.strProductName = strProductName;
  }

  public WSReportRequestDataInWSInputReportInputParameters dtReceiveDateFrom(String dtReceiveDateFrom) {
    this.dtReceiveDateFrom = dtReceiveDateFrom;
    return this;
  }

  /**
   * Get dtReceiveDateFrom
   * @return dtReceiveDateFrom
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtReceiveDateFrom() {
    return dtReceiveDateFrom;
  }

  public void setDtReceiveDateFrom(String dtReceiveDateFrom) {
    this.dtReceiveDateFrom = dtReceiveDateFrom;
  }

  public WSReportRequestDataInWSInputReportInputParameters dtReceiveDateTo(String dtReceiveDateTo) {
    this.dtReceiveDateTo = dtReceiveDateTo;
    return this;
  }

  /**
   * Get dtReceiveDateTo
   * @return dtReceiveDateTo
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtReceiveDateTo() {
    return dtReceiveDateTo;
  }

  public void setDtReceiveDateTo(String dtReceiveDateTo) {
    this.dtReceiveDateTo = dtReceiveDateTo;
  }

  public WSReportRequestDataInWSInputReportInputParameters dtRegisterDateFrom(String dtRegisterDateFrom) {
    this.dtRegisterDateFrom = dtRegisterDateFrom;
    return this;
  }

  /**
   * Get dtRegisterDateFrom
   * @return dtRegisterDateFrom
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtRegisterDateFrom() {
    return dtRegisterDateFrom;
  }

  public void setDtRegisterDateFrom(String dtRegisterDateFrom) {
    this.dtRegisterDateFrom = dtRegisterDateFrom;
  }

  public WSReportRequestDataInWSInputReportInputParameters dtRegisterDateTo(String dtRegisterDateTo) {
    this.dtRegisterDateTo = dtRegisterDateTo;
    return this;
  }

  /**
   * Get dtRegisterDateTo
   * @return dtRegisterDateTo
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtRegisterDateTo() {
    return dtRegisterDateTo;
  }

  public void setDtRegisterDateTo(String dtRegisterDateTo) {
    this.dtRegisterDateTo = dtRegisterDateTo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSReportRequestDataInWSInputReportInputParameters wsReportRequestDataInWSInputReportInputParameters = (WSReportRequestDataInWSInputReportInputParameters) o;
    return Objects.equals(this.dtDate, wsReportRequestDataInWSInputReportInputParameters.dtDate) &&
        Objects.equals(this.dtFromDate, wsReportRequestDataInWSInputReportInputParameters.dtFromDate) &&
        Objects.equals(this.dtToDate, wsReportRequestDataInWSInputReportInputParameters.dtToDate) &&
        Objects.equals(this.strClaimNumber, wsReportRequestDataInWSInputReportInputParameters.strClaimNumber) &&
        Objects.equals(this.strPOSRequestNumber, wsReportRequestDataInWSInputReportInputParameters.strPOSRequestNumber) &&
        Objects.equals(this.strPolicyNumber, wsReportRequestDataInWSInputReportInputParameters.strPolicyNumber) &&
        Objects.equals(this.strOperator, wsReportRequestDataInWSInputReportInputParameters.strOperator) &&
        Objects.equals(this.strProductName, wsReportRequestDataInWSInputReportInputParameters.strProductName) &&
        Objects.equals(this.dtReceiveDateFrom, wsReportRequestDataInWSInputReportInputParameters.dtReceiveDateFrom) &&
        Objects.equals(this.dtReceiveDateTo, wsReportRequestDataInWSInputReportInputParameters.dtReceiveDateTo) &&
        Objects.equals(this.dtRegisterDateFrom, wsReportRequestDataInWSInputReportInputParameters.dtRegisterDateFrom) &&
        Objects.equals(this.dtRegisterDateTo, wsReportRequestDataInWSInputReportInputParameters.dtRegisterDateTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dtDate, dtFromDate, dtToDate, strClaimNumber, strPOSRequestNumber, strPolicyNumber, strOperator, strProductName, dtReceiveDateFrom, dtReceiveDateTo, dtRegisterDateFrom, dtRegisterDateTo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSReportRequestDataInWSInputReportInputParameters {\n");
    
    sb.append("    dtDate: ").append(toIndentedString(dtDate)).append("\n");
    sb.append("    dtFromDate: ").append(toIndentedString(dtFromDate)).append("\n");
    sb.append("    dtToDate: ").append(toIndentedString(dtToDate)).append("\n");
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPOSRequestNumber: ").append(toIndentedString(strPOSRequestNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strOperator: ").append(toIndentedString(strOperator)).append("\n");
    sb.append("    strProductName: ").append(toIndentedString(strProductName)).append("\n");
    sb.append("    dtReceiveDateFrom: ").append(toIndentedString(dtReceiveDateFrom)).append("\n");
    sb.append("    dtReceiveDateTo: ").append(toIndentedString(dtReceiveDateTo)).append("\n");
    sb.append("    dtRegisterDateFrom: ").append(toIndentedString(dtRegisterDateFrom)).append("\n");
    sb.append("    dtRegisterDateTo: ").append(toIndentedString(dtRegisterDateTo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

