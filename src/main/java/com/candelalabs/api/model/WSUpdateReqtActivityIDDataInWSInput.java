package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateReqtActivityIDDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateReqtActivityIDDataInWSInput   {
  @JsonProperty("strProcessType")
  private String strProcessType = null;

  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strReqtActivityID")
  private String strReqtActivityID = null;

  public WSUpdateReqtActivityIDDataInWSInput strProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
    return this;
  }

  /**
   * Get strProcessType
   * @return strProcessType
  **/
  @ApiModelProperty(value = "")


  public String getStrProcessType() {
    return strProcessType;
  }

  public void setStrProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
  }

  public WSUpdateReqtActivityIDDataInWSInput strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public WSUpdateReqtActivityIDDataInWSInput strReqtActivityID(String strReqtActivityID) {
    this.strReqtActivityID = strReqtActivityID;
    return this;
  }

  /**
   * Get strReqtActivityID
   * @return strReqtActivityID
  **/
  @ApiModelProperty(value = "")


  public String getStrReqtActivityID() {
    return strReqtActivityID;
  }

  public void setStrReqtActivityID(String strReqtActivityID) {
    this.strReqtActivityID = strReqtActivityID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateReqtActivityIDDataInWSInput wsUpdateReqtActivityIDDataInWSInput = (WSUpdateReqtActivityIDDataInWSInput) o;
    return Objects.equals(this.strProcessType, wsUpdateReqtActivityIDDataInWSInput.strProcessType) &&
        Objects.equals(this.strClaimNumber, wsUpdateReqtActivityIDDataInWSInput.strClaimNumber) &&
        Objects.equals(this.strReqtActivityID, wsUpdateReqtActivityIDDataInWSInput.strReqtActivityID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strProcessType, strClaimNumber, strReqtActivityID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateReqtActivityIDDataInWSInput {\n");
    
    sb.append("    strProcessType: ").append(toIndentedString(strProcessType)).append("\n");
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strReqtActivityID: ").append(toIndentedString(strReqtActivityID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

