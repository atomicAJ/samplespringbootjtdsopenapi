package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.WSFetchRecordEntriesDataInWSInput;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSFetchRecordEntriesDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSFetchRecordEntriesDataIn   {
  @JsonProperty("WSInput")
  private WSFetchRecordEntriesDataInWSInput wsInput = null;

  public WSFetchRecordEntriesDataIn wsInput(WSFetchRecordEntriesDataInWSInput wsInput) {
    this.wsInput = wsInput;
    return this;
  }

  /**
   * Get wsInput
   * @return wsInput
  **/
  @ApiModelProperty(value = "")

  @Valid

  public WSFetchRecordEntriesDataInWSInput getWsInput() {
    return wsInput;
  }

  public void setWsInput(WSFetchRecordEntriesDataInWSInput wsInput) {
    this.wsInput = wsInput;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSFetchRecordEntriesDataIn wsFetchRecordEntriesDataIn = (WSFetchRecordEntriesDataIn) o;
    return Objects.equals(this.wsInput, wsFetchRecordEntriesDataIn.wsInput);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsInput);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSFetchRecordEntriesDataIn {\n");
    
    sb.append("    wsInput: ").append(toIndentedString(wsInput)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

