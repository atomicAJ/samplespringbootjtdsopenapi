package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGetReportNameListDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGetReportNameListDataInWSInput   {
  @JsonProperty("strReportCategoryName")
  private String strReportCategoryName = null;

  public WSGetReportNameListDataInWSInput strReportCategoryName(String strReportCategoryName) {
    this.strReportCategoryName = strReportCategoryName;
    return this;
  }

  /**
   * Get strReportCategoryName
   * @return strReportCategoryName
  **/
  @ApiModelProperty(example = "Claims", value = "")


  public String getStrReportCategoryName() {
    return strReportCategoryName;
  }

  public void setStrReportCategoryName(String strReportCategoryName) {
    this.strReportCategoryName = strReportCategoryName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGetReportNameListDataInWSInput wsGetReportNameListDataInWSInput = (WSGetReportNameListDataInWSInput) o;
    return Objects.equals(this.strReportCategoryName, wsGetReportNameListDataInWSInput.strReportCategoryName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strReportCategoryName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGetReportNameListDataInWSInput {\n");
    
    sb.append("    strReportCategoryName: ").append(toIndentedString(strReportCategoryName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

