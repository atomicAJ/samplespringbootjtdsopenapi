package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateRequirementDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateRequirementDataIn   {
  @JsonProperty("docId")
  private String docId = null;

  @JsonProperty("claimNumber")
  private String claimNumber = null;

  @JsonProperty("policyNumber")
  private String policyNumber = null;

  @JsonProperty("tpaClaimNumber")
  private String tpaClaimNumber = null;

  @JsonProperty("batchNumber")
  private String batchNumber = null;

  @JsonProperty("portalRequestNumber")
  private String portalRequestNumber = null;

  public WSUpdateRequirementDataIn docId(String docId) {
    this.docId = docId;
    return this;
  }

  /**
   * Get docId
   * @return docId
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getDocId() {
    return docId;
  }

  public void setDocId(String docId) {
    this.docId = docId;
  }

  public WSUpdateRequirementDataIn claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public WSUpdateRequirementDataIn policyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
    return this;
  }

  /**
   * Get policyNumber
   * @return policyNumber
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public WSUpdateRequirementDataIn tpaClaimNumber(String tpaClaimNumber) {
    this.tpaClaimNumber = tpaClaimNumber;
    return this;
  }

  /**
   * Get tpaClaimNumber
   * @return tpaClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getTpaClaimNumber() {
    return tpaClaimNumber;
  }

  public void setTpaClaimNumber(String tpaClaimNumber) {
    this.tpaClaimNumber = tpaClaimNumber;
  }

  public WSUpdateRequirementDataIn batchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
    return this;
  }

  /**
   * Get batchNumber
   * @return batchNumber
  **/
  @ApiModelProperty(value = "")


  public String getBatchNumber() {
    return batchNumber;
  }

  public void setBatchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
  }

  public WSUpdateRequirementDataIn portalRequestNumber(String portalRequestNumber) {
    this.portalRequestNumber = portalRequestNumber;
    return this;
  }

  /**
   * Get portalRequestNumber
   * @return portalRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getPortalRequestNumber() {
    return portalRequestNumber;
  }

  public void setPortalRequestNumber(String portalRequestNumber) {
    this.portalRequestNumber = portalRequestNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateRequirementDataIn wsUpdateRequirementDataIn = (WSUpdateRequirementDataIn) o;
    return Objects.equals(this.docId, wsUpdateRequirementDataIn.docId) &&
        Objects.equals(this.claimNumber, wsUpdateRequirementDataIn.claimNumber) &&
        Objects.equals(this.policyNumber, wsUpdateRequirementDataIn.policyNumber) &&
        Objects.equals(this.tpaClaimNumber, wsUpdateRequirementDataIn.tpaClaimNumber) &&
        Objects.equals(this.batchNumber, wsUpdateRequirementDataIn.batchNumber) &&
        Objects.equals(this.portalRequestNumber, wsUpdateRequirementDataIn.portalRequestNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docId, claimNumber, policyNumber, tpaClaimNumber, batchNumber, portalRequestNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateRequirementDataIn {\n");
    
    sb.append("    docId: ").append(toIndentedString(docId)).append("\n");
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    policyNumber: ").append(toIndentedString(policyNumber)).append("\n");
    sb.append("    tpaClaimNumber: ").append(toIndentedString(tpaClaimNumber)).append("\n");
    sb.append("    batchNumber: ").append(toIndentedString(batchNumber)).append("\n");
    sb.append("    portalRequestNumber: ").append(toIndentedString(portalRequestNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

