package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ReportNameListData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ReportNameListData   {
  @JsonProperty("strReportName")
  private String strReportName = null;

  @JsonProperty("strReportOutputFormat")
  private String strReportOutputFormat = null;

  public ReportNameListData strReportName(String strReportName) {
    this.strReportName = strReportName;
    return this;
  }

  /**
   * Get strReportName
   * @return strReportName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrReportName() {
    return strReportName;
  }

  public void setStrReportName(String strReportName) {
    this.strReportName = strReportName;
  }

  public ReportNameListData strReportOutputFormat(String strReportOutputFormat) {
    this.strReportOutputFormat = strReportOutputFormat;
    return this;
  }

  /**
   * Get strReportOutputFormat
   * @return strReportOutputFormat
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrReportOutputFormat() {
    return strReportOutputFormat;
  }

  public void setStrReportOutputFormat(String strReportOutputFormat) {
    this.strReportOutputFormat = strReportOutputFormat;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReportNameListData reportNameListData = (ReportNameListData) o;
    return Objects.equals(this.strReportName, reportNameListData.strReportName) &&
        Objects.equals(this.strReportOutputFormat, reportNameListData.strReportOutputFormat);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strReportName, strReportOutputFormat);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReportNameListData {\n");
    
    sb.append("    strReportName: ").append(toIndentedString(strReportName)).append("\n");
    sb.append("    strReportOutputFormat: ").append(toIndentedString(strReportOutputFormat)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

