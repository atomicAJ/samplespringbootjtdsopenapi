package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetBenefitCodeNameListWSInput1
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetBenefitCodeNameListWSInput1   {
  @JsonProperty("strComponentCode1")
  private String strComponentCode1 = null;

  @JsonProperty("strClaimTypeUI1")
  private String strClaimTypeUI1 = null;

  public DataInGetBenefitCodeNameListWSInput1 strComponentCode1(String strComponentCode1) {
    this.strComponentCode1 = strComponentCode1;
    return this;
  }

  /**
   * Get strComponentCode1
   * @return strComponentCode1
  **/
  @ApiModelProperty(value = "")


  public String getStrComponentCode1() {
    return strComponentCode1;
  }

  public void setStrComponentCode1(String strComponentCode1) {
    this.strComponentCode1 = strComponentCode1;
  }

  public DataInGetBenefitCodeNameListWSInput1 strClaimTypeUI1(String strClaimTypeUI1) {
    this.strClaimTypeUI1 = strClaimTypeUI1;
    return this;
  }

  /**
   * Get strClaimTypeUI1
   * @return strClaimTypeUI1
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeUI1() {
    return strClaimTypeUI1;
  }

  public void setStrClaimTypeUI1(String strClaimTypeUI1) {
    this.strClaimTypeUI1 = strClaimTypeUI1;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetBenefitCodeNameListWSInput1 dataInGetBenefitCodeNameListWSInput1 = (DataInGetBenefitCodeNameListWSInput1) o;
    return Objects.equals(this.strComponentCode1, dataInGetBenefitCodeNameListWSInput1.strComponentCode1) &&
        Objects.equals(this.strClaimTypeUI1, dataInGetBenefitCodeNameListWSInput1.strClaimTypeUI1);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strComponentCode1, strClaimTypeUI1);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetBenefitCodeNameListWSInput1 {\n");
    
    sb.append("    strComponentCode1: ").append(toIndentedString(strComponentCode1)).append("\n");
    sb.append("    strClaimTypeUI1: ").append(toIndentedString(strClaimTypeUI1)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

