package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSProcessRuleRecordDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSProcessRuleRecordDataInWSInput   {
  @JsonProperty("strProcessType")
  private String strProcessType = null;

  @JsonProperty("strProcessKeyReferenceNumber")
  private String strProcessKeyReferenceNumber = null;

  @JsonProperty("strRuleType")
  private String strRuleType = null;

  @JsonProperty("strRuleDescription")
  private String strRuleDescription = null;

  @JsonProperty("strRuleStatus")
  private String strRuleStatus = null;

  @JsonProperty("strRuleReason")
  private String strRuleReason = null;

  @JsonProperty("strRuleProcessingStartTimeStamp")
  private String strRuleProcessingStartTimeStamp = null;

  @JsonProperty("strRuleProcessingEndTimeStamp")
  private String strRuleProcessingEndTimeStamp = null;

  public WSProcessRuleRecordDataInWSInput strProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
    return this;
  }

  /**
   * Get strProcessType
   * @return strProcessType
  **/
  @ApiModelProperty(value = "")


  public String getStrProcessType() {
    return strProcessType;
  }

  public void setStrProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
  }

  public WSProcessRuleRecordDataInWSInput strProcessKeyReferenceNumber(String strProcessKeyReferenceNumber) {
    this.strProcessKeyReferenceNumber = strProcessKeyReferenceNumber;
    return this;
  }

  /**
   * Get strProcessKeyReferenceNumber
   * @return strProcessKeyReferenceNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrProcessKeyReferenceNumber() {
    return strProcessKeyReferenceNumber;
  }

  public void setStrProcessKeyReferenceNumber(String strProcessKeyReferenceNumber) {
    this.strProcessKeyReferenceNumber = strProcessKeyReferenceNumber;
  }

  public WSProcessRuleRecordDataInWSInput strRuleType(String strRuleType) {
    this.strRuleType = strRuleType;
    return this;
  }

  /**
   * Get strRuleType
   * @return strRuleType
  **/
  @ApiModelProperty(value = "")


  public String getStrRuleType() {
    return strRuleType;
  }

  public void setStrRuleType(String strRuleType) {
    this.strRuleType = strRuleType;
  }

  public WSProcessRuleRecordDataInWSInput strRuleDescription(String strRuleDescription) {
    this.strRuleDescription = strRuleDescription;
    return this;
  }

  /**
   * Get strRuleDescription
   * @return strRuleDescription
  **/
  @ApiModelProperty(value = "")


  public String getStrRuleDescription() {
    return strRuleDescription;
  }

  public void setStrRuleDescription(String strRuleDescription) {
    this.strRuleDescription = strRuleDescription;
  }

  public WSProcessRuleRecordDataInWSInput strRuleStatus(String strRuleStatus) {
    this.strRuleStatus = strRuleStatus;
    return this;
  }

  /**
   * Get strRuleStatus
   * @return strRuleStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrRuleStatus() {
    return strRuleStatus;
  }

  public void setStrRuleStatus(String strRuleStatus) {
    this.strRuleStatus = strRuleStatus;
  }

  public WSProcessRuleRecordDataInWSInput strRuleReason(String strRuleReason) {
    this.strRuleReason = strRuleReason;
    return this;
  }

  /**
   * Get strRuleReason
   * @return strRuleReason
  **/
  @ApiModelProperty(value = "")


  public String getStrRuleReason() {
    return strRuleReason;
  }

  public void setStrRuleReason(String strRuleReason) {
    this.strRuleReason = strRuleReason;
  }

  public WSProcessRuleRecordDataInWSInput strRuleProcessingStartTimeStamp(String strRuleProcessingStartTimeStamp) {
    this.strRuleProcessingStartTimeStamp = strRuleProcessingStartTimeStamp;
    return this;
  }

  /**
   * Get strRuleProcessingStartTimeStamp
   * @return strRuleProcessingStartTimeStamp
  **/
  @ApiModelProperty(value = "")


  public String getStrRuleProcessingStartTimeStamp() {
    return strRuleProcessingStartTimeStamp;
  }

  public void setStrRuleProcessingStartTimeStamp(String strRuleProcessingStartTimeStamp) {
    this.strRuleProcessingStartTimeStamp = strRuleProcessingStartTimeStamp;
  }

  public WSProcessRuleRecordDataInWSInput strRuleProcessingEndTimeStamp(String strRuleProcessingEndTimeStamp) {
    this.strRuleProcessingEndTimeStamp = strRuleProcessingEndTimeStamp;
    return this;
  }

  /**
   * Get strRuleProcessingEndTimeStamp
   * @return strRuleProcessingEndTimeStamp
  **/
  @ApiModelProperty(value = "")


  public String getStrRuleProcessingEndTimeStamp() {
    return strRuleProcessingEndTimeStamp;
  }

  public void setStrRuleProcessingEndTimeStamp(String strRuleProcessingEndTimeStamp) {
    this.strRuleProcessingEndTimeStamp = strRuleProcessingEndTimeStamp;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSProcessRuleRecordDataInWSInput wsProcessRuleRecordDataInWSInput = (WSProcessRuleRecordDataInWSInput) o;
    return Objects.equals(this.strProcessType, wsProcessRuleRecordDataInWSInput.strProcessType) &&
        Objects.equals(this.strProcessKeyReferenceNumber, wsProcessRuleRecordDataInWSInput.strProcessKeyReferenceNumber) &&
        Objects.equals(this.strRuleType, wsProcessRuleRecordDataInWSInput.strRuleType) &&
        Objects.equals(this.strRuleDescription, wsProcessRuleRecordDataInWSInput.strRuleDescription) &&
        Objects.equals(this.strRuleStatus, wsProcessRuleRecordDataInWSInput.strRuleStatus) &&
        Objects.equals(this.strRuleReason, wsProcessRuleRecordDataInWSInput.strRuleReason) &&
        Objects.equals(this.strRuleProcessingStartTimeStamp, wsProcessRuleRecordDataInWSInput.strRuleProcessingStartTimeStamp) &&
        Objects.equals(this.strRuleProcessingEndTimeStamp, wsProcessRuleRecordDataInWSInput.strRuleProcessingEndTimeStamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strProcessType, strProcessKeyReferenceNumber, strRuleType, strRuleDescription, strRuleStatus, strRuleReason, strRuleProcessingStartTimeStamp, strRuleProcessingEndTimeStamp);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSProcessRuleRecordDataInWSInput {\n");
    
    sb.append("    strProcessType: ").append(toIndentedString(strProcessType)).append("\n");
    sb.append("    strProcessKeyReferenceNumber: ").append(toIndentedString(strProcessKeyReferenceNumber)).append("\n");
    sb.append("    strRuleType: ").append(toIndentedString(strRuleType)).append("\n");
    sb.append("    strRuleDescription: ").append(toIndentedString(strRuleDescription)).append("\n");
    sb.append("    strRuleStatus: ").append(toIndentedString(strRuleStatus)).append("\n");
    sb.append("    strRuleReason: ").append(toIndentedString(strRuleReason)).append("\n");
    sb.append("    strRuleProcessingStartTimeStamp: ").append(toIndentedString(strRuleProcessingStartTimeStamp)).append("\n");
    sb.append("    strRuleProcessingEndTimeStamp: ").append(toIndentedString(strRuleProcessingEndTimeStamp)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

