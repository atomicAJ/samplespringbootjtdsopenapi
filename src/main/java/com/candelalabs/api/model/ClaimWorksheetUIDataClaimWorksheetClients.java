package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClientsOwnerData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetClients
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetClients   {
  @JsonProperty("OwnerData")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> ownerData = null;

  @JsonProperty("LifeInsuredData")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> lifeInsuredData = null;

  @JsonProperty("BeneficiaryData")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> beneficiaryData = null;

  public ClaimWorksheetUIDataClaimWorksheetClients ownerData(List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> ownerData) {
    this.ownerData = ownerData;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetClients addOwnerDataItem(ClaimWorksheetUIDataClaimWorksheetClientsOwnerData ownerDataItem) {
    if (this.ownerData == null) {
      this.ownerData = new ArrayList<>();
    }
    this.ownerData.add(ownerDataItem);
    return this;
  }

  /**
   * Get ownerData
   * @return ownerData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> getOwnerData() {
    return ownerData;
  }

  public void setOwnerData(List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> ownerData) {
    this.ownerData = ownerData;
  }

  public ClaimWorksheetUIDataClaimWorksheetClients lifeInsuredData(List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> lifeInsuredData) {
    this.lifeInsuredData = lifeInsuredData;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetClients addLifeInsuredDataItem(ClaimWorksheetUIDataClaimWorksheetClientsOwnerData lifeInsuredDataItem) {
    if (this.lifeInsuredData == null) {
      this.lifeInsuredData = new ArrayList<>();
    }
    this.lifeInsuredData.add(lifeInsuredDataItem);
    return this;
  }

  /**
   * Get lifeInsuredData
   * @return lifeInsuredData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> getLifeInsuredData() {
    return lifeInsuredData;
  }

  public void setLifeInsuredData(List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> lifeInsuredData) {
    this.lifeInsuredData = lifeInsuredData;
  }

  public ClaimWorksheetUIDataClaimWorksheetClients beneficiaryData(List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> beneficiaryData) {
    this.beneficiaryData = beneficiaryData;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetClients addBeneficiaryDataItem(ClaimWorksheetUIDataClaimWorksheetClientsOwnerData beneficiaryDataItem) {
    if (this.beneficiaryData == null) {
      this.beneficiaryData = new ArrayList<>();
    }
    this.beneficiaryData.add(beneficiaryDataItem);
    return this;
  }

  /**
   * Get beneficiaryData
   * @return beneficiaryData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> getBeneficiaryData() {
    return beneficiaryData;
  }

  public void setBeneficiaryData(List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> beneficiaryData) {
    this.beneficiaryData = beneficiaryData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetClients claimWorksheetUIDataClaimWorksheetClients = (ClaimWorksheetUIDataClaimWorksheetClients) o;
    return Objects.equals(this.ownerData, claimWorksheetUIDataClaimWorksheetClients.ownerData) &&
        Objects.equals(this.lifeInsuredData, claimWorksheetUIDataClaimWorksheetClients.lifeInsuredData) &&
        Objects.equals(this.beneficiaryData, claimWorksheetUIDataClaimWorksheetClients.beneficiaryData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ownerData, lifeInsuredData, beneficiaryData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetClients {\n");
    
    sb.append("    ownerData: ").append(toIndentedString(ownerData)).append("\n");
    sb.append("    lifeInsuredData: ").append(toIndentedString(lifeInsuredData)).append("\n");
    sb.append("    beneficiaryData: ").append(toIndentedString(beneficiaryData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

