package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CaseDecisionTrackerModel
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class CaseDecisionTrackerModel   {
  @JsonProperty("ProcessType")
  private String processType = null;

  @JsonProperty("ProcessTransactionType")
  private String processTransactionType = null;

  @JsonProperty("UserID")
  private String userID = null;

  @JsonProperty("CaseReferenceNumber")
  private String caseReferenceNumber = null;

  @JsonProperty("CaseUserDecision")
  private String caseUserDecision = null;

  @JsonProperty("CaseUserActivityName")
  private String caseUserActivityName = null;

  @JsonProperty("operation")
  private String operation = null;

  @JsonProperty("caseOperationTs")
  private String caseOperationTs = null;

  public CaseDecisionTrackerModel processType(String processType) {
    this.processType = processType;
    return this;
  }

  /**
   * Get processType
   * @return processType
  **/
  @ApiModelProperty(value = "")


  public String getProcessType() {
    return processType;
  }

  public void setProcessType(String processType) {
    this.processType = processType;
  }

  public CaseDecisionTrackerModel processTransactionType(String processTransactionType) {
    this.processTransactionType = processTransactionType;
    return this;
  }

  /**
   * Get processTransactionType
   * @return processTransactionType
  **/
  @ApiModelProperty(value = "")


  public String getProcessTransactionType() {
    return processTransactionType;
  }

  public void setProcessTransactionType(String processTransactionType) {
    this.processTransactionType = processTransactionType;
  }

  public CaseDecisionTrackerModel userID(String userID) {
    this.userID = userID;
    return this;
  }

  /**
   * Get userID
   * @return userID
  **/
  @ApiModelProperty(value = "")


  public String getUserID() {
    return userID;
  }

  public void setUserID(String userID) {
    this.userID = userID;
  }

  public CaseDecisionTrackerModel caseReferenceNumber(String caseReferenceNumber) {
    this.caseReferenceNumber = caseReferenceNumber;
    return this;
  }

  /**
   * Get caseReferenceNumber
   * @return caseReferenceNumber
  **/
  @ApiModelProperty(value = "")


  public String getCaseReferenceNumber() {
    return caseReferenceNumber;
  }

  public void setCaseReferenceNumber(String caseReferenceNumber) {
    this.caseReferenceNumber = caseReferenceNumber;
  }

  public CaseDecisionTrackerModel caseUserDecision(String caseUserDecision) {
    this.caseUserDecision = caseUserDecision;
    return this;
  }

  /**
   * Get caseUserDecision
   * @return caseUserDecision
  **/
  @ApiModelProperty(value = "")


  public String getCaseUserDecision() {
    return caseUserDecision;
  }

  public void setCaseUserDecision(String caseUserDecision) {
    this.caseUserDecision = caseUserDecision;
  }

  public CaseDecisionTrackerModel caseUserActivityName(String caseUserActivityName) {
    this.caseUserActivityName = caseUserActivityName;
    return this;
  }

  /**
   * Get caseUserActivityName
   * @return caseUserActivityName
  **/
  @ApiModelProperty(value = "")


  public String getCaseUserActivityName() {
    return caseUserActivityName;
  }

  public void setCaseUserActivityName(String caseUserActivityName) {
    this.caseUserActivityName = caseUserActivityName;
  }

  public CaseDecisionTrackerModel operation(String operation) {
    this.operation = operation;
    return this;
  }

  /**
   * Get operation
   * @return operation
  **/
  @ApiModelProperty(example = "start or end", value = "")


  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public CaseDecisionTrackerModel caseOperationTs(String caseOperationTs) {
    this.caseOperationTs = caseOperationTs;
    return this;
  }

  /**
   * Get caseOperationTs
   * @return caseOperationTs
  **/
  @ApiModelProperty(value = "")


  public String getCaseOperationTs() {
    return caseOperationTs;
  }

  public void setCaseOperationTs(String caseOperationTs) {
    this.caseOperationTs = caseOperationTs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CaseDecisionTrackerModel caseDecisionTrackerModel = (CaseDecisionTrackerModel) o;
    return Objects.equals(this.processType, caseDecisionTrackerModel.processType) &&
        Objects.equals(this.processTransactionType, caseDecisionTrackerModel.processTransactionType) &&
        Objects.equals(this.userID, caseDecisionTrackerModel.userID) &&
        Objects.equals(this.caseReferenceNumber, caseDecisionTrackerModel.caseReferenceNumber) &&
        Objects.equals(this.caseUserDecision, caseDecisionTrackerModel.caseUserDecision) &&
        Objects.equals(this.caseUserActivityName, caseDecisionTrackerModel.caseUserActivityName) &&
        Objects.equals(this.operation, caseDecisionTrackerModel.operation) &&
        Objects.equals(this.caseOperationTs, caseDecisionTrackerModel.caseOperationTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(processType, processTransactionType, userID, caseReferenceNumber, caseUserDecision, caseUserActivityName, operation, caseOperationTs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CaseDecisionTrackerModel {\n");
    
    sb.append("    processType: ").append(toIndentedString(processType)).append("\n");
    sb.append("    processTransactionType: ").append(toIndentedString(processTransactionType)).append("\n");
    sb.append("    userID: ").append(toIndentedString(userID)).append("\n");
    sb.append("    caseReferenceNumber: ").append(toIndentedString(caseReferenceNumber)).append("\n");
    sb.append("    caseUserDecision: ").append(toIndentedString(caseUserDecision)).append("\n");
    sb.append("    caseUserActivityName: ").append(toIndentedString(caseUserActivityName)).append("\n");
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("    caseOperationTs: ").append(toIndentedString(caseOperationTs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

