package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutGetAdjustmentCodeListWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutGetAdjustmentCodeListWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strAdjustmentCodeList")
  @Valid
  private List<String> strAdjustmentCodeList = null;

  public DataOutGetAdjustmentCodeListWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutGetAdjustmentCodeListWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutGetAdjustmentCodeListWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutGetAdjustmentCodeListWSResponse strAdjustmentCodeList(List<String> strAdjustmentCodeList) {
    this.strAdjustmentCodeList = strAdjustmentCodeList;
    return this;
  }

  public DataOutGetAdjustmentCodeListWSResponse addStrAdjustmentCodeListItem(String strAdjustmentCodeListItem) {
    if (this.strAdjustmentCodeList == null) {
      this.strAdjustmentCodeList = new ArrayList<>();
    }
    this.strAdjustmentCodeList.add(strAdjustmentCodeListItem);
    return this;
  }

  /**
   * Get strAdjustmentCodeList
   * @return strAdjustmentCodeList
  **/
  @ApiModelProperty(value = "")


  public List<String> getStrAdjustmentCodeList() {
    return strAdjustmentCodeList;
  }

  public void setStrAdjustmentCodeList(List<String> strAdjustmentCodeList) {
    this.strAdjustmentCodeList = strAdjustmentCodeList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutGetAdjustmentCodeListWSResponse dataOutGetAdjustmentCodeListWSResponse = (DataOutGetAdjustmentCodeListWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutGetAdjustmentCodeListWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutGetAdjustmentCodeListWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutGetAdjustmentCodeListWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strAdjustmentCodeList, dataOutGetAdjustmentCodeListWSResponse.strAdjustmentCodeList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strAdjustmentCodeList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutGetAdjustmentCodeListWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strAdjustmentCodeList: ").append(toIndentedString(strAdjustmentCodeList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

