package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSDNULRegClaimDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSDNULRegClaimDataIn   {
  @JsonProperty("CLAIMNO")
  private String CLAIMNO = null;

  @JsonProperty("ACTIVITYID")
  private String ACTIVITYID = null;

  public WSDNULRegClaimDataIn CLAIMNO(String CLAIMNO) {
    this.CLAIMNO = CLAIMNO;
    return this;
  }

  /**
   * Get CLAIMNO
   * @return CLAIMNO
  **/
  @ApiModelProperty(example = "[claim Number]", required = true, value = "")
  @NotNull


  public String getCLAIMNO() {
    return CLAIMNO;
  }

  public void setCLAIMNO(String CLAIMNO) {
    this.CLAIMNO = CLAIMNO;
  }

  public WSDNULRegClaimDataIn ACTIVITYID(String ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
    return this;
  }

  /**
   * Get ACTIVITYID
   * @return ACTIVITYID
  **/
  @ApiModelProperty(example = "[activityId]", required = true, value = "")
  @NotNull


  public String getACTIVITYID() {
    return ACTIVITYID;
  }

  public void setACTIVITYID(String ACTIVITYID) {
    this.ACTIVITYID = ACTIVITYID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSDNULRegClaimDataIn wsDNULRegClaimDataIn = (WSDNULRegClaimDataIn) o;
    return Objects.equals(this.CLAIMNO, wsDNULRegClaimDataIn.CLAIMNO) &&
        Objects.equals(this.ACTIVITYID, wsDNULRegClaimDataIn.ACTIVITYID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(CLAIMNO, ACTIVITYID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSDNULRegClaimDataIn {\n");
    
    sb.append("    CLAIMNO: ").append(toIndentedString(CLAIMNO)).append("\n");
    sb.append("    ACTIVITYID: ").append(toIndentedString(ACTIVITYID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

