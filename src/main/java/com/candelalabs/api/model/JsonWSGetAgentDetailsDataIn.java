package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSGetAgentDetailsDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSGetAgentDetailsDataIn   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strEmailCategory")
  private String strEmailCategory = null;

  public JsonWSGetAgentDetailsDataIn strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public JsonWSGetAgentDetailsDataIn strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public JsonWSGetAgentDetailsDataIn strEmailCategory(String strEmailCategory) {
    this.strEmailCategory = strEmailCategory;
    return this;
  }

  /**
   * Get strEmailCategory
   * @return strEmailCategory
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getStrEmailCategory() {
    return strEmailCategory;
  }

  public void setStrEmailCategory(String strEmailCategory) {
    this.strEmailCategory = strEmailCategory;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSGetAgentDetailsDataIn jsonWSGetAgentDetailsDataIn = (JsonWSGetAgentDetailsDataIn) o;
    return Objects.equals(this.strClaimNumber, jsonWSGetAgentDetailsDataIn.strClaimNumber) &&
        Objects.equals(this.strPolicyNumber, jsonWSGetAgentDetailsDataIn.strPolicyNumber) &&
        Objects.equals(this.strEmailCategory, jsonWSGetAgentDetailsDataIn.strEmailCategory);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strPolicyNumber, strEmailCategory);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSGetAgentDetailsDataIn {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strEmailCategory: ").append(toIndentedString(strEmailCategory)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

