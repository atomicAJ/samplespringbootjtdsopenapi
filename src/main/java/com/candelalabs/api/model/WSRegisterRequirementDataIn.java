package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSRegisterRequirementDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSRegisterRequirementDataIn   {
  @JsonProperty("CLAIMNUMBER")
  private String CLAIMNUMBER = null;

  @JsonProperty("APPNUMBER")
  private String APPNUMBER = null;

  @JsonProperty("POLICYNUMBER")
  private String POLICYNUMBER = null;

  @JsonProperty("TPACLAIMNUMBER")
  private String TPACLAIMNUMBER = null;

  @JsonProperty("BATCHNUMBER")
  private String BATCHNUMBER = null;

  @JsonProperty("PORTALREQUESTNUMBER")
  private String PORTALREQUESTNUMBER = null;

  @JsonProperty("POSREQUESTNUMBER")
  private String POSREQUESTNUMBER = null;

  @JsonProperty("REQTSUBCATEGORY")
  private String REQTSUBCATEGORY = null;

  @JsonProperty("REQDOCID")
  private String REQDOCID = null;

  @JsonProperty("REQTTEXT")
  private String REQTTEXT = null;

  @JsonProperty("REQTREQTS")
  private OffsetDateTime REQTREQTS = null;

  @JsonProperty("REQTRCDTS")
  private OffsetDateTime REQTRCDTS = null;

  @JsonProperty("REQTNOTIFYFLAG")
  private String REQTNOTIFYFLAG = null;

  @JsonProperty("CUSTOMERINITIATEDREQ")
  private String CUSTOMERINITIATEDREQ = null;

  @JsonProperty("REQTREQACTIVITYID")
  private String REQTREQACTIVITYID = null;

  public WSRegisterRequirementDataIn CLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
    return this;
  }

  /**
   * Get CLAIMNUMBER
   * @return CLAIMNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getCLAIMNUMBER() {
    return CLAIMNUMBER;
  }

  public void setCLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
  }

  public WSRegisterRequirementDataIn APPNUMBER(String APPNUMBER) {
    this.APPNUMBER = APPNUMBER;
    return this;
  }

  /**
   * Get APPNUMBER
   * @return APPNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getAPPNUMBER() {
    return APPNUMBER;
  }

  public void setAPPNUMBER(String APPNUMBER) {
    this.APPNUMBER = APPNUMBER;
  }

  public WSRegisterRequirementDataIn POLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
    return this;
  }

  /**
   * Get POLICYNUMBER
   * @return POLICYNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getPOLICYNUMBER() {
    return POLICYNUMBER;
  }

  public void setPOLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
  }

  public WSRegisterRequirementDataIn TPACLAIMNUMBER(String TPACLAIMNUMBER) {
    this.TPACLAIMNUMBER = TPACLAIMNUMBER;
    return this;
  }

  /**
   * Get TPACLAIMNUMBER
   * @return TPACLAIMNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getTPACLAIMNUMBER() {
    return TPACLAIMNUMBER;
  }

  public void setTPACLAIMNUMBER(String TPACLAIMNUMBER) {
    this.TPACLAIMNUMBER = TPACLAIMNUMBER;
  }

  public WSRegisterRequirementDataIn BATCHNUMBER(String BATCHNUMBER) {
    this.BATCHNUMBER = BATCHNUMBER;
    return this;
  }

  /**
   * Get BATCHNUMBER
   * @return BATCHNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getBATCHNUMBER() {
    return BATCHNUMBER;
  }

  public void setBATCHNUMBER(String BATCHNUMBER) {
    this.BATCHNUMBER = BATCHNUMBER;
  }

  public WSRegisterRequirementDataIn PORTALREQUESTNUMBER(String PORTALREQUESTNUMBER) {
    this.PORTALREQUESTNUMBER = PORTALREQUESTNUMBER;
    return this;
  }

  /**
   * Get PORTALREQUESTNUMBER
   * @return PORTALREQUESTNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getPORTALREQUESTNUMBER() {
    return PORTALREQUESTNUMBER;
  }

  public void setPORTALREQUESTNUMBER(String PORTALREQUESTNUMBER) {
    this.PORTALREQUESTNUMBER = PORTALREQUESTNUMBER;
  }

  public WSRegisterRequirementDataIn POSREQUESTNUMBER(String POSREQUESTNUMBER) {
    this.POSREQUESTNUMBER = POSREQUESTNUMBER;
    return this;
  }

  /**
   * Get POSREQUESTNUMBER
   * @return POSREQUESTNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getPOSREQUESTNUMBER() {
    return POSREQUESTNUMBER;
  }

  public void setPOSREQUESTNUMBER(String POSREQUESTNUMBER) {
    this.POSREQUESTNUMBER = POSREQUESTNUMBER;
  }

  public WSRegisterRequirementDataIn REQTSUBCATEGORY(String REQTSUBCATEGORY) {
    this.REQTSUBCATEGORY = REQTSUBCATEGORY;
    return this;
  }

  /**
   * Get REQTSUBCATEGORY
   * @return REQTSUBCATEGORY
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getREQTSUBCATEGORY() {
    return REQTSUBCATEGORY;
  }

  public void setREQTSUBCATEGORY(String REQTSUBCATEGORY) {
    this.REQTSUBCATEGORY = REQTSUBCATEGORY;
  }

  public WSRegisterRequirementDataIn REQDOCID(String REQDOCID) {
    this.REQDOCID = REQDOCID;
    return this;
  }

  /**
   * Get REQDOCID
   * @return REQDOCID
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getREQDOCID() {
    return REQDOCID;
  }

  public void setREQDOCID(String REQDOCID) {
    this.REQDOCID = REQDOCID;
  }

  public WSRegisterRequirementDataIn REQTTEXT(String REQTTEXT) {
    this.REQTTEXT = REQTTEXT;
    return this;
  }

  /**
   * Get REQTTEXT
   * @return REQTTEXT
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getREQTTEXT() {
    return REQTTEXT;
  }

  public void setREQTTEXT(String REQTTEXT) {
    this.REQTTEXT = REQTTEXT;
  }

  public WSRegisterRequirementDataIn REQTREQTS(OffsetDateTime REQTREQTS) {
    this.REQTREQTS = REQTREQTS;
    return this;
  }

  /**
   * Get REQTREQTS
   * @return REQTREQTS
  **/
  @ApiModelProperty(example = "2017-07-21T17:32:28Z", value = "")

  @Valid

  public OffsetDateTime getREQTREQTS() {
    return REQTREQTS;
  }

  public void setREQTREQTS(OffsetDateTime REQTREQTS) {
    this.REQTREQTS = REQTREQTS;
  }

  public WSRegisterRequirementDataIn REQTRCDTS(OffsetDateTime REQTRCDTS) {
    this.REQTRCDTS = REQTRCDTS;
    return this;
  }

  /**
   * Get REQTRCDTS
   * @return REQTRCDTS
  **/
  @ApiModelProperty(example = "2017-07-21T17:32:28Z", value = "")

  @Valid

  public OffsetDateTime getREQTRCDTS() {
    return REQTRCDTS;
  }

  public void setREQTRCDTS(OffsetDateTime REQTRCDTS) {
    this.REQTRCDTS = REQTRCDTS;
  }

  public WSRegisterRequirementDataIn REQTNOTIFYFLAG(String REQTNOTIFYFLAG) {
    this.REQTNOTIFYFLAG = REQTNOTIFYFLAG;
    return this;
  }

  /**
   * Get REQTNOTIFYFLAG
   * @return REQTNOTIFYFLAG
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getREQTNOTIFYFLAG() {
    return REQTNOTIFYFLAG;
  }

  public void setREQTNOTIFYFLAG(String REQTNOTIFYFLAG) {
    this.REQTNOTIFYFLAG = REQTNOTIFYFLAG;
  }

  public WSRegisterRequirementDataIn CUSTOMERINITIATEDREQ(String CUSTOMERINITIATEDREQ) {
    this.CUSTOMERINITIATEDREQ = CUSTOMERINITIATEDREQ;
    return this;
  }

  /**
   * Get CUSTOMERINITIATEDREQ
   * @return CUSTOMERINITIATEDREQ
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getCUSTOMERINITIATEDREQ() {
    return CUSTOMERINITIATEDREQ;
  }

  public void setCUSTOMERINITIATEDREQ(String CUSTOMERINITIATEDREQ) {
    this.CUSTOMERINITIATEDREQ = CUSTOMERINITIATEDREQ;
  }

  public WSRegisterRequirementDataIn REQTREQACTIVITYID(String REQTREQACTIVITYID) {
    this.REQTREQACTIVITYID = REQTREQACTIVITYID;
    return this;
  }

  /**
   * Get REQTREQACTIVITYID
   * @return REQTREQACTIVITYID
  **/
  @ApiModelProperty(value = "")


  public String getREQTREQACTIVITYID() {
    return REQTREQACTIVITYID;
  }

  public void setREQTREQACTIVITYID(String REQTREQACTIVITYID) {
    this.REQTREQACTIVITYID = REQTREQACTIVITYID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSRegisterRequirementDataIn wsRegisterRequirementDataIn = (WSRegisterRequirementDataIn) o;
    return Objects.equals(this.CLAIMNUMBER, wsRegisterRequirementDataIn.CLAIMNUMBER) &&
        Objects.equals(this.APPNUMBER, wsRegisterRequirementDataIn.APPNUMBER) &&
        Objects.equals(this.POLICYNUMBER, wsRegisterRequirementDataIn.POLICYNUMBER) &&
        Objects.equals(this.TPACLAIMNUMBER, wsRegisterRequirementDataIn.TPACLAIMNUMBER) &&
        Objects.equals(this.BATCHNUMBER, wsRegisterRequirementDataIn.BATCHNUMBER) &&
        Objects.equals(this.PORTALREQUESTNUMBER, wsRegisterRequirementDataIn.PORTALREQUESTNUMBER) &&
        Objects.equals(this.POSREQUESTNUMBER, wsRegisterRequirementDataIn.POSREQUESTNUMBER) &&
        Objects.equals(this.REQTSUBCATEGORY, wsRegisterRequirementDataIn.REQTSUBCATEGORY) &&
        Objects.equals(this.REQDOCID, wsRegisterRequirementDataIn.REQDOCID) &&
        Objects.equals(this.REQTTEXT, wsRegisterRequirementDataIn.REQTTEXT) &&
        Objects.equals(this.REQTREQTS, wsRegisterRequirementDataIn.REQTREQTS) &&
        Objects.equals(this.REQTRCDTS, wsRegisterRequirementDataIn.REQTRCDTS) &&
        Objects.equals(this.REQTNOTIFYFLAG, wsRegisterRequirementDataIn.REQTNOTIFYFLAG) &&
        Objects.equals(this.CUSTOMERINITIATEDREQ, wsRegisterRequirementDataIn.CUSTOMERINITIATEDREQ) &&
        Objects.equals(this.REQTREQACTIVITYID, wsRegisterRequirementDataIn.REQTREQACTIVITYID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(CLAIMNUMBER, APPNUMBER, POLICYNUMBER, TPACLAIMNUMBER, BATCHNUMBER, PORTALREQUESTNUMBER, POSREQUESTNUMBER, REQTSUBCATEGORY, REQDOCID, REQTTEXT, REQTREQTS, REQTRCDTS, REQTNOTIFYFLAG, CUSTOMERINITIATEDREQ, REQTREQACTIVITYID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSRegisterRequirementDataIn {\n");
    
    sb.append("    CLAIMNUMBER: ").append(toIndentedString(CLAIMNUMBER)).append("\n");
    sb.append("    APPNUMBER: ").append(toIndentedString(APPNUMBER)).append("\n");
    sb.append("    POLICYNUMBER: ").append(toIndentedString(POLICYNUMBER)).append("\n");
    sb.append("    TPACLAIMNUMBER: ").append(toIndentedString(TPACLAIMNUMBER)).append("\n");
    sb.append("    BATCHNUMBER: ").append(toIndentedString(BATCHNUMBER)).append("\n");
    sb.append("    PORTALREQUESTNUMBER: ").append(toIndentedString(PORTALREQUESTNUMBER)).append("\n");
    sb.append("    POSREQUESTNUMBER: ").append(toIndentedString(POSREQUESTNUMBER)).append("\n");
    sb.append("    REQTSUBCATEGORY: ").append(toIndentedString(REQTSUBCATEGORY)).append("\n");
    sb.append("    REQDOCID: ").append(toIndentedString(REQDOCID)).append("\n");
    sb.append("    REQTTEXT: ").append(toIndentedString(REQTTEXT)).append("\n");
    sb.append("    REQTREQTS: ").append(toIndentedString(REQTREQTS)).append("\n");
    sb.append("    REQTRCDTS: ").append(toIndentedString(REQTRCDTS)).append("\n");
    sb.append("    REQTNOTIFYFLAG: ").append(toIndentedString(REQTNOTIFYFLAG)).append("\n");
    sb.append("    CUSTOMERINITIATEDREQ: ").append(toIndentedString(CUSTOMERINITIATEDREQ)).append("\n");
    sb.append("    REQTREQACTIVITYID: ").append(toIndentedString(REQTREQACTIVITYID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

