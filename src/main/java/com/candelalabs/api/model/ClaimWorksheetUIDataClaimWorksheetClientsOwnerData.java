package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetClientsOwnerData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetClientsOwnerData   {
  @JsonProperty("strCLNTLifeName")
  private String strCLNTLifeName = null;

  @JsonProperty("strCLNTLifeNumber")
  private String strCLNTLifeNumber = null;

  @JsonProperty("strCLNTClientNumber")
  private String strCLNTClientNumber = null;

  @JsonProperty("dtCLNTClientDOB")
  private BigDecimal dtCLNTClientDOB = null;

  @JsonProperty("strCLNTGender")
  private String strCLNTGender = null;

  @JsonProperty("strCLNTAddress1")
  private String strCLNTAddress1 = null;

  @JsonProperty("strCLNTAddress2")
  private String strCLNTAddress2 = null;

  @JsonProperty("strCLNTAddress3")
  private String strCLNTAddress3 = null;

  @JsonProperty("strCLNTAddress4")
  private String strCLNTAddress4 = null;

  @JsonProperty("strCLNTAddress5")
  private String strCLNTAddress5 = null;

  @JsonProperty("strCLNTOccupationCode")
  private String strCLNTOccupationCode = null;

  @JsonProperty("strCLNTIDType")
  private String strCLNTIDType = null;

  @JsonProperty("strCLNTIDNumber")
  private String strCLNTIDNumber = null;

  @JsonProperty("strCLNTPhoneNumber1")
  private String strCLNTPhoneNumber1 = null;

  @JsonProperty("strCLNTPhoneNumber2")
  private String strCLNTPhoneNumber2 = null;

  @JsonProperty("strCLNTPhoneNumber3")
  private String strCLNTPhoneNumber3 = null;

  @JsonProperty("strCLNTNationality")
  private String strCLNTNationality = null;

  @JsonProperty("strCLNTClientStatus")
  private String strCLNTClientStatus = null;

  @JsonProperty("strCLNTRelationship")
  private String strCLNTRelationship = null;

  @JsonProperty("strCLNTBeneficiaryPercentage")
  private String strCLNTBeneficiaryPercentage = null;

  @JsonProperty("strCLNTAbuseFlag")
  private String strCLNTAbuseFlag = null;

  @JsonProperty("strCLNTFatcaFlag")
  private String strCLNTFatcaFlag = null;

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTLifeName(String strCLNTLifeName) {
    this.strCLNTLifeName = strCLNTLifeName;
    return this;
  }

  /**
   * Get strCLNTLifeName
   * @return strCLNTLifeName
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTLifeName() {
    return strCLNTLifeName;
  }

  public void setStrCLNTLifeName(String strCLNTLifeName) {
    this.strCLNTLifeName = strCLNTLifeName;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTLifeNumber(String strCLNTLifeNumber) {
    this.strCLNTLifeNumber = strCLNTLifeNumber;
    return this;
  }

  /**
   * Get strCLNTLifeNumber
   * @return strCLNTLifeNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTLifeNumber() {
    return strCLNTLifeNumber;
  }

  public void setStrCLNTLifeNumber(String strCLNTLifeNumber) {
    this.strCLNTLifeNumber = strCLNTLifeNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTClientNumber(String strCLNTClientNumber) {
    this.strCLNTClientNumber = strCLNTClientNumber;
    return this;
  }

  /**
   * Get strCLNTClientNumber
   * @return strCLNTClientNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTClientNumber() {
    return strCLNTClientNumber;
  }

  public void setStrCLNTClientNumber(String strCLNTClientNumber) {
    this.strCLNTClientNumber = strCLNTClientNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData dtCLNTClientDOB(BigDecimal dtCLNTClientDOB) {
    this.dtCLNTClientDOB = dtCLNTClientDOB;
    return this;
  }

  /**
   * Get dtCLNTClientDOB
   * @return dtCLNTClientDOB
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtCLNTClientDOB() {
    return dtCLNTClientDOB;
  }

  public void setDtCLNTClientDOB(BigDecimal dtCLNTClientDOB) {
    this.dtCLNTClientDOB = dtCLNTClientDOB;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTGender(String strCLNTGender) {
    this.strCLNTGender = strCLNTGender;
    return this;
  }

  /**
   * Get strCLNTGender
   * @return strCLNTGender
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTGender() {
    return strCLNTGender;
  }

  public void setStrCLNTGender(String strCLNTGender) {
    this.strCLNTGender = strCLNTGender;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTAddress1(String strCLNTAddress1) {
    this.strCLNTAddress1 = strCLNTAddress1;
    return this;
  }

  /**
   * Get strCLNTAddress1
   * @return strCLNTAddress1
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTAddress1() {
    return strCLNTAddress1;
  }

  public void setStrCLNTAddress1(String strCLNTAddress1) {
    this.strCLNTAddress1 = strCLNTAddress1;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTAddress2(String strCLNTAddress2) {
    this.strCLNTAddress2 = strCLNTAddress2;
    return this;
  }

  /**
   * Get strCLNTAddress2
   * @return strCLNTAddress2
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTAddress2() {
    return strCLNTAddress2;
  }

  public void setStrCLNTAddress2(String strCLNTAddress2) {
    this.strCLNTAddress2 = strCLNTAddress2;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTAddress3(String strCLNTAddress3) {
    this.strCLNTAddress3 = strCLNTAddress3;
    return this;
  }

  /**
   * Get strCLNTAddress3
   * @return strCLNTAddress3
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTAddress3() {
    return strCLNTAddress3;
  }

  public void setStrCLNTAddress3(String strCLNTAddress3) {
    this.strCLNTAddress3 = strCLNTAddress3;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTAddress4(String strCLNTAddress4) {
    this.strCLNTAddress4 = strCLNTAddress4;
    return this;
  }

  /**
   * Get strCLNTAddress4
   * @return strCLNTAddress4
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTAddress4() {
    return strCLNTAddress4;
  }

  public void setStrCLNTAddress4(String strCLNTAddress4) {
    this.strCLNTAddress4 = strCLNTAddress4;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTAddress5(String strCLNTAddress5) {
    this.strCLNTAddress5 = strCLNTAddress5;
    return this;
  }

  /**
   * Get strCLNTAddress5
   * @return strCLNTAddress5
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTAddress5() {
    return strCLNTAddress5;
  }

  public void setStrCLNTAddress5(String strCLNTAddress5) {
    this.strCLNTAddress5 = strCLNTAddress5;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTOccupationCode(String strCLNTOccupationCode) {
    this.strCLNTOccupationCode = strCLNTOccupationCode;
    return this;
  }

  /**
   * Get strCLNTOccupationCode
   * @return strCLNTOccupationCode
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTOccupationCode() {
    return strCLNTOccupationCode;
  }

  public void setStrCLNTOccupationCode(String strCLNTOccupationCode) {
    this.strCLNTOccupationCode = strCLNTOccupationCode;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTIDType(String strCLNTIDType) {
    this.strCLNTIDType = strCLNTIDType;
    return this;
  }

  /**
   * Get strCLNTIDType
   * @return strCLNTIDType
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTIDType() {
    return strCLNTIDType;
  }

  public void setStrCLNTIDType(String strCLNTIDType) {
    this.strCLNTIDType = strCLNTIDType;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTIDNumber(String strCLNTIDNumber) {
    this.strCLNTIDNumber = strCLNTIDNumber;
    return this;
  }

  /**
   * Get strCLNTIDNumber
   * @return strCLNTIDNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTIDNumber() {
    return strCLNTIDNumber;
  }

  public void setStrCLNTIDNumber(String strCLNTIDNumber) {
    this.strCLNTIDNumber = strCLNTIDNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTPhoneNumber1(String strCLNTPhoneNumber1) {
    this.strCLNTPhoneNumber1 = strCLNTPhoneNumber1;
    return this;
  }

  /**
   * Get strCLNTPhoneNumber1
   * @return strCLNTPhoneNumber1
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTPhoneNumber1() {
    return strCLNTPhoneNumber1;
  }

  public void setStrCLNTPhoneNumber1(String strCLNTPhoneNumber1) {
    this.strCLNTPhoneNumber1 = strCLNTPhoneNumber1;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTPhoneNumber2(String strCLNTPhoneNumber2) {
    this.strCLNTPhoneNumber2 = strCLNTPhoneNumber2;
    return this;
  }

  /**
   * Get strCLNTPhoneNumber2
   * @return strCLNTPhoneNumber2
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTPhoneNumber2() {
    return strCLNTPhoneNumber2;
  }

  public void setStrCLNTPhoneNumber2(String strCLNTPhoneNumber2) {
    this.strCLNTPhoneNumber2 = strCLNTPhoneNumber2;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTPhoneNumber3(String strCLNTPhoneNumber3) {
    this.strCLNTPhoneNumber3 = strCLNTPhoneNumber3;
    return this;
  }

  /**
   * Get strCLNTPhoneNumber3
   * @return strCLNTPhoneNumber3
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTPhoneNumber3() {
    return strCLNTPhoneNumber3;
  }

  public void setStrCLNTPhoneNumber3(String strCLNTPhoneNumber3) {
    this.strCLNTPhoneNumber3 = strCLNTPhoneNumber3;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTNationality(String strCLNTNationality) {
    this.strCLNTNationality = strCLNTNationality;
    return this;
  }

  /**
   * Get strCLNTNationality
   * @return strCLNTNationality
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTNationality() {
    return strCLNTNationality;
  }

  public void setStrCLNTNationality(String strCLNTNationality) {
    this.strCLNTNationality = strCLNTNationality;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTClientStatus(String strCLNTClientStatus) {
    this.strCLNTClientStatus = strCLNTClientStatus;
    return this;
  }

  /**
   * Get strCLNTClientStatus
   * @return strCLNTClientStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTClientStatus() {
    return strCLNTClientStatus;
  }

  public void setStrCLNTClientStatus(String strCLNTClientStatus) {
    this.strCLNTClientStatus = strCLNTClientStatus;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTRelationship(String strCLNTRelationship) {
    this.strCLNTRelationship = strCLNTRelationship;
    return this;
  }

  /**
   * Get strCLNTRelationship
   * @return strCLNTRelationship
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTRelationship() {
    return strCLNTRelationship;
  }

  public void setStrCLNTRelationship(String strCLNTRelationship) {
    this.strCLNTRelationship = strCLNTRelationship;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTBeneficiaryPercentage(String strCLNTBeneficiaryPercentage) {
    this.strCLNTBeneficiaryPercentage = strCLNTBeneficiaryPercentage;
    return this;
  }

  /**
   * Get strCLNTBeneficiaryPercentage
   * @return strCLNTBeneficiaryPercentage
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTBeneficiaryPercentage() {
    return strCLNTBeneficiaryPercentage;
  }

  public void setStrCLNTBeneficiaryPercentage(String strCLNTBeneficiaryPercentage) {
    this.strCLNTBeneficiaryPercentage = strCLNTBeneficiaryPercentage;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTAbuseFlag(String strCLNTAbuseFlag) {
    this.strCLNTAbuseFlag = strCLNTAbuseFlag;
    return this;
  }

  /**
   * Get strCLNTAbuseFlag
   * @return strCLNTAbuseFlag
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTAbuseFlag() {
    return strCLNTAbuseFlag;
  }

  public void setStrCLNTAbuseFlag(String strCLNTAbuseFlag) {
    this.strCLNTAbuseFlag = strCLNTAbuseFlag;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientsOwnerData strCLNTFatcaFlag(String strCLNTFatcaFlag) {
    this.strCLNTFatcaFlag = strCLNTFatcaFlag;
    return this;
  }

  /**
   * Get strCLNTFatcaFlag
   * @return strCLNTFatcaFlag
  **/
  @ApiModelProperty(value = "")


  public String getStrCLNTFatcaFlag() {
    return strCLNTFatcaFlag;
  }

  public void setStrCLNTFatcaFlag(String strCLNTFatcaFlag) {
    this.strCLNTFatcaFlag = strCLNTFatcaFlag;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetClientsOwnerData claimWorksheetUIDataClaimWorksheetClientsOwnerData = (ClaimWorksheetUIDataClaimWorksheetClientsOwnerData) o;
    return Objects.equals(this.strCLNTLifeName, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTLifeName) &&
        Objects.equals(this.strCLNTLifeNumber, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTLifeNumber) &&
        Objects.equals(this.strCLNTClientNumber, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTClientNumber) &&
        Objects.equals(this.dtCLNTClientDOB, claimWorksheetUIDataClaimWorksheetClientsOwnerData.dtCLNTClientDOB) &&
        Objects.equals(this.strCLNTGender, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTGender) &&
        Objects.equals(this.strCLNTAddress1, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTAddress1) &&
        Objects.equals(this.strCLNTAddress2, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTAddress2) &&
        Objects.equals(this.strCLNTAddress3, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTAddress3) &&
        Objects.equals(this.strCLNTAddress4, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTAddress4) &&
        Objects.equals(this.strCLNTAddress5, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTAddress5) &&
        Objects.equals(this.strCLNTOccupationCode, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTOccupationCode) &&
        Objects.equals(this.strCLNTIDType, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTIDType) &&
        Objects.equals(this.strCLNTIDNumber, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTIDNumber) &&
        Objects.equals(this.strCLNTPhoneNumber1, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTPhoneNumber1) &&
        Objects.equals(this.strCLNTPhoneNumber2, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTPhoneNumber2) &&
        Objects.equals(this.strCLNTPhoneNumber3, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTPhoneNumber3) &&
        Objects.equals(this.strCLNTNationality, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTNationality) &&
        Objects.equals(this.strCLNTClientStatus, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTClientStatus) &&
        Objects.equals(this.strCLNTRelationship, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTRelationship) &&
        Objects.equals(this.strCLNTBeneficiaryPercentage, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTBeneficiaryPercentage) &&
        Objects.equals(this.strCLNTAbuseFlag, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTAbuseFlag) &&
        Objects.equals(this.strCLNTFatcaFlag, claimWorksheetUIDataClaimWorksheetClientsOwnerData.strCLNTFatcaFlag);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strCLNTLifeName, strCLNTLifeNumber, strCLNTClientNumber, dtCLNTClientDOB, strCLNTGender, strCLNTAddress1, strCLNTAddress2, strCLNTAddress3, strCLNTAddress4, strCLNTAddress5, strCLNTOccupationCode, strCLNTIDType, strCLNTIDNumber, strCLNTPhoneNumber1, strCLNTPhoneNumber2, strCLNTPhoneNumber3, strCLNTNationality, strCLNTClientStatus, strCLNTRelationship, strCLNTBeneficiaryPercentage, strCLNTAbuseFlag, strCLNTFatcaFlag);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetClientsOwnerData {\n");
    
    sb.append("    strCLNTLifeName: ").append(toIndentedString(strCLNTLifeName)).append("\n");
    sb.append("    strCLNTLifeNumber: ").append(toIndentedString(strCLNTLifeNumber)).append("\n");
    sb.append("    strCLNTClientNumber: ").append(toIndentedString(strCLNTClientNumber)).append("\n");
    sb.append("    dtCLNTClientDOB: ").append(toIndentedString(dtCLNTClientDOB)).append("\n");
    sb.append("    strCLNTGender: ").append(toIndentedString(strCLNTGender)).append("\n");
    sb.append("    strCLNTAddress1: ").append(toIndentedString(strCLNTAddress1)).append("\n");
    sb.append("    strCLNTAddress2: ").append(toIndentedString(strCLNTAddress2)).append("\n");
    sb.append("    strCLNTAddress3: ").append(toIndentedString(strCLNTAddress3)).append("\n");
    sb.append("    strCLNTAddress4: ").append(toIndentedString(strCLNTAddress4)).append("\n");
    sb.append("    strCLNTAddress5: ").append(toIndentedString(strCLNTAddress5)).append("\n");
    sb.append("    strCLNTOccupationCode: ").append(toIndentedString(strCLNTOccupationCode)).append("\n");
    sb.append("    strCLNTIDType: ").append(toIndentedString(strCLNTIDType)).append("\n");
    sb.append("    strCLNTIDNumber: ").append(toIndentedString(strCLNTIDNumber)).append("\n");
    sb.append("    strCLNTPhoneNumber1: ").append(toIndentedString(strCLNTPhoneNumber1)).append("\n");
    sb.append("    strCLNTPhoneNumber2: ").append(toIndentedString(strCLNTPhoneNumber2)).append("\n");
    sb.append("    strCLNTPhoneNumber3: ").append(toIndentedString(strCLNTPhoneNumber3)).append("\n");
    sb.append("    strCLNTNationality: ").append(toIndentedString(strCLNTNationality)).append("\n");
    sb.append("    strCLNTClientStatus: ").append(toIndentedString(strCLNTClientStatus)).append("\n");
    sb.append("    strCLNTRelationship: ").append(toIndentedString(strCLNTRelationship)).append("\n");
    sb.append("    strCLNTBeneficiaryPercentage: ").append(toIndentedString(strCLNTBeneficiaryPercentage)).append("\n");
    sb.append("    strCLNTAbuseFlag: ").append(toIndentedString(strCLNTAbuseFlag)).append("\n");
    sb.append("    strCLNTFatcaFlag: ").append(toIndentedString(strCLNTFatcaFlag)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

