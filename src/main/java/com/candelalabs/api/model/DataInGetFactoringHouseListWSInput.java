package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetFactoringHouseListWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetFactoringHouseListWSInput   {
  @JsonProperty("strCurrencyCode")
  private String strCurrencyCode = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  public DataInGetFactoringHouseListWSInput strCurrencyCode(String strCurrencyCode) {
    this.strCurrencyCode = strCurrencyCode;
    return this;
  }

  /**
   * Get strCurrencyCode
   * @return strCurrencyCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrCurrencyCode() {
    return strCurrencyCode;
  }

  public void setStrCurrencyCode(String strCurrencyCode) {
    this.strCurrencyCode = strCurrencyCode;
  }

  public DataInGetFactoringHouseListWSInput strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetFactoringHouseListWSInput dataInGetFactoringHouseListWSInput = (DataInGetFactoringHouseListWSInput) o;
    return Objects.equals(this.strCurrencyCode, dataInGetFactoringHouseListWSInput.strCurrencyCode) &&
        Objects.equals(this.strPolicyNumber, dataInGetFactoringHouseListWSInput.strPolicyNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strCurrencyCode, strPolicyNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetFactoringHouseListWSInput {\n");
    
    sb.append("    strCurrencyCode: ").append(toIndentedString(strCurrencyCode)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

