package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSDownloadGeneratedReportDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSDownloadGeneratedReportDataInWSInput   {
  @JsonProperty("strReportNamePath")
  private String strReportNamePath = null;

  @JsonProperty("strReportId")
  private String strReportId = null;

  public WSDownloadGeneratedReportDataInWSInput strReportNamePath(String strReportNamePath) {
    this.strReportNamePath = strReportNamePath;
    return this;
  }

  /**
   * Get strReportNamePath
   * @return strReportNamePath
  **/
  @ApiModelProperty(example = "STP.xlxs", value = "")


  public String getStrReportNamePath() {
    return strReportNamePath;
  }

  public void setStrReportNamePath(String strReportNamePath) {
    this.strReportNamePath = strReportNamePath;
  }

  public WSDownloadGeneratedReportDataInWSInput strReportId(String strReportId) {
    this.strReportId = strReportId;
    return this;
  }

  /**
   * Get strReportId
   * @return strReportId
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getStrReportId() {
    return strReportId;
  }

  public void setStrReportId(String strReportId) {
    this.strReportId = strReportId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSDownloadGeneratedReportDataInWSInput wsDownloadGeneratedReportDataInWSInput = (WSDownloadGeneratedReportDataInWSInput) o;
    return Objects.equals(this.strReportNamePath, wsDownloadGeneratedReportDataInWSInput.strReportNamePath) &&
        Objects.equals(this.strReportId, wsDownloadGeneratedReportDataInWSInput.strReportId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strReportNamePath, strReportId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSDownloadGeneratedReportDataInWSInput {\n");
    
    sb.append("    strReportNamePath: ").append(toIndentedString(strReportNamePath)).append("\n");
    sb.append("    strReportId: ").append(toIndentedString(strReportId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

