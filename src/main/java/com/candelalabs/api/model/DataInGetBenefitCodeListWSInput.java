package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetBenefitCodeListWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetBenefitCodeListWSInput   {
  @JsonProperty("strComponentCode")
  private String strComponentCode = null;

  @JsonProperty("strClaimTypeUI")
  private String strClaimTypeUI = null;

  public DataInGetBenefitCodeListWSInput strComponentCode(String strComponentCode) {
    this.strComponentCode = strComponentCode;
    return this;
  }

  /**
   * Get strComponentCode
   * @return strComponentCode
  **/
  @ApiModelProperty(value = "")


  public String getStrComponentCode() {
    return strComponentCode;
  }

  public void setStrComponentCode(String strComponentCode) {
    this.strComponentCode = strComponentCode;
  }

  public DataInGetBenefitCodeListWSInput strClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
    return this;
  }

  /**
   * Get strClaimTypeUI
   * @return strClaimTypeUI
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeUI() {
    return strClaimTypeUI;
  }

  public void setStrClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetBenefitCodeListWSInput dataInGetBenefitCodeListWSInput = (DataInGetBenefitCodeListWSInput) o;
    return Objects.equals(this.strComponentCode, dataInGetBenefitCodeListWSInput.strComponentCode) &&
        Objects.equals(this.strClaimTypeUI, dataInGetBenefitCodeListWSInput.strClaimTypeUI);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strComponentCode, strClaimTypeUI);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetBenefitCodeListWSInput {\n");
    
    sb.append("    strComponentCode: ").append(toIndentedString(strComponentCode)).append("\n");
    sb.append("    strClaimTypeUI: ").append(toIndentedString(strClaimTypeUI)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

