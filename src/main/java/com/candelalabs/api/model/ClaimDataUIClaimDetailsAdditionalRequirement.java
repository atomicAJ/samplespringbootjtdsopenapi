package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDataUIClaimDetailsAdditionalRequirement
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDataUIClaimDetailsAdditionalRequirement   {
  @JsonProperty("reqtDropDownText")
  private String reqtDropDownText = null;

  @JsonProperty("subCategory")
  private String subCategory = null;

  @JsonProperty("additionaltext")
  private String additionaltext = null;

  @JsonProperty("notificationsent")
  private String notificationsent = null;

  @JsonProperty("__notificationcomment__")
  private String notificationcomment_ = null;

  public ClaimDataUIClaimDetailsAdditionalRequirement reqtDropDownText(String reqtDropDownText) {
    this.reqtDropDownText = reqtDropDownText;
    return this;
  }

  /**
   * Get reqtDropDownText
   * @return reqtDropDownText
  **/
  @ApiModelProperty(value = "")


  public String getReqtDropDownText() {
    return reqtDropDownText;
  }

  public void setReqtDropDownText(String reqtDropDownText) {
    this.reqtDropDownText = reqtDropDownText;
  }

  public ClaimDataUIClaimDetailsAdditionalRequirement subCategory(String subCategory) {
    this.subCategory = subCategory;
    return this;
  }

  /**
   * Get subCategory
   * @return subCategory
  **/
  @ApiModelProperty(value = "")


  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }

  public ClaimDataUIClaimDetailsAdditionalRequirement additionaltext(String additionaltext) {
    this.additionaltext = additionaltext;
    return this;
  }

  /**
   * Get additionaltext
   * @return additionaltext
  **/
  @ApiModelProperty(value = "")


  public String getAdditionaltext() {
    return additionaltext;
  }

  public void setAdditionaltext(String additionaltext) {
    this.additionaltext = additionaltext;
  }

  public ClaimDataUIClaimDetailsAdditionalRequirement notificationsent(String notificationsent) {
    this.notificationsent = notificationsent;
    return this;
  }

  /**
   * Get notificationsent
   * @return notificationsent
  **/
  @ApiModelProperty(value = "")


  public String getNotificationsent() {
    return notificationsent;
  }

  public void setNotificationsent(String notificationsent) {
    this.notificationsent = notificationsent;
  }

  public ClaimDataUIClaimDetailsAdditionalRequirement notificationcomment_(String notificationcomment_) {
    this.notificationcomment_ = notificationcomment_;
    return this;
  }

  /**
   * Get notificationcomment_
   * @return notificationcomment_
  **/
  @ApiModelProperty(value = "")


  public String getNotificationcomment_() {
    return notificationcomment_;
  }

  public void setNotificationcomment_(String notificationcomment_) {
    this.notificationcomment_ = notificationcomment_;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDataUIClaimDetailsAdditionalRequirement claimDataUIClaimDetailsAdditionalRequirement = (ClaimDataUIClaimDetailsAdditionalRequirement) o;
    return Objects.equals(this.reqtDropDownText, claimDataUIClaimDetailsAdditionalRequirement.reqtDropDownText) &&
        Objects.equals(this.subCategory, claimDataUIClaimDetailsAdditionalRequirement.subCategory) &&
        Objects.equals(this.additionaltext, claimDataUIClaimDetailsAdditionalRequirement.additionaltext) &&
        Objects.equals(this.notificationsent, claimDataUIClaimDetailsAdditionalRequirement.notificationsent) &&
        Objects.equals(this.notificationcomment_, claimDataUIClaimDetailsAdditionalRequirement.notificationcomment_);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reqtDropDownText, subCategory, additionaltext, notificationsent, notificationcomment_);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDataUIClaimDetailsAdditionalRequirement {\n");
    
    sb.append("    reqtDropDownText: ").append(toIndentedString(reqtDropDownText)).append("\n");
    sb.append("    subCategory: ").append(toIndentedString(subCategory)).append("\n");
    sb.append("    additionaltext: ").append(toIndentedString(additionaltext)).append("\n");
    sb.append("    notificationsent: ").append(toIndentedString(notificationsent)).append("\n");
    sb.append("    notificationcomment_: ").append(toIndentedString(notificationcomment_)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

