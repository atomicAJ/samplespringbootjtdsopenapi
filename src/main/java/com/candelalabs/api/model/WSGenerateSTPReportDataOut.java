package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.STPReportData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGenerateSTPReportDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGenerateSTPReportDataOut   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strReportRequestID")
  private String strReportRequestID = null;

  @JsonProperty("reportData")
  @Valid
  private List<STPReportData> reportData = null;

  public WSGenerateSTPReportDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSGenerateSTPReportDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSGenerateSTPReportDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public WSGenerateSTPReportDataOut strReportRequestID(String strReportRequestID) {
    this.strReportRequestID = strReportRequestID;
    return this;
  }

  /**
   * Get strReportRequestID
   * @return strReportRequestID
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getStrReportRequestID() {
    return strReportRequestID;
  }

  public void setStrReportRequestID(String strReportRequestID) {
    this.strReportRequestID = strReportRequestID;
  }

  public WSGenerateSTPReportDataOut reportData(List<STPReportData> reportData) {
    this.reportData = reportData;
    return this;
  }

  public WSGenerateSTPReportDataOut addReportDataItem(STPReportData reportDataItem) {
    if (this.reportData == null) {
      this.reportData = new ArrayList<>();
    }
    this.reportData.add(reportDataItem);
    return this;
  }

  /**
   * Get reportData
   * @return reportData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<STPReportData> getReportData() {
    return reportData;
  }

  public void setReportData(List<STPReportData> reportData) {
    this.reportData = reportData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGenerateSTPReportDataOut wsGenerateSTPReportDataOut = (WSGenerateSTPReportDataOut) o;
    return Objects.equals(this.wsProcessingStatus, wsGenerateSTPReportDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsGenerateSTPReportDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsGenerateSTPReportDataOut.wsSuccessMessage) &&
        Objects.equals(this.strReportRequestID, wsGenerateSTPReportDataOut.strReportRequestID) &&
        Objects.equals(this.reportData, wsGenerateSTPReportDataOut.reportData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strReportRequestID, reportData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGenerateSTPReportDataOut {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strReportRequestID: ").append(toIndentedString(strReportRequestID)).append("\n");
    sb.append("    reportData: ").append(toIndentedString(reportData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

