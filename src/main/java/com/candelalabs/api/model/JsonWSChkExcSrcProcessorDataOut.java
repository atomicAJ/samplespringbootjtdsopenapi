package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSChkExcSrcProcessorDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSChkExcSrcProcessorDataOut   {
  @JsonProperty("processmsg")
  private String processmsg = null;

  @JsonProperty("decision")
  private String decision = null;

  @JsonProperty("SOURCEEXCEPTIONSTEP")
  private String SOURCEEXCEPTIONSTEP = null;

  @JsonProperty("SOURCEEXCEPTIONPROCESS")
  private String SOURCEEXCEPTIONPROCESS = null;

  public JsonWSChkExcSrcProcessorDataOut processmsg(String processmsg) {
    this.processmsg = processmsg;
    return this;
  }

  /**
   * Get processmsg
   * @return processmsg
  **/
  @ApiModelProperty(example = "[Success]", required = true, value = "")
  @NotNull


  public String getProcessmsg() {
    return processmsg;
  }

  public void setProcessmsg(String processmsg) {
    this.processmsg = processmsg;
  }

  public JsonWSChkExcSrcProcessorDataOut decision(String decision) {
    this.decision = decision;
    return this;
  }

  /**
   * Get decision
   * @return decision
  **/
  @ApiModelProperty(example = "[Success]", required = true, value = "")
  @NotNull


  public String getDecision() {
    return decision;
  }

  public void setDecision(String decision) {
    this.decision = decision;
  }

  public JsonWSChkExcSrcProcessorDataOut SOURCEEXCEPTIONSTEP(String SOURCEEXCEPTIONSTEP) {
    this.SOURCEEXCEPTIONSTEP = SOURCEEXCEPTIONSTEP;
    return this;
  }

  /**
   * Get SOURCEEXCEPTIONSTEP
   * @return SOURCEEXCEPTIONSTEP
  **/
  @ApiModelProperty(example = "[SOURCEEXCEPTIONSTEP]", required = true, value = "")
  @NotNull


  public String getSOURCEEXCEPTIONSTEP() {
    return SOURCEEXCEPTIONSTEP;
  }

  public void setSOURCEEXCEPTIONSTEP(String SOURCEEXCEPTIONSTEP) {
    this.SOURCEEXCEPTIONSTEP = SOURCEEXCEPTIONSTEP;
  }

  public JsonWSChkExcSrcProcessorDataOut SOURCEEXCEPTIONPROCESS(String SOURCEEXCEPTIONPROCESS) {
    this.SOURCEEXCEPTIONPROCESS = SOURCEEXCEPTIONPROCESS;
    return this;
  }

  /**
   * Get SOURCEEXCEPTIONPROCESS
   * @return SOURCEEXCEPTIONPROCESS
  **/
  @ApiModelProperty(example = "[Cashless]", required = true, value = "")
  @NotNull


  public String getSOURCEEXCEPTIONPROCESS() {
    return SOURCEEXCEPTIONPROCESS;
  }

  public void setSOURCEEXCEPTIONPROCESS(String SOURCEEXCEPTIONPROCESS) {
    this.SOURCEEXCEPTIONPROCESS = SOURCEEXCEPTIONPROCESS;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSChkExcSrcProcessorDataOut jsonWSChkExcSrcProcessorDataOut = (JsonWSChkExcSrcProcessorDataOut) o;
    return Objects.equals(this.processmsg, jsonWSChkExcSrcProcessorDataOut.processmsg) &&
        Objects.equals(this.decision, jsonWSChkExcSrcProcessorDataOut.decision) &&
        Objects.equals(this.SOURCEEXCEPTIONSTEP, jsonWSChkExcSrcProcessorDataOut.SOURCEEXCEPTIONSTEP) &&
        Objects.equals(this.SOURCEEXCEPTIONPROCESS, jsonWSChkExcSrcProcessorDataOut.SOURCEEXCEPTIONPROCESS);
  }

  @Override
  public int hashCode() {
    return Objects.hash(processmsg, decision, SOURCEEXCEPTIONSTEP, SOURCEEXCEPTIONPROCESS);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSChkExcSrcProcessorDataOut {\n");
    
    sb.append("    processmsg: ").append(toIndentedString(processmsg)).append("\n");
    sb.append("    decision: ").append(toIndentedString(decision)).append("\n");
    sb.append("    SOURCEEXCEPTIONSTEP: ").append(toIndentedString(SOURCEEXCEPTIONSTEP)).append("\n");
    sb.append("    SOURCEEXCEPTIONPROCESS: ").append(toIndentedString(SOURCEEXCEPTIONPROCESS)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

