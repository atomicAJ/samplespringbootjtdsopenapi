package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClaimHistory;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClients;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetComments;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetComponentsListData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheet
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheet   {
  @JsonProperty("strApplicationNumber")
  private String strApplicationNumber = null;

  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPOSRequestNumber")
  private String strPOSRequestNumber = null;

  @JsonProperty("strProcessType")
  private String strProcessType = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strProductCode")
  private String strProductCode = null;

  @JsonProperty("strProductName")
  private String strProductName = null;

  @JsonProperty("strPolicyStatus")
  private String strPolicyStatus = null;

  @JsonProperty("strPolicyCurrency")
  private String strPolicyCurrency = null;

  @JsonProperty("strPremiumStatus")
  private String strPremiumStatus = null;

  @JsonProperty("strOwnerName")
  private String strOwnerName = null;

  @JsonProperty("strOwnerClientNumber")
  private String strOwnerClientNumber = null;

  @JsonProperty("strLifeClientNumber")
  private String strLifeClientNumber = null;

  @JsonProperty("strPayorName")
  private String strPayorName = null;

  @JsonProperty("strPayorClientNumber")
  private String strPayorClientNumber = null;

  @JsonProperty("dtPolicyIssuedDate")
  private BigDecimal dtPolicyIssuedDate = null;

  @JsonProperty("dtRiskCommencementDate")
  private BigDecimal dtRiskCommencementDate = null;

  @JsonProperty("dtPaidToDate")
  private BigDecimal dtPaidToDate = null;

  @JsonProperty("dtBillToDate")
  private BigDecimal dtBillToDate = null;

  @JsonProperty("strPaymentMethod")
  private String strPaymentMethod = null;

  @JsonProperty("strPaymentFrequency")
  private String strPaymentFrequency = null;

  @JsonProperty("strAgentName")
  private String strAgentName = null;

  @JsonProperty("strAgentNumber")
  private String strAgentNumber = null;

  @JsonProperty("dblTotalPremium")
  private BigDecimal dblTotalPremium = null;

  @JsonProperty("dblBasicPremium")
  private BigDecimal dblBasicPremium = null;

  @JsonProperty("dblRegularTopup")
  private BigDecimal dblRegularTopup = null;

  @JsonProperty("dtReinstatementDate")
  private BigDecimal dtReinstatementDate = null;

  @JsonProperty("componentsListData")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> componentsListData = null;

  @JsonProperty("policyRelatedDetails")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> policyRelatedDetails = null;

  @JsonProperty("clients")
  private ClaimWorksheetUIDataClaimWorksheetClients clients = null;

  @JsonProperty("claimHistory")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> claimHistory = null;

  @JsonProperty("Comments")
  private ClaimWorksheetUIDataClaimWorksheetComments comments = null;

  public ClaimWorksheetUIDataClaimWorksheet strApplicationNumber(String strApplicationNumber) {
    this.strApplicationNumber = strApplicationNumber;
    return this;
  }

  /**
   * Get strApplicationNumber
   * @return strApplicationNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrApplicationNumber() {
    return strApplicationNumber;
  }

  public void setStrApplicationNumber(String strApplicationNumber) {
    this.strApplicationNumber = strApplicationNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPOSRequestNumber(String strPOSRequestNumber) {
    this.strPOSRequestNumber = strPOSRequestNumber;
    return this;
  }

  /**
   * Get strPOSRequestNumber
   * @return strPOSRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPOSRequestNumber() {
    return strPOSRequestNumber;
  }

  public void setStrPOSRequestNumber(String strPOSRequestNumber) {
    this.strPOSRequestNumber = strPOSRequestNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet strProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
    return this;
  }

  /**
   * Get strProcessType
   * @return strProcessType
  **/
  @ApiModelProperty(value = "")


  public String getStrProcessType() {
    return strProcessType;
  }

  public void setStrProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet strProductCode(String strProductCode) {
    this.strProductCode = strProductCode;
    return this;
  }

  /**
   * Get strProductCode
   * @return strProductCode
  **/
  @ApiModelProperty(value = "")


  public String getStrProductCode() {
    return strProductCode;
  }

  public void setStrProductCode(String strProductCode) {
    this.strProductCode = strProductCode;
  }

  public ClaimWorksheetUIDataClaimWorksheet strProductName(String strProductName) {
    this.strProductName = strProductName;
    return this;
  }

  /**
   * Get strProductName
   * @return strProductName
  **/
  @ApiModelProperty(value = "")


  public String getStrProductName() {
    return strProductName;
  }

  public void setStrProductName(String strProductName) {
    this.strProductName = strProductName;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPolicyStatus(String strPolicyStatus) {
    this.strPolicyStatus = strPolicyStatus;
    return this;
  }

  /**
   * Get strPolicyStatus
   * @return strPolicyStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyStatus() {
    return strPolicyStatus;
  }

  public void setStrPolicyStatus(String strPolicyStatus) {
    this.strPolicyStatus = strPolicyStatus;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPolicyCurrency(String strPolicyCurrency) {
    this.strPolicyCurrency = strPolicyCurrency;
    return this;
  }

  /**
   * Get strPolicyCurrency
   * @return strPolicyCurrency
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyCurrency() {
    return strPolicyCurrency;
  }

  public void setStrPolicyCurrency(String strPolicyCurrency) {
    this.strPolicyCurrency = strPolicyCurrency;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPremiumStatus(String strPremiumStatus) {
    this.strPremiumStatus = strPremiumStatus;
    return this;
  }

  /**
   * Get strPremiumStatus
   * @return strPremiumStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrPremiumStatus() {
    return strPremiumStatus;
  }

  public void setStrPremiumStatus(String strPremiumStatus) {
    this.strPremiumStatus = strPremiumStatus;
  }

  public ClaimWorksheetUIDataClaimWorksheet strOwnerName(String strOwnerName) {
    this.strOwnerName = strOwnerName;
    return this;
  }

  /**
   * Get strOwnerName
   * @return strOwnerName
  **/
  @ApiModelProperty(value = "")


  public String getStrOwnerName() {
    return strOwnerName;
  }

  public void setStrOwnerName(String strOwnerName) {
    this.strOwnerName = strOwnerName;
  }

  public ClaimWorksheetUIDataClaimWorksheet strOwnerClientNumber(String strOwnerClientNumber) {
    this.strOwnerClientNumber = strOwnerClientNumber;
    return this;
  }

  /**
   * Get strOwnerClientNumber
   * @return strOwnerClientNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrOwnerClientNumber() {
    return strOwnerClientNumber;
  }

  public void setStrOwnerClientNumber(String strOwnerClientNumber) {
    this.strOwnerClientNumber = strOwnerClientNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet strLifeClientNumber(String strLifeClientNumber) {
    this.strLifeClientNumber = strLifeClientNumber;
    return this;
  }

  /**
   * Get strLifeClientNumber
   * @return strLifeClientNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrLifeClientNumber() {
    return strLifeClientNumber;
  }

  public void setStrLifeClientNumber(String strLifeClientNumber) {
    this.strLifeClientNumber = strLifeClientNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPayorName(String strPayorName) {
    this.strPayorName = strPayorName;
    return this;
  }

  /**
   * Get strPayorName
   * @return strPayorName
  **/
  @ApiModelProperty(value = "")


  public String getStrPayorName() {
    return strPayorName;
  }

  public void setStrPayorName(String strPayorName) {
    this.strPayorName = strPayorName;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPayorClientNumber(String strPayorClientNumber) {
    this.strPayorClientNumber = strPayorClientNumber;
    return this;
  }

  /**
   * Get strPayorClientNumber
   * @return strPayorClientNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPayorClientNumber() {
    return strPayorClientNumber;
  }

  public void setStrPayorClientNumber(String strPayorClientNumber) {
    this.strPayorClientNumber = strPayorClientNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet dtPolicyIssuedDate(BigDecimal dtPolicyIssuedDate) {
    this.dtPolicyIssuedDate = dtPolicyIssuedDate;
    return this;
  }

  /**
   * Get dtPolicyIssuedDate
   * @return dtPolicyIssuedDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtPolicyIssuedDate() {
    return dtPolicyIssuedDate;
  }

  public void setDtPolicyIssuedDate(BigDecimal dtPolicyIssuedDate) {
    this.dtPolicyIssuedDate = dtPolicyIssuedDate;
  }

  public ClaimWorksheetUIDataClaimWorksheet dtRiskCommencementDate(BigDecimal dtRiskCommencementDate) {
    this.dtRiskCommencementDate = dtRiskCommencementDate;
    return this;
  }

  /**
   * Get dtRiskCommencementDate
   * @return dtRiskCommencementDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtRiskCommencementDate() {
    return dtRiskCommencementDate;
  }

  public void setDtRiskCommencementDate(BigDecimal dtRiskCommencementDate) {
    this.dtRiskCommencementDate = dtRiskCommencementDate;
  }

  public ClaimWorksheetUIDataClaimWorksheet dtPaidToDate(BigDecimal dtPaidToDate) {
    this.dtPaidToDate = dtPaidToDate;
    return this;
  }

  /**
   * Get dtPaidToDate
   * @return dtPaidToDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtPaidToDate() {
    return dtPaidToDate;
  }

  public void setDtPaidToDate(BigDecimal dtPaidToDate) {
    this.dtPaidToDate = dtPaidToDate;
  }

  public ClaimWorksheetUIDataClaimWorksheet dtBillToDate(BigDecimal dtBillToDate) {
    this.dtBillToDate = dtBillToDate;
    return this;
  }

  /**
   * Get dtBillToDate
   * @return dtBillToDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtBillToDate() {
    return dtBillToDate;
  }

  public void setDtBillToDate(BigDecimal dtBillToDate) {
    this.dtBillToDate = dtBillToDate;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPaymentMethod(String strPaymentMethod) {
    this.strPaymentMethod = strPaymentMethod;
    return this;
  }

  /**
   * Get strPaymentMethod
   * @return strPaymentMethod
  **/
  @ApiModelProperty(value = "")


  public String getStrPaymentMethod() {
    return strPaymentMethod;
  }

  public void setStrPaymentMethod(String strPaymentMethod) {
    this.strPaymentMethod = strPaymentMethod;
  }

  public ClaimWorksheetUIDataClaimWorksheet strPaymentFrequency(String strPaymentFrequency) {
    this.strPaymentFrequency = strPaymentFrequency;
    return this;
  }

  /**
   * Get strPaymentFrequency
   * @return strPaymentFrequency
  **/
  @ApiModelProperty(value = "")


  public String getStrPaymentFrequency() {
    return strPaymentFrequency;
  }

  public void setStrPaymentFrequency(String strPaymentFrequency) {
    this.strPaymentFrequency = strPaymentFrequency;
  }

  public ClaimWorksheetUIDataClaimWorksheet strAgentName(String strAgentName) {
    this.strAgentName = strAgentName;
    return this;
  }

  /**
   * Get strAgentName
   * @return strAgentName
  **/
  @ApiModelProperty(value = "")


  public String getStrAgentName() {
    return strAgentName;
  }

  public void setStrAgentName(String strAgentName) {
    this.strAgentName = strAgentName;
  }

  public ClaimWorksheetUIDataClaimWorksheet strAgentNumber(String strAgentNumber) {
    this.strAgentNumber = strAgentNumber;
    return this;
  }

  /**
   * Get strAgentNumber
   * @return strAgentNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrAgentNumber() {
    return strAgentNumber;
  }

  public void setStrAgentNumber(String strAgentNumber) {
    this.strAgentNumber = strAgentNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheet dblTotalPremium(BigDecimal dblTotalPremium) {
    this.dblTotalPremium = dblTotalPremium;
    return this;
  }

  /**
   * Get dblTotalPremium
   * @return dblTotalPremium
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDblTotalPremium() {
    return dblTotalPremium;
  }

  public void setDblTotalPremium(BigDecimal dblTotalPremium) {
    this.dblTotalPremium = dblTotalPremium;
  }

  public ClaimWorksheetUIDataClaimWorksheet dblBasicPremium(BigDecimal dblBasicPremium) {
    this.dblBasicPremium = dblBasicPremium;
    return this;
  }

  /**
   * Get dblBasicPremium
   * @return dblBasicPremium
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDblBasicPremium() {
    return dblBasicPremium;
  }

  public void setDblBasicPremium(BigDecimal dblBasicPremium) {
    this.dblBasicPremium = dblBasicPremium;
  }

  public ClaimWorksheetUIDataClaimWorksheet dblRegularTopup(BigDecimal dblRegularTopup) {
    this.dblRegularTopup = dblRegularTopup;
    return this;
  }

  /**
   * Get dblRegularTopup
   * @return dblRegularTopup
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDblRegularTopup() {
    return dblRegularTopup;
  }

  public void setDblRegularTopup(BigDecimal dblRegularTopup) {
    this.dblRegularTopup = dblRegularTopup;
  }

  public ClaimWorksheetUIDataClaimWorksheet dtReinstatementDate(BigDecimal dtReinstatementDate) {
    this.dtReinstatementDate = dtReinstatementDate;
    return this;
  }

  /**
   * Get dtReinstatementDate
   * @return dtReinstatementDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtReinstatementDate() {
    return dtReinstatementDate;
  }

  public void setDtReinstatementDate(BigDecimal dtReinstatementDate) {
    this.dtReinstatementDate = dtReinstatementDate;
  }

  public ClaimWorksheetUIDataClaimWorksheet componentsListData(List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> componentsListData) {
    this.componentsListData = componentsListData;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheet addComponentsListDataItem(ClaimWorksheetUIDataClaimWorksheetComponentsListData componentsListDataItem) {
    if (this.componentsListData == null) {
      this.componentsListData = new ArrayList<>();
    }
    this.componentsListData.add(componentsListDataItem);
    return this;
  }

  /**
   * Get componentsListData
   * @return componentsListData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> getComponentsListData() {
    return componentsListData;
  }

  public void setComponentsListData(List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> componentsListData) {
    this.componentsListData = componentsListData;
  }

  public ClaimWorksheetUIDataClaimWorksheet policyRelatedDetails(List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> policyRelatedDetails) {
    this.policyRelatedDetails = policyRelatedDetails;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheet addPolicyRelatedDetailsItem(ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails policyRelatedDetailsItem) {
    if (this.policyRelatedDetails == null) {
      this.policyRelatedDetails = new ArrayList<>();
    }
    this.policyRelatedDetails.add(policyRelatedDetailsItem);
    return this;
  }

  /**
   * Get policyRelatedDetails
   * @return policyRelatedDetails
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> getPolicyRelatedDetails() {
    return policyRelatedDetails;
  }

  public void setPolicyRelatedDetails(List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> policyRelatedDetails) {
    this.policyRelatedDetails = policyRelatedDetails;
  }

  public ClaimWorksheetUIDataClaimWorksheet clients(ClaimWorksheetUIDataClaimWorksheetClients clients) {
    this.clients = clients;
    return this;
  }

  /**
   * Get clients
   * @return clients
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ClaimWorksheetUIDataClaimWorksheetClients getClients() {
    return clients;
  }

  public void setClients(ClaimWorksheetUIDataClaimWorksheetClients clients) {
    this.clients = clients;
  }

  public ClaimWorksheetUIDataClaimWorksheet claimHistory(List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> claimHistory) {
    this.claimHistory = claimHistory;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheet addClaimHistoryItem(ClaimWorksheetUIDataClaimWorksheetClaimHistory claimHistoryItem) {
    if (this.claimHistory == null) {
      this.claimHistory = new ArrayList<>();
    }
    this.claimHistory.add(claimHistoryItem);
    return this;
  }

  /**
   * Get claimHistory
   * @return claimHistory
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> getClaimHistory() {
    return claimHistory;
  }

  public void setClaimHistory(List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> claimHistory) {
    this.claimHistory = claimHistory;
  }

  public ClaimWorksheetUIDataClaimWorksheet comments(ClaimWorksheetUIDataClaimWorksheetComments comments) {
    this.comments = comments;
    return this;
  }

  /**
   * Get comments
   * @return comments
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ClaimWorksheetUIDataClaimWorksheetComments getComments() {
    return comments;
  }

  public void setComments(ClaimWorksheetUIDataClaimWorksheetComments comments) {
    this.comments = comments;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheet claimWorksheetUIDataClaimWorksheet = (ClaimWorksheetUIDataClaimWorksheet) o;
    return Objects.equals(this.strApplicationNumber, claimWorksheetUIDataClaimWorksheet.strApplicationNumber) &&
        Objects.equals(this.strClaimNumber, claimWorksheetUIDataClaimWorksheet.strClaimNumber) &&
        Objects.equals(this.strPOSRequestNumber, claimWorksheetUIDataClaimWorksheet.strPOSRequestNumber) &&
        Objects.equals(this.strProcessType, claimWorksheetUIDataClaimWorksheet.strProcessType) &&
        Objects.equals(this.strPolicyNumber, claimWorksheetUIDataClaimWorksheet.strPolicyNumber) &&
        Objects.equals(this.strProductCode, claimWorksheetUIDataClaimWorksheet.strProductCode) &&
        Objects.equals(this.strProductName, claimWorksheetUIDataClaimWorksheet.strProductName) &&
        Objects.equals(this.strPolicyStatus, claimWorksheetUIDataClaimWorksheet.strPolicyStatus) &&
        Objects.equals(this.strPolicyCurrency, claimWorksheetUIDataClaimWorksheet.strPolicyCurrency) &&
        Objects.equals(this.strPremiumStatus, claimWorksheetUIDataClaimWorksheet.strPremiumStatus) &&
        Objects.equals(this.strOwnerName, claimWorksheetUIDataClaimWorksheet.strOwnerName) &&
        Objects.equals(this.strOwnerClientNumber, claimWorksheetUIDataClaimWorksheet.strOwnerClientNumber) &&
        Objects.equals(this.strLifeClientNumber, claimWorksheetUIDataClaimWorksheet.strLifeClientNumber) &&
        Objects.equals(this.strPayorName, claimWorksheetUIDataClaimWorksheet.strPayorName) &&
        Objects.equals(this.strPayorClientNumber, claimWorksheetUIDataClaimWorksheet.strPayorClientNumber) &&
        Objects.equals(this.dtPolicyIssuedDate, claimWorksheetUIDataClaimWorksheet.dtPolicyIssuedDate) &&
        Objects.equals(this.dtRiskCommencementDate, claimWorksheetUIDataClaimWorksheet.dtRiskCommencementDate) &&
        Objects.equals(this.dtPaidToDate, claimWorksheetUIDataClaimWorksheet.dtPaidToDate) &&
        Objects.equals(this.dtBillToDate, claimWorksheetUIDataClaimWorksheet.dtBillToDate) &&
        Objects.equals(this.strPaymentMethod, claimWorksheetUIDataClaimWorksheet.strPaymentMethod) &&
        Objects.equals(this.strPaymentFrequency, claimWorksheetUIDataClaimWorksheet.strPaymentFrequency) &&
        Objects.equals(this.strAgentName, claimWorksheetUIDataClaimWorksheet.strAgentName) &&
        Objects.equals(this.strAgentNumber, claimWorksheetUIDataClaimWorksheet.strAgentNumber) &&
        Objects.equals(this.dblTotalPremium, claimWorksheetUIDataClaimWorksheet.dblTotalPremium) &&
        Objects.equals(this.dblBasicPremium, claimWorksheetUIDataClaimWorksheet.dblBasicPremium) &&
        Objects.equals(this.dblRegularTopup, claimWorksheetUIDataClaimWorksheet.dblRegularTopup) &&
        Objects.equals(this.dtReinstatementDate, claimWorksheetUIDataClaimWorksheet.dtReinstatementDate) &&
        Objects.equals(this.componentsListData, claimWorksheetUIDataClaimWorksheet.componentsListData) &&
        Objects.equals(this.policyRelatedDetails, claimWorksheetUIDataClaimWorksheet.policyRelatedDetails) &&
        Objects.equals(this.clients, claimWorksheetUIDataClaimWorksheet.clients) &&
        Objects.equals(this.claimHistory, claimWorksheetUIDataClaimWorksheet.claimHistory) &&
        Objects.equals(this.comments, claimWorksheetUIDataClaimWorksheet.comments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strApplicationNumber, strClaimNumber, strPOSRequestNumber, strProcessType, strPolicyNumber, strProductCode, strProductName, strPolicyStatus, strPolicyCurrency, strPremiumStatus, strOwnerName, strOwnerClientNumber, strLifeClientNumber, strPayorName, strPayorClientNumber, dtPolicyIssuedDate, dtRiskCommencementDate, dtPaidToDate, dtBillToDate, strPaymentMethod, strPaymentFrequency, strAgentName, strAgentNumber, dblTotalPremium, dblBasicPremium, dblRegularTopup, dtReinstatementDate, componentsListData, policyRelatedDetails, clients, claimHistory, comments);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheet {\n");
    
    sb.append("    strApplicationNumber: ").append(toIndentedString(strApplicationNumber)).append("\n");
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPOSRequestNumber: ").append(toIndentedString(strPOSRequestNumber)).append("\n");
    sb.append("    strProcessType: ").append(toIndentedString(strProcessType)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strProductCode: ").append(toIndentedString(strProductCode)).append("\n");
    sb.append("    strProductName: ").append(toIndentedString(strProductName)).append("\n");
    sb.append("    strPolicyStatus: ").append(toIndentedString(strPolicyStatus)).append("\n");
    sb.append("    strPolicyCurrency: ").append(toIndentedString(strPolicyCurrency)).append("\n");
    sb.append("    strPremiumStatus: ").append(toIndentedString(strPremiumStatus)).append("\n");
    sb.append("    strOwnerName: ").append(toIndentedString(strOwnerName)).append("\n");
    sb.append("    strOwnerClientNumber: ").append(toIndentedString(strOwnerClientNumber)).append("\n");
    sb.append("    strLifeClientNumber: ").append(toIndentedString(strLifeClientNumber)).append("\n");
    sb.append("    strPayorName: ").append(toIndentedString(strPayorName)).append("\n");
    sb.append("    strPayorClientNumber: ").append(toIndentedString(strPayorClientNumber)).append("\n");
    sb.append("    dtPolicyIssuedDate: ").append(toIndentedString(dtPolicyIssuedDate)).append("\n");
    sb.append("    dtRiskCommencementDate: ").append(toIndentedString(dtRiskCommencementDate)).append("\n");
    sb.append("    dtPaidToDate: ").append(toIndentedString(dtPaidToDate)).append("\n");
    sb.append("    dtBillToDate: ").append(toIndentedString(dtBillToDate)).append("\n");
    sb.append("    strPaymentMethod: ").append(toIndentedString(strPaymentMethod)).append("\n");
    sb.append("    strPaymentFrequency: ").append(toIndentedString(strPaymentFrequency)).append("\n");
    sb.append("    strAgentName: ").append(toIndentedString(strAgentName)).append("\n");
    sb.append("    strAgentNumber: ").append(toIndentedString(strAgentNumber)).append("\n");
    sb.append("    dblTotalPremium: ").append(toIndentedString(dblTotalPremium)).append("\n");
    sb.append("    dblBasicPremium: ").append(toIndentedString(dblBasicPremium)).append("\n");
    sb.append("    dblRegularTopup: ").append(toIndentedString(dblRegularTopup)).append("\n");
    sb.append("    dtReinstatementDate: ").append(toIndentedString(dtReinstatementDate)).append("\n");
    sb.append("    componentsListData: ").append(toIndentedString(componentsListData)).append("\n");
    sb.append("    policyRelatedDetails: ").append(toIndentedString(policyRelatedDetails)).append("\n");
    sb.append("    clients: ").append(toIndentedString(clients)).append("\n");
    sb.append("    claimHistory: ").append(toIndentedString(claimHistory)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

