package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSgetComponentCodeStatusDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSgetComponentCodeStatusDataIn   {
  @JsonProperty("polNo")
  private String polNo = null;

  @JsonProperty("clientNo")
  private String clientNo = null;

  @JsonProperty("comp_code")
  private String compCode = null;

  public WSgetComponentCodeStatusDataIn polNo(String polNo) {
    this.polNo = polNo;
    return this;
  }

  /**
   * Get polNo
   * @return polNo
  **/
  @ApiModelProperty(example = "polNo", value = "")


  public String getPolNo() {
    return polNo;
  }

  public void setPolNo(String polNo) {
    this.polNo = polNo;
  }

  public WSgetComponentCodeStatusDataIn clientNo(String clientNo) {
    this.clientNo = clientNo;
    return this;
  }

  /**
   * Get clientNo
   * @return clientNo
  **/
  @ApiModelProperty(example = "clientNo", value = "")


  public String getClientNo() {
    return clientNo;
  }

  public void setClientNo(String clientNo) {
    this.clientNo = clientNo;
  }

  public WSgetComponentCodeStatusDataIn compCode(String compCode) {
    this.compCode = compCode;
    return this;
  }

  /**
   * Get compCode
   * @return compCode
  **/
  @ApiModelProperty(example = "comp_code", value = "")


  public String getCompCode() {
    return compCode;
  }

  public void setCompCode(String compCode) {
    this.compCode = compCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSgetComponentCodeStatusDataIn wsgetComponentCodeStatusDataIn = (WSgetComponentCodeStatusDataIn) o;
    return Objects.equals(this.polNo, wsgetComponentCodeStatusDataIn.polNo) &&
        Objects.equals(this.clientNo, wsgetComponentCodeStatusDataIn.clientNo) &&
        Objects.equals(this.compCode, wsgetComponentCodeStatusDataIn.compCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(polNo, clientNo, compCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSgetComponentCodeStatusDataIn {\n");
    
    sb.append("    polNo: ").append(toIndentedString(polNo)).append("\n");
    sb.append("    clientNo: ").append(toIndentedString(clientNo)).append("\n");
    sb.append("    compCode: ").append(toIndentedString(compCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

