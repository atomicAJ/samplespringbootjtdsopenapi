package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSPolicyDataRequestorDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSPolicyDataRequestorDataIn   {
  @JsonProperty("POLICYNUMBER")
  private String POLICYNUMBER = null;

  @JsonProperty("POSRequestNumber")
  private String poSRequestNumber = null;

  @JsonProperty("ApplicationNumber")
  private String applicationNumber = null;

  public JsonWSPolicyDataRequestorDataIn POLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
    return this;
  }

  /**
   * POLICY NUMBER
   * @return POLICYNUMBER
  **/
  @ApiModelProperty(example = "100395", required = true, value = "POLICY NUMBER")
  @NotNull


  public String getPOLICYNUMBER() {
    return POLICYNUMBER;
  }

  public void setPOLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
  }

  public JsonWSPolicyDataRequestorDataIn poSRequestNumber(String poSRequestNumber) {
    this.poSRequestNumber = poSRequestNumber;
    return this;
  }

  /**
   * Get poSRequestNumber
   * @return poSRequestNumber
  **/
  @ApiModelProperty(example = "POSRequestNumber", required = true, value = "")
  @NotNull


  public String getPoSRequestNumber() {
    return poSRequestNumber;
  }

  public void setPoSRequestNumber(String poSRequestNumber) {
    this.poSRequestNumber = poSRequestNumber;
  }

  public JsonWSPolicyDataRequestorDataIn applicationNumber(String applicationNumber) {
    this.applicationNumber = applicationNumber;
    return this;
  }

  /**
   * Get applicationNumber
   * @return applicationNumber
  **/
  @ApiModelProperty(example = "ApplicationNumber", value = "")


  public String getApplicationNumber() {
    return applicationNumber;
  }

  public void setApplicationNumber(String applicationNumber) {
    this.applicationNumber = applicationNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSPolicyDataRequestorDataIn jsonWSPolicyDataRequestorDataIn = (JsonWSPolicyDataRequestorDataIn) o;
    return Objects.equals(this.POLICYNUMBER, jsonWSPolicyDataRequestorDataIn.POLICYNUMBER) &&
        Objects.equals(this.poSRequestNumber, jsonWSPolicyDataRequestorDataIn.poSRequestNumber) &&
        Objects.equals(this.applicationNumber, jsonWSPolicyDataRequestorDataIn.applicationNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(POLICYNUMBER, poSRequestNumber, applicationNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSPolicyDataRequestorDataIn {\n");
    
    sb.append("    POLICYNUMBER: ").append(toIndentedString(POLICYNUMBER)).append("\n");
    sb.append("    poSRequestNumber: ").append(toIndentedString(poSRequestNumber)).append("\n");
    sb.append("    applicationNumber: ").append(toIndentedString(applicationNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

