package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSFetchRecordEntriesDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSFetchRecordEntriesDataInWSInput   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strComparisonOperator")
  private String strComparisonOperator = null;

  public WSFetchRecordEntriesDataInWSInput strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public WSFetchRecordEntriesDataInWSInput strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(example = "654321", value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public WSFetchRecordEntriesDataInWSInput strComparisonOperator(String strComparisonOperator) {
    this.strComparisonOperator = strComparisonOperator;
    return this;
  }

  /**
   * Get strComparisonOperator
   * @return strComparisonOperator
  **/
  @ApiModelProperty(example = "AND", value = "")


  public String getStrComparisonOperator() {
    return strComparisonOperator;
  }

  public void setStrComparisonOperator(String strComparisonOperator) {
    this.strComparisonOperator = strComparisonOperator;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSFetchRecordEntriesDataInWSInput wsFetchRecordEntriesDataInWSInput = (WSFetchRecordEntriesDataInWSInput) o;
    return Objects.equals(this.strClaimNumber, wsFetchRecordEntriesDataInWSInput.strClaimNumber) &&
        Objects.equals(this.strPolicyNumber, wsFetchRecordEntriesDataInWSInput.strPolicyNumber) &&
        Objects.equals(this.strComparisonOperator, wsFetchRecordEntriesDataInWSInput.strComparisonOperator);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strPolicyNumber, strComparisonOperator);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSFetchRecordEntriesDataInWSInput {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strComparisonOperator: ").append(toIndentedString(strComparisonOperator)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

