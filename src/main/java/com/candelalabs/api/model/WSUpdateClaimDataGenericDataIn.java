package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateClaimDataGenericDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateClaimDataGenericDataIn   {
  @JsonProperty("claimNumber")
  private String claimNumber = null;

  @JsonProperty("strField")
  private String strField = null;

  @JsonProperty("strFieldValueType")
  private String strFieldValueType = null;

  @JsonProperty("strFieldValue")
  private Object strFieldValue = null;

  public WSUpdateClaimDataGenericDataIn claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public WSUpdateClaimDataGenericDataIn strField(String strField) {
    this.strField = strField;
    return this;
  }

  /**
   * Get strField
   * @return strField
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrField() {
    return strField;
  }

  public void setStrField(String strField) {
    this.strField = strField;
  }

  public WSUpdateClaimDataGenericDataIn strFieldValueType(String strFieldValueType) {
    this.strFieldValueType = strFieldValueType;
    return this;
  }

  /**
   * Get strFieldValueType
   * @return strFieldValueType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrFieldValueType() {
    return strFieldValueType;
  }

  public void setStrFieldValueType(String strFieldValueType) {
    this.strFieldValueType = strFieldValueType;
  }

  public WSUpdateClaimDataGenericDataIn strFieldValue(Object strFieldValue) {
    this.strFieldValue = strFieldValue;
    return this;
  }

  /**
   * Get strFieldValue
   * @return strFieldValue
  **/
  @ApiModelProperty(value = "")


  public Object getStrFieldValue() {
    return strFieldValue;
  }

  public void setStrFieldValue(Object strFieldValue) {
    this.strFieldValue = strFieldValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateClaimDataGenericDataIn wsUpdateClaimDataGenericDataIn = (WSUpdateClaimDataGenericDataIn) o;
    return Objects.equals(this.claimNumber, wsUpdateClaimDataGenericDataIn.claimNumber) &&
        Objects.equals(this.strField, wsUpdateClaimDataGenericDataIn.strField) &&
        Objects.equals(this.strFieldValueType, wsUpdateClaimDataGenericDataIn.strFieldValueType) &&
        Objects.equals(this.strFieldValue, wsUpdateClaimDataGenericDataIn.strFieldValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNumber, strField, strFieldValueType, strFieldValue);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateClaimDataGenericDataIn {\n");
    
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    strField: ").append(toIndentedString(strField)).append("\n");
    sb.append("    strFieldValueType: ").append(toIndentedString(strFieldValueType)).append("\n");
    sb.append("    strFieldValue: ").append(toIndentedString(strFieldValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

