package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetComments
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetComments   {
  @JsonProperty("NBUWComments")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> nbUWComments = null;

  @JsonProperty("PSUWComments")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> psUWComments = null;

  @JsonProperty("CLUWComments")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> clUWComments = null;

  public ClaimWorksheetUIDataClaimWorksheetComments nbUWComments(List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> nbUWComments) {
    this.nbUWComments = nbUWComments;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetComments addNbUWCommentsItem(ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments nbUWCommentsItem) {
    if (this.nbUWComments == null) {
      this.nbUWComments = new ArrayList<>();
    }
    this.nbUWComments.add(nbUWCommentsItem);
    return this;
  }

  /**
   * Get nbUWComments
   * @return nbUWComments
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> getNbUWComments() {
    return nbUWComments;
  }

  public void setNbUWComments(List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> nbUWComments) {
    this.nbUWComments = nbUWComments;
  }

  public ClaimWorksheetUIDataClaimWorksheetComments psUWComments(List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> psUWComments) {
    this.psUWComments = psUWComments;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetComments addPsUWCommentsItem(ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments psUWCommentsItem) {
    if (this.psUWComments == null) {
      this.psUWComments = new ArrayList<>();
    }
    this.psUWComments.add(psUWCommentsItem);
    return this;
  }

  /**
   * Get psUWComments
   * @return psUWComments
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> getPsUWComments() {
    return psUWComments;
  }

  public void setPsUWComments(List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> psUWComments) {
    this.psUWComments = psUWComments;
  }

  public ClaimWorksheetUIDataClaimWorksheetComments clUWComments(List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> clUWComments) {
    this.clUWComments = clUWComments;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetComments addClUWCommentsItem(ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments clUWCommentsItem) {
    if (this.clUWComments == null) {
      this.clUWComments = new ArrayList<>();
    }
    this.clUWComments.add(clUWCommentsItem);
    return this;
  }

  /**
   * Get clUWComments
   * @return clUWComments
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> getClUWComments() {
    return clUWComments;
  }

  public void setClUWComments(List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> clUWComments) {
    this.clUWComments = clUWComments;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetComments claimWorksheetUIDataClaimWorksheetComments = (ClaimWorksheetUIDataClaimWorksheetComments) o;
    return Objects.equals(this.nbUWComments, claimWorksheetUIDataClaimWorksheetComments.nbUWComments) &&
        Objects.equals(this.psUWComments, claimWorksheetUIDataClaimWorksheetComments.psUWComments) &&
        Objects.equals(this.clUWComments, claimWorksheetUIDataClaimWorksheetComments.clUWComments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nbUWComments, psUWComments, clUWComments);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetComments {\n");
    
    sb.append("    nbUWComments: ").append(toIndentedString(nbUWComments)).append("\n");
    sb.append("    psUWComments: ").append(toIndentedString(psUWComments)).append("\n");
    sb.append("    clUWComments: ").append(toIndentedString(clUWComments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

