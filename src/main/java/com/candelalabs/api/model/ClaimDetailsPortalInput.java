package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDetailsPortalInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDetailsPortalInput   {
  @JsonProperty("strOwnerClientID")
  private String strOwnerClientID = null;

  public ClaimDetailsPortalInput strOwnerClientID(String strOwnerClientID) {
    this.strOwnerClientID = strOwnerClientID;
    return this;
  }

  /**
   * Get strOwnerClientID
   * @return strOwnerClientID
  **/
  @ApiModelProperty(value = "")


  public String getStrOwnerClientID() {
    return strOwnerClientID;
  }

  public void setStrOwnerClientID(String strOwnerClientID) {
    this.strOwnerClientID = strOwnerClientID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDetailsPortalInput claimDetailsPortalInput = (ClaimDetailsPortalInput) o;
    return Objects.equals(this.strOwnerClientID, claimDetailsPortalInput.strOwnerClientID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strOwnerClientID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDetailsPortalInput {\n");
    
    sb.append("    strOwnerClientID: ").append(toIndentedString(strOwnerClientID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

