package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSPolicyDataRequestorDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSPolicyDataRequestorDataOut   {
  @JsonProperty("processmsg")
  private String processmsg = null;

  @JsonProperty("decision")
  private String decision = null;

  @JsonProperty("productcode")
  private String productcode = null;

  @JsonProperty("COMPONENTCODE")
  private String COMPONENTCODE = null;

  @JsonProperty("LENGTHOFSTAY")
  private String LENGTHOFSTAY = null;

  @JsonProperty("SUMASSURED")
  private String SUMASSURED = null;

  @JsonProperty("PLAN")
  private String PLAN = null;

  @JsonProperty("CLAIMBILLAMOUNT")
  private String CLAIMBILLAMOUNT = null;

  @JsonProperty("FLAGABUSE")
  private String FLAGABUSE = null;

  @JsonProperty("PENDINGCLAIMS")
  private String PENDINGCLAIMS = null;

  @JsonProperty("FATCAFLAG")
  private String FATCAFLAG = null;

  @JsonProperty("CLAIMINVOICEDATE")
  private String CLAIMINVOICEDATE = null;

  @JsonProperty("PAIDTODATE")
  private String PAIDTODATE = null;

  @JsonProperty("POLICYSTATUS")
  private String POLICYSTATUS = null;

  @JsonProperty("PREMIUMPOLICYSTATUS")
  private String PREMIUMPOLICYSTATUS = null;

  @JsonProperty("COMPONENTSTATUS")
  private String COMPONENTSTATUS = null;

  @JsonProperty("PREMIUMCOMPONENTSTATUS")
  private String PREMIUMCOMPONENTSTATUS = null;

  @JsonProperty("BALANCELIMIT")
  private String BALANCELIMIT = null;

  @JsonProperty("POLICYCURRENCY")
  private String POLICYCURRENCY = null;

  @JsonProperty("CASECURRENCY")
  private String CASECURRENCY = null;

  @JsonProperty("POLICYAGE")
  private String POLICYAGE = null;

  @JsonProperty("DATEOFDEATH")
  private String DATEOFDEATH = null;

  @JsonProperty("SAUNIT")
  private String SAUNIT = null;

  @JsonProperty("CASESURGICALBENEFIT")
  private String CASESURGICALBENEFIT = null;

  @JsonProperty("CLIENTSTATUS")
  private String CLIENTSTATUS = null;

  @JsonProperty("ELAPSEDATE")
  private String ELAPSEDATE = null;

  @JsonProperty("SURRENDERDATE")
  private String SURRENDERDATE = null;

  @JsonProperty("30DINVESNOTIFICATION")
  private String _30DINVESNOTIFICATION = null;

  @JsonProperty("45DINVESNOTIFICATION")
  private String _45DINVESNOTIFICATION = null;

  @JsonProperty("60DINVESNOTIFICATION")
  private String _60DINVESNOTIFICATION = null;

  @JsonProperty("CLAIMTYPEUI")
  private String CLAIMTYPEUI = null;

  @JsonProperty("CLAIMTYPELA")
  private String CLAIMTYPELA = null;

  @JsonProperty("POSINITIATED")
  private String POSINITIATED = null;

  @JsonProperty("INPROGRESSCLAIMNO")
  private String INPROGRESSCLAIMNO = null;

  @JsonProperty("INVES2CUSTNOTIFY")
  private String inVES2CUSTNOTIFY = null;

  @JsonProperty("INVES1CLUSERNOTIFY")
  private String inVES1CLUSERNOTIFY = null;

  @JsonProperty("INVES2CLUSERNOTIFY")
  private String inVES2CLUSERNOTIFY = null;

  @JsonProperty("DOCUMENTWAITPERIOD")
  private String DOCUMENTWAITPERIOD = null;

  @JsonProperty("TPACLAIMNUMBER")
  private String TPACLAIMNUMBER = null;

  @JsonProperty("POSRequestNumber")
  private String poSRequestNumber = null;

  @JsonProperty("POLICYNUMBER")
  private String POLICYNUMBER = null;

  @JsonProperty("INSUREDCLIENTID")
  private String INSUREDCLIENTID = null;

  @JsonProperty("OWNERCLIENTID")
  private String OWNERCLIENTID = null;

  @JsonProperty("i15DINVESNOTIFICATION")
  private String i15DINVESNOTIFICATION = null;

  @JsonProperty("i30DINVESNOTIFICATION")
  private String i30DINVESNOTIFICATION = null;

  @JsonProperty("WsProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WsExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WsSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("batchNumber")
  private String batchNumber = null;

  @JsonProperty("portalClaimRequestNumber")
  private String portalClaimRequestNumber = null;

  public JsonWSPolicyDataRequestorDataOut processmsg(String processmsg) {
    this.processmsg = processmsg;
    return this;
  }

  /**
   * Get processmsg
   * @return processmsg
  **/
  @ApiModelProperty(example = "success", required = true, value = "")
  @NotNull


  public String getProcessmsg() {
    return processmsg;
  }

  public void setProcessmsg(String processmsg) {
    this.processmsg = processmsg;
  }

  public JsonWSPolicyDataRequestorDataOut decision(String decision) {
    this.decision = decision;
    return this;
  }

  /**
   * Get decision
   * @return decision
  **/
  @ApiModelProperty(example = "Success", required = true, value = "")
  @NotNull


  public String getDecision() {
    return decision;
  }

  public void setDecision(String decision) {
    this.decision = decision;
  }

  public JsonWSPolicyDataRequestorDataOut productcode(String productcode) {
    this.productcode = productcode;
    return this;
  }

  /**
   * Get productcode
   * @return productcode
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getProductcode() {
    return productcode;
  }

  public void setProductcode(String productcode) {
    this.productcode = productcode;
  }

  public JsonWSPolicyDataRequestorDataOut COMPONENTCODE(String COMPONENTCODE) {
    this.COMPONENTCODE = COMPONENTCODE;
    return this;
  }

  /**
   * Get COMPONENTCODE
   * @return COMPONENTCODE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCOMPONENTCODE() {
    return COMPONENTCODE;
  }

  public void setCOMPONENTCODE(String COMPONENTCODE) {
    this.COMPONENTCODE = COMPONENTCODE;
  }

  public JsonWSPolicyDataRequestorDataOut LENGTHOFSTAY(String LENGTHOFSTAY) {
    this.LENGTHOFSTAY = LENGTHOFSTAY;
    return this;
  }

  /**
   * Get LENGTHOFSTAY
   * @return LENGTHOFSTAY
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getLENGTHOFSTAY() {
    return LENGTHOFSTAY;
  }

  public void setLENGTHOFSTAY(String LENGTHOFSTAY) {
    this.LENGTHOFSTAY = LENGTHOFSTAY;
  }

  public JsonWSPolicyDataRequestorDataOut SUMASSURED(String SUMASSURED) {
    this.SUMASSURED = SUMASSURED;
    return this;
  }

  /**
   * Get SUMASSURED
   * @return SUMASSURED
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getSUMASSURED() {
    return SUMASSURED;
  }

  public void setSUMASSURED(String SUMASSURED) {
    this.SUMASSURED = SUMASSURED;
  }

  public JsonWSPolicyDataRequestorDataOut PLAN(String PLAN) {
    this.PLAN = PLAN;
    return this;
  }

  /**
   * Get PLAN
   * @return PLAN
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPLAN() {
    return PLAN;
  }

  public void setPLAN(String PLAN) {
    this.PLAN = PLAN;
  }

  public JsonWSPolicyDataRequestorDataOut CLAIMBILLAMOUNT(String CLAIMBILLAMOUNT) {
    this.CLAIMBILLAMOUNT = CLAIMBILLAMOUNT;
    return this;
  }

  /**
   * Get CLAIMBILLAMOUNT
   * @return CLAIMBILLAMOUNT
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCLAIMBILLAMOUNT() {
    return CLAIMBILLAMOUNT;
  }

  public void setCLAIMBILLAMOUNT(String CLAIMBILLAMOUNT) {
    this.CLAIMBILLAMOUNT = CLAIMBILLAMOUNT;
  }

  public JsonWSPolicyDataRequestorDataOut FLAGABUSE(String FLAGABUSE) {
    this.FLAGABUSE = FLAGABUSE;
    return this;
  }

  /**
   * Get FLAGABUSE
   * @return FLAGABUSE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getFLAGABUSE() {
    return FLAGABUSE;
  }

  public void setFLAGABUSE(String FLAGABUSE) {
    this.FLAGABUSE = FLAGABUSE;
  }

  public JsonWSPolicyDataRequestorDataOut PENDINGCLAIMS(String PENDINGCLAIMS) {
    this.PENDINGCLAIMS = PENDINGCLAIMS;
    return this;
  }

  /**
   * Get PENDINGCLAIMS
   * @return PENDINGCLAIMS
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPENDINGCLAIMS() {
    return PENDINGCLAIMS;
  }

  public void setPENDINGCLAIMS(String PENDINGCLAIMS) {
    this.PENDINGCLAIMS = PENDINGCLAIMS;
  }

  public JsonWSPolicyDataRequestorDataOut FATCAFLAG(String FATCAFLAG) {
    this.FATCAFLAG = FATCAFLAG;
    return this;
  }

  /**
   * Get FATCAFLAG
   * @return FATCAFLAG
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getFATCAFLAG() {
    return FATCAFLAG;
  }

  public void setFATCAFLAG(String FATCAFLAG) {
    this.FATCAFLAG = FATCAFLAG;
  }

  public JsonWSPolicyDataRequestorDataOut CLAIMINVOICEDATE(String CLAIMINVOICEDATE) {
    this.CLAIMINVOICEDATE = CLAIMINVOICEDATE;
    return this;
  }

  /**
   * Get CLAIMINVOICEDATE
   * @return CLAIMINVOICEDATE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCLAIMINVOICEDATE() {
    return CLAIMINVOICEDATE;
  }

  public void setCLAIMINVOICEDATE(String CLAIMINVOICEDATE) {
    this.CLAIMINVOICEDATE = CLAIMINVOICEDATE;
  }

  public JsonWSPolicyDataRequestorDataOut PAIDTODATE(String PAIDTODATE) {
    this.PAIDTODATE = PAIDTODATE;
    return this;
  }

  /**
   * Get PAIDTODATE
   * @return PAIDTODATE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPAIDTODATE() {
    return PAIDTODATE;
  }

  public void setPAIDTODATE(String PAIDTODATE) {
    this.PAIDTODATE = PAIDTODATE;
  }

  public JsonWSPolicyDataRequestorDataOut POLICYSTATUS(String POLICYSTATUS) {
    this.POLICYSTATUS = POLICYSTATUS;
    return this;
  }

  /**
   * Get POLICYSTATUS
   * @return POLICYSTATUS
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPOLICYSTATUS() {
    return POLICYSTATUS;
  }

  public void setPOLICYSTATUS(String POLICYSTATUS) {
    this.POLICYSTATUS = POLICYSTATUS;
  }

  public JsonWSPolicyDataRequestorDataOut PREMIUMPOLICYSTATUS(String PREMIUMPOLICYSTATUS) {
    this.PREMIUMPOLICYSTATUS = PREMIUMPOLICYSTATUS;
    return this;
  }

  /**
   * Get PREMIUMPOLICYSTATUS
   * @return PREMIUMPOLICYSTATUS
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPREMIUMPOLICYSTATUS() {
    return PREMIUMPOLICYSTATUS;
  }

  public void setPREMIUMPOLICYSTATUS(String PREMIUMPOLICYSTATUS) {
    this.PREMIUMPOLICYSTATUS = PREMIUMPOLICYSTATUS;
  }

  public JsonWSPolicyDataRequestorDataOut COMPONENTSTATUS(String COMPONENTSTATUS) {
    this.COMPONENTSTATUS = COMPONENTSTATUS;
    return this;
  }

  /**
   * Get COMPONENTSTATUS
   * @return COMPONENTSTATUS
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCOMPONENTSTATUS() {
    return COMPONENTSTATUS;
  }

  public void setCOMPONENTSTATUS(String COMPONENTSTATUS) {
    this.COMPONENTSTATUS = COMPONENTSTATUS;
  }

  public JsonWSPolicyDataRequestorDataOut PREMIUMCOMPONENTSTATUS(String PREMIUMCOMPONENTSTATUS) {
    this.PREMIUMCOMPONENTSTATUS = PREMIUMCOMPONENTSTATUS;
    return this;
  }

  /**
   * Get PREMIUMCOMPONENTSTATUS
   * @return PREMIUMCOMPONENTSTATUS
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPREMIUMCOMPONENTSTATUS() {
    return PREMIUMCOMPONENTSTATUS;
  }

  public void setPREMIUMCOMPONENTSTATUS(String PREMIUMCOMPONENTSTATUS) {
    this.PREMIUMCOMPONENTSTATUS = PREMIUMCOMPONENTSTATUS;
  }

  public JsonWSPolicyDataRequestorDataOut BALANCELIMIT(String BALANCELIMIT) {
    this.BALANCELIMIT = BALANCELIMIT;
    return this;
  }

  /**
   * Get BALANCELIMIT
   * @return BALANCELIMIT
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getBALANCELIMIT() {
    return BALANCELIMIT;
  }

  public void setBALANCELIMIT(String BALANCELIMIT) {
    this.BALANCELIMIT = BALANCELIMIT;
  }

  public JsonWSPolicyDataRequestorDataOut POLICYCURRENCY(String POLICYCURRENCY) {
    this.POLICYCURRENCY = POLICYCURRENCY;
    return this;
  }

  /**
   * Get POLICYCURRENCY
   * @return POLICYCURRENCY
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPOLICYCURRENCY() {
    return POLICYCURRENCY;
  }

  public void setPOLICYCURRENCY(String POLICYCURRENCY) {
    this.POLICYCURRENCY = POLICYCURRENCY;
  }

  public JsonWSPolicyDataRequestorDataOut CASECURRENCY(String CASECURRENCY) {
    this.CASECURRENCY = CASECURRENCY;
    return this;
  }

  /**
   * Get CASECURRENCY
   * @return CASECURRENCY
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCASECURRENCY() {
    return CASECURRENCY;
  }

  public void setCASECURRENCY(String CASECURRENCY) {
    this.CASECURRENCY = CASECURRENCY;
  }

  public JsonWSPolicyDataRequestorDataOut POLICYAGE(String POLICYAGE) {
    this.POLICYAGE = POLICYAGE;
    return this;
  }

  /**
   * Get POLICYAGE
   * @return POLICYAGE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPOLICYAGE() {
    return POLICYAGE;
  }

  public void setPOLICYAGE(String POLICYAGE) {
    this.POLICYAGE = POLICYAGE;
  }

  public JsonWSPolicyDataRequestorDataOut DATEOFDEATH(String DATEOFDEATH) {
    this.DATEOFDEATH = DATEOFDEATH;
    return this;
  }

  /**
   * Get DATEOFDEATH
   * @return DATEOFDEATH
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getDATEOFDEATH() {
    return DATEOFDEATH;
  }

  public void setDATEOFDEATH(String DATEOFDEATH) {
    this.DATEOFDEATH = DATEOFDEATH;
  }

  public JsonWSPolicyDataRequestorDataOut SAUNIT(String SAUNIT) {
    this.SAUNIT = SAUNIT;
    return this;
  }

  /**
   * Get SAUNIT
   * @return SAUNIT
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getSAUNIT() {
    return SAUNIT;
  }

  public void setSAUNIT(String SAUNIT) {
    this.SAUNIT = SAUNIT;
  }

  public JsonWSPolicyDataRequestorDataOut CASESURGICALBENEFIT(String CASESURGICALBENEFIT) {
    this.CASESURGICALBENEFIT = CASESURGICALBENEFIT;
    return this;
  }

  /**
   * Get CASESURGICALBENEFIT
   * @return CASESURGICALBENEFIT
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCASESURGICALBENEFIT() {
    return CASESURGICALBENEFIT;
  }

  public void setCASESURGICALBENEFIT(String CASESURGICALBENEFIT) {
    this.CASESURGICALBENEFIT = CASESURGICALBENEFIT;
  }

  public JsonWSPolicyDataRequestorDataOut CLIENTSTATUS(String CLIENTSTATUS) {
    this.CLIENTSTATUS = CLIENTSTATUS;
    return this;
  }

  /**
   * Get CLIENTSTATUS
   * @return CLIENTSTATUS
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCLIENTSTATUS() {
    return CLIENTSTATUS;
  }

  public void setCLIENTSTATUS(String CLIENTSTATUS) {
    this.CLIENTSTATUS = CLIENTSTATUS;
  }

  public JsonWSPolicyDataRequestorDataOut ELAPSEDATE(String ELAPSEDATE) {
    this.ELAPSEDATE = ELAPSEDATE;
    return this;
  }

  /**
   * Get ELAPSEDATE
   * @return ELAPSEDATE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getELAPSEDATE() {
    return ELAPSEDATE;
  }

  public void setELAPSEDATE(String ELAPSEDATE) {
    this.ELAPSEDATE = ELAPSEDATE;
  }

  public JsonWSPolicyDataRequestorDataOut SURRENDERDATE(String SURRENDERDATE) {
    this.SURRENDERDATE = SURRENDERDATE;
    return this;
  }

  /**
   * Get SURRENDERDATE
   * @return SURRENDERDATE
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getSURRENDERDATE() {
    return SURRENDERDATE;
  }

  public void setSURRENDERDATE(String SURRENDERDATE) {
    this.SURRENDERDATE = SURRENDERDATE;
  }

  public JsonWSPolicyDataRequestorDataOut _30DINVESNOTIFICATION(String _30DINVESNOTIFICATION) {
    this._30DINVESNOTIFICATION = _30DINVESNOTIFICATION;
    return this;
  }

  /**
   * Get _30DINVESNOTIFICATION
   * @return _30DINVESNOTIFICATION
  **/
  @ApiModelProperty(example = "success", value = "")


  public String get30DINVESNOTIFICATION() {
    return _30DINVESNOTIFICATION;
  }

  public void set30DINVESNOTIFICATION(String _30DINVESNOTIFICATION) {
    this._30DINVESNOTIFICATION = _30DINVESNOTIFICATION;
  }

  public JsonWSPolicyDataRequestorDataOut _45DINVESNOTIFICATION(String _45DINVESNOTIFICATION) {
    this._45DINVESNOTIFICATION = _45DINVESNOTIFICATION;
    return this;
  }

  /**
   * Get _45DINVESNOTIFICATION
   * @return _45DINVESNOTIFICATION
  **/
  @ApiModelProperty(example = "success", value = "")


  public String get45DINVESNOTIFICATION() {
    return _45DINVESNOTIFICATION;
  }

  public void set45DINVESNOTIFICATION(String _45DINVESNOTIFICATION) {
    this._45DINVESNOTIFICATION = _45DINVESNOTIFICATION;
  }

  public JsonWSPolicyDataRequestorDataOut _60DINVESNOTIFICATION(String _60DINVESNOTIFICATION) {
    this._60DINVESNOTIFICATION = _60DINVESNOTIFICATION;
    return this;
  }

  /**
   * Get _60DINVESNOTIFICATION
   * @return _60DINVESNOTIFICATION
  **/
  @ApiModelProperty(example = "success", value = "")


  public String get60DINVESNOTIFICATION() {
    return _60DINVESNOTIFICATION;
  }

  public void set60DINVESNOTIFICATION(String _60DINVESNOTIFICATION) {
    this._60DINVESNOTIFICATION = _60DINVESNOTIFICATION;
  }

  public JsonWSPolicyDataRequestorDataOut CLAIMTYPEUI(String CLAIMTYPEUI) {
    this.CLAIMTYPEUI = CLAIMTYPEUI;
    return this;
  }

  /**
   * Get CLAIMTYPEUI
   * @return CLAIMTYPEUI
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCLAIMTYPEUI() {
    return CLAIMTYPEUI;
  }

  public void setCLAIMTYPEUI(String CLAIMTYPEUI) {
    this.CLAIMTYPEUI = CLAIMTYPEUI;
  }

  public JsonWSPolicyDataRequestorDataOut CLAIMTYPELA(String CLAIMTYPELA) {
    this.CLAIMTYPELA = CLAIMTYPELA;
    return this;
  }

  /**
   * Get CLAIMTYPELA
   * @return CLAIMTYPELA
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getCLAIMTYPELA() {
    return CLAIMTYPELA;
  }

  public void setCLAIMTYPELA(String CLAIMTYPELA) {
    this.CLAIMTYPELA = CLAIMTYPELA;
  }

  public JsonWSPolicyDataRequestorDataOut POSINITIATED(String POSINITIATED) {
    this.POSINITIATED = POSINITIATED;
    return this;
  }

  /**
   * Get POSINITIATED
   * @return POSINITIATED
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPOSINITIATED() {
    return POSINITIATED;
  }

  public void setPOSINITIATED(String POSINITIATED) {
    this.POSINITIATED = POSINITIATED;
  }

  public JsonWSPolicyDataRequestorDataOut INPROGRESSCLAIMNO(String INPROGRESSCLAIMNO) {
    this.INPROGRESSCLAIMNO = INPROGRESSCLAIMNO;
    return this;
  }

  /**
   * Get INPROGRESSCLAIMNO
   * @return INPROGRESSCLAIMNO
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getINPROGRESSCLAIMNO() {
    return INPROGRESSCLAIMNO;
  }

  public void setINPROGRESSCLAIMNO(String INPROGRESSCLAIMNO) {
    this.INPROGRESSCLAIMNO = INPROGRESSCLAIMNO;
  }

  public JsonWSPolicyDataRequestorDataOut inVES2CUSTNOTIFY(String inVES2CUSTNOTIFY) {
    this.inVES2CUSTNOTIFY = inVES2CUSTNOTIFY;
    return this;
  }

  /**
   * Get inVES2CUSTNOTIFY
   * @return inVES2CUSTNOTIFY
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getInVES2CUSTNOTIFY() {
    return inVES2CUSTNOTIFY;
  }

  public void setInVES2CUSTNOTIFY(String inVES2CUSTNOTIFY) {
    this.inVES2CUSTNOTIFY = inVES2CUSTNOTIFY;
  }

  public JsonWSPolicyDataRequestorDataOut inVES1CLUSERNOTIFY(String inVES1CLUSERNOTIFY) {
    this.inVES1CLUSERNOTIFY = inVES1CLUSERNOTIFY;
    return this;
  }

  /**
   * Get inVES1CLUSERNOTIFY
   * @return inVES1CLUSERNOTIFY
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getInVES1CLUSERNOTIFY() {
    return inVES1CLUSERNOTIFY;
  }

  public void setInVES1CLUSERNOTIFY(String inVES1CLUSERNOTIFY) {
    this.inVES1CLUSERNOTIFY = inVES1CLUSERNOTIFY;
  }

  public JsonWSPolicyDataRequestorDataOut inVES2CLUSERNOTIFY(String inVES2CLUSERNOTIFY) {
    this.inVES2CLUSERNOTIFY = inVES2CLUSERNOTIFY;
    return this;
  }

  /**
   * Get inVES2CLUSERNOTIFY
   * @return inVES2CLUSERNOTIFY
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getInVES2CLUSERNOTIFY() {
    return inVES2CLUSERNOTIFY;
  }

  public void setInVES2CLUSERNOTIFY(String inVES2CLUSERNOTIFY) {
    this.inVES2CLUSERNOTIFY = inVES2CLUSERNOTIFY;
  }

  public JsonWSPolicyDataRequestorDataOut DOCUMENTWAITPERIOD(String DOCUMENTWAITPERIOD) {
    this.DOCUMENTWAITPERIOD = DOCUMENTWAITPERIOD;
    return this;
  }

  /**
   * Get DOCUMENTWAITPERIOD
   * @return DOCUMENTWAITPERIOD
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getDOCUMENTWAITPERIOD() {
    return DOCUMENTWAITPERIOD;
  }

  public void setDOCUMENTWAITPERIOD(String DOCUMENTWAITPERIOD) {
    this.DOCUMENTWAITPERIOD = DOCUMENTWAITPERIOD;
  }

  public JsonWSPolicyDataRequestorDataOut TPACLAIMNUMBER(String TPACLAIMNUMBER) {
    this.TPACLAIMNUMBER = TPACLAIMNUMBER;
    return this;
  }

  /**
   * Get TPACLAIMNUMBER
   * @return TPACLAIMNUMBER
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getTPACLAIMNUMBER() {
    return TPACLAIMNUMBER;
  }

  public void setTPACLAIMNUMBER(String TPACLAIMNUMBER) {
    this.TPACLAIMNUMBER = TPACLAIMNUMBER;
  }

  public JsonWSPolicyDataRequestorDataOut poSRequestNumber(String poSRequestNumber) {
    this.poSRequestNumber = poSRequestNumber;
    return this;
  }

  /**
   * Get poSRequestNumber
   * @return poSRequestNumber
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPoSRequestNumber() {
    return poSRequestNumber;
  }

  public void setPoSRequestNumber(String poSRequestNumber) {
    this.poSRequestNumber = poSRequestNumber;
  }

  public JsonWSPolicyDataRequestorDataOut POLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
    return this;
  }

  /**
   * Get POLICYNUMBER
   * @return POLICYNUMBER
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getPOLICYNUMBER() {
    return POLICYNUMBER;
  }

  public void setPOLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
  }

  public JsonWSPolicyDataRequestorDataOut INSUREDCLIENTID(String INSUREDCLIENTID) {
    this.INSUREDCLIENTID = INSUREDCLIENTID;
    return this;
  }

  /**
   * Get INSUREDCLIENTID
   * @return INSUREDCLIENTID
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getINSUREDCLIENTID() {
    return INSUREDCLIENTID;
  }

  public void setINSUREDCLIENTID(String INSUREDCLIENTID) {
    this.INSUREDCLIENTID = INSUREDCLIENTID;
  }

  public JsonWSPolicyDataRequestorDataOut OWNERCLIENTID(String OWNERCLIENTID) {
    this.OWNERCLIENTID = OWNERCLIENTID;
    return this;
  }

  /**
   * Get OWNERCLIENTID
   * @return OWNERCLIENTID
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getOWNERCLIENTID() {
    return OWNERCLIENTID;
  }

  public void setOWNERCLIENTID(String OWNERCLIENTID) {
    this.OWNERCLIENTID = OWNERCLIENTID;
  }

  public JsonWSPolicyDataRequestorDataOut i15DINVESNOTIFICATION(String i15DINVESNOTIFICATION) {
    this.i15DINVESNOTIFICATION = i15DINVESNOTIFICATION;
    return this;
  }

  /**
   * Get i15DINVESNOTIFICATION
   * @return i15DINVESNOTIFICATION
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getI15DINVESNOTIFICATION() {
    return i15DINVESNOTIFICATION;
  }

  public void setI15DINVESNOTIFICATION(String i15DINVESNOTIFICATION) {
    this.i15DINVESNOTIFICATION = i15DINVESNOTIFICATION;
  }

  public JsonWSPolicyDataRequestorDataOut i30DINVESNOTIFICATION(String i30DINVESNOTIFICATION) {
    this.i30DINVESNOTIFICATION = i30DINVESNOTIFICATION;
    return this;
  }

  /**
   * Get i30DINVESNOTIFICATION
   * @return i30DINVESNOTIFICATION
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getI30DINVESNOTIFICATION() {
    return i30DINVESNOTIFICATION;
  }

  public void setI30DINVESNOTIFICATION(String i30DINVESNOTIFICATION) {
    this.i30DINVESNOTIFICATION = i30DINVESNOTIFICATION;
  }

  public JsonWSPolicyDataRequestorDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public JsonWSPolicyDataRequestorDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public JsonWSPolicyDataRequestorDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public JsonWSPolicyDataRequestorDataOut batchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
    return this;
  }

  /**
   * Get batchNumber
   * @return batchNumber
  **/
  @ApiModelProperty(value = "")


  public String getBatchNumber() {
    return batchNumber;
  }

  public void setBatchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
  }

  public JsonWSPolicyDataRequestorDataOut portalClaimRequestNumber(String portalClaimRequestNumber) {
    this.portalClaimRequestNumber = portalClaimRequestNumber;
    return this;
  }

  /**
   * Get portalClaimRequestNumber
   * @return portalClaimRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getPortalClaimRequestNumber() {
    return portalClaimRequestNumber;
  }

  public void setPortalClaimRequestNumber(String portalClaimRequestNumber) {
    this.portalClaimRequestNumber = portalClaimRequestNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSPolicyDataRequestorDataOut jsonWSPolicyDataRequestorDataOut = (JsonWSPolicyDataRequestorDataOut) o;
    return Objects.equals(this.processmsg, jsonWSPolicyDataRequestorDataOut.processmsg) &&
        Objects.equals(this.decision, jsonWSPolicyDataRequestorDataOut.decision) &&
        Objects.equals(this.productcode, jsonWSPolicyDataRequestorDataOut.productcode) &&
        Objects.equals(this.COMPONENTCODE, jsonWSPolicyDataRequestorDataOut.COMPONENTCODE) &&
        Objects.equals(this.LENGTHOFSTAY, jsonWSPolicyDataRequestorDataOut.LENGTHOFSTAY) &&
        Objects.equals(this.SUMASSURED, jsonWSPolicyDataRequestorDataOut.SUMASSURED) &&
        Objects.equals(this.PLAN, jsonWSPolicyDataRequestorDataOut.PLAN) &&
        Objects.equals(this.CLAIMBILLAMOUNT, jsonWSPolicyDataRequestorDataOut.CLAIMBILLAMOUNT) &&
        Objects.equals(this.FLAGABUSE, jsonWSPolicyDataRequestorDataOut.FLAGABUSE) &&
        Objects.equals(this.PENDINGCLAIMS, jsonWSPolicyDataRequestorDataOut.PENDINGCLAIMS) &&
        Objects.equals(this.FATCAFLAG, jsonWSPolicyDataRequestorDataOut.FATCAFLAG) &&
        Objects.equals(this.CLAIMINVOICEDATE, jsonWSPolicyDataRequestorDataOut.CLAIMINVOICEDATE) &&
        Objects.equals(this.PAIDTODATE, jsonWSPolicyDataRequestorDataOut.PAIDTODATE) &&
        Objects.equals(this.POLICYSTATUS, jsonWSPolicyDataRequestorDataOut.POLICYSTATUS) &&
        Objects.equals(this.PREMIUMPOLICYSTATUS, jsonWSPolicyDataRequestorDataOut.PREMIUMPOLICYSTATUS) &&
        Objects.equals(this.COMPONENTSTATUS, jsonWSPolicyDataRequestorDataOut.COMPONENTSTATUS) &&
        Objects.equals(this.PREMIUMCOMPONENTSTATUS, jsonWSPolicyDataRequestorDataOut.PREMIUMCOMPONENTSTATUS) &&
        Objects.equals(this.BALANCELIMIT, jsonWSPolicyDataRequestorDataOut.BALANCELIMIT) &&
        Objects.equals(this.POLICYCURRENCY, jsonWSPolicyDataRequestorDataOut.POLICYCURRENCY) &&
        Objects.equals(this.CASECURRENCY, jsonWSPolicyDataRequestorDataOut.CASECURRENCY) &&
        Objects.equals(this.POLICYAGE, jsonWSPolicyDataRequestorDataOut.POLICYAGE) &&
        Objects.equals(this.DATEOFDEATH, jsonWSPolicyDataRequestorDataOut.DATEOFDEATH) &&
        Objects.equals(this.SAUNIT, jsonWSPolicyDataRequestorDataOut.SAUNIT) &&
        Objects.equals(this.CASESURGICALBENEFIT, jsonWSPolicyDataRequestorDataOut.CASESURGICALBENEFIT) &&
        Objects.equals(this.CLIENTSTATUS, jsonWSPolicyDataRequestorDataOut.CLIENTSTATUS) &&
        Objects.equals(this.ELAPSEDATE, jsonWSPolicyDataRequestorDataOut.ELAPSEDATE) &&
        Objects.equals(this.SURRENDERDATE, jsonWSPolicyDataRequestorDataOut.SURRENDERDATE) &&
        Objects.equals(this._30DINVESNOTIFICATION, jsonWSPolicyDataRequestorDataOut._30DINVESNOTIFICATION) &&
        Objects.equals(this._45DINVESNOTIFICATION, jsonWSPolicyDataRequestorDataOut._45DINVESNOTIFICATION) &&
        Objects.equals(this._60DINVESNOTIFICATION, jsonWSPolicyDataRequestorDataOut._60DINVESNOTIFICATION) &&
        Objects.equals(this.CLAIMTYPEUI, jsonWSPolicyDataRequestorDataOut.CLAIMTYPEUI) &&
        Objects.equals(this.CLAIMTYPELA, jsonWSPolicyDataRequestorDataOut.CLAIMTYPELA) &&
        Objects.equals(this.POSINITIATED, jsonWSPolicyDataRequestorDataOut.POSINITIATED) &&
        Objects.equals(this.INPROGRESSCLAIMNO, jsonWSPolicyDataRequestorDataOut.INPROGRESSCLAIMNO) &&
        Objects.equals(this.inVES2CUSTNOTIFY, jsonWSPolicyDataRequestorDataOut.inVES2CUSTNOTIFY) &&
        Objects.equals(this.inVES1CLUSERNOTIFY, jsonWSPolicyDataRequestorDataOut.inVES1CLUSERNOTIFY) &&
        Objects.equals(this.inVES2CLUSERNOTIFY, jsonWSPolicyDataRequestorDataOut.inVES2CLUSERNOTIFY) &&
        Objects.equals(this.DOCUMENTWAITPERIOD, jsonWSPolicyDataRequestorDataOut.DOCUMENTWAITPERIOD) &&
        Objects.equals(this.TPACLAIMNUMBER, jsonWSPolicyDataRequestorDataOut.TPACLAIMNUMBER) &&
        Objects.equals(this.poSRequestNumber, jsonWSPolicyDataRequestorDataOut.poSRequestNumber) &&
        Objects.equals(this.POLICYNUMBER, jsonWSPolicyDataRequestorDataOut.POLICYNUMBER) &&
        Objects.equals(this.INSUREDCLIENTID, jsonWSPolicyDataRequestorDataOut.INSUREDCLIENTID) &&
        Objects.equals(this.OWNERCLIENTID, jsonWSPolicyDataRequestorDataOut.OWNERCLIENTID) &&
        Objects.equals(this.i15DINVESNOTIFICATION, jsonWSPolicyDataRequestorDataOut.i15DINVESNOTIFICATION) &&
        Objects.equals(this.i30DINVESNOTIFICATION, jsonWSPolicyDataRequestorDataOut.i30DINVESNOTIFICATION) &&
        Objects.equals(this.wsProcessingStatus, jsonWSPolicyDataRequestorDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, jsonWSPolicyDataRequestorDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, jsonWSPolicyDataRequestorDataOut.wsSuccessMessage) &&
        Objects.equals(this.batchNumber, jsonWSPolicyDataRequestorDataOut.batchNumber) &&
        Objects.equals(this.portalClaimRequestNumber, jsonWSPolicyDataRequestorDataOut.portalClaimRequestNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(processmsg, decision, productcode, COMPONENTCODE, LENGTHOFSTAY, SUMASSURED, PLAN, CLAIMBILLAMOUNT, FLAGABUSE, PENDINGCLAIMS, FATCAFLAG, CLAIMINVOICEDATE, PAIDTODATE, POLICYSTATUS, PREMIUMPOLICYSTATUS, COMPONENTSTATUS, PREMIUMCOMPONENTSTATUS, BALANCELIMIT, POLICYCURRENCY, CASECURRENCY, POLICYAGE, DATEOFDEATH, SAUNIT, CASESURGICALBENEFIT, CLIENTSTATUS, ELAPSEDATE, SURRENDERDATE, _30DINVESNOTIFICATION, _45DINVESNOTIFICATION, _60DINVESNOTIFICATION, CLAIMTYPEUI, CLAIMTYPELA, POSINITIATED, INPROGRESSCLAIMNO, inVES2CUSTNOTIFY, inVES1CLUSERNOTIFY, inVES2CLUSERNOTIFY, DOCUMENTWAITPERIOD, TPACLAIMNUMBER, poSRequestNumber, POLICYNUMBER, INSUREDCLIENTID, OWNERCLIENTID, i15DINVESNOTIFICATION, i30DINVESNOTIFICATION, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, batchNumber, portalClaimRequestNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSPolicyDataRequestorDataOut {\n");
    
    sb.append("    processmsg: ").append(toIndentedString(processmsg)).append("\n");
    sb.append("    decision: ").append(toIndentedString(decision)).append("\n");
    sb.append("    productcode: ").append(toIndentedString(productcode)).append("\n");
    sb.append("    COMPONENTCODE: ").append(toIndentedString(COMPONENTCODE)).append("\n");
    sb.append("    LENGTHOFSTAY: ").append(toIndentedString(LENGTHOFSTAY)).append("\n");
    sb.append("    SUMASSURED: ").append(toIndentedString(SUMASSURED)).append("\n");
    sb.append("    PLAN: ").append(toIndentedString(PLAN)).append("\n");
    sb.append("    CLAIMBILLAMOUNT: ").append(toIndentedString(CLAIMBILLAMOUNT)).append("\n");
    sb.append("    FLAGABUSE: ").append(toIndentedString(FLAGABUSE)).append("\n");
    sb.append("    PENDINGCLAIMS: ").append(toIndentedString(PENDINGCLAIMS)).append("\n");
    sb.append("    FATCAFLAG: ").append(toIndentedString(FATCAFLAG)).append("\n");
    sb.append("    CLAIMINVOICEDATE: ").append(toIndentedString(CLAIMINVOICEDATE)).append("\n");
    sb.append("    PAIDTODATE: ").append(toIndentedString(PAIDTODATE)).append("\n");
    sb.append("    POLICYSTATUS: ").append(toIndentedString(POLICYSTATUS)).append("\n");
    sb.append("    PREMIUMPOLICYSTATUS: ").append(toIndentedString(PREMIUMPOLICYSTATUS)).append("\n");
    sb.append("    COMPONENTSTATUS: ").append(toIndentedString(COMPONENTSTATUS)).append("\n");
    sb.append("    PREMIUMCOMPONENTSTATUS: ").append(toIndentedString(PREMIUMCOMPONENTSTATUS)).append("\n");
    sb.append("    BALANCELIMIT: ").append(toIndentedString(BALANCELIMIT)).append("\n");
    sb.append("    POLICYCURRENCY: ").append(toIndentedString(POLICYCURRENCY)).append("\n");
    sb.append("    CASECURRENCY: ").append(toIndentedString(CASECURRENCY)).append("\n");
    sb.append("    POLICYAGE: ").append(toIndentedString(POLICYAGE)).append("\n");
    sb.append("    DATEOFDEATH: ").append(toIndentedString(DATEOFDEATH)).append("\n");
    sb.append("    SAUNIT: ").append(toIndentedString(SAUNIT)).append("\n");
    sb.append("    CASESURGICALBENEFIT: ").append(toIndentedString(CASESURGICALBENEFIT)).append("\n");
    sb.append("    CLIENTSTATUS: ").append(toIndentedString(CLIENTSTATUS)).append("\n");
    sb.append("    ELAPSEDATE: ").append(toIndentedString(ELAPSEDATE)).append("\n");
    sb.append("    SURRENDERDATE: ").append(toIndentedString(SURRENDERDATE)).append("\n");
    sb.append("    _30DINVESNOTIFICATION: ").append(toIndentedString(_30DINVESNOTIFICATION)).append("\n");
    sb.append("    _45DINVESNOTIFICATION: ").append(toIndentedString(_45DINVESNOTIFICATION)).append("\n");
    sb.append("    _60DINVESNOTIFICATION: ").append(toIndentedString(_60DINVESNOTIFICATION)).append("\n");
    sb.append("    CLAIMTYPEUI: ").append(toIndentedString(CLAIMTYPEUI)).append("\n");
    sb.append("    CLAIMTYPELA: ").append(toIndentedString(CLAIMTYPELA)).append("\n");
    sb.append("    POSINITIATED: ").append(toIndentedString(POSINITIATED)).append("\n");
    sb.append("    INPROGRESSCLAIMNO: ").append(toIndentedString(INPROGRESSCLAIMNO)).append("\n");
    sb.append("    inVES2CUSTNOTIFY: ").append(toIndentedString(inVES2CUSTNOTIFY)).append("\n");
    sb.append("    inVES1CLUSERNOTIFY: ").append(toIndentedString(inVES1CLUSERNOTIFY)).append("\n");
    sb.append("    inVES2CLUSERNOTIFY: ").append(toIndentedString(inVES2CLUSERNOTIFY)).append("\n");
    sb.append("    DOCUMENTWAITPERIOD: ").append(toIndentedString(DOCUMENTWAITPERIOD)).append("\n");
    sb.append("    TPACLAIMNUMBER: ").append(toIndentedString(TPACLAIMNUMBER)).append("\n");
    sb.append("    poSRequestNumber: ").append(toIndentedString(poSRequestNumber)).append("\n");
    sb.append("    POLICYNUMBER: ").append(toIndentedString(POLICYNUMBER)).append("\n");
    sb.append("    INSUREDCLIENTID: ").append(toIndentedString(INSUREDCLIENTID)).append("\n");
    sb.append("    OWNERCLIENTID: ").append(toIndentedString(OWNERCLIENTID)).append("\n");
    sb.append("    i15DINVESNOTIFICATION: ").append(toIndentedString(i15DINVESNOTIFICATION)).append("\n");
    sb.append("    i30DINVESNOTIFICATION: ").append(toIndentedString(i30DINVESNOTIFICATION)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    batchNumber: ").append(toIndentedString(batchNumber)).append("\n");
    sb.append("    portalClaimRequestNumber: ").append(toIndentedString(portalClaimRequestNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

