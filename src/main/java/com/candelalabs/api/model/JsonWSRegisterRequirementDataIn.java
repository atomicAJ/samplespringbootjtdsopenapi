package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSRegisterRequirementDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSRegisterRequirementDataIn   {
  @JsonProperty("claimNo")
  private String claimNo = null;

  @JsonProperty("polNo")
  private String polNo = null;

  @JsonProperty("docId")
  private String docId = null;

  @JsonProperty("requirementText")
  private String requirementText = null;

  @JsonProperty("processName")
  private String processName = null;

  @JsonProperty("decision")
  private String decision = null;

  @JsonProperty("lastUser")
  private String lastUser = null;

  @JsonProperty("RequirementStatus")
  private String requirementStatus = null;

  public JsonWSRegisterRequirementDataIn claimNo(String claimNo) {
    this.claimNo = claimNo;
    return this;
  }

  /**
   * Get claimNo
   * @return claimNo
  **/
  @ApiModelProperty(example = "7", required = true, value = "")
  @NotNull


  public String getClaimNo() {
    return claimNo;
  }

  public void setClaimNo(String claimNo) {
    this.claimNo = claimNo;
  }

  public JsonWSRegisterRequirementDataIn polNo(String polNo) {
    this.polNo = polNo;
    return this;
  }

  /**
   * Get polNo
   * @return polNo
  **/
  @ApiModelProperty(example = "10035913", required = true, value = "")
  @NotNull


  public String getPolNo() {
    return polNo;
  }

  public void setPolNo(String polNo) {
    this.polNo = polNo;
  }

  public JsonWSRegisterRequirementDataIn docId(String docId) {
    this.docId = docId;
    return this;
  }

  /**
   * Get docId
   * @return docId
  **/
  @ApiModelProperty(example = "DOCID", required = true, value = "")
  @NotNull


  public String getDocId() {
    return docId;
  }

  public void setDocId(String docId) {
    this.docId = docId;
  }

  public JsonWSRegisterRequirementDataIn requirementText(String requirementText) {
    this.requirementText = requirementText;
    return this;
  }

  /**
   * Get requirementText
   * @return requirementText
  **/
  @ApiModelProperty(example = "requirementText", required = true, value = "")
  @NotNull


  public String getRequirementText() {
    return requirementText;
  }

  public void setRequirementText(String requirementText) {
    this.requirementText = requirementText;
  }

  public JsonWSRegisterRequirementDataIn processName(String processName) {
    this.processName = processName;
    return this;
  }

  /**
   * Get processName
   * @return processName
  **/
  @ApiModelProperty(example = "CashLess", required = true, value = "")
  @NotNull


  public String getProcessName() {
    return processName;
  }

  public void setProcessName(String processName) {
    this.processName = processName;
  }

  public JsonWSRegisterRequirementDataIn decision(String decision) {
    this.decision = decision;
    return this;
  }

  /**
   * Get decision
   * @return decision
  **/
  @ApiModelProperty(example = "APPROVE", required = true, value = "")
  @NotNull


  public String getDecision() {
    return decision;
  }

  public void setDecision(String decision) {
    this.decision = decision;
  }

  public JsonWSRegisterRequirementDataIn lastUser(String lastUser) {
    this.lastUser = lastUser;
    return this;
  }

  /**
   * Get lastUser
   * @return lastUser
  **/
  @ApiModelProperty(example = "lastUser1", required = true, value = "")
  @NotNull


  public String getLastUser() {
    return lastUser;
  }

  public void setLastUser(String lastUser) {
    this.lastUser = lastUser;
  }

  public JsonWSRegisterRequirementDataIn requirementStatus(String requirementStatus) {
    this.requirementStatus = requirementStatus;
    return this;
  }

  /**
   * Get requirementStatus
   * @return requirementStatus
  **/
  @ApiModelProperty(example = "RequirementStatus", required = true, value = "")
  @NotNull


  public String getRequirementStatus() {
    return requirementStatus;
  }

  public void setRequirementStatus(String requirementStatus) {
    this.requirementStatus = requirementStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSRegisterRequirementDataIn jsonWSRegisterRequirementDataIn = (JsonWSRegisterRequirementDataIn) o;
    return Objects.equals(this.claimNo, jsonWSRegisterRequirementDataIn.claimNo) &&
        Objects.equals(this.polNo, jsonWSRegisterRequirementDataIn.polNo) &&
        Objects.equals(this.docId, jsonWSRegisterRequirementDataIn.docId) &&
        Objects.equals(this.requirementText, jsonWSRegisterRequirementDataIn.requirementText) &&
        Objects.equals(this.processName, jsonWSRegisterRequirementDataIn.processName) &&
        Objects.equals(this.decision, jsonWSRegisterRequirementDataIn.decision) &&
        Objects.equals(this.lastUser, jsonWSRegisterRequirementDataIn.lastUser) &&
        Objects.equals(this.requirementStatus, jsonWSRegisterRequirementDataIn.requirementStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNo, polNo, docId, requirementText, processName, decision, lastUser, requirementStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSRegisterRequirementDataIn {\n");
    
    sb.append("    claimNo: ").append(toIndentedString(claimNo)).append("\n");
    sb.append("    polNo: ").append(toIndentedString(polNo)).append("\n");
    sb.append("    docId: ").append(toIndentedString(docId)).append("\n");
    sb.append("    requirementText: ").append(toIndentedString(requirementText)).append("\n");
    sb.append("    processName: ").append(toIndentedString(processName)).append("\n");
    sb.append("    decision: ").append(toIndentedString(decision)).append("\n");
    sb.append("    lastUser: ").append(toIndentedString(lastUser)).append("\n");
    sb.append("    requirementStatus: ").append(toIndentedString(requirementStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

