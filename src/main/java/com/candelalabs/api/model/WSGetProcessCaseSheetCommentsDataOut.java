package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGetProcessCaseSheetCommentsDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGetProcessCaseSheetCommentsDataOut   {
  @JsonProperty("casesheetId")
  private BigDecimal casesheetId = null;

  @JsonProperty("processInstanceName")
  private String processInstanceName = null;

  @JsonProperty("caseReferenceKey")
  private String caseReferenceKey = null;

  @JsonProperty("comments")
  private String comments = null;

  @JsonProperty("processCaseSheetInsertionTime")
  private OffsetDateTime processCaseSheetInsertionTime = null;

  @JsonProperty("processId")
  private Integer processId = null;

  @JsonProperty("userName")
  private String userName = null;

  @JsonProperty("activityName")
  private String activityName = null;

  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  public WSGetProcessCaseSheetCommentsDataOut casesheetId(BigDecimal casesheetId) {
    this.casesheetId = casesheetId;
    return this;
  }

  /**
   * Get casesheetId
   * @return casesheetId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getCasesheetId() {
    return casesheetId;
  }

  public void setCasesheetId(BigDecimal casesheetId) {
    this.casesheetId = casesheetId;
  }

  public WSGetProcessCaseSheetCommentsDataOut processInstanceName(String processInstanceName) {
    this.processInstanceName = processInstanceName;
    return this;
  }

  /**
   * Get processInstanceName
   * @return processInstanceName
  **/
  @ApiModelProperty(value = "")


  public String getProcessInstanceName() {
    return processInstanceName;
  }

  public void setProcessInstanceName(String processInstanceName) {
    this.processInstanceName = processInstanceName;
  }

  public WSGetProcessCaseSheetCommentsDataOut caseReferenceKey(String caseReferenceKey) {
    this.caseReferenceKey = caseReferenceKey;
    return this;
  }

  /**
   * Get caseReferenceKey
   * @return caseReferenceKey
  **/
  @ApiModelProperty(value = "")


  public String getCaseReferenceKey() {
    return caseReferenceKey;
  }

  public void setCaseReferenceKey(String caseReferenceKey) {
    this.caseReferenceKey = caseReferenceKey;
  }

  public WSGetProcessCaseSheetCommentsDataOut comments(String comments) {
    this.comments = comments;
    return this;
  }

  /**
   * Get comments
   * @return comments
  **/
  @ApiModelProperty(value = "")


  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public WSGetProcessCaseSheetCommentsDataOut processCaseSheetInsertionTime(OffsetDateTime processCaseSheetInsertionTime) {
    this.processCaseSheetInsertionTime = processCaseSheetInsertionTime;
    return this;
  }

  /**
   * Get processCaseSheetInsertionTime
   * @return processCaseSheetInsertionTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getProcessCaseSheetInsertionTime() {
    return processCaseSheetInsertionTime;
  }

  public void setProcessCaseSheetInsertionTime(OffsetDateTime processCaseSheetInsertionTime) {
    this.processCaseSheetInsertionTime = processCaseSheetInsertionTime;
  }

  public WSGetProcessCaseSheetCommentsDataOut processId(Integer processId) {
    this.processId = processId;
    return this;
  }

  /**
   * Get processId
   * @return processId
  **/
  @ApiModelProperty(value = "")


  public Integer getProcessId() {
    return processId;
  }

  public void setProcessId(Integer processId) {
    this.processId = processId;
  }

  public WSGetProcessCaseSheetCommentsDataOut userName(String userName) {
    this.userName = userName;
    return this;
  }

  /**
   * Get userName
   * @return userName
  **/
  @ApiModelProperty(value = "")


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public WSGetProcessCaseSheetCommentsDataOut activityName(String activityName) {
    this.activityName = activityName;
    return this;
  }

  /**
   * Get activityName
   * @return activityName
  **/
  @ApiModelProperty(value = "")


  public String getActivityName() {
    return activityName;
  }

  public void setActivityName(String activityName) {
    this.activityName = activityName;
  }

  public WSGetProcessCaseSheetCommentsDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSGetProcessCaseSheetCommentsDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSGetProcessCaseSheetCommentsDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGetProcessCaseSheetCommentsDataOut wsGetProcessCaseSheetCommentsDataOut = (WSGetProcessCaseSheetCommentsDataOut) o;
    return Objects.equals(this.casesheetId, wsGetProcessCaseSheetCommentsDataOut.casesheetId) &&
        Objects.equals(this.processInstanceName, wsGetProcessCaseSheetCommentsDataOut.processInstanceName) &&
        Objects.equals(this.caseReferenceKey, wsGetProcessCaseSheetCommentsDataOut.caseReferenceKey) &&
        Objects.equals(this.comments, wsGetProcessCaseSheetCommentsDataOut.comments) &&
        Objects.equals(this.processCaseSheetInsertionTime, wsGetProcessCaseSheetCommentsDataOut.processCaseSheetInsertionTime) &&
        Objects.equals(this.processId, wsGetProcessCaseSheetCommentsDataOut.processId) &&
        Objects.equals(this.userName, wsGetProcessCaseSheetCommentsDataOut.userName) &&
        Objects.equals(this.activityName, wsGetProcessCaseSheetCommentsDataOut.activityName) &&
        Objects.equals(this.wsProcessingStatus, wsGetProcessCaseSheetCommentsDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsGetProcessCaseSheetCommentsDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsGetProcessCaseSheetCommentsDataOut.wsSuccessMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(casesheetId, processInstanceName, caseReferenceKey, comments, processCaseSheetInsertionTime, processId, userName, activityName, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGetProcessCaseSheetCommentsDataOut {\n");
    
    sb.append("    casesheetId: ").append(toIndentedString(casesheetId)).append("\n");
    sb.append("    processInstanceName: ").append(toIndentedString(processInstanceName)).append("\n");
    sb.append("    caseReferenceKey: ").append(toIndentedString(caseReferenceKey)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("    processCaseSheetInsertionTime: ").append(toIndentedString(processCaseSheetInsertionTime)).append("\n");
    sb.append("    processId: ").append(toIndentedString(processId)).append("\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    activityName: ").append(toIndentedString(activityName)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

