package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.SignalDetailsVariables;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SignalDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class SignalDetails   {
  @JsonProperty("processInstanceId")
  private String processInstanceId = null;

  @JsonProperty("activityId")
  private String activityId = null;

  @JsonProperty("variables")
  @Valid
  private List<SignalDetailsVariables> variables = null;

  public SignalDetails processInstanceId(String processInstanceId) {
    this.processInstanceId = processInstanceId;
    return this;
  }

  /**
   * Get processInstanceId
   * @return processInstanceId
  **/
  @ApiModelProperty(example = "[processInstanceId]", value = "")


  public String getProcessInstanceId() {
    return processInstanceId;
  }

  public void setProcessInstanceId(String processInstanceId) {
    this.processInstanceId = processInstanceId;
  }

  public SignalDetails activityId(String activityId) {
    this.activityId = activityId;
    return this;
  }

  /**
   * Get activityId
   * @return activityId
  **/
  @ApiModelProperty(example = "[activityId]", value = "")


  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }

  public SignalDetails variables(List<SignalDetailsVariables> variables) {
    this.variables = variables;
    return this;
  }

  public SignalDetails addVariablesItem(SignalDetailsVariables variablesItem) {
    if (this.variables == null) {
      this.variables = new ArrayList<>();
    }
    this.variables.add(variablesItem);
    return this;
  }

  /**
   * Get variables
   * @return variables
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<SignalDetailsVariables> getVariables() {
    return variables;
  }

  public void setVariables(List<SignalDetailsVariables> variables) {
    this.variables = variables;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SignalDetails signalDetails = (SignalDetails) o;
    return Objects.equals(this.processInstanceId, signalDetails.processInstanceId) &&
        Objects.equals(this.activityId, signalDetails.activityId) &&
        Objects.equals(this.variables, signalDetails.variables);
  }

  @Override
  public int hashCode() {
    return Objects.hash(processInstanceId, activityId, variables);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SignalDetails {\n");
    
    sb.append("    processInstanceId: ").append(toIndentedString(processInstanceId)).append("\n");
    sb.append("    activityId: ").append(toIndentedString(activityId)).append("\n");
    sb.append("    variables: ").append(toIndentedString(variables)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

