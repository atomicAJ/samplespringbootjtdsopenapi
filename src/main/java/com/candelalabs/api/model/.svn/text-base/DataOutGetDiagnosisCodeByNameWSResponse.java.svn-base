package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutGetDiagnosisCodeByNameWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutGetDiagnosisCodeByNameWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strDiagnosisCode")
  private String strDiagnosisCode = null;

  @JsonProperty("strClaimTypeUI")
  private String strClaimTypeUI = null;

  @JsonProperty("iClaimSumAssuredPercent")
  private BigDecimal iClaimSumAssuredPercent = null;

  public DataOutGetDiagnosisCodeByNameWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutGetDiagnosisCodeByNameWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutGetDiagnosisCodeByNameWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutGetDiagnosisCodeByNameWSResponse strDiagnosisCode(String strDiagnosisCode) {
    this.strDiagnosisCode = strDiagnosisCode;
    return this;
  }

  /**
   * Get strDiagnosisCode
   * @return strDiagnosisCode
  **/
  @ApiModelProperty(value = "")


  public String getStrDiagnosisCode() {
    return strDiagnosisCode;
  }

  public void setStrDiagnosisCode(String strDiagnosisCode) {
    this.strDiagnosisCode = strDiagnosisCode;
  }

  public DataOutGetDiagnosisCodeByNameWSResponse strClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
    return this;
  }

  /**
   * Get strClaimTypeUI
   * @return strClaimTypeUI
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeUI() {
    return strClaimTypeUI;
  }

  public void setStrClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
  }

  public DataOutGetDiagnosisCodeByNameWSResponse iClaimSumAssuredPercent(BigDecimal iClaimSumAssuredPercent) {
    this.iClaimSumAssuredPercent = iClaimSumAssuredPercent;
    return this;
  }

  /**
   * Get iClaimSumAssuredPercent
   * @return iClaimSumAssuredPercent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIClaimSumAssuredPercent() {
    return iClaimSumAssuredPercent;
  }

  public void setIClaimSumAssuredPercent(BigDecimal iClaimSumAssuredPercent) {
    this.iClaimSumAssuredPercent = iClaimSumAssuredPercent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutGetDiagnosisCodeByNameWSResponse dataOutGetDiagnosisCodeByNameWSResponse = (DataOutGetDiagnosisCodeByNameWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutGetDiagnosisCodeByNameWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutGetDiagnosisCodeByNameWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutGetDiagnosisCodeByNameWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strDiagnosisCode, dataOutGetDiagnosisCodeByNameWSResponse.strDiagnosisCode) &&
        Objects.equals(this.strClaimTypeUI, dataOutGetDiagnosisCodeByNameWSResponse.strClaimTypeUI) &&
        Objects.equals(this.iClaimSumAssuredPercent, dataOutGetDiagnosisCodeByNameWSResponse.iClaimSumAssuredPercent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strDiagnosisCode, strClaimTypeUI, iClaimSumAssuredPercent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutGetDiagnosisCodeByNameWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strDiagnosisCode: ").append(toIndentedString(strDiagnosisCode)).append("\n");
    sb.append("    strClaimTypeUI: ").append(toIndentedString(strClaimTypeUI)).append("\n");
    sb.append("    iClaimSumAssuredPercent: ").append(toIndentedString(iClaimSumAssuredPercent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

