package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TransactionHistory
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class TransactionHistory   {
  @JsonProperty("posRequestNumber")
  private String posRequestNumber = null;

  @JsonProperty("typeOfTransaction")
  private String typeOfTransaction = null;

  @JsonProperty("requestSubmissionDate")
  private String requestSubmissionDate = null;

  @JsonProperty("requestReceivedDate")
  private String requestReceivedDate = null;

  @JsonProperty("channelSubmission")
  private String channelSubmission = null;

  @JsonProperty("requestStatus")
  private String requestStatus = null;

  public TransactionHistory posRequestNumber(String posRequestNumber) {
    this.posRequestNumber = posRequestNumber;
    return this;
  }

  /**
   * Get posRequestNumber
   * @return posRequestNumber
  **/
  @ApiModelProperty(example = "[posRequestNumber]", value = "")


  public String getPosRequestNumber() {
    return posRequestNumber;
  }

  public void setPosRequestNumber(String posRequestNumber) {
    this.posRequestNumber = posRequestNumber;
  }

  public TransactionHistory typeOfTransaction(String typeOfTransaction) {
    this.typeOfTransaction = typeOfTransaction;
    return this;
  }

  /**
   * Get typeOfTransaction
   * @return typeOfTransaction
  **/
  @ApiModelProperty(example = "[typeOfTransaction]", value = "")


  public String getTypeOfTransaction() {
    return typeOfTransaction;
  }

  public void setTypeOfTransaction(String typeOfTransaction) {
    this.typeOfTransaction = typeOfTransaction;
  }

  public TransactionHistory requestSubmissionDate(String requestSubmissionDate) {
    this.requestSubmissionDate = requestSubmissionDate;
    return this;
  }

  /**
   * Get requestSubmissionDate
   * @return requestSubmissionDate
  **/
  @ApiModelProperty(example = "[requestSubmissionDate]", value = "")


  public String getRequestSubmissionDate() {
    return requestSubmissionDate;
  }

  public void setRequestSubmissionDate(String requestSubmissionDate) {
    this.requestSubmissionDate = requestSubmissionDate;
  }

  public TransactionHistory requestReceivedDate(String requestReceivedDate) {
    this.requestReceivedDate = requestReceivedDate;
    return this;
  }

  /**
   * Get requestReceivedDate
   * @return requestReceivedDate
  **/
  @ApiModelProperty(example = "[requestReceivedDate]", value = "")


  public String getRequestReceivedDate() {
    return requestReceivedDate;
  }

  public void setRequestReceivedDate(String requestReceivedDate) {
    this.requestReceivedDate = requestReceivedDate;
  }

  public TransactionHistory channelSubmission(String channelSubmission) {
    this.channelSubmission = channelSubmission;
    return this;
  }

  /**
   * Get channelSubmission
   * @return channelSubmission
  **/
  @ApiModelProperty(example = "[channelSubmission]", value = "")


  public String getChannelSubmission() {
    return channelSubmission;
  }

  public void setChannelSubmission(String channelSubmission) {
    this.channelSubmission = channelSubmission;
  }

  public TransactionHistory requestStatus(String requestStatus) {
    this.requestStatus = requestStatus;
    return this;
  }

  /**
   * Get requestStatus
   * @return requestStatus
  **/
  @ApiModelProperty(example = "[requestStatus]", value = "")


  public String getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(String requestStatus) {
    this.requestStatus = requestStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionHistory transactionHistory = (TransactionHistory) o;
    return Objects.equals(this.posRequestNumber, transactionHistory.posRequestNumber) &&
        Objects.equals(this.typeOfTransaction, transactionHistory.typeOfTransaction) &&
        Objects.equals(this.requestSubmissionDate, transactionHistory.requestSubmissionDate) &&
        Objects.equals(this.requestReceivedDate, transactionHistory.requestReceivedDate) &&
        Objects.equals(this.channelSubmission, transactionHistory.channelSubmission) &&
        Objects.equals(this.requestStatus, transactionHistory.requestStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(posRequestNumber, typeOfTransaction, requestSubmissionDate, requestReceivedDate, channelSubmission, requestStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionHistory {\n");
    
    sb.append("    posRequestNumber: ").append(toIndentedString(posRequestNumber)).append("\n");
    sb.append("    typeOfTransaction: ").append(toIndentedString(typeOfTransaction)).append("\n");
    sb.append("    requestSubmissionDate: ").append(toIndentedString(requestSubmissionDate)).append("\n");
    sb.append("    requestReceivedDate: ").append(toIndentedString(requestReceivedDate)).append("\n");
    sb.append("    channelSubmission: ").append(toIndentedString(channelSubmission)).append("\n");
    sb.append("    requestStatus: ").append(toIndentedString(requestStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

