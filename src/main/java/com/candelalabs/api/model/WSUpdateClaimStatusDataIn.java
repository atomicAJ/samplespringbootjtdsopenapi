package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateClaimStatusDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateClaimStatusDataIn   {
  @JsonProperty("posRequestNumber")
  private String posRequestNumber = null;

  @JsonProperty("claimstatus")
  private String claimstatus = null;

  public WSUpdateClaimStatusDataIn posRequestNumber(String posRequestNumber) {
    this.posRequestNumber = posRequestNumber;
    return this;
  }

  /**
   * Get posRequestNumber
   * @return posRequestNumber
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getPosRequestNumber() {
    return posRequestNumber;
  }

  public void setPosRequestNumber(String posRequestNumber) {
    this.posRequestNumber = posRequestNumber;
  }

  public WSUpdateClaimStatusDataIn claimstatus(String claimstatus) {
    this.claimstatus = claimstatus;
    return this;
  }

  /**
   * Get claimstatus
   * @return claimstatus
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getClaimstatus() {
    return claimstatus;
  }

  public void setClaimstatus(String claimstatus) {
    this.claimstatus = claimstatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateClaimStatusDataIn wsUpdateClaimStatusDataIn = (WSUpdateClaimStatusDataIn) o;
    return Objects.equals(this.posRequestNumber, wsUpdateClaimStatusDataIn.posRequestNumber) &&
        Objects.equals(this.claimstatus, wsUpdateClaimStatusDataIn.claimstatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(posRequestNumber, claimstatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateClaimStatusDataIn {\n");
    
    sb.append("    posRequestNumber: ").append(toIndentedString(posRequestNumber)).append("\n");
    sb.append("    claimstatus: ").append(toIndentedString(claimstatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

