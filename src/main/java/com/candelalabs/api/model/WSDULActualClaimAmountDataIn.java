package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSDULActualClaimAmountDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSDULActualClaimAmountDataIn   {
  @JsonProperty("CLAIMNO")
  private String CLAIMNO = null;

  @JsonProperty("ACITVITYID")
  private String ACITVITYID = null;

  @JsonProperty("approveDate")
  private LocalDate approveDate = null;

  public WSDULActualClaimAmountDataIn CLAIMNO(String CLAIMNO) {
    this.CLAIMNO = CLAIMNO;
    return this;
  }

  /**
   * claimNo
   * @return CLAIMNO
  **/
  @ApiModelProperty(example = "string", required = true, value = "claimNo")
  @NotNull


  public String getCLAIMNO() {
    return CLAIMNO;
  }

  public void setCLAIMNO(String CLAIMNO) {
    this.CLAIMNO = CLAIMNO;
  }

  public WSDULActualClaimAmountDataIn ACITVITYID(String ACITVITYID) {
    this.ACITVITYID = ACITVITYID;
    return this;
  }

  /**
   * Get ACITVITYID
   * @return ACITVITYID
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getACITVITYID() {
    return ACITVITYID;
  }

  public void setACITVITYID(String ACITVITYID) {
    this.ACITVITYID = ACITVITYID;
  }

  public WSDULActualClaimAmountDataIn approveDate(LocalDate approveDate) {
    this.approveDate = approveDate;
    return this;
  }

  /**
   * Get approveDate
   * @return approveDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LocalDate getApproveDate() {
    return approveDate;
  }

  public void setApproveDate(LocalDate approveDate) {
    this.approveDate = approveDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSDULActualClaimAmountDataIn wsDULActualClaimAmountDataIn = (WSDULActualClaimAmountDataIn) o;
    return Objects.equals(this.CLAIMNO, wsDULActualClaimAmountDataIn.CLAIMNO) &&
        Objects.equals(this.ACITVITYID, wsDULActualClaimAmountDataIn.ACITVITYID) &&
        Objects.equals(this.approveDate, wsDULActualClaimAmountDataIn.approveDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(CLAIMNO, ACITVITYID, approveDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSDULActualClaimAmountDataIn {\n");
    
    sb.append("    CLAIMNO: ").append(toIndentedString(CLAIMNO)).append("\n");
    sb.append("    ACITVITYID: ").append(toIndentedString(ACITVITYID)).append("\n");
    sb.append("    approveDate: ").append(toIndentedString(approveDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

