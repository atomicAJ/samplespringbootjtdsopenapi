package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClientComponentList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetComponentsListData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetComponentsListData   {
  @JsonProperty("strClientNumber")
  private String strClientNumber = null;

  @JsonProperty("clientComponentList")
  @Valid
  private List<ClaimWorksheetUIDataClaimWorksheetClientComponentList> clientComponentList = null;

  public ClaimWorksheetUIDataClaimWorksheetComponentsListData strClientNumber(String strClientNumber) {
    this.strClientNumber = strClientNumber;
    return this;
  }

  /**
   * Get strClientNumber
   * @return strClientNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrClientNumber() {
    return strClientNumber;
  }

  public void setStrClientNumber(String strClientNumber) {
    this.strClientNumber = strClientNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetComponentsListData clientComponentList(List<ClaimWorksheetUIDataClaimWorksheetClientComponentList> clientComponentList) {
    this.clientComponentList = clientComponentList;
    return this;
  }

  public ClaimWorksheetUIDataClaimWorksheetComponentsListData addClientComponentListItem(ClaimWorksheetUIDataClaimWorksheetClientComponentList clientComponentListItem) {
    if (this.clientComponentList == null) {
      this.clientComponentList = new ArrayList<>();
    }
    this.clientComponentList.add(clientComponentListItem);
    return this;
  }

  /**
   * Get clientComponentList
   * @return clientComponentList
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimWorksheetUIDataClaimWorksheetClientComponentList> getClientComponentList() {
    return clientComponentList;
  }

  public void setClientComponentList(List<ClaimWorksheetUIDataClaimWorksheetClientComponentList> clientComponentList) {
    this.clientComponentList = clientComponentList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetComponentsListData claimWorksheetUIDataClaimWorksheetComponentsListData = (ClaimWorksheetUIDataClaimWorksheetComponentsListData) o;
    return Objects.equals(this.strClientNumber, claimWorksheetUIDataClaimWorksheetComponentsListData.strClientNumber) &&
        Objects.equals(this.clientComponentList, claimWorksheetUIDataClaimWorksheetComponentsListData.clientComponentList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClientNumber, clientComponentList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetComponentsListData {\n");
    
    sb.append("    strClientNumber: ").append(toIndentedString(strClientNumber)).append("\n");
    sb.append("    clientComponentList: ").append(toIndentedString(clientComponentList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

