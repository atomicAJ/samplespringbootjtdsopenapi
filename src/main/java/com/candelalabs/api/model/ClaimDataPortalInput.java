package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDataPortalInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDataPortalInput   {
  @JsonProperty("strPencilClaimNumber")
  private String strPencilClaimNumber = null;

  public ClaimDataPortalInput strPencilClaimNumber(String strPencilClaimNumber) {
    this.strPencilClaimNumber = strPencilClaimNumber;
    return this;
  }

  /**
   * Get strPencilClaimNumber
   * @return strPencilClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPencilClaimNumber() {
    return strPencilClaimNumber;
  }

  public void setStrPencilClaimNumber(String strPencilClaimNumber) {
    this.strPencilClaimNumber = strPencilClaimNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDataPortalInput claimDataPortalInput = (ClaimDataPortalInput) o;
    return Objects.equals(this.strPencilClaimNumber, claimDataPortalInput.strPencilClaimNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strPencilClaimNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDataPortalInput {\n");
    
    sb.append("    strPencilClaimNumber: ").append(toIndentedString(strPencilClaimNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

