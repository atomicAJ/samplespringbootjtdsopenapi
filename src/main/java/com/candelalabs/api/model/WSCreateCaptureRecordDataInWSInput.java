package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSCreateCaptureRecordDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSCreateCaptureRecordDataInWSInput   {
  @JsonProperty("strBatchNumber")
  private String strBatchNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("dtDocumentScannedDate")
  private String dtDocumentScannedDate = null;

  @JsonProperty("strOwnerIdType")
  private String strOwnerIdType = null;

  @JsonProperty("strOwnerIdNumber")
  private String strOwnerIdNumber = null;

  @JsonProperty("strPayeeName")
  private String strPayeeName = null;

  @JsonProperty("strInsuredClientName")
  private String strInsuredClientName = null;

  @JsonProperty("strInsuredIdType")
  private String strInsuredIdType = null;

  @JsonProperty("strInsuredIdNumber")
  private String strInsuredIdNumber = null;

  @JsonProperty("strFormClaimType")
  private String strFormClaimType = null;

  @JsonProperty("strFormBenefitType")
  private String strFormBenefitType = null;

  @JsonProperty("strSurgeryPerformed")
  private String strSurgeryPerformed = null;

  @JsonProperty("dtTreatmentStartDate")
  private String dtTreatmentStartDate = null;

  @JsonProperty("dtTreatmentCompletionDate")
  private String dtTreatmentCompletionDate = null;

  @JsonProperty("strHospitalName")
  private String strHospitalName = null;

  @JsonProperty("strFormCurrency")
  private String strFormCurrency = null;

  @JsonProperty("decTotalBillingAmount")
  private String decTotalBillingAmount = null;

  @JsonProperty("strPhysician")
  private String strPhysician = null;

  @JsonProperty("strBankAccountNumber")
  private String strBankAccountNumber = null;

  @JsonProperty("strPaymentAccountOwnerName")
  private String strPaymentAccountOwnerName = null;

  public WSCreateCaptureRecordDataInWSInput strBatchNumber(String strBatchNumber) {
    this.strBatchNumber = strBatchNumber;
    return this;
  }

  /**
   * Get strBatchNumber
   * @return strBatchNumber
  **/
  @ApiModelProperty(example = "12323", value = "")


  public String getStrBatchNumber() {
    return strBatchNumber;
  }

  public void setStrBatchNumber(String strBatchNumber) {
    this.strBatchNumber = strBatchNumber;
  }

  public WSCreateCaptureRecordDataInWSInput strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(example = "88888888", value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public WSCreateCaptureRecordDataInWSInput dtDocumentScannedDate(String dtDocumentScannedDate) {
    this.dtDocumentScannedDate = dtDocumentScannedDate;
    return this;
  }

  /**
   * Get dtDocumentScannedDate
   * @return dtDocumentScannedDate
  **/
  @ApiModelProperty(example = "01/10/2018", value = "")


  public String getDtDocumentScannedDate() {
    return dtDocumentScannedDate;
  }

  public void setDtDocumentScannedDate(String dtDocumentScannedDate) {
    this.dtDocumentScannedDate = dtDocumentScannedDate;
  }

  public WSCreateCaptureRecordDataInWSInput strOwnerIdType(String strOwnerIdType) {
    this.strOwnerIdType = strOwnerIdType;
    return this;
  }

  /**
   * Get strOwnerIdType
   * @return strOwnerIdType
  **/
  @ApiModelProperty(example = "DL", value = "")


  public String getStrOwnerIdType() {
    return strOwnerIdType;
  }

  public void setStrOwnerIdType(String strOwnerIdType) {
    this.strOwnerIdType = strOwnerIdType;
  }

  public WSCreateCaptureRecordDataInWSInput strOwnerIdNumber(String strOwnerIdNumber) {
    this.strOwnerIdNumber = strOwnerIdNumber;
    return this;
  }

  /**
   * Get strOwnerIdNumber
   * @return strOwnerIdNumber
  **/
  @ApiModelProperty(example = "DL1234", value = "")


  public String getStrOwnerIdNumber() {
    return strOwnerIdNumber;
  }

  public void setStrOwnerIdNumber(String strOwnerIdNumber) {
    this.strOwnerIdNumber = strOwnerIdNumber;
  }

  public WSCreateCaptureRecordDataInWSInput strPayeeName(String strPayeeName) {
    this.strPayeeName = strPayeeName;
    return this;
  }

  /**
   * Get strPayeeName
   * @return strPayeeName
  **/
  @ApiModelProperty(example = "abdnbcd", value = "")


  public String getStrPayeeName() {
    return strPayeeName;
  }

  public void setStrPayeeName(String strPayeeName) {
    this.strPayeeName = strPayeeName;
  }

  public WSCreateCaptureRecordDataInWSInput strInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
    return this;
  }

  /**
   * Get strInsuredClientName
   * @return strInsuredClientName
  **/
  @ApiModelProperty(example = "aabbccdd", value = "")


  public String getStrInsuredClientName() {
    return strInsuredClientName;
  }

  public void setStrInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
  }

  public WSCreateCaptureRecordDataInWSInput strInsuredIdType(String strInsuredIdType) {
    this.strInsuredIdType = strInsuredIdType;
    return this;
  }

  /**
   * Get strInsuredIdType
   * @return strInsuredIdType
  **/
  @ApiModelProperty(example = "DL", value = "")


  public String getStrInsuredIdType() {
    return strInsuredIdType;
  }

  public void setStrInsuredIdType(String strInsuredIdType) {
    this.strInsuredIdType = strInsuredIdType;
  }

  public WSCreateCaptureRecordDataInWSInput strInsuredIdNumber(String strInsuredIdNumber) {
    this.strInsuredIdNumber = strInsuredIdNumber;
    return this;
  }

  /**
   * Get strInsuredIdNumber
   * @return strInsuredIdNumber
  **/
  @ApiModelProperty(example = "DL1234", value = "")


  public String getStrInsuredIdNumber() {
    return strInsuredIdNumber;
  }

  public void setStrInsuredIdNumber(String strInsuredIdNumber) {
    this.strInsuredIdNumber = strInsuredIdNumber;
  }

  public WSCreateCaptureRecordDataInWSInput strFormClaimType(String strFormClaimType) {
    this.strFormClaimType = strFormClaimType;
    return this;
  }

  /**
   * Get strFormClaimType
   * @return strFormClaimType
  **/
  @ApiModelProperty(example = "Living Benefit", value = "")


  public String getStrFormClaimType() {
    return strFormClaimType;
  }

  public void setStrFormClaimType(String strFormClaimType) {
    this.strFormClaimType = strFormClaimType;
  }

  public WSCreateCaptureRecordDataInWSInput strFormBenefitType(String strFormBenefitType) {
    this.strFormBenefitType = strFormBenefitType;
    return this;
  }

  /**
   * Get strFormBenefitType
   * @return strFormBenefitType
  **/
  @ApiModelProperty(example = "Hospital Cash", value = "")


  public String getStrFormBenefitType() {
    return strFormBenefitType;
  }

  public void setStrFormBenefitType(String strFormBenefitType) {
    this.strFormBenefitType = strFormBenefitType;
  }

  public WSCreateCaptureRecordDataInWSInput strSurgeryPerformed(String strSurgeryPerformed) {
    this.strSurgeryPerformed = strSurgeryPerformed;
    return this;
  }

  /**
   * Get strSurgeryPerformed
   * @return strSurgeryPerformed
  **/
  @ApiModelProperty(example = "N", value = "")


  public String getStrSurgeryPerformed() {
    return strSurgeryPerformed;
  }

  public void setStrSurgeryPerformed(String strSurgeryPerformed) {
    this.strSurgeryPerformed = strSurgeryPerformed;
  }

  public WSCreateCaptureRecordDataInWSInput dtTreatmentStartDate(String dtTreatmentStartDate) {
    this.dtTreatmentStartDate = dtTreatmentStartDate;
    return this;
  }

  /**
   * Get dtTreatmentStartDate
   * @return dtTreatmentStartDate
  **/
  @ApiModelProperty(example = "01/10/2018", value = "")


  public String getDtTreatmentStartDate() {
    return dtTreatmentStartDate;
  }

  public void setDtTreatmentStartDate(String dtTreatmentStartDate) {
    this.dtTreatmentStartDate = dtTreatmentStartDate;
  }

  public WSCreateCaptureRecordDataInWSInput dtTreatmentCompletionDate(String dtTreatmentCompletionDate) {
    this.dtTreatmentCompletionDate = dtTreatmentCompletionDate;
    return this;
  }

  /**
   * Get dtTreatmentCompletionDate
   * @return dtTreatmentCompletionDate
  **/
  @ApiModelProperty(example = "01/10/2018", value = "")


  public String getDtTreatmentCompletionDate() {
    return dtTreatmentCompletionDate;
  }

  public void setDtTreatmentCompletionDate(String dtTreatmentCompletionDate) {
    this.dtTreatmentCompletionDate = dtTreatmentCompletionDate;
  }

  public WSCreateCaptureRecordDataInWSInput strHospitalName(String strHospitalName) {
    this.strHospitalName = strHospitalName;
    return this;
  }

  /**
   * Get strHospitalName
   * @return strHospitalName
  **/
  @ApiModelProperty(example = "abccba", value = "")


  public String getStrHospitalName() {
    return strHospitalName;
  }

  public void setStrHospitalName(String strHospitalName) {
    this.strHospitalName = strHospitalName;
  }

  public WSCreateCaptureRecordDataInWSInput strFormCurrency(String strFormCurrency) {
    this.strFormCurrency = strFormCurrency;
    return this;
  }

  /**
   * Get strFormCurrency
   * @return strFormCurrency
  **/
  @ApiModelProperty(example = "IDR", value = "")


  public String getStrFormCurrency() {
    return strFormCurrency;
  }

  public void setStrFormCurrency(String strFormCurrency) {
    this.strFormCurrency = strFormCurrency;
  }

  public WSCreateCaptureRecordDataInWSInput decTotalBillingAmount(String decTotalBillingAmount) {
    this.decTotalBillingAmount = decTotalBillingAmount;
    return this;
  }

  /**
   * Get decTotalBillingAmount
   * @return decTotalBillingAmount
  **/
  @ApiModelProperty(example = "10000000.00", value = "")


  public String getDecTotalBillingAmount() {
    return decTotalBillingAmount;
  }

  public void setDecTotalBillingAmount(String decTotalBillingAmount) {
    this.decTotalBillingAmount = decTotalBillingAmount;
  }

  public WSCreateCaptureRecordDataInWSInput strPhysician(String strPhysician) {
    this.strPhysician = strPhysician;
    return this;
  }

  /**
   * Get strPhysician
   * @return strPhysician
  **/
  @ApiModelProperty(example = "abcdef", value = "")


  public String getStrPhysician() {
    return strPhysician;
  }

  public void setStrPhysician(String strPhysician) {
    this.strPhysician = strPhysician;
  }

  public WSCreateCaptureRecordDataInWSInput strBankAccountNumber(String strBankAccountNumber) {
    this.strBankAccountNumber = strBankAccountNumber;
    return this;
  }

  /**
   * Get strBankAccountNumber
   * @return strBankAccountNumber
  **/
  @ApiModelProperty(example = "1234567890", value = "")


  public String getStrBankAccountNumber() {
    return strBankAccountNumber;
  }

  public void setStrBankAccountNumber(String strBankAccountNumber) {
    this.strBankAccountNumber = strBankAccountNumber;
  }

  public WSCreateCaptureRecordDataInWSInput strPaymentAccountOwnerName(String strPaymentAccountOwnerName) {
    this.strPaymentAccountOwnerName = strPaymentAccountOwnerName;
    return this;
  }

  /**
   * Get strPaymentAccountOwnerName
   * @return strPaymentAccountOwnerName
  **/
  @ApiModelProperty(example = "abcdef", value = "")


  public String getStrPaymentAccountOwnerName() {
    return strPaymentAccountOwnerName;
  }

  public void setStrPaymentAccountOwnerName(String strPaymentAccountOwnerName) {
    this.strPaymentAccountOwnerName = strPaymentAccountOwnerName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSCreateCaptureRecordDataInWSInput wsCreateCaptureRecordDataInWSInput = (WSCreateCaptureRecordDataInWSInput) o;
    return Objects.equals(this.strBatchNumber, wsCreateCaptureRecordDataInWSInput.strBatchNumber) &&
        Objects.equals(this.strPolicyNumber, wsCreateCaptureRecordDataInWSInput.strPolicyNumber) &&
        Objects.equals(this.dtDocumentScannedDate, wsCreateCaptureRecordDataInWSInput.dtDocumentScannedDate) &&
        Objects.equals(this.strOwnerIdType, wsCreateCaptureRecordDataInWSInput.strOwnerIdType) &&
        Objects.equals(this.strOwnerIdNumber, wsCreateCaptureRecordDataInWSInput.strOwnerIdNumber) &&
        Objects.equals(this.strPayeeName, wsCreateCaptureRecordDataInWSInput.strPayeeName) &&
        Objects.equals(this.strInsuredClientName, wsCreateCaptureRecordDataInWSInput.strInsuredClientName) &&
        Objects.equals(this.strInsuredIdType, wsCreateCaptureRecordDataInWSInput.strInsuredIdType) &&
        Objects.equals(this.strInsuredIdNumber, wsCreateCaptureRecordDataInWSInput.strInsuredIdNumber) &&
        Objects.equals(this.strFormClaimType, wsCreateCaptureRecordDataInWSInput.strFormClaimType) &&
        Objects.equals(this.strFormBenefitType, wsCreateCaptureRecordDataInWSInput.strFormBenefitType) &&
        Objects.equals(this.strSurgeryPerformed, wsCreateCaptureRecordDataInWSInput.strSurgeryPerformed) &&
        Objects.equals(this.dtTreatmentStartDate, wsCreateCaptureRecordDataInWSInput.dtTreatmentStartDate) &&
        Objects.equals(this.dtTreatmentCompletionDate, wsCreateCaptureRecordDataInWSInput.dtTreatmentCompletionDate) &&
        Objects.equals(this.strHospitalName, wsCreateCaptureRecordDataInWSInput.strHospitalName) &&
        Objects.equals(this.strFormCurrency, wsCreateCaptureRecordDataInWSInput.strFormCurrency) &&
        Objects.equals(this.decTotalBillingAmount, wsCreateCaptureRecordDataInWSInput.decTotalBillingAmount) &&
        Objects.equals(this.strPhysician, wsCreateCaptureRecordDataInWSInput.strPhysician) &&
        Objects.equals(this.strBankAccountNumber, wsCreateCaptureRecordDataInWSInput.strBankAccountNumber) &&
        Objects.equals(this.strPaymentAccountOwnerName, wsCreateCaptureRecordDataInWSInput.strPaymentAccountOwnerName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strBatchNumber, strPolicyNumber, dtDocumentScannedDate, strOwnerIdType, strOwnerIdNumber, strPayeeName, strInsuredClientName, strInsuredIdType, strInsuredIdNumber, strFormClaimType, strFormBenefitType, strSurgeryPerformed, dtTreatmentStartDate, dtTreatmentCompletionDate, strHospitalName, strFormCurrency, decTotalBillingAmount, strPhysician, strBankAccountNumber, strPaymentAccountOwnerName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSCreateCaptureRecordDataInWSInput {\n");
    
    sb.append("    strBatchNumber: ").append(toIndentedString(strBatchNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    dtDocumentScannedDate: ").append(toIndentedString(dtDocumentScannedDate)).append("\n");
    sb.append("    strOwnerIdType: ").append(toIndentedString(strOwnerIdType)).append("\n");
    sb.append("    strOwnerIdNumber: ").append(toIndentedString(strOwnerIdNumber)).append("\n");
    sb.append("    strPayeeName: ").append(toIndentedString(strPayeeName)).append("\n");
    sb.append("    strInsuredClientName: ").append(toIndentedString(strInsuredClientName)).append("\n");
    sb.append("    strInsuredIdType: ").append(toIndentedString(strInsuredIdType)).append("\n");
    sb.append("    strInsuredIdNumber: ").append(toIndentedString(strInsuredIdNumber)).append("\n");
    sb.append("    strFormClaimType: ").append(toIndentedString(strFormClaimType)).append("\n");
    sb.append("    strFormBenefitType: ").append(toIndentedString(strFormBenefitType)).append("\n");
    sb.append("    strSurgeryPerformed: ").append(toIndentedString(strSurgeryPerformed)).append("\n");
    sb.append("    dtTreatmentStartDate: ").append(toIndentedString(dtTreatmentStartDate)).append("\n");
    sb.append("    dtTreatmentCompletionDate: ").append(toIndentedString(dtTreatmentCompletionDate)).append("\n");
    sb.append("    strHospitalName: ").append(toIndentedString(strHospitalName)).append("\n");
    sb.append("    strFormCurrency: ").append(toIndentedString(strFormCurrency)).append("\n");
    sb.append("    decTotalBillingAmount: ").append(toIndentedString(decTotalBillingAmount)).append("\n");
    sb.append("    strPhysician: ").append(toIndentedString(strPhysician)).append("\n");
    sb.append("    strBankAccountNumber: ").append(toIndentedString(strBankAccountNumber)).append("\n");
    sb.append("    strPaymentAccountOwnerName: ").append(toIndentedString(strPaymentAccountOwnerName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

