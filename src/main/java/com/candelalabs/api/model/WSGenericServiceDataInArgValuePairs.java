package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGenericServiceDataInArgValuePairs
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGenericServiceDataInArgValuePairs   {
  @JsonProperty("index")
  private Integer index = null;

  @JsonProperty("value")
  private Object value = null;

  @JsonProperty("typeParam")
  private String typeParam = null;

  public WSGenericServiceDataInArgValuePairs index(Integer index) {
    this.index = index;
    return this;
  }

  /**
   * Get index
   * @return index
  **/
  @ApiModelProperty(value = "")


  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }

  public WSGenericServiceDataInArgValuePairs value(Object value) {
    this.value = value;
    return this;
  }

  /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(value = "")


  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  public WSGenericServiceDataInArgValuePairs typeParam(String typeParam) {
    this.typeParam = typeParam;
    return this;
  }

  /**
   * Get typeParam
   * @return typeParam
  **/
  @ApiModelProperty(value = "")


  public String getTypeParam() {
    return typeParam;
  }

  public void setTypeParam(String typeParam) {
    this.typeParam = typeParam;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGenericServiceDataInArgValuePairs wsGenericServiceDataInArgValuePairs = (WSGenericServiceDataInArgValuePairs) o;
    return Objects.equals(this.index, wsGenericServiceDataInArgValuePairs.index) &&
        Objects.equals(this.value, wsGenericServiceDataInArgValuePairs.value) &&
        Objects.equals(this.typeParam, wsGenericServiceDataInArgValuePairs.typeParam);
  }

  @Override
  public int hashCode() {
    return Objects.hash(index, value, typeParam);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGenericServiceDataInArgValuePairs {\n");
    
    sb.append("    index: ").append(toIndentedString(index)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    typeParam: ").append(toIndentedString(typeParam)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

