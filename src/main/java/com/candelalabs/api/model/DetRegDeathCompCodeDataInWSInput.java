package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DetRegDeathCompCodeDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DetRegDeathCompCodeDataInWSInput   {
  @JsonProperty("claimNumber")
  private String claimNumber = null;

  @JsonProperty("clientNumber")
  private String clientNumber = null;

  @JsonProperty("componentStatus")
  private String componentStatus = null;

  public DetRegDeathCompCodeDataInWSInput claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public DetRegDeathCompCodeDataInWSInput clientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
    return this;
  }

  /**
   * Get clientNumber
   * @return clientNumber
  **/
  @ApiModelProperty(value = "")


  public String getClientNumber() {
    return clientNumber;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public DetRegDeathCompCodeDataInWSInput componentStatus(String componentStatus) {
    this.componentStatus = componentStatus;
    return this;
  }

  /**
   * Get componentStatus
   * @return componentStatus
  **/
  @ApiModelProperty(value = "")


  public String getComponentStatus() {
    return componentStatus;
  }

  public void setComponentStatus(String componentStatus) {
    this.componentStatus = componentStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DetRegDeathCompCodeDataInWSInput detRegDeathCompCodeDataInWSInput = (DetRegDeathCompCodeDataInWSInput) o;
    return Objects.equals(this.claimNumber, detRegDeathCompCodeDataInWSInput.claimNumber) &&
        Objects.equals(this.clientNumber, detRegDeathCompCodeDataInWSInput.clientNumber) &&
        Objects.equals(this.componentStatus, detRegDeathCompCodeDataInWSInput.componentStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNumber, clientNumber, componentStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DetRegDeathCompCodeDataInWSInput {\n");
    
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    clientNumber: ").append(toIndentedString(clientNumber)).append("\n");
    sb.append("    componentStatus: ").append(toIndentedString(componentStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

