package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.WSGenericServiceDataInArgValuePairs;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGenericServiceDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGenericServiceDataIn   {
  @JsonProperty("sqlQuery")
  private String sqlQuery = null;

  @JsonProperty("argValuePairs")
  @Valid
  private List<WSGenericServiceDataInArgValuePairs> argValuePairs = null;

  public WSGenericServiceDataIn sqlQuery(String sqlQuery) {
    this.sqlQuery = sqlQuery;
    return this;
  }

  /**
   * Get sqlQuery
   * @return sqlQuery
  **/
  @ApiModelProperty(value = "")


  public String getSqlQuery() {
    return sqlQuery;
  }

  public void setSqlQuery(String sqlQuery) {
    this.sqlQuery = sqlQuery;
  }

  public WSGenericServiceDataIn argValuePairs(List<WSGenericServiceDataInArgValuePairs> argValuePairs) {
    this.argValuePairs = argValuePairs;
    return this;
  }

  public WSGenericServiceDataIn addArgValuePairsItem(WSGenericServiceDataInArgValuePairs argValuePairsItem) {
    if (this.argValuePairs == null) {
      this.argValuePairs = new ArrayList<>();
    }
    this.argValuePairs.add(argValuePairsItem);
    return this;
  }

  /**
   * Get argValuePairs
   * @return argValuePairs
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<WSGenericServiceDataInArgValuePairs> getArgValuePairs() {
    return argValuePairs;
  }

  public void setArgValuePairs(List<WSGenericServiceDataInArgValuePairs> argValuePairs) {
    this.argValuePairs = argValuePairs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGenericServiceDataIn wsGenericServiceDataIn = (WSGenericServiceDataIn) o;
    return Objects.equals(this.sqlQuery, wsGenericServiceDataIn.sqlQuery) &&
        Objects.equals(this.argValuePairs, wsGenericServiceDataIn.argValuePairs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sqlQuery, argValuePairs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGenericServiceDataIn {\n");
    
    sb.append("    sqlQuery: ").append(toIndentedString(sqlQuery)).append("\n");
    sb.append("    argValuePairs: ").append(toIndentedString(argValuePairs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

