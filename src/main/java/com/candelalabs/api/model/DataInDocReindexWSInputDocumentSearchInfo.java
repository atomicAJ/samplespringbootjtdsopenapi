package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInDocReindexWSInputDocumentSearchInfo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInDocReindexWSInputDocumentSearchInfo   {
  @JsonProperty("ClaimNumber")
  private String claimNumber = null;

  @JsonProperty("PolicyNumber")
  private String policyNumber = null;

  @JsonProperty("TPAClaimNo")
  private String tpAClaimNo = null;

  @JsonProperty("BatchNumber")
  private String batchNumber = null;

  @JsonProperty("PortalClaimRequestNumber")
  private String portalClaimRequestNumber = null;

  public DataInDocReindexWSInputDocumentSearchInfo claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public DataInDocReindexWSInputDocumentSearchInfo policyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
    return this;
  }

  /**
   * Get policyNumber
   * @return policyNumber
  **/
  @ApiModelProperty(value = "")


  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public DataInDocReindexWSInputDocumentSearchInfo tpAClaimNo(String tpAClaimNo) {
    this.tpAClaimNo = tpAClaimNo;
    return this;
  }

  /**
   * Get tpAClaimNo
   * @return tpAClaimNo
  **/
  @ApiModelProperty(value = "")


  public String getTpAClaimNo() {
    return tpAClaimNo;
  }

  public void setTpAClaimNo(String tpAClaimNo) {
    this.tpAClaimNo = tpAClaimNo;
  }

  public DataInDocReindexWSInputDocumentSearchInfo batchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
    return this;
  }

  /**
   * Get batchNumber
   * @return batchNumber
  **/
  @ApiModelProperty(value = "")


  public String getBatchNumber() {
    return batchNumber;
  }

  public void setBatchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
  }

  public DataInDocReindexWSInputDocumentSearchInfo portalClaimRequestNumber(String portalClaimRequestNumber) {
    this.portalClaimRequestNumber = portalClaimRequestNumber;
    return this;
  }

  /**
   * Get portalClaimRequestNumber
   * @return portalClaimRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getPortalClaimRequestNumber() {
    return portalClaimRequestNumber;
  }

  public void setPortalClaimRequestNumber(String portalClaimRequestNumber) {
    this.portalClaimRequestNumber = portalClaimRequestNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInDocReindexWSInputDocumentSearchInfo dataInDocReindexWSInputDocumentSearchInfo = (DataInDocReindexWSInputDocumentSearchInfo) o;
    return Objects.equals(this.claimNumber, dataInDocReindexWSInputDocumentSearchInfo.claimNumber) &&
        Objects.equals(this.policyNumber, dataInDocReindexWSInputDocumentSearchInfo.policyNumber) &&
        Objects.equals(this.tpAClaimNo, dataInDocReindexWSInputDocumentSearchInfo.tpAClaimNo) &&
        Objects.equals(this.batchNumber, dataInDocReindexWSInputDocumentSearchInfo.batchNumber) &&
        Objects.equals(this.portalClaimRequestNumber, dataInDocReindexWSInputDocumentSearchInfo.portalClaimRequestNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNumber, policyNumber, tpAClaimNo, batchNumber, portalClaimRequestNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInDocReindexWSInputDocumentSearchInfo {\n");
    
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    policyNumber: ").append(toIndentedString(policyNumber)).append("\n");
    sb.append("    tpAClaimNo: ").append(toIndentedString(tpAClaimNo)).append("\n");
    sb.append("    batchNumber: ").append(toIndentedString(batchNumber)).append("\n");
    sb.append("    portalClaimRequestNumber: ").append(toIndentedString(portalClaimRequestNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

