package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ArgValuePairs
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ArgValuePairs   {
  @JsonProperty("index")
  private String index = null;

  @JsonProperty("typeParam")
  private String typeParam = null;

  @JsonProperty("value")
  private String value = null;

  public ArgValuePairs index(String index) {
    this.index = index;
    return this;
  }

  /**
   * Get index
   * @return index
  **/
  @ApiModelProperty(value = "")


  public String getIndex() {
    return index;
  }

  public void setIndex(String index) {
    this.index = index;
  }

  public ArgValuePairs typeParam(String typeParam) {
    this.typeParam = typeParam;
    return this;
  }

  /**
   * Get typeParam
   * @return typeParam
  **/
  @ApiModelProperty(value = "")


  public String getTypeParam() {
    return typeParam;
  }

  public void setTypeParam(String typeParam) {
    this.typeParam = typeParam;
  }

  public ArgValuePairs value(String value) {
    this.value = value;
    return this;
  }

  /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(value = "")


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ArgValuePairs argValuePairs = (ArgValuePairs) o;
    return Objects.equals(this.index, argValuePairs.index) &&
        Objects.equals(this.typeParam, argValuePairs.typeParam) &&
        Objects.equals(this.value, argValuePairs.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(index, typeParam, value);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ArgValuePairs {\n");
    
    sb.append("    index: ").append(toIndentedString(index)).append("\n");
    sb.append("    typeParam: ").append(toIndentedString(typeParam)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

