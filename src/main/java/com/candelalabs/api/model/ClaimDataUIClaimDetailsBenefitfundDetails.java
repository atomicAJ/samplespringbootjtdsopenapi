package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDataUIClaimDetailsBenefitfundDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDataUIClaimDetailsBenefitfundDetails   {
  @JsonProperty("benefitCode")
  private String benefitCode = null;

  @JsonProperty("benefitName")
  private String benefitName = null;

  @JsonProperty("benefitnoOfDaysClaimed")
  private Integer benefitnoOfDaysClaimed = null;

  @JsonProperty("benefittotalAmountClaimedAsOfNow")
  private BigDecimal benefittotalAmountClaimedAsOfNow = null;

  @JsonProperty("benefitTotalAvailableAmountForClaim")
  private BigDecimal benefitTotalAvailableAmountForClaim = null;

  @JsonProperty("benefitdailyIncurred")
  private BigDecimal benefitdailyIncurred = null;

  @JsonProperty("benefitDateFrom")
  private BigDecimal benefitDateFrom = null;

  @JsonProperty("benefitDateTo")
  private BigDecimal benefitDateTo = null;

  @JsonProperty("benefitNoOfDays")
  private BigDecimal benefitNoOfDays = null;

  @JsonProperty("benefitAmountIncurred")
  private BigDecimal benefitAmountIncurred = null;

  @JsonProperty("benefitApprovedAmount")
  private BigDecimal benefitApprovedAmount = null;

  @JsonProperty("benefitAactualPayableByClaimUser")
  private BigDecimal benefitAactualPayableByClaimUser = null;

  @JsonProperty("benefitPercentage")
  private BigDecimal benefitPercentage = null;

  @JsonProperty("benefitAnnualTotalDays")
  private BigDecimal benefitAnnualTotalDays = null;

  @JsonProperty("benefitNotApprovedAmount")
  private BigDecimal benefitNotApprovedAmount = null;

  @JsonProperty("benefitMaxAmountPerDay")
  private BigDecimal benefitMaxAmountPerDay = null;

  @JsonProperty("benefitTotalIncurredAmount")
  private BigDecimal benefitTotalIncurredAmount = null;

  @JsonProperty("benefitNoOfUnits")
  private BigDecimal benefitNoOfUnits = null;

  @JsonProperty("fundComponentCode")
  private String fundComponentCode = null;

  @JsonProperty("fundComponentName")
  private String fundComponentName = null;

  @JsonProperty("fundCode")
  private String fundCode = null;

  @JsonProperty("fundDescription")
  private String fundDescription = null;

  @JsonProperty("fundEstimatedAmount")
  private BigDecimal fundEstimatedAmount = null;

  @JsonProperty("fundActualAmount")
  private BigDecimal fundActualAmount = null;

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitCode(String benefitCode) {
    this.benefitCode = benefitCode;
    return this;
  }

  /**
   * Get benefitCode
   * @return benefitCode
  **/
  @ApiModelProperty(value = "")


  public String getBenefitCode() {
    return benefitCode;
  }

  public void setBenefitCode(String benefitCode) {
    this.benefitCode = benefitCode;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitName(String benefitName) {
    this.benefitName = benefitName;
    return this;
  }

  /**
   * Get benefitName
   * @return benefitName
  **/
  @ApiModelProperty(value = "")


  public String getBenefitName() {
    return benefitName;
  }

  public void setBenefitName(String benefitName) {
    this.benefitName = benefitName;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitnoOfDaysClaimed(Integer benefitnoOfDaysClaimed) {
    this.benefitnoOfDaysClaimed = benefitnoOfDaysClaimed;
    return this;
  }

  /**
   * Get benefitnoOfDaysClaimed
   * @return benefitnoOfDaysClaimed
  **/
  @ApiModelProperty(value = "")


  public Integer getBenefitnoOfDaysClaimed() {
    return benefitnoOfDaysClaimed;
  }

  public void setBenefitnoOfDaysClaimed(Integer benefitnoOfDaysClaimed) {
    this.benefitnoOfDaysClaimed = benefitnoOfDaysClaimed;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefittotalAmountClaimedAsOfNow(BigDecimal benefittotalAmountClaimedAsOfNow) {
    this.benefittotalAmountClaimedAsOfNow = benefittotalAmountClaimedAsOfNow;
    return this;
  }

  /**
   * Get benefittotalAmountClaimedAsOfNow
   * @return benefittotalAmountClaimedAsOfNow
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefittotalAmountClaimedAsOfNow() {
    return benefittotalAmountClaimedAsOfNow;
  }

  public void setBenefittotalAmountClaimedAsOfNow(BigDecimal benefittotalAmountClaimedAsOfNow) {
    this.benefittotalAmountClaimedAsOfNow = benefittotalAmountClaimedAsOfNow;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitTotalAvailableAmountForClaim(BigDecimal benefitTotalAvailableAmountForClaim) {
    this.benefitTotalAvailableAmountForClaim = benefitTotalAvailableAmountForClaim;
    return this;
  }

  /**
   * Get benefitTotalAvailableAmountForClaim
   * @return benefitTotalAvailableAmountForClaim
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitTotalAvailableAmountForClaim() {
    return benefitTotalAvailableAmountForClaim;
  }

  public void setBenefitTotalAvailableAmountForClaim(BigDecimal benefitTotalAvailableAmountForClaim) {
    this.benefitTotalAvailableAmountForClaim = benefitTotalAvailableAmountForClaim;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitdailyIncurred(BigDecimal benefitdailyIncurred) {
    this.benefitdailyIncurred = benefitdailyIncurred;
    return this;
  }

  /**
   * Get benefitdailyIncurred
   * @return benefitdailyIncurred
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitdailyIncurred() {
    return benefitdailyIncurred;
  }

  public void setBenefitdailyIncurred(BigDecimal benefitdailyIncurred) {
    this.benefitdailyIncurred = benefitdailyIncurred;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitDateFrom(BigDecimal benefitDateFrom) {
    this.benefitDateFrom = benefitDateFrom;
    return this;
  }

  /**
   * Get benefitDateFrom
   * @return benefitDateFrom
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitDateFrom() {
    return benefitDateFrom;
  }

  public void setBenefitDateFrom(BigDecimal benefitDateFrom) {
    this.benefitDateFrom = benefitDateFrom;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitDateTo(BigDecimal benefitDateTo) {
    this.benefitDateTo = benefitDateTo;
    return this;
  }

  /**
   * Get benefitDateTo
   * @return benefitDateTo
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitDateTo() {
    return benefitDateTo;
  }

  public void setBenefitDateTo(BigDecimal benefitDateTo) {
    this.benefitDateTo = benefitDateTo;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitNoOfDays(BigDecimal benefitNoOfDays) {
    this.benefitNoOfDays = benefitNoOfDays;
    return this;
  }

  /**
   * Get benefitNoOfDays
   * @return benefitNoOfDays
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitNoOfDays() {
    return benefitNoOfDays;
  }

  public void setBenefitNoOfDays(BigDecimal benefitNoOfDays) {
    this.benefitNoOfDays = benefitNoOfDays;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitAmountIncurred(BigDecimal benefitAmountIncurred) {
    this.benefitAmountIncurred = benefitAmountIncurred;
    return this;
  }

  /**
   * Get benefitAmountIncurred
   * @return benefitAmountIncurred
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitAmountIncurred() {
    return benefitAmountIncurred;
  }

  public void setBenefitAmountIncurred(BigDecimal benefitAmountIncurred) {
    this.benefitAmountIncurred = benefitAmountIncurred;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitApprovedAmount(BigDecimal benefitApprovedAmount) {
    this.benefitApprovedAmount = benefitApprovedAmount;
    return this;
  }

  /**
   * Get benefitApprovedAmount
   * @return benefitApprovedAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitApprovedAmount() {
    return benefitApprovedAmount;
  }

  public void setBenefitApprovedAmount(BigDecimal benefitApprovedAmount) {
    this.benefitApprovedAmount = benefitApprovedAmount;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitAactualPayableByClaimUser(BigDecimal benefitAactualPayableByClaimUser) {
    this.benefitAactualPayableByClaimUser = benefitAactualPayableByClaimUser;
    return this;
  }

  /**
   * Get benefitAactualPayableByClaimUser
   * @return benefitAactualPayableByClaimUser
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitAactualPayableByClaimUser() {
    return benefitAactualPayableByClaimUser;
  }

  public void setBenefitAactualPayableByClaimUser(BigDecimal benefitAactualPayableByClaimUser) {
    this.benefitAactualPayableByClaimUser = benefitAactualPayableByClaimUser;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitPercentage(BigDecimal benefitPercentage) {
    this.benefitPercentage = benefitPercentage;
    return this;
  }

  /**
   * Get benefitPercentage
   * @return benefitPercentage
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitPercentage() {
    return benefitPercentage;
  }

  public void setBenefitPercentage(BigDecimal benefitPercentage) {
    this.benefitPercentage = benefitPercentage;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitAnnualTotalDays(BigDecimal benefitAnnualTotalDays) {
    this.benefitAnnualTotalDays = benefitAnnualTotalDays;
    return this;
  }

  /**
   * Get benefitAnnualTotalDays
   * @return benefitAnnualTotalDays
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitAnnualTotalDays() {
    return benefitAnnualTotalDays;
  }

  public void setBenefitAnnualTotalDays(BigDecimal benefitAnnualTotalDays) {
    this.benefitAnnualTotalDays = benefitAnnualTotalDays;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitNotApprovedAmount(BigDecimal benefitNotApprovedAmount) {
    this.benefitNotApprovedAmount = benefitNotApprovedAmount;
    return this;
  }

  /**
   * Get benefitNotApprovedAmount
   * @return benefitNotApprovedAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitNotApprovedAmount() {
    return benefitNotApprovedAmount;
  }

  public void setBenefitNotApprovedAmount(BigDecimal benefitNotApprovedAmount) {
    this.benefitNotApprovedAmount = benefitNotApprovedAmount;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitMaxAmountPerDay(BigDecimal benefitMaxAmountPerDay) {
    this.benefitMaxAmountPerDay = benefitMaxAmountPerDay;
    return this;
  }

  /**
   * Get benefitMaxAmountPerDay
   * @return benefitMaxAmountPerDay
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitMaxAmountPerDay() {
    return benefitMaxAmountPerDay;
  }

  public void setBenefitMaxAmountPerDay(BigDecimal benefitMaxAmountPerDay) {
    this.benefitMaxAmountPerDay = benefitMaxAmountPerDay;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitTotalIncurredAmount(BigDecimal benefitTotalIncurredAmount) {
    this.benefitTotalIncurredAmount = benefitTotalIncurredAmount;
    return this;
  }

  /**
   * Get benefitTotalIncurredAmount
   * @return benefitTotalIncurredAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitTotalIncurredAmount() {
    return benefitTotalIncurredAmount;
  }

  public void setBenefitTotalIncurredAmount(BigDecimal benefitTotalIncurredAmount) {
    this.benefitTotalIncurredAmount = benefitTotalIncurredAmount;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails benefitNoOfUnits(BigDecimal benefitNoOfUnits) {
    this.benefitNoOfUnits = benefitNoOfUnits;
    return this;
  }

  /**
   * Get benefitNoOfUnits
   * @return benefitNoOfUnits
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBenefitNoOfUnits() {
    return benefitNoOfUnits;
  }

  public void setBenefitNoOfUnits(BigDecimal benefitNoOfUnits) {
    this.benefitNoOfUnits = benefitNoOfUnits;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails fundComponentCode(String fundComponentCode) {
    this.fundComponentCode = fundComponentCode;
    return this;
  }

  /**
   * Get fundComponentCode
   * @return fundComponentCode
  **/
  @ApiModelProperty(value = "")


  public String getFundComponentCode() {
    return fundComponentCode;
  }

  public void setFundComponentCode(String fundComponentCode) {
    this.fundComponentCode = fundComponentCode;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails fundComponentName(String fundComponentName) {
    this.fundComponentName = fundComponentName;
    return this;
  }

  /**
   * Get fundComponentName
   * @return fundComponentName
  **/
  @ApiModelProperty(value = "")


  public String getFundComponentName() {
    return fundComponentName;
  }

  public void setFundComponentName(String fundComponentName) {
    this.fundComponentName = fundComponentName;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails fundCode(String fundCode) {
    this.fundCode = fundCode;
    return this;
  }

  /**
   * Get fundCode
   * @return fundCode
  **/
  @ApiModelProperty(value = "")


  public String getFundCode() {
    return fundCode;
  }

  public void setFundCode(String fundCode) {
    this.fundCode = fundCode;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails fundDescription(String fundDescription) {
    this.fundDescription = fundDescription;
    return this;
  }

  /**
   * Get fundDescription
   * @return fundDescription
  **/
  @ApiModelProperty(value = "")


  public String getFundDescription() {
    return fundDescription;
  }

  public void setFundDescription(String fundDescription) {
    this.fundDescription = fundDescription;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails fundEstimatedAmount(BigDecimal fundEstimatedAmount) {
    this.fundEstimatedAmount = fundEstimatedAmount;
    return this;
  }

  /**
   * Get fundEstimatedAmount
   * @return fundEstimatedAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getFundEstimatedAmount() {
    return fundEstimatedAmount;
  }

  public void setFundEstimatedAmount(BigDecimal fundEstimatedAmount) {
    this.fundEstimatedAmount = fundEstimatedAmount;
  }

  public ClaimDataUIClaimDetailsBenefitfundDetails fundActualAmount(BigDecimal fundActualAmount) {
    this.fundActualAmount = fundActualAmount;
    return this;
  }

  /**
   * Get fundActualAmount
   * @return fundActualAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getFundActualAmount() {
    return fundActualAmount;
  }

  public void setFundActualAmount(BigDecimal fundActualAmount) {
    this.fundActualAmount = fundActualAmount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDataUIClaimDetailsBenefitfundDetails claimDataUIClaimDetailsBenefitfundDetails = (ClaimDataUIClaimDetailsBenefitfundDetails) o;
    return Objects.equals(this.benefitCode, claimDataUIClaimDetailsBenefitfundDetails.benefitCode) &&
        Objects.equals(this.benefitName, claimDataUIClaimDetailsBenefitfundDetails.benefitName) &&
        Objects.equals(this.benefitnoOfDaysClaimed, claimDataUIClaimDetailsBenefitfundDetails.benefitnoOfDaysClaimed) &&
        Objects.equals(this.benefittotalAmountClaimedAsOfNow, claimDataUIClaimDetailsBenefitfundDetails.benefittotalAmountClaimedAsOfNow) &&
        Objects.equals(this.benefitTotalAvailableAmountForClaim, claimDataUIClaimDetailsBenefitfundDetails.benefitTotalAvailableAmountForClaim) &&
        Objects.equals(this.benefitdailyIncurred, claimDataUIClaimDetailsBenefitfundDetails.benefitdailyIncurred) &&
        Objects.equals(this.benefitDateFrom, claimDataUIClaimDetailsBenefitfundDetails.benefitDateFrom) &&
        Objects.equals(this.benefitDateTo, claimDataUIClaimDetailsBenefitfundDetails.benefitDateTo) &&
        Objects.equals(this.benefitNoOfDays, claimDataUIClaimDetailsBenefitfundDetails.benefitNoOfDays) &&
        Objects.equals(this.benefitAmountIncurred, claimDataUIClaimDetailsBenefitfundDetails.benefitAmountIncurred) &&
        Objects.equals(this.benefitApprovedAmount, claimDataUIClaimDetailsBenefitfundDetails.benefitApprovedAmount) &&
        Objects.equals(this.benefitAactualPayableByClaimUser, claimDataUIClaimDetailsBenefitfundDetails.benefitAactualPayableByClaimUser) &&
        Objects.equals(this.benefitPercentage, claimDataUIClaimDetailsBenefitfundDetails.benefitPercentage) &&
        Objects.equals(this.benefitAnnualTotalDays, claimDataUIClaimDetailsBenefitfundDetails.benefitAnnualTotalDays) &&
        Objects.equals(this.benefitNotApprovedAmount, claimDataUIClaimDetailsBenefitfundDetails.benefitNotApprovedAmount) &&
        Objects.equals(this.benefitMaxAmountPerDay, claimDataUIClaimDetailsBenefitfundDetails.benefitMaxAmountPerDay) &&
        Objects.equals(this.benefitTotalIncurredAmount, claimDataUIClaimDetailsBenefitfundDetails.benefitTotalIncurredAmount) &&
        Objects.equals(this.benefitNoOfUnits, claimDataUIClaimDetailsBenefitfundDetails.benefitNoOfUnits) &&
        Objects.equals(this.fundComponentCode, claimDataUIClaimDetailsBenefitfundDetails.fundComponentCode) &&
        Objects.equals(this.fundComponentName, claimDataUIClaimDetailsBenefitfundDetails.fundComponentName) &&
        Objects.equals(this.fundCode, claimDataUIClaimDetailsBenefitfundDetails.fundCode) &&
        Objects.equals(this.fundDescription, claimDataUIClaimDetailsBenefitfundDetails.fundDescription) &&
        Objects.equals(this.fundEstimatedAmount, claimDataUIClaimDetailsBenefitfundDetails.fundEstimatedAmount) &&
        Objects.equals(this.fundActualAmount, claimDataUIClaimDetailsBenefitfundDetails.fundActualAmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(benefitCode, benefitName, benefitnoOfDaysClaimed, benefittotalAmountClaimedAsOfNow, benefitTotalAvailableAmountForClaim, benefitdailyIncurred, benefitDateFrom, benefitDateTo, benefitNoOfDays, benefitAmountIncurred, benefitApprovedAmount, benefitAactualPayableByClaimUser, benefitPercentage, benefitAnnualTotalDays, benefitNotApprovedAmount, benefitMaxAmountPerDay, benefitTotalIncurredAmount, benefitNoOfUnits, fundComponentCode, fundComponentName, fundCode, fundDescription, fundEstimatedAmount, fundActualAmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDataUIClaimDetailsBenefitfundDetails {\n");
    
    sb.append("    benefitCode: ").append(toIndentedString(benefitCode)).append("\n");
    sb.append("    benefitName: ").append(toIndentedString(benefitName)).append("\n");
    sb.append("    benefitnoOfDaysClaimed: ").append(toIndentedString(benefitnoOfDaysClaimed)).append("\n");
    sb.append("    benefittotalAmountClaimedAsOfNow: ").append(toIndentedString(benefittotalAmountClaimedAsOfNow)).append("\n");
    sb.append("    benefitTotalAvailableAmountForClaim: ").append(toIndentedString(benefitTotalAvailableAmountForClaim)).append("\n");
    sb.append("    benefitdailyIncurred: ").append(toIndentedString(benefitdailyIncurred)).append("\n");
    sb.append("    benefitDateFrom: ").append(toIndentedString(benefitDateFrom)).append("\n");
    sb.append("    benefitDateTo: ").append(toIndentedString(benefitDateTo)).append("\n");
    sb.append("    benefitNoOfDays: ").append(toIndentedString(benefitNoOfDays)).append("\n");
    sb.append("    benefitAmountIncurred: ").append(toIndentedString(benefitAmountIncurred)).append("\n");
    sb.append("    benefitApprovedAmount: ").append(toIndentedString(benefitApprovedAmount)).append("\n");
    sb.append("    benefitAactualPayableByClaimUser: ").append(toIndentedString(benefitAactualPayableByClaimUser)).append("\n");
    sb.append("    benefitPercentage: ").append(toIndentedString(benefitPercentage)).append("\n");
    sb.append("    benefitAnnualTotalDays: ").append(toIndentedString(benefitAnnualTotalDays)).append("\n");
    sb.append("    benefitNotApprovedAmount: ").append(toIndentedString(benefitNotApprovedAmount)).append("\n");
    sb.append("    benefitMaxAmountPerDay: ").append(toIndentedString(benefitMaxAmountPerDay)).append("\n");
    sb.append("    benefitTotalIncurredAmount: ").append(toIndentedString(benefitTotalIncurredAmount)).append("\n");
    sb.append("    benefitNoOfUnits: ").append(toIndentedString(benefitNoOfUnits)).append("\n");
    sb.append("    fundComponentCode: ").append(toIndentedString(fundComponentCode)).append("\n");
    sb.append("    fundComponentName: ").append(toIndentedString(fundComponentName)).append("\n");
    sb.append("    fundCode: ").append(toIndentedString(fundCode)).append("\n");
    sb.append("    fundDescription: ").append(toIndentedString(fundDescription)).append("\n");
    sb.append("    fundEstimatedAmount: ").append(toIndentedString(fundEstimatedAmount)).append("\n");
    sb.append("    fundActualAmount: ").append(toIndentedString(fundActualAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

