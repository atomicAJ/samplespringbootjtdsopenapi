package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsAdditionalRequirement;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsBenefitfundDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsWSResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDataUIClaimDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDataUIClaimDetails   {
  @JsonProperty("claimNumber")
  private String claimNumber = null;

  @JsonProperty("claimPolicyNumber")
  private String claimPolicyNumber = null;

  @JsonProperty("claimProductName")
  private String claimProductName = null;

  @JsonProperty("claimTypeLA")
  private String claimTypeLA = null;

  @JsonProperty("claimPlanCode")
  private String claimPlanCode = null;

  @JsonProperty("claimReceivedDate")
  private BigDecimal claimReceivedDate = null;

  @JsonProperty("claimRegisterDate")
  private BigDecimal claimRegisterDate = null;

  @JsonProperty("claimInsuredName")
  private String claimInsuredName = null;

  @JsonProperty("claimInsuredClientID")
  private String claimInsuredClientID = null;

  @JsonProperty("claimDiagnoseCode")
  private String claimDiagnoseCode = null;

  @JsonProperty("claimDiagnoseName")
  private String claimDiagnoseName = null;

  @JsonProperty("claimAdmissionDate")
  private BigDecimal claimAdmissionDate = null;

  @JsonProperty("claimDischargeDate")
  private BigDecimal claimDischargeDate = null;

  @JsonProperty("claimProvider")
  private String claimProvider = null;

  @JsonProperty("claimProviderSource")
  private String claimProviderSource = null;

  @JsonProperty("claimDoctorName")
  private String claimDoctorName = null;

  @JsonProperty("claimInvoiceAmount")
  private BigDecimal claimInvoiceAmount = null;

  @JsonProperty("claimInvoiceAmountUI")
  private BigDecimal claimInvoiceAmountUI = null;

  @JsonProperty("claimPolicyCurrency")
  private String claimPolicyCurrency = null;

  @JsonProperty("ClaimSourcecurrency")
  private String claimSourcecurrency = null;

  @JsonProperty("ClaimDeathBenefitCategory")
  private String claimDeathBenefitCategory = null;

  @JsonProperty("claimPayeeName")
  private String claimPayeeName = null;

  @JsonProperty("claimFactoringHouse")
  private String claimFactoringHouse = null;

  @JsonProperty("claimBankCode")
  private String claimBankCode = null;

  @JsonProperty("claimBankAccountName")
  private String claimBankAccountName = null;

  @JsonProperty("claimBankAccountNumber")
  private String claimBankAccountNumber = null;

  @JsonProperty("claimBankType")
  private String claimBankType = null;

  @JsonProperty("_comment1_")
  private String comment1_ = null;

  @JsonProperty("claimSumAssuredPercent")
  private BigDecimal claimSumAssuredPercent = null;

  @JsonProperty("_comment2_")
  private String comment2_ = null;

  @JsonProperty("claimComponentName")
  private String claimComponentName = null;

  @JsonProperty("_comment3_")
  private String comment3_ = null;

  @JsonProperty("claimComponentCode")
  private String claimComponentCode = null;

  @JsonProperty("claimComponentSumAssured")
  private BigDecimal claimComponentSumAssured = null;

  @JsonProperty("claimOSCharges")
  private BigDecimal claimOSCharges = null;

  @JsonProperty("claimOtherAdjustment")
  private BigDecimal claimOtherAdjustment = null;

  @JsonProperty("claimAdjustmentCode")
  private String claimAdjustmentCode = null;

  @JsonProperty("claimPolicyLoan")
  private BigDecimal claimPolicyLoan = null;

  @JsonProperty("claimPolicyDebt")
  private BigDecimal claimPolicyDebt = null;

  @JsonProperty("claimFullyClaim")
  private String claimFullyClaim = null;

  @JsonProperty("claimUITotalAmount")
  private BigDecimal claimUITotalAmount = null;

  @JsonProperty("claimCurrentFrom")
  private BigDecimal claimCurrentFrom = null;

  @JsonProperty("claimStayDuration")
  private BigDecimal claimStayDuration = null;

  @JsonProperty("autosavetime")
  private BigDecimal autosavetime = null;

  @JsonProperty("autosaveflag")
  private String autosaveflag = null;

  @JsonProperty("claimPaymentDate")
  private BigDecimal claimPaymentDate = null;

  @JsonProperty("claimPaidToDate")
  private BigDecimal claimPaidToDate = null;

  @JsonProperty("claimCompPremCessDate")
  private BigDecimal claimCompPremCessDate = null;

  @JsonProperty("WSResponse")
  private ClaimDataUIClaimDetailsWSResponse wsResponse = null;

  @JsonProperty("additionalRequirement")
  @Valid
  private List<ClaimDataUIClaimDetailsAdditionalRequirement> additionalRequirement = null;

  @JsonProperty("benefitfundDetails")
  @Valid
  private List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitfundDetails = null;

  public ClaimDataUIClaimDetails claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public ClaimDataUIClaimDetails claimPolicyNumber(String claimPolicyNumber) {
    this.claimPolicyNumber = claimPolicyNumber;
    return this;
  }

  /**
   * Get claimPolicyNumber
   * @return claimPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimPolicyNumber() {
    return claimPolicyNumber;
  }

  public void setClaimPolicyNumber(String claimPolicyNumber) {
    this.claimPolicyNumber = claimPolicyNumber;
  }

  public ClaimDataUIClaimDetails claimProductName(String claimProductName) {
    this.claimProductName = claimProductName;
    return this;
  }

  /**
   * Get claimProductName
   * @return claimProductName
  **/
  @ApiModelProperty(value = "")


  public String getClaimProductName() {
    return claimProductName;
  }

  public void setClaimProductName(String claimProductName) {
    this.claimProductName = claimProductName;
  }

  public ClaimDataUIClaimDetails claimTypeLA(String claimTypeLA) {
    this.claimTypeLA = claimTypeLA;
    return this;
  }

  /**
   * Get claimTypeLA
   * @return claimTypeLA
  **/
  @ApiModelProperty(value = "")


  public String getClaimTypeLA() {
    return claimTypeLA;
  }

  public void setClaimTypeLA(String claimTypeLA) {
    this.claimTypeLA = claimTypeLA;
  }

  public ClaimDataUIClaimDetails claimPlanCode(String claimPlanCode) {
    this.claimPlanCode = claimPlanCode;
    return this;
  }

  /**
   * Get claimPlanCode
   * @return claimPlanCode
  **/
  @ApiModelProperty(value = "")


  public String getClaimPlanCode() {
    return claimPlanCode;
  }

  public void setClaimPlanCode(String claimPlanCode) {
    this.claimPlanCode = claimPlanCode;
  }

  public ClaimDataUIClaimDetails claimReceivedDate(BigDecimal claimReceivedDate) {
    this.claimReceivedDate = claimReceivedDate;
    return this;
  }

  /**
   * Get claimReceivedDate
   * @return claimReceivedDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimReceivedDate() {
    return claimReceivedDate;
  }

  public void setClaimReceivedDate(BigDecimal claimReceivedDate) {
    this.claimReceivedDate = claimReceivedDate;
  }

  public ClaimDataUIClaimDetails claimRegisterDate(BigDecimal claimRegisterDate) {
    this.claimRegisterDate = claimRegisterDate;
    return this;
  }

  /**
   * Get claimRegisterDate
   * @return claimRegisterDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimRegisterDate() {
    return claimRegisterDate;
  }

  public void setClaimRegisterDate(BigDecimal claimRegisterDate) {
    this.claimRegisterDate = claimRegisterDate;
  }

  public ClaimDataUIClaimDetails claimInsuredName(String claimInsuredName) {
    this.claimInsuredName = claimInsuredName;
    return this;
  }

  /**
   * Get claimInsuredName
   * @return claimInsuredName
  **/
  @ApiModelProperty(value = "")


  public String getClaimInsuredName() {
    return claimInsuredName;
  }

  public void setClaimInsuredName(String claimInsuredName) {
    this.claimInsuredName = claimInsuredName;
  }

  public ClaimDataUIClaimDetails claimInsuredClientID(String claimInsuredClientID) {
    this.claimInsuredClientID = claimInsuredClientID;
    return this;
  }

  /**
   * Get claimInsuredClientID
   * @return claimInsuredClientID
  **/
  @ApiModelProperty(value = "")


  public String getClaimInsuredClientID() {
    return claimInsuredClientID;
  }

  public void setClaimInsuredClientID(String claimInsuredClientID) {
    this.claimInsuredClientID = claimInsuredClientID;
  }

  public ClaimDataUIClaimDetails claimDiagnoseCode(String claimDiagnoseCode) {
    this.claimDiagnoseCode = claimDiagnoseCode;
    return this;
  }

  /**
   * Get claimDiagnoseCode
   * @return claimDiagnoseCode
  **/
  @ApiModelProperty(value = "")


  public String getClaimDiagnoseCode() {
    return claimDiagnoseCode;
  }

  public void setClaimDiagnoseCode(String claimDiagnoseCode) {
    this.claimDiagnoseCode = claimDiagnoseCode;
  }

  public ClaimDataUIClaimDetails claimDiagnoseName(String claimDiagnoseName) {
    this.claimDiagnoseName = claimDiagnoseName;
    return this;
  }

  /**
   * Get claimDiagnoseName
   * @return claimDiagnoseName
  **/
  @ApiModelProperty(value = "")


  public String getClaimDiagnoseName() {
    return claimDiagnoseName;
  }

  public void setClaimDiagnoseName(String claimDiagnoseName) {
    this.claimDiagnoseName = claimDiagnoseName;
  }

  public ClaimDataUIClaimDetails claimAdmissionDate(BigDecimal claimAdmissionDate) {
    this.claimAdmissionDate = claimAdmissionDate;
    return this;
  }

  /**
   * Get claimAdmissionDate
   * @return claimAdmissionDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimAdmissionDate() {
    return claimAdmissionDate;
  }

  public void setClaimAdmissionDate(BigDecimal claimAdmissionDate) {
    this.claimAdmissionDate = claimAdmissionDate;
  }

  public ClaimDataUIClaimDetails claimDischargeDate(BigDecimal claimDischargeDate) {
    this.claimDischargeDate = claimDischargeDate;
    return this;
  }

  /**
   * Get claimDischargeDate
   * @return claimDischargeDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimDischargeDate() {
    return claimDischargeDate;
  }

  public void setClaimDischargeDate(BigDecimal claimDischargeDate) {
    this.claimDischargeDate = claimDischargeDate;
  }

  public ClaimDataUIClaimDetails claimProvider(String claimProvider) {
    this.claimProvider = claimProvider;
    return this;
  }

  /**
   * Get claimProvider
   * @return claimProvider
  **/
  @ApiModelProperty(value = "")


  public String getClaimProvider() {
    return claimProvider;
  }

  public void setClaimProvider(String claimProvider) {
    this.claimProvider = claimProvider;
  }

  public ClaimDataUIClaimDetails claimProviderSource(String claimProviderSource) {
    this.claimProviderSource = claimProviderSource;
    return this;
  }

  /**
   * Get claimProviderSource
   * @return claimProviderSource
  **/
  @ApiModelProperty(value = "")


  public String getClaimProviderSource() {
    return claimProviderSource;
  }

  public void setClaimProviderSource(String claimProviderSource) {
    this.claimProviderSource = claimProviderSource;
  }

  public ClaimDataUIClaimDetails claimDoctorName(String claimDoctorName) {
    this.claimDoctorName = claimDoctorName;
    return this;
  }

  /**
   * Get claimDoctorName
   * @return claimDoctorName
  **/
  @ApiModelProperty(value = "")


  public String getClaimDoctorName() {
    return claimDoctorName;
  }

  public void setClaimDoctorName(String claimDoctorName) {
    this.claimDoctorName = claimDoctorName;
  }

  public ClaimDataUIClaimDetails claimInvoiceAmount(BigDecimal claimInvoiceAmount) {
    this.claimInvoiceAmount = claimInvoiceAmount;
    return this;
  }

  /**
   * Get claimInvoiceAmount
   * @return claimInvoiceAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimInvoiceAmount() {
    return claimInvoiceAmount;
  }

  public void setClaimInvoiceAmount(BigDecimal claimInvoiceAmount) {
    this.claimInvoiceAmount = claimInvoiceAmount;
  }

  public ClaimDataUIClaimDetails claimInvoiceAmountUI(BigDecimal claimInvoiceAmountUI) {
    this.claimInvoiceAmountUI = claimInvoiceAmountUI;
    return this;
  }

  /**
   * Get claimInvoiceAmountUI
   * @return claimInvoiceAmountUI
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimInvoiceAmountUI() {
    return claimInvoiceAmountUI;
  }

  public void setClaimInvoiceAmountUI(BigDecimal claimInvoiceAmountUI) {
    this.claimInvoiceAmountUI = claimInvoiceAmountUI;
  }

  public ClaimDataUIClaimDetails claimPolicyCurrency(String claimPolicyCurrency) {
    this.claimPolicyCurrency = claimPolicyCurrency;
    return this;
  }

  /**
   * Get claimPolicyCurrency
   * @return claimPolicyCurrency
  **/
  @ApiModelProperty(value = "")


  public String getClaimPolicyCurrency() {
    return claimPolicyCurrency;
  }

  public void setClaimPolicyCurrency(String claimPolicyCurrency) {
    this.claimPolicyCurrency = claimPolicyCurrency;
  }

  public ClaimDataUIClaimDetails claimSourcecurrency(String claimSourcecurrency) {
    this.claimSourcecurrency = claimSourcecurrency;
    return this;
  }

  /**
   * Get claimSourcecurrency
   * @return claimSourcecurrency
  **/
  @ApiModelProperty(value = "")


  public String getClaimSourcecurrency() {
    return claimSourcecurrency;
  }

  public void setClaimSourcecurrency(String claimSourcecurrency) {
    this.claimSourcecurrency = claimSourcecurrency;
  }

  public ClaimDataUIClaimDetails claimDeathBenefitCategory(String claimDeathBenefitCategory) {
    this.claimDeathBenefitCategory = claimDeathBenefitCategory;
    return this;
  }

  /**
   * Get claimDeathBenefitCategory
   * @return claimDeathBenefitCategory
  **/
  @ApiModelProperty(value = "")


  public String getClaimDeathBenefitCategory() {
    return claimDeathBenefitCategory;
  }

  public void setClaimDeathBenefitCategory(String claimDeathBenefitCategory) {
    this.claimDeathBenefitCategory = claimDeathBenefitCategory;
  }

  public ClaimDataUIClaimDetails claimPayeeName(String claimPayeeName) {
    this.claimPayeeName = claimPayeeName;
    return this;
  }

  /**
   * Get claimPayeeName
   * @return claimPayeeName
  **/
  @ApiModelProperty(value = "")


  public String getClaimPayeeName() {
    return claimPayeeName;
  }

  public void setClaimPayeeName(String claimPayeeName) {
    this.claimPayeeName = claimPayeeName;
  }

  public ClaimDataUIClaimDetails claimFactoringHouse(String claimFactoringHouse) {
    this.claimFactoringHouse = claimFactoringHouse;
    return this;
  }

  /**
   * Get claimFactoringHouse
   * @return claimFactoringHouse
  **/
  @ApiModelProperty(value = "")


  public String getClaimFactoringHouse() {
    return claimFactoringHouse;
  }

  public void setClaimFactoringHouse(String claimFactoringHouse) {
    this.claimFactoringHouse = claimFactoringHouse;
  }

  public ClaimDataUIClaimDetails claimBankCode(String claimBankCode) {
    this.claimBankCode = claimBankCode;
    return this;
  }

  /**
   * Get claimBankCode
   * @return claimBankCode
  **/
  @ApiModelProperty(value = "")


  public String getClaimBankCode() {
    return claimBankCode;
  }

  public void setClaimBankCode(String claimBankCode) {
    this.claimBankCode = claimBankCode;
  }

  public ClaimDataUIClaimDetails claimBankAccountName(String claimBankAccountName) {
    this.claimBankAccountName = claimBankAccountName;
    return this;
  }

  /**
   * Get claimBankAccountName
   * @return claimBankAccountName
  **/
  @ApiModelProperty(value = "")


  public String getClaimBankAccountName() {
    return claimBankAccountName;
  }

  public void setClaimBankAccountName(String claimBankAccountName) {
    this.claimBankAccountName = claimBankAccountName;
  }

  public ClaimDataUIClaimDetails claimBankAccountNumber(String claimBankAccountNumber) {
    this.claimBankAccountNumber = claimBankAccountNumber;
    return this;
  }

  /**
   * Get claimBankAccountNumber
   * @return claimBankAccountNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimBankAccountNumber() {
    return claimBankAccountNumber;
  }

  public void setClaimBankAccountNumber(String claimBankAccountNumber) {
    this.claimBankAccountNumber = claimBankAccountNumber;
  }

  public ClaimDataUIClaimDetails claimBankType(String claimBankType) {
    this.claimBankType = claimBankType;
    return this;
  }

  /**
   * Get claimBankType
   * @return claimBankType
  **/
  @ApiModelProperty(value = "")


  public String getClaimBankType() {
    return claimBankType;
  }

  public void setClaimBankType(String claimBankType) {
    this.claimBankType = claimBankType;
  }

  public ClaimDataUIClaimDetails comment1_(String comment1_) {
    this.comment1_ = comment1_;
    return this;
  }

  /**
   * Get comment1_
   * @return comment1_
  **/
  @ApiModelProperty(value = "")


  public String getComment1_() {
    return comment1_;
  }

  public void setComment1_(String comment1_) {
    this.comment1_ = comment1_;
  }

  public ClaimDataUIClaimDetails claimSumAssuredPercent(BigDecimal claimSumAssuredPercent) {
    this.claimSumAssuredPercent = claimSumAssuredPercent;
    return this;
  }

  /**
   * Get claimSumAssuredPercent
   * @return claimSumAssuredPercent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimSumAssuredPercent() {
    return claimSumAssuredPercent;
  }

  public void setClaimSumAssuredPercent(BigDecimal claimSumAssuredPercent) {
    this.claimSumAssuredPercent = claimSumAssuredPercent;
  }

  public ClaimDataUIClaimDetails comment2_(String comment2_) {
    this.comment2_ = comment2_;
    return this;
  }

  /**
   * Get comment2_
   * @return comment2_
  **/
  @ApiModelProperty(value = "")


  public String getComment2_() {
    return comment2_;
  }

  public void setComment2_(String comment2_) {
    this.comment2_ = comment2_;
  }

  public ClaimDataUIClaimDetails claimComponentName(String claimComponentName) {
    this.claimComponentName = claimComponentName;
    return this;
  }

  /**
   * Get claimComponentName
   * @return claimComponentName
  **/
  @ApiModelProperty(value = "")


  public String getClaimComponentName() {
    return claimComponentName;
  }

  public void setClaimComponentName(String claimComponentName) {
    this.claimComponentName = claimComponentName;
  }

  public ClaimDataUIClaimDetails comment3_(String comment3_) {
    this.comment3_ = comment3_;
    return this;
  }

  /**
   * Get comment3_
   * @return comment3_
  **/
  @ApiModelProperty(value = "")


  public String getComment3_() {
    return comment3_;
  }

  public void setComment3_(String comment3_) {
    this.comment3_ = comment3_;
  }

  public ClaimDataUIClaimDetails claimComponentCode(String claimComponentCode) {
    this.claimComponentCode = claimComponentCode;
    return this;
  }

  /**
   * Get claimComponentCode
   * @return claimComponentCode
  **/
  @ApiModelProperty(value = "")


  public String getClaimComponentCode() {
    return claimComponentCode;
  }

  public void setClaimComponentCode(String claimComponentCode) {
    this.claimComponentCode = claimComponentCode;
  }

  public ClaimDataUIClaimDetails claimComponentSumAssured(BigDecimal claimComponentSumAssured) {
    this.claimComponentSumAssured = claimComponentSumAssured;
    return this;
  }

  /**
   * Get claimComponentSumAssured
   * @return claimComponentSumAssured
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimComponentSumAssured() {
    return claimComponentSumAssured;
  }

  public void setClaimComponentSumAssured(BigDecimal claimComponentSumAssured) {
    this.claimComponentSumAssured = claimComponentSumAssured;
  }

  public ClaimDataUIClaimDetails claimOSCharges(BigDecimal claimOSCharges) {
    this.claimOSCharges = claimOSCharges;
    return this;
  }

  /**
   * Get claimOSCharges
   * @return claimOSCharges
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimOSCharges() {
    return claimOSCharges;
  }

  public void setClaimOSCharges(BigDecimal claimOSCharges) {
    this.claimOSCharges = claimOSCharges;
  }

  public ClaimDataUIClaimDetails claimOtherAdjustment(BigDecimal claimOtherAdjustment) {
    this.claimOtherAdjustment = claimOtherAdjustment;
    return this;
  }

  /**
   * Get claimOtherAdjustment
   * @return claimOtherAdjustment
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimOtherAdjustment() {
    return claimOtherAdjustment;
  }

  public void setClaimOtherAdjustment(BigDecimal claimOtherAdjustment) {
    this.claimOtherAdjustment = claimOtherAdjustment;
  }

  public ClaimDataUIClaimDetails claimAdjustmentCode(String claimAdjustmentCode) {
    this.claimAdjustmentCode = claimAdjustmentCode;
    return this;
  }

  /**
   * Get claimAdjustmentCode
   * @return claimAdjustmentCode
  **/
  @ApiModelProperty(value = "")


  public String getClaimAdjustmentCode() {
    return claimAdjustmentCode;
  }

  public void setClaimAdjustmentCode(String claimAdjustmentCode) {
    this.claimAdjustmentCode = claimAdjustmentCode;
  }

  public ClaimDataUIClaimDetails claimPolicyLoan(BigDecimal claimPolicyLoan) {
    this.claimPolicyLoan = claimPolicyLoan;
    return this;
  }

  /**
   * Get claimPolicyLoan
   * @return claimPolicyLoan
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimPolicyLoan() {
    return claimPolicyLoan;
  }

  public void setClaimPolicyLoan(BigDecimal claimPolicyLoan) {
    this.claimPolicyLoan = claimPolicyLoan;
  }

  public ClaimDataUIClaimDetails claimPolicyDebt(BigDecimal claimPolicyDebt) {
    this.claimPolicyDebt = claimPolicyDebt;
    return this;
  }

  /**
   * Get claimPolicyDebt
   * @return claimPolicyDebt
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimPolicyDebt() {
    return claimPolicyDebt;
  }

  public void setClaimPolicyDebt(BigDecimal claimPolicyDebt) {
    this.claimPolicyDebt = claimPolicyDebt;
  }

  public ClaimDataUIClaimDetails claimFullyClaim(String claimFullyClaim) {
    this.claimFullyClaim = claimFullyClaim;
    return this;
  }

  /**
   * Get claimFullyClaim
   * @return claimFullyClaim
  **/
  @ApiModelProperty(value = "")


  public String getClaimFullyClaim() {
    return claimFullyClaim;
  }

  public void setClaimFullyClaim(String claimFullyClaim) {
    this.claimFullyClaim = claimFullyClaim;
  }

  public ClaimDataUIClaimDetails claimUITotalAmount(BigDecimal claimUITotalAmount) {
    this.claimUITotalAmount = claimUITotalAmount;
    return this;
  }

  /**
   * Get claimUITotalAmount
   * @return claimUITotalAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimUITotalAmount() {
    return claimUITotalAmount;
  }

  public void setClaimUITotalAmount(BigDecimal claimUITotalAmount) {
    this.claimUITotalAmount = claimUITotalAmount;
  }

  public ClaimDataUIClaimDetails claimCurrentFrom(BigDecimal claimCurrentFrom) {
    this.claimCurrentFrom = claimCurrentFrom;
    return this;
  }

  /**
   * Get claimCurrentFrom
   * @return claimCurrentFrom
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimCurrentFrom() {
    return claimCurrentFrom;
  }

  public void setClaimCurrentFrom(BigDecimal claimCurrentFrom) {
    this.claimCurrentFrom = claimCurrentFrom;
  }

  public ClaimDataUIClaimDetails claimStayDuration(BigDecimal claimStayDuration) {
    this.claimStayDuration = claimStayDuration;
    return this;
  }

  /**
   * Get claimStayDuration
   * @return claimStayDuration
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimStayDuration() {
    return claimStayDuration;
  }

  public void setClaimStayDuration(BigDecimal claimStayDuration) {
    this.claimStayDuration = claimStayDuration;
  }

  public ClaimDataUIClaimDetails autosavetime(BigDecimal autosavetime) {
    this.autosavetime = autosavetime;
    return this;
  }

  /**
   * Get autosavetime
   * @return autosavetime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getAutosavetime() {
    return autosavetime;
  }

  public void setAutosavetime(BigDecimal autosavetime) {
    this.autosavetime = autosavetime;
  }

  public ClaimDataUIClaimDetails autosaveflag(String autosaveflag) {
    this.autosaveflag = autosaveflag;
    return this;
  }

  /**
   * Get autosaveflag
   * @return autosaveflag
  **/
  @ApiModelProperty(value = "")


  public String getAutosaveflag() {
    return autosaveflag;
  }

  public void setAutosaveflag(String autosaveflag) {
    this.autosaveflag = autosaveflag;
  }

  public ClaimDataUIClaimDetails claimPaymentDate(BigDecimal claimPaymentDate) {
    this.claimPaymentDate = claimPaymentDate;
    return this;
  }

  /**
   * Get claimPaymentDate
   * @return claimPaymentDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimPaymentDate() {
    return claimPaymentDate;
  }

  public void setClaimPaymentDate(BigDecimal claimPaymentDate) {
    this.claimPaymentDate = claimPaymentDate;
  }

  public ClaimDataUIClaimDetails claimPaidToDate(BigDecimal claimPaidToDate) {
    this.claimPaidToDate = claimPaidToDate;
    return this;
  }

  /**
   * Get claimPaidToDate
   * @return claimPaidToDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimPaidToDate() {
    return claimPaidToDate;
  }

  public void setClaimPaidToDate(BigDecimal claimPaidToDate) {
    this.claimPaidToDate = claimPaidToDate;
  }

  public ClaimDataUIClaimDetails claimCompPremCessDate(BigDecimal claimCompPremCessDate) {
    this.claimCompPremCessDate = claimCompPremCessDate;
    return this;
  }

  /**
   * Get claimCompPremCessDate
   * @return claimCompPremCessDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getClaimCompPremCessDate() {
    return claimCompPremCessDate;
  }

  public void setClaimCompPremCessDate(BigDecimal claimCompPremCessDate) {
    this.claimCompPremCessDate = claimCompPremCessDate;
  }

  public ClaimDataUIClaimDetails wsResponse(ClaimDataUIClaimDetailsWSResponse wsResponse) {
    this.wsResponse = wsResponse;
    return this;
  }

  /**
   * Get wsResponse
   * @return wsResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ClaimDataUIClaimDetailsWSResponse getWsResponse() {
    return wsResponse;
  }

  public void setWsResponse(ClaimDataUIClaimDetailsWSResponse wsResponse) {
    this.wsResponse = wsResponse;
  }

  public ClaimDataUIClaimDetails additionalRequirement(List<ClaimDataUIClaimDetailsAdditionalRequirement> additionalRequirement) {
    this.additionalRequirement = additionalRequirement;
    return this;
  }

  public ClaimDataUIClaimDetails addAdditionalRequirementItem(ClaimDataUIClaimDetailsAdditionalRequirement additionalRequirementItem) {
    if (this.additionalRequirement == null) {
      this.additionalRequirement = new ArrayList<>();
    }
    this.additionalRequirement.add(additionalRequirementItem);
    return this;
  }

  /**
   * Get additionalRequirement
   * @return additionalRequirement
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimDataUIClaimDetailsAdditionalRequirement> getAdditionalRequirement() {
    return additionalRequirement;
  }

  public void setAdditionalRequirement(List<ClaimDataUIClaimDetailsAdditionalRequirement> additionalRequirement) {
    this.additionalRequirement = additionalRequirement;
  }

  public ClaimDataUIClaimDetails benefitfundDetails(List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitfundDetails) {
    this.benefitfundDetails = benefitfundDetails;
    return this;
  }

  public ClaimDataUIClaimDetails addBenefitfundDetailsItem(ClaimDataUIClaimDetailsBenefitfundDetails benefitfundDetailsItem) {
    if (this.benefitfundDetails == null) {
      this.benefitfundDetails = new ArrayList<>();
    }
    this.benefitfundDetails.add(benefitfundDetailsItem);
    return this;
  }

  /**
   * Get benefitfundDetails
   * @return benefitfundDetails
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ClaimDataUIClaimDetailsBenefitfundDetails> getBenefitfundDetails() {
    return benefitfundDetails;
  }

  public void setBenefitfundDetails(List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitfundDetails) {
    this.benefitfundDetails = benefitfundDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDataUIClaimDetails claimDataUIClaimDetails = (ClaimDataUIClaimDetails) o;
    return Objects.equals(this.claimNumber, claimDataUIClaimDetails.claimNumber) &&
        Objects.equals(this.claimPolicyNumber, claimDataUIClaimDetails.claimPolicyNumber) &&
        Objects.equals(this.claimProductName, claimDataUIClaimDetails.claimProductName) &&
        Objects.equals(this.claimTypeLA, claimDataUIClaimDetails.claimTypeLA) &&
        Objects.equals(this.claimPlanCode, claimDataUIClaimDetails.claimPlanCode) &&
        Objects.equals(this.claimReceivedDate, claimDataUIClaimDetails.claimReceivedDate) &&
        Objects.equals(this.claimRegisterDate, claimDataUIClaimDetails.claimRegisterDate) &&
        Objects.equals(this.claimInsuredName, claimDataUIClaimDetails.claimInsuredName) &&
        Objects.equals(this.claimInsuredClientID, claimDataUIClaimDetails.claimInsuredClientID) &&
        Objects.equals(this.claimDiagnoseCode, claimDataUIClaimDetails.claimDiagnoseCode) &&
        Objects.equals(this.claimDiagnoseName, claimDataUIClaimDetails.claimDiagnoseName) &&
        Objects.equals(this.claimAdmissionDate, claimDataUIClaimDetails.claimAdmissionDate) &&
        Objects.equals(this.claimDischargeDate, claimDataUIClaimDetails.claimDischargeDate) &&
        Objects.equals(this.claimProvider, claimDataUIClaimDetails.claimProvider) &&
        Objects.equals(this.claimProviderSource, claimDataUIClaimDetails.claimProviderSource) &&
        Objects.equals(this.claimDoctorName, claimDataUIClaimDetails.claimDoctorName) &&
        Objects.equals(this.claimInvoiceAmount, claimDataUIClaimDetails.claimInvoiceAmount) &&
        Objects.equals(this.claimInvoiceAmountUI, claimDataUIClaimDetails.claimInvoiceAmountUI) &&
        Objects.equals(this.claimPolicyCurrency, claimDataUIClaimDetails.claimPolicyCurrency) &&
        Objects.equals(this.claimSourcecurrency, claimDataUIClaimDetails.claimSourcecurrency) &&
        Objects.equals(this.claimDeathBenefitCategory, claimDataUIClaimDetails.claimDeathBenefitCategory) &&
        Objects.equals(this.claimPayeeName, claimDataUIClaimDetails.claimPayeeName) &&
        Objects.equals(this.claimFactoringHouse, claimDataUIClaimDetails.claimFactoringHouse) &&
        Objects.equals(this.claimBankCode, claimDataUIClaimDetails.claimBankCode) &&
        Objects.equals(this.claimBankAccountName, claimDataUIClaimDetails.claimBankAccountName) &&
        Objects.equals(this.claimBankAccountNumber, claimDataUIClaimDetails.claimBankAccountNumber) &&
        Objects.equals(this.claimBankType, claimDataUIClaimDetails.claimBankType) &&
        Objects.equals(this.comment1_, claimDataUIClaimDetails.comment1_) &&
        Objects.equals(this.claimSumAssuredPercent, claimDataUIClaimDetails.claimSumAssuredPercent) &&
        Objects.equals(this.comment2_, claimDataUIClaimDetails.comment2_) &&
        Objects.equals(this.claimComponentName, claimDataUIClaimDetails.claimComponentName) &&
        Objects.equals(this.comment3_, claimDataUIClaimDetails.comment3_) &&
        Objects.equals(this.claimComponentCode, claimDataUIClaimDetails.claimComponentCode) &&
        Objects.equals(this.claimComponentSumAssured, claimDataUIClaimDetails.claimComponentSumAssured) &&
        Objects.equals(this.claimOSCharges, claimDataUIClaimDetails.claimOSCharges) &&
        Objects.equals(this.claimOtherAdjustment, claimDataUIClaimDetails.claimOtherAdjustment) &&
        Objects.equals(this.claimAdjustmentCode, claimDataUIClaimDetails.claimAdjustmentCode) &&
        Objects.equals(this.claimPolicyLoan, claimDataUIClaimDetails.claimPolicyLoan) &&
        Objects.equals(this.claimPolicyDebt, claimDataUIClaimDetails.claimPolicyDebt) &&
        Objects.equals(this.claimFullyClaim, claimDataUIClaimDetails.claimFullyClaim) &&
        Objects.equals(this.claimUITotalAmount, claimDataUIClaimDetails.claimUITotalAmount) &&
        Objects.equals(this.claimCurrentFrom, claimDataUIClaimDetails.claimCurrentFrom) &&
        Objects.equals(this.claimStayDuration, claimDataUIClaimDetails.claimStayDuration) &&
        Objects.equals(this.autosavetime, claimDataUIClaimDetails.autosavetime) &&
        Objects.equals(this.autosaveflag, claimDataUIClaimDetails.autosaveflag) &&
        Objects.equals(this.claimPaymentDate, claimDataUIClaimDetails.claimPaymentDate) &&
        Objects.equals(this.claimPaidToDate, claimDataUIClaimDetails.claimPaidToDate) &&
        Objects.equals(this.claimCompPremCessDate, claimDataUIClaimDetails.claimCompPremCessDate) &&
        Objects.equals(this.wsResponse, claimDataUIClaimDetails.wsResponse) &&
        Objects.equals(this.additionalRequirement, claimDataUIClaimDetails.additionalRequirement) &&
        Objects.equals(this.benefitfundDetails, claimDataUIClaimDetails.benefitfundDetails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNumber, claimPolicyNumber, claimProductName, claimTypeLA, claimPlanCode, claimReceivedDate, claimRegisterDate, claimInsuredName, claimInsuredClientID, claimDiagnoseCode, claimDiagnoseName, claimAdmissionDate, claimDischargeDate, claimProvider, claimProviderSource, claimDoctorName, claimInvoiceAmount, claimInvoiceAmountUI, claimPolicyCurrency, claimSourcecurrency, claimDeathBenefitCategory, claimPayeeName, claimFactoringHouse, claimBankCode, claimBankAccountName, claimBankAccountNumber, claimBankType, comment1_, claimSumAssuredPercent, comment2_, claimComponentName, comment3_, claimComponentCode, claimComponentSumAssured, claimOSCharges, claimOtherAdjustment, claimAdjustmentCode, claimPolicyLoan, claimPolicyDebt, claimFullyClaim, claimUITotalAmount, claimCurrentFrom, claimStayDuration, autosavetime, autosaveflag, claimPaymentDate, claimPaidToDate, claimCompPremCessDate, wsResponse, additionalRequirement, benefitfundDetails);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDataUIClaimDetails {\n");
    
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    claimPolicyNumber: ").append(toIndentedString(claimPolicyNumber)).append("\n");
    sb.append("    claimProductName: ").append(toIndentedString(claimProductName)).append("\n");
    sb.append("    claimTypeLA: ").append(toIndentedString(claimTypeLA)).append("\n");
    sb.append("    claimPlanCode: ").append(toIndentedString(claimPlanCode)).append("\n");
    sb.append("    claimReceivedDate: ").append(toIndentedString(claimReceivedDate)).append("\n");
    sb.append("    claimRegisterDate: ").append(toIndentedString(claimRegisterDate)).append("\n");
    sb.append("    claimInsuredName: ").append(toIndentedString(claimInsuredName)).append("\n");
    sb.append("    claimInsuredClientID: ").append(toIndentedString(claimInsuredClientID)).append("\n");
    sb.append("    claimDiagnoseCode: ").append(toIndentedString(claimDiagnoseCode)).append("\n");
    sb.append("    claimDiagnoseName: ").append(toIndentedString(claimDiagnoseName)).append("\n");
    sb.append("    claimAdmissionDate: ").append(toIndentedString(claimAdmissionDate)).append("\n");
    sb.append("    claimDischargeDate: ").append(toIndentedString(claimDischargeDate)).append("\n");
    sb.append("    claimProvider: ").append(toIndentedString(claimProvider)).append("\n");
    sb.append("    claimProviderSource: ").append(toIndentedString(claimProviderSource)).append("\n");
    sb.append("    claimDoctorName: ").append(toIndentedString(claimDoctorName)).append("\n");
    sb.append("    claimInvoiceAmount: ").append(toIndentedString(claimInvoiceAmount)).append("\n");
    sb.append("    claimInvoiceAmountUI: ").append(toIndentedString(claimInvoiceAmountUI)).append("\n");
    sb.append("    claimPolicyCurrency: ").append(toIndentedString(claimPolicyCurrency)).append("\n");
    sb.append("    claimSourcecurrency: ").append(toIndentedString(claimSourcecurrency)).append("\n");
    sb.append("    claimDeathBenefitCategory: ").append(toIndentedString(claimDeathBenefitCategory)).append("\n");
    sb.append("    claimPayeeName: ").append(toIndentedString(claimPayeeName)).append("\n");
    sb.append("    claimFactoringHouse: ").append(toIndentedString(claimFactoringHouse)).append("\n");
    sb.append("    claimBankCode: ").append(toIndentedString(claimBankCode)).append("\n");
    sb.append("    claimBankAccountName: ").append(toIndentedString(claimBankAccountName)).append("\n");
    sb.append("    claimBankAccountNumber: ").append(toIndentedString(claimBankAccountNumber)).append("\n");
    sb.append("    claimBankType: ").append(toIndentedString(claimBankType)).append("\n");
    sb.append("    comment1_: ").append(toIndentedString(comment1_)).append("\n");
    sb.append("    claimSumAssuredPercent: ").append(toIndentedString(claimSumAssuredPercent)).append("\n");
    sb.append("    comment2_: ").append(toIndentedString(comment2_)).append("\n");
    sb.append("    claimComponentName: ").append(toIndentedString(claimComponentName)).append("\n");
    sb.append("    comment3_: ").append(toIndentedString(comment3_)).append("\n");
    sb.append("    claimComponentCode: ").append(toIndentedString(claimComponentCode)).append("\n");
    sb.append("    claimComponentSumAssured: ").append(toIndentedString(claimComponentSumAssured)).append("\n");
    sb.append("    claimOSCharges: ").append(toIndentedString(claimOSCharges)).append("\n");
    sb.append("    claimOtherAdjustment: ").append(toIndentedString(claimOtherAdjustment)).append("\n");
    sb.append("    claimAdjustmentCode: ").append(toIndentedString(claimAdjustmentCode)).append("\n");
    sb.append("    claimPolicyLoan: ").append(toIndentedString(claimPolicyLoan)).append("\n");
    sb.append("    claimPolicyDebt: ").append(toIndentedString(claimPolicyDebt)).append("\n");
    sb.append("    claimFullyClaim: ").append(toIndentedString(claimFullyClaim)).append("\n");
    sb.append("    claimUITotalAmount: ").append(toIndentedString(claimUITotalAmount)).append("\n");
    sb.append("    claimCurrentFrom: ").append(toIndentedString(claimCurrentFrom)).append("\n");
    sb.append("    claimStayDuration: ").append(toIndentedString(claimStayDuration)).append("\n");
    sb.append("    autosavetime: ").append(toIndentedString(autosavetime)).append("\n");
    sb.append("    autosaveflag: ").append(toIndentedString(autosaveflag)).append("\n");
    sb.append("    claimPaymentDate: ").append(toIndentedString(claimPaymentDate)).append("\n");
    sb.append("    claimPaidToDate: ").append(toIndentedString(claimPaidToDate)).append("\n");
    sb.append("    claimCompPremCessDate: ").append(toIndentedString(claimCompPremCessDate)).append("\n");
    sb.append("    wsResponse: ").append(toIndentedString(wsResponse)).append("\n");
    sb.append("    additionalRequirement: ").append(toIndentedString(additionalRequirement)).append("\n");
    sb.append("    benefitfundDetails: ").append(toIndentedString(benefitfundDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

