package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.CLaimAray;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimRecordList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimRecordList   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("historyDetails")
  @Valid
  private List<CLaimAray> historyDetails = null;

  public ClaimRecordList strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(example = "12345", value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public ClaimRecordList historyDetails(List<CLaimAray> historyDetails) {
    this.historyDetails = historyDetails;
    return this;
  }

  public ClaimRecordList addHistoryDetailsItem(CLaimAray historyDetailsItem) {
    if (this.historyDetails == null) {
      this.historyDetails = new ArrayList<>();
    }
    this.historyDetails.add(historyDetailsItem);
    return this;
  }

  /**
   * Get historyDetails
   * @return historyDetails
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CLaimAray> getHistoryDetails() {
    return historyDetails;
  }

  public void setHistoryDetails(List<CLaimAray> historyDetails) {
    this.historyDetails = historyDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimRecordList claimRecordList = (ClaimRecordList) o;
    return Objects.equals(this.strClaimNumber, claimRecordList.strClaimNumber) &&
        Objects.equals(this.historyDetails, claimRecordList.historyDetails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, historyDetails);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimRecordList {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    historyDetails: ").append(toIndentedString(historyDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

