package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ArgValuePairs;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateDataDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateDataDataIn   {
  @JsonProperty("argValuePairs")
  @Valid
  private List<ArgValuePairs> argValuePairs = null;

  @JsonProperty("strSQLQuery")
  private String strSQLQuery = null;

  public WSUpdateDataDataIn argValuePairs(List<ArgValuePairs> argValuePairs) {
    this.argValuePairs = argValuePairs;
    return this;
  }

  public WSUpdateDataDataIn addArgValuePairsItem(ArgValuePairs argValuePairsItem) {
    if (this.argValuePairs == null) {
      this.argValuePairs = new ArrayList<>();
    }
    this.argValuePairs.add(argValuePairsItem);
    return this;
  }

  /**
   * Get argValuePairs
   * @return argValuePairs
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ArgValuePairs> getArgValuePairs() {
    return argValuePairs;
  }

  public void setArgValuePairs(List<ArgValuePairs> argValuePairs) {
    this.argValuePairs = argValuePairs;
  }

  public WSUpdateDataDataIn strSQLQuery(String strSQLQuery) {
    this.strSQLQuery = strSQLQuery;
    return this;
  }

  /**
   * Get strSQLQuery
   * @return strSQLQuery
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrSQLQuery() {
    return strSQLQuery;
  }

  public void setStrSQLQuery(String strSQLQuery) {
    this.strSQLQuery = strSQLQuery;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateDataDataIn wsUpdateDataDataIn = (WSUpdateDataDataIn) o;
    return Objects.equals(this.argValuePairs, wsUpdateDataDataIn.argValuePairs) &&
        Objects.equals(this.strSQLQuery, wsUpdateDataDataIn.strSQLQuery);
  }

  @Override
  public int hashCode() {
    return Objects.hash(argValuePairs, strSQLQuery);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateDataDataIn {\n");
    
    sb.append("    argValuePairs: ").append(toIndentedString(argValuePairs)).append("\n");
    sb.append("    strSQLQuery: ").append(toIndentedString(strSQLQuery)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

