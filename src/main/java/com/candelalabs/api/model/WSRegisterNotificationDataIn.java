package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSRegisterNotificationDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSRegisterNotificationDataIn   {
  @JsonProperty("DECISION")
  private String DECISION = null;

  @JsonProperty("PROCESSNAME")
  private String PROCESSNAME = null;

  @JsonProperty("CLAIMNUMBER")
  private String CLAIMNUMBER = null;

  @JsonProperty("POLICYNUMBER")
  private String POLICYNUMBER = null;

  @JsonProperty("APPLICATIONNUMBER")
  private String APPLICATIONNUMBER = null;

  @JsonProperty("PROCESSACTIVITYNAME")
  private String PROCESSACTIVITYNAME = null;

  @JsonProperty("PROCESSCATEGORY")
  private String PROCESSCATEGORY = null;

  public WSRegisterNotificationDataIn DECISION(String DECISION) {
    this.DECISION = DECISION;
    return this;
  }

  /**
   * Get DECISION
   * @return DECISION
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getDECISION() {
    return DECISION;
  }

  public void setDECISION(String DECISION) {
    this.DECISION = DECISION;
  }

  public WSRegisterNotificationDataIn PROCESSNAME(String PROCESSNAME) {
    this.PROCESSNAME = PROCESSNAME;
    return this;
  }

  /**
   * Get PROCESSNAME
   * @return PROCESSNAME
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getPROCESSNAME() {
    return PROCESSNAME;
  }

  public void setPROCESSNAME(String PROCESSNAME) {
    this.PROCESSNAME = PROCESSNAME;
  }

  public WSRegisterNotificationDataIn CLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
    return this;
  }

  /**
   * Get CLAIMNUMBER
   * @return CLAIMNUMBER
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getCLAIMNUMBER() {
    return CLAIMNUMBER;
  }

  public void setCLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
  }

  public WSRegisterNotificationDataIn POLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
    return this;
  }

  /**
   * Get POLICYNUMBER
   * @return POLICYNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getPOLICYNUMBER() {
    return POLICYNUMBER;
  }

  public void setPOLICYNUMBER(String POLICYNUMBER) {
    this.POLICYNUMBER = POLICYNUMBER;
  }

  public WSRegisterNotificationDataIn APPLICATIONNUMBER(String APPLICATIONNUMBER) {
    this.APPLICATIONNUMBER = APPLICATIONNUMBER;
    return this;
  }

  /**
   * Get APPLICATIONNUMBER
   * @return APPLICATIONNUMBER
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getAPPLICATIONNUMBER() {
    return APPLICATIONNUMBER;
  }

  public void setAPPLICATIONNUMBER(String APPLICATIONNUMBER) {
    this.APPLICATIONNUMBER = APPLICATIONNUMBER;
  }

  public WSRegisterNotificationDataIn PROCESSACTIVITYNAME(String PROCESSACTIVITYNAME) {
    this.PROCESSACTIVITYNAME = PROCESSACTIVITYNAME;
    return this;
  }

  /**
   * Get PROCESSACTIVITYNAME
   * @return PROCESSACTIVITYNAME
  **/
  @ApiModelProperty(value = "")


  public String getPROCESSACTIVITYNAME() {
    return PROCESSACTIVITYNAME;
  }

  public void setPROCESSACTIVITYNAME(String PROCESSACTIVITYNAME) {
    this.PROCESSACTIVITYNAME = PROCESSACTIVITYNAME;
  }

  public WSRegisterNotificationDataIn PROCESSCATEGORY(String PROCESSCATEGORY) {
    this.PROCESSCATEGORY = PROCESSCATEGORY;
    return this;
  }

  /**
   * Get PROCESSCATEGORY
   * @return PROCESSCATEGORY
  **/
  @ApiModelProperty(value = "")


  public String getPROCESSCATEGORY() {
    return PROCESSCATEGORY;
  }

  public void setPROCESSCATEGORY(String PROCESSCATEGORY) {
    this.PROCESSCATEGORY = PROCESSCATEGORY;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSRegisterNotificationDataIn wsRegisterNotificationDataIn = (WSRegisterNotificationDataIn) o;
    return Objects.equals(this.DECISION, wsRegisterNotificationDataIn.DECISION) &&
        Objects.equals(this.PROCESSNAME, wsRegisterNotificationDataIn.PROCESSNAME) &&
        Objects.equals(this.CLAIMNUMBER, wsRegisterNotificationDataIn.CLAIMNUMBER) &&
        Objects.equals(this.POLICYNUMBER, wsRegisterNotificationDataIn.POLICYNUMBER) &&
        Objects.equals(this.APPLICATIONNUMBER, wsRegisterNotificationDataIn.APPLICATIONNUMBER) &&
        Objects.equals(this.PROCESSACTIVITYNAME, wsRegisterNotificationDataIn.PROCESSACTIVITYNAME) &&
        Objects.equals(this.PROCESSCATEGORY, wsRegisterNotificationDataIn.PROCESSCATEGORY);
  }

  @Override
  public int hashCode() {
    return Objects.hash(DECISION, PROCESSNAME, CLAIMNUMBER, POLICYNUMBER, APPLICATIONNUMBER, PROCESSACTIVITYNAME, PROCESSCATEGORY);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSRegisterNotificationDataIn {\n");
    
    sb.append("    DECISION: ").append(toIndentedString(DECISION)).append("\n");
    sb.append("    PROCESSNAME: ").append(toIndentedString(PROCESSNAME)).append("\n");
    sb.append("    CLAIMNUMBER: ").append(toIndentedString(CLAIMNUMBER)).append("\n");
    sb.append("    POLICYNUMBER: ").append(toIndentedString(POLICYNUMBER)).append("\n");
    sb.append("    APPLICATIONNUMBER: ").append(toIndentedString(APPLICATIONNUMBER)).append("\n");
    sb.append("    PROCESSACTIVITYNAME: ").append(toIndentedString(PROCESSACTIVITYNAME)).append("\n");
    sb.append("    PROCESSCATEGORY: ").append(toIndentedString(PROCESSCATEGORY)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

