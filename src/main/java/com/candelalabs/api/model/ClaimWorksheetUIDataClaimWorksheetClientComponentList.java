package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetClientComponentList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetClientComponentList   {
  @JsonProperty("strCLDRider")
  private String strCLDRider = null;

  @JsonProperty("strCLDComponentName")
  private String strCLDComponentName = null;

  @JsonProperty("strCLDComponentCode")
  private String strCLDComponentCode = null;

  @JsonProperty("strCLDLifeName")
  private String strCLDLifeName = null;

  @JsonProperty("strCLDLifeNumber")
  private String strCLDLifeNumber = null;

  @JsonProperty("dblCLDSumAssured")
  private BigDecimal dblCLDSumAssured = null;

  @JsonProperty("dtCLDComponentRiskCommencementDate")
  private BigDecimal dtCLDComponentRiskCommencementDate = null;

  @JsonProperty("dtCLDComponentRiskCessDate")
  private BigDecimal dtCLDComponentRiskCessDate = null;

  @JsonProperty("dtCLDComponentPremCessDate")
  private BigDecimal dtCLDComponentPremCessDate = null;

  @JsonProperty("strCLDComponentStatus")
  private String strCLDComponentStatus = null;

  @JsonProperty("strCLDComponentPremStatus")
  private String strCLDComponentPremStatus = null;

  @JsonProperty("strCLDPlanCode")
  private String strCLDPlanCode = null;

  @JsonProperty("strCLDUnit")
  private String strCLDUnit = null;

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDRider(String strCLDRider) {
    this.strCLDRider = strCLDRider;
    return this;
  }

  /**
   * Get strCLDRider
   * @return strCLDRider
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDRider() {
    return strCLDRider;
  }

  public void setStrCLDRider(String strCLDRider) {
    this.strCLDRider = strCLDRider;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDComponentName(String strCLDComponentName) {
    this.strCLDComponentName = strCLDComponentName;
    return this;
  }

  /**
   * Get strCLDComponentName
   * @return strCLDComponentName
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDComponentName() {
    return strCLDComponentName;
  }

  public void setStrCLDComponentName(String strCLDComponentName) {
    this.strCLDComponentName = strCLDComponentName;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDComponentCode(String strCLDComponentCode) {
    this.strCLDComponentCode = strCLDComponentCode;
    return this;
  }

  /**
   * Get strCLDComponentCode
   * @return strCLDComponentCode
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDComponentCode() {
    return strCLDComponentCode;
  }

  public void setStrCLDComponentCode(String strCLDComponentCode) {
    this.strCLDComponentCode = strCLDComponentCode;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDLifeName(String strCLDLifeName) {
    this.strCLDLifeName = strCLDLifeName;
    return this;
  }

  /**
   * Get strCLDLifeName
   * @return strCLDLifeName
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDLifeName() {
    return strCLDLifeName;
  }

  public void setStrCLDLifeName(String strCLDLifeName) {
    this.strCLDLifeName = strCLDLifeName;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDLifeNumber(String strCLDLifeNumber) {
    this.strCLDLifeNumber = strCLDLifeNumber;
    return this;
  }

  /**
   * Get strCLDLifeNumber
   * @return strCLDLifeNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDLifeNumber() {
    return strCLDLifeNumber;
  }

  public void setStrCLDLifeNumber(String strCLDLifeNumber) {
    this.strCLDLifeNumber = strCLDLifeNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList dblCLDSumAssured(BigDecimal dblCLDSumAssured) {
    this.dblCLDSumAssured = dblCLDSumAssured;
    return this;
  }

  /**
   * Get dblCLDSumAssured
   * @return dblCLDSumAssured
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDblCLDSumAssured() {
    return dblCLDSumAssured;
  }

  public void setDblCLDSumAssured(BigDecimal dblCLDSumAssured) {
    this.dblCLDSumAssured = dblCLDSumAssured;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList dtCLDComponentRiskCommencementDate(BigDecimal dtCLDComponentRiskCommencementDate) {
    this.dtCLDComponentRiskCommencementDate = dtCLDComponentRiskCommencementDate;
    return this;
  }

  /**
   * Get dtCLDComponentRiskCommencementDate
   * @return dtCLDComponentRiskCommencementDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtCLDComponentRiskCommencementDate() {
    return dtCLDComponentRiskCommencementDate;
  }

  public void setDtCLDComponentRiskCommencementDate(BigDecimal dtCLDComponentRiskCommencementDate) {
    this.dtCLDComponentRiskCommencementDate = dtCLDComponentRiskCommencementDate;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList dtCLDComponentRiskCessDate(BigDecimal dtCLDComponentRiskCessDate) {
    this.dtCLDComponentRiskCessDate = dtCLDComponentRiskCessDate;
    return this;
  }

  /**
   * Get dtCLDComponentRiskCessDate
   * @return dtCLDComponentRiskCessDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtCLDComponentRiskCessDate() {
    return dtCLDComponentRiskCessDate;
  }

  public void setDtCLDComponentRiskCessDate(BigDecimal dtCLDComponentRiskCessDate) {
    this.dtCLDComponentRiskCessDate = dtCLDComponentRiskCessDate;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList dtCLDComponentPremCessDate(BigDecimal dtCLDComponentPremCessDate) {
    this.dtCLDComponentPremCessDate = dtCLDComponentPremCessDate;
    return this;
  }

  /**
   * Get dtCLDComponentPremCessDate
   * @return dtCLDComponentPremCessDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtCLDComponentPremCessDate() {
    return dtCLDComponentPremCessDate;
  }

  public void setDtCLDComponentPremCessDate(BigDecimal dtCLDComponentPremCessDate) {
    this.dtCLDComponentPremCessDate = dtCLDComponentPremCessDate;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDComponentStatus(String strCLDComponentStatus) {
    this.strCLDComponentStatus = strCLDComponentStatus;
    return this;
  }

  /**
   * Get strCLDComponentStatus
   * @return strCLDComponentStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDComponentStatus() {
    return strCLDComponentStatus;
  }

  public void setStrCLDComponentStatus(String strCLDComponentStatus) {
    this.strCLDComponentStatus = strCLDComponentStatus;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDComponentPremStatus(String strCLDComponentPremStatus) {
    this.strCLDComponentPremStatus = strCLDComponentPremStatus;
    return this;
  }

  /**
   * Get strCLDComponentPremStatus
   * @return strCLDComponentPremStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDComponentPremStatus() {
    return strCLDComponentPremStatus;
  }

  public void setStrCLDComponentPremStatus(String strCLDComponentPremStatus) {
    this.strCLDComponentPremStatus = strCLDComponentPremStatus;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDPlanCode(String strCLDPlanCode) {
    this.strCLDPlanCode = strCLDPlanCode;
    return this;
  }

  /**
   * Get strCLDPlanCode
   * @return strCLDPlanCode
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDPlanCode() {
    return strCLDPlanCode;
  }

  public void setStrCLDPlanCode(String strCLDPlanCode) {
    this.strCLDPlanCode = strCLDPlanCode;
  }

  public ClaimWorksheetUIDataClaimWorksheetClientComponentList strCLDUnit(String strCLDUnit) {
    this.strCLDUnit = strCLDUnit;
    return this;
  }

  /**
   * Get strCLDUnit
   * @return strCLDUnit
  **/
  @ApiModelProperty(value = "")


  public String getStrCLDUnit() {
    return strCLDUnit;
  }

  public void setStrCLDUnit(String strCLDUnit) {
    this.strCLDUnit = strCLDUnit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetClientComponentList claimWorksheetUIDataClaimWorksheetClientComponentList = (ClaimWorksheetUIDataClaimWorksheetClientComponentList) o;
    return Objects.equals(this.strCLDRider, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDRider) &&
        Objects.equals(this.strCLDComponentName, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDComponentName) &&
        Objects.equals(this.strCLDComponentCode, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDComponentCode) &&
        Objects.equals(this.strCLDLifeName, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDLifeName) &&
        Objects.equals(this.strCLDLifeNumber, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDLifeNumber) &&
        Objects.equals(this.dblCLDSumAssured, claimWorksheetUIDataClaimWorksheetClientComponentList.dblCLDSumAssured) &&
        Objects.equals(this.dtCLDComponentRiskCommencementDate, claimWorksheetUIDataClaimWorksheetClientComponentList.dtCLDComponentRiskCommencementDate) &&
        Objects.equals(this.dtCLDComponentRiskCessDate, claimWorksheetUIDataClaimWorksheetClientComponentList.dtCLDComponentRiskCessDate) &&
        Objects.equals(this.dtCLDComponentPremCessDate, claimWorksheetUIDataClaimWorksheetClientComponentList.dtCLDComponentPremCessDate) &&
        Objects.equals(this.strCLDComponentStatus, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDComponentStatus) &&
        Objects.equals(this.strCLDComponentPremStatus, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDComponentPremStatus) &&
        Objects.equals(this.strCLDPlanCode, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDPlanCode) &&
        Objects.equals(this.strCLDUnit, claimWorksheetUIDataClaimWorksheetClientComponentList.strCLDUnit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strCLDRider, strCLDComponentName, strCLDComponentCode, strCLDLifeName, strCLDLifeNumber, dblCLDSumAssured, dtCLDComponentRiskCommencementDate, dtCLDComponentRiskCessDate, dtCLDComponentPremCessDate, strCLDComponentStatus, strCLDComponentPremStatus, strCLDPlanCode, strCLDUnit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetClientComponentList {\n");
    
    sb.append("    strCLDRider: ").append(toIndentedString(strCLDRider)).append("\n");
    sb.append("    strCLDComponentName: ").append(toIndentedString(strCLDComponentName)).append("\n");
    sb.append("    strCLDComponentCode: ").append(toIndentedString(strCLDComponentCode)).append("\n");
    sb.append("    strCLDLifeName: ").append(toIndentedString(strCLDLifeName)).append("\n");
    sb.append("    strCLDLifeNumber: ").append(toIndentedString(strCLDLifeNumber)).append("\n");
    sb.append("    dblCLDSumAssured: ").append(toIndentedString(dblCLDSumAssured)).append("\n");
    sb.append("    dtCLDComponentRiskCommencementDate: ").append(toIndentedString(dtCLDComponentRiskCommencementDate)).append("\n");
    sb.append("    dtCLDComponentRiskCessDate: ").append(toIndentedString(dtCLDComponentRiskCessDate)).append("\n");
    sb.append("    dtCLDComponentPremCessDate: ").append(toIndentedString(dtCLDComponentPremCessDate)).append("\n");
    sb.append("    strCLDComponentStatus: ").append(toIndentedString(strCLDComponentStatus)).append("\n");
    sb.append("    strCLDComponentPremStatus: ").append(toIndentedString(strCLDComponentPremStatus)).append("\n");
    sb.append("    strCLDPlanCode: ").append(toIndentedString(strCLDPlanCode)).append("\n");
    sb.append("    strCLDUnit: ").append(toIndentedString(strCLDUnit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

