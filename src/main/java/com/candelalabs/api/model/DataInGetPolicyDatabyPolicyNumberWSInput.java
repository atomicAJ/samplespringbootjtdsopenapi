package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetPolicyDatabyPolicyNumberWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetPolicyDatabyPolicyNumberWSInput   {
  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strClaimTypeUI")
  private String strClaimTypeUI = null;

  public DataInGetPolicyDatabyPolicyNumberWSInput strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public DataInGetPolicyDatabyPolicyNumberWSInput strClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
    return this;
  }

  /**
   * Get strClaimTypeUI
   * @return strClaimTypeUI
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeUI() {
    return strClaimTypeUI;
  }

  public void setStrClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetPolicyDatabyPolicyNumberWSInput dataInGetPolicyDatabyPolicyNumberWSInput = (DataInGetPolicyDatabyPolicyNumberWSInput) o;
    return Objects.equals(this.strPolicyNumber, dataInGetPolicyDatabyPolicyNumberWSInput.strPolicyNumber) &&
        Objects.equals(this.strClaimTypeUI, dataInGetPolicyDatabyPolicyNumberWSInput.strClaimTypeUI);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strPolicyNumber, strClaimTypeUI);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetPolicyDatabyPolicyNumberWSInput {\n");
    
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strClaimTypeUI: ").append(toIndentedString(strClaimTypeUI)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

