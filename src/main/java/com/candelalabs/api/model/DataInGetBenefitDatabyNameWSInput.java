package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetBenefitDatabyNameWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetBenefitDatabyNameWSInput   {
  @JsonProperty("strBenefitName")
  private String strBenefitName = null;

  @JsonProperty("dtAdmissionDate")
  private BigDecimal dtAdmissionDate = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strInsuredClientID")
  private String strInsuredClientID = null;

  @JsonProperty("strComponentCode")
  private String strComponentCode = null;

  @JsonProperty("strPlanCode")
  private String strPlanCode = null;

  @JsonProperty("strClaimTypeUI")
  private String strClaimTypeUI = null;

  public DataInGetBenefitDatabyNameWSInput strBenefitName(String strBenefitName) {
    this.strBenefitName = strBenefitName;
    return this;
  }

  /**
   * Get strBenefitName
   * @return strBenefitName
  **/
  @ApiModelProperty(value = "")


  public String getStrBenefitName() {
    return strBenefitName;
  }

  public void setStrBenefitName(String strBenefitName) {
    this.strBenefitName = strBenefitName;
  }

  public DataInGetBenefitDatabyNameWSInput dtAdmissionDate(BigDecimal dtAdmissionDate) {
    this.dtAdmissionDate = dtAdmissionDate;
    return this;
  }

  /**
   * Get dtAdmissionDate
   * @return dtAdmissionDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtAdmissionDate() {
    return dtAdmissionDate;
  }

  public void setDtAdmissionDate(BigDecimal dtAdmissionDate) {
    this.dtAdmissionDate = dtAdmissionDate;
  }

  public DataInGetBenefitDatabyNameWSInput strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public DataInGetBenefitDatabyNameWSInput strInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
    return this;
  }

  /**
   * Get strInsuredClientID
   * @return strInsuredClientID
  **/
  @ApiModelProperty(value = "")


  public String getStrInsuredClientID() {
    return strInsuredClientID;
  }

  public void setStrInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
  }

  public DataInGetBenefitDatabyNameWSInput strComponentCode(String strComponentCode) {
    this.strComponentCode = strComponentCode;
    return this;
  }

  /**
   * Get strComponentCode
   * @return strComponentCode
  **/
  @ApiModelProperty(value = "")


  public String getStrComponentCode() {
    return strComponentCode;
  }

  public void setStrComponentCode(String strComponentCode) {
    this.strComponentCode = strComponentCode;
  }

  public DataInGetBenefitDatabyNameWSInput strPlanCode(String strPlanCode) {
    this.strPlanCode = strPlanCode;
    return this;
  }

  /**
   * Get strPlanCode
   * @return strPlanCode
  **/
  @ApiModelProperty(value = "")


  public String getStrPlanCode() {
    return strPlanCode;
  }

  public void setStrPlanCode(String strPlanCode) {
    this.strPlanCode = strPlanCode;
  }

  public DataInGetBenefitDatabyNameWSInput strClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
    return this;
  }

  /**
   * Get strClaimTypeUI
   * @return strClaimTypeUI
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeUI() {
    return strClaimTypeUI;
  }

  public void setStrClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetBenefitDatabyNameWSInput dataInGetBenefitDatabyNameWSInput = (DataInGetBenefitDatabyNameWSInput) o;
    return Objects.equals(this.strBenefitName, dataInGetBenefitDatabyNameWSInput.strBenefitName) &&
        Objects.equals(this.dtAdmissionDate, dataInGetBenefitDatabyNameWSInput.dtAdmissionDate) &&
        Objects.equals(this.strPolicyNumber, dataInGetBenefitDatabyNameWSInput.strPolicyNumber) &&
        Objects.equals(this.strInsuredClientID, dataInGetBenefitDatabyNameWSInput.strInsuredClientID) &&
        Objects.equals(this.strComponentCode, dataInGetBenefitDatabyNameWSInput.strComponentCode) &&
        Objects.equals(this.strPlanCode, dataInGetBenefitDatabyNameWSInput.strPlanCode) &&
        Objects.equals(this.strClaimTypeUI, dataInGetBenefitDatabyNameWSInput.strClaimTypeUI);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strBenefitName, dtAdmissionDate, strPolicyNumber, strInsuredClientID, strComponentCode, strPlanCode, strClaimTypeUI);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetBenefitDatabyNameWSInput {\n");
    
    sb.append("    strBenefitName: ").append(toIndentedString(strBenefitName)).append("\n");
    sb.append("    dtAdmissionDate: ").append(toIndentedString(dtAdmissionDate)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strInsuredClientID: ").append(toIndentedString(strInsuredClientID)).append("\n");
    sb.append("    strComponentCode: ").append(toIndentedString(strComponentCode)).append("\n");
    sb.append("    strPlanCode: ").append(toIndentedString(strPlanCode)).append("\n");
    sb.append("    strClaimTypeUI: ").append(toIndentedString(strClaimTypeUI)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

