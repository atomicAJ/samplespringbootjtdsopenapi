package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSManageReportEntriesDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSManageReportEntriesDataIn   {
  @JsonProperty("ClaimNumber")
  private String claimNumber = null;

  @JsonProperty("PolicyRequestNumber")
  private String policyRequestNumber = null;

  @JsonProperty("AppNumber")
  private String appNumber = null;

  @JsonProperty("PolicyNumber")
  private String policyNumber = null;

  @JsonProperty("ProcessInstanecID")
  private String processInstanecID = null;

  @JsonProperty("ProcessName")
  private String processName = null;

  @JsonProperty("ActivityType")
  private String activityType = null;

  @JsonProperty("ActivityName")
  private String activityName = null;

  @JsonProperty("StartTime")
  private OffsetDateTime startTime = null;

  @JsonProperty("EndTime")
  private OffsetDateTime endTime = null;

  @JsonProperty("ActionedUser")
  private String actionedUser = null;

  @JsonProperty("UserDecision")
  private String userDecision = null;

  @JsonProperty("ReportAction")
  private String reportAction = null;

  public WSManageReportEntriesDataIn claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public WSManageReportEntriesDataIn policyRequestNumber(String policyRequestNumber) {
    this.policyRequestNumber = policyRequestNumber;
    return this;
  }

  /**
   * Get policyRequestNumber
   * @return policyRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getPolicyRequestNumber() {
    return policyRequestNumber;
  }

  public void setPolicyRequestNumber(String policyRequestNumber) {
    this.policyRequestNumber = policyRequestNumber;
  }

  public WSManageReportEntriesDataIn appNumber(String appNumber) {
    this.appNumber = appNumber;
    return this;
  }

  /**
   * Get appNumber
   * @return appNumber
  **/
  @ApiModelProperty(value = "")


  public String getAppNumber() {
    return appNumber;
  }

  public void setAppNumber(String appNumber) {
    this.appNumber = appNumber;
  }

  public WSManageReportEntriesDataIn policyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
    return this;
  }

  /**
   * Get policyNumber
   * @return policyNumber
  **/
  @ApiModelProperty(value = "")


  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public WSManageReportEntriesDataIn processInstanecID(String processInstanecID) {
    this.processInstanecID = processInstanecID;
    return this;
  }

  /**
   * Get processInstanecID
   * @return processInstanecID
  **/
  @ApiModelProperty(value = "")


  public String getProcessInstanecID() {
    return processInstanecID;
  }

  public void setProcessInstanecID(String processInstanecID) {
    this.processInstanecID = processInstanecID;
  }

  public WSManageReportEntriesDataIn processName(String processName) {
    this.processName = processName;
    return this;
  }

  /**
   * Get processName
   * @return processName
  **/
  @ApiModelProperty(value = "")


  public String getProcessName() {
    return processName;
  }

  public void setProcessName(String processName) {
    this.processName = processName;
  }

  public WSManageReportEntriesDataIn activityType(String activityType) {
    this.activityType = activityType;
    return this;
  }

  /**
   * Get activityType
   * @return activityType
  **/
  @ApiModelProperty(value = "")


  public String getActivityType() {
    return activityType;
  }

  public void setActivityType(String activityType) {
    this.activityType = activityType;
  }

  public WSManageReportEntriesDataIn activityName(String activityName) {
    this.activityName = activityName;
    return this;
  }

  /**
   * Get activityName
   * @return activityName
  **/
  @ApiModelProperty(value = "")


  public String getActivityName() {
    return activityName;
  }

  public void setActivityName(String activityName) {
    this.activityName = activityName;
  }

  public WSManageReportEntriesDataIn startTime(OffsetDateTime startTime) {
    this.startTime = startTime;
    return this;
  }

  /**
   * Get startTime
   * @return startTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(OffsetDateTime startTime) {
    this.startTime = startTime;
  }

  public WSManageReportEntriesDataIn endTime(OffsetDateTime endTime) {
    this.endTime = endTime;
    return this;
  }

  /**
   * Get endTime
   * @return endTime
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(OffsetDateTime endTime) {
    this.endTime = endTime;
  }

  public WSManageReportEntriesDataIn actionedUser(String actionedUser) {
    this.actionedUser = actionedUser;
    return this;
  }

  /**
   * Get actionedUser
   * @return actionedUser
  **/
  @ApiModelProperty(value = "")


  public String getActionedUser() {
    return actionedUser;
  }

  public void setActionedUser(String actionedUser) {
    this.actionedUser = actionedUser;
  }

  public WSManageReportEntriesDataIn userDecision(String userDecision) {
    this.userDecision = userDecision;
    return this;
  }

  /**
   * Get userDecision
   * @return userDecision
  **/
  @ApiModelProperty(value = "")


  public String getUserDecision() {
    return userDecision;
  }

  public void setUserDecision(String userDecision) {
    this.userDecision = userDecision;
  }

  public WSManageReportEntriesDataIn reportAction(String reportAction) {
    this.reportAction = reportAction;
    return this;
  }

  /**
   * Get reportAction
   * @return reportAction
  **/
  @ApiModelProperty(value = "")


  public String getReportAction() {
    return reportAction;
  }

  public void setReportAction(String reportAction) {
    this.reportAction = reportAction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSManageReportEntriesDataIn wsManageReportEntriesDataIn = (WSManageReportEntriesDataIn) o;
    return Objects.equals(this.claimNumber, wsManageReportEntriesDataIn.claimNumber) &&
        Objects.equals(this.policyRequestNumber, wsManageReportEntriesDataIn.policyRequestNumber) &&
        Objects.equals(this.appNumber, wsManageReportEntriesDataIn.appNumber) &&
        Objects.equals(this.policyNumber, wsManageReportEntriesDataIn.policyNumber) &&
        Objects.equals(this.processInstanecID, wsManageReportEntriesDataIn.processInstanecID) &&
        Objects.equals(this.processName, wsManageReportEntriesDataIn.processName) &&
        Objects.equals(this.activityType, wsManageReportEntriesDataIn.activityType) &&
        Objects.equals(this.activityName, wsManageReportEntriesDataIn.activityName) &&
        Objects.equals(this.startTime, wsManageReportEntriesDataIn.startTime) &&
        Objects.equals(this.endTime, wsManageReportEntriesDataIn.endTime) &&
        Objects.equals(this.actionedUser, wsManageReportEntriesDataIn.actionedUser) &&
        Objects.equals(this.userDecision, wsManageReportEntriesDataIn.userDecision) &&
        Objects.equals(this.reportAction, wsManageReportEntriesDataIn.reportAction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNumber, policyRequestNumber, appNumber, policyNumber, processInstanecID, processName, activityType, activityName, startTime, endTime, actionedUser, userDecision, reportAction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSManageReportEntriesDataIn {\n");
    
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    policyRequestNumber: ").append(toIndentedString(policyRequestNumber)).append("\n");
    sb.append("    appNumber: ").append(toIndentedString(appNumber)).append("\n");
    sb.append("    policyNumber: ").append(toIndentedString(policyNumber)).append("\n");
    sb.append("    processInstanecID: ").append(toIndentedString(processInstanecID)).append("\n");
    sb.append("    processName: ").append(toIndentedString(processName)).append("\n");
    sb.append("    activityType: ").append(toIndentedString(activityType)).append("\n");
    sb.append("    activityName: ").append(toIndentedString(activityName)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    endTime: ").append(toIndentedString(endTime)).append("\n");
    sb.append("    actionedUser: ").append(toIndentedString(actionedUser)).append("\n");
    sb.append("    userDecision: ").append(toIndentedString(userDecision)).append("\n");
    sb.append("    reportAction: ").append(toIndentedString(reportAction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

