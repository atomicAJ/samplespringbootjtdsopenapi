package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ProductListData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ProductListData   {
  @JsonProperty("strProductName")
  private String strProductName = null;

  @JsonProperty("strProudctCode")
  private String strProudctCode = null;

  public ProductListData strProductName(String strProductName) {
    this.strProductName = strProductName;
    return this;
  }

  /**
   * Get strProductName
   * @return strProductName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrProductName() {
    return strProductName;
  }

  public void setStrProductName(String strProductName) {
    this.strProductName = strProductName;
  }

  public ProductListData strProudctCode(String strProudctCode) {
    this.strProudctCode = strProudctCode;
    return this;
  }

  /**
   * Get strProudctCode
   * @return strProudctCode
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrProudctCode() {
    return strProudctCode;
  }

  public void setStrProudctCode(String strProudctCode) {
    this.strProudctCode = strProudctCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductListData productListData = (ProductListData) o;
    return Objects.equals(this.strProductName, productListData.strProductName) &&
        Objects.equals(this.strProudctCode, productListData.strProudctCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strProductName, strProudctCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProductListData {\n");
    
    sb.append("    strProductName: ").append(toIndentedString(strProductName)).append("\n");
    sb.append("    strProudctCode: ").append(toIndentedString(strProudctCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

