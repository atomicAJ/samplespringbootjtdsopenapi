package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DocumentList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DocumentList   {
  @JsonProperty("strDocID")
  private String strDocID = null;

  @JsonProperty("strSubCategory")
  private String strSubCategory = null;

  @JsonProperty("strDocReceiveStatus")
  private String strDocReceiveStatus = null;

  public DocumentList strDocID(String strDocID) {
    this.strDocID = strDocID;
    return this;
  }

  /**
   * Get strDocID
   * @return strDocID
  **/
  @ApiModelProperty(value = "")


  public String getStrDocID() {
    return strDocID;
  }

  public void setStrDocID(String strDocID) {
    this.strDocID = strDocID;
  }

  public DocumentList strSubCategory(String strSubCategory) {
    this.strSubCategory = strSubCategory;
    return this;
  }

  /**
   * Get strSubCategory
   * @return strSubCategory
  **/
  @ApiModelProperty(value = "")


  public String getStrSubCategory() {
    return strSubCategory;
  }

  public void setStrSubCategory(String strSubCategory) {
    this.strSubCategory = strSubCategory;
  }

  public DocumentList strDocReceiveStatus(String strDocReceiveStatus) {
    this.strDocReceiveStatus = strDocReceiveStatus;
    return this;
  }

  /**
   * Get strDocReceiveStatus
   * @return strDocReceiveStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrDocReceiveStatus() {
    return strDocReceiveStatus;
  }

  public void setStrDocReceiveStatus(String strDocReceiveStatus) {
    this.strDocReceiveStatus = strDocReceiveStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DocumentList documentList = (DocumentList) o;
    return Objects.equals(this.strDocID, documentList.strDocID) &&
        Objects.equals(this.strSubCategory, documentList.strSubCategory) &&
        Objects.equals(this.strDocReceiveStatus, documentList.strDocReceiveStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strDocID, strSubCategory, strDocReceiveStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DocumentList {\n");
    
    sb.append("    strDocID: ").append(toIndentedString(strDocID)).append("\n");
    sb.append("    strSubCategory: ").append(toIndentedString(strSubCategory)).append("\n");
    sb.append("    strDocReceiveStatus: ").append(toIndentedString(strDocReceiveStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

