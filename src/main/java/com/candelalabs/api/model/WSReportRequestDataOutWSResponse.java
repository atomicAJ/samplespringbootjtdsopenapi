package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSReportRequestDataOutWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSReportRequestDataOutWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strReportOutuputFormat")
  private String strReportOutuputFormat = null;

  @JsonProperty("strReportRequestID")
  private String strReportRequestID = null;

  @JsonProperty("strReportName")
  private String strReportName = null;

  @JsonProperty("strReportRequestTime")
  private String strReportRequestTime = null;

  @JsonProperty("strReportResponseTime")
  private String strReportResponseTime = null;

  @JsonProperty("strReportGenerationStatus")
  private String strReportGenerationStatus = null;

  @JsonProperty("strReportGenerationRemarks")
  private String strReportGenerationRemarks = null;

  @JsonProperty("strReportFileLocation")
  private String strReportFileLocation = null;

  @JsonProperty("reportData")
  private String reportData = null;

  public WSReportRequestDataOutWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSReportRequestDataOutWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSReportRequestDataOutWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public WSReportRequestDataOutWSResponse strReportOutuputFormat(String strReportOutuputFormat) {
    this.strReportOutuputFormat = strReportOutuputFormat;
    return this;
  }

  /**
   * Get strReportOutuputFormat
   * @return strReportOutuputFormat
  **/
  @ApiModelProperty(value = "")


  public String getStrReportOutuputFormat() {
    return strReportOutuputFormat;
  }

  public void setStrReportOutuputFormat(String strReportOutuputFormat) {
    this.strReportOutuputFormat = strReportOutuputFormat;
  }

  public WSReportRequestDataOutWSResponse strReportRequestID(String strReportRequestID) {
    this.strReportRequestID = strReportRequestID;
    return this;
  }

  /**
   * Get strReportRequestID
   * @return strReportRequestID
  **/
  @ApiModelProperty(value = "")


  public String getStrReportRequestID() {
    return strReportRequestID;
  }

  public void setStrReportRequestID(String strReportRequestID) {
    this.strReportRequestID = strReportRequestID;
  }

  public WSReportRequestDataOutWSResponse strReportName(String strReportName) {
    this.strReportName = strReportName;
    return this;
  }

  /**
   * Get strReportName
   * @return strReportName
  **/
  @ApiModelProperty(value = "")


  public String getStrReportName() {
    return strReportName;
  }

  public void setStrReportName(String strReportName) {
    this.strReportName = strReportName;
  }

  public WSReportRequestDataOutWSResponse strReportRequestTime(String strReportRequestTime) {
    this.strReportRequestTime = strReportRequestTime;
    return this;
  }

  /**
   * Get strReportRequestTime
   * @return strReportRequestTime
  **/
  @ApiModelProperty(value = "")


  public String getStrReportRequestTime() {
    return strReportRequestTime;
  }

  public void setStrReportRequestTime(String strReportRequestTime) {
    this.strReportRequestTime = strReportRequestTime;
  }

  public WSReportRequestDataOutWSResponse strReportResponseTime(String strReportResponseTime) {
    this.strReportResponseTime = strReportResponseTime;
    return this;
  }

  /**
   * Get strReportResponseTime
   * @return strReportResponseTime
  **/
  @ApiModelProperty(value = "")


  public String getStrReportResponseTime() {
    return strReportResponseTime;
  }

  public void setStrReportResponseTime(String strReportResponseTime) {
    this.strReportResponseTime = strReportResponseTime;
  }

  public WSReportRequestDataOutWSResponse strReportGenerationStatus(String strReportGenerationStatus) {
    this.strReportGenerationStatus = strReportGenerationStatus;
    return this;
  }

  /**
   * Get strReportGenerationStatus
   * @return strReportGenerationStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrReportGenerationStatus() {
    return strReportGenerationStatus;
  }

  public void setStrReportGenerationStatus(String strReportGenerationStatus) {
    this.strReportGenerationStatus = strReportGenerationStatus;
  }

  public WSReportRequestDataOutWSResponse strReportGenerationRemarks(String strReportGenerationRemarks) {
    this.strReportGenerationRemarks = strReportGenerationRemarks;
    return this;
  }

  /**
   * Get strReportGenerationRemarks
   * @return strReportGenerationRemarks
  **/
  @ApiModelProperty(value = "")


  public String getStrReportGenerationRemarks() {
    return strReportGenerationRemarks;
  }

  public void setStrReportGenerationRemarks(String strReportGenerationRemarks) {
    this.strReportGenerationRemarks = strReportGenerationRemarks;
  }

  public WSReportRequestDataOutWSResponse strReportFileLocation(String strReportFileLocation) {
    this.strReportFileLocation = strReportFileLocation;
    return this;
  }

  /**
   * Get strReportFileLocation
   * @return strReportFileLocation
  **/
  @ApiModelProperty(value = "")


  public String getStrReportFileLocation() {
    return strReportFileLocation;
  }

  public void setStrReportFileLocation(String strReportFileLocation) {
    this.strReportFileLocation = strReportFileLocation;
  }

  public WSReportRequestDataOutWSResponse reportData(String reportData) {
    this.reportData = reportData;
    return this;
  }

  /**
   * Get reportData
   * @return reportData
  **/
  @ApiModelProperty(example = "[[{'strColName':'value1','strColValue':'value2'},{'strColName':'value1','strColValue':'value2'}]]", value = "")


  public String getReportData() {
    return reportData;
  }

  public void setReportData(String reportData) {
    this.reportData = reportData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSReportRequestDataOutWSResponse wsReportRequestDataOutWSResponse = (WSReportRequestDataOutWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, wsReportRequestDataOutWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsReportRequestDataOutWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsReportRequestDataOutWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strReportOutuputFormat, wsReportRequestDataOutWSResponse.strReportOutuputFormat) &&
        Objects.equals(this.strReportRequestID, wsReportRequestDataOutWSResponse.strReportRequestID) &&
        Objects.equals(this.strReportName, wsReportRequestDataOutWSResponse.strReportName) &&
        Objects.equals(this.strReportRequestTime, wsReportRequestDataOutWSResponse.strReportRequestTime) &&
        Objects.equals(this.strReportResponseTime, wsReportRequestDataOutWSResponse.strReportResponseTime) &&
        Objects.equals(this.strReportGenerationStatus, wsReportRequestDataOutWSResponse.strReportGenerationStatus) &&
        Objects.equals(this.strReportGenerationRemarks, wsReportRequestDataOutWSResponse.strReportGenerationRemarks) &&
        Objects.equals(this.strReportFileLocation, wsReportRequestDataOutWSResponse.strReportFileLocation) &&
        Objects.equals(this.reportData, wsReportRequestDataOutWSResponse.reportData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strReportOutuputFormat, strReportRequestID, strReportName, strReportRequestTime, strReportResponseTime, strReportGenerationStatus, strReportGenerationRemarks, strReportFileLocation, reportData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSReportRequestDataOutWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strReportOutuputFormat: ").append(toIndentedString(strReportOutuputFormat)).append("\n");
    sb.append("    strReportRequestID: ").append(toIndentedString(strReportRequestID)).append("\n");
    sb.append("    strReportName: ").append(toIndentedString(strReportName)).append("\n");
    sb.append("    strReportRequestTime: ").append(toIndentedString(strReportRequestTime)).append("\n");
    sb.append("    strReportResponseTime: ").append(toIndentedString(strReportResponseTime)).append("\n");
    sb.append("    strReportGenerationStatus: ").append(toIndentedString(strReportGenerationStatus)).append("\n");
    sb.append("    strReportGenerationRemarks: ").append(toIndentedString(strReportGenerationRemarks)).append("\n");
    sb.append("    strReportFileLocation: ").append(toIndentedString(strReportFileLocation)).append("\n");
    sb.append("    reportData: ").append(toIndentedString(reportData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

