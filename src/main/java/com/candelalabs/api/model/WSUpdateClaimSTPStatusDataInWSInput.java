package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateClaimSTPStatusDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateClaimSTPStatusDataInWSInput   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strClaimSTPStatus")
  private String strClaimSTPStatus = null;

  public WSUpdateClaimSTPStatusDataInWSInput strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public WSUpdateClaimSTPStatusDataInWSInput strClaimSTPStatus(String strClaimSTPStatus) {
    this.strClaimSTPStatus = strClaimSTPStatus;
    return this;
  }

  /**
   * Get strClaimSTPStatus
   * @return strClaimSTPStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getStrClaimSTPStatus() {
    return strClaimSTPStatus;
  }

  public void setStrClaimSTPStatus(String strClaimSTPStatus) {
    this.strClaimSTPStatus = strClaimSTPStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateClaimSTPStatusDataInWSInput wsUpdateClaimSTPStatusDataInWSInput = (WSUpdateClaimSTPStatusDataInWSInput) o;
    return Objects.equals(this.strClaimNumber, wsUpdateClaimSTPStatusDataInWSInput.strClaimNumber) &&
        Objects.equals(this.strClaimSTPStatus, wsUpdateClaimSTPStatusDataInWSInput.strClaimSTPStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strClaimSTPStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateClaimSTPStatusDataInWSInput {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strClaimSTPStatus: ").append(toIndentedString(strClaimSTPStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

