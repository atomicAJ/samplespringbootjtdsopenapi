package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.BeneficiaryList;
import com.candelalabs.api.model.DocumentList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDataPortalOutput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDataPortalOutput   {
  @JsonProperty("strPortalClaimRequestNumber")
  private String strPortalClaimRequestNumber = null;

  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strClaimTypeLA")
  private String strClaimTypeLA = null;

  @JsonProperty("strClaimStatus")
  private String strClaimStatus = null;

  @JsonProperty("strClaimSubStatus")
  private String strClaimSubStatus = null;

  @JsonProperty("strSurgeryPerformed")
  private String strSurgeryPerformed = null;

  @JsonProperty("dtAdmissionDate")
  private String dtAdmissionDate = null;

  @JsonProperty("dtDischargeDate")
  private String dtDischargeDate = null;

  @JsonProperty("strHospitalName")
  private String strHospitalName = null;

  @JsonProperty("strDoctorName")
  private String strDoctorName = null;

  @JsonProperty("dblTotalBilling")
  private String dblTotalBilling = null;

  @JsonProperty("strTotalBillingCurrency")
  private String strTotalBillingCurrency = null;

  @JsonProperty("strLifeInsuredName")
  private String strLifeInsuredName = null;

  @JsonProperty("strInsuredClientID")
  private String strInsuredClientID = null;

  @JsonProperty("strClaimProcessingReason")
  private String strClaimProcessingReason = null;

  @JsonProperty("strAccountNumber")
  private String strAccountNumber = null;

  @JsonProperty("strAccountHolderName")
  private String strAccountHolderName = null;

  @JsonProperty("strBankName")
  private String strBankName = null;

  @JsonProperty("dtClaimSubmissionDate")
  private String dtClaimSubmissionDate = null;

  @JsonProperty("dtModifiedDate")
  private String dtModifiedDate = null;

  @JsonProperty("documentList")
  @Valid
  private List<DocumentList> documentList = null;

  @JsonProperty("beneficiaryList")
  @Valid
  private List<BeneficiaryList> beneficiaryList = null;

  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  public ClaimDataPortalOutput strPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
    return this;
  }

  /**
   * Get strPortalClaimRequestNumber
   * @return strPortalClaimRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPortalClaimRequestNumber() {
    return strPortalClaimRequestNumber;
  }

  public void setStrPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
  }

  public ClaimDataPortalOutput strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public ClaimDataPortalOutput strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public ClaimDataPortalOutput strClaimTypeLA(String strClaimTypeLA) {
    this.strClaimTypeLA = strClaimTypeLA;
    return this;
  }

  /**
   * Get strClaimTypeLA
   * @return strClaimTypeLA
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeLA() {
    return strClaimTypeLA;
  }

  public void setStrClaimTypeLA(String strClaimTypeLA) {
    this.strClaimTypeLA = strClaimTypeLA;
  }

  public ClaimDataPortalOutput strClaimStatus(String strClaimStatus) {
    this.strClaimStatus = strClaimStatus;
    return this;
  }

  /**
   * Get strClaimStatus
   * @return strClaimStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimStatus() {
    return strClaimStatus;
  }

  public void setStrClaimStatus(String strClaimStatus) {
    this.strClaimStatus = strClaimStatus;
  }

  public ClaimDataPortalOutput strClaimSubStatus(String strClaimSubStatus) {
    this.strClaimSubStatus = strClaimSubStatus;
    return this;
  }

  /**
   * Get strClaimSubStatus
   * @return strClaimSubStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimSubStatus() {
    return strClaimSubStatus;
  }

  public void setStrClaimSubStatus(String strClaimSubStatus) {
    this.strClaimSubStatus = strClaimSubStatus;
  }

  public ClaimDataPortalOutput strSurgeryPerformed(String strSurgeryPerformed) {
    this.strSurgeryPerformed = strSurgeryPerformed;
    return this;
  }

  /**
   * Get strSurgeryPerformed
   * @return strSurgeryPerformed
  **/
  @ApiModelProperty(value = "")


  public String getStrSurgeryPerformed() {
    return strSurgeryPerformed;
  }

  public void setStrSurgeryPerformed(String strSurgeryPerformed) {
    this.strSurgeryPerformed = strSurgeryPerformed;
  }

  public ClaimDataPortalOutput dtAdmissionDate(String dtAdmissionDate) {
    this.dtAdmissionDate = dtAdmissionDate;
    return this;
  }

  /**
   * Get dtAdmissionDate
   * @return dtAdmissionDate
  **/
  @ApiModelProperty(value = "")


  public String getDtAdmissionDate() {
    return dtAdmissionDate;
  }

  public void setDtAdmissionDate(String dtAdmissionDate) {
    this.dtAdmissionDate = dtAdmissionDate;
  }

  public ClaimDataPortalOutput dtDischargeDate(String dtDischargeDate) {
    this.dtDischargeDate = dtDischargeDate;
    return this;
  }

  /**
   * Get dtDischargeDate
   * @return dtDischargeDate
  **/
  @ApiModelProperty(value = "")


  public String getDtDischargeDate() {
    return dtDischargeDate;
  }

  public void setDtDischargeDate(String dtDischargeDate) {
    this.dtDischargeDate = dtDischargeDate;
  }

  public ClaimDataPortalOutput strHospitalName(String strHospitalName) {
    this.strHospitalName = strHospitalName;
    return this;
  }

  /**
   * Get strHospitalName
   * @return strHospitalName
  **/
  @ApiModelProperty(value = "")


  public String getStrHospitalName() {
    return strHospitalName;
  }

  public void setStrHospitalName(String strHospitalName) {
    this.strHospitalName = strHospitalName;
  }

  public ClaimDataPortalOutput strDoctorName(String strDoctorName) {
    this.strDoctorName = strDoctorName;
    return this;
  }

  /**
   * Get strDoctorName
   * @return strDoctorName
  **/
  @ApiModelProperty(value = "")


  public String getStrDoctorName() {
    return strDoctorName;
  }

  public void setStrDoctorName(String strDoctorName) {
    this.strDoctorName = strDoctorName;
  }

  public ClaimDataPortalOutput dblTotalBilling(String dblTotalBilling) {
    this.dblTotalBilling = dblTotalBilling;
    return this;
  }

  /**
   * Get dblTotalBilling
   * @return dblTotalBilling
  **/
  @ApiModelProperty(value = "")


  public String getDblTotalBilling() {
    return dblTotalBilling;
  }

  public void setDblTotalBilling(String dblTotalBilling) {
    this.dblTotalBilling = dblTotalBilling;
  }

  public ClaimDataPortalOutput strTotalBillingCurrency(String strTotalBillingCurrency) {
    this.strTotalBillingCurrency = strTotalBillingCurrency;
    return this;
  }

  /**
   * Get strTotalBillingCurrency
   * @return strTotalBillingCurrency
  **/
  @ApiModelProperty(value = "")


  public String getStrTotalBillingCurrency() {
    return strTotalBillingCurrency;
  }

  public void setStrTotalBillingCurrency(String strTotalBillingCurrency) {
    this.strTotalBillingCurrency = strTotalBillingCurrency;
  }

  public ClaimDataPortalOutput strLifeInsuredName(String strLifeInsuredName) {
    this.strLifeInsuredName = strLifeInsuredName;
    return this;
  }

  /**
   * Get strLifeInsuredName
   * @return strLifeInsuredName
  **/
  @ApiModelProperty(value = "")


  public String getStrLifeInsuredName() {
    return strLifeInsuredName;
  }

  public void setStrLifeInsuredName(String strLifeInsuredName) {
    this.strLifeInsuredName = strLifeInsuredName;
  }

  public ClaimDataPortalOutput strInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
    return this;
  }

  /**
   * Get strInsuredClientID
   * @return strInsuredClientID
  **/
  @ApiModelProperty(value = "")


  public String getStrInsuredClientID() {
    return strInsuredClientID;
  }

  public void setStrInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
  }

  public ClaimDataPortalOutput strClaimProcessingReason(String strClaimProcessingReason) {
    this.strClaimProcessingReason = strClaimProcessingReason;
    return this;
  }

  /**
   * Get strClaimProcessingReason
   * @return strClaimProcessingReason
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimProcessingReason() {
    return strClaimProcessingReason;
  }

  public void setStrClaimProcessingReason(String strClaimProcessingReason) {
    this.strClaimProcessingReason = strClaimProcessingReason;
  }

  public ClaimDataPortalOutput strAccountNumber(String strAccountNumber) {
    this.strAccountNumber = strAccountNumber;
    return this;
  }

  /**
   * Get strAccountNumber
   * @return strAccountNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrAccountNumber() {
    return strAccountNumber;
  }

  public void setStrAccountNumber(String strAccountNumber) {
    this.strAccountNumber = strAccountNumber;
  }

  public ClaimDataPortalOutput strAccountHolderName(String strAccountHolderName) {
    this.strAccountHolderName = strAccountHolderName;
    return this;
  }

  /**
   * Get strAccountHolderName
   * @return strAccountHolderName
  **/
  @ApiModelProperty(value = "")


  public String getStrAccountHolderName() {
    return strAccountHolderName;
  }

  public void setStrAccountHolderName(String strAccountHolderName) {
    this.strAccountHolderName = strAccountHolderName;
  }

  public ClaimDataPortalOutput strBankName(String strBankName) {
    this.strBankName = strBankName;
    return this;
  }

  /**
   * Get strBankName
   * @return strBankName
  **/
  @ApiModelProperty(value = "")


  public String getStrBankName() {
    return strBankName;
  }

  public void setStrBankName(String strBankName) {
    this.strBankName = strBankName;
  }

  public ClaimDataPortalOutput dtClaimSubmissionDate(String dtClaimSubmissionDate) {
    this.dtClaimSubmissionDate = dtClaimSubmissionDate;
    return this;
  }

  /**
   * Get dtClaimSubmissionDate
   * @return dtClaimSubmissionDate
  **/
  @ApiModelProperty(value = "")


  public String getDtClaimSubmissionDate() {
    return dtClaimSubmissionDate;
  }

  public void setDtClaimSubmissionDate(String dtClaimSubmissionDate) {
    this.dtClaimSubmissionDate = dtClaimSubmissionDate;
  }

  public ClaimDataPortalOutput dtModifiedDate(String dtModifiedDate) {
    this.dtModifiedDate = dtModifiedDate;
    return this;
  }

  /**
   * Get dtModifiedDate
   * @return dtModifiedDate
  **/
  @ApiModelProperty(value = "")


  public String getDtModifiedDate() {
    return dtModifiedDate;
  }

  public void setDtModifiedDate(String dtModifiedDate) {
    this.dtModifiedDate = dtModifiedDate;
  }

  public ClaimDataPortalOutput documentList(List<DocumentList> documentList) {
    this.documentList = documentList;
    return this;
  }

  public ClaimDataPortalOutput addDocumentListItem(DocumentList documentListItem) {
    if (this.documentList == null) {
      this.documentList = new ArrayList<>();
    }
    this.documentList.add(documentListItem);
    return this;
  }

  /**
   * Get documentList
   * @return documentList
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<DocumentList> getDocumentList() {
    return documentList;
  }

  public void setDocumentList(List<DocumentList> documentList) {
    this.documentList = documentList;
  }

  public ClaimDataPortalOutput beneficiaryList(List<BeneficiaryList> beneficiaryList) {
    this.beneficiaryList = beneficiaryList;
    return this;
  }

  public ClaimDataPortalOutput addBeneficiaryListItem(BeneficiaryList beneficiaryListItem) {
    if (this.beneficiaryList == null) {
      this.beneficiaryList = new ArrayList<>();
    }
    this.beneficiaryList.add(beneficiaryListItem);
    return this;
  }

  /**
   * Get beneficiaryList
   * @return beneficiaryList
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<BeneficiaryList> getBeneficiaryList() {
    return beneficiaryList;
  }

  public void setBeneficiaryList(List<BeneficiaryList> beneficiaryList) {
    this.beneficiaryList = beneficiaryList;
  }

  public ClaimDataPortalOutput wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public ClaimDataPortalOutput wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public ClaimDataPortalOutput wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDataPortalOutput claimDataPortalOutput = (ClaimDataPortalOutput) o;
    return Objects.equals(this.strPortalClaimRequestNumber, claimDataPortalOutput.strPortalClaimRequestNumber) &&
        Objects.equals(this.strClaimNumber, claimDataPortalOutput.strClaimNumber) &&
        Objects.equals(this.strPolicyNumber, claimDataPortalOutput.strPolicyNumber) &&
        Objects.equals(this.strClaimTypeLA, claimDataPortalOutput.strClaimTypeLA) &&
        Objects.equals(this.strClaimStatus, claimDataPortalOutput.strClaimStatus) &&
        Objects.equals(this.strClaimSubStatus, claimDataPortalOutput.strClaimSubStatus) &&
        Objects.equals(this.strSurgeryPerformed, claimDataPortalOutput.strSurgeryPerformed) &&
        Objects.equals(this.dtAdmissionDate, claimDataPortalOutput.dtAdmissionDate) &&
        Objects.equals(this.dtDischargeDate, claimDataPortalOutput.dtDischargeDate) &&
        Objects.equals(this.strHospitalName, claimDataPortalOutput.strHospitalName) &&
        Objects.equals(this.strDoctorName, claimDataPortalOutput.strDoctorName) &&
        Objects.equals(this.dblTotalBilling, claimDataPortalOutput.dblTotalBilling) &&
        Objects.equals(this.strTotalBillingCurrency, claimDataPortalOutput.strTotalBillingCurrency) &&
        Objects.equals(this.strLifeInsuredName, claimDataPortalOutput.strLifeInsuredName) &&
        Objects.equals(this.strInsuredClientID, claimDataPortalOutput.strInsuredClientID) &&
        Objects.equals(this.strClaimProcessingReason, claimDataPortalOutput.strClaimProcessingReason) &&
        Objects.equals(this.strAccountNumber, claimDataPortalOutput.strAccountNumber) &&
        Objects.equals(this.strAccountHolderName, claimDataPortalOutput.strAccountHolderName) &&
        Objects.equals(this.strBankName, claimDataPortalOutput.strBankName) &&
        Objects.equals(this.dtClaimSubmissionDate, claimDataPortalOutput.dtClaimSubmissionDate) &&
        Objects.equals(this.dtModifiedDate, claimDataPortalOutput.dtModifiedDate) &&
        Objects.equals(this.documentList, claimDataPortalOutput.documentList) &&
        Objects.equals(this.beneficiaryList, claimDataPortalOutput.beneficiaryList) &&
        Objects.equals(this.wsProcessingStatus, claimDataPortalOutput.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, claimDataPortalOutput.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, claimDataPortalOutput.wsSuccessMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strPortalClaimRequestNumber, strClaimNumber, strPolicyNumber, strClaimTypeLA, strClaimStatus, strClaimSubStatus, strSurgeryPerformed, dtAdmissionDate, dtDischargeDate, strHospitalName, strDoctorName, dblTotalBilling, strTotalBillingCurrency, strLifeInsuredName, strInsuredClientID, strClaimProcessingReason, strAccountNumber, strAccountHolderName, strBankName, dtClaimSubmissionDate, dtModifiedDate, documentList, beneficiaryList, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDataPortalOutput {\n");
    
    sb.append("    strPortalClaimRequestNumber: ").append(toIndentedString(strPortalClaimRequestNumber)).append("\n");
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strClaimTypeLA: ").append(toIndentedString(strClaimTypeLA)).append("\n");
    sb.append("    strClaimStatus: ").append(toIndentedString(strClaimStatus)).append("\n");
    sb.append("    strClaimSubStatus: ").append(toIndentedString(strClaimSubStatus)).append("\n");
    sb.append("    strSurgeryPerformed: ").append(toIndentedString(strSurgeryPerformed)).append("\n");
    sb.append("    dtAdmissionDate: ").append(toIndentedString(dtAdmissionDate)).append("\n");
    sb.append("    dtDischargeDate: ").append(toIndentedString(dtDischargeDate)).append("\n");
    sb.append("    strHospitalName: ").append(toIndentedString(strHospitalName)).append("\n");
    sb.append("    strDoctorName: ").append(toIndentedString(strDoctorName)).append("\n");
    sb.append("    dblTotalBilling: ").append(toIndentedString(dblTotalBilling)).append("\n");
    sb.append("    strTotalBillingCurrency: ").append(toIndentedString(strTotalBillingCurrency)).append("\n");
    sb.append("    strLifeInsuredName: ").append(toIndentedString(strLifeInsuredName)).append("\n");
    sb.append("    strInsuredClientID: ").append(toIndentedString(strInsuredClientID)).append("\n");
    sb.append("    strClaimProcessingReason: ").append(toIndentedString(strClaimProcessingReason)).append("\n");
    sb.append("    strAccountNumber: ").append(toIndentedString(strAccountNumber)).append("\n");
    sb.append("    strAccountHolderName: ").append(toIndentedString(strAccountHolderName)).append("\n");
    sb.append("    strBankName: ").append(toIndentedString(strBankName)).append("\n");
    sb.append("    dtClaimSubmissionDate: ").append(toIndentedString(dtClaimSubmissionDate)).append("\n");
    sb.append("    dtModifiedDate: ").append(toIndentedString(dtModifiedDate)).append("\n");
    sb.append("    documentList: ").append(toIndentedString(documentList)).append("\n");
    sb.append("    beneficiaryList: ").append(toIndentedString(beneficiaryList)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

