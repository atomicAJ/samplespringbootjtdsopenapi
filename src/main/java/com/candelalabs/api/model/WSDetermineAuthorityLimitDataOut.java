package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSDetermineAuthorityLimitDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSDetermineAuthorityLimitDataOut   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("APPROVALAUTHORITY")
  private Boolean APPROVALAUTHORITY = null;

  @JsonProperty("REJECTIONAUTHORITY")
  private Boolean REJECTIONAUTHORITY = null;

  public WSDetermineAuthorityLimitDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSDetermineAuthorityLimitDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSDetermineAuthorityLimitDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public WSDetermineAuthorityLimitDataOut APPROVALAUTHORITY(Boolean APPROVALAUTHORITY) {
    this.APPROVALAUTHORITY = APPROVALAUTHORITY;
    return this;
  }

  /**
   * Get APPROVALAUTHORITY
   * @return APPROVALAUTHORITY
  **/
  @ApiModelProperty(example = "false", required = true, value = "")
  @NotNull


  public Boolean isAPPROVALAUTHORITY() {
    return APPROVALAUTHORITY;
  }

  public void setAPPROVALAUTHORITY(Boolean APPROVALAUTHORITY) {
    this.APPROVALAUTHORITY = APPROVALAUTHORITY;
  }

  public WSDetermineAuthorityLimitDataOut REJECTIONAUTHORITY(Boolean REJECTIONAUTHORITY) {
    this.REJECTIONAUTHORITY = REJECTIONAUTHORITY;
    return this;
  }

  /**
   * Get REJECTIONAUTHORITY
   * @return REJECTIONAUTHORITY
  **/
  @ApiModelProperty(example = "false", required = true, value = "")
  @NotNull


  public Boolean isREJECTIONAUTHORITY() {
    return REJECTIONAUTHORITY;
  }

  public void setREJECTIONAUTHORITY(Boolean REJECTIONAUTHORITY) {
    this.REJECTIONAUTHORITY = REJECTIONAUTHORITY;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSDetermineAuthorityLimitDataOut wsDetermineAuthorityLimitDataOut = (WSDetermineAuthorityLimitDataOut) o;
    return Objects.equals(this.wsProcessingStatus, wsDetermineAuthorityLimitDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsDetermineAuthorityLimitDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsDetermineAuthorityLimitDataOut.wsSuccessMessage) &&
        Objects.equals(this.APPROVALAUTHORITY, wsDetermineAuthorityLimitDataOut.APPROVALAUTHORITY) &&
        Objects.equals(this.REJECTIONAUTHORITY, wsDetermineAuthorityLimitDataOut.REJECTIONAUTHORITY);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, APPROVALAUTHORITY, REJECTIONAUTHORITY);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSDetermineAuthorityLimitDataOut {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    APPROVALAUTHORITY: ").append(toIndentedString(APPROVALAUTHORITY)).append("\n");
    sb.append("    REJECTIONAUTHORITY: ").append(toIndentedString(REJECTIONAUTHORITY)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

