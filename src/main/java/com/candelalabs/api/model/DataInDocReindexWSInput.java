package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.DataInDocReindexWSInputDocumentSearchInfo;
import com.candelalabs.api.model.DataInDocReindexWSInputDocumentUpdateInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInDocReindexWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInDocReindexWSInput   {
  @JsonProperty("DocumentSearchInfo")
  private DataInDocReindexWSInputDocumentSearchInfo documentSearchInfo = null;

  @JsonProperty("DocumentUpdateInfo")
  private DataInDocReindexWSInputDocumentUpdateInfo documentUpdateInfo = null;

  public DataInDocReindexWSInput documentSearchInfo(DataInDocReindexWSInputDocumentSearchInfo documentSearchInfo) {
    this.documentSearchInfo = documentSearchInfo;
    return this;
  }

  /**
   * Get documentSearchInfo
   * @return documentSearchInfo
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataInDocReindexWSInputDocumentSearchInfo getDocumentSearchInfo() {
    return documentSearchInfo;
  }

  public void setDocumentSearchInfo(DataInDocReindexWSInputDocumentSearchInfo documentSearchInfo) {
    this.documentSearchInfo = documentSearchInfo;
  }

  public DataInDocReindexWSInput documentUpdateInfo(DataInDocReindexWSInputDocumentUpdateInfo documentUpdateInfo) {
    this.documentUpdateInfo = documentUpdateInfo;
    return this;
  }

  /**
   * Get documentUpdateInfo
   * @return documentUpdateInfo
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataInDocReindexWSInputDocumentUpdateInfo getDocumentUpdateInfo() {
    return documentUpdateInfo;
  }

  public void setDocumentUpdateInfo(DataInDocReindexWSInputDocumentUpdateInfo documentUpdateInfo) {
    this.documentUpdateInfo = documentUpdateInfo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInDocReindexWSInput dataInDocReindexWSInput = (DataInDocReindexWSInput) o;
    return Objects.equals(this.documentSearchInfo, dataInDocReindexWSInput.documentSearchInfo) &&
        Objects.equals(this.documentUpdateInfo, dataInDocReindexWSInput.documentUpdateInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(documentSearchInfo, documentUpdateInfo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInDocReindexWSInput {\n");
    
    sb.append("    documentSearchInfo: ").append(toIndentedString(documentSearchInfo)).append("\n");
    sb.append("    documentUpdateInfo: ").append(toIndentedString(documentUpdateInfo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

