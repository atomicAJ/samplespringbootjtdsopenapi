package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails   {
  @JsonProperty("strPRDPolicyNumber")
  private String strPRDPolicyNumber = null;

  @JsonProperty("strPRDProductCode")
  private String strPRDProductCode = null;

  @JsonProperty("strPRDProductName")
  private String strPRDProductName = null;

  @JsonProperty("strPRDOwnerName")
  private String strPRDOwnerName = null;

  @JsonProperty("strPRDInsuredName")
  private String strPRDInsuredName = null;

  @JsonProperty("strPRDClientRole")
  private String strPRDClientRole = null;

  @JsonProperty("strPRDPolicyStatus")
  private String strPRDPolicyStatus = null;

  @JsonProperty("dblPRDSumAssured")
  private BigDecimal dblPRDSumAssured = null;

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDPolicyNumber(String strPRDPolicyNumber) {
    this.strPRDPolicyNumber = strPRDPolicyNumber;
    return this;
  }

  /**
   * Get strPRDPolicyNumber
   * @return strPRDPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDPolicyNumber() {
    return strPRDPolicyNumber;
  }

  public void setStrPRDPolicyNumber(String strPRDPolicyNumber) {
    this.strPRDPolicyNumber = strPRDPolicyNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDProductCode(String strPRDProductCode) {
    this.strPRDProductCode = strPRDProductCode;
    return this;
  }

  /**
   * Get strPRDProductCode
   * @return strPRDProductCode
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDProductCode() {
    return strPRDProductCode;
  }

  public void setStrPRDProductCode(String strPRDProductCode) {
    this.strPRDProductCode = strPRDProductCode;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDProductName(String strPRDProductName) {
    this.strPRDProductName = strPRDProductName;
    return this;
  }

  /**
   * Get strPRDProductName
   * @return strPRDProductName
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDProductName() {
    return strPRDProductName;
  }

  public void setStrPRDProductName(String strPRDProductName) {
    this.strPRDProductName = strPRDProductName;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDOwnerName(String strPRDOwnerName) {
    this.strPRDOwnerName = strPRDOwnerName;
    return this;
  }

  /**
   * Get strPRDOwnerName
   * @return strPRDOwnerName
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDOwnerName() {
    return strPRDOwnerName;
  }

  public void setStrPRDOwnerName(String strPRDOwnerName) {
    this.strPRDOwnerName = strPRDOwnerName;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDInsuredName(String strPRDInsuredName) {
    this.strPRDInsuredName = strPRDInsuredName;
    return this;
  }

  /**
   * Get strPRDInsuredName
   * @return strPRDInsuredName
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDInsuredName() {
    return strPRDInsuredName;
  }

  public void setStrPRDInsuredName(String strPRDInsuredName) {
    this.strPRDInsuredName = strPRDInsuredName;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDClientRole(String strPRDClientRole) {
    this.strPRDClientRole = strPRDClientRole;
    return this;
  }

  /**
   * Get strPRDClientRole
   * @return strPRDClientRole
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDClientRole() {
    return strPRDClientRole;
  }

  public void setStrPRDClientRole(String strPRDClientRole) {
    this.strPRDClientRole = strPRDClientRole;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails strPRDPolicyStatus(String strPRDPolicyStatus) {
    this.strPRDPolicyStatus = strPRDPolicyStatus;
    return this;
  }

  /**
   * Get strPRDPolicyStatus
   * @return strPRDPolicyStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrPRDPolicyStatus() {
    return strPRDPolicyStatus;
  }

  public void setStrPRDPolicyStatus(String strPRDPolicyStatus) {
    this.strPRDPolicyStatus = strPRDPolicyStatus;
  }

  public ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails dblPRDSumAssured(BigDecimal dblPRDSumAssured) {
    this.dblPRDSumAssured = dblPRDSumAssured;
    return this;
  }

  /**
   * Get dblPRDSumAssured
   * @return dblPRDSumAssured
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDblPRDSumAssured() {
    return dblPRDSumAssured;
  }

  public void setDblPRDSumAssured(BigDecimal dblPRDSumAssured) {
    this.dblPRDSumAssured = dblPRDSumAssured;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails = (ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails) o;
    return Objects.equals(this.strPRDPolicyNumber, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDPolicyNumber) &&
        Objects.equals(this.strPRDProductCode, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDProductCode) &&
        Objects.equals(this.strPRDProductName, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDProductName) &&
        Objects.equals(this.strPRDOwnerName, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDOwnerName) &&
        Objects.equals(this.strPRDInsuredName, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDInsuredName) &&
        Objects.equals(this.strPRDClientRole, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDClientRole) &&
        Objects.equals(this.strPRDPolicyStatus, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.strPRDPolicyStatus) &&
        Objects.equals(this.dblPRDSumAssured, claimWorksheetUIDataClaimWorksheetPolicyRelatedDetails.dblPRDSumAssured);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strPRDPolicyNumber, strPRDProductCode, strPRDProductName, strPRDOwnerName, strPRDInsuredName, strPRDClientRole, strPRDPolicyStatus, dblPRDSumAssured);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails {\n");
    
    sb.append("    strPRDPolicyNumber: ").append(toIndentedString(strPRDPolicyNumber)).append("\n");
    sb.append("    strPRDProductCode: ").append(toIndentedString(strPRDProductCode)).append("\n");
    sb.append("    strPRDProductName: ").append(toIndentedString(strPRDProductName)).append("\n");
    sb.append("    strPRDOwnerName: ").append(toIndentedString(strPRDOwnerName)).append("\n");
    sb.append("    strPRDInsuredName: ").append(toIndentedString(strPRDInsuredName)).append("\n");
    sb.append("    strPRDClientRole: ").append(toIndentedString(strPRDClientRole)).append("\n");
    sb.append("    strPRDPolicyStatus: ").append(toIndentedString(strPRDPolicyStatus)).append("\n");
    sb.append("    dblPRDSumAssured: ").append(toIndentedString(dblPRDSumAssured)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

