package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetBankCodeTypebyFactoringHouseWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetBankCodeTypebyFactoringHouseWSInput   {
  @JsonProperty("strFactoringHouse")
  private String strFactoringHouse = null;

  public DataInGetBankCodeTypebyFactoringHouseWSInput strFactoringHouse(String strFactoringHouse) {
    this.strFactoringHouse = strFactoringHouse;
    return this;
  }

  /**
   * Get strFactoringHouse
   * @return strFactoringHouse
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrFactoringHouse() {
    return strFactoringHouse;
  }

  public void setStrFactoringHouse(String strFactoringHouse) {
    this.strFactoringHouse = strFactoringHouse;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetBankCodeTypebyFactoringHouseWSInput dataInGetBankCodeTypebyFactoringHouseWSInput = (DataInGetBankCodeTypebyFactoringHouseWSInput) o;
    return Objects.equals(this.strFactoringHouse, dataInGetBankCodeTypebyFactoringHouseWSInput.strFactoringHouse);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strFactoringHouse);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetBankCodeTypebyFactoringHouseWSInput {\n");
    
    sb.append("    strFactoringHouse: ").append(toIndentedString(strFactoringHouse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

