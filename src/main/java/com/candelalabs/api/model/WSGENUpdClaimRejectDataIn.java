package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGENUpdClaimRejectDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGENUpdClaimRejectDataIn   {
  @JsonProperty("claimNo")
  private String claimNo = null;

  @JsonProperty("activityId")
  private String activityId = null;

  public WSGENUpdClaimRejectDataIn claimNo(String claimNo) {
    this.claimNo = claimNo;
    return this;
  }

  /**
   * Get claimNo
   * @return claimNo
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getClaimNo() {
    return claimNo;
  }

  public void setClaimNo(String claimNo) {
    this.claimNo = claimNo;
  }

  public WSGENUpdClaimRejectDataIn activityId(String activityId) {
    this.activityId = activityId;
    return this;
  }

  /**
   * Get activityId
   * @return activityId
  **/
  @ApiModelProperty(example = "string", required = true, value = "")
  @NotNull


  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGENUpdClaimRejectDataIn wsGENUpdClaimRejectDataIn = (WSGENUpdClaimRejectDataIn) o;
    return Objects.equals(this.claimNo, wsGENUpdClaimRejectDataIn.claimNo) &&
        Objects.equals(this.activityId, wsGENUpdClaimRejectDataIn.activityId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNo, activityId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGENUpdClaimRejectDataIn {\n");
    
    sb.append("    claimNo: ").append(toIndentedString(claimNo)).append("\n");
    sb.append("    activityId: ").append(toIndentedString(activityId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

