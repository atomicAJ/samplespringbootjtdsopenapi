package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSupdateClaimDecisionDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSupdateClaimDecisionDataIn   {
  @JsonProperty("CLAIMNUMBER")
  private String CLAIMNUMBER = null;

  @JsonProperty("DECISION")
  private String DECISION = null;

  public WSupdateClaimDecisionDataIn CLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
    return this;
  }

  /**
   * Get CLAIMNUMBER
   * @return CLAIMNUMBER
  **/
  @ApiModelProperty(example = "[CLAIMNUMBER]", required = true, value = "")
  @NotNull


  public String getCLAIMNUMBER() {
    return CLAIMNUMBER;
  }

  public void setCLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
  }

  public WSupdateClaimDecisionDataIn DECISION(String DECISION) {
    this.DECISION = DECISION;
    return this;
  }

  /**
   * Get DECISION
   * @return DECISION
  **/
  @ApiModelProperty(example = "[APPROVE]", value = "")


  public String getDECISION() {
    return DECISION;
  }

  public void setDECISION(String DECISION) {
    this.DECISION = DECISION;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSupdateClaimDecisionDataIn wsupdateClaimDecisionDataIn = (WSupdateClaimDecisionDataIn) o;
    return Objects.equals(this.CLAIMNUMBER, wsupdateClaimDecisionDataIn.CLAIMNUMBER) &&
        Objects.equals(this.DECISION, wsupdateClaimDecisionDataIn.DECISION);
  }

  @Override
  public int hashCode() {
    return Objects.hash(CLAIMNUMBER, DECISION);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSupdateClaimDecisionDataIn {\n");
    
    sb.append("    CLAIMNUMBER: ").append(toIndentedString(CLAIMNUMBER)).append("\n");
    sb.append("    DECISION: ").append(toIndentedString(DECISION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

