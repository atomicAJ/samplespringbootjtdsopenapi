package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSPortalServiceDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSPortalServiceDataIn   {
  @JsonProperty("strPortalClaimRequestNumber")
  private String strPortalClaimRequestNumber = null;

  @JsonProperty("dtPortalClaimRequestCreationDate")
  private String dtPortalClaimRequestCreationDate = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strOwnerClientID")
  private String strOwnerClientID = null;

  @JsonProperty("strPayeeName")
  private String strPayeeName = null;

  @JsonProperty("strInsuredClientName")
  private String strInsuredClientName = null;

  @JsonProperty("strInsuredClientID")
  private String strInsuredClientID = null;

  @JsonProperty("strFormClaimType")
  private String strFormClaimType = null;

  @JsonProperty("strFormBenefitType")
  private String strFormBenefitType = null;

  @JsonProperty("strSurgeryPerformed")
  private String strSurgeryPerformed = null;

  @JsonProperty("dtTreatmentStartDate")
  private String dtTreatmentStartDate = null;

  @JsonProperty("dtTreatmentCompletionDate")
  private String dtTreatmentCompletionDate = null;

  @JsonProperty("strHospitalName")
  private String strHospitalName = null;

  @JsonProperty("strCurrency")
  private String strCurrency = null;

  @JsonProperty("dblTotalBillingAmount")
  private String dblTotalBillingAmount = null;

  @JsonProperty("strPhysician")
  private String strPhysician = null;

  @JsonProperty("strBankAccountNumber")
  private String strBankAccountNumber = null;

  @JsonProperty("strPaymentAccountOwnerName")
  private String strPaymentAccountOwnerName = null;

  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  public WSPortalServiceDataIn strPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
    return this;
  }

  /**
   * strPortalClaimRequestNumber
   * @return strPortalClaimRequestNumber
  **/
  @ApiModelProperty(example = "12323", value = "strPortalClaimRequestNumber")


  public String getStrPortalClaimRequestNumber() {
    return strPortalClaimRequestNumber;
  }

  public void setStrPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
  }

  public WSPortalServiceDataIn dtPortalClaimRequestCreationDate(String dtPortalClaimRequestCreationDate) {
    this.dtPortalClaimRequestCreationDate = dtPortalClaimRequestCreationDate;
    return this;
  }

  /**
   * Get dtPortalClaimRequestCreationDate
   * @return dtPortalClaimRequestCreationDate
  **/
  @ApiModelProperty(example = "01/10/2018", value = "")


  public String getDtPortalClaimRequestCreationDate() {
    return dtPortalClaimRequestCreationDate;
  }

  public void setDtPortalClaimRequestCreationDate(String dtPortalClaimRequestCreationDate) {
    this.dtPortalClaimRequestCreationDate = dtPortalClaimRequestCreationDate;
  }

  public WSPortalServiceDataIn strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(example = "123123", value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public WSPortalServiceDataIn strOwnerClientID(String strOwnerClientID) {
    this.strOwnerClientID = strOwnerClientID;
    return this;
  }

  /**
   * Get strOwnerClientID
   * @return strOwnerClientID
  **/
  @ApiModelProperty(example = "123123", value = "")


  public String getStrOwnerClientID() {
    return strOwnerClientID;
  }

  public void setStrOwnerClientID(String strOwnerClientID) {
    this.strOwnerClientID = strOwnerClientID;
  }

  public WSPortalServiceDataIn strPayeeName(String strPayeeName) {
    this.strPayeeName = strPayeeName;
    return this;
  }

  /**
   * Get strPayeeName
   * @return strPayeeName
  **/
  @ApiModelProperty(example = "abdnbcd", value = "")


  public String getStrPayeeName() {
    return strPayeeName;
  }

  public void setStrPayeeName(String strPayeeName) {
    this.strPayeeName = strPayeeName;
  }

  public WSPortalServiceDataIn strInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
    return this;
  }

  /**
   * Get strInsuredClientName
   * @return strInsuredClientName
  **/
  @ApiModelProperty(example = "abdnbcd", value = "")


  public String getStrInsuredClientName() {
    return strInsuredClientName;
  }

  public void setStrInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
  }

  public WSPortalServiceDataIn strInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
    return this;
  }

  /**
   * Get strInsuredClientID
   * @return strInsuredClientID
  **/
  @ApiModelProperty(example = "123123", value = "")


  public String getStrInsuredClientID() {
    return strInsuredClientID;
  }

  public void setStrInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
  }

  public WSPortalServiceDataIn strFormClaimType(String strFormClaimType) {
    this.strFormClaimType = strFormClaimType;
    return this;
  }

  /**
   * Get strFormClaimType
   * @return strFormClaimType
  **/
  @ApiModelProperty(example = "Living Benefit", value = "")


  public String getStrFormClaimType() {
    return strFormClaimType;
  }

  public void setStrFormClaimType(String strFormClaimType) {
    this.strFormClaimType = strFormClaimType;
  }

  public WSPortalServiceDataIn strFormBenefitType(String strFormBenefitType) {
    this.strFormBenefitType = strFormBenefitType;
    return this;
  }

  /**
   * Get strFormBenefitType
   * @return strFormBenefitType
  **/
  @ApiModelProperty(example = "Waiver", value = "")


  public String getStrFormBenefitType() {
    return strFormBenefitType;
  }

  public void setStrFormBenefitType(String strFormBenefitType) {
    this.strFormBenefitType = strFormBenefitType;
  }

  public WSPortalServiceDataIn strSurgeryPerformed(String strSurgeryPerformed) {
    this.strSurgeryPerformed = strSurgeryPerformed;
    return this;
  }

  /**
   * Get strSurgeryPerformed
   * @return strSurgeryPerformed
  **/
  @ApiModelProperty(example = "N", value = "")


  public String getStrSurgeryPerformed() {
    return strSurgeryPerformed;
  }

  public void setStrSurgeryPerformed(String strSurgeryPerformed) {
    this.strSurgeryPerformed = strSurgeryPerformed;
  }

  public WSPortalServiceDataIn dtTreatmentStartDate(String dtTreatmentStartDate) {
    this.dtTreatmentStartDate = dtTreatmentStartDate;
    return this;
  }

  /**
   * Get dtTreatmentStartDate
   * @return dtTreatmentStartDate
  **/
  @ApiModelProperty(example = "01/10/2018", value = "")


  public String getDtTreatmentStartDate() {
    return dtTreatmentStartDate;
  }

  public void setDtTreatmentStartDate(String dtTreatmentStartDate) {
    this.dtTreatmentStartDate = dtTreatmentStartDate;
  }

  public WSPortalServiceDataIn dtTreatmentCompletionDate(String dtTreatmentCompletionDate) {
    this.dtTreatmentCompletionDate = dtTreatmentCompletionDate;
    return this;
  }

  /**
   * Get dtTreatmentCompletionDate
   * @return dtTreatmentCompletionDate
  **/
  @ApiModelProperty(example = "01/10/2018", value = "")


  public String getDtTreatmentCompletionDate() {
    return dtTreatmentCompletionDate;
  }

  public void setDtTreatmentCompletionDate(String dtTreatmentCompletionDate) {
    this.dtTreatmentCompletionDate = dtTreatmentCompletionDate;
  }

  public WSPortalServiceDataIn strHospitalName(String strHospitalName) {
    this.strHospitalName = strHospitalName;
    return this;
  }

  /**
   * Get strHospitalName
   * @return strHospitalName
  **/
  @ApiModelProperty(example = "abccabc", value = "")


  public String getStrHospitalName() {
    return strHospitalName;
  }

  public void setStrHospitalName(String strHospitalName) {
    this.strHospitalName = strHospitalName;
  }

  public WSPortalServiceDataIn strCurrency(String strCurrency) {
    this.strCurrency = strCurrency;
    return this;
  }

  /**
   * Get strCurrency
   * @return strCurrency
  **/
  @ApiModelProperty(example = "IDR", value = "")


  public String getStrCurrency() {
    return strCurrency;
  }

  public void setStrCurrency(String strCurrency) {
    this.strCurrency = strCurrency;
  }

  public WSPortalServiceDataIn dblTotalBillingAmount(String dblTotalBillingAmount) {
    this.dblTotalBillingAmount = dblTotalBillingAmount;
    return this;
  }

  /**
   * Get dblTotalBillingAmount
   * @return dblTotalBillingAmount
  **/
  @ApiModelProperty(example = "12312321.11", value = "")


  public String getDblTotalBillingAmount() {
    return dblTotalBillingAmount;
  }

  public void setDblTotalBillingAmount(String dblTotalBillingAmount) {
    this.dblTotalBillingAmount = dblTotalBillingAmount;
  }

  public WSPortalServiceDataIn strPhysician(String strPhysician) {
    this.strPhysician = strPhysician;
    return this;
  }

  /**
   * Get strPhysician
   * @return strPhysician
  **/
  @ApiModelProperty(example = "abccba", value = "")


  public String getStrPhysician() {
    return strPhysician;
  }

  public void setStrPhysician(String strPhysician) {
    this.strPhysician = strPhysician;
  }

  public WSPortalServiceDataIn strBankAccountNumber(String strBankAccountNumber) {
    this.strBankAccountNumber = strBankAccountNumber;
    return this;
  }

  /**
   * Get strBankAccountNumber
   * @return strBankAccountNumber
  **/
  @ApiModelProperty(example = "abccba123", value = "")


  public String getStrBankAccountNumber() {
    return strBankAccountNumber;
  }

  public void setStrBankAccountNumber(String strBankAccountNumber) {
    this.strBankAccountNumber = strBankAccountNumber;
  }

  public WSPortalServiceDataIn strPaymentAccountOwnerName(String strPaymentAccountOwnerName) {
    this.strPaymentAccountOwnerName = strPaymentAccountOwnerName;
    return this;
  }

  /**
   * Get strPaymentAccountOwnerName
   * @return strPaymentAccountOwnerName
  **/
  @ApiModelProperty(example = "abccba", value = "")


  public String getStrPaymentAccountOwnerName() {
    return strPaymentAccountOwnerName;
  }

  public void setStrPaymentAccountOwnerName(String strPaymentAccountOwnerName) {
    this.strPaymentAccountOwnerName = strPaymentAccountOwnerName;
  }

  public WSPortalServiceDataIn wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSPortalServiceDataIn wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSPortalServiceDataIn wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSPortalServiceDataIn wsPortalServiceDataIn = (WSPortalServiceDataIn) o;
    return Objects.equals(this.strPortalClaimRequestNumber, wsPortalServiceDataIn.strPortalClaimRequestNumber) &&
        Objects.equals(this.dtPortalClaimRequestCreationDate, wsPortalServiceDataIn.dtPortalClaimRequestCreationDate) &&
        Objects.equals(this.strPolicyNumber, wsPortalServiceDataIn.strPolicyNumber) &&
        Objects.equals(this.strOwnerClientID, wsPortalServiceDataIn.strOwnerClientID) &&
        Objects.equals(this.strPayeeName, wsPortalServiceDataIn.strPayeeName) &&
        Objects.equals(this.strInsuredClientName, wsPortalServiceDataIn.strInsuredClientName) &&
        Objects.equals(this.strInsuredClientID, wsPortalServiceDataIn.strInsuredClientID) &&
        Objects.equals(this.strFormClaimType, wsPortalServiceDataIn.strFormClaimType) &&
        Objects.equals(this.strFormBenefitType, wsPortalServiceDataIn.strFormBenefitType) &&
        Objects.equals(this.strSurgeryPerformed, wsPortalServiceDataIn.strSurgeryPerformed) &&
        Objects.equals(this.dtTreatmentStartDate, wsPortalServiceDataIn.dtTreatmentStartDate) &&
        Objects.equals(this.dtTreatmentCompletionDate, wsPortalServiceDataIn.dtTreatmentCompletionDate) &&
        Objects.equals(this.strHospitalName, wsPortalServiceDataIn.strHospitalName) &&
        Objects.equals(this.strCurrency, wsPortalServiceDataIn.strCurrency) &&
        Objects.equals(this.dblTotalBillingAmount, wsPortalServiceDataIn.dblTotalBillingAmount) &&
        Objects.equals(this.strPhysician, wsPortalServiceDataIn.strPhysician) &&
        Objects.equals(this.strBankAccountNumber, wsPortalServiceDataIn.strBankAccountNumber) &&
        Objects.equals(this.strPaymentAccountOwnerName, wsPortalServiceDataIn.strPaymentAccountOwnerName) &&
        Objects.equals(this.wsProcessingStatus, wsPortalServiceDataIn.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsPortalServiceDataIn.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsPortalServiceDataIn.wsSuccessMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strPortalClaimRequestNumber, dtPortalClaimRequestCreationDate, strPolicyNumber, strOwnerClientID, strPayeeName, strInsuredClientName, strInsuredClientID, strFormClaimType, strFormBenefitType, strSurgeryPerformed, dtTreatmentStartDate, dtTreatmentCompletionDate, strHospitalName, strCurrency, dblTotalBillingAmount, strPhysician, strBankAccountNumber, strPaymentAccountOwnerName, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSPortalServiceDataIn {\n");
    
    sb.append("    strPortalClaimRequestNumber: ").append(toIndentedString(strPortalClaimRequestNumber)).append("\n");
    sb.append("    dtPortalClaimRequestCreationDate: ").append(toIndentedString(dtPortalClaimRequestCreationDate)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strOwnerClientID: ").append(toIndentedString(strOwnerClientID)).append("\n");
    sb.append("    strPayeeName: ").append(toIndentedString(strPayeeName)).append("\n");
    sb.append("    strInsuredClientName: ").append(toIndentedString(strInsuredClientName)).append("\n");
    sb.append("    strInsuredClientID: ").append(toIndentedString(strInsuredClientID)).append("\n");
    sb.append("    strFormClaimType: ").append(toIndentedString(strFormClaimType)).append("\n");
    sb.append("    strFormBenefitType: ").append(toIndentedString(strFormBenefitType)).append("\n");
    sb.append("    strSurgeryPerformed: ").append(toIndentedString(strSurgeryPerformed)).append("\n");
    sb.append("    dtTreatmentStartDate: ").append(toIndentedString(dtTreatmentStartDate)).append("\n");
    sb.append("    dtTreatmentCompletionDate: ").append(toIndentedString(dtTreatmentCompletionDate)).append("\n");
    sb.append("    strHospitalName: ").append(toIndentedString(strHospitalName)).append("\n");
    sb.append("    strCurrency: ").append(toIndentedString(strCurrency)).append("\n");
    sb.append("    dblTotalBillingAmount: ").append(toIndentedString(dblTotalBillingAmount)).append("\n");
    sb.append("    strPhysician: ").append(toIndentedString(strPhysician)).append("\n");
    sb.append("    strBankAccountNumber: ").append(toIndentedString(strBankAccountNumber)).append("\n");
    sb.append("    strPaymentAccountOwnerName: ").append(toIndentedString(strPaymentAccountOwnerName)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

