package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AdditionalDocument
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class AdditionalDocument   {
  @JsonProperty("reqtDropDownText")
  private String reqtDropDownText = null;

  @JsonProperty("subCategory")
  private String subCategory = null;

  @JsonProperty("additionaltext")
  private String additionaltext = null;

  @JsonProperty("notificationsent")
  private String notificationsent = null;

  public AdditionalDocument reqtDropDownText(String reqtDropDownText) {
    this.reqtDropDownText = reqtDropDownText;
    return this;
  }

  /**
   * Get reqtDropDownText
   * @return reqtDropDownText
  **/
  @ApiModelProperty(example = "[reqtDropDownText]", value = "")


  public String getReqtDropDownText() {
    return reqtDropDownText;
  }

  public void setReqtDropDownText(String reqtDropDownText) {
    this.reqtDropDownText = reqtDropDownText;
  }

  public AdditionalDocument subCategory(String subCategory) {
    this.subCategory = subCategory;
    return this;
  }

  /**
   * Get subCategory
   * @return subCategory
  **/
  @ApiModelProperty(example = "[subCategory]", value = "")


  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }

  public AdditionalDocument additionaltext(String additionaltext) {
    this.additionaltext = additionaltext;
    return this;
  }

  /**
   * Get additionaltext
   * @return additionaltext
  **/
  @ApiModelProperty(example = "[additionaltext]", value = "")


  public String getAdditionaltext() {
    return additionaltext;
  }

  public void setAdditionaltext(String additionaltext) {
    this.additionaltext = additionaltext;
  }

  public AdditionalDocument notificationsent(String notificationsent) {
    this.notificationsent = notificationsent;
    return this;
  }

  /**
   * Get notificationsent
   * @return notificationsent
  **/
  @ApiModelProperty(example = "[notificationsent]", value = "")


  public String getNotificationsent() {
    return notificationsent;
  }

  public void setNotificationsent(String notificationsent) {
    this.notificationsent = notificationsent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AdditionalDocument additionalDocument = (AdditionalDocument) o;
    return Objects.equals(this.reqtDropDownText, additionalDocument.reqtDropDownText) &&
        Objects.equals(this.subCategory, additionalDocument.subCategory) &&
        Objects.equals(this.additionaltext, additionalDocument.additionaltext) &&
        Objects.equals(this.notificationsent, additionalDocument.notificationsent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reqtDropDownText, subCategory, additionaltext, notificationsent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AdditionalDocument {\n");
    
    sb.append("    reqtDropDownText: ").append(toIndentedString(reqtDropDownText)).append("\n");
    sb.append("    subCategory: ").append(toIndentedString(subCategory)).append("\n");
    sb.append("    additionaltext: ").append(toIndentedString(additionaltext)).append("\n");
    sb.append("    notificationsent: ").append(toIndentedString(notificationsent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

