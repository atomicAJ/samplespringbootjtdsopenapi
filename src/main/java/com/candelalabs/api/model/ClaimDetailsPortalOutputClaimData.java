package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDetailsPortalOutputClaimData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDetailsPortalOutputClaimData   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPortalClaimRequestNumber")
  private String strPortalClaimRequestNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strClaimTypeLA")
  private String strClaimTypeLA = null;

  @JsonProperty("strClaimStatus")
  private String strClaimStatus = null;

  @JsonProperty("strInsuredClientName")
  private String strInsuredClientName = null;

  @JsonProperty("strInsuredClientID")
  private String strInsuredClientID = null;

  @JsonProperty("dtRegisterDate")
  private String dtRegisterDate = null;

  public ClaimDetailsPortalOutputClaimData strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public ClaimDetailsPortalOutputClaimData strPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
    return this;
  }

  /**
   * Get strPortalClaimRequestNumber
   * @return strPortalClaimRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPortalClaimRequestNumber() {
    return strPortalClaimRequestNumber;
  }

  public void setStrPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
  }

  public ClaimDetailsPortalOutputClaimData strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public ClaimDetailsPortalOutputClaimData strClaimTypeLA(String strClaimTypeLA) {
    this.strClaimTypeLA = strClaimTypeLA;
    return this;
  }

  /**
   * Get strClaimTypeLA
   * @return strClaimTypeLA
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeLA() {
    return strClaimTypeLA;
  }

  public void setStrClaimTypeLA(String strClaimTypeLA) {
    this.strClaimTypeLA = strClaimTypeLA;
  }

  public ClaimDetailsPortalOutputClaimData strClaimStatus(String strClaimStatus) {
    this.strClaimStatus = strClaimStatus;
    return this;
  }

  /**
   * Get strClaimStatus
   * @return strClaimStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimStatus() {
    return strClaimStatus;
  }

  public void setStrClaimStatus(String strClaimStatus) {
    this.strClaimStatus = strClaimStatus;
  }

  public ClaimDetailsPortalOutputClaimData strInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
    return this;
  }

  /**
   * Get strInsuredClientName
   * @return strInsuredClientName
  **/
  @ApiModelProperty(value = "")


  public String getStrInsuredClientName() {
    return strInsuredClientName;
  }

  public void setStrInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
  }

  public ClaimDetailsPortalOutputClaimData strInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
    return this;
  }

  /**
   * Get strInsuredClientID
   * @return strInsuredClientID
  **/
  @ApiModelProperty(value = "")


  public String getStrInsuredClientID() {
    return strInsuredClientID;
  }

  public void setStrInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
  }

  public ClaimDetailsPortalOutputClaimData dtRegisterDate(String dtRegisterDate) {
    this.dtRegisterDate = dtRegisterDate;
    return this;
  }

  /**
   * Get dtRegisterDate
   * @return dtRegisterDate
  **/
  @ApiModelProperty(value = "")


  public String getDtRegisterDate() {
    return dtRegisterDate;
  }

  public void setDtRegisterDate(String dtRegisterDate) {
    this.dtRegisterDate = dtRegisterDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDetailsPortalOutputClaimData claimDetailsPortalOutputClaimData = (ClaimDetailsPortalOutputClaimData) o;
    return Objects.equals(this.strClaimNumber, claimDetailsPortalOutputClaimData.strClaimNumber) &&
        Objects.equals(this.strPortalClaimRequestNumber, claimDetailsPortalOutputClaimData.strPortalClaimRequestNumber) &&
        Objects.equals(this.strPolicyNumber, claimDetailsPortalOutputClaimData.strPolicyNumber) &&
        Objects.equals(this.strClaimTypeLA, claimDetailsPortalOutputClaimData.strClaimTypeLA) &&
        Objects.equals(this.strClaimStatus, claimDetailsPortalOutputClaimData.strClaimStatus) &&
        Objects.equals(this.strInsuredClientName, claimDetailsPortalOutputClaimData.strInsuredClientName) &&
        Objects.equals(this.strInsuredClientID, claimDetailsPortalOutputClaimData.strInsuredClientID) &&
        Objects.equals(this.dtRegisterDate, claimDetailsPortalOutputClaimData.dtRegisterDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strPortalClaimRequestNumber, strPolicyNumber, strClaimTypeLA, strClaimStatus, strInsuredClientName, strInsuredClientID, dtRegisterDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDetailsPortalOutputClaimData {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPortalClaimRequestNumber: ").append(toIndentedString(strPortalClaimRequestNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strClaimTypeLA: ").append(toIndentedString(strClaimTypeLA)).append("\n");
    sb.append("    strClaimStatus: ").append(toIndentedString(strClaimStatus)).append("\n");
    sb.append("    strInsuredClientName: ").append(toIndentedString(strInsuredClientName)).append("\n");
    sb.append("    strInsuredClientID: ").append(toIndentedString(strInsuredClientID)).append("\n");
    sb.append("    dtRegisterDate: ").append(toIndentedString(dtRegisterDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

