package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSReportOutputXLSDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSReportOutputXLSDataInWSInput   {
  @JsonProperty("strTemplateFileLocation")
  private String strTemplateFileLocation = null;

  @JsonProperty("strTemplateFileName")
  private String strTemplateFileName = null;

  @JsonProperty("strReportFileLocation")
  private String strReportFileLocation = null;

  @JsonProperty("reportData")
  private String reportData = null;

  public WSReportOutputXLSDataInWSInput strTemplateFileLocation(String strTemplateFileLocation) {
    this.strTemplateFileLocation = strTemplateFileLocation;
    return this;
  }

  /**
   * Get strTemplateFileLocation
   * @return strTemplateFileLocation
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrTemplateFileLocation() {
    return strTemplateFileLocation;
  }

  public void setStrTemplateFileLocation(String strTemplateFileLocation) {
    this.strTemplateFileLocation = strTemplateFileLocation;
  }

  public WSReportOutputXLSDataInWSInput strTemplateFileName(String strTemplateFileName) {
    this.strTemplateFileName = strTemplateFileName;
    return this;
  }

  /**
   * Get strTemplateFileName
   * @return strTemplateFileName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrTemplateFileName() {
    return strTemplateFileName;
  }

  public void setStrTemplateFileName(String strTemplateFileName) {
    this.strTemplateFileName = strTemplateFileName;
  }

  public WSReportOutputXLSDataInWSInput strReportFileLocation(String strReportFileLocation) {
    this.strReportFileLocation = strReportFileLocation;
    return this;
  }

  /**
   * Get strReportFileLocation
   * @return strReportFileLocation
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrReportFileLocation() {
    return strReportFileLocation;
  }

  public void setStrReportFileLocation(String strReportFileLocation) {
    this.strReportFileLocation = strReportFileLocation;
  }

  public WSReportOutputXLSDataInWSInput reportData(String reportData) {
    this.reportData = reportData;
    return this;
  }

  /**
   * Get reportData
   * @return reportData
  **/
  @ApiModelProperty(example = "", value = "")


  public String getReportData() {
    return reportData;
  }

  public void setReportData(String reportData) {
    this.reportData = reportData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSReportOutputXLSDataInWSInput wsReportOutputXLSDataInWSInput = (WSReportOutputXLSDataInWSInput) o;
    return Objects.equals(this.strTemplateFileLocation, wsReportOutputXLSDataInWSInput.strTemplateFileLocation) &&
        Objects.equals(this.strTemplateFileName, wsReportOutputXLSDataInWSInput.strTemplateFileName) &&
        Objects.equals(this.strReportFileLocation, wsReportOutputXLSDataInWSInput.strReportFileLocation) &&
        Objects.equals(this.reportData, wsReportOutputXLSDataInWSInput.reportData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strTemplateFileLocation, strTemplateFileName, strReportFileLocation, reportData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSReportOutputXLSDataInWSInput {\n");
    
    sb.append("    strTemplateFileLocation: ").append(toIndentedString(strTemplateFileLocation)).append("\n");
    sb.append("    strTemplateFileName: ").append(toIndentedString(strTemplateFileName)).append("\n");
    sb.append("    strReportFileLocation: ").append(toIndentedString(strReportFileLocation)).append("\n");
    sb.append("    reportData: ").append(toIndentedString(reportData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

