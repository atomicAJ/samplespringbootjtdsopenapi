package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutGetBenefitDatabyCodeWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutGetBenefitDatabyCodeWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strBenefitName")
  private String strBenefitName = null;

  @JsonProperty("iBenefitDaysClaimed")
  private BigDecimal iBenefitDaysClaimed = null;

  @JsonProperty("iBenefitTotAmtUsed")
  private BigDecimal iBenefitTotAmtUsed = null;

  @JsonProperty("iBenefitAmtApplicable")
  private BigDecimal iBenefitAmtApplicable = null;

  @JsonProperty("iBenefitAmtPerDay")
  private BigDecimal iBenefitAmtPerDay = null;

  @JsonProperty("iBenefitPercentage")
  private BigDecimal iBenefitPercentage = null;

  @JsonProperty("iBenefitAnnualTotalDays")
  private BigDecimal iBenefitAnnualTotalDays = null;

  @JsonProperty("iBenefitNotApprovedAmount")
  private BigDecimal iBenefitNotApprovedAmount = null;

  @JsonProperty("iBenefitMaxAmountPerDay")
  private BigDecimal iBenefitMaxAmountPerDay = null;

  @JsonProperty("iBenefitNoOfUnits")
  private BigDecimal iBenefitNoOfUnits = null;

  public DataOutGetBenefitDatabyCodeWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutGetBenefitDatabyCodeWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutGetBenefitDatabyCodeWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutGetBenefitDatabyCodeWSResponse strBenefitName(String strBenefitName) {
    this.strBenefitName = strBenefitName;
    return this;
  }

  /**
   * Get strBenefitName
   * @return strBenefitName
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getStrBenefitName() {
    return strBenefitName;
  }

  public void setStrBenefitName(String strBenefitName) {
    this.strBenefitName = strBenefitName;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitDaysClaimed(BigDecimal iBenefitDaysClaimed) {
    this.iBenefitDaysClaimed = iBenefitDaysClaimed;
    return this;
  }

  /**
   * Get iBenefitDaysClaimed
   * @return iBenefitDaysClaimed
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitDaysClaimed() {
    return iBenefitDaysClaimed;
  }

  public void setIBenefitDaysClaimed(BigDecimal iBenefitDaysClaimed) {
    this.iBenefitDaysClaimed = iBenefitDaysClaimed;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitTotAmtUsed(BigDecimal iBenefitTotAmtUsed) {
    this.iBenefitTotAmtUsed = iBenefitTotAmtUsed;
    return this;
  }

  /**
   * Get iBenefitTotAmtUsed
   * @return iBenefitTotAmtUsed
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitTotAmtUsed() {
    return iBenefitTotAmtUsed;
  }

  public void setIBenefitTotAmtUsed(BigDecimal iBenefitTotAmtUsed) {
    this.iBenefitTotAmtUsed = iBenefitTotAmtUsed;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitAmtApplicable(BigDecimal iBenefitAmtApplicable) {
    this.iBenefitAmtApplicable = iBenefitAmtApplicable;
    return this;
  }

  /**
   * Get iBenefitAmtApplicable
   * @return iBenefitAmtApplicable
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitAmtApplicable() {
    return iBenefitAmtApplicable;
  }

  public void setIBenefitAmtApplicable(BigDecimal iBenefitAmtApplicable) {
    this.iBenefitAmtApplicable = iBenefitAmtApplicable;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitAmtPerDay(BigDecimal iBenefitAmtPerDay) {
    this.iBenefitAmtPerDay = iBenefitAmtPerDay;
    return this;
  }

  /**
   * Get iBenefitAmtPerDay
   * @return iBenefitAmtPerDay
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitAmtPerDay() {
    return iBenefitAmtPerDay;
  }

  public void setIBenefitAmtPerDay(BigDecimal iBenefitAmtPerDay) {
    this.iBenefitAmtPerDay = iBenefitAmtPerDay;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitPercentage(BigDecimal iBenefitPercentage) {
    this.iBenefitPercentage = iBenefitPercentage;
    return this;
  }

  /**
   * Get iBenefitPercentage
   * @return iBenefitPercentage
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitPercentage() {
    return iBenefitPercentage;
  }

  public void setIBenefitPercentage(BigDecimal iBenefitPercentage) {
    this.iBenefitPercentage = iBenefitPercentage;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitAnnualTotalDays(BigDecimal iBenefitAnnualTotalDays) {
    this.iBenefitAnnualTotalDays = iBenefitAnnualTotalDays;
    return this;
  }

  /**
   * Get iBenefitAnnualTotalDays
   * @return iBenefitAnnualTotalDays
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitAnnualTotalDays() {
    return iBenefitAnnualTotalDays;
  }

  public void setIBenefitAnnualTotalDays(BigDecimal iBenefitAnnualTotalDays) {
    this.iBenefitAnnualTotalDays = iBenefitAnnualTotalDays;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitNotApprovedAmount(BigDecimal iBenefitNotApprovedAmount) {
    this.iBenefitNotApprovedAmount = iBenefitNotApprovedAmount;
    return this;
  }

  /**
   * Get iBenefitNotApprovedAmount
   * @return iBenefitNotApprovedAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitNotApprovedAmount() {
    return iBenefitNotApprovedAmount;
  }

  public void setIBenefitNotApprovedAmount(BigDecimal iBenefitNotApprovedAmount) {
    this.iBenefitNotApprovedAmount = iBenefitNotApprovedAmount;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitMaxAmountPerDay(BigDecimal iBenefitMaxAmountPerDay) {
    this.iBenefitMaxAmountPerDay = iBenefitMaxAmountPerDay;
    return this;
  }

  /**
   * Get iBenefitMaxAmountPerDay
   * @return iBenefitMaxAmountPerDay
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitMaxAmountPerDay() {
    return iBenefitMaxAmountPerDay;
  }

  public void setIBenefitMaxAmountPerDay(BigDecimal iBenefitMaxAmountPerDay) {
    this.iBenefitMaxAmountPerDay = iBenefitMaxAmountPerDay;
  }

  public DataOutGetBenefitDatabyCodeWSResponse iBenefitNoOfUnits(BigDecimal iBenefitNoOfUnits) {
    this.iBenefitNoOfUnits = iBenefitNoOfUnits;
    return this;
  }

  /**
   * Get iBenefitNoOfUnits
   * @return iBenefitNoOfUnits
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIBenefitNoOfUnits() {
    return iBenefitNoOfUnits;
  }

  public void setIBenefitNoOfUnits(BigDecimal iBenefitNoOfUnits) {
    this.iBenefitNoOfUnits = iBenefitNoOfUnits;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutGetBenefitDatabyCodeWSResponse dataOutGetBenefitDatabyCodeWSResponse = (DataOutGetBenefitDatabyCodeWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutGetBenefitDatabyCodeWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutGetBenefitDatabyCodeWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutGetBenefitDatabyCodeWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strBenefitName, dataOutGetBenefitDatabyCodeWSResponse.strBenefitName) &&
        Objects.equals(this.iBenefitDaysClaimed, dataOutGetBenefitDatabyCodeWSResponse.iBenefitDaysClaimed) &&
        Objects.equals(this.iBenefitTotAmtUsed, dataOutGetBenefitDatabyCodeWSResponse.iBenefitTotAmtUsed) &&
        Objects.equals(this.iBenefitAmtApplicable, dataOutGetBenefitDatabyCodeWSResponse.iBenefitAmtApplicable) &&
        Objects.equals(this.iBenefitAmtPerDay, dataOutGetBenefitDatabyCodeWSResponse.iBenefitAmtPerDay) &&
        Objects.equals(this.iBenefitPercentage, dataOutGetBenefitDatabyCodeWSResponse.iBenefitPercentage) &&
        Objects.equals(this.iBenefitAnnualTotalDays, dataOutGetBenefitDatabyCodeWSResponse.iBenefitAnnualTotalDays) &&
        Objects.equals(this.iBenefitNotApprovedAmount, dataOutGetBenefitDatabyCodeWSResponse.iBenefitNotApprovedAmount) &&
        Objects.equals(this.iBenefitMaxAmountPerDay, dataOutGetBenefitDatabyCodeWSResponse.iBenefitMaxAmountPerDay) &&
        Objects.equals(this.iBenefitNoOfUnits, dataOutGetBenefitDatabyCodeWSResponse.iBenefitNoOfUnits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strBenefitName, iBenefitDaysClaimed, iBenefitTotAmtUsed, iBenefitAmtApplicable, iBenefitAmtPerDay, iBenefitPercentage, iBenefitAnnualTotalDays, iBenefitNotApprovedAmount, iBenefitMaxAmountPerDay, iBenefitNoOfUnits);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutGetBenefitDatabyCodeWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strBenefitName: ").append(toIndentedString(strBenefitName)).append("\n");
    sb.append("    iBenefitDaysClaimed: ").append(toIndentedString(iBenefitDaysClaimed)).append("\n");
    sb.append("    iBenefitTotAmtUsed: ").append(toIndentedString(iBenefitTotAmtUsed)).append("\n");
    sb.append("    iBenefitAmtApplicable: ").append(toIndentedString(iBenefitAmtApplicable)).append("\n");
    sb.append("    iBenefitAmtPerDay: ").append(toIndentedString(iBenefitAmtPerDay)).append("\n");
    sb.append("    iBenefitPercentage: ").append(toIndentedString(iBenefitPercentage)).append("\n");
    sb.append("    iBenefitAnnualTotalDays: ").append(toIndentedString(iBenefitAnnualTotalDays)).append("\n");
    sb.append("    iBenefitNotApprovedAmount: ").append(toIndentedString(iBenefitNotApprovedAmount)).append("\n");
    sb.append("    iBenefitMaxAmountPerDay: ").append(toIndentedString(iBenefitMaxAmountPerDay)).append("\n");
    sb.append("    iBenefitNoOfUnits: ").append(toIndentedString(iBenefitNoOfUnits)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

