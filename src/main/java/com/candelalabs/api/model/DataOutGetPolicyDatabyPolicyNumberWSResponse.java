package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutGetPolicyDatabyPolicyNumberWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutGetPolicyDatabyPolicyNumberWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strProductCode")
  private String strProductCode = null;

  @JsonProperty("strProductName")
  private String strProductName = null;

  public DataOutGetPolicyDatabyPolicyNumberWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutGetPolicyDatabyPolicyNumberWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutGetPolicyDatabyPolicyNumberWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutGetPolicyDatabyPolicyNumberWSResponse strProductCode(String strProductCode) {
    this.strProductCode = strProductCode;
    return this;
  }

  /**
   * Get strProductCode
   * @return strProductCode
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getStrProductCode() {
    return strProductCode;
  }

  public void setStrProductCode(String strProductCode) {
    this.strProductCode = strProductCode;
  }

  public DataOutGetPolicyDatabyPolicyNumberWSResponse strProductName(String strProductName) {
    this.strProductName = strProductName;
    return this;
  }

  /**
   * Get strProductName
   * @return strProductName
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getStrProductName() {
    return strProductName;
  }

  public void setStrProductName(String strProductName) {
    this.strProductName = strProductName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutGetPolicyDatabyPolicyNumberWSResponse dataOutGetPolicyDatabyPolicyNumberWSResponse = (DataOutGetPolicyDatabyPolicyNumberWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutGetPolicyDatabyPolicyNumberWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutGetPolicyDatabyPolicyNumberWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutGetPolicyDatabyPolicyNumberWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strProductCode, dataOutGetPolicyDatabyPolicyNumberWSResponse.strProductCode) &&
        Objects.equals(this.strProductName, dataOutGetPolicyDatabyPolicyNumberWSResponse.strProductName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strProductCode, strProductName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutGetPolicyDatabyPolicyNumberWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strProductCode: ").append(toIndentedString(strProductCode)).append("\n");
    sb.append("    strProductName: ").append(toIndentedString(strProductName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

