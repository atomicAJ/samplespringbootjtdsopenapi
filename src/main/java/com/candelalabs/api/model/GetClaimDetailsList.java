package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * GetClaimDetailsList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class GetClaimDetailsList   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPortalClaimRequestNumber")
  private String strPortalClaimRequestNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strClaimTypeLA")
  private String strClaimTypeLA = null;

  @JsonProperty("strClaimStatus")
  private String strClaimStatus = null;

  @JsonProperty("strInsuredClientName")
  private String strInsuredClientName = null;

  @JsonProperty("strInsuredClientID")
  private String strInsuredClientID = null;

  @JsonProperty("strClaimSubStatus")
  private String strClaimSubStatus = null;

  @JsonProperty("strClaimProcessingReason")
  private String strClaimProcessingReason = null;

  @JsonProperty("dtRegisterDate")
  private String dtRegisterDate = null;

  public GetClaimDetailsList strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public GetClaimDetailsList strPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
    return this;
  }

  /**
   * Get strPortalClaimRequestNumber
   * @return strPortalClaimRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPortalClaimRequestNumber() {
    return strPortalClaimRequestNumber;
  }

  public void setStrPortalClaimRequestNumber(String strPortalClaimRequestNumber) {
    this.strPortalClaimRequestNumber = strPortalClaimRequestNumber;
  }

  public GetClaimDetailsList strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public GetClaimDetailsList strClaimTypeLA(String strClaimTypeLA) {
    this.strClaimTypeLA = strClaimTypeLA;
    return this;
  }

  /**
   * Get strClaimTypeLA
   * @return strClaimTypeLA
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimTypeLA() {
    return strClaimTypeLA;
  }

  public void setStrClaimTypeLA(String strClaimTypeLA) {
    this.strClaimTypeLA = strClaimTypeLA;
  }

  public GetClaimDetailsList strClaimStatus(String strClaimStatus) {
    this.strClaimStatus = strClaimStatus;
    return this;
  }

  /**
   * Get strClaimStatus
   * @return strClaimStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimStatus() {
    return strClaimStatus;
  }

  public void setStrClaimStatus(String strClaimStatus) {
    this.strClaimStatus = strClaimStatus;
  }

  public GetClaimDetailsList strInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
    return this;
  }

  /**
   * Get strInsuredClientName
   * @return strInsuredClientName
  **/
  @ApiModelProperty(value = "")


  public String getStrInsuredClientName() {
    return strInsuredClientName;
  }

  public void setStrInsuredClientName(String strInsuredClientName) {
    this.strInsuredClientName = strInsuredClientName;
  }

  public GetClaimDetailsList strInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
    return this;
  }

  /**
   * Get strInsuredClientID
   * @return strInsuredClientID
  **/
  @ApiModelProperty(value = "")


  public String getStrInsuredClientID() {
    return strInsuredClientID;
  }

  public void setStrInsuredClientID(String strInsuredClientID) {
    this.strInsuredClientID = strInsuredClientID;
  }

  public GetClaimDetailsList strClaimSubStatus(String strClaimSubStatus) {
    this.strClaimSubStatus = strClaimSubStatus;
    return this;
  }

  /**
   * Get strClaimSubStatus
   * @return strClaimSubStatus
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimSubStatus() {
    return strClaimSubStatus;
  }

  public void setStrClaimSubStatus(String strClaimSubStatus) {
    this.strClaimSubStatus = strClaimSubStatus;
  }

  public GetClaimDetailsList strClaimProcessingReason(String strClaimProcessingReason) {
    this.strClaimProcessingReason = strClaimProcessingReason;
    return this;
  }

  /**
   * Get strClaimProcessingReason
   * @return strClaimProcessingReason
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimProcessingReason() {
    return strClaimProcessingReason;
  }

  public void setStrClaimProcessingReason(String strClaimProcessingReason) {
    this.strClaimProcessingReason = strClaimProcessingReason;
  }

  public GetClaimDetailsList dtRegisterDate(String dtRegisterDate) {
    this.dtRegisterDate = dtRegisterDate;
    return this;
  }

  /**
   * Get dtRegisterDate
   * @return dtRegisterDate
  **/
  @ApiModelProperty(value = "")


  public String getDtRegisterDate() {
    return dtRegisterDate;
  }

  public void setDtRegisterDate(String dtRegisterDate) {
    this.dtRegisterDate = dtRegisterDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetClaimDetailsList getClaimDetailsList = (GetClaimDetailsList) o;
    return Objects.equals(this.strClaimNumber, getClaimDetailsList.strClaimNumber) &&
        Objects.equals(this.strPortalClaimRequestNumber, getClaimDetailsList.strPortalClaimRequestNumber) &&
        Objects.equals(this.strPolicyNumber, getClaimDetailsList.strPolicyNumber) &&
        Objects.equals(this.strClaimTypeLA, getClaimDetailsList.strClaimTypeLA) &&
        Objects.equals(this.strClaimStatus, getClaimDetailsList.strClaimStatus) &&
        Objects.equals(this.strInsuredClientName, getClaimDetailsList.strInsuredClientName) &&
        Objects.equals(this.strInsuredClientID, getClaimDetailsList.strInsuredClientID) &&
        Objects.equals(this.strClaimSubStatus, getClaimDetailsList.strClaimSubStatus) &&
        Objects.equals(this.strClaimProcessingReason, getClaimDetailsList.strClaimProcessingReason) &&
        Objects.equals(this.dtRegisterDate, getClaimDetailsList.dtRegisterDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strPortalClaimRequestNumber, strPolicyNumber, strClaimTypeLA, strClaimStatus, strInsuredClientName, strInsuredClientID, strClaimSubStatus, strClaimProcessingReason, dtRegisterDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetClaimDetailsList {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPortalClaimRequestNumber: ").append(toIndentedString(strPortalClaimRequestNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strClaimTypeLA: ").append(toIndentedString(strClaimTypeLA)).append("\n");
    sb.append("    strClaimStatus: ").append(toIndentedString(strClaimStatus)).append("\n");
    sb.append("    strInsuredClientName: ").append(toIndentedString(strInsuredClientName)).append("\n");
    sb.append("    strInsuredClientID: ").append(toIndentedString(strInsuredClientID)).append("\n");
    sb.append("    strClaimSubStatus: ").append(toIndentedString(strClaimSubStatus)).append("\n");
    sb.append("    strClaimProcessingReason: ").append(toIndentedString(strClaimProcessingReason)).append("\n");
    sb.append("    dtRegisterDate: ").append(toIndentedString(dtRegisterDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

