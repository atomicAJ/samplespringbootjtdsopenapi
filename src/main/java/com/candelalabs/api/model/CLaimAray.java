package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CLaimAray
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class CLaimAray   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strPolicyNumber")
  private String strPolicyNumber = null;

  @JsonProperty("strActivityName")
  private String strActivityName = null;

  @JsonProperty("strActivityStartTime")
  private String strActivityStartTime = null;

  @JsonProperty("strActivityEndTime")
  private String strActivityEndTime = null;

  @JsonProperty("strActivityUser")
  private String strActivityUser = null;

  @JsonProperty("strActivityDecsion")
  private String strActivityDecsion = null;

  public CLaimAray strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public CLaimAray strPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
    return this;
  }

  /**
   * Get strPolicyNumber
   * @return strPolicyNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPolicyNumber() {
    return strPolicyNumber;
  }

  public void setStrPolicyNumber(String strPolicyNumber) {
    this.strPolicyNumber = strPolicyNumber;
  }

  public CLaimAray strActivityName(String strActivityName) {
    this.strActivityName = strActivityName;
    return this;
  }

  /**
   * Get strActivityName
   * @return strActivityName
  **/
  @ApiModelProperty(value = "")


  public String getStrActivityName() {
    return strActivityName;
  }

  public void setStrActivityName(String strActivityName) {
    this.strActivityName = strActivityName;
  }

  public CLaimAray strActivityStartTime(String strActivityStartTime) {
    this.strActivityStartTime = strActivityStartTime;
    return this;
  }

  /**
   * Get strActivityStartTime
   * @return strActivityStartTime
  **/
  @ApiModelProperty(value = "")


  public String getStrActivityStartTime() {
    return strActivityStartTime;
  }

  public void setStrActivityStartTime(String strActivityStartTime) {
    this.strActivityStartTime = strActivityStartTime;
  }

  public CLaimAray strActivityEndTime(String strActivityEndTime) {
    this.strActivityEndTime = strActivityEndTime;
    return this;
  }

  /**
   * Get strActivityEndTime
   * @return strActivityEndTime
  **/
  @ApiModelProperty(value = "")


  public String getStrActivityEndTime() {
    return strActivityEndTime;
  }

  public void setStrActivityEndTime(String strActivityEndTime) {
    this.strActivityEndTime = strActivityEndTime;
  }

  public CLaimAray strActivityUser(String strActivityUser) {
    this.strActivityUser = strActivityUser;
    return this;
  }

  /**
   * Get strActivityUser
   * @return strActivityUser
  **/
  @ApiModelProperty(value = "")


  public String getStrActivityUser() {
    return strActivityUser;
  }

  public void setStrActivityUser(String strActivityUser) {
    this.strActivityUser = strActivityUser;
  }

  public CLaimAray strActivityDecsion(String strActivityDecsion) {
    this.strActivityDecsion = strActivityDecsion;
    return this;
  }

  /**
   * Get strActivityDecsion
   * @return strActivityDecsion
  **/
  @ApiModelProperty(value = "")


  public String getStrActivityDecsion() {
    return strActivityDecsion;
  }

  public void setStrActivityDecsion(String strActivityDecsion) {
    this.strActivityDecsion = strActivityDecsion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CLaimAray claimAray = (CLaimAray) o;
    return Objects.equals(this.strClaimNumber, claimAray.strClaimNumber) &&
        Objects.equals(this.strPolicyNumber, claimAray.strPolicyNumber) &&
        Objects.equals(this.strActivityName, claimAray.strActivityName) &&
        Objects.equals(this.strActivityStartTime, claimAray.strActivityStartTime) &&
        Objects.equals(this.strActivityEndTime, claimAray.strActivityEndTime) &&
        Objects.equals(this.strActivityUser, claimAray.strActivityUser) &&
        Objects.equals(this.strActivityDecsion, claimAray.strActivityDecsion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strPolicyNumber, strActivityName, strActivityStartTime, strActivityEndTime, strActivityUser, strActivityDecsion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CLaimAray {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strPolicyNumber: ").append(toIndentedString(strPolicyNumber)).append("\n");
    sb.append("    strActivityName: ").append(toIndentedString(strActivityName)).append("\n");
    sb.append("    strActivityStartTime: ").append(toIndentedString(strActivityStartTime)).append("\n");
    sb.append("    strActivityEndTime: ").append(toIndentedString(strActivityEndTime)).append("\n");
    sb.append("    strActivityUser: ").append(toIndentedString(strActivityUser)).append("\n");
    sb.append("    strActivityDecsion: ").append(toIndentedString(strActivityDecsion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

