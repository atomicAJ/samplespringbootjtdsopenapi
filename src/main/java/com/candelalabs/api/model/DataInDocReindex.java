package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.DataInDocReindexWSInput;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInDocReindex
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInDocReindex   {
  @JsonProperty("WSInput")
  private DataInDocReindexWSInput wsInput = null;

  public DataInDocReindex wsInput(DataInDocReindexWSInput wsInput) {
    this.wsInput = wsInput;
    return this;
  }

  /**
   * Get wsInput
   * @return wsInput
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataInDocReindexWSInput getWsInput() {
    return wsInput;
  }

  public void setWsInput(DataInDocReindexWSInput wsInput) {
    this.wsInput = wsInput;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInDocReindex dataInDocReindex = (DataInDocReindex) o;
    return Objects.equals(this.wsInput, dataInDocReindex.wsInput);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsInput);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInDocReindex {\n");
    
    sb.append("    wsInput: ").append(toIndentedString(wsInput)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

