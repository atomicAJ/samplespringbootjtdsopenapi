package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutGetDiagnosisCodeListWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutGetDiagnosisCodeListWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strDiagnosisCodeList")
  @Valid
  private List<String> strDiagnosisCodeList = null;

  public DataOutGetDiagnosisCodeListWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutGetDiagnosisCodeListWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutGetDiagnosisCodeListWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutGetDiagnosisCodeListWSResponse strDiagnosisCodeList(List<String> strDiagnosisCodeList) {
    this.strDiagnosisCodeList = strDiagnosisCodeList;
    return this;
  }

  public DataOutGetDiagnosisCodeListWSResponse addStrDiagnosisCodeListItem(String strDiagnosisCodeListItem) {
    if (this.strDiagnosisCodeList == null) {
      this.strDiagnosisCodeList = new ArrayList<>();
    }
    this.strDiagnosisCodeList.add(strDiagnosisCodeListItem);
    return this;
  }

  /**
   * Get strDiagnosisCodeList
   * @return strDiagnosisCodeList
  **/
  @ApiModelProperty(value = "")


  public List<String> getStrDiagnosisCodeList() {
    return strDiagnosisCodeList;
  }

  public void setStrDiagnosisCodeList(List<String> strDiagnosisCodeList) {
    this.strDiagnosisCodeList = strDiagnosisCodeList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutGetDiagnosisCodeListWSResponse dataOutGetDiagnosisCodeListWSResponse = (DataOutGetDiagnosisCodeListWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutGetDiagnosisCodeListWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutGetDiagnosisCodeListWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutGetDiagnosisCodeListWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strDiagnosisCodeList, dataOutGetDiagnosisCodeListWSResponse.strDiagnosisCodeList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strDiagnosisCodeList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutGetDiagnosisCodeListWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strDiagnosisCodeList: ").append(toIndentedString(strDiagnosisCodeList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

