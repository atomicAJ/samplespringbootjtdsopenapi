package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutGetDiagnosisNameListWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutGetDiagnosisNameListWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strDiagnosisNameList")
  @Valid
  private List<String> strDiagnosisNameList = null;

  public DataOutGetDiagnosisNameListWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutGetDiagnosisNameListWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutGetDiagnosisNameListWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutGetDiagnosisNameListWSResponse strDiagnosisNameList(List<String> strDiagnosisNameList) {
    this.strDiagnosisNameList = strDiagnosisNameList;
    return this;
  }

  public DataOutGetDiagnosisNameListWSResponse addStrDiagnosisNameListItem(String strDiagnosisNameListItem) {
    if (this.strDiagnosisNameList == null) {
      this.strDiagnosisNameList = new ArrayList<>();
    }
    this.strDiagnosisNameList.add(strDiagnosisNameListItem);
    return this;
  }

  /**
   * Get strDiagnosisNameList
   * @return strDiagnosisNameList
  **/
  @ApiModelProperty(value = "")


  public List<String> getStrDiagnosisNameList() {
    return strDiagnosisNameList;
  }

  public void setStrDiagnosisNameList(List<String> strDiagnosisNameList) {
    this.strDiagnosisNameList = strDiagnosisNameList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutGetDiagnosisNameListWSResponse dataOutGetDiagnosisNameListWSResponse = (DataOutGetDiagnosisNameListWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutGetDiagnosisNameListWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutGetDiagnosisNameListWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutGetDiagnosisNameListWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strDiagnosisNameList, dataOutGetDiagnosisNameListWSResponse.strDiagnosisNameList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strDiagnosisNameList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutGetDiagnosisNameListWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strDiagnosisNameList: ").append(toIndentedString(strDiagnosisNameList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

