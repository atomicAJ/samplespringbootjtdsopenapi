package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ReportTypeListData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataOutWSGetReportTypeListWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataOutWSGetReportTypeListWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strReportTypeList")
  @Valid
  private List<ReportTypeListData> strReportTypeList = null;

  public DataOutWSGetReportTypeListWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public DataOutWSGetReportTypeListWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public DataOutWSGetReportTypeListWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public DataOutWSGetReportTypeListWSResponse strReportTypeList(List<ReportTypeListData> strReportTypeList) {
    this.strReportTypeList = strReportTypeList;
    return this;
  }

  public DataOutWSGetReportTypeListWSResponse addStrReportTypeListItem(ReportTypeListData strReportTypeListItem) {
    if (this.strReportTypeList == null) {
      this.strReportTypeList = new ArrayList<>();
    }
    this.strReportTypeList.add(strReportTypeListItem);
    return this;
  }

  /**
   * Get strReportTypeList
   * @return strReportTypeList
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ReportTypeListData> getStrReportTypeList() {
    return strReportTypeList;
  }

  public void setStrReportTypeList(List<ReportTypeListData> strReportTypeList) {
    this.strReportTypeList = strReportTypeList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataOutWSGetReportTypeListWSResponse dataOutWSGetReportTypeListWSResponse = (DataOutWSGetReportTypeListWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, dataOutWSGetReportTypeListWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, dataOutWSGetReportTypeListWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, dataOutWSGetReportTypeListWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strReportTypeList, dataOutWSGetReportTypeListWSResponse.strReportTypeList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strReportTypeList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataOutWSGetReportTypeListWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strReportTypeList: ").append(toIndentedString(strReportTypeList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

