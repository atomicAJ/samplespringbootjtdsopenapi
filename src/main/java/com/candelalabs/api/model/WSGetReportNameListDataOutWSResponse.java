package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ReportNameListData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGetReportNameListDataOutWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGetReportNameListDataOutWSResponse   {
  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("strReportNameList")
  @Valid
  private List<ReportNameListData> strReportNameList = null;

  public WSGetReportNameListDataOutWSResponse wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSGetReportNameListDataOutWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSGetReportNameListDataOutWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public WSGetReportNameListDataOutWSResponse strReportNameList(List<ReportNameListData> strReportNameList) {
    this.strReportNameList = strReportNameList;
    return this;
  }

  public WSGetReportNameListDataOutWSResponse addStrReportNameListItem(ReportNameListData strReportNameListItem) {
    if (this.strReportNameList == null) {
      this.strReportNameList = new ArrayList<>();
    }
    this.strReportNameList.add(strReportNameListItem);
    return this;
  }

  /**
   * Get strReportNameList
   * @return strReportNameList
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ReportNameListData> getStrReportNameList() {
    return strReportNameList;
  }

  public void setStrReportNameList(List<ReportNameListData> strReportNameList) {
    this.strReportNameList = strReportNameList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGetReportNameListDataOutWSResponse wsGetReportNameListDataOutWSResponse = (WSGetReportNameListDataOutWSResponse) o;
    return Objects.equals(this.wsProcessingStatus, wsGetReportNameListDataOutWSResponse.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsGetReportNameListDataOutWSResponse.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsGetReportNameListDataOutWSResponse.wsSuccessMessage) &&
        Objects.equals(this.strReportNameList, wsGetReportNameListDataOutWSResponse.strReportNameList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessingStatus, wsExceptionMessage, wsSuccessMessage, strReportNameList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGetReportNameListDataOutWSResponse {\n");
    
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    strReportNameList: ").append(toIndentedString(strReportNameList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

