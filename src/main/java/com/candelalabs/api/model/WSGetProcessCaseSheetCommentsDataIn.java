package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGetProcessCaseSheetCommentsDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGetProcessCaseSheetCommentsDataIn   {
  @JsonProperty("activityName")
  private String activityName = null;

  @JsonProperty("processName")
  private String processName = null;

  @JsonProperty("caseReferenceKey")
  private String caseReferenceKey = null;

  @JsonProperty("tagName")
  private String tagName = null;

  public WSGetProcessCaseSheetCommentsDataIn activityName(String activityName) {
    this.activityName = activityName;
    return this;
  }

  /**
   * Get activityName
   * @return activityName
  **/
  @ApiModelProperty(value = "")


  public String getActivityName() {
    return activityName;
  }

  public void setActivityName(String activityName) {
    this.activityName = activityName;
  }

  public WSGetProcessCaseSheetCommentsDataIn processName(String processName) {
    this.processName = processName;
    return this;
  }

  /**
   * Get processName
   * @return processName
  **/
  @ApiModelProperty(value = "")


  public String getProcessName() {
    return processName;
  }

  public void setProcessName(String processName) {
    this.processName = processName;
  }

  public WSGetProcessCaseSheetCommentsDataIn caseReferenceKey(String caseReferenceKey) {
    this.caseReferenceKey = caseReferenceKey;
    return this;
  }

  /**
   * Get caseReferenceKey
   * @return caseReferenceKey
  **/
  @ApiModelProperty(value = "")


  public String getCaseReferenceKey() {
    return caseReferenceKey;
  }

  public void setCaseReferenceKey(String caseReferenceKey) {
    this.caseReferenceKey = caseReferenceKey;
  }

  public WSGetProcessCaseSheetCommentsDataIn tagName(String tagName) {
    this.tagName = tagName;
    return this;
  }

  /**
   * Get tagName
   * @return tagName
  **/
  @ApiModelProperty(value = "")


  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGetProcessCaseSheetCommentsDataIn wsGetProcessCaseSheetCommentsDataIn = (WSGetProcessCaseSheetCommentsDataIn) o;
    return Objects.equals(this.activityName, wsGetProcessCaseSheetCommentsDataIn.activityName) &&
        Objects.equals(this.processName, wsGetProcessCaseSheetCommentsDataIn.processName) &&
        Objects.equals(this.caseReferenceKey, wsGetProcessCaseSheetCommentsDataIn.caseReferenceKey) &&
        Objects.equals(this.tagName, wsGetProcessCaseSheetCommentsDataIn.tagName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(activityName, processName, caseReferenceKey, tagName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGetProcessCaseSheetCommentsDataIn {\n");
    
    sb.append("    activityName: ").append(toIndentedString(activityName)).append("\n");
    sb.append("    processName: ").append(toIndentedString(processName)).append("\n");
    sb.append("    caseReferenceKey: ").append(toIndentedString(caseReferenceKey)).append("\n");
    sb.append("    tagName: ").append(toIndentedString(tagName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

