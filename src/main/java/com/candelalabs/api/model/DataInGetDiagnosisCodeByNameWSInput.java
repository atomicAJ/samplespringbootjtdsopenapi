package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetDiagnosisCodeByNameWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetDiagnosisCodeByNameWSInput   {
  @JsonProperty("strClaimTypeUI")
  private String strClaimTypeUI = null;

  @JsonProperty("strDiagnosisName")
  private String strDiagnosisName = null;

  @JsonProperty("iClaimSumAssuredPercent")
  private BigDecimal iClaimSumAssuredPercent = null;

  public DataInGetDiagnosisCodeByNameWSInput strClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
    return this;
  }

  /**
   * Get strClaimTypeUI
   * @return strClaimTypeUI
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrClaimTypeUI() {
    return strClaimTypeUI;
  }

  public void setStrClaimTypeUI(String strClaimTypeUI) {
    this.strClaimTypeUI = strClaimTypeUI;
  }

  public DataInGetDiagnosisCodeByNameWSInput strDiagnosisName(String strDiagnosisName) {
    this.strDiagnosisName = strDiagnosisName;
    return this;
  }

  /**
   * Get strDiagnosisName
   * @return strDiagnosisName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrDiagnosisName() {
    return strDiagnosisName;
  }

  public void setStrDiagnosisName(String strDiagnosisName) {
    this.strDiagnosisName = strDiagnosisName;
  }

  public DataInGetDiagnosisCodeByNameWSInput iClaimSumAssuredPercent(BigDecimal iClaimSumAssuredPercent) {
    this.iClaimSumAssuredPercent = iClaimSumAssuredPercent;
    return this;
  }

  /**
   * Get iClaimSumAssuredPercent
   * @return iClaimSumAssuredPercent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIClaimSumAssuredPercent() {
    return iClaimSumAssuredPercent;
  }

  public void setIClaimSumAssuredPercent(BigDecimal iClaimSumAssuredPercent) {
    this.iClaimSumAssuredPercent = iClaimSumAssuredPercent;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetDiagnosisCodeByNameWSInput dataInGetDiagnosisCodeByNameWSInput = (DataInGetDiagnosisCodeByNameWSInput) o;
    return Objects.equals(this.strClaimTypeUI, dataInGetDiagnosisCodeByNameWSInput.strClaimTypeUI) &&
        Objects.equals(this.strDiagnosisName, dataInGetDiagnosisCodeByNameWSInput.strDiagnosisName) &&
        Objects.equals(this.iClaimSumAssuredPercent, dataInGetDiagnosisCodeByNameWSInput.iClaimSumAssuredPercent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimTypeUI, strDiagnosisName, iClaimSumAssuredPercent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetDiagnosisCodeByNameWSInput {\n");
    
    sb.append("    strClaimTypeUI: ").append(toIndentedString(strClaimTypeUI)).append("\n");
    sb.append("    strDiagnosisName: ").append(toIndentedString(strDiagnosisName)).append("\n");
    sb.append("    iClaimSumAssuredPercent: ").append(toIndentedString(iClaimSumAssuredPercent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

