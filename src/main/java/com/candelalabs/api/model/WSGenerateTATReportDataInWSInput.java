package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSGenerateTATReportDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSGenerateTATReportDataInWSInput   {
  @JsonProperty("strReportRequestID")
  private String strReportRequestID = null;

  @JsonProperty("strReportOutuputFormat")
  private String strReportOutuputFormat = null;

  @JsonProperty("strTemplateFileLocation")
  private String strTemplateFileLocation = null;

  @JsonProperty("strTemplateFileName")
  private String strTemplateFileName = null;

  @JsonProperty("strReportFileLocation")
  private String strReportFileLocation = null;

  @JsonProperty("strReportFileName")
  private String strReportFileName = null;

  @JsonProperty("strReportType")
  private String strReportType = null;

  @JsonProperty("strTagName")
  private String strTagName = null;

  @JsonProperty("strReportDateType")
  private String strReportDateType = null;

  @JsonProperty("strProductCode")
  private String strProductCode = null;

  @JsonProperty("dtFromDate")
  private String dtFromDate = null;

  @JsonProperty("dtToDate")
  private String dtToDate = null;

  public WSGenerateTATReportDataInWSInput strReportRequestID(String strReportRequestID) {
    this.strReportRequestID = strReportRequestID;
    return this;
  }

  /**
   * Get strReportRequestID
   * @return strReportRequestID
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getStrReportRequestID() {
    return strReportRequestID;
  }

  public void setStrReportRequestID(String strReportRequestID) {
    this.strReportRequestID = strReportRequestID;
  }

  public WSGenerateTATReportDataInWSInput strReportOutuputFormat(String strReportOutuputFormat) {
    this.strReportOutuputFormat = strReportOutuputFormat;
    return this;
  }

  /**
   * Get strReportOutuputFormat
   * @return strReportOutuputFormat
  **/
  @ApiModelProperty(example = "xls", value = "")


  public String getStrReportOutuputFormat() {
    return strReportOutuputFormat;
  }

  public void setStrReportOutuputFormat(String strReportOutuputFormat) {
    this.strReportOutuputFormat = strReportOutuputFormat;
  }

  public WSGenerateTATReportDataInWSInput strTemplateFileLocation(String strTemplateFileLocation) {
    this.strTemplateFileLocation = strTemplateFileLocation;
    return this;
  }

  /**
   * Get strTemplateFileLocation
   * @return strTemplateFileLocation
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrTemplateFileLocation() {
    return strTemplateFileLocation;
  }

  public void setStrTemplateFileLocation(String strTemplateFileLocation) {
    this.strTemplateFileLocation = strTemplateFileLocation;
  }

  public WSGenerateTATReportDataInWSInput strTemplateFileName(String strTemplateFileName) {
    this.strTemplateFileName = strTemplateFileName;
    return this;
  }

  /**
   * Get strTemplateFileName
   * @return strTemplateFileName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrTemplateFileName() {
    return strTemplateFileName;
  }

  public void setStrTemplateFileName(String strTemplateFileName) {
    this.strTemplateFileName = strTemplateFileName;
  }

  public WSGenerateTATReportDataInWSInput strReportFileLocation(String strReportFileLocation) {
    this.strReportFileLocation = strReportFileLocation;
    return this;
  }

  /**
   * Get strReportFileLocation
   * @return strReportFileLocation
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrReportFileLocation() {
    return strReportFileLocation;
  }

  public void setStrReportFileLocation(String strReportFileLocation) {
    this.strReportFileLocation = strReportFileLocation;
  }

  public WSGenerateTATReportDataInWSInput strReportFileName(String strReportFileName) {
    this.strReportFileName = strReportFileName;
    return this;
  }

  /**
   * Get strReportFileName
   * @return strReportFileName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrReportFileName() {
    return strReportFileName;
  }

  public void setStrReportFileName(String strReportFileName) {
    this.strReportFileName = strReportFileName;
  }

  public WSGenerateTATReportDataInWSInput strReportType(String strReportType) {
    this.strReportType = strReportType;
    return this;
  }

  /**
   * Get strReportType
   * @return strReportType
  **/
  @ApiModelProperty(example = "Claims", value = "")


  public String getStrReportType() {
    return strReportType;
  }

  public void setStrReportType(String strReportType) {
    this.strReportType = strReportType;
  }

  public WSGenerateTATReportDataInWSInput strTagName(String strTagName) {
    this.strTagName = strTagName;
    return this;
  }

  /**
   * Get strTagName
   * @return strTagName
  **/
  @ApiModelProperty(example = "FWDCustomerComment", value = "")


  public String getStrTagName() {
    return strTagName;
  }

  public void setStrTagName(String strTagName) {
    this.strTagName = strTagName;
  }

  public WSGenerateTATReportDataInWSInput strReportDateType(String strReportDateType) {
    this.strReportDateType = strReportDateType;
    return this;
  }

  /**
   * Get strReportDateType
   * @return strReportDateType
  **/
  @ApiModelProperty(example = "Register Date", value = "")


  public String getStrReportDateType() {
    return strReportDateType;
  }

  public void setStrReportDateType(String strReportDateType) {
    this.strReportDateType = strReportDateType;
  }

  public WSGenerateTATReportDataInWSInput strProductCode(String strProductCode) {
    this.strProductCode = strProductCode;
    return this;
  }

  /**
   * Get strProductCode
   * @return strProductCode
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrProductCode() {
    return strProductCode;
  }

  public void setStrProductCode(String strProductCode) {
    this.strProductCode = strProductCode;
  }

  public WSGenerateTATReportDataInWSInput dtFromDate(String dtFromDate) {
    this.dtFromDate = dtFromDate;
    return this;
  }

  /**
   * Get dtFromDate
   * @return dtFromDate
  **/
  @ApiModelProperty(example = "2019-01-01", value = "")


  public String getDtFromDate() {
    return dtFromDate;
  }

  public void setDtFromDate(String dtFromDate) {
    this.dtFromDate = dtFromDate;
  }

  public WSGenerateTATReportDataInWSInput dtToDate(String dtToDate) {
    this.dtToDate = dtToDate;
    return this;
  }

  /**
   * Get dtToDate
   * @return dtToDate
  **/
  @ApiModelProperty(example = "2019-03-01", value = "")


  public String getDtToDate() {
    return dtToDate;
  }

  public void setDtToDate(String dtToDate) {
    this.dtToDate = dtToDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSGenerateTATReportDataInWSInput wsGenerateTATReportDataInWSInput = (WSGenerateTATReportDataInWSInput) o;
    return Objects.equals(this.strReportRequestID, wsGenerateTATReportDataInWSInput.strReportRequestID) &&
        Objects.equals(this.strReportOutuputFormat, wsGenerateTATReportDataInWSInput.strReportOutuputFormat) &&
        Objects.equals(this.strTemplateFileLocation, wsGenerateTATReportDataInWSInput.strTemplateFileLocation) &&
        Objects.equals(this.strTemplateFileName, wsGenerateTATReportDataInWSInput.strTemplateFileName) &&
        Objects.equals(this.strReportFileLocation, wsGenerateTATReportDataInWSInput.strReportFileLocation) &&
        Objects.equals(this.strReportFileName, wsGenerateTATReportDataInWSInput.strReportFileName) &&
        Objects.equals(this.strReportType, wsGenerateTATReportDataInWSInput.strReportType) &&
        Objects.equals(this.strTagName, wsGenerateTATReportDataInWSInput.strTagName) &&
        Objects.equals(this.strReportDateType, wsGenerateTATReportDataInWSInput.strReportDateType) &&
        Objects.equals(this.strProductCode, wsGenerateTATReportDataInWSInput.strProductCode) &&
        Objects.equals(this.dtFromDate, wsGenerateTATReportDataInWSInput.dtFromDate) &&
        Objects.equals(this.dtToDate, wsGenerateTATReportDataInWSInput.dtToDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strReportRequestID, strReportOutuputFormat, strTemplateFileLocation, strTemplateFileName, strReportFileLocation, strReportFileName, strReportType, strTagName, strReportDateType, strProductCode, dtFromDate, dtToDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSGenerateTATReportDataInWSInput {\n");
    
    sb.append("    strReportRequestID: ").append(toIndentedString(strReportRequestID)).append("\n");
    sb.append("    strReportOutuputFormat: ").append(toIndentedString(strReportOutuputFormat)).append("\n");
    sb.append("    strTemplateFileLocation: ").append(toIndentedString(strTemplateFileLocation)).append("\n");
    sb.append("    strTemplateFileName: ").append(toIndentedString(strTemplateFileName)).append("\n");
    sb.append("    strReportFileLocation: ").append(toIndentedString(strReportFileLocation)).append("\n");
    sb.append("    strReportFileName: ").append(toIndentedString(strReportFileName)).append("\n");
    sb.append("    strReportType: ").append(toIndentedString(strReportType)).append("\n");
    sb.append("    strTagName: ").append(toIndentedString(strTagName)).append("\n");
    sb.append("    strReportDateType: ").append(toIndentedString(strReportDateType)).append("\n");
    sb.append("    strProductCode: ").append(toIndentedString(strProductCode)).append("\n");
    sb.append("    dtFromDate: ").append(toIndentedString(dtFromDate)).append("\n");
    sb.append("    dtToDate: ").append(toIndentedString(dtToDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

