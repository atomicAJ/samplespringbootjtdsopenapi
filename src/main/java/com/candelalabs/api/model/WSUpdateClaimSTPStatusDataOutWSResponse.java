package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSUpdateClaimSTPStatusDataOutWSResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSUpdateClaimSTPStatusDataOutWSResponse   {
  @JsonProperty("WSProcessStatus")
  private String wsProcessStatus = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  public WSUpdateClaimSTPStatusDataOutWSResponse wsProcessStatus(String wsProcessStatus) {
    this.wsProcessStatus = wsProcessStatus;
    return this;
  }

  /**
   * Get wsProcessStatus
   * @return wsProcessStatus
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getWsProcessStatus() {
    return wsProcessStatus;
  }

  public void setWsProcessStatus(String wsProcessStatus) {
    this.wsProcessStatus = wsProcessStatus;
  }

  public WSUpdateClaimSTPStatusDataOutWSResponse wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }

  public WSUpdateClaimSTPStatusDataOutWSResponse wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSUpdateClaimSTPStatusDataOutWSResponse wsUpdateClaimSTPStatusDataOutWSResponse = (WSUpdateClaimSTPStatusDataOutWSResponse) o;
    return Objects.equals(this.wsProcessStatus, wsUpdateClaimSTPStatusDataOutWSResponse.wsProcessStatus) &&
        Objects.equals(this.wsSuccessMessage, wsUpdateClaimSTPStatusDataOutWSResponse.wsSuccessMessage) &&
        Objects.equals(this.wsExceptionMessage, wsUpdateClaimSTPStatusDataOutWSResponse.wsExceptionMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsProcessStatus, wsSuccessMessage, wsExceptionMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSUpdateClaimSTPStatusDataOutWSResponse {\n");
    
    sb.append("    wsProcessStatus: ").append(toIndentedString(wsProcessStatus)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

