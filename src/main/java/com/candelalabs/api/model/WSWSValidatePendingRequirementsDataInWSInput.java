package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSWSValidatePendingRequirementsDataInWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSWSValidatePendingRequirementsDataInWSInput   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  public WSWSValidatePendingRequirementsDataInWSInput strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(example = "1910WP00001", value = "")


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSWSValidatePendingRequirementsDataInWSInput wsWSValidatePendingRequirementsDataInWSInput = (WSWSValidatePendingRequirementsDataInWSInput) o;
    return Objects.equals(this.strClaimNumber, wsWSValidatePendingRequirementsDataInWSInput.strClaimNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSWSValidatePendingRequirementsDataInWSInput {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

