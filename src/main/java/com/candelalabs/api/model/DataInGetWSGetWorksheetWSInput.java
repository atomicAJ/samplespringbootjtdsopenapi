package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetWSGetWorksheetWSInput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetWSGetWorksheetWSInput   {
  @JsonProperty("strClaimNumber")
  private String strClaimNumber = null;

  @JsonProperty("strApplicationNumber")
  private String strApplicationNumber = null;

  @JsonProperty("strPOSRequestNumber")
  private String strPOSRequestNumber = null;

  @JsonProperty("strProcessType")
  private String strProcessType = null;

  public DataInGetWSGetWorksheetWSInput strClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
    return this;
  }

  /**
   * Get strClaimNumber
   * @return strClaimNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStrClaimNumber() {
    return strClaimNumber;
  }

  public void setStrClaimNumber(String strClaimNumber) {
    this.strClaimNumber = strClaimNumber;
  }

  public DataInGetWSGetWorksheetWSInput strApplicationNumber(String strApplicationNumber) {
    this.strApplicationNumber = strApplicationNumber;
    return this;
  }

  /**
   * Get strApplicationNumber
   * @return strApplicationNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrApplicationNumber() {
    return strApplicationNumber;
  }

  public void setStrApplicationNumber(String strApplicationNumber) {
    this.strApplicationNumber = strApplicationNumber;
  }

  public DataInGetWSGetWorksheetWSInput strPOSRequestNumber(String strPOSRequestNumber) {
    this.strPOSRequestNumber = strPOSRequestNumber;
    return this;
  }

  /**
   * Get strPOSRequestNumber
   * @return strPOSRequestNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrPOSRequestNumber() {
    return strPOSRequestNumber;
  }

  public void setStrPOSRequestNumber(String strPOSRequestNumber) {
    this.strPOSRequestNumber = strPOSRequestNumber;
  }

  public DataInGetWSGetWorksheetWSInput strProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
    return this;
  }

  /**
   * Get strProcessType
   * @return strProcessType
  **/
  @ApiModelProperty(value = "")


  public String getStrProcessType() {
    return strProcessType;
  }

  public void setStrProcessType(String strProcessType) {
    this.strProcessType = strProcessType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetWSGetWorksheetWSInput dataInGetWSGetWorksheetWSInput = (DataInGetWSGetWorksheetWSInput) o;
    return Objects.equals(this.strClaimNumber, dataInGetWSGetWorksheetWSInput.strClaimNumber) &&
        Objects.equals(this.strApplicationNumber, dataInGetWSGetWorksheetWSInput.strApplicationNumber) &&
        Objects.equals(this.strPOSRequestNumber, dataInGetWSGetWorksheetWSInput.strPOSRequestNumber) &&
        Objects.equals(this.strProcessType, dataInGetWSGetWorksheetWSInput.strProcessType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strClaimNumber, strApplicationNumber, strPOSRequestNumber, strProcessType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetWSGetWorksheetWSInput {\n");
    
    sb.append("    strClaimNumber: ").append(toIndentedString(strClaimNumber)).append("\n");
    sb.append("    strApplicationNumber: ").append(toIndentedString(strApplicationNumber)).append("\n");
    sb.append("    strPOSRequestNumber: ").append(toIndentedString(strPOSRequestNumber)).append("\n");
    sb.append("    strProcessType: ").append(toIndentedString(strProcessType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

