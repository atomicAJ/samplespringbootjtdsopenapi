package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.ClaimDetailsPortalOutputClaimData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimDetailsPortalOutput
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimDetailsPortalOutput   {
  @JsonProperty("claimData")
  private ClaimDetailsPortalOutputClaimData claimData = null;

  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  public ClaimDetailsPortalOutput claimData(ClaimDetailsPortalOutputClaimData claimData) {
    this.claimData = claimData;
    return this;
  }

  /**
   * Get claimData
   * @return claimData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ClaimDetailsPortalOutputClaimData getClaimData() {
    return claimData;
  }

  public void setClaimData(ClaimDetailsPortalOutputClaimData claimData) {
    this.claimData = claimData;
  }

  public ClaimDetailsPortalOutput wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public ClaimDetailsPortalOutput wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public ClaimDetailsPortalOutput wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimDetailsPortalOutput claimDetailsPortalOutput = (ClaimDetailsPortalOutput) o;
    return Objects.equals(this.claimData, claimDetailsPortalOutput.claimData) &&
        Objects.equals(this.wsProcessingStatus, claimDetailsPortalOutput.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, claimDetailsPortalOutput.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, claimDetailsPortalOutput.wsSuccessMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimData, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimDetailsPortalOutput {\n");
    
    sb.append("    claimData: ").append(toIndentedString(claimData)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

