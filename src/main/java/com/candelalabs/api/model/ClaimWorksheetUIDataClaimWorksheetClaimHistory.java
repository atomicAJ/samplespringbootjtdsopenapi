package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ClaimWorksheetUIDataClaimWorksheetClaimHistory
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ClaimWorksheetUIDataClaimWorksheetClaimHistory   {
  @JsonProperty("strCHLifeAsiaClaimNumber")
  private String strCHLifeAsiaClaimNumber = null;

  @JsonProperty("strCHClaimNumber")
  private String strCHClaimNumber = null;

  @JsonProperty("strCHPolicynumber")
  private String strCHPolicynumber = null;

  @JsonProperty("dblCHTotalClaimAmount")
  private BigDecimal dblCHTotalClaimAmount = null;

  @JsonProperty("strCHClientNumber")
  private String strCHClientNumber = null;

  @JsonProperty("dtCHApprovalDate")
  private BigDecimal dtCHApprovalDate = null;

  @JsonProperty("strCHComponentCode")
  private String strCHComponentCode = null;

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory strCHLifeAsiaClaimNumber(String strCHLifeAsiaClaimNumber) {
    this.strCHLifeAsiaClaimNumber = strCHLifeAsiaClaimNumber;
    return this;
  }

  /**
   * Get strCHLifeAsiaClaimNumber
   * @return strCHLifeAsiaClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCHLifeAsiaClaimNumber() {
    return strCHLifeAsiaClaimNumber;
  }

  public void setStrCHLifeAsiaClaimNumber(String strCHLifeAsiaClaimNumber) {
    this.strCHLifeAsiaClaimNumber = strCHLifeAsiaClaimNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory strCHClaimNumber(String strCHClaimNumber) {
    this.strCHClaimNumber = strCHClaimNumber;
    return this;
  }

  /**
   * Get strCHClaimNumber
   * @return strCHClaimNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCHClaimNumber() {
    return strCHClaimNumber;
  }

  public void setStrCHClaimNumber(String strCHClaimNumber) {
    this.strCHClaimNumber = strCHClaimNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory strCHPolicynumber(String strCHPolicynumber) {
    this.strCHPolicynumber = strCHPolicynumber;
    return this;
  }

  /**
   * Get strCHPolicynumber
   * @return strCHPolicynumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCHPolicynumber() {
    return strCHPolicynumber;
  }

  public void setStrCHPolicynumber(String strCHPolicynumber) {
    this.strCHPolicynumber = strCHPolicynumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory dblCHTotalClaimAmount(BigDecimal dblCHTotalClaimAmount) {
    this.dblCHTotalClaimAmount = dblCHTotalClaimAmount;
    return this;
  }

  /**
   * Get dblCHTotalClaimAmount
   * @return dblCHTotalClaimAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDblCHTotalClaimAmount() {
    return dblCHTotalClaimAmount;
  }

  public void setDblCHTotalClaimAmount(BigDecimal dblCHTotalClaimAmount) {
    this.dblCHTotalClaimAmount = dblCHTotalClaimAmount;
  }

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory strCHClientNumber(String strCHClientNumber) {
    this.strCHClientNumber = strCHClientNumber;
    return this;
  }

  /**
   * Get strCHClientNumber
   * @return strCHClientNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrCHClientNumber() {
    return strCHClientNumber;
  }

  public void setStrCHClientNumber(String strCHClientNumber) {
    this.strCHClientNumber = strCHClientNumber;
  }

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory dtCHApprovalDate(BigDecimal dtCHApprovalDate) {
    this.dtCHApprovalDate = dtCHApprovalDate;
    return this;
  }

  /**
   * Get dtCHApprovalDate
   * @return dtCHApprovalDate
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDtCHApprovalDate() {
    return dtCHApprovalDate;
  }

  public void setDtCHApprovalDate(BigDecimal dtCHApprovalDate) {
    this.dtCHApprovalDate = dtCHApprovalDate;
  }

  public ClaimWorksheetUIDataClaimWorksheetClaimHistory strCHComponentCode(String strCHComponentCode) {
    this.strCHComponentCode = strCHComponentCode;
    return this;
  }

  /**
   * Get strCHComponentCode
   * @return strCHComponentCode
  **/
  @ApiModelProperty(value = "")


  public String getStrCHComponentCode() {
    return strCHComponentCode;
  }

  public void setStrCHComponentCode(String strCHComponentCode) {
    this.strCHComponentCode = strCHComponentCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClaimWorksheetUIDataClaimWorksheetClaimHistory claimWorksheetUIDataClaimWorksheetClaimHistory = (ClaimWorksheetUIDataClaimWorksheetClaimHistory) o;
    return Objects.equals(this.strCHLifeAsiaClaimNumber, claimWorksheetUIDataClaimWorksheetClaimHistory.strCHLifeAsiaClaimNumber) &&
        Objects.equals(this.strCHClaimNumber, claimWorksheetUIDataClaimWorksheetClaimHistory.strCHClaimNumber) &&
        Objects.equals(this.strCHPolicynumber, claimWorksheetUIDataClaimWorksheetClaimHistory.strCHPolicynumber) &&
        Objects.equals(this.dblCHTotalClaimAmount, claimWorksheetUIDataClaimWorksheetClaimHistory.dblCHTotalClaimAmount) &&
        Objects.equals(this.strCHClientNumber, claimWorksheetUIDataClaimWorksheetClaimHistory.strCHClientNumber) &&
        Objects.equals(this.dtCHApprovalDate, claimWorksheetUIDataClaimWorksheetClaimHistory.dtCHApprovalDate) &&
        Objects.equals(this.strCHComponentCode, claimWorksheetUIDataClaimWorksheetClaimHistory.strCHComponentCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strCHLifeAsiaClaimNumber, strCHClaimNumber, strCHPolicynumber, dblCHTotalClaimAmount, strCHClientNumber, dtCHApprovalDate, strCHComponentCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaimWorksheetUIDataClaimWorksheetClaimHistory {\n");
    
    sb.append("    strCHLifeAsiaClaimNumber: ").append(toIndentedString(strCHLifeAsiaClaimNumber)).append("\n");
    sb.append("    strCHClaimNumber: ").append(toIndentedString(strCHClaimNumber)).append("\n");
    sb.append("    strCHPolicynumber: ").append(toIndentedString(strCHPolicynumber)).append("\n");
    sb.append("    dblCHTotalClaimAmount: ").append(toIndentedString(dblCHTotalClaimAmount)).append("\n");
    sb.append("    strCHClientNumber: ").append(toIndentedString(strCHClientNumber)).append("\n");
    sb.append("    dtCHApprovalDate: ").append(toIndentedString(dtCHApprovalDate)).append("\n");
    sb.append("    strCHComponentCode: ").append(toIndentedString(strCHComponentCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

