package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.DataInGetBenefitCodeNameListWSInput1;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DataInGetBenefitCodeNameList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class DataInGetBenefitCodeNameList   {
  @JsonProperty("WSInput1")
  private DataInGetBenefitCodeNameListWSInput1 wsInput1 = null;

  public DataInGetBenefitCodeNameList wsInput1(DataInGetBenefitCodeNameListWSInput1 wsInput1) {
    this.wsInput1 = wsInput1;
    return this;
  }

  /**
   * Get wsInput1
   * @return wsInput1
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataInGetBenefitCodeNameListWSInput1 getWsInput1() {
    return wsInput1;
  }

  public void setWsInput1(DataInGetBenefitCodeNameListWSInput1 wsInput1) {
    this.wsInput1 = wsInput1;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataInGetBenefitCodeNameList dataInGetBenefitCodeNameList = (DataInGetBenefitCodeNameList) o;
    return Objects.equals(this.wsInput1, dataInGetBenefitCodeNameList.wsInput1);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsInput1);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataInGetBenefitCodeNameList {\n");
    
    sb.append("    wsInput1: ").append(toIndentedString(wsInput1)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

