package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * STPReportData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class STPReportData   {
  @JsonProperty("policyNumber")
  private String policyNumber = null;

  @JsonProperty("claimNumber")
  private String claimNumber = null;

  @JsonProperty("claimType")
  private String claimType = null;

  @JsonProperty("claimStatus")
  private String claimStatus = null;

  @JsonProperty("claimReceiveDate")
  private String claimReceiveDate = null;

  @JsonProperty("claimRegisterDate")
  private String claimRegisterDate = null;

  @JsonProperty("STPStatus")
  private String stPStatus = null;

  @JsonProperty("reasonForSTPFail")
  private String reasonForSTPFail = null;

  @JsonProperty("channel")
  private String channel = null;

  public STPReportData policyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
    return this;
  }

  /**
   * Get policyNumber
   * @return policyNumber
  **/
  @ApiModelProperty(value = "")


  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public STPReportData claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public STPReportData claimType(String claimType) {
    this.claimType = claimType;
    return this;
  }

  /**
   * Get claimType
   * @return claimType
  **/
  @ApiModelProperty(value = "")


  public String getClaimType() {
    return claimType;
  }

  public void setClaimType(String claimType) {
    this.claimType = claimType;
  }

  public STPReportData claimStatus(String claimStatus) {
    this.claimStatus = claimStatus;
    return this;
  }

  /**
   * Get claimStatus
   * @return claimStatus
  **/
  @ApiModelProperty(value = "")


  public String getClaimStatus() {
    return claimStatus;
  }

  public void setClaimStatus(String claimStatus) {
    this.claimStatus = claimStatus;
  }

  public STPReportData claimReceiveDate(String claimReceiveDate) {
    this.claimReceiveDate = claimReceiveDate;
    return this;
  }

  /**
   * Get claimReceiveDate
   * @return claimReceiveDate
  **/
  @ApiModelProperty(value = "")


  public String getClaimReceiveDate() {
    return claimReceiveDate;
  }

  public void setClaimReceiveDate(String claimReceiveDate) {
    this.claimReceiveDate = claimReceiveDate;
  }

  public STPReportData claimRegisterDate(String claimRegisterDate) {
    this.claimRegisterDate = claimRegisterDate;
    return this;
  }

  /**
   * Get claimRegisterDate
   * @return claimRegisterDate
  **/
  @ApiModelProperty(value = "")


  public String getClaimRegisterDate() {
    return claimRegisterDate;
  }

  public void setClaimRegisterDate(String claimRegisterDate) {
    this.claimRegisterDate = claimRegisterDate;
  }

  public STPReportData stPStatus(String stPStatus) {
    this.stPStatus = stPStatus;
    return this;
  }

  /**
   * Get stPStatus
   * @return stPStatus
  **/
  @ApiModelProperty(value = "")


  public String getStPStatus() {
    return stPStatus;
  }

  public void setStPStatus(String stPStatus) {
    this.stPStatus = stPStatus;
  }

  public STPReportData reasonForSTPFail(String reasonForSTPFail) {
    this.reasonForSTPFail = reasonForSTPFail;
    return this;
  }

  /**
   * Get reasonForSTPFail
   * @return reasonForSTPFail
  **/
  @ApiModelProperty(value = "")


  public String getReasonForSTPFail() {
    return reasonForSTPFail;
  }

  public void setReasonForSTPFail(String reasonForSTPFail) {
    this.reasonForSTPFail = reasonForSTPFail;
  }

  public STPReportData channel(String channel) {
    this.channel = channel;
    return this;
  }

  /**
   * Get channel
   * @return channel
  **/
  @ApiModelProperty(value = "")


  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    STPReportData stPReportData = (STPReportData) o;
    return Objects.equals(this.policyNumber, stPReportData.policyNumber) &&
        Objects.equals(this.claimNumber, stPReportData.claimNumber) &&
        Objects.equals(this.claimType, stPReportData.claimType) &&
        Objects.equals(this.claimStatus, stPReportData.claimStatus) &&
        Objects.equals(this.claimReceiveDate, stPReportData.claimReceiveDate) &&
        Objects.equals(this.claimRegisterDate, stPReportData.claimRegisterDate) &&
        Objects.equals(this.stPStatus, stPReportData.stPStatus) &&
        Objects.equals(this.reasonForSTPFail, stPReportData.reasonForSTPFail) &&
        Objects.equals(this.channel, stPReportData.channel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(policyNumber, claimNumber, claimType, claimStatus, claimReceiveDate, claimRegisterDate, stPStatus, reasonForSTPFail, channel);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class STPReportData {\n");
    
    sb.append("    policyNumber: ").append(toIndentedString(policyNumber)).append("\n");
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    claimType: ").append(toIndentedString(claimType)).append("\n");
    sb.append("    claimStatus: ").append(toIndentedString(claimStatus)).append("\n");
    sb.append("    claimReceiveDate: ").append(toIndentedString(claimReceiveDate)).append("\n");
    sb.append("    claimRegisterDate: ").append(toIndentedString(claimRegisterDate)).append("\n");
    sb.append("    stPStatus: ").append(toIndentedString(stPStatus)).append("\n");
    sb.append("    reasonForSTPFail: ").append(toIndentedString(reasonForSTPFail)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

