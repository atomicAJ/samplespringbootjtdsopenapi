package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TATReportData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class TATReportData   {
  @JsonProperty("policyNumber")
  private String policyNumber = null;

  @JsonProperty("claimNumber")
  private String claimNumber = null;

  @JsonProperty("claimType")
  private String claimType = null;

  @JsonProperty("claimStatus")
  private String claimStatus = null;

  @JsonProperty("channel")
  private String channel = null;

  @JsonProperty("productName")
  private String productName = null;

  @JsonProperty("policyStatus")
  private String policyStatus = null;

  @JsonProperty("RCDDate")
  private String rcDDate = null;

  @JsonProperty("ageOfPolicy")
  private String ageOfPolicy = null;

  @JsonProperty("policyOwner")
  private String policyOwner = null;

  @JsonProperty("insuredName")
  private String insuredName = null;

  @JsonProperty("receiveDate")
  private String receiveDate = null;

  @JsonProperty("registerDate")
  private String registerDate = null;

  @JsonProperty("dateOfDeath")
  private String dateOfDeath = null;

  @JsonProperty("admissionDate")
  private String admissionDate = null;

  @JsonProperty("dischargeDate")
  private String dischargeDate = null;

  @JsonProperty("lengthOfStay")
  private String lengthOfStay = null;

  @JsonProperty("diagnosis")
  private String diagnosis = null;

  @JsonProperty("providerName")
  private String providerName = null;

  @JsonProperty("payeeName")
  private String payeeName = null;

  @JsonProperty("sumAssured")
  private String sumAssured = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("amountIncurred")
  private String amountIncurred = null;

  @JsonProperty("amountApproved")
  private String amountApproved = null;

  @JsonProperty("amountNotApproved")
  private String amountNotApproved = null;

  @JsonProperty("pendingCompleteDate")
  private String pendingCompleteDate = null;

  @JsonProperty("claimDecisionDate")
  private String claimDecisionDate = null;

  @JsonProperty("tatClean")
  private String tatClean = null;

  @JsonProperty("tatAllProcess")
  private String tatAllProcess = null;

  @JsonProperty("tatTarget")
  private String tatTarget = null;

  @JsonProperty("tatAchieve")
  private String tatAchieve = null;

  @JsonProperty("remarkClaim")
  private String remarkClaim = null;

  @JsonProperty("fatcaCRS")
  private String fatcaCRS = null;

  @JsonProperty("agenName")
  private String agenName = null;

  @JsonProperty("salesOfficeName")
  private String salesOfficeName = null;

  @JsonProperty("userID")
  private String userID = null;

  public TATReportData policyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
    return this;
  }

  /**
   * Get policyNumber
   * @return policyNumber
  **/
  @ApiModelProperty(value = "")


  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public TATReportData claimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
    return this;
  }

  /**
   * Get claimNumber
   * @return claimNumber
  **/
  @ApiModelProperty(value = "")


  public String getClaimNumber() {
    return claimNumber;
  }

  public void setClaimNumber(String claimNumber) {
    this.claimNumber = claimNumber;
  }

  public TATReportData claimType(String claimType) {
    this.claimType = claimType;
    return this;
  }

  /**
   * Get claimType
   * @return claimType
  **/
  @ApiModelProperty(value = "")


  public String getClaimType() {
    return claimType;
  }

  public void setClaimType(String claimType) {
    this.claimType = claimType;
  }

  public TATReportData claimStatus(String claimStatus) {
    this.claimStatus = claimStatus;
    return this;
  }

  /**
   * Get claimStatus
   * @return claimStatus
  **/
  @ApiModelProperty(value = "")


  public String getClaimStatus() {
    return claimStatus;
  }

  public void setClaimStatus(String claimStatus) {
    this.claimStatus = claimStatus;
  }

  public TATReportData channel(String channel) {
    this.channel = channel;
    return this;
  }

  /**
   * Get channel
   * @return channel
  **/
  @ApiModelProperty(value = "")


  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public TATReportData productName(String productName) {
    this.productName = productName;
    return this;
  }

  /**
   * Get productName
   * @return productName
  **/
  @ApiModelProperty(value = "")


  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public TATReportData policyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
    return this;
  }

  /**
   * Get policyStatus
   * @return policyStatus
  **/
  @ApiModelProperty(value = "")


  public String getPolicyStatus() {
    return policyStatus;
  }

  public void setPolicyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
  }

  public TATReportData rcDDate(String rcDDate) {
    this.rcDDate = rcDDate;
    return this;
  }

  /**
   * Get rcDDate
   * @return rcDDate
  **/
  @ApiModelProperty(value = "")


  public String getRcDDate() {
    return rcDDate;
  }

  public void setRcDDate(String rcDDate) {
    this.rcDDate = rcDDate;
  }

  public TATReportData ageOfPolicy(String ageOfPolicy) {
    this.ageOfPolicy = ageOfPolicy;
    return this;
  }

  /**
   * Get ageOfPolicy
   * @return ageOfPolicy
  **/
  @ApiModelProperty(value = "")


  public String getAgeOfPolicy() {
    return ageOfPolicy;
  }

  public void setAgeOfPolicy(String ageOfPolicy) {
    this.ageOfPolicy = ageOfPolicy;
  }

  public TATReportData policyOwner(String policyOwner) {
    this.policyOwner = policyOwner;
    return this;
  }

  /**
   * Get policyOwner
   * @return policyOwner
  **/
  @ApiModelProperty(value = "")


  public String getPolicyOwner() {
    return policyOwner;
  }

  public void setPolicyOwner(String policyOwner) {
    this.policyOwner = policyOwner;
  }

  public TATReportData insuredName(String insuredName) {
    this.insuredName = insuredName;
    return this;
  }

  /**
   * Get insuredName
   * @return insuredName
  **/
  @ApiModelProperty(value = "")


  public String getInsuredName() {
    return insuredName;
  }

  public void setInsuredName(String insuredName) {
    this.insuredName = insuredName;
  }

  public TATReportData receiveDate(String receiveDate) {
    this.receiveDate = receiveDate;
    return this;
  }

  /**
   * Get receiveDate
   * @return receiveDate
  **/
  @ApiModelProperty(value = "")


  public String getReceiveDate() {
    return receiveDate;
  }

  public void setReceiveDate(String receiveDate) {
    this.receiveDate = receiveDate;
  }

  public TATReportData registerDate(String registerDate) {
    this.registerDate = registerDate;
    return this;
  }

  /**
   * Get registerDate
   * @return registerDate
  **/
  @ApiModelProperty(value = "")


  public String getRegisterDate() {
    return registerDate;
  }

  public void setRegisterDate(String registerDate) {
    this.registerDate = registerDate;
  }

  public TATReportData dateOfDeath(String dateOfDeath) {
    this.dateOfDeath = dateOfDeath;
    return this;
  }

  /**
   * Get dateOfDeath
   * @return dateOfDeath
  **/
  @ApiModelProperty(value = "")


  public String getDateOfDeath() {
    return dateOfDeath;
  }

  public void setDateOfDeath(String dateOfDeath) {
    this.dateOfDeath = dateOfDeath;
  }

  public TATReportData admissionDate(String admissionDate) {
    this.admissionDate = admissionDate;
    return this;
  }

  /**
   * Get admissionDate
   * @return admissionDate
  **/
  @ApiModelProperty(value = "")


  public String getAdmissionDate() {
    return admissionDate;
  }

  public void setAdmissionDate(String admissionDate) {
    this.admissionDate = admissionDate;
  }

  public TATReportData dischargeDate(String dischargeDate) {
    this.dischargeDate = dischargeDate;
    return this;
  }

  /**
   * Get dischargeDate
   * @return dischargeDate
  **/
  @ApiModelProperty(value = "")


  public String getDischargeDate() {
    return dischargeDate;
  }

  public void setDischargeDate(String dischargeDate) {
    this.dischargeDate = dischargeDate;
  }

  public TATReportData lengthOfStay(String lengthOfStay) {
    this.lengthOfStay = lengthOfStay;
    return this;
  }

  /**
   * Get lengthOfStay
   * @return lengthOfStay
  **/
  @ApiModelProperty(value = "")


  public String getLengthOfStay() {
    return lengthOfStay;
  }

  public void setLengthOfStay(String lengthOfStay) {
    this.lengthOfStay = lengthOfStay;
  }

  public TATReportData diagnosis(String diagnosis) {
    this.diagnosis = diagnosis;
    return this;
  }

  /**
   * Get diagnosis
   * @return diagnosis
  **/
  @ApiModelProperty(value = "")


  public String getDiagnosis() {
    return diagnosis;
  }

  public void setDiagnosis(String diagnosis) {
    this.diagnosis = diagnosis;
  }

  public TATReportData providerName(String providerName) {
    this.providerName = providerName;
    return this;
  }

  /**
   * Get providerName
   * @return providerName
  **/
  @ApiModelProperty(value = "")


  public String getProviderName() {
    return providerName;
  }

  public void setProviderName(String providerName) {
    this.providerName = providerName;
  }

  public TATReportData payeeName(String payeeName) {
    this.payeeName = payeeName;
    return this;
  }

  /**
   * Get payeeName
   * @return payeeName
  **/
  @ApiModelProperty(value = "")


  public String getPayeeName() {
    return payeeName;
  }

  public void setPayeeName(String payeeName) {
    this.payeeName = payeeName;
  }

  public TATReportData sumAssured(String sumAssured) {
    this.sumAssured = sumAssured;
    return this;
  }

  /**
   * Get sumAssured
   * @return sumAssured
  **/
  @ApiModelProperty(value = "")


  public String getSumAssured() {
    return sumAssured;
  }

  public void setSumAssured(String sumAssured) {
    this.sumAssured = sumAssured;
  }

  public TATReportData currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public TATReportData amountIncurred(String amountIncurred) {
    this.amountIncurred = amountIncurred;
    return this;
  }

  /**
   * Get amountIncurred
   * @return amountIncurred
  **/
  @ApiModelProperty(value = "")


  public String getAmountIncurred() {
    return amountIncurred;
  }

  public void setAmountIncurred(String amountIncurred) {
    this.amountIncurred = amountIncurred;
  }

  public TATReportData amountApproved(String amountApproved) {
    this.amountApproved = amountApproved;
    return this;
  }

  /**
   * Get amountApproved
   * @return amountApproved
  **/
  @ApiModelProperty(value = "")


  public String getAmountApproved() {
    return amountApproved;
  }

  public void setAmountApproved(String amountApproved) {
    this.amountApproved = amountApproved;
  }

  public TATReportData amountNotApproved(String amountNotApproved) {
    this.amountNotApproved = amountNotApproved;
    return this;
  }

  /**
   * Get amountNotApproved
   * @return amountNotApproved
  **/
  @ApiModelProperty(value = "")


  public String getAmountNotApproved() {
    return amountNotApproved;
  }

  public void setAmountNotApproved(String amountNotApproved) {
    this.amountNotApproved = amountNotApproved;
  }

  public TATReportData pendingCompleteDate(String pendingCompleteDate) {
    this.pendingCompleteDate = pendingCompleteDate;
    return this;
  }

  /**
   * Get pendingCompleteDate
   * @return pendingCompleteDate
  **/
  @ApiModelProperty(value = "")


  public String getPendingCompleteDate() {
    return pendingCompleteDate;
  }

  public void setPendingCompleteDate(String pendingCompleteDate) {
    this.pendingCompleteDate = pendingCompleteDate;
  }

  public TATReportData claimDecisionDate(String claimDecisionDate) {
    this.claimDecisionDate = claimDecisionDate;
    return this;
  }

  /**
   * Get claimDecisionDate
   * @return claimDecisionDate
  **/
  @ApiModelProperty(value = "")


  public String getClaimDecisionDate() {
    return claimDecisionDate;
  }

  public void setClaimDecisionDate(String claimDecisionDate) {
    this.claimDecisionDate = claimDecisionDate;
  }

  public TATReportData tatClean(String tatClean) {
    this.tatClean = tatClean;
    return this;
  }

  /**
   * Get tatClean
   * @return tatClean
  **/
  @ApiModelProperty(value = "")


  public String getTatClean() {
    return tatClean;
  }

  public void setTatClean(String tatClean) {
    this.tatClean = tatClean;
  }

  public TATReportData tatAllProcess(String tatAllProcess) {
    this.tatAllProcess = tatAllProcess;
    return this;
  }

  /**
   * Get tatAllProcess
   * @return tatAllProcess
  **/
  @ApiModelProperty(value = "")


  public String getTatAllProcess() {
    return tatAllProcess;
  }

  public void setTatAllProcess(String tatAllProcess) {
    this.tatAllProcess = tatAllProcess;
  }

  public TATReportData tatTarget(String tatTarget) {
    this.tatTarget = tatTarget;
    return this;
  }

  /**
   * Get tatTarget
   * @return tatTarget
  **/
  @ApiModelProperty(value = "")


  public String getTatTarget() {
    return tatTarget;
  }

  public void setTatTarget(String tatTarget) {
    this.tatTarget = tatTarget;
  }

  public TATReportData tatAchieve(String tatAchieve) {
    this.tatAchieve = tatAchieve;
    return this;
  }

  /**
   * Get tatAchieve
   * @return tatAchieve
  **/
  @ApiModelProperty(value = "")


  public String getTatAchieve() {
    return tatAchieve;
  }

  public void setTatAchieve(String tatAchieve) {
    this.tatAchieve = tatAchieve;
  }

  public TATReportData remarkClaim(String remarkClaim) {
    this.remarkClaim = remarkClaim;
    return this;
  }

  /**
   * Get remarkClaim
   * @return remarkClaim
  **/
  @ApiModelProperty(value = "")


  public String getRemarkClaim() {
    return remarkClaim;
  }

  public void setRemarkClaim(String remarkClaim) {
    this.remarkClaim = remarkClaim;
  }

  public TATReportData fatcaCRS(String fatcaCRS) {
    this.fatcaCRS = fatcaCRS;
    return this;
  }

  /**
   * Get fatcaCRS
   * @return fatcaCRS
  **/
  @ApiModelProperty(value = "")


  public String getFatcaCRS() {
    return fatcaCRS;
  }

  public void setFatcaCRS(String fatcaCRS) {
    this.fatcaCRS = fatcaCRS;
  }

  public TATReportData agenName(String agenName) {
    this.agenName = agenName;
    return this;
  }

  /**
   * Get agenName
   * @return agenName
  **/
  @ApiModelProperty(value = "")


  public String getAgenName() {
    return agenName;
  }

  public void setAgenName(String agenName) {
    this.agenName = agenName;
  }

  public TATReportData salesOfficeName(String salesOfficeName) {
    this.salesOfficeName = salesOfficeName;
    return this;
  }

  /**
   * Get salesOfficeName
   * @return salesOfficeName
  **/
  @ApiModelProperty(value = "")


  public String getSalesOfficeName() {
    return salesOfficeName;
  }

  public void setSalesOfficeName(String salesOfficeName) {
    this.salesOfficeName = salesOfficeName;
  }

  public TATReportData userID(String userID) {
    this.userID = userID;
    return this;
  }

  /**
   * Get userID
   * @return userID
  **/
  @ApiModelProperty(value = "")


  public String getUserID() {
    return userID;
  }

  public void setUserID(String userID) {
    this.userID = userID;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TATReportData taTReportData = (TATReportData) o;
    return Objects.equals(this.policyNumber, taTReportData.policyNumber) &&
        Objects.equals(this.claimNumber, taTReportData.claimNumber) &&
        Objects.equals(this.claimType, taTReportData.claimType) &&
        Objects.equals(this.claimStatus, taTReportData.claimStatus) &&
        Objects.equals(this.channel, taTReportData.channel) &&
        Objects.equals(this.productName, taTReportData.productName) &&
        Objects.equals(this.policyStatus, taTReportData.policyStatus) &&
        Objects.equals(this.rcDDate, taTReportData.rcDDate) &&
        Objects.equals(this.ageOfPolicy, taTReportData.ageOfPolicy) &&
        Objects.equals(this.policyOwner, taTReportData.policyOwner) &&
        Objects.equals(this.insuredName, taTReportData.insuredName) &&
        Objects.equals(this.receiveDate, taTReportData.receiveDate) &&
        Objects.equals(this.registerDate, taTReportData.registerDate) &&
        Objects.equals(this.dateOfDeath, taTReportData.dateOfDeath) &&
        Objects.equals(this.admissionDate, taTReportData.admissionDate) &&
        Objects.equals(this.dischargeDate, taTReportData.dischargeDate) &&
        Objects.equals(this.lengthOfStay, taTReportData.lengthOfStay) &&
        Objects.equals(this.diagnosis, taTReportData.diagnosis) &&
        Objects.equals(this.providerName, taTReportData.providerName) &&
        Objects.equals(this.payeeName, taTReportData.payeeName) &&
        Objects.equals(this.sumAssured, taTReportData.sumAssured) &&
        Objects.equals(this.currency, taTReportData.currency) &&
        Objects.equals(this.amountIncurred, taTReportData.amountIncurred) &&
        Objects.equals(this.amountApproved, taTReportData.amountApproved) &&
        Objects.equals(this.amountNotApproved, taTReportData.amountNotApproved) &&
        Objects.equals(this.pendingCompleteDate, taTReportData.pendingCompleteDate) &&
        Objects.equals(this.claimDecisionDate, taTReportData.claimDecisionDate) &&
        Objects.equals(this.tatClean, taTReportData.tatClean) &&
        Objects.equals(this.tatAllProcess, taTReportData.tatAllProcess) &&
        Objects.equals(this.tatTarget, taTReportData.tatTarget) &&
        Objects.equals(this.tatAchieve, taTReportData.tatAchieve) &&
        Objects.equals(this.remarkClaim, taTReportData.remarkClaim) &&
        Objects.equals(this.fatcaCRS, taTReportData.fatcaCRS) &&
        Objects.equals(this.agenName, taTReportData.agenName) &&
        Objects.equals(this.salesOfficeName, taTReportData.salesOfficeName) &&
        Objects.equals(this.userID, taTReportData.userID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(policyNumber, claimNumber, claimType, claimStatus, channel, productName, policyStatus, rcDDate, ageOfPolicy, policyOwner, insuredName, receiveDate, registerDate, dateOfDeath, admissionDate, dischargeDate, lengthOfStay, diagnosis, providerName, payeeName, sumAssured, currency, amountIncurred, amountApproved, amountNotApproved, pendingCompleteDate, claimDecisionDate, tatClean, tatAllProcess, tatTarget, tatAchieve, remarkClaim, fatcaCRS, agenName, salesOfficeName, userID);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TATReportData {\n");
    
    sb.append("    policyNumber: ").append(toIndentedString(policyNumber)).append("\n");
    sb.append("    claimNumber: ").append(toIndentedString(claimNumber)).append("\n");
    sb.append("    claimType: ").append(toIndentedString(claimType)).append("\n");
    sb.append("    claimStatus: ").append(toIndentedString(claimStatus)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    productName: ").append(toIndentedString(productName)).append("\n");
    sb.append("    policyStatus: ").append(toIndentedString(policyStatus)).append("\n");
    sb.append("    rcDDate: ").append(toIndentedString(rcDDate)).append("\n");
    sb.append("    ageOfPolicy: ").append(toIndentedString(ageOfPolicy)).append("\n");
    sb.append("    policyOwner: ").append(toIndentedString(policyOwner)).append("\n");
    sb.append("    insuredName: ").append(toIndentedString(insuredName)).append("\n");
    sb.append("    receiveDate: ").append(toIndentedString(receiveDate)).append("\n");
    sb.append("    registerDate: ").append(toIndentedString(registerDate)).append("\n");
    sb.append("    dateOfDeath: ").append(toIndentedString(dateOfDeath)).append("\n");
    sb.append("    admissionDate: ").append(toIndentedString(admissionDate)).append("\n");
    sb.append("    dischargeDate: ").append(toIndentedString(dischargeDate)).append("\n");
    sb.append("    lengthOfStay: ").append(toIndentedString(lengthOfStay)).append("\n");
    sb.append("    diagnosis: ").append(toIndentedString(diagnosis)).append("\n");
    sb.append("    providerName: ").append(toIndentedString(providerName)).append("\n");
    sb.append("    payeeName: ").append(toIndentedString(payeeName)).append("\n");
    sb.append("    sumAssured: ").append(toIndentedString(sumAssured)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    amountIncurred: ").append(toIndentedString(amountIncurred)).append("\n");
    sb.append("    amountApproved: ").append(toIndentedString(amountApproved)).append("\n");
    sb.append("    amountNotApproved: ").append(toIndentedString(amountNotApproved)).append("\n");
    sb.append("    pendingCompleteDate: ").append(toIndentedString(pendingCompleteDate)).append("\n");
    sb.append("    claimDecisionDate: ").append(toIndentedString(claimDecisionDate)).append("\n");
    sb.append("    tatClean: ").append(toIndentedString(tatClean)).append("\n");
    sb.append("    tatAllProcess: ").append(toIndentedString(tatAllProcess)).append("\n");
    sb.append("    tatTarget: ").append(toIndentedString(tatTarget)).append("\n");
    sb.append("    tatAchieve: ").append(toIndentedString(tatAchieve)).append("\n");
    sb.append("    remarkClaim: ").append(toIndentedString(remarkClaim)).append("\n");
    sb.append("    fatcaCRS: ").append(toIndentedString(fatcaCRS)).append("\n");
    sb.append("    agenName: ").append(toIndentedString(agenName)).append("\n");
    sb.append("    salesOfficeName: ").append(toIndentedString(salesOfficeName)).append("\n");
    sb.append("    userID: ").append(toIndentedString(userID)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

