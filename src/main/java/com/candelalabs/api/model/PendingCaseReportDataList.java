package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PendingCaseReportDataList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-10-21T10:15:09.840+05:30")

public class PendingCaseReportDataList   {
  @JsonProperty("strColName")
  private String strColName = null;

  @JsonProperty("strColValue")
  private String strColValue = null;

  @JsonProperty("strColNum")
  private String strColNum = null;

  public PendingCaseReportDataList strColName(String strColName) {
    this.strColName = strColName;
    return this;
  }

  /**
   * Get strColName
   * @return strColName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrColName() {
    return strColName;
  }

  public void setStrColName(String strColName) {
    this.strColName = strColName;
  }

  public PendingCaseReportDataList strColValue(String strColValue) {
    this.strColValue = strColValue;
    return this;
  }

  /**
   * Get strColValue
   * @return strColValue
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrColValue() {
    return strColValue;
  }

  public void setStrColValue(String strColValue) {
    this.strColValue = strColValue;
  }

  public PendingCaseReportDataList strColNum(String strColNum) {
    this.strColNum = strColNum;
    return this;
  }

  /**
   * Get strColNum
   * @return strColNum
  **/
  @ApiModelProperty(example = "", value = "")


  public String getStrColNum() {
    return strColNum;
  }

  public void setStrColNum(String strColNum) {
    this.strColNum = strColNum;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PendingCaseReportDataList pendingCaseReportDataList = (PendingCaseReportDataList) o;
    return Objects.equals(this.strColName, pendingCaseReportDataList.strColName) &&
        Objects.equals(this.strColValue, pendingCaseReportDataList.strColValue) &&
        Objects.equals(this.strColNum, pendingCaseReportDataList.strColNum);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strColName, strColValue, strColNum);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PendingCaseReportDataList {\n");
    
    sb.append("    strColName: ").append(toIndentedString(strColName)).append("\n");
    sb.append("    strColValue: ").append(toIndentedString(strColValue)).append("\n");
    sb.append("    strColNum: ").append(toIndentedString(strColNum)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

