package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * BeneficiaryList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class BeneficiaryList   {
  @JsonProperty("strBeneficiaryClientNo")
  private String strBeneficiaryClientNo = null;

  @JsonProperty("strBeneficiaryIDNumber")
  private String strBeneficiaryIDNumber = null;

  @JsonProperty("strBeneficiaryClientName")
  private String strBeneficiaryClientName = null;

  @JsonProperty("strBeneficiaryRelationship")
  private String strBeneficiaryRelationship = null;

  @JsonProperty("strBenefitPercentage")
  private String strBenefitPercentage = null;

  @JsonProperty("strBeneficiaryGender")
  private String strBeneficiaryGender = null;

  @JsonProperty("dtDateOfBirth")
  private String dtDateOfBirth = null;

  public BeneficiaryList strBeneficiaryClientNo(String strBeneficiaryClientNo) {
    this.strBeneficiaryClientNo = strBeneficiaryClientNo;
    return this;
  }

  /**
   * Get strBeneficiaryClientNo
   * @return strBeneficiaryClientNo
  **/
  @ApiModelProperty(value = "")


  public String getStrBeneficiaryClientNo() {
    return strBeneficiaryClientNo;
  }

  public void setStrBeneficiaryClientNo(String strBeneficiaryClientNo) {
    this.strBeneficiaryClientNo = strBeneficiaryClientNo;
  }

  public BeneficiaryList strBeneficiaryIDNumber(String strBeneficiaryIDNumber) {
    this.strBeneficiaryIDNumber = strBeneficiaryIDNumber;
    return this;
  }

  /**
   * Get strBeneficiaryIDNumber
   * @return strBeneficiaryIDNumber
  **/
  @ApiModelProperty(value = "")


  public String getStrBeneficiaryIDNumber() {
    return strBeneficiaryIDNumber;
  }

  public void setStrBeneficiaryIDNumber(String strBeneficiaryIDNumber) {
    this.strBeneficiaryIDNumber = strBeneficiaryIDNumber;
  }

  public BeneficiaryList strBeneficiaryClientName(String strBeneficiaryClientName) {
    this.strBeneficiaryClientName = strBeneficiaryClientName;
    return this;
  }

  /**
   * Get strBeneficiaryClientName
   * @return strBeneficiaryClientName
  **/
  @ApiModelProperty(value = "")


  public String getStrBeneficiaryClientName() {
    return strBeneficiaryClientName;
  }

  public void setStrBeneficiaryClientName(String strBeneficiaryClientName) {
    this.strBeneficiaryClientName = strBeneficiaryClientName;
  }

  public BeneficiaryList strBeneficiaryRelationship(String strBeneficiaryRelationship) {
    this.strBeneficiaryRelationship = strBeneficiaryRelationship;
    return this;
  }

  /**
   * Get strBeneficiaryRelationship
   * @return strBeneficiaryRelationship
  **/
  @ApiModelProperty(value = "")


  public String getStrBeneficiaryRelationship() {
    return strBeneficiaryRelationship;
  }

  public void setStrBeneficiaryRelationship(String strBeneficiaryRelationship) {
    this.strBeneficiaryRelationship = strBeneficiaryRelationship;
  }

  public BeneficiaryList strBenefitPercentage(String strBenefitPercentage) {
    this.strBenefitPercentage = strBenefitPercentage;
    return this;
  }

  /**
   * Get strBenefitPercentage
   * @return strBenefitPercentage
  **/
  @ApiModelProperty(value = "")


  public String getStrBenefitPercentage() {
    return strBenefitPercentage;
  }

  public void setStrBenefitPercentage(String strBenefitPercentage) {
    this.strBenefitPercentage = strBenefitPercentage;
  }

  public BeneficiaryList strBeneficiaryGender(String strBeneficiaryGender) {
    this.strBeneficiaryGender = strBeneficiaryGender;
    return this;
  }

  /**
   * Get strBeneficiaryGender
   * @return strBeneficiaryGender
  **/
  @ApiModelProperty(value = "")


  public String getStrBeneficiaryGender() {
    return strBeneficiaryGender;
  }

  public void setStrBeneficiaryGender(String strBeneficiaryGender) {
    this.strBeneficiaryGender = strBeneficiaryGender;
  }

  public BeneficiaryList dtDateOfBirth(String dtDateOfBirth) {
    this.dtDateOfBirth = dtDateOfBirth;
    return this;
  }

  /**
   * Get dtDateOfBirth
   * @return dtDateOfBirth
  **/
  @ApiModelProperty(value = "")


  public String getDtDateOfBirth() {
    return dtDateOfBirth;
  }

  public void setDtDateOfBirth(String dtDateOfBirth) {
    this.dtDateOfBirth = dtDateOfBirth;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BeneficiaryList beneficiaryList = (BeneficiaryList) o;
    return Objects.equals(this.strBeneficiaryClientNo, beneficiaryList.strBeneficiaryClientNo) &&
        Objects.equals(this.strBeneficiaryIDNumber, beneficiaryList.strBeneficiaryIDNumber) &&
        Objects.equals(this.strBeneficiaryClientName, beneficiaryList.strBeneficiaryClientName) &&
        Objects.equals(this.strBeneficiaryRelationship, beneficiaryList.strBeneficiaryRelationship) &&
        Objects.equals(this.strBenefitPercentage, beneficiaryList.strBenefitPercentage) &&
        Objects.equals(this.strBeneficiaryGender, beneficiaryList.strBeneficiaryGender) &&
        Objects.equals(this.dtDateOfBirth, beneficiaryList.dtDateOfBirth);
  }

  @Override
  public int hashCode() {
    return Objects.hash(strBeneficiaryClientNo, strBeneficiaryIDNumber, strBeneficiaryClientName, strBeneficiaryRelationship, strBenefitPercentage, strBeneficiaryGender, dtDateOfBirth);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BeneficiaryList {\n");
    
    sb.append("    strBeneficiaryClientNo: ").append(toIndentedString(strBeneficiaryClientNo)).append("\n");
    sb.append("    strBeneficiaryIDNumber: ").append(toIndentedString(strBeneficiaryIDNumber)).append("\n");
    sb.append("    strBeneficiaryClientName: ").append(toIndentedString(strBeneficiaryClientName)).append("\n");
    sb.append("    strBeneficiaryRelationship: ").append(toIndentedString(strBeneficiaryRelationship)).append("\n");
    sb.append("    strBenefitPercentage: ").append(toIndentedString(strBenefitPercentage)).append("\n");
    sb.append("    strBeneficiaryGender: ").append(toIndentedString(strBeneficiaryGender)).append("\n");
    sb.append("    dtDateOfBirth: ").append(toIndentedString(dtDateOfBirth)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

