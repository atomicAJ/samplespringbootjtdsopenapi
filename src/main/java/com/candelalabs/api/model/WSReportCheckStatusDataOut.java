package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.WSReportCheckStatusDataOutWSResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSReportCheckStatusDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSReportCheckStatusDataOut   {
  @JsonProperty("WSResponse")
  private WSReportCheckStatusDataOutWSResponse wsResponse = null;

  public WSReportCheckStatusDataOut wsResponse(WSReportCheckStatusDataOutWSResponse wsResponse) {
    this.wsResponse = wsResponse;
    return this;
  }

  /**
   * Get wsResponse
   * @return wsResponse
  **/
  @ApiModelProperty(value = "")

  @Valid

  public WSReportCheckStatusDataOutWSResponse getWsResponse() {
    return wsResponse;
  }

  public void setWsResponse(WSReportCheckStatusDataOutWSResponse wsResponse) {
    this.wsResponse = wsResponse;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSReportCheckStatusDataOut wsReportCheckStatusDataOut = (WSReportCheckStatusDataOut) o;
    return Objects.equals(this.wsResponse, wsReportCheckStatusDataOut.wsResponse);
  }

  @Override
  public int hashCode() {
    return Objects.hash(wsResponse);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSReportCheckStatusDataOut {\n");
    
    sb.append("    wsResponse: ").append(toIndentedString(wsResponse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

