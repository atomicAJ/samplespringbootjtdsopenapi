package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSupdateClaimUserDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSupdateClaimUserDataIn   {
  @JsonProperty("ClaimNo")
  private String claimNo = null;

  @JsonProperty("activityUser")
  private String activityUser = null;

  public WSupdateClaimUserDataIn claimNo(String claimNo) {
    this.claimNo = claimNo;
    return this;
  }

  /**
   * Get claimNo
   * @return claimNo
  **/
  @ApiModelProperty(example = "[claim Number]", value = "")


  public String getClaimNo() {
    return claimNo;
  }

  public void setClaimNo(String claimNo) {
    this.claimNo = claimNo;
  }

  public WSupdateClaimUserDataIn activityUser(String activityUser) {
    this.activityUser = activityUser;
    return this;
  }

  /**
   * Get activityUser
   * @return activityUser
  **/
  @ApiModelProperty(example = "[activityId]", value = "")


  public String getActivityUser() {
    return activityUser;
  }

  public void setActivityUser(String activityUser) {
    this.activityUser = activityUser;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSupdateClaimUserDataIn wsupdateClaimUserDataIn = (WSupdateClaimUserDataIn) o;
    return Objects.equals(this.claimNo, wsupdateClaimUserDataIn.claimNo) &&
        Objects.equals(this.activityUser, wsupdateClaimUserDataIn.activityUser);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNo, activityUser);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSupdateClaimUserDataIn {\n");
    
    sb.append("    claimNo: ").append(toIndentedString(claimNo)).append("\n");
    sb.append("    activityUser: ").append(toIndentedString(activityUser)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

