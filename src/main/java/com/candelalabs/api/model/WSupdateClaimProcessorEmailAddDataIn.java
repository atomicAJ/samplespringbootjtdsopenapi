package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSupdateClaimProcessorEmailAddDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSupdateClaimProcessorEmailAddDataIn   {
  @JsonProperty("claimNo")
  private String claimNo = null;

  @JsonProperty("emailAdd")
  private String emailAdd = null;

  public WSupdateClaimProcessorEmailAddDataIn claimNo(String claimNo) {
    this.claimNo = claimNo;
    return this;
  }

  /**
   * Get claimNo
   * @return claimNo
  **/
  @ApiModelProperty(example = "claimNo", required = true, value = "")
  @NotNull


  public String getClaimNo() {
    return claimNo;
  }

  public void setClaimNo(String claimNo) {
    this.claimNo = claimNo;
  }

  public WSupdateClaimProcessorEmailAddDataIn emailAdd(String emailAdd) {
    this.emailAdd = emailAdd;
    return this;
  }

  /**
   * Get emailAdd
   * @return emailAdd
  **/
  @ApiModelProperty(example = "emailAdd", required = true, value = "")
  @NotNull


  public String getEmailAdd() {
    return emailAdd;
  }

  public void setEmailAdd(String emailAdd) {
    this.emailAdd = emailAdd;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSupdateClaimProcessorEmailAddDataIn wsupdateClaimProcessorEmailAddDataIn = (WSupdateClaimProcessorEmailAddDataIn) o;
    return Objects.equals(this.claimNo, wsupdateClaimProcessorEmailAddDataIn.claimNo) &&
        Objects.equals(this.emailAdd, wsupdateClaimProcessorEmailAddDataIn.emailAdd);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimNo, emailAdd);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSupdateClaimProcessorEmailAddDataIn {\n");
    
    sb.append("    claimNo: ").append(toIndentedString(claimNo)).append("\n");
    sb.append("    emailAdd: ").append(toIndentedString(emailAdd)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

