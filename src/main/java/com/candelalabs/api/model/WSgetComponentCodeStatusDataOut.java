package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSgetComponentCodeStatusDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSgetComponentCodeStatusDataOut   {
  @JsonProperty("COMPONENTSTATUS")
  private String COMPONENTSTATUS = null;

  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  public WSgetComponentCodeStatusDataOut COMPONENTSTATUS(String COMPONENTSTATUS) {
    this.COMPONENTSTATUS = COMPONENTSTATUS;
    return this;
  }

  /**
   * Get COMPONENTSTATUS
   * @return COMPONENTSTATUS
  **/
  @ApiModelProperty(example = "COMPONENTSTATUS", value = "")


  public String getCOMPONENTSTATUS() {
    return COMPONENTSTATUS;
  }

  public void setCOMPONENTSTATUS(String COMPONENTSTATUS) {
    this.COMPONENTSTATUS = COMPONENTSTATUS;
  }

  public WSgetComponentCodeStatusDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSgetComponentCodeStatusDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSgetComponentCodeStatusDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "string", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSgetComponentCodeStatusDataOut wsgetComponentCodeStatusDataOut = (WSgetComponentCodeStatusDataOut) o;
    return Objects.equals(this.COMPONENTSTATUS, wsgetComponentCodeStatusDataOut.COMPONENTSTATUS) &&
        Objects.equals(this.wsProcessingStatus, wsgetComponentCodeStatusDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsgetComponentCodeStatusDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsgetComponentCodeStatusDataOut.wsSuccessMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(COMPONENTSTATUS, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSgetComponentCodeStatusDataOut {\n");
    
    sb.append("    COMPONENTSTATUS: ").append(toIndentedString(COMPONENTSTATUS)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

