package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JsonWSChkExcSrcProcessorDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class JsonWSChkExcSrcProcessorDataIn   {
  @JsonProperty("SOURCEEXCEPTIONSTEP")
  private String SOURCEEXCEPTIONSTEP = null;

  @JsonProperty("SOURCEEXCEPTIONPROCESS")
  private String SOURCEEXCEPTIONPROCESS = null;

  public JsonWSChkExcSrcProcessorDataIn SOURCEEXCEPTIONSTEP(String SOURCEEXCEPTIONSTEP) {
    this.SOURCEEXCEPTIONSTEP = SOURCEEXCEPTIONSTEP;
    return this;
  }

  /**
   * Get SOURCEEXCEPTIONSTEP
   * @return SOURCEEXCEPTIONSTEP
  **/
  @ApiModelProperty(example = "[SOURCEEXCEPTIONSTEP]", required = true, value = "")
  @NotNull


  public String getSOURCEEXCEPTIONSTEP() {
    return SOURCEEXCEPTIONSTEP;
  }

  public void setSOURCEEXCEPTIONSTEP(String SOURCEEXCEPTIONSTEP) {
    this.SOURCEEXCEPTIONSTEP = SOURCEEXCEPTIONSTEP;
  }

  public JsonWSChkExcSrcProcessorDataIn SOURCEEXCEPTIONPROCESS(String SOURCEEXCEPTIONPROCESS) {
    this.SOURCEEXCEPTIONPROCESS = SOURCEEXCEPTIONPROCESS;
    return this;
  }

  /**
   * Get SOURCEEXCEPTIONPROCESS
   * @return SOURCEEXCEPTIONPROCESS
  **/
  @ApiModelProperty(example = "[Cashless]", required = true, value = "")
  @NotNull


  public String getSOURCEEXCEPTIONPROCESS() {
    return SOURCEEXCEPTIONPROCESS;
  }

  public void setSOURCEEXCEPTIONPROCESS(String SOURCEEXCEPTIONPROCESS) {
    this.SOURCEEXCEPTIONPROCESS = SOURCEEXCEPTIONPROCESS;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonWSChkExcSrcProcessorDataIn jsonWSChkExcSrcProcessorDataIn = (JsonWSChkExcSrcProcessorDataIn) o;
    return Objects.equals(this.SOURCEEXCEPTIONSTEP, jsonWSChkExcSrcProcessorDataIn.SOURCEEXCEPTIONSTEP) &&
        Objects.equals(this.SOURCEEXCEPTIONPROCESS, jsonWSChkExcSrcProcessorDataIn.SOURCEEXCEPTIONPROCESS);
  }

  @Override
  public int hashCode() {
    return Objects.hash(SOURCEEXCEPTIONSTEP, SOURCEEXCEPTIONPROCESS);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonWSChkExcSrcProcessorDataIn {\n");
    
    sb.append("    SOURCEEXCEPTIONSTEP: ").append(toIndentedString(SOURCEEXCEPTIONSTEP)).append("\n");
    sb.append("    SOURCEEXCEPTIONPROCESS: ").append(toIndentedString(SOURCEEXCEPTIONPROCESS)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

