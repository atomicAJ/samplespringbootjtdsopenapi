package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSCheckMandatoryDocsDataIn
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSCheckMandatoryDocsDataIn   {
  @JsonProperty("CLAIMNUMBER")
  private String CLAIMNUMBER = null;

  public WSCheckMandatoryDocsDataIn CLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
    return this;
  }

  /**
   * Get CLAIMNUMBER
   * @return CLAIMNUMBER
  **/
  @ApiModelProperty(example = "[CLAIMNUMBER]", required = true, value = "")
  @NotNull


  public String getCLAIMNUMBER() {
    return CLAIMNUMBER;
  }

  public void setCLAIMNUMBER(String CLAIMNUMBER) {
    this.CLAIMNUMBER = CLAIMNUMBER;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSCheckMandatoryDocsDataIn wsCheckMandatoryDocsDataIn = (WSCheckMandatoryDocsDataIn) o;
    return Objects.equals(this.CLAIMNUMBER, wsCheckMandatoryDocsDataIn.CLAIMNUMBER);
  }

  @Override
  public int hashCode() {
    return Objects.hash(CLAIMNUMBER);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSCheckMandatoryDocsDataIn {\n");
    
    sb.append("    CLAIMNUMBER: ").append(toIndentedString(CLAIMNUMBER)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

