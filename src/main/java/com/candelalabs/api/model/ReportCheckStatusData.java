package com.candelalabs.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ReportCheckStatusData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class ReportCheckStatusData   {
  @JsonProperty("webColumnName")
  private String webColumnName = null;

  @JsonProperty("webColumnValue")
  private String webColumnValue = null;

  public ReportCheckStatusData webColumnName(String webColumnName) {
    this.webColumnName = webColumnName;
    return this;
  }

  /**
   * Get webColumnName
   * @return webColumnName
  **/
  @ApiModelProperty(example = "", value = "")


  public String getWebColumnName() {
    return webColumnName;
  }

  public void setWebColumnName(String webColumnName) {
    this.webColumnName = webColumnName;
  }

  public ReportCheckStatusData webColumnValue(String webColumnValue) {
    this.webColumnValue = webColumnValue;
    return this;
  }

  /**
   * Get webColumnValue
   * @return webColumnValue
  **/
  @ApiModelProperty(example = "", value = "")


  public String getWebColumnValue() {
    return webColumnValue;
  }

  public void setWebColumnValue(String webColumnValue) {
    this.webColumnValue = webColumnValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReportCheckStatusData reportCheckStatusData = (ReportCheckStatusData) o;
    return Objects.equals(this.webColumnName, reportCheckStatusData.webColumnName) &&
        Objects.equals(this.webColumnValue, reportCheckStatusData.webColumnValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(webColumnName, webColumnValue);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReportCheckStatusData {\n");
    
    sb.append("    webColumnName: ").append(toIndentedString(webColumnName)).append("\n");
    sb.append("    webColumnValue: ").append(toIndentedString(webColumnValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

