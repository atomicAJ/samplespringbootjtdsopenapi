package com.candelalabs.api.model;

import java.util.Objects;
import com.candelalabs.api.model.GetClaimDetailsList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * WSPortalServiceDataOut
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-12-05T17:39:56.323+05:30")

public class WSPortalServiceDataOut   {
  @JsonProperty("claimData")
  @Valid
  private List<GetClaimDetailsList> claimData = null;

  @JsonProperty("WSProcessingStatus")
  private String wsProcessingStatus = null;

  @JsonProperty("WSExceptionMessage")
  private String wsExceptionMessage = null;

  @JsonProperty("WSSuccessMessage")
  private String wsSuccessMessage = null;

  public WSPortalServiceDataOut claimData(List<GetClaimDetailsList> claimData) {
    this.claimData = claimData;
    return this;
  }

  public WSPortalServiceDataOut addClaimDataItem(GetClaimDetailsList claimDataItem) {
    if (this.claimData == null) {
      this.claimData = new ArrayList<>();
    }
    this.claimData.add(claimDataItem);
    return this;
  }

  /**
   * Get claimData
   * @return claimData
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<GetClaimDetailsList> getClaimData() {
    return claimData;
  }

  public void setClaimData(List<GetClaimDetailsList> claimData) {
    this.claimData = claimData;
  }

  public WSPortalServiceDataOut wsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
    return this;
  }

  /**
   * Get wsProcessingStatus
   * @return wsProcessingStatus
  **/
  @ApiModelProperty(example = "1", value = "")


  public String getWsProcessingStatus() {
    return wsProcessingStatus;
  }

  public void setWsProcessingStatus(String wsProcessingStatus) {
    this.wsProcessingStatus = wsProcessingStatus;
  }

  public WSPortalServiceDataOut wsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
    return this;
  }

  /**
   * Get wsExceptionMessage
   * @return wsExceptionMessage
  **/
  @ApiModelProperty(example = "exception message", value = "")


  public String getWsExceptionMessage() {
    return wsExceptionMessage;
  }

  public void setWsExceptionMessage(String wsExceptionMessage) {
    this.wsExceptionMessage = wsExceptionMessage;
  }

  public WSPortalServiceDataOut wsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
    return this;
  }

  /**
   * Get wsSuccessMessage
   * @return wsSuccessMessage
  **/
  @ApiModelProperty(example = "success message", value = "")


  public String getWsSuccessMessage() {
    return wsSuccessMessage;
  }

  public void setWsSuccessMessage(String wsSuccessMessage) {
    this.wsSuccessMessage = wsSuccessMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WSPortalServiceDataOut wsPortalServiceDataOut = (WSPortalServiceDataOut) o;
    return Objects.equals(this.claimData, wsPortalServiceDataOut.claimData) &&
        Objects.equals(this.wsProcessingStatus, wsPortalServiceDataOut.wsProcessingStatus) &&
        Objects.equals(this.wsExceptionMessage, wsPortalServiceDataOut.wsExceptionMessage) &&
        Objects.equals(this.wsSuccessMessage, wsPortalServiceDataOut.wsSuccessMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(claimData, wsProcessingStatus, wsExceptionMessage, wsSuccessMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WSPortalServiceDataOut {\n");
    
    sb.append("    claimData: ").append(toIndentedString(claimData)).append("\n");
    sb.append("    wsProcessingStatus: ").append(toIndentedString(wsProcessingStatus)).append("\n");
    sb.append("    wsExceptionMessage: ").append(toIndentedString(wsExceptionMessage)).append("\n");
    sb.append("    wsSuccessMessage: ").append(toIndentedString(wsSuccessMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

