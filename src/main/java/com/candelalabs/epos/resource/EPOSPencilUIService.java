package com.candelalabs.epos.resource;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.candelalabs.epos.model.ActivatedPremiumHoliday;
import com.candelalabs.epos.model.AdditionalDocument;
import com.candelalabs.epos.model.Attribute;
import com.candelalabs.epos.model.AutoReinstatement;
import com.candelalabs.epos.model.ChangeAddress;
import com.candelalabs.epos.model.ChangeEmail;
import com.candelalabs.epos.model.ChangeId;
import com.candelalabs.epos.model.ChangeName;
import com.candelalabs.epos.model.ChangeOfBenificary;
import com.candelalabs.epos.model.ChangePaymentFrequency;
import com.candelalabs.epos.model.ChangePaymentMethod;
import com.candelalabs.epos.model.ChangePhoneNumber;
import com.candelalabs.epos.model.FundAllocation;
import com.candelalabs.epos.model.FundAllocationDB;
import com.candelalabs.epos.model.FundSwitch;
import com.candelalabs.epos.model.HQClientDetails;
import com.candelalabs.epos.model.HQDetails;
import com.candelalabs.epos.model.POSTransactionDetails;
import com.candelalabs.epos.model.PartialWithdrawal;
import com.candelalabs.epos.model.PaymentCreator;
import com.candelalabs.epos.model.PersonalInfo;
import com.candelalabs.epos.model.PolicyDuplicate;
import com.candelalabs.epos.model.PolicyInfo;
import com.candelalabs.epos.model.RegularTopUp;
import com.candelalabs.epos.model.Reinstatement;
import com.candelalabs.epos.model.ReprintHS;
import com.candelalabs.epos.model.SingleTopUp;
import com.candelalabs.epos.model.TransactionData;
import com.candelalabs.epos.model.TransactionDataWrapper;
import com.candelalabs.epos.model.TransactionHistory;
import com.candelalabs.ws.dao.PosDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.POSDataModel;
import com.candelalabs.ws.util.DateUtil;

@Service
public class EPOSPencilUIService
{
	private static final Log log = LogFactory.getLog(EPOSPencilUIService.class);

	@Autowired
	private PosDetailsDao dao;

	private List<Attribute> getHeader(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		int rbaScore = 0;
		JSONObject parseJSON = new JSONObject();
		try
		{
			rbaScore = dao.getRBAScore(posData.getPolicyNumber());

		}
		catch (Exception e)
		{
			log.error("Error getting RBA Score:", e);
		}
		try
		{
			parseJSON = getTransactionDataFromJson(posData.getTransactionData());
		}
		catch (Exception e)
		{
			log.error("Error parsing transaction data", e);
		}
		ArrayList<Attribute> arrayList = new ArrayList<>();
		arrayList.add(new Attribute("POS Request Number", posData.getPOSRequestNumber(), "String"));
		arrayList.add(new Attribute("Policy Number", posData.getPolicyNumber(), "String"));
		arrayList.add(new Attribute("Fatca/CRS Flag Owner", clientDetails.getFATCAFLAG(), "String"));
		arrayList.add(new Attribute("Fatca/CRS Flag Form", parseJSON.get("strFatcaCRSFlag") != null ? parseJSON.get("strFatcaCRSFlag").toString() : "", "String"));
		arrayList.add(new Attribute("Type of Transaction", posData.getTransactionDescription(), "String"));
		arrayList.add(new Attribute("RBA Score", rbaScore, "Integer"));
		arrayList.add(new Attribute("Request Submission Date", DateUtil.formatDate(posData.getRequestSubmissionTS()), "DATE"));
		arrayList.add(new Attribute("Request Received Date", DateUtil.formatDate(posData.getProcessInitiatonTS()), "DATE"));
		arrayList.add(new Attribute("Channel Submission ", parseJSON.get("strChannelType") != null ? parseJSON.get("strChannelType").toString() : "", "String"));
		return arrayList;
	}

	public POSTransactionDetails getTransactionData(String policyNumber, String tranxId) throws Exception
	{
		log.info("Fetching transaction data: policyNumber- " + policyNumber);
		log.info("Fetching transaction data: tranxId- " + tranxId);

		POSDataModel posData = dao.getPosDataModel(policyNumber, tranxId);
		ClientDetails clientDetails = null;
		log.info("Fetching InsuredClientID- " + posData.getInsuredClientID());
		if (null != posData.getInsuredClientID())
			clientDetails = dao.getClientDetails(posData.getInsuredClientID());

		POSTransactionDetails res = new POSTransactionDetails();
		log.info("Fetching trasactiond fields");

		res.setTransactionFields(getHeader(posData, clientDetails));
		log.info("Fetching trasaction history");

		String transxDesc = posData.getTransactionDescription();

		TransactionData data = getTransactionTypeData(transxDesc, posData, clientDetails);

		res.setTransactionTypeData(data);
		log.info(res);

		try
		{
			List<TransactionHistory> listTransactionHistory = dao.listTransactionHistory(policyNumber);
			res.setTransactionHistory(listTransactionHistory);
		}
		catch (Exception e)
		{
			log.error("Error getting transaction history", e);
		}

		try
		{
			log.info("Fetching Additional Documents ");
			List<AdditionalDocument> listAdditionalDocuments = dao.listAdditionalDocuments(posData.getPOSRequestNumber());
			res.setAdditionalDocuments(listAdditionalDocuments);
		}
		catch (Exception e)
		{
			log.error("Error while fetching additional documents", e);
		}
		return res;

	}

	private TransactionData getTransactionTypeData(String transxDesc, POSDataModel posData, ClientDetails clientDetails) throws Exception
	{
		String type = transxDesc != null ? transxDesc.toUpperCase() : "";
		log.info("Will get data for type of transaction: " + type);
		switch (type)
		{
		// XXX only 14 first transactions to be completed for first phase
		case "AUTO REINSTATEMENT":
			return getAutoResinstatement(posData, clientDetails);
		case "CHANGE FREQUENCY":
			return getChangeOfPaymentFrequency(posData, clientDetails);
		case "CHANGE PAYMENT METHOD":
			return getChangeOfPaymentMethod(posData, clientDetails);
		case "PREMIUM HOLIDAY ACTIVATION":
			return getActivatedPremiumHoliday(posData, clientDetails);
		case "CHANGE ADDRESS":
			return getAddressDetails(posData, clientDetails);
		case "CHANGE PHONE NUMBER":
			return getChangePhoneNumberDetails(posData, clientDetails);
		case "CHANGE EMAIL ADDRESS":
			return getChangeEmailDetails(posData, clientDetails);
		case "CHANGE ID (KTP)":
			return getChangeIdDetails(posData, clientDetails);
		case "CHANGE / REVISE NAME":
		case "CHANGE/REVISE NAME":
			return getChangeNameDetails(posData, clientDetails);
		case "DUPLICATE POLICY":
			return getPolicyDuplicateData(posData, clientDetails);
		case "REPRINT HS":
			return getReprintHSdata(posData, clientDetails);
		case "FUND ALLOCATION":
			return getFundAllocationData(posData, clientDetails);

		// The transaction below could be addressed in second Phase
		case "PREMIUM HOLIDAY REINSTATEMENT":
			return getDeactivatedPremiumHoliday(posData, clientDetails);
		case "SWITCHING":
			return getFundSwitchingData(posData, clientDetails);
		case "TOP UP SINGLE":
			return getSingleTopUpData(posData, clientDetails);
		case "CHANGE REGULAR TOP UP":
		case "ADD REGULAR TOP UP":
			return getRegularTopUpData(posData, clientDetails);
		case "CHANGE BENEFICARY":
			return getChangeBenificaryDate(posData, clientDetails);
		// How the JSON is returned
		case "REINSTATEMENT":
			return getResinstatement(posData, clientDetails);

		// Needs small verification
		case "WITHDRAWAL":
			return getWithDrawalData(posData, clientDetails);

		// The following Transaction will not have data returned from back end
		case "Generaral 14 Transaction UI":
			return null;
		case "BANKER'S CLAUSE":
			return null;
		case "PAYMENT CREATOR":
			return null;
		case "DELETERIDER":
			return null;

		default:
			throw new DAOException("TRANSACTION Description not found: " + type);
		}
	}

	private TransactionData getRegularTopUpData(POSDataModel posData, ClientDetails clientDetails)
	{
		List<RegularTopUp> regularTopUpList = new ArrayList<>();
		String policyNumber = posData.getPolicyNumber();
		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posRegularTopup");
		try
		{
			List<FundAllocationDB> fundAllocationDBList = dao.getFundAllocation(policyNumber);
			for (FundAllocationDB fundAllocationDB : fundAllocationDBList)
			{
				RegularTopUp regularTopup = new RegularTopUp();
				regularTopup.setPolicyNumber(policyNumber);
				regularTopup.setModifiedRegularTopUpAmount(parseJSON.get("dblNewTopupAmount") != null ? parseJSON.get("dblNewTopupAmount").toString() : "");
				regularTopup.setNewPolicyPremium(parseJSON.get("dblNewPremiumAmount") != null ? parseJSON.get("dblNewPremiumAmount").toString() : "");
				regularTopup.setPayorDifferentWithOwnerIndicator(parseJSON.get("strPayerOwnerIndicator") != null ? parseJSON.get("strPayerOwnerIndicator").toString() : "");
				regularTopup.setCurrentPolicyPremium(dao.getPolicyDetails(policyNumber).getPREMIUMTOTAL());
				regularTopup.setFundName(fundAllocationDB.getFundCode());
				regularTopup.setPercentage(fundAllocationDB.getAllocationPercentage());
				regularTopUpList.add(regularTopup);
			}
		}
		catch (Exception e)
		{
			log.error("Error while get Regular top data:", e);
		}
		return new TransactionDataWrapper().setListData(regularTopUpList);
	}

	private TransactionData getWithDrawalData(POSDataModel posData, ClientDetails clientDetails)
	{
		List<PartialWithdrawal> partialWithdrawlList = new ArrayList<>();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posPartialWithdrawal");
			JSONArray parsedArray = parseJSON.getJSONArray("posPWDetails");
			for (int i = 0; i < parsedArray.length(); i++)
			{
				JSONObject data = parsedArray.getJSONObject(i);
				PartialWithdrawal partialWithdrawal = new PartialWithdrawal();
				partialWithdrawal.setBankName(parseJSON.get("strBankName") != null ? parseJSON.get("strBankName").toString() : "");
				partialWithdrawal.setBankAccountName(parseJSON.get("strAccountName") != null ? parseJSON.get("strAccountName").toString() : "");
				partialWithdrawal.setBankAccountNumber(parseJSON.get("strAccountNumber") != null ? parseJSON.get("strAccountNumber").toString() : "");
				partialWithdrawal.setCurrency(parseJSON.get("strCurrency") != null ? parseJSON.get("strCurrency").toString() : "");
				partialWithdrawal.setAmountToWithdraw(parseJSON.get("dblFundAmount") != null ? parseJSON.get("strCurrency").toString() : "");
				partialWithdrawal.setFundName(data.get("strFundName") != null ? data.get("strFundName").toString() : "");
				partialWithdrawal.setComponentName(data.get("strFundComponentName") != null ? data.get("strFundComponentName").toString() : "");
				partialWithdrawal.setUnitBalance(data.get("dblFundUnitBalance") != null ? data.get("dblFundUnitBalance").toString() : "");
				partialWithdrawal.setUnitPrice(data.get("dtFundPriceEffectiveDate") != null ? data.get("dtFundPriceEffectiveDate").toString() : "");
				partialWithdrawal.setEstimateFundValue(data.get("dblFundEstimatedAmount") != null ? data.get("dblFundEstimatedAmount").toString() : "");
				partialWithdrawal.setPercentageToWithdraw(data.get("dblFundWithdrawalPercent") != null ? data.get("dblFundWithdrawalPercent").toString() : "");
				partialWithdrawlList.add(partialWithdrawal);
			}
		}
		catch (Exception e)
		{
			log.error("Error getting Single top update: ", e);
		}
		return new TransactionDataWrapper().setListData(partialWithdrawlList);
	}

	private TransactionData getSingleTopUpData(POSDataModel posData, ClientDetails clientDetails) throws Exception
	{
		List<SingleTopUp> singleTopUpList = new ArrayList<SingleTopUp>();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posFundSingleTopup");
			JSONArray parsedArray = parseJSON.getJSONArray("posFSTDetails");
			for (int i = 0; i < parsedArray.length(); i++)
			{
				JSONObject data = parsedArray.getJSONObject(i);
				SingleTopUp singleTopUp = new SingleTopUp();
				singleTopUp.setPolicyNumber(posData.getPolicyNumber());
				singleTopUp.setAmountToBeTopUp(parseJSON.get("dblTopupAmount") != null ? parseJSON.get("dblTopupAmount").toString() : "");
				singleTopUp.setAmount(data.get("dblFundAmount") != null ? data.get("dblFundAmount").toString() : "");
				singleTopUp.setFundName(data.get("strFundName") != null ? data.get("strFundName").toString() : "");
				singleTopUp.setPercentage(data.get("dblFundPercentage") != null ? data.get("dblFundPercentage").toString() : "");
				singleTopUpList.add(singleTopUp);
			}
		}
		catch (Exception e)
		{
			log.error("Error getting Single top update: ", e);
		}
		return new TransactionDataWrapper().setListData(singleTopUpList);
	}

	private TransactionData getResinstatement(POSDataModel posData, ClientDetails clientDetails) throws Exception
	{
		List<Reinstatement> reinsatementList = new ArrayList<Reinstatement>();
		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posReinstatePolicyGT90");
		JSONArray parsedArray = parseJSON.getJSONArray("posInsuredDataDetails");
		JSONArray parsedArrayHQDetails = parseJSON.getJSONArray("posHQDetails");
		for (int i = 0; i < parsedArray.length(); i++)
		{
			JSONObject data = parsedArray.getJSONObject(i);
			JSONObject hqData = parsedArrayHQDetails.getJSONObject(0);
			JSONArray hqDataArray = hqData.getJSONArray("posHQClientDetails");
			JSONObject dataHQClientDetails = hqDataArray.getJSONObject(i);
			Reinstatement reinstatement = new Reinstatement();

			// posHQClientDetails
			HQClientDetails hqClientDetails = new HQClientDetails();

			hqClientDetails.setClientNumber(dataHQClientDetails.get("strHQClientNumber") != null ? dataHQClientDetails.get("strHQClientNumber").toString() : "");
			JSONArray HQDetailsArray = dataHQClientDetails.getJSONArray("strHQDetails");

			// strHQDetails
			List<HQDetails> hqDetailList = new ArrayList<>();
			for (int j = 0; j < HQDetailsArray.length(); j++)
			{
				JSONObject haData = HQDetailsArray.getJSONObject(i);
				HQDetails hqDetails = new HQDetails();
				hqDetails.setHQQuestion(haData.get("strHQQuestion") != null ? haData.get("strHQQuestion").toString() : "");
				hqDetails.setHQLifeResponse(haData.get("strHQLifeResponse") != null ? haData.get("strHQLifeResponse").toString() : "");
				hqDetailList.add(hqDetails);
			}

			hqClientDetails.setHqDetails(hqDetailList);

			reinstatement.setPolicyNumber(posData.getPolicyNumber());
			reinstatement.setOutStandingPremiumAmount(parseJSON.get("dblOutStandingPremAmount") != null ? parseJSON.get("dblOutStandingPremAmount").toString() : "");
			reinstatement.setInsuredName(data.get("strInsuredName") != null ? data.get("strInsuredName").toString() : "");
			reinstatement.setInsuredOccupation(data.get("strInsuredOccupation") != null ? data.get("strInsuredOccupation").toString() : "");
			reinstatement.setInsuredHeight(data.get("dblInsuredHeight") != null ? data.get("dblInsuredHeight").toString() : "");
			reinstatement.setInsuredWeight(data.get("dblInsuredWeight") != null ? data.get("dblInsuredWeight").toString() : "");
			reinstatement.setClientDetailsList(hqClientDetails);
			reinsatementList.add(reinstatement);
		}
		return new TransactionDataWrapper().setListData(reinsatementList);
	}

	private TransactionData getFundSwitchingData(POSDataModel posData, ClientDetails clientDetails) throws Exception
	{
		List<FundSwitch> oldFundList = new ArrayList<>();
		List<FundSwitch> newFundList = new ArrayList<>();
		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posFundSwitch");
		JSONArray sourceParsedArray = parseJSON.getJSONArray("posSourceFSDetails");
		JSONArray targetParsedArray = parseJSON.getJSONArray("posTargetFSDetails");
		for (int i = 0; i < sourceParsedArray.length(); i++)
		{
			JSONObject sourceData = sourceParsedArray.getJSONObject(i);
			FundSwitch oldFundSwitch = new FundSwitch();
			oldFundSwitch.setFundCode(sourceData.get("strSourceFundCode") != null ? sourceData.get("strSourceFundCode").toString() : "");
			oldFundSwitch.setFundName(sourceData.get("strSourceFundName") != null ? sourceData.get("strSourceFundName").toString() : "");
			oldFundSwitch.setPercentageToSwitch(sourceData.get("strSourceSwitchPercentage") != null ? sourceData.get("strSourceSwitchPercentage").toString() : "");
			oldFundSwitch.setEstimatedValueToSwitchOut(sourceData.get("strSourceEstimatedSwitchAmount") != null ? sourceData.get("strSourceEstimatedSwitchAmount").toString() : "");
			oldFundList.add(oldFundSwitch);

			JSONObject targetData = targetParsedArray.getJSONObject(i);
			FundSwitch newFundSwitch = new FundSwitch();
			newFundSwitch.setFundCode(targetData.get("strTargetFundCode") != null ? targetData.get("strTargetFundCode").toString() : "");
			newFundSwitch.setFundName(targetData.get("strTargetFundName") != null ? targetData.get("strTargetFundName").toString() : "");
			newFundSwitch.setPercentageToSwitch(targetData.get("strTargetSwitchPercentage") != null ? targetData.get("strTargetSwitchPercentage").toString() : "");
			newFundSwitch.setEstimatedValueToSwitchOut(targetData.get("strTargetEstimatedSwitchAmount") != null ? targetData.get("strTargetEstimatedSwitchAmount").toString() : "");
			newFundList.add(newFundSwitch);
		}
		return new TransactionDataWrapper().setOldList(oldFundList).setNewList(newFundList);
	}

	private TransactionData getChangeBenificaryDate(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		List<ChangeOfBenificary> oldChangeBenificaryList = new ArrayList<ChangeOfBenificary>();
		List<ChangeOfBenificary> newChangeBenificaryList = new ArrayList<ChangeOfBenificary>();
		List<BENEFICIARYLIST> benificaryList = dao.getBenificaryList(posData.getPolicyNumber());

		benificaryList.forEach(benificary -> {
			ChangeOfBenificary oldBenificary = new ChangeOfBenificary();
			oldBenificary.setClientNumber(benificary.getCLIENTNO());
			oldBenificary.setClientName(benificary.getCLIENTNAME());
			oldBenificary.setRelationship(benificary.getRELATIONSHIP());
			oldBenificary.setPercentage(benificary.getPERCENTAGE());
			oldChangeBenificaryList.add(oldBenificary);
		});

		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangeBeneficiary");
		JSONArray parsedArray = parseJSON.getJSONArray("posCBDetails");
		for (int i = 0; i < parsedArray.length(); i++)
		{
			JSONObject data = parsedArray.getJSONObject(i);
			ChangeOfBenificary newBenificary = new ChangeOfBenificary();
			newBenificary.setClientNumber(data.get("strChangeBeneficiaryClientNumber") != null ? data.get("strChangeBeneficiaryClientNumber").toString() : "");
			newBenificary.setClientName(data.get("strChangeBeneficiaryFName") != null ? data.get("strChangeBeneficiaryFName").toString() : "");
			newBenificary.setRelationship(data.get("strChangeBeneficiaryRelationship") != null ? data.get("strChangeBeneficiaryRelationship").toString() : "");
			newBenificary.setPercentage(data.get("strChangeBeneficiaryPercentage") != null ? data.get("strChangeBeneficiaryPercentage").toString() : "");
			newBenificary.setDob(data.get("dtChangeBeneficiaryDOB") != null ? data.get("dtChangeBeneficiaryDOB").toString() : "");
			newBenificary.setNik_ktp_kitas(data.get("strChangeBeneficiaryID") != null ? data.get("strChangeBeneficiaryID").toString() : "");
			newChangeBenificaryList.add(newBenificary);
		}
		return new TransactionDataWrapper().setOldList(oldChangeBenificaryList).setNewList(newChangeBenificaryList);
	}

	private TransactionData getChangeNameDetails(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		PersonalInfo clientPersonalInfo = dao.getClientPersonalInfo(clientDetails.getCLIENTNO());
		ChangeName oldName = new ChangeName();
		ChangeName newName = new ChangeName();
		oldName.setClientNumber(clientDetails.getCLIENTNO());
		List<String> role = dao.getPolicyRoles(clientDetails.getCLIENTNO());
		oldName.setCustomerRole(role.get(0));
		oldName.setGivename(clientPersonalInfo.getFirstName());
		oldName.setMiddleName(clientPersonalInfo.getMiddleName());
		oldName.setSurname(clientPersonalInfo.getSurname());
		try
		{
			newName.setClientNumber(clientDetails.getCLIENTNO());
			newName.setCustomerRole(role.get(0));
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangeName");
			newName.setGivename(parseJSON.get("strNewClientNameFirst") != null ? parseJSON.get("strNewClientNameFirst").toString() : "");
			newName.setMiddleName(parseJSON.get("strNewClientNameMiddle") != null ? parseJSON.get("strNewClientNameMiddle").toString() : "");
			newName.setSurname(parseJSON.get("strNewClientNameLast") != null ? parseJSON.get("strNewClientNameLast").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}

		return new TransactionDataWrapper().setOldData(oldName).setNewData(newName);
	}

	private TransactionData getChangeEmailDetails(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		PersonalInfo clientPersonalInfo = dao.getClientPersonalInfo(clientDetails.getCLIENTNO());
		ChangeEmail changeEmail = new ChangeEmail();
		changeEmail.setGiveName(clientPersonalInfo.getFirstName());
		changeEmail.setMiddleName(clientPersonalInfo.getMiddleName());
		changeEmail.setSurname(clientPersonalInfo.getSurname());
		changeEmail.setClientNumber(clientDetails.getCLIENTNO());

		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangeEAddress");
			changeEmail.setNewEmailAddress(parseJSON.get("strEmailAddress") != null ? parseJSON.get("strEmailAddress").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return changeEmail;
	}

	private TransactionData getChangeIdDetails(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		PersonalInfo clientPersonalInfo = dao.getClientPersonalInfo(clientDetails.getCLIENTNO());
		ChangeId oldData = new ChangeId();
		oldData.setGiveName(clientPersonalInfo.getFirstName());
		oldData.setMiddleName(clientPersonalInfo.getMiddleName());
		oldData.setSurname(clientPersonalInfo.getSurname());
		oldData.setClientNumber(clientDetails.getCLIENTNO());
		oldData.setIdNumber(clientPersonalInfo.getIdNumber());
		oldData.setIdType(clientPersonalInfo.getIdType());

		ChangeId newData = new ChangeId();
		newData.setGiveName(clientPersonalInfo.getFirstName());
		newData.setMiddleName(clientPersonalInfo.getMiddleName());
		newData.setSurname(clientPersonalInfo.getSurname());
		newData.setClientNumber(clientDetails.getCLIENTNO());
		log.info("tData : " + posData.getTransactionData());
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangeID");
			newData.setNewIdType(parseJSON.get("strNewIDType") != null ? parseJSON.get("strNewIDType").toString() : "");
			newData.setNewIdNumber(parseJSON.get("strNewIDNumber") != null ? parseJSON.get("strNewIDNumber").toString() : "");
			newData.setIdValidity(parseJSON.get("strNewIDValidity") != null ? parseJSON.get("strNewIDValidity").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return new TransactionDataWrapper().setOldData(oldData).setNewData(newData);
	}

	private JSONObject getTransactionDataFromJson(String data)
	{
		return new JSONObject(data).getJSONArray("WSInput").getJSONObject(0);
	}

	private TransactionData getChangePhoneNumberDetails(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		PersonalInfo clientPersonalInfo = dao.getClientPersonalInfo(clientDetails.getCLIENTNO());

		ChangePhoneNumber newPhoneNumber = new ChangePhoneNumber();
		newPhoneNumber.setClientNumber(clientDetails.getCLIENTNO());
		newPhoneNumber.setGivenname(clientPersonalInfo.getFirstName());
		newPhoneNumber.setMiddleName(clientPersonalInfo.getMiddleName());
		newPhoneNumber.setSurname(clientPersonalInfo.getSurname());
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangePhone");
			newPhoneNumber.setMobileNo(parseJSON.get("strPhoneNumberMobile") != null ? parseJSON.get("strPhoneNumberMobile").toString() : "");
			newPhoneNumber.setOfficeNo(parseJSON.get("strPhoneNumberOffice") != null ? parseJSON.get("strPhoneNumberOffice").toString() : "");
			newPhoneNumber.setHomeNo(parseJSON.get("strPhoneNumberHome") != null ? parseJSON.get("strPhoneNumberHome").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return newPhoneNumber;
	}

	private TransactionData getActivatedPremiumHoliday(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		ActivatedPremiumHoliday actPremiumHoliday = new ActivatedPremiumHoliday();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posAPH");
			JSONArray parsedArray = parseJSON.getJSONArray("strAPHDetails");
			for (int i = 0; i < parsedArray.length(); i++)
			{
				JSONObject data = parsedArray.getJSONObject(i);
				actPremiumHoliday.setPaymentFrequency(data.get("strPolicyPaymentFrequency") != null ? data.get("strPolicyPaymentFrequency").toString() : "");
				actPremiumHoliday.setPolicyNumber(posData.getPolicyNumber());
				actPremiumHoliday.setPremiumAmount(data.get("iPolicyPremiumAmount") != null ? data.get("iPolicyPremiumAmount").toString() : "");
				actPremiumHoliday.setPaidToDate(data.get("dtPaidToDate") != null ? data.get("dtPaidToDate").toString() : "");
			}
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}

		return actPremiumHoliday;
	}

	private TransactionData getDeactivatedPremiumHoliday(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		PolicyInfo policyInfo = dao.getPolicyInfo(posData.getPolicyNumber());
		ActivatedPremiumHoliday deActivatedPremiumHoliday = new ActivatedPremiumHoliday();
		deActivatedPremiumHoliday.setPaidToDate(DateUtil.formatDate(policyInfo.getPaidToDate()));
		deActivatedPremiumHoliday.setPaymentFrequency(policyInfo.getPaymentFrequency());
		deActivatedPremiumHoliday.setPolicyNumber(posData.getPolicyNumber());
		deActivatedPremiumHoliday.setPremiumAmount(policyInfo.getPremiumTotal());
		return deActivatedPremiumHoliday;
	}

	private TransactionData getAutoResinstatement(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		AutoReinstatement autoReinsatement = new AutoReinstatement();
		autoReinsatement.setPolicyNumber(posData.getPolicyNumber());
		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posReinstatePolicyLT90");
		autoReinsatement.setOutsTandingPremiumAmount(parseJSON.get("dblOutStandingPremAmount") != null ? parseJSON.get("dblOutStandingPremAmount").toString() : "");
		return autoReinsatement;
	}

	private TransactionData getChangeOfPaymentFrequency(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		ChangePaymentFrequency changePaymentFrequency = new ChangePaymentFrequency();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posPaymentFrequency");
			changePaymentFrequency.setCurrentPaymentFrequency(parseJSON.get("strPaymentMethodCurrent") != null ? parseJSON.get("strPaymentMethodCurrent").toString() : "");
			changePaymentFrequency.setNewPaymentFrequency(parseJSON.get("strPaymentFrequencyNew") != null ? parseJSON.get("strPaymentFrequencyNew").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return changePaymentFrequency;
	}

	private TransactionData getChangeOfPaymentMethod(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{

		PolicyInfo policyInfo = dao.getPolicyInfo(posData.getPolicyNumber());
		ChangePaymentMethod changePaymentMethod = new ChangePaymentMethod();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangePaymentMethod");
			changePaymentMethod.setCurrentPaymentMethod(policyInfo.getCurrentPaymentMethod());
			changePaymentMethod.setBankAccountHolderName(parseJSON.get("strBankAccountHolderName") != null ? parseJSON.get("strBankAccountHolderName").toString() : "");
			changePaymentMethod.setBankAccountNumber(parseJSON.get("strBankAccountNumber") != null ? parseJSON.get("strBankAccountNumber").toString() : "");
			changePaymentMethod.setBankName(parseJSON.get("strBankName") != null ? parseJSON.get("strBankName").toString() : "");
			changePaymentMethod.setCreditCardHolderName(parseJSON.get("strCreditCardHolderName") != null ? parseJSON.get("strCreditCardHolderName").toString() : "");
			changePaymentMethod.setCreditCardNumber(parseJSON.get("strCreditCardNumber") != null ? parseJSON.get("strCreditCardNumber").toString() : "");
			changePaymentMethod.setExpiredDate(parseJSON.get("dtCreditCardExpiredDate") != null ? parseJSON.get("dtCreditCardExpiredDate").toString() : "");
			changePaymentMethod.setNewPaymentMethod(parseJSON.get("strNewPaymentMethod") != null ? parseJSON.get("strNewPaymentMethod").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return changePaymentMethod;
	}

	private TransactionData getPolicyDuplicateData(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{
		PolicyDuplicate policyDuplicate = new PolicyDuplicate();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posPolicyDuplicate");
			policyDuplicate.setPolicyNumber(posData.getPolicyNumber());
			policyDuplicate.setReason(parseJSON.get("strReason") != null ? parseJSON.get("strReason").toString() : "");
			policyDuplicate.setDispatchAddress(parseJSON.get("strDispatchAddress") != null ? parseJSON.get("strDispatchAddress").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return policyDuplicate;
	}

	private TransactionData getPaymentCreatorData(POSDataModel posData, ClientDetails clientDetails) throws DAOException
	{

		PaymentCreator paymentCreator = new PaymentCreator();
		try
		{
			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posPolicyDuplicate");
			paymentCreator.setAmount(parseJSON.get("AMOUNT") != null ? parseJSON.get("AMOUNT").toString() : "");
			paymentCreator.setRecieptNumber(parseJSON.get("STRRECIEPTNUMBER") != null ? parseJSON.get("STRRECIEPTNUMBER").toString() : "");
			paymentCreator.setTransactionNumber(parseJSON.get("STRTRANSACTIONNUMBER") != null ? parseJSON.get("STRTRANSACTIONNUMBER").toString() : "");
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return paymentCreator;
	}

	private TransactionData getAddressDetails(POSDataModel posData, ClientDetails clientDetails)
	{
		ChangeAddress oldAddress = new ChangeAddress();
		ChangeAddress newAddress = new ChangeAddress();
		try
		{
			PersonalInfo clientPersonalInfo = dao.getClientPersonalInfo(clientDetails.getCLIENTNO());
			oldAddress.setClientNumber(clientDetails.getCLIENTNO());
			oldAddress.setGivenName(clientPersonalInfo.getFirstName());
			oldAddress.setMiddleName(clientPersonalInfo.getMiddleName());
			oldAddress.setLastName(clientPersonalInfo.getSurname());
			oldAddress.setAddress1(clientPersonalInfo.getAddress1());
			oldAddress.setAddress2(clientPersonalInfo.getAddress2());
			oldAddress.setAddress3(clientPersonalInfo.getAddress3());
			oldAddress.setAddress4(clientPersonalInfo.getAddress4());
			oldAddress.setAddress5(clientPersonalInfo.getAddress5());
			oldAddress.setZipcode(clientPersonalInfo.getZipcode());
			oldAddress.setCountry(clientPersonalInfo.getCountryCode());

			newAddress.setClientNumber(clientDetails.getCLIENTNO());
			newAddress.setGivenName(clientPersonalInfo.getFirstName());
			newAddress.setMiddleName(clientPersonalInfo.getMiddleName());
			newAddress.setLastName(clientPersonalInfo.getSurname());

			JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posChangeAddress");
			newAddress.setAddress1(parseJSON.get("strNewAddress1") != null ? parseJSON.get("strNewAddress1").toString() : "");
			newAddress.setAddress2(parseJSON.get("strNewAddress2") != null ? parseJSON.get("strNewAddress2").toString() : "");
			newAddress.setAddress3(parseJSON.get("strNewAddress3") != null ? parseJSON.get("strNewAddress3").toString() : "");
			newAddress.setAddress4(parseJSON.get("strNewAddress4") != null ? parseJSON.get("strNewAddress4").toString() : "");
			newAddress.setAddress5(parseJSON.get("strNewAddress5") != null ? parseJSON.get("strNewAddress5").toString() : "");
			newAddress.setZipcode(parseJSON.get("strNewPostalCode") != null ? parseJSON.get("strNewPostalCode").toString() : "");
			newAddress.setCountry(parseJSON.get("strNewCountry") != null ? parseJSON.get("strNewCountry").toString() : "");
			newAddress.setPoliciesImpacted(dao.getPolicyRoles(clientDetails.getCLIENTNO()));
		}
		catch (Exception e)
		{
			log.error("Unable to parse transactiondata.", e);
		}
		return new TransactionDataWrapper().setNewData(newAddress).setOldData(oldAddress);
	}

	private TransactionData getReprintHSdata(POSDataModel posData, ClientDetails clientDetails)
	{
		List<ReprintHS> reprintArray = new ArrayList<>();
		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posReprintHS");
		JSONArray parsedArray = parseJSON.getJSONArray("posRHSDetails");
		for (int i = 0; i < parsedArray.length(); i++)
		{
			JSONObject data = parsedArray.getJSONObject(i);
			ReprintHS reprintHs = new ReprintHS();
			reprintHs.setReason(data.get("strReason") != null ? data.get("strReason").toString() : "");
			reprintHs.setInsuredName(parseJSON.get("strInsuredName") != null ? parseJSON.get("strInsuredName").toString() : "");
			reprintHs.setFirstName(data.get("strLifeInsuredFName") != null ? data.get("strLifeInsuredFName").toString() : "");
			reprintHs.setMiddleName(data.get("strLifeInsuredMName") != null ? data.get("strLifeInsuredMName").toString() : "");
			reprintHs.setLastName(data.get("strLifeInsuredLName") != null ? data.get("strLifeInsuredLName").toString() : "");
			reprintHs.setComponentName(data.get("strLifeINsuredComponentNameHS") != null ? data.get("strLifeINsuredComponentNameHS").toString() : "");
			reprintHs.setPlanId(data.get("strLifeINsuredComponentPlanID") != null ? data.get("strLifeINsuredComponentPlanID").toString() : "");
			reprintHs.setComponenetStatus(data.get("strLifeINsuredComponentStatus") != null ? data.get("strLifeINsuredComponentStatus").toString() : "");
			reprintHs.setCompPermStatus(data.get("strLifeINsuredComponentPremiumStatus") != null ? data.get("strLifeINsuredComponentPremiumStatus").toString() : "");
			reprintArray.add(reprintHs);
		}
		return new TransactionDataWrapper().setListData(reprintArray);
	}

	private TransactionData getFundAllocationData(POSDataModel posData, ClientDetails clientDetails)
	{
		List<FundAllocation> oldFundList = new ArrayList<>();
		List<FundAllocation> newFundList = new ArrayList<>();
		JSONObject parseJSON = getTransactionDataFromJson(posData.getTransactionData()).getJSONObject("posFundAllocation");
		JSONArray sourceParsedArray = parseJSON.getJSONArray("posSourceFADetails");
		JSONArray targetParsedArray = parseJSON.getJSONArray("posTargetFADetails");
		for (int i = 0; i < sourceParsedArray.length(); i++)
		{
			JSONObject sourceData = sourceParsedArray.getJSONObject(i);
			FundAllocation oldFundAllocation = new FundAllocation();
			oldFundAllocation.setFundCode(sourceData.get("strSourceFundCode") != null ? sourceData.get("strSourceFundCode").toString() : "");
			oldFundAllocation.setFundName(sourceData.get("strSourceFundName") != null ? sourceData.get("strSourceFundName").toString() : "");
			oldFundAllocation.setPercentageToSwitch(sourceData.get("strSourceSwitchPercentage") != null ? sourceData.get("strSourceSwitchPercentage").toString() : "");
			oldFundList.add(oldFundAllocation);

			JSONObject tragetData = targetParsedArray.getJSONObject(i);
			FundAllocation newFundAllocation = new FundAllocation();
			newFundAllocation.setFundCode(tragetData.get("strTargetFundCode") != null ? tragetData.get("strTargetFundCode").toString() : "");
			newFundAllocation.setFundName(tragetData.get("strTargetFundName") != null ? tragetData.get("strTargetFundName").toString() : "");
			newFundAllocation.setPercentageToSwitch(tragetData.get("strTargetSwitchPercentage") != null ? tragetData.get("strTargetSwitchPercentage").toString() : "");
			newFundList.add(newFundAllocation);
		}
		return new TransactionDataWrapper().setOldList(oldFundList).setNewList(newFundList);
	}
}
