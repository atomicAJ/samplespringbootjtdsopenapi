package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ChangeEmail implements TransactionData
{

	private String clientNumber;

	private String givenName;
	private String middleName;
	private String surName;
	private String newEmailAddress;

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getGiveName()
	{
		return givenName;
	}

	public void setGiveName(String giveName)
	{
		this.givenName = giveName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getSurname()
	{
		return surName;
	}

	public void setSurname(String surname)
	{
		this.surName = surname;
	}

	public String getNewEmailAddress()
	{
		return newEmailAddress;
	}

	public void setNewEmailAddress(String newEmailAddress)
	{
		this.newEmailAddress = newEmailAddress;
	}
}
