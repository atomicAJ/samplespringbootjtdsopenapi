package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PolicyDuplicate implements TransactionData
{
	private String policyNumber;

	private String reason;

	private String dispatchAddress;

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public String getDispatchAddress()
	{
		return dispatchAddress;
	}

	public void setDispatchAddress(String dispatchAddress)
	{
		this.dispatchAddress = dispatchAddress;
	}

}
