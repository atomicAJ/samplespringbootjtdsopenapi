package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ChangeOfBenificary implements TransactionData
{
	String clientNumber;

	String clientName;

	String relationship;

	String percentage;

	String dob;

	String nik_ktp_kitas;

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getClientName()
	{
		return clientName;
	}

	public void setClientName(String clientName)
	{
		this.clientName = clientName;
	}

	public String getRelationship()
	{
		return relationship;
	}

	public void setRelationship(String relationship)
	{
		this.relationship = relationship;
	}

	public String getPercentage()
	{
		return percentage;
	}

	public void setPercentage(String percentage)
	{
		this.percentage = percentage;
	}

	public String getDob()
	{
		return dob;
	}

	public void setDob(String dob)
	{
		this.dob = dob;
	}

	public String getNik_ktp_kitas()
	{
		return nik_ktp_kitas;
	}

	public void setNik_ktp_kitas(String nik_ktp_kitas)
	{
		this.nik_ktp_kitas = nik_ktp_kitas;
	}

	@Override
	public String toString()
	{
		return "ChangeOfBenificary [clientNumber=" + clientNumber + ", clientName=" + clientName + ", relationship=" + relationship + ", percentage=" + percentage + ", dob=" + dob + ", nik_ktp_kitas=" + nik_ktp_kitas + "]";
	}
}
