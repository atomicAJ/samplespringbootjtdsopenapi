package com.candelalabs.epos.model;

public class SingleTopUp implements TransactionData
{
	private String amountToBeTopUp;

	private String policyNumber;

	private String fundName;

	private String Amount;

	private String percentage;

	public String getAmountToBeTopUp()
	{
		return amountToBeTopUp;
	}

	public void setAmountToBeTopUp(String amountToBeTopUp)
	{
		this.amountToBeTopUp = amountToBeTopUp;
	}

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getFundName()
	{
		return fundName;
	}

	public void setFundName(String fundName)
	{
		this.fundName = fundName;
	}

	public String getAmount()
	{
		return Amount;
	}

	public void setAmount(String amount)
	{
		Amount = amount;
	}

	public String getPercentage()
	{
		return percentage;
	}

	public void setPercentage(String percentage)
	{
		this.percentage = percentage;
	}

}
