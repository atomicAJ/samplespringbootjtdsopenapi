package com.candelalabs.epos.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
public class POSTransactionDetails
{

	private List<Attribute> transactionFields = new ArrayList<>();
	private List<TransactionHistory> transactionHistory;
	private List<AdditionalDocument> additionalDocuments;
	private TransactionData transactionTypeData;

	public List<Attribute> getTransactionFields()
	{
		return transactionFields;
	}

	public POSTransactionDetails setTransactionFields(List<Attribute> transactionFields)
	{
		this.transactionFields = transactionFields;
		return this;
	}

	
	public List<TransactionHistory> getTransactionHistory() {
		return transactionHistory;
	}

	public void setTransactionHistory(List<TransactionHistory> transactionHistory) {
		this.transactionHistory = transactionHistory;
	}

	public List getAdditionalDocuments()
	{
		return additionalDocuments;
	}

	public POSTransactionDetails setAdditionalDocuments(List additionalDocuments)
	{
		this.additionalDocuments = additionalDocuments;
		return this;
	}

	public TransactionData getTransactionTypeData()
	{
		return transactionTypeData;
	}

	public void setTransactionTypeData(TransactionData transactionTypeData)
	{
		this.transactionTypeData = transactionTypeData;
	}

}
