package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ChangePaymentFrequency implements TransactionData
{

	private String currentPaymentFrequency;
	private String newPaymentFrequency;

	public String getCurrentPaymentFrequency()
	{
		return currentPaymentFrequency;
	}

	public void setCurrentPaymentFrequency(String currentPaymentFrequency)
	{
		this.currentPaymentFrequency = currentPaymentFrequency;
	}

	public String getNewPaymentFrequency()
	{
		return newPaymentFrequency;
	}

	public void setNewPaymentFrequency(String newPaymentFrequency)
	{
		this.newPaymentFrequency = newPaymentFrequency;
	}
}
