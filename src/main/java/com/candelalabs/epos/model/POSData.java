package com.candelalabs.epos.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class POSData
{
	private String posRequestNumber;

	private String portalRequestNumber;

	private String scanRequestNumber;

	private String policyNumber;

	private String transactionId;

	private String transactionDescription;

	private String transactionCategory;

	private String transactionSource;

	private Integer processInitiatedFlag;

	private String posDecision;

	private Date recordCreationDateTime;

	private Date unitDealingSubmissionDate;

	private String duplicateFlag;

	private String psUser;

	private String qcUser;

	private String transactionData;

	private String psPayoutCreator;

	private String psPayoutApprover;

	private String posStatus;

	public String getPosRequestNumber()
	{
		return posRequestNumber;
	}

	public void setPosRequestNumber(String posRequestNumber)
	{
		this.posRequestNumber = posRequestNumber;
	}

	public String getPortalRequestNumber()
	{
		return portalRequestNumber;
	}

	public void setPortalRequestNumber(String portalRequestNumber)
	{
		this.portalRequestNumber = portalRequestNumber;
	}

	public String getScanRequestNumber()
	{
		return scanRequestNumber;
	}

	public void setScanRequestNumber(String scanRequestNumber)
	{
		this.scanRequestNumber = scanRequestNumber;
	}

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getTransactionDescription()
	{
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription)
	{
		this.transactionDescription = transactionDescription;
	}

	public String getTransactionCategory()
	{
		return transactionCategory;
	}

	public void setTransactionCategory(String transactionCategory)
	{
		this.transactionCategory = transactionCategory;
	}

	public String getTransactionSource()
	{
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource)
	{
		this.transactionSource = transactionSource;
	}

	public Integer getProcessInitiatedFlag()
	{
		return processInitiatedFlag;
	}

	public void setProcessInitiatedFlag(Integer processInitiatedFlag)
	{
		this.processInitiatedFlag = processInitiatedFlag;
	}

	public String getPosDecision()
	{
		return posDecision;
	}

	public void setPosDecision(String posDecision)
	{
		this.posDecision = posDecision;
	}

	public Date getRecordCreationDateTime()
	{
		return recordCreationDateTime;
	}

	public void setRecordCreationDateTime(Date recordCreationDateTime)
	{
		this.recordCreationDateTime = recordCreationDateTime;
	}

	public Date getUnitDealingSubmissionDate()
	{
		return unitDealingSubmissionDate;
	}

	public void setUnitDealingSubmissionDate(Date unitDealingSubmissionDate)
	{
		this.unitDealingSubmissionDate = unitDealingSubmissionDate;
	}

	public String getDuplicateFlag()
	{
		return duplicateFlag;
	}

	public void setDuplicateFlag(String duplicateFlag)
	{
		this.duplicateFlag = duplicateFlag;
	}

	public String getPsUser()
	{
		return psUser;
	}

	public void setPsUser(String psUser)
	{
		this.psUser = psUser;
	}

	public String getQcUser()
	{
		return qcUser;
	}

	public void setQcUser(String qcUser)
	{
		this.qcUser = qcUser;
	}

	public String getTransactionData()
	{
		return transactionData;
	}

	public void setTransactionData(String transactionData)
	{
		this.transactionData = transactionData;
	}

	public String getPsPayoutCreator()
	{
		return psPayoutCreator;
	}

	public void setPsPayoutCreator(String psPayoutCreator)
	{
		this.psPayoutCreator = psPayoutCreator;
	}

	public String getPsPayoutApprover()
	{
		return psPayoutApprover;
	}

	public void setPsPayoutApprover(String psPayoutApprover)
	{
		this.psPayoutApprover = psPayoutApprover;
	}

	public String getPosStatus()
	{
		return posStatus;
	}

	public void setPosStatus(String posStatus)
	{
		this.posStatus = posStatus;
	}
}
