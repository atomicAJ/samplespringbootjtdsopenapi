package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ChangePhoneNumber implements TransactionData
{

	private String clientNumber;
	private String givenName;
	private String middleName;
	private String surName;
	private String mobileNo;
	private String homeNo;
	private String officeNo;

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getGivenname()
	{
		return givenName;
	}

	public void setGivenname(String givenname)
	{
		this.givenName = givenname;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getSurname()
	{
		return surName;
	}

	public void setSurname(String surname)
	{
		this.surName = surname;
	}

	public String getMobileNo()
	{
		return mobileNo;
	}

	public void setMobileNo(String mobileNo)
	{
		this.mobileNo = mobileNo;
	}

	public String getHomeNo()
	{
		return homeNo;
	}

	public void setHomeNo(String homeNo)
	{
		this.homeNo = homeNo;
	}

	public String getOfficeNo()
	{
		return officeNo;
	}

	public void setOfficeNo(String officeNo)
	{
		this.officeNo = officeNo;
	}

}
