package com.candelalabs.epos.model;

public class FundAllocationDB
{

	private String policyNumber;

	private String fundCode;

	private String allocationPercentage;

	private String coverage;

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getFundCode()
	{
		return fundCode;
	}

	public void setFundCode(String fundCode)
	{
		this.fundCode = fundCode;
	}

	public String getAllocationPercentage()
	{
		return allocationPercentage;
	}

	public void setAllocationPercentage(String allocationPercentage)
	{
		this.allocationPercentage = allocationPercentage;
	}

	public String getCoverage()
	{
		return coverage;
	}

	public void setCoverage(String coverage)
	{
		this.coverage = coverage;
	}

}
