package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ChangeId implements TransactionData
{

	private String clientNumber;
	private String givenName;
	private String middleName;
	private String surName;
	private String idType;
	private String idNumber;
	private String newIdType;
	private String newIdNumber;
	private String idValidity;

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getGiveName()
	{
		return givenName;
	}

	public void setGiveName(String giveName)
	{
		this.givenName = giveName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getSurname()
	{
		return surName;
	}

	public void setSurname(String surname)
	{
		this.surName = surname;
	}

	public String getIdType()
	{
		return idType;
	}

	public void setIdType(String idType)
	{
		this.idType = idType;
	}

	public String getIdNumber()
	{
		return idNumber;
	}

	public void setIdNumber(String idNumber)
	{
		this.idNumber = idNumber;
	}

	public String getNewIdType()
	{
		return newIdType;
	}

	public void setNewIdType(String newIdType)
	{
		this.newIdType = newIdType;
	}

	public String getNewIdNumber()
	{
		return newIdNumber;
	}

	public void setNewIdNumber(String newIdNumber)
	{
		this.newIdNumber = newIdNumber;
	}

	public String getIdValidity()
	{
		return idValidity;
	}

	public void setIdValidity(String idValidity)
	{
		this.idValidity = idValidity;
	}

	@Override
	public String toString()
	{
		return "ChangeId [clientNumber=" + clientNumber + ", giveName=" + givenName + ", middleName=" + middleName + ", surname=" + surName + ", idType=" + idType + ", idNumber=" + idNumber + ", newIdType=" + newIdType + ", newIdNumber=" + newIdNumber + ", idValidity=" + idValidity + "]";
	}

}
