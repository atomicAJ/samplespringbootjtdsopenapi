package com.candelalabs.epos.model;

import java.util.List;

public class HQClientDetails
{
	private String clientNumber;

	private List<HQDetails> hqDetails;

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public List<HQDetails> getHqDetails()
	{
		return hqDetails;
	}

	public void setHqDetails(List<HQDetails> hqDetails)
	{
		this.hqDetails = hqDetails;
	}

}
