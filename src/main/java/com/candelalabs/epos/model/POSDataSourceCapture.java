package com.candelalabs.epos.model;

import java.util.Date;

public class POSDataSourceCapture {
	private String policyNumber;
	private String caseReferenceNumber;
	private String transactionType;
	private String transactionSource;
	private Date recordCreationTS;
	private String exceptionRecord;
	private String processRecord;
	private Date recordProcessedTS;
	private String transactionData;
	private String processExceptionMessage;

	public POSDataSourceCapture() {
		super();
		// TODO Auto-generated constructor stub
	}

	public POSDataSourceCapture(String policyNumber, String caseReferenceNumber, String transactionType,
			String transactionSource, Date recordCreationTS, String exceptionRecord, String processRecord,
			Date recordProcessedTS, String transactionData, String processExceptionMessage) {
		super();
		this.policyNumber = policyNumber;
		this.caseReferenceNumber = caseReferenceNumber;
		this.transactionType = transactionType;
		this.transactionSource = transactionSource;
		this.recordCreationTS = recordCreationTS;
		this.exceptionRecord = exceptionRecord;
		this.processRecord = processRecord;
		this.recordProcessedTS = recordProcessedTS;
		this.transactionData = transactionData;
		this.processExceptionMessage = processExceptionMessage;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getCaseReferenceNumber() {
		return caseReferenceNumber;
	}

	public void setCaseReferenceNumber(String caseReferenceNumber) {
		this.caseReferenceNumber = caseReferenceNumber;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public Date getRecordCreationTS() {
		return recordCreationTS;
	}

	public void setRecordCreationTS(Date recordCreationTS) {
		this.recordCreationTS = recordCreationTS;
	}

	public String getExceptionRecord() {
		return exceptionRecord;
	}

	public void setExceptionRecord(String exceptionRecord) {
		this.exceptionRecord = exceptionRecord;
	}

	public String getProcessRecord() {
		return processRecord;
	}

	public void setProcessRecord(String processRecord) {
		this.processRecord = processRecord;
	}

	public Date getRecordProcessedTS() {
		return recordProcessedTS;
	}

	public void setRecordProcessedTS(Date recordProcessedTS) {
		this.recordProcessedTS = recordProcessedTS;
	}

	public String getTransactionData() {
		return transactionData;
	}

	public void setTransactionData(String transactionData) {
		this.transactionData = transactionData;
	}

	public String getProcessExceptionMessage() {
		return processExceptionMessage;
	}

	public void setProcessExceptionMessage(String processExceptionMessage) {
		this.processExceptionMessage = processExceptionMessage;
	}

	@Override
	public String toString() {
		return "POSDataSourceCapture [policyNumber=" + policyNumber + ", caseReferenceNumber=" + caseReferenceNumber
				+ ", transactionType=" + transactionType + ", transactionSource=" + transactionSource
				+ ", recordCreationTS=" + recordCreationTS + ", exceptionRecord=" + exceptionRecord + ", processRecord="
				+ processRecord + ", recordProcessedTS=" + recordProcessedTS + ", transactionData=" + transactionData
				+ ", processExceptionMessage=" + processExceptionMessage + "]";
	}

}
