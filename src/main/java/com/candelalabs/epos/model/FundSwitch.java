package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class FundSwitch implements TransactionData
{
	private String fundCode;

	private String fundName;

	private String percentageToSwitch;

	private String estimatedValueToSwitchOut;

	public String getEstimatedValueToSwitchOut()
	{
		return estimatedValueToSwitchOut;
	}

	public void setEstimatedValueToSwitchOut(String estimatedValueToSwitchOut)
	{
		this.estimatedValueToSwitchOut = estimatedValueToSwitchOut;
	}

	public String getFundCode()
	{
		return fundCode;
	}

	public void setFundCode(String fundCode)
	{
		this.fundCode = fundCode;
	}

	public String getFundName()
	{
		return fundName;
	}

	public void setFundName(String fundName)
	{
		this.fundName = fundName;
	}

	public String getPercentageToSwitch()
	{
		return percentageToSwitch;
	}

	public void setPercentageToSwitch(String percentageToSwitch)
	{
		this.percentageToSwitch = percentageToSwitch;
	}

}
