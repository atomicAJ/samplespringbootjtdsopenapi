package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AutoReinstatement implements TransactionData
{
	private String policyNumber;

	private String outStandingPremiumAmount;

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getOutsTandingPremiumAmount()
	{
		return outStandingPremiumAmount;
	}

	public void setOutsTandingPremiumAmount(String outsTandingPremiumAmount)
	{
		this.outStandingPremiumAmount = outsTandingPremiumAmount;
	}

}
