package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TransactionHistory
{
	private String posRequestNumber;
	private String typeOfTransaction;
	private String requestSubmissionDate;
	private String requestReceivedDate;
	private String channelSubmission;
	private String requestStatus;

	public String getPosRequestNumber()
	{
		return posRequestNumber;
	}

	public void setPosRequestNumber(String posRequestNumber)
	{
		this.posRequestNumber = posRequestNumber;
	}

	public String getTypeOfTransaction()
	{
		return typeOfTransaction;
	}

	public void setTypeOfTransaction(String typeOfTransaction)
	{
		this.typeOfTransaction = typeOfTransaction;
	}

	public String getRequestSubmissionDate()
	{
		return requestSubmissionDate;
	}

	public void setRequestSubmissionDate(String requestSubmissionDate)
	{
		this.requestSubmissionDate = requestSubmissionDate;
	}

	public String getRequestReceivedDate()
	{
		return requestReceivedDate;
	}

	public void setRequestReceivedDate(String requestReceivedDate)
	{
		this.requestReceivedDate = requestReceivedDate;
	}

	public String getChannelSubmission()
	{
		return channelSubmission;
	}

	public void setChannelSubmission(String channelSubmission)
	{
		this.channelSubmission = channelSubmission;
	}

	public String getRequestStatus()
	{
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus)
	{
		this.requestStatus = requestStatus;
	}

	@Override
	public String toString()
	{
		return "TransactionHistory [posRequestNumber=" + posRequestNumber + ", typeOfTransaction=" + typeOfTransaction + ", requestSubmissionDate=" + requestSubmissionDate + ", requestReceivedDate=" + requestReceivedDate + ", channelSubmission=" + channelSubmission + ", requestStatus=" + requestStatus + "]";
	}

}
