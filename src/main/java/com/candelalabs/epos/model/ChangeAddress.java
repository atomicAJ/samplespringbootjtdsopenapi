package com.candelalabs.epos.model;

import java.util.List;

public class ChangeAddress implements TransactionData
{
	private String clientNumber;
	private String givenName;
	private String middleName;
	private String lastName;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String address5;
	private String zipcode;
	private String country;
	private List<String> policiesImpacted;

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getAddress1()
	{
		return address1;
	}

	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}

	public String getAddress2()
	{
		return address2;
	}

	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}

	public String getAddress3()
	{
		return address3;
	}

	public void setAddress3(String address3)
	{
		this.address3 = address3;
	}

	public String getAddress4()
	{
		return address4;
	}

	public void setAddress4(String address4)
	{
		this.address4 = address4;
	}

	public String getAddress5()
	{
		return address5;
	}

	public void setAddress5(String address5)
	{
		this.address5 = address5;
	}

	public String getZipcode()
	{
		return zipcode;
	}

	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public List<String> getPoliciesImpacted()
	{
		return policiesImpacted;
	}

	public void setPoliciesImpacted(List<String> policiesImpacted)
	{
		this.policiesImpacted = policiesImpacted;
	}

}
