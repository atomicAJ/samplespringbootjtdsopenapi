package com.candelalabs.epos.model;

import java.math.BigDecimal;

public class RegularTopUp implements TransactionData
{
	private String policyNumber;

	private BigDecimal currentPolicyPremium;

	private String modifiedRegularTopUpAmount;

	private String newPolicyPremium;

	private String payorDifferentWithOwnerIndicator;

	private String fundName;

	private String percentage;

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public BigDecimal getCurrentPolicyPremium()
	{
		return currentPolicyPremium;
	}

	public void setCurrentPolicyPremium(BigDecimal currentPolicyPremium)
	{
		this.currentPolicyPremium = currentPolicyPremium;
	}

	public String getModifiedRegularTopUpAmount()
	{
		return modifiedRegularTopUpAmount;
	}

	public void setModifiedRegularTopUpAmount(String modifiedRegularTopUpAmount)
	{
		this.modifiedRegularTopUpAmount = modifiedRegularTopUpAmount;
	}

	public String getNewPolicyPremium()
	{
		return newPolicyPremium;
	}

	public void setNewPolicyPremium(String newPolicyPremium)
	{
		this.newPolicyPremium = newPolicyPremium;
	}

	public String getPayorDifferentWithOwnerIndicator()
	{
		return payorDifferentWithOwnerIndicator;
	}

	public void setPayorDifferentWithOwnerIndicator(String payorDifferentWithOwnerIndicator)
	{
		this.payorDifferentWithOwnerIndicator = payorDifferentWithOwnerIndicator;
	}

	public String getFundName()
	{
		return fundName;
	}

	public void setFundName(String fundName)
	{
		this.fundName = fundName;
	}

	public String getPercentage()
	{
		return percentage;
	}

	public void setPercentage(String percentage)
	{
		this.percentage = percentage;
	}

}
