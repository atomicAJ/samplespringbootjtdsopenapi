package com.candelalabs.epos.model;

public class PartialWithdrawal implements TransactionData
{
	private String amountToWithdraw;

	private String fundName;

	private String componentName;

	private String unitBalance;

	private String unitPrice;

	private String priceEffectiveDate;

	private String estimateFundValue;

	private String percentageToWithdraw;

	private String subTotalEastimatedValueToWithdraw;

	private String bankName;

	private String bankAccountName;

	private String bankAccountNumber;

	private String currency;

	public String getAmountToWithdraw()
	{
		return amountToWithdraw;
	}

	public void setAmountToWithdraw(String amountToWithdraw)
	{
		this.amountToWithdraw = amountToWithdraw;
	}

	public String getFundName()
	{
		return fundName;
	}

	public void setFundName(String fundName)
	{
		this.fundName = fundName;
	}

	public String getComponentName()
	{
		return componentName;
	}

	public void setComponentName(String componentName)
	{
		this.componentName = componentName;
	}

	public String getUnitBalance()
	{
		return unitBalance;
	}

	public void setUnitBalance(String unitBalance)
	{
		this.unitBalance = unitBalance;
	}

	public String getUnitPrice()
	{
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	public String getPriceEffectiveDate()
	{
		return priceEffectiveDate;
	}

	public void setPriceEffectiveDate(String priceEffectiveDate)
	{
		this.priceEffectiveDate = priceEffectiveDate;
	}

	public String getEstimateFundValue()
	{
		return estimateFundValue;
	}

	public void setEstimateFundValue(String estimateFundValue)
	{
		this.estimateFundValue = estimateFundValue;
	}

	public String getPercentageToWithdraw()
	{
		return percentageToWithdraw;
	}

	public void setPercentageToWithdraw(String percentageToWithdraw)
	{
		this.percentageToWithdraw = percentageToWithdraw;
	}

	public String getSubTotalEastimatedValueToWithdraw()
	{
		return subTotalEastimatedValueToWithdraw;
	}

	public void setSubTotalEastimatedValueToWithdraw(String subTotalEastimatedValueToWithdraw)
	{
		this.subTotalEastimatedValueToWithdraw = subTotalEastimatedValueToWithdraw;
	}

	public String getBankName()
	{
		return bankName;
	}

	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	public String getBankAccountName()
	{
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName)
	{
		this.bankAccountName = bankAccountName;
	}

	public String getBankAccountNumber()
	{
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber)
	{
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}

}
