package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Attribute
{
	private String label;
	private Object value;
	@JsonIgnore
	private String type;

	public Attribute(String label, Object value, String type)
	{
		this.label = label;
		this.value = value;
		this.type = type;
	}

	public String getLabel()
	{
		return label;
	}

	public Attribute setLabel(String label)
	{
		this.label = label;
		return this;
	}

	public Object getValue()
	{
		return value;
	}

	public Attribute setValue(Object value)
	{
		this.value = value;
		return this;
	}

	public String getType()
	{
		return type;
	}

	public Attribute setType(String type)
	{
		this.type = type;
		return this;
	}

	@Override
	public String toString()
	{
		return "Attribute [label=" + label + ", value=" + value + ", type=" + type + "]";
	}

}
