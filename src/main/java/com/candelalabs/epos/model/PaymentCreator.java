package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PaymentCreator implements TransactionData
{
	private String amount;

	private String transactionNumber;

	private String recieptNumber;

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public String getTransactionNumber()
	{
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber)
	{
		this.transactionNumber = transactionNumber;
	}

	public String getRecieptNumber()
	{
		return recieptNumber;
	}

	public void setRecieptNumber(String recieptNumber)
	{
		this.recieptNumber = recieptNumber;
	}

}
