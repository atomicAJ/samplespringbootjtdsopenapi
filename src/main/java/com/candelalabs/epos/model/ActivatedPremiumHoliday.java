package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ActivatedPremiumHoliday implements TransactionData
{

	private String paymentFrequency;

	private String policyNumber;

	private String premiumAmount;

	private String paidToDate;

	public String getPaymentFrequency()
	{
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency)
	{
		this.paymentFrequency = paymentFrequency;
	}

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getPremiumAmount()
	{
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount)
	{
		this.premiumAmount = premiumAmount;
	}

	public String getPaidToDate()
	{
		return paidToDate;
	}

	public void setPaidToDate(String paidToDate)
	{
		this.paidToDate = paidToDate;
	}

}
