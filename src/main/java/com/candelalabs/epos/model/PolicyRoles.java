package com.candelalabs.epos.model;

public class PolicyRoles
{
	private String policyNumber;

	private String clientNumber;

	private String customerrole;

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getClientNumber()
	{
		return clientNumber;
	}

	public void setClientNumber(String clientNumber)
	{
		this.clientNumber = clientNumber;
	}

	public String getCustomerrole()
	{
		return customerrole;
	}

	public void setCustomerrole(String customerrole)
	{
		this.customerrole = customerrole;
	}

}
