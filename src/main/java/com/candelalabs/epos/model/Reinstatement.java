package com.candelalabs.epos.model;

public class Reinstatement implements TransactionData
{
	private String policyNumber;

	private String outStandingPremiumAmount;

	private String insuredName;

	private String insuredOccupation;

	private String insuredHeight;

	private String insuredWeight;

	private HQClientDetails clientDetailsList;

	public HQClientDetails getClientDetailsList()
	{
		return clientDetailsList;
	}

	public void setClientDetailsList(HQClientDetails clientDetailsList)
	{
		this.clientDetailsList = clientDetailsList;
	}

	public String getPolicyNumber()
	{
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		this.policyNumber = policyNumber;
	}

	public String getOutStandingPremiumAmount()
	{
		return outStandingPremiumAmount;
	}

	public void setOutStandingPremiumAmount(String outStandingPremiumAmount)
	{
		this.outStandingPremiumAmount = outStandingPremiumAmount;
	}

	public String getInsuredName()
	{
		return insuredName;
	}

	public void setInsuredName(String insuredName)
	{
		this.insuredName = insuredName;
	}

	public String getInsuredOccupation()
	{
		return insuredOccupation;
	}

	public void setInsuredOccupation(String insuredOccupation)
	{
		this.insuredOccupation = insuredOccupation;
	}

	public String getInsuredHeight()
	{
		return insuredHeight;
	}

	public void setInsuredHeight(String insuredHeight)
	{
		this.insuredHeight = insuredHeight;
	}

	public String getInsuredWeight()
	{
		return insuredWeight;
	}

	public void setInsuredWeight(String insuredWeight)
	{
		this.insuredWeight = insuredWeight;
	}
}
