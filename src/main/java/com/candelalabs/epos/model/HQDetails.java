package com.candelalabs.epos.model;

public class HQDetails
{

	private String HQQuestion;

	private String HQLifeResponse;

	public String getHQQuestion()
	{
		return HQQuestion;
	}

	public void setHQQuestion(String hQQuestion)
	{
		HQQuestion = hQQuestion;
	}

	public String getHQLifeResponse()
	{
		return HQLifeResponse;
	}

	public void setHQLifeResponse(String hQLifeResponse)
	{
		HQLifeResponse = hQLifeResponse;
	}

}
