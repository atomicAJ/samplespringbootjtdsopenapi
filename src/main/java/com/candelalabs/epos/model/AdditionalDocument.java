package com.candelalabs.epos.model;

public class AdditionalDocument {

	private String reqtDropDownText;
	private String subCategory;
	private String additionaltext;
	private String notificationsent;

	public AdditionalDocument(String reqtDropDownText, String subCategory, String additionaltext,
			String notificationsent) {
		super();
		this.reqtDropDownText = reqtDropDownText;
		this.subCategory = subCategory;
		this.additionaltext = additionaltext;
		this.notificationsent = notificationsent;
	}

	public String getReqtDropDownText() {
		return reqtDropDownText;
	}

	public void setReqtDropDownText(String reqtDropDownText) {
		this.reqtDropDownText = reqtDropDownText;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getAdditionaltext() {
		return additionaltext;
	}

	public void setAdditionaltext(String additionaltext) {
		this.additionaltext = additionaltext;
	}

	public String getNotificationsent() {
		return notificationsent;
	}

	public void setNotificationsent(String notificationsent) {
		this.notificationsent = notificationsent;
	}

}
