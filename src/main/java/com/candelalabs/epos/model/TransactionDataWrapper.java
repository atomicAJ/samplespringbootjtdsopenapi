package com.candelalabs.epos.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TransactionDataWrapper implements TransactionData
{
	private TransactionData oldData;

	private TransactionData newData;

	private List<? extends TransactionData> listData;

	private List<? extends TransactionData> oldList;

	private List<? extends TransactionData> newList;

	public TransactionData getOldData()
	{
		return oldData;
	}

	public TransactionDataWrapper setOldData(TransactionData oldData)
	{
		this.oldData = oldData;
		return this;

	}

	public TransactionData getNewData()
	{
		return newData;
	}

	public TransactionDataWrapper setNewData(TransactionData newData)
	{
		this.newData = newData;
		return this;

	}

	public List<? extends TransactionData> getListData()
	{
		return listData;
	}

	public TransactionDataWrapper setListData(List<? extends TransactionData> listData)
	{
		this.listData = listData;
		return this;
	}

	public List<? extends TransactionData> getOldList()
	{
		return oldList;
	}

	public TransactionDataWrapper setOldList(List<? extends TransactionData> oldList)
	{
		this.oldList = oldList;
		return this;
	}

	public List<? extends TransactionData> getNewList()
	{
		return newList;
	}

	public TransactionDataWrapper setNewList(List<? extends TransactionData> newList)
	{
		this.newList = newList;
		return this;
	}

}
