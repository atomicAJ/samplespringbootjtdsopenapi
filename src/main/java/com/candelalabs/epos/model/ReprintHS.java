package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ReprintHS implements TransactionData
{
	private String insuredName;

	private String reason;

	private String firstName;

	private String middleName;

	private String lastName;

	private String componentName;

	private String planId;

	private String componentStatus;

	private String compPermStatus;

	public String getInsuredName()
	{
		return insuredName;
	}

	public void setInsuredName(String insuredName)
	{
		this.insuredName = insuredName;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getComponentName()
	{
		return componentName;
	}

	public void setComponentName(String componentName)
	{
		this.componentName = componentName;
	}

	public String getPlanId()
	{
		return planId;
	}

	public void setPlanId(String planId)
	{
		this.planId = planId;
	}

	public String getComponenetStatus()
	{
		return componentStatus;
	}

	public void setComponenetStatus(String componenetStatus)
	{
		this.componentStatus = componenetStatus;
	}

	public String getCompPermStatus()
	{
		return compPermStatus;
	}

	public void setCompPermStatus(String compPermStatus)
	{
		this.compPermStatus = compPermStatus;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

}
