package com.candelalabs.epos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PolicyInfo
{
	private String paymentFrequency;

	private String premiumTotal;

	private String paidToDate;

	private String currentPaymentMethod;

	public String getPaymentFrequency()
	{
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency)
	{
		this.paymentFrequency = paymentFrequency;
	}

	public String getPremiumTotal()
	{
		return premiumTotal;
	}

	public void setPremiumTotal(String premiumTotal)
	{
		this.premiumTotal = premiumTotal;
	}

	public String getPaidToDate()
	{
		return paidToDate;
	}

	public void setPaidToDate(String paidToDate)
	{
		this.paidToDate = paidToDate;
	}

	public String getCurrentPaymentMethod()
	{
		return currentPaymentMethod;
	}

	public void setCurrentPaymentMethod(String currentPaymentMethod)
	{
		this.currentPaymentMethod = currentPaymentMethod;
	}

}
