package com.candelalabs.ws.exception;

/**
 * If any Error occurs in the DAO layer we will throw this exception and 
 * set proper exception message which is required to display in UI 
 * @author Vikshith CV
 *
 */
public class DAOException extends Exception {

	public DAOException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DAOException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DAOException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7123368663846521960L;

	/**
	 * 
	 */
	
}
