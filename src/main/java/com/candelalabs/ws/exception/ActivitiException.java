/**
 * 
 */
package com.candelalabs.ws.exception;

/**
 * @author Triaji
 *
 */
public class ActivitiException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7687900100870584203L;

	/**
	 * 
	 */
	public ActivitiException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ActivitiException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public ActivitiException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ActivitiException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ActivitiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
