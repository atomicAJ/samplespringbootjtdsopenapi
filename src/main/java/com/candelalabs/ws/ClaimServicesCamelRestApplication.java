package com.candelalabs.ws;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableTransactionManagement
@PropertySource(value = { "classpath:application.properties" })
@ComponentScan(basePackages = { "com.candelalabs.ws", "com.awpl.ecm","com.candelalabs.epos"  })
public class ClaimServicesCamelRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimServicesCamelRestApplication.class, args);
	}

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {

		JndiTemplate jndiTemplate = new JndiTemplate();
		DataSource dataSource = null;
		try {
			dataSource = (DataSource) jndiTemplate.lookup(env.getRequiredProperty("fwdeService.db.jndiName"));
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return dataSource;
	}

	@Bean(name = "jdbcTemplateClaims")
	public JdbcTemplate jdbcTemplateClaims(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}

	@Bean
	public DataSource odsDataSource() {
		JndiTemplate jndiTemplate = new JndiTemplate();
		DataSource dataSource = null;
		try {
			dataSource = (DataSource) jndiTemplate.lookup(env.getRequiredProperty("ods.db.jndiName"));
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return dataSource;
	}

	@Bean(name = "jdbcTemplateOds")
	public JdbcTemplate jdbcTemplateOds(DataSource odsDataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(odsDataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}

	@Bean
	public DataSource dsConfigDataSource() {
		JndiTemplate jndiTemplate = new JndiTemplate();
		DataSource dataSource = null;
		try {
			dataSource = (DataSource) jndiTemplate.lookup(env.getRequiredProperty("dsConfig.db.jndiName"));
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return dataSource;
	}

	@Bean(name = "jdbcTemplateDsConfig")
	public JdbcTemplate jdbcTemplateDsConfig(DataSource dsConfigDataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dsConfigDataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}
	
	@Bean(name="transactionManager")
	public PlatformTransactionManager txManager(){
	    DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource());
	    return transactionManager;
	}	

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		// builder.basicAuthorization(this.activitiUser, this.activitiPassword);
		return builder.build();
	}
}
