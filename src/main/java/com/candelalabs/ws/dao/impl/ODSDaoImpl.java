package com.candelalabs.ws.dao.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;

import com.candelalabs.api.model.ProductListData;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.JsonWSSaveClaimUIDataIn;
import com.candelalabs.ws.model.WSCLRegClaimBnfcDetailsBOData;
import com.candelalabs.ws.model.WSDULRegClaimClaimPolicyDetailsBOData;
import com.candelalabs.ws.model.WSGENRegClaimBO;
import com.candelalabs.ws.model.WSGENUpdClaimRejectStatusBO;
import com.candelalabs.ws.model.tables.BankCodeMasterModel;
import com.candelalabs.ws.model.tables.BenCltAddDetails;
import com.candelalabs.ws.model.tables.Beneficiarydetails;
import com.candelalabs.ws.model.tables.BenefitPlanMaster;
import com.candelalabs.ws.model.tables.BenefitSummaryModel;
import com.candelalabs.ws.model.tables.ClaimHistoryModel;
import com.candelalabs.ws.model.tables.CleintDetPolNoBased;
import com.candelalabs.ws.model.tables.DiagnosisMasterModel;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.ComponentCode;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.HospitalMasterDetails;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.model.tables.PolicyClientBeneficiaryAddressList;
import com.candelalabs.ws.model.tables.PolicyClientOwnerPhoneResultSet;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.model.tables.PolicyRelatedModel;
import com.candelalabs.ws.util.Util;

@Repository("oDSDao")
public class ODSDaoImpl implements ODSDao {

	private static Log log = LogFactory.getLog("ODSDaoImpl");

	@Autowired
	private PlatformTransactionManager transactionManager;

	@Autowired
	JdbcTemplate jdbcTemplateOds;
	@Autowired
	ClaimDetailsDao claimDetailsDao;
	public String sql;

	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public static Log getLog() {
		return log;
	}

	public static void setLog(Log log) {
		ODSDaoImpl.log = log;
	}

	public JdbcTemplate getJdbcTemplateOds() {
		return jdbcTemplateOds;
	}

	public void setJdbcTemplateOds(JdbcTemplate jdbcTemplateOds) {
		this.jdbcTemplateOds = jdbcTemplateOds;
	}

	public PolicyDetails getPayeeNumber(String polNo) throws DAOException {

		log.info("ODSDaoImpl - In Method - getPayeeNumber");
		log.info("ODSDaoImpl - In Method - getPayeeNumber for polNo - " + polNo);
		log.debug("ODSDaoImpl - In Method - getPayeeNumber");
		List<PolicyDetails> details = new ArrayList<PolicyDetails>();
		// String details= null;
		try {
			details = jdbcTemplateOds.query("Select OWNERCLIENTNO,CURRENCY from [dbo].[V_POLICYDETAILS] "
					+ "where POLICYNUMBER = '" + polNo + "'",
					new BeanPropertyRowMapper<PolicyDetails>(PolicyDetails.class));
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPayeeNumber - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		log.info("ODSDaoImpl - Success - getPayeeNumber - Retreived Payee Number");
		log.debug("ODSDaoImpl - Return From  Method - getPayeeNumber");
		if (null != details)
			return details.get(0);
		else
			return null;

	}

	public PolicyDetails getPolicyDetail(String polNo) throws DAOException {

		log.info("ODSDaoImpl - In Method - getPolicyDetail");
		log.info("ODSDaoImpl - In Method - getPolicyDetail for polNo - " + polNo);
		log.debug("ODSDaoImpl - In Method - getPolicyDetail");
		PolicyDetails details = new PolicyDetails();
		// String details= null;
		try {

			// details = jdbcTemplateOds.queryForObject(
			// "Select PAYORCLIENTNO,CURRENCY,COMPANY,OWNERCLIENTNO from
			// [dbo].[V_POLICYDETAILS] "
			// + "where POLICYNUMBER = '" + polNo + "'",
			// new BeanPropertyRowMapper<PolicyDetails>(PolicyDetails.class));

			details = jdbcTemplateOds.queryForObject(
					"select * from V_POLICYDETAILS " + " where POLICYNUMBER = '" + polNo + "'",
					new BeanPropertyRowMapper<PolicyDetails>(PolicyDetails.class));

			/*
			 * String sql =
			 * "SELECT * from V_POLICYDETAILS WHERE POLICYNUMBER=?  " + "";
			 * 
			 * details = (PolicyDetails) jdbcTemplateOds.queryForObject(sql, new
			 * Object[] { polNo }, PolicyDetails.class);
			 */

		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyDetail - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		log.info("ODSDaoImpl - Success - getPolicyDetail - Retreived PolicyDetail ");
		log.debug("ODSDaoImpl - Return From  Method - getPolicyDetail");
		if (null != details)
			return details;
		else
			return null;

	}

	public ComponentCode getComponentDetails(String PolicyNo, String compCode, String ins_clnt_no) throws DAOException {
		log.info("ODSDaoImpl - In Method - getComponentDetails");
		log.info("ODSDaoImpl - In Method - getComponentDetails for polNo - " + PolicyNo);
		log.debug("ODSDaoImpl - In Method - getComponentDetails");
		ComponentCode details = new ComponentCode();
		// String details= null;
		try {
			sql = "Select * from V_COMPONENTDETAILS " + "where POLICYNUMBER = '" + PolicyNo + "' "
					+ "and COMPONENTCODE = '" + compCode + "' " + "and CLIENTNUMBER = '" + ins_clnt_no + "'";
			details = jdbcTemplateOds.queryForObject(sql,
					new BeanPropertyRowMapper<ComponentCode>(ComponentCode.class));
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getComponentDetails - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		log.info("ODSDaoImpl - Success - getComponentDetails - Retreived ComponentDetail ");
		log.debug("ODSDaoImpl - Return From  Method - getComponentDetails");
		if (null != details)
			return details;
		else
			return null;
	}
	@Override
	public PolicyClientOwnerPhoneResultSet getPolicyClientOwnerPhoneNos(String polNo)  throws DAOException{
		log.info("ODSDaoImpl - In Method - getPolicyClientOwnerPhoneNos");
		log.info("ODSDaoImpl - In Method - getPolicyClientOwnerPhoneNos for polNo - " + polNo);
		log.debug("ODSDaoImpl - In Method - getPolicyClientOwnerPhoneNos");
		PolicyClientOwnerPhoneResultSet details = new PolicyClientOwnerPhoneResultSet();
		// String details= null;
		try {

			sql =  "SELECT PHONENO01, PHONENO02, PHONENO03 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD "
					+ "WHERE VCD.CLIENTNO=VPD.OWNERCLIENTNO AND VPD.POLICYNUMBER = '" + polNo + "'";
			details = jdbcTemplateOds.queryForObject(sql,
					new BeanPropertyRowMapper<PolicyClientOwnerPhoneResultSet>(PolicyClientOwnerPhoneResultSet.class));
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyClientOwnerPhoneNos - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		log.debug("ODSDaoImpl - Return From  Method - getPolicyClientOwnerPhoneNos");
		if (null != details)
			return details;
		else
			return null;
	}
	@Override
	public PolicyClientBeneficiaryAddressList getPolicyClientBeneficiaryAddressList(String clientNo) throws DAOException{
		log.info("ODSDaoImpl - In Method - getPolicyClientBeneficiaryAddressList");
		log.info("ODSDaoImpl - In Method - getPolicyClientBeneficiaryAddressList for clientNo - " + clientNo);
		log.debug("ODSDaoImpl - In Method - getPolicyClientBeneficiaryAddressList");
		PolicyClientBeneficiaryAddressList details = new PolicyClientBeneficiaryAddressList();
		// String details= null;
		try {

			sql =  "SELECT ADDRESS1, ADDRESS2, ADDRESS3,ADDRESS4,ADDRESS5 from  "
					+ "V_CLIENTDETAILS AS VCD  "
					+ " WHERE VCD.CLIENTNO = '" + clientNo + "'";

			details = jdbcTemplateOds.queryForObject(sql,
					new BeanPropertyRowMapper<PolicyClientBeneficiaryAddressList>(PolicyClientBeneficiaryAddressList.class));
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyClientBeneficiaryAddressList - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		log.debug("ODSDaoImpl - Return From  Method - getPolicyClientBeneficiaryAddressList");
		if (null != details)
			return details;
		else
			return null;
	}
	public PolicyDetails getWSPolicyDetails(String PolicyNo) {
		log.info("ODSDaoImpl - In Method - getWSPolicyDetails - PolicyNo - is" + PolicyNo);

		List<PolicyDetails> listMapPolcDetails = new ArrayList<PolicyDetails>();
		Object[] inputs = new Object[] { PolicyNo };

		try {
			listMapPolcDetails = jdbcTemplateOds.query(
					"Select PAYORCLIENTNO,CURRENCY,COMPANY,OWNERCLIENTNO from [dbo].[POLICY DETAILS] "
							+ "where POLICYNUMBER = ?",
							inputs, new BeanPropertyRowMapper<PolicyDetails>(PolicyDetails.class));
			log.info(
					"ODSDaoImpl - In Method - getWSPolicyDetails - result from databse query result in list of object is"
							+ listMapPolcDetails);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getWSPolicyDetails - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapPolcDetails).isPresent() && !listMapPolcDetails.isEmpty())
			return listMapPolcDetails.get(0);
		else
			return null;
	}

	public WSCLRegClaimBnfcDetailsBOData getWSCLRegClmBeneficiaryDetails(String PolicyNo) {
		log.info("ODSDaoImpl - In Method - getWSCLRegClmBeneficiaryDetails - PolicyNo - is" + PolicyNo);

		List<WSCLRegClaimBnfcDetailsBOData> listMapBnfcDetails = new ArrayList<WSCLRegClaimBnfcDetailsBOData>();
		Object[] inputs = new Object[] { PolicyNo };

		try {
			listMapBnfcDetails = jdbcTemplateOds.query(
					"Select Chdrcoy,Boiden,Msglng,Msgcnt,Indc,Life,Coverage,Rider,Crtable,ClamParty,CurrCode,PayClt,BankAcount,BankAccDsc \r\n"
							+ " from [BENEFICIARY DETAILS] where PolicyNo = ?",
							inputs,
							new BeanPropertyRowMapper<WSCLRegClaimBnfcDetailsBOData>(WSCLRegClaimBnfcDetailsBOData.class));
			log.info(
					"ODSDaoImpl - In Method - getWSCLRegClmBeneficiaryDetails - result from databse query result in list of object is"
							+ listMapBnfcDetails);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getWSCLRegClmBeneficiaryDetails - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapBnfcDetails).isPresent() && !listMapBnfcDetails.isEmpty())
			return listMapBnfcDetails.get(0);
		else
			return null;
	}

	public HospitalMasterDetails getHospitalMasterDetails(String ProviderCode) throws DAOException {
		log.info("ODSDaoImpl - In Method - getHospitalMasterDetails - ProviderCode - is" + ProviderCode);

		List<HospitalMasterDetails> listMapHospitalDetails = new ArrayList<HospitalMasterDetails>();
		Object[] inputs = new Object[] { ProviderCode };

		try {
			listMapHospitalDetails = jdbcTemplateOds.query(
					"Select PROVIDERCLIENTNUMBER,PROVIDERNAME \r\n"
							+ " from dbo.V_HOSPITALMASTER where PROVIDERCODELA = ?",
							inputs, new BeanPropertyRowMapper<HospitalMasterDetails>(HospitalMasterDetails.class));
			log.info(
					"ODSDaoImpl - In Method - getWSCLRegClmBeneficiaryDetails - result from databse query result in list of object is"
							+ listMapHospitalDetails);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getWSCLRegClmBeneficiaryDetails - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		if (Optional.ofNullable(listMapHospitalDetails).isPresent() && !listMapHospitalDetails.isEmpty())
			return listMapHospitalDetails.get(0);
		else
			return null;
	}

	@Override
	public WSGENRegClaimBO getWSGENRegClaimData(String policyNumber) throws Exception {

		log.debug("ODSDaoImpl - In Method - getWSGENRegClaimData");
		List<WSGENRegClaimBO> wsGENRegClaimBO = new ArrayList<WSGENRegClaimBO>();
		Object[] queryParams = new Object[] { policyNumber };
		;
		try {
			wsGENRegClaimBO = jdbcTemplateOds.query(
					" Select BenDet.CLIENTNO as CLNTNUM, ComDet.LIFE as LIFE, ComDet.COVERAGE as COVERAGE, ComDet.RIDER as RIDER, ComDet.COMPONENTCODE as CRTABLE "
							+ " ,ComDet.SUMASSURED as TOTAMNT "
							+ " from dbo.[BENEFICIARY DETAILS] BenDet join dbo.[COMPONENT DETAILS] ComDet on BenDet.POLICYNUMBER = ComDet.POLICYNUMBER where BenDet.POLICYNUMBER = ? ",
							queryParams, new BeanPropertyRowMapper<WSGENRegClaimBO>(WSGENRegClaimBO.class));
			log.info(wsGENRegClaimBO);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getWSGENRegClaimData - " + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to get Generic registration claim BO data from ODSServices DB");
		}
		log.info(
				"ODSDaoImpl - Success - getWSGENRegClaimData - Retreived Generic registration claim BO data from ODSServices DB");
		log.debug("ODSDaoImpl - Return From  Method - getWSGENRegClaimData");
		if (Optional.ofNullable(wsGENRegClaimBO).isPresent() && !wsGENRegClaimBO.isEmpty())
			return wsGENRegClaimBO.get(0);
		else
			return null;

	}

	@Override
	public WSGENUpdClaimRejectStatusBO getWSGENUpdClaimRejectData(String policyNumber) throws Exception {

		log.debug("ODSDaoImpl - In Method - getWSGENUpdClaimRejectData");
		List<WSGENUpdClaimRejectStatusBO> wsGENRegClaimBO = new ArrayList<WSGENUpdClaimRejectStatusBO>();
		Object[] queryParams = new Object[] { policyNumber };
		;
		try {
			wsGENRegClaimBO = jdbcTemplateOds.query(
					" Select PolDet.COMPANY as CHDRCOY, ComDet.LIFE as LIFE, ComDet.COVERAGE as COVERAGE, ComDet.RIDER as RIDER, ComDet.COMPONENTCODE as CRTABLE "
							+ " from [dbo].[POLICY DETAILS] PolDet join [dbo].[COMPONENT DETAILS] ComDet on PolDet.POLICYNUMBER = ComDet.POLICYNUMBER where ComDet.POLICYNUMBER = ? ",
							queryParams,
							new BeanPropertyRowMapper<WSGENUpdClaimRejectStatusBO>(WSGENUpdClaimRejectStatusBO.class));

			log.info(
					"ODSDaoImpl - Success - getWSGENUpdClaimRejectData - Retreived Generic claim update claim reject BO data from ODSServices DB");
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getWSGENUpdClaimRejectData - " + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to get Generic claim update claim reject BO data from ODSServices DB");
		}
		log.debug("ODSDaoImpl - Return From  Method - getWSGENUpdClaimRejectData");
		if (Optional.ofNullable(wsGENRegClaimBO).isPresent() && !wsGENRegClaimBO.isEmpty())
			return wsGENRegClaimBO.get(0);
		else
			return null;

	}

	@Override
	public void updateClaimUiData(JsonWSSaveClaimUIDataIn jsonWSSaveClaimUIDataIn) throws Exception {
		log.info("ODSDaoImpl - In Method - updateClaimUiData");
		log.info("ODSDaoImpl - In Method - updateClaimUiData Data JsonWSSaveClaimUIDataIn" + jsonWSSaveClaimUIDataIn);
		int beneficiaryDetailsUpdatedCount = 0;
		try {

			beneficiaryDetailsUpdatedCount = jdbcTemplateOds.update(
					" update [BENEFICIARY DETAILS] set CLIENTNAME = ? where POLICYNUMBER = ?",
					jsonWSSaveClaimUIDataIn.getInsuredName(), jsonWSSaveClaimUIDataIn.getPolicyNumber());

			if (beneficiaryDetailsUpdatedCount == 0) {
				log.info("ODSDaoImpl - In Method - updateClaimUiData - No rows updated for POLICYNUMBER = "
						+ jsonWSSaveClaimUIDataIn.getPolicyNumber() + " in ODS database.");
			} else {
				log.info("ODSDaoImpl - In Method - updateClaimUiData - " + beneficiaryDetailsUpdatedCount
						+ " Rows updated for POLICYNUMBER = " + jsonWSSaveClaimUIDataIn.getPolicyNumber());
			}

		} catch (Exception e) {
			log.error(
					"ODSDaoImpl - Error - updateClaimUiData - unable to update ClaimDataUI data into BENEFICIARY table "
							+ e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to update ClaimDataUI data into BENEFICIARY table for TPA policy number "
							+ jsonWSSaveClaimUIDataIn.getPolicyNumber());
		}
		log.info("ODSDaoImpl - Success - updateClaimUiData - Updated BENEFICIARY DETAILS");
		log.info("ODSDaoImpl - Return From  Method - updateClaimUiData");

	}

	public Beneficiarydetails getPolicyDataRequestorByPOLICYNUMBER(String POLICYNUMBER) throws DAOException {
		log.debug("ODSDaoImpl - In Method - getPendingTriggerClaims");
		List<Beneficiarydetails> details = new ArrayList<Beneficiarydetails>();
		try {
			details = jdbcTemplateOds.query(
					"Select * from [dbo].[BENEFICIARY DETAILS] " + "where POLICYNUMBER = '" + POLICYNUMBER + "'",
					new BeanPropertyRowMapper<Beneficiarydetails>(Beneficiarydetails.class));
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPendingTriggerClaims - " + e.getMessage());
			throw new DAOException(
					e.getMessage() + "Unable to get Beneficiary details for policy number " + POLICYNUMBER);
		}
		log.info("ODSDaoImpl - Success - getPendingTriggerClaims - Retreived Trigger Pending Claims");
		log.debug("ODSDaoImpl - Return From  Method - getPendingTriggerClaims");
		if (null != details && !details.isEmpty())
			return details.get(0);
		else
			return null;
	}

	public String getComponentCode(String polNo, String clientNo, String comp_status) {
		String componentCode = null;
		sql = "SELECT COMPONENTCODE FROM [dbo].[COMPONENT DETAILS] WHERE POLICYNUMBER=? and "
				+ "CLIENTNUMBER =? and COMPONENTSTATUS = ?";

		componentCode = (String) jdbcTemplateOds.queryForObject(sql, new Object[] { polNo, clientNo, comp_status },
				String.class);

		return componentCode;
	}
	public String getAgentEmailAddr(String polNo) throws DAOException{
		String agentEmailaddr = null;
		sql = "SELECT VADM.ROLEEMAILADDRESS FROM V_AGENTDETAILMASTER AS VADM, V_POLICYDETAILS AS VPD "
				+ " WHERE VADM.ROLEID = VPD.AGENTID AND VADM.SUPERVISORID = VPD.AGENTID "
				+ "AND VPD.POLICYNUMBER= ?";
		log.info("ODSDaoImpl--getAgentEmailAddr-- sql is "+ sql);
		try{ agentEmailaddr = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);
		}
		catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyClientOwnerPhoneNos - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}
		return agentEmailaddr;
	}
	public String getEmailAddrSD(String polNo) throws DAOException{
		String sDEmailaddr = null;
		sql = "SELECT VADM.ROLEEMAILADDRESS from V_AGENTDETAILMASTER AS VADM, V_POLICYDETAILS AS VPD "
				+ "WHERE VADM.ROLEID = VPD.AGENTID AND VADM.ROLENAME ='RM' "
				+ "AND VPD.POLICYNUMBER = ?";
		log.info("ODSDaoImpl--getEmailAddrSD-- sql is "+ sql);
		try{ sDEmailaddr = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);}
		catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyClientOwnerPhoneNos - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}
		return sDEmailaddr;
	}
	public String getEmailAddrNSD(String polNo) throws DAOException{
		String nSDEmailaddr = null;
		sql = "SELECT Distinct(VADM.ROLEEMAILADDRESS) from V_AGENTDETAILMASTER AS VADM, V_POLICYDETAILS AS VPD  "
				+ "WHERE VADM.ROLEID = VPD.AGENTID AND VADM.ROLENAME = 'SD' "
				+ "AND VPD.POLICYNUMBER = ? ";

		log.info("ODSDaoImpl--getEmailAddrNSD-- sql is "+ sql);
		try{ nSDEmailaddr = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);}
		catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyClientOwnerPhoneNos - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}
		return nSDEmailaddr;
	}
	@Override
	public String getPolicyClientOwnerAddress1(String polNo){
		String PolicyClientOwnerAddress1 = null;
		sql = "SELECT ADDRESS1 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD  "
				+ "WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO ";


		log.info("ODSDaoImpl--getPolicyClientOwnerAddress1-- sql is "+ sql);
		PolicyClientOwnerAddress1 = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);

		return PolicyClientOwnerAddress1;
	}
	@Override
	public String getPolicyClientBeneficiaryNumber(String polNo) throws DAOException{
		String CLIENTNO = null;
		sql = "SELECT TOP 1 CLIENTNO FROM V_BENEFICIARYDETAILS AS VBD WHERE VBD.POLICYNUMBER = ?";


		log.info("ODSDaoImpl--getPolicyClientBeneficiaryNumber-- sql is "+ sql);
		try {CLIENTNO = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);}
		catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPolicyClientBeneficiaryAddressList - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return CLIENTNO;

	}
	@Override
	public String getPaymentFreq(String polNo) throws DAOException{
		String paymentFreq = null;
		sql = "SELECT PAYMENTFREQ FROM V_POLICYDETAILS WHERE POLICYNUMBER =  ?";

		log.info("ODSDaoImpl--getPaymentFreq-- sql is "+ sql);
		try {
			paymentFreq = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
					{ polNo },
					String.class);
		}
		catch (Exception e) {
			log.error("ODSDaoImpl - Error - getPaymentFreq - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return paymentFreq;
	}
	@Override
	public String getPolicyClientOwnerAddress2(String polNo){
		String PolicyClientOwnerAddress2 = null;
		sql = "SELECT ADDRESS2 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD  "
				+ "WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO ";


		log.info("ODSDaoImpl--getPolicyClientOwnerAddress1-- sql is "+ sql);
		PolicyClientOwnerAddress2 = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);

		return PolicyClientOwnerAddress2;
	}
	@Override
	public String getPolicyClientOwnerAddress3(String polNo){
		String PolicyClientOwnerAddress3 = null;
		sql = "SELECT ADDRESS3 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD  "
				+ "WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO ";


		log.info("ODSDaoImpl--getPolicyClientOwnerAddress1-- sql is "+ sql);
		PolicyClientOwnerAddress3 = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);

		return PolicyClientOwnerAddress3;
	}
	@Override
	public String getPolicyClientOwnerAddress4(String polNo){
		String PolicyClientOwnerAddress4 = null;
		sql = "SELECT ADDRESS4 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD  "
				+ "WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO ";


		log.info("ODSDaoImpl--getPolicyClientOwnerAddress1-- sql is "+ sql);
		PolicyClientOwnerAddress4 = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);

		return PolicyClientOwnerAddress4;
	}
	@Override
	public String getPolicyClientOwnerAddress5(String polNo){
		String PolicyClientOwnerAddress5 = null;
		sql = "SELECT Address5 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD  "
				+ "WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO ";


		log.info("ODSDaoImpl--getPolicyClientOwnerAddress1-- sql is "+ sql);
		PolicyClientOwnerAddress5 = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);

		return PolicyClientOwnerAddress5;
	}
	@Override
	public String getPolicyClientOwnerEmailAddress(String polNo){
		String PolicyClientOwnerEamilAddress = null;
		sql = "SELECT EMAILADDRESS FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD   "
				+ "WHERE VCD.CLIENTNO=VPD.OWNERCLIENTNO AND VPD.POLICYNUMBER = ? ";


		log.info("ODSDaoImpl--getPolicyClientOwnerEmailAddress-- sql is "+ sql);
		PolicyClientOwnerEamilAddress = (String) jdbcTemplateOds.queryForObject(sql, new Object[] 
				{ polNo },
				String.class);

		return PolicyClientOwnerEamilAddress;
	}
	public ComponentDetailsBO getComponentDetailsBO(String PolicyNo, String ComponentCode, String ClientID)
			throws Exception {
		log.info("ODSDaoImpl - In Method - getwsclRegClaimComponentDetailsBO - PolicyNo - is" + PolicyNo);

		List<ComponentDetailsBO> listMapBnfcDetails = new ArrayList<ComponentDetailsBO>();
		Object[] inputs = new Object[] { PolicyNo, ComponentCode, ClientID };

		try {
			listMapBnfcDetails = jdbcTemplateOds.query(
					"Select LIFE as Life,COVERAGE as Coverage,RIDER as Rider from V_COMPONENTDETAILS where POLICYNUMBER = ? "
							+ "and COMPONENTCODE = ? and CLIENTNUMBER = ?",
							inputs, new BeanPropertyRowMapper<ComponentDetailsBO>(ComponentDetailsBO.class));

			log.info(
					"ODSDaoImpl - In Method - getwsclRegClaimComponentDetailsBO - result from databse query result in list of object is"
							+ listMapBnfcDetails);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getwsclRegClaimComponentDetailsBO - " + e.getMessage());
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		if (Optional.ofNullable(listMapBnfcDetails).isPresent() && !listMapBnfcDetails.isEmpty())
			return listMapBnfcDetails.get(0);
		else
			return null;
	}

	public ClientDetails getClientDetails(String CLIENTNO) throws Exception {
		ClientDetails clt_det = new ClientDetails();

		try {
			this.sql = "Select * from [dbo].[V_CLIENTDETAILS] where CLIENTNO=?";
			clt_det = jdbcTemplateOds.queryForObject(this.sql, new Object[] { CLIENTNO },
					new BeanPropertyRowMapper<ClientDetails>(ClientDetails.class));
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getClientDetails - " + e.getMessage());
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		log.info("ODSDaoImpl - Success - getClientDetails - Retreived getClientDetails ");
		log.debug("ODSDaoImpl - Return From  Method - getClientDetails");
		if (null != clt_det)
			return clt_det;
		else
			return null;
		// return clt_det;
	}

	public String getpHEmailAddress(String PolNo) throws DAOException {
		String pHEmailAddress = null;
		String insClntId = null;
		insClntId = claimDetailsDao.getInsuredClientNo(PolNo);
		sql = "SELECT EMAILADDRESS FROM V_CLIENTDETAILS " + "WHERE CLIENTNO=?";

		try {
			pHEmailAddress = (String) jdbcTemplateOds.queryForObject(sql, new Object[] { insClntId }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return pHEmailAddress;

	}

	public String getpHPhoneNo(String polNo) throws DAOException {
		String pHPhoneNo = null;
		String insClntId = null;
		insClntId = claimDetailsDao.getInsuredClientNo(polNo);
		sql = "SELECT PHONENO01 FROM V_CLIENTDETAILS " + "WHERE CLIENTNO=?";

		try {
			pHPhoneNo = (String) jdbcTemplateOds.queryForObject(sql, new Object[] { insClntId }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return pHPhoneNo;
	}

	public String getAgentemailadd(String polNo) throws DAOException {
		String agentemailadd = null;
		String aGENTID = null;

		sql = "SELECT AGENTID FROM Policy_Details " + "WHERE POLICYNUMBER=?";
		aGENTID = (String) jdbcTemplateOds.queryForObject(sql, new Object[] { polNo }, String.class);
		log.info("ODSDaoImpl - getAgentemailadd -- aGENTID is " + aGENTID);
		sql = "SELECT ROLEEMAILADDRESS FROM AGENT_DETAILS_MASTER " + "WHERE ROLEID=?";

		try {
			agentemailadd = (String) jdbcTemplateOds.queryForObject(sql, new Object[] { aGENTID }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		log.info("ODSDaoImpl - getAgentemailadd -- agentemailadd is " + agentemailadd);

		return agentemailadd;
	}

	public WSDULRegClaimClaimPolicyDetailsBOData getWSDULClaimPolicyDetails(String policyNo) {
		log.info("ODSDaoImpl - In Method - getWSDULClaimPolicyDetails - PolicyNo - is" + policyNo);

		List<WSDULRegClaimClaimPolicyDetailsBOData> listMapBnfcDetails = new ArrayList<WSDULRegClaimClaimPolicyDetailsBOData>();
		Object[] inputs = new Object[] { policyNo };

		try {
			listMapBnfcDetails = jdbcTemplateOds.query("Select Chdrcoy from [BENEFICIARY DETAILS] where PolicyNo = ?",
					inputs, new BeanPropertyRowMapper<WSDULRegClaimClaimPolicyDetailsBOData>(
							WSDULRegClaimClaimPolicyDetailsBOData.class));
			log.info(
					"ODSDaoImpl - In Method - getWSDULClaimPolicyDetails - result from databse query result in list of object is"
							+ listMapBnfcDetails);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getWSDULClaimPolicyDetails - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapBnfcDetails).isPresent() && !listMapBnfcDetails.isEmpty())
			return listMapBnfcDetails.get(0);
		else
			return null;
	}

	@Override
	public List<String> getDiagnoseCodeList(String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub
		sql = "";
		String result = "";
		List<String> myList = null;
		if (claimTypeUI.equalsIgnoreCase("CI") || claimTypeUI.equalsIgnoreCase("C1")) {
			sql = "SELECT STUFF((SELECT ',' + CRITICALILLNESSCODE + ''  FROM ods.dbo.V_CIMASTER FOR XML PATH ('')), 1,1,'') AS DIAGNOSISCODELIST";
			try {
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else {
			sql = "SELECT STUFF((SELECT ',' + DIAGNOSISCODE + '' FROM ods.dbo.V_DIAGNOSISMASTER FOR XML PATH ('')), 1,1,'') AS DIAGNOSISCODELIST";
			try {
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}
		if (result.trim() != "" && result != null) {
			myList = new ArrayList<String>(Arrays.asList(result.split(",")));
		}

		return myList;
	}

	@Override
	public List<String> getDiagnoseNameList(String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub
		sql = "";
		// String result = "";
		List<String> myList = new ArrayList<String>();
		if (claimTypeUI.equalsIgnoreCase("CI") || claimTypeUI.equalsIgnoreCase("C1")) {
			// sql = "SELECT STUFF((SELECT ',' + CRITICALILLNESSDESC + '' FROM
			// ods.dbo.V_CIMASTER FOR XML PATH ('')), 1,1,'') AS
			// DIAGNOSISNAMELIST";
			sql = "select CRITICALILLNESSDESC FROM ods.dbo.V_CIMASTER";
			try {
				myList = this.jdbcTemplateOds.queryForList(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else {
			// sql = "SELECT STUFF((SELECT ',' + DIAGNOSISNAME + '' FROM
			// ods.dbo.V_DIAGNOSISMASTER FOR XML PATH ('')), 1,1,'') AS
			// DIAGNOSISNAMELIST";
			sql = "select DIAGNOSISNAME FROM V_DIAGNOSISMASTER";
			try {
				myList = this.jdbcTemplateOds.queryForList(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}

		return myList;
	}

	@Override
	public DiagnosisMasterModel getDiagnosisModelByCode(String diagnosisCode, String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub
		sql = "";
		DiagnosisMasterModel masterModel = new DiagnosisMasterModel();
		if (claimTypeUI.trim().equalsIgnoreCase("CI") || claimTypeUI.trim().equalsIgnoreCase("C1")) {
			sql = "select CRITICALILLNESSCODE as DIAGNOSISCODE, CRITICALILLNESSDESC as DIAGNOSISNAME, CLAIMTYPE, CLAIMPERCENTAGE from dbo.V_CIMASTER "
					+ " where CRITICALILLNESSCODE = ? ";

			try {
				masterModel = this.jdbcTemplateOds.queryForObject(sql, new Object[] { diagnosisCode },
						new BeanPropertyRowMapper<DiagnosisMasterModel>(DiagnosisMasterModel.class));
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else {
			List<DiagnosisMasterModel> ciModel_li = new ArrayList<DiagnosisMasterModel>();
			sql = "SELECT DIAGNOSISNAME FROM dbo.V_DIAGNOSISMASTER WHERE DIAGNOSISCODE = ?";
			try {
				ciModel_li = this.jdbcTemplateOds.query(sql, new Object[] { diagnosisCode },
						new BeanPropertyRowMapper<DiagnosisMasterModel>(DiagnosisMasterModel.class));
				masterModel = ciModel_li.get(0);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}

		return masterModel;
	}

	@Override
	public List<DiagnosisMasterModel> getDiagnosisModelByName(String diagnosisName, String claimTypeUI)
			throws DAOException {
		// TODO Auto-generated method stub
		sql = "";
		List<DiagnosisMasterModel> masterModel = new ArrayList<DiagnosisMasterModel>();
		if (claimTypeUI.trim().equalsIgnoreCase("CI") || claimTypeUI.trim().equalsIgnoreCase("C1")) {
			sql = "select CRITICALILLNESSCODE as DIAGNOSISCODE, CRITICALILLNESSDESC as DIAGNOSISNAME, CLAIMTYPE, CLAIMPERCENTAGE from dbo.V_CIMASTER "
					+ " where CRITICALILLNESSDESC = ? ";

			try {
				masterModel = this.jdbcTemplateOds.query(sql, new Object[] { diagnosisName },
						new BeanPropertyRowMapper<DiagnosisMasterModel>(DiagnosisMasterModel.class));
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else {
			sql = "SELECT * FROM V_DIAGNOSISMASTER WHERE DIAGNOSISNAME = ? ";

			try {
				masterModel = this.jdbcTemplateOds.query(sql, new Object[] { diagnosisName },
						new BeanPropertyRowMapper<DiagnosisMasterModel>(DiagnosisMasterModel.class));
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}
		return masterModel;
	}

	@Override
	public List<String> getProviderNameList() throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT STUFF((SELECT ','  + PROVIDERNAME  + '' FROM V_HOSPITALMASTER FOR XML PATH ('')), 1,1,'') AS PROVIDERNAMELIST";
		String result = "";
		List<String> myList = null;
		try {
			result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			if (result.trim() != "" && result != null) {
				myList = new ArrayList<String>(Arrays.asList(result.split(",")));
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return myList;
	}

	@Override
	public String getProviderCode(String providerName) throws DAOException {
		sql = "select PROVIDERCODELA from V_HOSPITALMASTER where PROVIDERNAME = '" + providerName.trim() + "'";
		List<String> result_li = new ArrayList<String>();
		String result = "";
		try {
			result_li = this.jdbcTemplateOds.queryForList(sql, String.class);
			if (result_li != null && result_li.size() > 0) {
				result = result_li.get(0);
			}
		} catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;

	}

	@Override
	public List<String> getFactoringHouseList(String currency, String polNum) throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT FACTORINGHOUSE FROM dbo.V_BANKCODEMASTER AS VBCM, ods.dbo.V_POLICYDETAILS AS VPD "
				+ " WHERE VPD.COMPANY = VBCM.COMPANY AND VPD.CURRENCY = VBCM.CURRENCY AND VPD.CURRENCY = ? "
				+ " AND VPD.POLICYNUMBER = ? ";
		List<String> myList = new ArrayList<String>();
		try {
			myList = this.jdbcTemplateOds.queryForList(sql, new Object[] { currency, polNum }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return myList;
	}

	@Override
	public BankCodeMasterModel getBankCodeMasterByFactHouse(String factHouse) throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT * FROM V_BANKCODEMASTER AS VBCM WHERE VBCM.FACTORINGHOUSE = ?";
		List<BankCodeMasterModel> bankCode_li = new ArrayList<BankCodeMasterModel>();

		try {
			bankCode_li = this.jdbcTemplateOds.query(sql, new Object[] { factHouse },
					new BeanPropertyRowMapper<BankCodeMasterModel>(BankCodeMasterModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return bankCode_li.get(0);
	}

	@Override
	public List<String> getBenefitCodeList(String compCode, String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub
		if (claimTypeUI.trim().equalsIgnoreCase("ADDB")) {
			sql = "select DISTINCT(BENEFITCODE) from dbo.v_addbenefit where COMPONENTCODE = '" + compCode + "'";
		} else {
			sql = "SELECT DISTINCT(BENEFITCODE) FROM dbo.V_BENEFITPLANMASTER where COMPONENTCODE = '" + compCode + "'";
		}
		List<String> benList = new ArrayList<String>();

		try {
			benList = this.jdbcTemplateOds.queryForList(sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return benList;
	}

	@Override
	public List<String> getBenefitNameList(String compCode, String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub

		List<String> benList = new ArrayList<String>();
		if (claimTypeUI.trim().equalsIgnoreCase("ADDB")) {
			sql = "select DISTINCT(BENEFITNAME) from dbo.v_addbenefit where COMPONENTCODE = '" + compCode + "'";
		} else {
			sql = "SELECT DISTINCT(BENEFITNAME) FROM dbo.V_BENEFITPLANMASTER where COMPONENTCODE = '" + compCode + "'";
		}
		try {
			benList = this.jdbcTemplateOds.queryForList(sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return benList;
	}

	@Override
	public ComponentDetailsBO getComponentDetailsBO(String PolicyNo, String ComponentCode, String ClientID,
			String planCode) throws DAOException {
		// TODO Auto-generated method stub
		log.info("ODSDaoImpl - In Method - getwsclRegClaimComponentDetailsBO - PolicyNo - is" + PolicyNo);

		List<ComponentDetailsBO> listMapBnfcDetails = new ArrayList<ComponentDetailsBO>();
		Object[] inputs = new Object[] { PolicyNo, ComponentCode, ClientID, planCode };

		try {
			listMapBnfcDetails = jdbcTemplateOds.query(
					"Select LIFE as Life,COVERAGE as Coverage,RIDER as Rider, COMPONENTRCD, UNIT  from dbo.V_COMPONENTDETAILS where POLICYNUMBER = ? "
							+ "and COMPONENTCODE = ? and CLIENTNUMBER = ? AND PLANCODE = ?",
							inputs, new BeanPropertyRowMapper<ComponentDetailsBO>(ComponentDetailsBO.class));

			log.info(
					"ODSDaoImpl - In Method - getwsclRegClaimComponentDetailsBO - result from databse query result in list of object is"
							+ listMapBnfcDetails);
		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getwsclRegClaimComponentDetailsBO - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		if (Optional.ofNullable(listMapBnfcDetails).isPresent() && !listMapBnfcDetails.isEmpty())
			return listMapBnfcDetails.get(0);
		else
			return null;
	}

	@Override
	public String getBenefitCodeByName(String benefitName, String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub
		if (claimTypeUI.trim().equalsIgnoreCase("ADDB")) {
			sql = "SELECT DISTINCT(BENEFITCODE) FROM dbo.v_addbenefit where BENEFITNAME = ?";
		} else {
			sql = "SELECT DISTINCT(BENEFITCODE) FROM dbo.V_BENEFITPLANMASTER WHERE BENEFITNAME = ?";
		}
		String result = "";
		try {
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { benefitName }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public List<BenefitSummaryModel> getBenefitSummaryResultSet(String benefitCode, String polNum, String planCode,
			Date admissionDate, Date receivedDate) throws DAOException {
		// TODO Auto-generated method stub
		int i = Util.getDiffYears(receivedDate, admissionDate);
		BigDecimal bg = new BigDecimal(i);
		sql = "SELECT TOTALDAYSUSED, TOTALAMOUNTUSED FROM V_BENEFITSUMMARY AS VBS WHERE VBS.BENEFITCODE = ? "
				+ " AND VBS.POLICYNUMBER = ? AND VBS.BENEFITPLAN = ? " + " AND VBS.POLICYYEAR = ? ";
		List<BenefitSummaryModel> benSum_li = new ArrayList<BenefitSummaryModel>();

		try {
			benSum_li = this.jdbcTemplateOds.query(sql, new Object[] { benefitCode, polNum, planCode, bg },
					new BeanPropertyRowMapper<BenefitSummaryModel>(BenefitSummaryModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return benSum_li;
	}

	@Override
	public List<BenefitPlanMaster> getBenefitPlanResultSet(String benefitCode, String planCode,String componentCode) throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT AMOUNTANNUALLY, AMOUNTPERDAY, TOTALNOOFDAYS FROM V_BENEFITPLANMASTER AS VBP WHERE VBP.BENEFITCODE  = ? "
				+ " AND VBP.BENEFITPLAN = ? and VBP.COMPONENTCODE = ? ";

		List<BenefitPlanMaster> benPlan_li = new ArrayList<BenefitPlanMaster>();
		try {
			benPlan_li = this.jdbcTemplateOds.query(sql, new Object[] { benefitCode, planCode, componentCode },
					new BeanPropertyRowMapper<BenefitPlanMaster>(BenefitPlanMaster.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return benPlan_li;
	}

	@Override
	public BigDecimal getBenefitPercentage(String compCode, String benefitCode) throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT BENEFITPERCENTAGE FROM V_ADDBENEFIT AS VAB WHERE VAB.COMPONENTCODE = ? AND VAB.BENEFITCODE = ?";
		BigDecimal result = new BigDecimal(0.0);

		try {
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { compCode, benefitCode }, BigDecimal.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public String getBenefitNameByCode(String benefitCode, String claimTypeUI) throws DAOException {
		// TODO Auto-generated method stub
		if (claimTypeUI.trim().equalsIgnoreCase("ADDB")) {
			sql = "SELECT DISTINCT(BENEFITNAME) FROM dbo.v_addbenefit where BENEFITCODE = ?";
		} else {
			sql = "SELECT DISTINCT(BENEFITNAME) FROM dbo.V_BENEFITPLANMASTER WHERE BENEFITCODE = ?";
		}
		String result = "";
		try {
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { benefitCode }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public List<String> getAdjustmentCodeList() throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT Itemcode FROM dbo.V_ADJUSTMENTCODE";
		List<String> adjCOdelist = new ArrayList<String>();

		try {
			adjCOdelist = this.jdbcTemplateOds.queryForList(sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return adjCOdelist;

	}

	@Override
	public List<String> getRequirementList() throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT DOCID + '-' + SUBCATEGORY FROM dbo.V_REQUIREMENTMASTER where BUSINESSPROCESS = 'CLAIMS' "
				+ " and REQUIREMENTDOCUMENT = 'Y'";
		List<String> reqList = new ArrayList<String>();

		try {
			reqList = this.jdbcTemplateOds.queryForList(sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return reqList;
	}

	@Override
	public ODSRequirementMasterModel getRequirementMaster(String docid) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select DOCID+'-'+SUBCATEGORY as DROPDOWNTEXT,SUBCATEGORY "
				+ " from ODS.dbo.V_REQUIREMENTMASTER where DOCID = ? ";
		List<ODSRequirementMasterModel> reqMaster_li = new ArrayList<ODSRequirementMasterModel>();
		try {
			reqMaster_li = this.jdbcTemplateOds.query(sql, new Object[] { docid },
					new BeanPropertyRowMapper<ODSRequirementMasterModel>(ODSRequirementMasterModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return reqMaster_li.get(0);
	}

	@Override
	public ODSRequirementMasterModel getRequirementMasterbySubCategory(String subcat) throws Exception {
		// TODO Auto-generated method stub
		sql = "SELECT * from ods.dbo.V_REQUIREMENTMASTER WHERE SUBCATEGORY = ?";
		List<ODSRequirementMasterModel> reqMaster_li = new ArrayList<ODSRequirementMasterModel>();
		try {
			reqMaster_li = this.jdbcTemplateOds.query(sql, new Object[] { subcat },
					new BeanPropertyRowMapper<ODSRequirementMasterModel>(ODSRequirementMasterModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		if (reqMaster_li == null || reqMaster_li.size() == 0) {
			throw new Exception("SubCategory for the selected Requirement");
		}

		return reqMaster_li.get(0);

	}

	@Override
	public String getListLifeNameString(String polNum) throws DAOException {
		// TODO Auto-generated method stub
		List<String> result = new ArrayList<String>();
		String resultStr = "";
		try {
			sql = " SELECT VCD.LIFENAME FROM dbo.V_POLICYDETAILS AS VPD, ODS.dbo.V_CLIENTDETAILS AS VCD "
					+ " WHERE VPD.OWNERCLIENTNO = VCD.CLIENTNO AND VPD.POLICYNUMBER = ? ";
			// result = this.jdbcTemplateOds.queryForObject(sql, new Object[] {
			// polNum }, String.class);
			result = this.jdbcTemplateOds.queryForList(sql, new Object[] { polNum }, String.class);
			if (result != null && result.size() > 0) {
				resultStr = result.get(0);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return resultStr;
	}

	@Override
	public String getListSubcategory(String docIdList) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		List<String> result_li = new ArrayList<String>();
		try {
			//			sql = "SELECT STUFF ((SELECT '; ' +RQ1.SUBCATEGORY FROM dbo.V_REQUIREMENTMASTER RQ1 "
			//					+ " WHERE RQ1.DOCID in (" + docIdList + ") ORDER BY RQ1.DOCID FOR XML PATH('')), 1,1,'') ";
			sql = "SELECT STUFF ((select '; ' + RQ1.SUBCATEGORY from dbo.V_REQUIREMENTMASTER RQ1 " +
					"inner join ( " +
					"select id from (values "+docIdList+" ) AS Category(ID) " +
					") ct ON ct.id = rq1.DOCID ORDER BY RQ1.DOCID FOR XML PATH('')), 1,1,'') ";
			// result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			result_li = this.jdbcTemplateOds.queryForList(sql, String.class);
			if (result_li != null && result_li.size() > 0) {
				result = result_li.get(0);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public String getListBenefitName(String benefitCodeList) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		List<String> result_li = new ArrayList<String>();
		try {
			// sql = "SELECT STUFF ((SELECT '; ' + VBP.BENEFITNAME FROM
			// dbo.V_BENEFITPLANMASTER VBP WHERE VBP.BENEFITCODE "
			// + " in ( " + benefitCodeList + " ) ORDER BY VBP.BENEFITCODE FOR
			// XML PATH('')), 1,1,'') ";
			sql = "SELECT STUFF ((SELECT distinct '; ' + VBP.BENEFITNAME FROM ODS.dbo.V_BENEFITPLANMASTER VBP "
					+ " WHERE VBP.BENEFITCODE in (" + benefitCodeList + " )  FOR XML PATH('')), 1,1,'') ";
			// result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			result_li = this.jdbcTemplateOds.queryForList(sql, String.class);
			if (result_li != null && result_li.size() > 0) {
				result = result_li.get(0);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public String getSubCategory(String docid) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sql = "SELECT SUBCATEGORY FROM V_REQUIREMENTMASTER WHERE DOCID = ? AND BUSINESSPROCESS = 'CLAIMS'";
			log.info("getSubCategory sql is -" + sql);
			result = this.jdbcTemplateOds.queryForObject(sql,  new Object[] { docid },
					String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public Date getCompRiskCesDate(String polNum, String componentCode, String clientNumber) throws DAOException {
		// TODO Auto-generated method stub

		Date result = null;
		try {
			sql = "SELECT VCD.COMPRISKCESDATE FROM dbo.V_COMPONENTDETAILS VCD "
					+ " WHERE VCD.POLICYNUMBER = ? AND VCD.COMPONENTCODE = ? AND VCD.CLIENTNUMBER = ? ";
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { polNum, componentCode, clientNumber },
					java.sql.Date.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}
	@Override
	public Date getCOMPPREMCESDATE(String polNum) throws DAOException{
		Date result = null;
		try {
			/*sql = "SELECT vcd.COMPPREMCESDATE FROM V_COMPONENTDETAILS as vcd,"
					+ " V_CLAIMTYPEMAPPING as vctm"
					+ " WHERE vcd.POLICYNUMBER = ?"
					+ "  AND vcd.COMPONENTCODE = vctm.COMPONENTCODE AND vctm.WPFLAG = 'Y'";
			*/
			sql="SELECT vcd.COMPPREMCESDATE FROM V_COMPONENTDETAILS  "
					+ "as vcd, V_CLAIMTYPEMAPPING as vctm, V_POLICYDETAILS VP WHERE vcd.POLICYNUMBER= ? "
					+ "AND vcd.COMPONENTCODE = vctm.COMPONENTCODE AND VCD.POLICYNUMBER = VP.policynumber and  "
					+ "vp.PRODUCTCODE = vctm.PRODUCT AND vctm.WPFLAG = 'Y'";
			
			log.info("getCOMPPREMCESDATE --sql is : "+sql);
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { polNum},
					java.sql.Date.class);
		} 
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}
	@Override
	public Date getPAIDTODATE(String polNum) throws DAOException{
		Date result = null;
		try {
			sql = "SELECT PAIDTODATE FROM V_POLICYDETAILS WHERE POLICYNUMBER= ?";
			log.info("getPAIDTODATE --sql is : "+sql);
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { polNum},
					java.sql.Date.class);
		} 
		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;	
	}
	@Override
	public Beneficiarydetails getTopClntNameNO(String polNo) throws DAOException {
		log.info("getTopClntNameNO b4 sql ");
		List<Beneficiarydetails> bankCode_li = new ArrayList<Beneficiarydetails>();
		try {
			log.info("getTopClntNameNO polno is -" + polNo);
			sql = "SELECT count(*) FROM V_BENEFICIARYDETAILS AS VBD WHERE "
					+ "VBD.POLICYNUMBER = ? and ( CLIENTNAME is not NULL OR CLIENTNO is not NULL)";
			log.info("getTopClntNameNO first sql is  -" + sql);
			int i = 0;
			Integer myInteger = null;
			myInteger = this.jdbcTemplateOds.queryForObject(sql, new Object[] { polNo }, Integer.class);
			i = myInteger.intValue();
			log.info("getTopClntNameNO - after executing first Sql i is = " + String.valueOf(i));

			if (i > 0) {
				log.info("getTopClntNameNO - in if i>0");

				sql = "SELECT TOP 1 CLIENTNAME, CLIENTNO FROM V_BENEFICIARYDETAILS AS VBD WHERE "
						+ "VBD.POLICYNUMBER = ?";
				log.info(" getTopClntNameNO second sql is -" + sql);
				bankCode_li = this.jdbcTemplateOds.query(sql, new Object[] { polNo },
						new BeanPropertyRowMapper<Beneficiarydetails>(Beneficiarydetails.class));
				return bankCode_li.get(0);
			} else {
				return null;
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

	}

	@Override
	public String getAddress2(String BenCnltNo) throws DAOException {
		String result = "";
		try {
			sql = " SELECT ADDRESS2 FROM  V_CLIENTDETAILS AS" + " VCD WHERE VCD.CLIENTNO = ? ";
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { BenCnltNo }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;

	}

	@Override
	public String getAddress3(String BenCnltNo) throws DAOException {
		String result = "";
		try {
			sql = " SELECT ADDRESS3 FROM  V_CLIENTDETAILS AS" + " VCD WHERE VCD.CLIENTNO = ? ";
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { BenCnltNo }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;

	}

	@Override
	public String getAddress4(String BenCnltNo) throws DAOException {
		String result = "";
		try {
			sql = " SELECT ADDRESS4 FROM  V_CLIENTDETAILS AS" + " VCD WHERE VCD.CLIENTNO = ? ";
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { BenCnltNo }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;

	}

	@Override
	public String getAddress5(String BenCnltNo) throws DAOException {
		String result = "";
		try {
			sql = " SELECT ADDRESS5 FROM  V_CLIENTDETAILS AS" + " VCD WHERE VCD.CLIENTNO = ? ";
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { BenCnltNo }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;

	}

	@Override
	public String getAgentEmailAddress(String polNo) throws DAOException {
		String result = "";
		try {
			int i = 0;
			Integer myInteger;
			sql = "SELECT Count(*) FROM V_AGENTDETAILMASTER AS VADM, V_POLICYDETAILS AS VPD "
					+ "WHERE VADM.ROLEID = VPD.AGENTID AND VADM.SUPERVISORID = VPD.AGENTID AND "
					+ "VPD.POLICYNUMBER=? ";
			log.info(" getAgentEmailAddress first sql is - " + sql);
			myInteger = this.jdbcTemplateOds.queryForObject(sql, new Object[] { polNo }, Integer.class);
			i = myInteger.intValue();
			log.info("after first sql  i is - " + String.valueOf(i));
			if (i > 0) {
				sql = "SELECT DISTINCT(VADM.ROLEEMAILADDRESS) FROM V_AGENTDETAILMASTER AS VADM, V_POLICYDETAILS AS VPD "
						+ "WHERE VADM.ROLEID = VPD.AGENTID AND VADM.SUPERVISORID = VPD.AGENTID AND VPD.POLICYNUMBER=? ";
				log.info(" getAgentEmailAddress 2nd sql is -" + sql);
				result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { polNo }, String.class);
				log.info(" getAgentEmailAddressresult is -" + result);
			}

		} catch (DataAccessException e) {

			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public String getSDEmailAddress(String polNo) throws DAOException {
		String result = "";
		try {

			String agentId = "";
			sql = "select PD.AgentID from V_POLICYDETAILS AS PD where PD.POLICYNUMBER = '" + polNo + "'";
			log.info("getAgentID sql is -" + sql);
			agentId = this.jdbcTemplateOds.queryForObject(sql, String.class);
			log.info("getAgentID  is -" + agentId);

			int i = 0;
			Integer myInteger;
			sql = "SELECT Count(*) FROM V_AGENTDETAILMASTER AS VADM WHERE " + " VADM.ROLEID = '" + agentId
					+ "' AND VADM.ROLENAME = 'RM'  ";

			log.info(" getSDEmailAddress first sql is - " + sql);
			myInteger = this.jdbcTemplateOds.queryForObject(sql, Integer.class);
			i = myInteger.intValue();
			log.info("after first sql i is - " + String.valueOf(i));
			if (i > 0) {

				sql = " SELECT DISTINCT(VADM.ROLEEMAILADDRESS) FROM V_AGENTDETAILMASTER AS VADM WHERE "
						+ "VADM.ROLEID = '" + agentId + "' AND VADM.ROLENAME = 'RM'  ";
				log.info("getSDEmailAddress sql is -" + sql);
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
				log.info("getSDEmailAddress result is -" + result);
			}

			log.info("getSDEmailAddress result is -" + result);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public String getNSDEmailAddress(String polNo) throws DAOException {
		String result = "";
		try {
			List<String> result_li = new ArrayList<String>();
			List<String> result_li_agentId = new ArrayList<String>();

			String agentId = "";
			sql = "select PD.AgentID from V_POLICYDETAILS AS PD where PD.POLICYNUMBER = '" + polNo + "'";
			log.info("getAgentID sql is -" + sql);
			agentId = this.jdbcTemplateOds.queryForObject(sql, String.class);
			log.info("getAgentID  is -" + agentId);

			int i = 0;
			Integer myInteger;
			sql = "SELECT Count(*) FROM V_AGENTDETAILMASTER AS VADM WHERE " + " VADM.ROLEID = '" + agentId
					+ "' AND VADM.ROLENAME = 'SD'  ";

			log.info(" getNSDEmailAddress first sql is - " + sql);
			myInteger = this.jdbcTemplateOds.queryForObject(sql, Integer.class);
			i = myInteger.intValue();
			log.info("after first sql i is - " + String.valueOf(i));
			if (i > 0) {

				sql = " SELECT DISTINCT(VADM.ROLEEMAILADDRESS) FROM V_AGENTDETAILMASTER AS VADM WHERE "
						+ "VADM.ROLEID = '" + agentId + "' AND VADM.ROLENAME = 'SD'  ";
				log.info("getNSDEmailAddress sql is -" + sql);
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
				log.info("getNSDEmailAddress result is -" + result);
			}

			log.info("getNSDEmailAddress result is -" + result);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	/*
	 * @Override public CleintDetPolNoBased getCommdetails(String polNo) throws
	 * DAOException { List<CleintDetPolNoBased> cltdetpolno = new
	 * ArrayList<CleintDetPolNoBased>(); try {
	 * 
	 * log.info("getCommdetails - value of PolicyNumber = " + polNo.toString());
	 * sql =
	 * "SELECT COUNT(*) FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD ";
	 * sql = sql +
	 * " WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO"; sql =
	 * sql + " AND (ADDRESS1 is not NULL or ADDRESS2 is not NULL or"; sql = sql
	 * +
	 * " ADDRESS3 is not NULL or ADDRESS4 is not NULL or ADDRESS5 is not NULL";
	 * sql = sql + " or EMAILADDRESS is not NULL or"; sql = sql +
	 * " PHONENO01 is not NULL or PHONENO02 is not NULL or PHONENO03 is not NULL)"
	 * ;
	 * 
	 * log.info("getCommdetails first sql is -"+sql); int i = 0; Integer
	 * myInteger = null;
	 * 
	 * log.info("getCommDetails - sql = " + sql.toString());
	 * 
	 * myInteger =this.jdbcTemplateOds.queryForObject(sql, new Object[]
	 * {polNo},Integer.class); i = myInteger.intValue();
	 * log.info("getCommDetails - After First Sql i is = "+ String.valueOf(i));
	 * 
	 * if (i>0) { log.info("getCommDetails - in i>0"); sql =
	 * "SELECT ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,ADDRESS5,EMAILADDRESS," +
	 * " PHONENO01, PHONENO02, PHONENO03 FROM V_CLIENTDETAILS AS VCD, V_POLICYDETAILS AS VPD"
	 * + " WHERE VPD.POLICYNUMBER = ? AND VPD.OWNERCLIENTNO = VCD.CLIENTNO";
	 * 
	 * log.info("getCommdetails - sql = "+ sql.toString());
	 * 
	 * cltdetpolno = this.jdbcTemplateOds.query(sql, new Object[] { polNo }, new
	 * BeanPropertyRowMapper<CleintDetPolNoBased>(CleintDetPolNoBased.class));
	 * log.info("getCommdetails - after running sql  cltdetpolno is = "+
	 * cltdetpolno); return cltdetpolno.get(0); } else{ return null; } } catch
	 * (DataAccessException e) { // TODO Auto-generated catch block throw new
	 * DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
	 * }
	 * 
	 * 
	 * }
	 */

	@Override
	public String getAddress1(String BenCnltNo) throws DAOException {
		String result = "";
		try {
			sql = " SELECT ADDRESS1 FROM  V_CLIENTDETAILS AS" + " VCD WHERE VCD.CLIENTNO = ? ";
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { BenCnltNo }, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;

	}

	public BenCltAddDetails getAddress(String BenCnltNo) throws DAOException {
		// BenCltAddDetails result = null;
		// sql = "SELECT count(VCD.CLIENTNO) FROM V_CLIENTDETAILS AS VCD WHERE
		// VCD.CLIENTNO = ? "
		// + "and (ADDRESS1 is not NULL or ADDRESS2 is not NULL or ADDRESS3 is
		// not NULL or "
		// + "ADDRESS4 is not NULL or ADDRESS5 is not NULL )";
		// int i = 0;
		// Integer myInteger;
		// log.info("getAddress sql is -" + sql);
		//List<BenCltAddDetails> result = new ArrayList<BenCltAddDetails>();
		BenCltAddDetails result = new BenCltAddDetails();
		// result = null;
		// myInteger = this.jdbcTemplateOds.queryForObject(sql, new Object[] {
		// BenCnltNo }, Integer.class);
		// i = myInteger.intValue();
		// log.info("after first sql i is - " + String.valueOf(i));
		// if (i > 0) {
		/*sql = " SELECT ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,ADDRESS5 " + "FROM  V_CLIENTDETAILS AS"
				+ " VCD WHERE VCD.CLIENTNO = ? ";*/
		sql = " SELECT * FROM  V_CLIENTDETAILS AS"
				+ " VCD WHERE VCD.CLIENTNO = ? ";
		log.info("getAddress sql is -" + sql);

		log.info("getAddress second sql is -" + sql);
		try {
			/*result = this.jdbcTemplateOds.query(sql, new Object[] { BenCnltNo },
					new BeanPropertyRowMapper<BenCltAddDetails>(BenCltAddDetails.class));*/
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { BenCnltNo },
					new BeanPropertyRowMapper<BenCltAddDetails>(BenCltAddDetails.class));
			log.info("getAddress sql result is -" + result);

		}

		catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		/*if (result != null && result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}*/
		if (result != null ) {
			return result;
		} else {
			return null;
		}
		// } else {
		//
		// return null;
		// }
	}

	@Override
	public String getSumAssured(String polNum, String lifeClientNo, String componentCode) throws DAOException {
		String result = "";
		try {
			sql = "select count(SUMASSURED) from V_COMPONENTDETAILS where COMPONENTCODE = ? and POLICYNUMBER = ? "
					+ " and CLIENTNUMBER = ?";
			int i = this.jdbcTemplateOds.queryForObject(sql, new Object[] { componentCode, polNum, lifeClientNo },
					Integer.class);

			if (i > 0) {
				sql = "select distinct SUMASSURED from V_COMPONENTDETAILS where COMPONENTCODE = ? and POLICYNUMBER = ? "
						+ " and CLIENTNUMBER = ?";
				result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { componentCode, polNum, lifeClientNo },
						String.class);
			} else {
				result = "0";
			}

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public BigDecimal getAMOUNTANNUALLY(String Compcode, String benefitcode) throws DAOException {
		BigDecimal result = null;
		try {
			/*
			 * String sql = "SELECT AMOUNTANNUALLY FROM V_BENEFITPLANMASTER" +
			 * " WHERE COMPONENTCODE= ?  ";
			 */
			log.info("ODSDaoImpl - getAMOUNTANNUALLY Compcode,benefitcode is - " + Compcode + " " + benefitcode);

			sql = "SELECT SUM(AMOUNTANNUALLY) FROM V_BENEFITPLANMASTER " + "WHERE COMPONENTCODE = ? and BENEFITPLAN=?";
			log.info("ODSDaoImpl - getAMOUNTANNUALLY sql is - " + sql);
			result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { Compcode, benefitcode }, BigDecimal.class);
			log.info("ODSDaoImpl - getAMOUNTANNUALLY result after firing sql  is - " + result);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}

	/*
	 * @Override public BigDecimal getTOTALAMOUNTUSED(String Compcode, String
	 * Polno, String plancode) throws DAOException { BigDecimal result = null;
	 * try {
	 * 
	 * String sql = "SELECT SUM(TOTALAMOUNTUSED) FROM V_BENEFITSUMMARY GROUP BY"
	 * + " POLICYNUMBER, COMPONENTCODE HAVING " +
	 * "POLICYNUMBER = ?, COMPONENTCODE =?";
	 * 
	 * String sql =
	 * "SELECT SUM(TOTALAMOUNTUSED) AS TOTALAMOUNTUSED FROM V_BENEFITSUMMARY GROUP BY POLICYNUMBER, "
	 * + "COMPONENTCODE, BENEFITPLAN HAVING POLICYNUMBER = ? and " +
	 * "COMPONENTCODE =? and BENEFITPLAN = ?";
	 * 
	 * log.info(" getTOTALAMOUNTUSED sql is " + sql); result =
	 * this.jdbcTemplateOds.queryForObject(sql, new Object[] { Polno, Compcode,
	 * plancode }, BigDecimal.class);
	 * log.info(" getTOTALAMOUNTUSED after sql result is " + result); } catch
	 * (DataAccessException e) { // TODO Auto-generated catch block throw new
	 * DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
	 * } return result; }
	 */

	@Override
	public BigDecimal getTOTALAMOUNTUSED(String Compcode, String Polno, String plancode) throws DAOException {
		BigDecimal result = null;
		try {
			/*
			 * String sql =
			 * "SELECT SUM(TOTALAMOUNTUSED) FROM V_BENEFITSUMMARY GROUP BY" +
			 * " POLICYNUMBER, COMPONENTCODE HAVING " +
			 * "POLICYNUMBER = ?, COMPONENTCODE =?";
			 */
			int i = 0;
			Integer myInteger;
			sql = "SELECT COUNT(*) AS COUNT FROM V_BENEFITSUMMARY WHERE "
					+ "POLICYNUMBER = ? AND COMPONENTCODE =? AND BENEFITPLAN = ? ";
			log.info("first sql is - " + sql);
			myInteger = this.jdbcTemplateOds.queryForObject(sql, new Object[] { Polno, Compcode, plancode },
					Integer.class);
			i = myInteger.intValue();
			log.info("after first sql  i is - " + String.valueOf(i));
			if (i > 0) {
				sql = "SELECT SUM(TOTALAMOUNTUSED) AS TOTALAMOUNTUSED FROM V_BENEFITSUMMARY GROUP BY POLICYNUMBER, "
						+ "COMPONENTCODE, BENEFITPLAN HAVING POLICYNUMBER = ? and "
						+ "COMPONENTCODE =? and BENEFITPLAN = ?";

				log.info(" getTOTALAMOUNTUSED second  sql is " + sql);
				result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { Polno, Compcode, plancode },
						BigDecimal.class);
			} else {
				result = BigDecimal.ZERO;
				log.info("in else");
			}
			log.info(" getTOTALAMOUNTUSED after sql result is " + result.toString());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}

	@Override
	public String getLifefromComponentDetails(String polNum, String clientNo) throws DAOException {
		// TODO Auto-generated method stub
		String resul = "";
		int i = 0;

		sql = "select count(distinct LIFE) from ODS.dbo.V_COMPONENTDETAILS where POLICYNUMBER = '" + polNum
				+ "' and CLIENTNUMBER = '" + clientNo + "' ";
		i = this.jdbcTemplateOds.queryForObject(sql, Integer.class);

		if (i > 0) {

			sql = "select distinct LIFE from ODS.dbo.V_COMPONENTDETAILS where POLICYNUMBER = '" + polNum
					+ "' and CLIENTNUMBER = '" + clientNo + "' ";

			try {
				resul = this.jdbcTemplateOds.queryForObject(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}

		return resul;
	}

	@Override
	public List<ComponentDetailsBO> getComponentDetailsBO(String PolicyNo, String clientNumber) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select * from dbo.V_COMPONENTDETAILS where POLICYNUMBER = '" + PolicyNo + "' and CLIENTNUMBER = '"
				+ clientNumber + "'";
		List<ComponentDetailsBO> compDetails_li = new ArrayList<ComponentDetailsBO>();

		try {
			compDetails_li = this.jdbcTemplateOds.query(sql,
					new BeanPropertyRowMapper<ComponentDetailsBO>(ComponentDetailsBO.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return compDetails_li;
	}

	@Override
	public List<BENEFICIARYLIST> getBENEFICIARYLIST(String polNo) throws DAOException {
		// TODO Auto-generated method stub
		sql = "SELECT CLIENTNO, CLIENTNAME, RELATIONSHIP, PERCENTAGE "
				+ "FROM V_BENEFICIARYDETAILS WHERE POLICYNUMBER = '" + polNo + "'";
		List<BENEFICIARYLIST> ben_list = new ArrayList<BENEFICIARYLIST>();
		log.info("getBENEFICIARYLIST sql is - " + sql);
		try {
			ben_list = this.jdbcTemplateOds.query(sql,
					new BeanPropertyRowMapper<BENEFICIARYLIST>(BENEFICIARYLIST.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return ben_list;
	}

	@Override
	public BENEFICIARYCLIENTINFO getBENEFICIARYCLIENTINFO(String strBeneficiaryClientNo) throws DAOException {

		log.info("ODSDaoImpl - In Method - getBENEFICIARYCLIENTINFO");

		BENEFICIARYCLIENTINFO details = new BENEFICIARYCLIENTINFO();
		details = null;
		try {

			log.info("ODSDaoImpl - In Method - getBENEFICIARYCLIENTINFO sql is -"
					+ "SELECT IDNUMBER, SEX, DOB FROM V_CLIENTDETAILS WHERE CLIENTNO = strBeneficiaryClientNo ");
			details = jdbcTemplateOds.queryForObject(
					"SELECT IDNUMBER, SEX, DOB FROM V_CLIENTDETAILS WHERE CLIENTNO = '" + strBeneficiaryClientNo + "'",
					new BeanPropertyRowMapper<BENEFICIARYCLIENTINFO>(BENEFICIARYCLIENTINFO.class));

		} catch (Exception e) {
			log.error("ODSDaoImpl - Error - getBENEFICIARYCLIENTINFO - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		log.debug("ODSDaoImpl - Return From  Method - getBENEFICIARYCLIENTINFO");
		if (null != details)
			return details;
		else
			return null;
	}

	@Override
	public String getLifenameFromClientDetails(String clientNo) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select LIFENAME from  dbo.V_CLIENTDETAILS where CLIENTNO =  '" + clientNo + "'";
		String result = "";
		try {
			result = this.jdbcTemplateOds.queryForObject(sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}

	@Override
	public List<PolicyRelatedModel> getPolicyRelatedData(String insuredClientID) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select * from dbo.V_POLICYRELATED where LIFECLIENTNO = '" + insuredClientID + "' ";
		List<PolicyRelatedModel> result_li = new ArrayList<PolicyRelatedModel>();

		try {
			result_li = this.jdbcTemplateOds.query(sql,
					new BeanPropertyRowMapper<PolicyRelatedModel>(PolicyRelatedModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result_li;
	}

	@Override
	public List<ClientDetails> getClientDetailsli(String clietnNo) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select * from dbo.V_CLIENTDETAILS where CLIENTNO = '" + clietnNo + "' ";
		List<ClientDetails> clientDetails_li = new ArrayList<ClientDetails>();

		try {
			clientDetails_li = this.jdbcTemplateOds.query(sql,
					new BeanPropertyRowMapper<ClientDetails>(ClientDetails.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return clientDetails_li;
	}

	@Override
	public List<ClientDetails> getClientDetailsli_2(String polNum) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select * from dbo.V_CLIENTDETAILS where CLIENTNO in (select CLIENTNO "
				+ " from dbo.V_BENEFICIARYDETAILS where POLICYNUMBER = '" + polNum + "' )";

		List<ClientDetails> client_li = new ArrayList<ClientDetails>();
		try {
			client_li = this.jdbcTemplateOds.query(sql, new BeanPropertyRowMapper<ClientDetails>(ClientDetails.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return client_li;
	}

	@Override
	public List<ClientDetails> getClientDetailsli_3(String polNum) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select * from dbo.V_CLIENTDETAILS where CLIENTNO in (SELECT LIFECLIENTNO "
				+ " FROM dbo.V_POLICYRELATED WHERE POLICYNUMBER = '" + polNum + "' AND CLIENTROLE = 'LF')";

		List<ClientDetails> client_li = new ArrayList<ClientDetails>();
		try {
			client_li = this.jdbcTemplateOds.query(sql, new BeanPropertyRowMapper<ClientDetails>(ClientDetails.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return client_li;
	}

	@Override
	public String getRelationshipFromBenDetails(String polNum, String clientNo) {
		// TODO Auto-generated method stub
		int i = 0;
		String result = "";
		sql = "select count(RELATIONSHIP) from dbo.V_BENEFICIARYDETAILS where CLIENTNO = '" + clientNo
				+ "' and POLICYNUMBER = '" + polNum + "' ";

		i = this.jdbcTemplateOds.queryForObject(sql, Integer.class);

		if (i > 0) {
			sql = "select RELATIONSHIP from dbo.V_BENEFICIARYDETAILS where CLIENTNO = '" + clientNo
					+ "' and POLICYNUMBER = '" + polNum + "' ";

			try {
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block

			}
		}

		return result;
	}

	@Override
	public String getPercentageFromBenDetails(String polNum, String clientNo) {
		// TODO Auto-generated method stub
		String result = "";
		int i = 0;

		sql = "select count(PERCENTAGE) from dbo.V_BENEFICIARYDETAILS where CLIENTNO = '" + clientNo
				+ "' and POLICYNUMBER = '" + polNum + "' ";
		i = this.jdbcTemplateOds.queryForObject(sql, Integer.class);

		if (i > 0) {

			sql = "select PERCENTAGE from dbo.V_BENEFICIARYDETAILS where CLIENTNO = '" + clientNo
					+ "' and POLICYNUMBER = '" + polNum + "' ";
			try {
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block

			}
		}

		return result;
	}

	@Override
	public List<ClaimHistoryModel> getClaimHistoryData(String polNum) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select * from dbo.V_CLAIMHISTORY WHERE POLICYNUMBER = '" + polNum + "' ";

		List<ClaimHistoryModel> claimHist_li = new ArrayList<ClaimHistoryModel>();

		try {
			claimHist_li = this.jdbcTemplateOds.query(sql,
					new BeanPropertyRowMapper<ClaimHistoryModel>(ClaimHistoryModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return claimHist_li;
	}

	@Override
	public CleintDetPolNoBased getCommdetails(String OwnerClientID) throws DAOException {
		//List<CleintDetPolNoBased> cltdetpolno = new ArrayList<CleintDetPolNoBased>();
		CleintDetPolNoBased cltdetpolno = new CleintDetPolNoBased();
		try {

			// log.info("getCommdetails - val of OwnerClientID = " +
			// OwnerClientID.toString());
			// sql = "SELECT COUNT(CLIENTNO) FROM V_CLIENTDETAILS ";
			// sql = sql + " WHERE CLIENTNO =? ";
			// sql = sql + " AND (ADDRESS1 is not NULL or ADDRESS2 is not NULL
			// or";
			// sql = sql + " ADDRESS3 is not NULL or ADDRESS4 is not NULL or
			// ADDRESS5 is not NULL";
			// sql = sql + " or EMAILADDRESS is not NULL or";
			// sql = sql + " PHONENO01 is not NULL or PHONENO02 is not NULL or
			// PHONENO03 is not NULL)";
			//
			// log.info("getCommdetails first sql is - " + sql.toString());
			// int i = 0;
			// Integer myInteger = null;
			// myInteger = this.jdbcTemplateOds.queryForObject(sql, new Object[]
			// { OwnerClientID }, Integer.class);
			// i = myInteger.intValue();
			// log.info("getCommDetails - after executing first Sql i is = " +
			// String.valueOf(i));

			// if (i > 0) {
			log.info("getCommDetails - in if i>0");
			/*sql = "SELECT ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,ADDRESS5,EMAILADDRESS,"
					+ " PHONENO01, PHONENO02, PHONENO03 FROM V_CLIENTDETAILS " + " WHERE  CLIENTNO = ?";*/
			sql = "SELECT * FROM V_CLIENTDETAILS " + " WHERE  CLIENTNO = ?";

			log.info("getCommdetails - second sql = " + sql.toString());

			/*cltdetpolno = this.jdbcTemplateOds.query(sql, new Object[] { OwnerClientID },
					new BeanPropertyRowMapper<CleintDetPolNoBased>(CleintDetPolNoBased.class));*/
			cltdetpolno = this.jdbcTemplateOds.queryForObject(sql, new Object[] { OwnerClientID },
					new BeanPropertyRowMapper<CleintDetPolNoBased>(CleintDetPolNoBased.class));
			log.info("getCommdetails - after running sql  cltdetpolno is = " + cltdetpolno);
			/*if (cltdetpolno != null && cltdetpolno.size() > 0) {
				return cltdetpolno.get(0);
			} else {
				return null;
			}*/
			if (cltdetpolno != null ) {
				return cltdetpolno;
			} else {
				return null;
			}
			// } else {
			// return null;
			// }
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

	}

	@Override
	public String getOwnerName(String clientNo) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select count(LIFENAME) from V_CLIENTDETAILS where CLIENTNO = '" + clientNo + "'";
		int i = 0;
		String result = "";
		try {
			i = this.jdbcTemplateOds.queryForObject(sql, Integer.class);

			if (i > 0) {
				sql = "select LIFENAME from V_CLIENTDETAILS where CLIENTNO = '" + clientNo + "'";
				result = this.jdbcTemplateOds.queryForObject(sql, String.class);
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}

	@Override
	public List<String> getClientNumberList(String policyNo) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select distinct CLIENTNUMBER from dbo.V_COMPONENTDETAILS where POLICYNUMBER = '" + policyNo + "' ";
		List<String> client_li = new ArrayList<String>();
		try {
			client_li = this.jdbcTemplateOds.queryForList(sql, String.class);
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return client_li;
	}

	@Override
	public String getComponentCodeStatusUserExit(String ClientNo, String PolNo, String CompCode) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		try {

			int i = 0;
			Integer myInteger;
			String sql = "SELECT COUNT(*) FROM V_COMPONENTDETAILS WHERE POLICYNUMBER=? and "
					+ "CLIENTNUMBER =? and COMPONENTCODE = ? and COMPONENTSTATUS is not NULL ";
			log.info("first sql is - " + sql);
			myInteger = this.jdbcTemplateOds.queryForObject(sql, new Object[] { PolNo, ClientNo, CompCode },
					Integer.class);
			i = myInteger.intValue();
			log.info("getComponentCodeStatus after first sql  i is - " + String.valueOf(i));
			if (i > 0) {
				sql = "SELECT COMPONENTSTATUS FROM V_COMPONENTDETAILS WHERE POLICYNUMBER=? and "
						+ "CLIENTNUMBER =? and COMPONENTCODE = ?";
				log.info("getComponentCodeStatus sql is - " + sql);
				result = this.jdbcTemplateOds.queryForObject(sql, new Object[] { PolNo, ClientNo, CompCode },
						String.class);
				log.info("getComponentCodeStatus sql is -" + sql);
			}

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			log.error("getComponentCodeStatus - error ");
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}

		return result;

	}

	@Override
	public PolicyDetails getPolicyDetailsForReport(String PolicyNo) throws DAOException {
		PolicyDetails policyDetails = new PolicyDetails();
		try {
			sql = "Select * from [dbo].[V_POLICYDETAILS] WHERE POLICYNUMBER = ?";
			policyDetails = jdbcTemplateOds.queryForObject(sql,	new Object[] { PolicyNo }, new BeanPropertyRowMapper<PolicyDetails>(PolicyDetails.class));

		} catch (DataAccessException e) {
			log.error("getPolicyDetailsForReport - error ");
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}
		if(policyDetails != null) {
			return policyDetails;	
		} else {
			return null;
		}
	}
	
	@Override
	public List<ProductListData> getProductList() throws DAOException {
		List<ProductListData> productListData = new ArrayList<ProductListData>();
		try {
			this.sql = "SELECT DISTINCT PRODUCTCODE AS strProudctCode, PRODUCTNAME AS strProductName FROM V_PRODUCTCOMPONENT ORDER BY PRODUCTCODE ASC";
			productListData = this.jdbcTemplateOds.query(this.sql, new Object[]{ }, new BeanPropertyRowMapper<ProductListData>(ProductListData.class));
		}catch(Exception e) {
			log.error("ODSDaoImpl - Error - getReportNameList - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return productListData;
	}

	@Override
	public CleintDetPolNoBased getClientDetailsData(String clientNo) {
		List<CleintDetPolNoBased> polNoBased_li = new ArrayList<>();
		String sqlj = "SELECT * FROM V_CLIENTDETAILS WHERE  CLIENTNO = '"+clientNo+"'";
		polNoBased_li = this.jdbcTemplateOds.query(sqlj,new BeanPropertyRowMapper<CleintDetPolNoBased>(CleintDetPolNoBased.class));
		if (polNoBased_li != null && polNoBased_li.size() > 0) {
			return polNoBased_li.get(0);
		}
		 else {
		 	return null;
		}
	}
	
	@Override
	public String getReqDocName(String docId) throws DAOException {
		String docName;
		try {
			this.sql = "SELECT SUBCATEGORY FROM dbo.V_REQUIREMENTMASTER where BUSINESSPROCESS = 'CLAIMS' AND DOCID=?";
			docName = this.jdbcTemplateOds.queryForObject(sql, new Object[] { docId }, String.class);
		}catch(Exception e) {
			log.error("ODSDaoImpl - Error - getReqDocName - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return docName;
	}

}
