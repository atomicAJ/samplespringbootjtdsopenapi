package com.candelalabs.ws.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.candelalabs.epos.model.AdditionalDocument;
import com.candelalabs.epos.model.FundAllocationDB;
import com.candelalabs.epos.model.POSDataSourceCapture;
import com.candelalabs.epos.model.PersonalInfo;
import com.candelalabs.epos.model.PolicyInfo;
import com.candelalabs.epos.model.TransactionCategory;
import com.candelalabs.epos.model.TransactionHistory;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.dao.PosDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.model.tables.POSDataModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.model.tables.RequirementDetailsModel;
import com.candelalabs.ws.util.DateUtil;

@Repository
public class PosDetailsDaoImpl implements PosDetailsDao
{
	private static Log log = LogFactory.getLog(PosDetailsDaoImpl.class);
	@Autowired
	private JdbcTemplate jdbcTemplateClaims;

	@Autowired
	private JdbcTemplate jdbcTemplateOds;

	@Autowired
	private ODSDao odsDao;

	@Autowired
	private ClaimDetailsDao claimsDao;

	@Override
	public POSDataModel getPosDataModel(String policyNumber, String transactionId) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getPosDataModel");
		log.info("PosDetailsDaoImpl - In Method - getPosDataModel for polNo - " + policyNumber);
		log.debug("PosDetailsDaoImpl - In Method - getPosDataModel");
		String posDatasql = "select * from POSDATA p where p.policyNumber=? AND p.transactionId=?";
		try
		{
			return jdbcTemplateClaims.queryForObject(posDatasql, new Object[]
			{ policyNumber, transactionId }, new BeanPropertyRowMapper<POSDataModel>(POSDataModel.class));
		}
		catch (Exception e)
		{
			log.error("Error - getPosDataModel - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + posDatasql);
		}
	}

	public TransactionCategory getTransactionDetails(String tranxId) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getTransactionDetails");
		log.info("PosDetailsDaoImpl - In Method - getTransactionDetails for tranxId - " + tranxId);
		log.debug("PosDetailsDaoImpl - In Method - getTransactionDetails");
		String sql = "SELECT * FROM dbo.TRANSACTIONCATEGORY where TransactionId = ?";

		try
		{
			return this.jdbcTemplateOds.queryForObject(sql, new Object[]
			{ tranxId }, new RowMapper<TransactionCategory>()
			{
				@Override
				public TransactionCategory mapRow(ResultSet rs, int rownum) throws SQLException
				{
					TransactionCategory tranxCategory = new TransactionCategory();
					tranxCategory.setTransactionID(rs.getString("TransactionID"));
					tranxCategory.setCategory(rs.getString("TransactionCategory"));
					tranxCategory.setTransactionCode(rs.getString("TransactionCode"));
					return tranxCategory;
				}
			});
		}
		catch (Exception e)
		{
			log.error("Error - getTransactionDetails - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	public Integer getRBAScore(String policyNumber) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getRBAScore");
		log.info("PosDetailsDaoImpl - In Method - getRBAScore for policyNumber - " + policyNumber);
		log.debug("PosDetailsDaoImpl - In Method - getRBAScore");
		String sql = "SELECT SCORE FROM dbo.V_RBASCORE where policyNumber = ?";
		try
		{
			return this.jdbcTemplateOds.queryForObject(sql, new Object[]
			{ policyNumber }, Integer.class);
		}
		catch (Exception e)
		{
			log.error("Error - getRBAScore - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public ClientDetails getClientDetails(String clientNo) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getClientDetails");
		log.info("PosDetailsDaoImpl - In Method - getClientDetails for clientNo - " + clientNo);
		log.debug("PosDetailsDaoImpl - In Method - getClientDetails");
		try
		{
			return odsDao.getClientDetails(clientNo);
		}
		catch (Exception e)
		{
			log.error("Error - getClientDetails - " + e.getMessage());
			throw new DAOException("Error getting Client Details");
		}
	}

	@Override
	public PolicyDetails getPolicyDetails(String policyNo) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getPolicyDetails");
		log.info("PosDetailsDaoImpl - In Method - getPolicyDetails for policyNumber - " + policyNo);
		log.debug("PosDetailsDaoImpl - In Method - getPolicyDetails");
		try
		{
			return odsDao.getPolicyDetail(policyNo);
		}
		catch (Exception e)
		{
			log.error("Error - getClientDetails - " + e.getMessage());
			throw new DAOException("Error getting Policy Details");
		}
	}

	@Override
	public PersonalInfo getClientPersonalInfo(String clientNo) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getClientPersonalInfo");
		log.info("PosDetailsDaoImpl - In Method - getClientPersonalInfo for clientNo - " + clientNo);
		log.debug("PosDetailsDaoImpl - In Method - getClientPersonalInfo");
		String sql = "select * from V_PERSONALINFO p where p.clientno = ?";
		try
		{
			List<PersonalInfo> personalList = jdbcTemplateOds.query(sql, new Object[]
			{ clientNo }, new RowMapper<PersonalInfo>()
			{

				@Override
				public PersonalInfo mapRow(ResultSet rs, int arg1) throws SQLException
				{
					PersonalInfo p = new PersonalInfo();
					p.setFirstName(rs.getString("firstname"));
					p.setMiddleName(rs.getString("middlename"));
					p.setSurname(rs.getString("surname"));
					p.setIdType(rs.getString("idtype"));
					p.setIdNumber(rs.getString("idnumber"));
					p.setAddress1(rs.getString("address1"));
					p.setAddress2(rs.getString("address2"));
					p.setAddress3(rs.getString("address3"));
					p.setAddress4(rs.getString("address4"));
					p.setAddress5(rs.getString("address5"));
					p.setZipcode(rs.getString("zipcode"));
					p.setCountryCode(rs.getString("countrycode"));
					return p;
				}
			});
			return personalList.isEmpty() ? null : personalList.get(0);
		}
		catch (Exception e)
		{
			log.error("Error - getClientPersonalInfo - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public PolicyInfo getPolicyInfo(String policyNumber) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getPolicyInfo");
		log.info("PosDetailsDaoImpl - In Method - getPolicyInfo for policyNumber - " + policyNumber);
		log.debug("PosDetailsDaoImpl - In Method - getPolicyInfo");
		String sql = "select * from V_POLICYDETAILS p where p.policynumber = ?";
		try
		{
			return jdbcTemplateOds.queryForObject(sql, new Object[]
			{ policyNumber }, new RowMapper<PolicyInfo>()
			{

				@Override
				public PolicyInfo mapRow(ResultSet rs, int arg1) throws SQLException
				{
					PolicyInfo p = new PolicyInfo();
					p.setPaidToDate(rs.getString("paidtodate"));// date
					p.setPaymentFrequency(rs.getString("paymentfreq"));
					p.setPremiumTotal(rs.getString("premiumtotal"));
					p.setCurrentPaymentMethod(rs.getString("PAYMENTMETHOD"));
					return p;
				}
			});
		}
		catch (Exception e)
		{
			log.error("Error - getPolicyInfo - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public List<TransactionHistory> listTransactionHistory(String policyNumber) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - listTransactionHistory");
		log.info("PosDetailsDaoImpl - In Method - listTransactionHistory for policyNumber - " + policyNumber);
		log.debug("PosDetailsDaoImpl - In Method - listTransactionHistory");
		String sql = "select * from POSDATA p where p.policyNumber=? and p.RequestSubmissionTS >= DATEADD(DAY,-14,GETDATE())";
		try
		{
			return jdbcTemplateClaims.query(sql, new Object[]
			{ policyNumber }, new RowMapper<TransactionHistory>()
			{

				@Override
				public TransactionHistory mapRow(ResultSet rs, int rowNum) throws SQLException
				{
					TransactionHistory transactionHistory = new TransactionHistory();
					transactionHistory.setPosRequestNumber(rs.getString("POSRequestNumber"));
					transactionHistory.setChannelSubmission(rs.getString("TransactionSource"));
					transactionHistory.setRequestReceivedDate(DateUtil.formatDate(rs.getTimestamp("ProcessInitiatonTS")));
					transactionHistory.setRequestStatus(rs.getString("POSStatus"));
					transactionHistory.setRequestSubmissionDate(DateUtil.formatDate(rs.getTimestamp("RequestSubmissionTS")));
					transactionHistory.setTypeOfTransaction(rs.getString("TransactionDescription"));
					return transactionHistory;
				}

			});
		}
		catch (Exception e)
		{
			log.error("Error - listTransactionHistory - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public List<String> getPolicyRoles(String clientNumber) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getPolicyRoles");
		log.info("PosDetailsDaoImpl - In Method - getPolicyRoles for clientNumber - " + clientNumber);
		log.debug("PosDetailsDaoImpl - In Method - getPolicyRoles");
		String sql = "select p.policynumber from V_POLICYROLES p where p.clientcode=?";
		try
		{
			return jdbcTemplateOds.queryForList(sql, new Object[]
			{ clientNumber }, String.class);
		}
		catch (Exception e)
		{
			log.error("Error - getPolicyRoles - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public List<AdditionalDocument> listAdditionalDocuments(String posRequestNumber) throws DAOException
	{
		log.debug("EPOSPencilDaoImpl - In Method - listAdditionalDocuments");
		log.info("EPOSPencilDaoImpl - In Method - listAdditionalDocuments for posRequestNumber" + posRequestNumber);
		String sql = "select * from dbo.REQUIREMENTSDETAILS where POSRequestNumber = ?";
		List<RequirementDetailsModel> requirementList = new ArrayList<>();
		try
		{
			requirementList = this.jdbcTemplateClaims.query(sql, new Object[]
			{ posRequestNumber }, new BeanPropertyRowMapper<RequirementDetailsModel>(RequirementDetailsModel.class));
		}
		catch (DataAccessException e)
		{
			log.error("Error - listAdditionalDocuments - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ",ClaimNumber = " + posRequestNumber + ", SQL Query = " + sql);
		}

		log.info("requirementList size:" + requirementList.size());
		List<AdditionalDocument> docList = requirementList.stream().map(req -> {
			try
			{
				ODSRequirementMasterModel requirementMaster = getRequirementMaster(req.getDocID());
				return new AdditionalDocument(requirementMaster.getDROPDOWNTEXT(), requirementMaster.getSUBCATEGORY(), req.getRequirementText(), req.getRequirementNotificationSent());
			}
			catch (Exception e)
			{
				log.error("Unablet to get the ODSRequirementMasterModel ", e);
				return null;
			}
		}).filter(Objects::nonNull).collect(Collectors.toList());
		log.debug("EPOSPencilDaoImpl - Return From  Method - listAdditionalDocuments");
		return docList;
	}

	@Override
	public ODSRequirementMasterModel getRequirementMaster(String docid) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getRequirementMaster");
		log.info("PosDetailsDaoImpl - In Method - getRequirementMaster for docid - " + docid);
		log.debug("PosDetailsDaoImpl - In Method - getRequirementMaster");
		String sql = "select DOCID+'-'+SUBCATEGORY as DROPDOWNTEXT,SUBCATEGORY from V_REQUIREMENTMASTER where DOCID = ? ";
		List<ODSRequirementMasterModel> reqMasterLi = new ArrayList<>();
		try
		{
			reqMasterLi = this.jdbcTemplateOds.query(sql, new Object[]
			{ docid }, new BeanPropertyRowMapper<ODSRequirementMasterModel>(ODSRequirementMasterModel.class));
		}
		catch (DataAccessException e)
		{
			log.error("Error - getRequirementMaster - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return reqMasterLi.get(0);
	}

	@Override
	public int insertPosDataSourceCapture(POSDataSourceCapture posDataSourceCapture) throws DAOException
	{
		int count = 0;
		String sql = "INSERT POSDATASOURCECAPTURE (PolicyNumber,CaseReferenceNumber,TrasactionType,TransactionSource," + "RecordCreationTS,ExceptionRecord,ProcessRecord,RecordProcessedTS,TransactionData," + "ProcessExceptionMessage) VALUES (?,?,?,?,?,?,?,?,?,?)";

		count = jdbcTemplateClaims.update(sql, posDataSourceCapture.getPolicyNumber(), posDataSourceCapture.getCaseReferenceNumber(), posDataSourceCapture.getTransactionType(), posDataSourceCapture.getTransactionSource(), posDataSourceCapture.getRecordCreationTS(), posDataSourceCapture.getExceptionRecord(), posDataSourceCapture.getProcessRecord(), posDataSourceCapture.getRecordProcessedTS(), posDataSourceCapture.getTransactionData(), posDataSourceCapture.getProcessExceptionMessage());
		try
		{

		}
		catch (Exception e)
		{
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	public List<FundAllocationDB> getFundAllocation(String policyNumber) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getFundAllocation");
		log.info("PosDetailsDaoImpl - In Method - getFundAllocation for polNo - " + policyNumber);
		log.debug("PosDetailsDaoImpl - In Method - getRequirementMaster");
		String sql = "select * from V_FUNDALLOCATION where coverage = '01' and policynumber= ? ";
		List<FundAllocationDB> fundAlloctionList = new ArrayList<>();
		try
		{
			fundAlloctionList = this.jdbcTemplateOds.query(sql, new Object[]
			{ policyNumber }, new BeanPropertyRowMapper<FundAllocationDB>(FundAllocationDB.class));
		}
		catch (DataAccessException e)
		{
			log.error("Error - getRequirementMaster - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return fundAlloctionList;
	}

	@Override
	public List<BENEFICIARYLIST> getBenificaryList(String policyNumber) throws DAOException
	{
		log.info("PosDetailsDaoImpl - In Method - getBenificaryList");
		log.info("PosDetailsDaoImpl - In Method - getBenificaryList for polNo - " + policyNumber);
		log.debug("PosDetailsDaoImpl - In Method - getBenificaryList");
		List<BENEFICIARYLIST> benificaryList = new ArrayList<>();
		try
		{
			benificaryList = odsDao.getBENEFICIARYLIST(policyNumber);
		}
		catch (DAOException e)
		{
			log.error("Error - getBenificaryList - " + e.getMessage());
			throw e;
		}
		return benificaryList;
	}
}
