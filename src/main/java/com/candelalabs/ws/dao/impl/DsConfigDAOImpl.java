/**
 * 
 */
package com.candelalabs.ws.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.candelalabs.utilities.model.DsProcessCaseSheetwithTs;
import com.candelalabs.ws.dao.DsConfigDAO;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.WSGENRegClaimBO;

/**
 * @author Triaji
 *
 */
@Repository("dsConfigDao")
public class DsConfigDAOImpl implements DsConfigDAO {

	@Autowired
	JdbcTemplate jdbcTemplateDsConfig;
	private static Log log = LogFactory.getLog("DsConfigDAOImpl");
	@Override
	public List<DsProcessCaseSheetwithTs> getCommentsByActivity(String activityName, String processName,
			String caseReferenceKey) throws DAOException {
		// TODO Auto-generated method stub
		List<DsProcessCaseSheetwithTs> processCaseSheetComments = new ArrayList<DsProcessCaseSheetwithTs>();
		/*	String query = "SELECT TOP 1 DPCS.casesheetid, DPCS.processInstanceName, DPCS.caseReferenceKey, DPCS.comments, "
				+ " DPCS.processCaseSheetInsertionTime, DPCS.processId, DPCS.userName, DPM.activityName "
				+ " FROM DSPROCESSCASESHEET AS DPCS INNER JOIN "
				+ " DSPROCESSMASTER AS DPM ON DPCS.processId=DPM.processid WHERE "
				+ " DPM.activityName=? AND DPM.processName=? AND DPCS.caseReferenceKey=? "
				+ " ORDER BY DPCS.processCaseSheetInsertionTime DESC";*/
		String query = "SELECT TOP 1 DPCS.casesheetid, DPCS.processInstanceName, DPCS.caseReferenceKey, "
				+ "DPCS.comments, "
				+ " DPCS.processCaseSheetInsertionTime, DPCS.processId, DPCS.userName, DPM.activityName "
				+ " FROM DSPROCESSCASESHEET AS DPCS INNER JOIN "
				+ " DSPROCESSMASTER AS DPM ON DPCS.processId=DPM.processid WHERE "
				+ " DPM.activityName=? AND DPCS.caseReferenceKey=? "
				+ " ORDER BY DPCS.processCaseSheetInsertionTime DESC";
		try {	
			processCaseSheetComments = jdbcTemplateDsConfig.query(query,
					new Object[] { activityName, caseReferenceKey },
					new BeanPropertyRowMapper<DsProcessCaseSheetwithTs>(DsProcessCaseSheetwithTs.class));
		} catch (Exception e) {
			throw new DAOException("Exception in querying with query = "+query);
		}
		return processCaseSheetComments;
	}

	@Override
	public List<DsProcessCaseSheetwithTs> getCommentsByActivityAndTag(String activityName, String tagName,
																String caseReferenceKey) throws DAOException {
		// TODO Auto-generated method stub
		List<DsProcessCaseSheetwithTs> processCaseSheetComments = new ArrayList<DsProcessCaseSheetwithTs>();
		/*	String query = "SELECT TOP 1 DPCS.casesheetid, DPCS.processInstanceName, DPCS.caseReferenceKey, DPCS.comments, "
				+ " DPCS.processCaseSheetInsertionTime, DPCS.processId, DPCS.userName, DPM.activityName "
				+ " FROM DSPROCESSCASESHEET AS DPCS INNER JOIN "
				+ " DSPROCESSMASTER AS DPM ON DPCS.processId=DPM.processid WHERE "
				+ " DPM.activityName=? AND DPM.processName=? AND DPCS.caseReferenceKey=? "
				+ " ORDER BY DPCS.processCaseSheetInsertionTime DESC";*/
		String query = "SELECT TOP 1 DPCS.casesheetid, DPCS.processInstanceName, DPCS.caseReferenceKey, DPCS.comments,  " +
				" DPCS.processCaseSheetInsertionTime, DPCS.processId, DPCS.userName, DPM.activityName  " +
				" FROM DSPROCESSCASESHEET AS DPCS " +
				" INNER JOIN  DSPROCESSMASTER AS DPM ON DPCS.processId=DPM.processid " +
				" inner join DSPREDEFINEDTAGMAPPING dspdtm on DPCS.casesheetid = dspdtm.casesheetid " +
				" inner join DSPREDEFINEDTAG DSPDT on dspdtm.TAGID = dspdt.TAGID " +
				" WHERE  " +
				" DPM.activityName=? AND DPCS.caseReferenceKey=?  " +
				" and DSPDT.TAGNAME=? " +
				" ORDER BY DPCS.processCaseSheetInsertionTime DESC ";
		try {
			processCaseSheetComments = jdbcTemplateDsConfig.query(query,
					new Object[] { activityName, caseReferenceKey,tagName },
					new BeanPropertyRowMapper<DsProcessCaseSheetwithTs>(DsProcessCaseSheetwithTs.class));
		} catch (Exception e) {
			throw new DAOException("Exception in querying with query = "+query);
		}
		return processCaseSheetComments;
	}

	@Override
	public String getProcessCaseSheetComments(String processInsName, String searchId, String tagName)
			throws DAOException {
		List<String> comments_li = new ArrayList<String>();
		String comments="";
		String sql = "select TOP 1 dspc.comments from DSPROCESSCASESHEET AS DSPC,"
				+ " DSPREDEFINEDTAG AS DSPDT, DSPREDEFINEDTAGMAPPING AS DSPDTM where "
				+ "dspc.processInstanceName like '%"+processInsName+"' "
				+ "and dspc.searchvalue = '"+searchId+"' and "
				+ "DSPC.casesheetid = dspdtm.casesheetid "
				+ "and dspdtm.TAGID = dspdt.TAGID and "
				+ "dspdt.TAGNAME = '"+tagName+"' "
				+ "order by processCaseSheetInsertionTime desc";
		log.info("getProcessCaseSheetComments sql is -"+ sql);
		try {
			comments_li = jdbcTemplateDsConfig.queryForList(
					sql, String.class);
		} catch (Exception e) {
			log.error(" Error - getProcessCaseSheetComments - " + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to select ProcessCaseSheetComments ");
		}

		if (comments_li != null && comments_li.size() > 0) {
			comments = comments_li.get(0);
		}

		return comments;
	}

	@Override
	public String getReportProcessCaseSheetComments(String caseReferenceKey, String tagName) throws DAOException {
		List<String> comments_li = new ArrayList<String>();
		String comments="";
		String sql = "SELECT TOP 1 DSPC.comments from DSPROCESSCASESHEET AS DSPC,DSPREDEFINEDTAG AS DSPDT, "
				+ "DSPREDEFINEDTAGMAPPING AS DSPDTM WHERE DSPC.caseReferenceKey = '"+caseReferenceKey+"' AND "
				+ "DSPC.casesheetid = DSPDTM.casesheetid AND DSPDTM.TAGID = DSPDT.TAGID AND "
				+ "DSPDT.TAGNAME = '"+tagName+"' ORDER BY processCaseSheetInsertionTime DESC";
		log.info("getProcessCaseSheetComments sql is -"+ sql);
		try {
			comments_li = jdbcTemplateDsConfig.queryForList(
					sql, String.class);
		} catch (Exception e) {
			log.error(" Error - getProcessCaseSheetComments - " + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to select ProcessCaseSheetComments ");
		}

		if (comments_li != null && comments_li.size() > 0) {
			comments = comments_li.get(0);
		}

		return comments;
	}

}
