package com.candelalabs.ws.dao.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.candelalabs.ws.model.tables.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.ClaimDataTATReport;
import com.candelalabs.ws.model.ClaimRecord;
import com.candelalabs.ws.model.JsonWSDULActualAmountProcessorDataIn;
import com.candelalabs.ws.model.JsonWSGenerateFilesDataIn;
import com.candelalabs.api.model.ClaimDataPortalOutput;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.ClaimDataUIClaimDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsAdditionalRequirement;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsBenefitfundDetails;
import com.candelalabs.api.model.JsonWSPolicyDataRequestorDataIn;
import com.candelalabs.api.model.JsonWSRegisterRequirementDataIn;
import com.candelalabs.api.model.ProductListData;
import com.candelalabs.api.model.ReportNameListData;
import com.candelalabs.api.model.ReportTypeListData;
import com.candelalabs.api.model.STPReportData;
import com.candelalabs.api.model.WSGenerateSTPReportDataIn;
//import com.candelalabs.api.model.WSFetchRecordEntriesDataOutClaimRecordList;
//import com.candelalabs.api.model.WSFetchRecordEntriesDataOutClaimRecordListClaimRecord;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.api.model.WSUpdateClaimStatusDataIn;
import com.candelalabs.ws.model.JsonWSSaveClaimUIDataIn;
import com.candelalabs.ws.model.JsonWSUpdateRequirementDataIn;
import com.candelalabs.ws.model.PencilReportRequest;
import com.candelalabs.ws.model.PendingCaseClaimData;
import com.candelalabs.ws.model.PortalsGetClaimDetailsData;
import com.candelalabs.ws.model.ReportDetails;
import com.candelalabs.ws.model.ReqDocList;
import com.candelalabs.ws.model.RequirementsDetails;
import com.candelalabs.ws.model.STPReportDataXls;
import com.candelalabs.ws.model.WSCLRegClaimClaimsDataBOData;
import com.candelalabs.ws.model.WSCLUpdAppClaimStatusClaimsDataBOData;
import com.candelalabs.ws.model.WSDNULRegClaimClaimsDataBOData;
import com.candelalabs.ws.model.WSDNULRegClaimClaimsDetailsBOData;
import com.candelalabs.ws.model.WSDNULUpdClaimRejectStatusClaimsDataBOData;
import com.candelalabs.ws.model.WSDNULUpdClaimRejectStatusClaimsDetailsBOData;
import com.candelalabs.ws.model.WSDULRegClaimClaimsDataBOData;
import com.candelalabs.ws.model.WSDULRegClaimClaimsDetailsBOData;
import com.candelalabs.ws.model.WSGENRegClaimBO;
import com.candelalabs.ws.util.Util;

@Repository("claimDetailsDao")
public class ClaimDetailsDaoImpl implements ClaimDetailsDao {

	private static Log log = LogFactory.getLog("ClaimDetailsDaoImpl");
	public String sql;
	@Autowired
	JdbcTemplate jdbcTemplateClaims;

	// @Autowired
	// ODSDao odsDao;
	@Autowired
	private PlatformTransactionManager transactionManager;

	public static Log getLog() {
		return log;
	}

	public static void setLog(Log log) {
		ClaimDetailsDaoImpl.log = log;
	}

	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public JdbcTemplate getJdbcTemplateClaims() {
		return jdbcTemplateClaims;
	}

	public void setJdbcTemplateClaims(JdbcTemplate jdbcTemplateClaims) {
		this.jdbcTemplateClaims = jdbcTemplateClaims;
	}

	public List<ClaimsData> getPendingTriggerClaims() {
		log.debug("ClaimDetailsDaoImpl - In Method - getPendingTriggerClaims");
		List<ClaimsData> tpaClaimDetails = new ArrayList<ClaimsData>();
		try {
			tpaClaimDetails = jdbcTemplateClaims.query(
					"Select * from dbo.CLAIMS_DATA where ProcessInitiated = 0 OR ProcessInitiated = 9 ",
					new BeanPropertyRowMapper<ClaimsData>(ClaimsData.class));
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendingTriggerClaims - " + e.getMessage());
		}
		log.info("ClaimDetailsDaoImpl - Success - getPendingTriggerClaims - Retreived Trigger Pending Claims");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - getPendingTriggerClaims");
		return tpaClaimDetails;
	}

	public void updatedClaimDetails(ClaimsData tpaClaimDetails) {
		log.debug("ClaimDetailsDaoImpl - In Method - updatedClaimDetails");
		log.info("ClaimDetailsDaoImpl - In Method - updatedClaimDetails Data ClaimsData" + tpaClaimDetails);
		try {
			jdbcTemplateClaims.update("update dbo.CLAIMS_DATA set  ProcessInitiated = ? where ClaimNo = ?",
					tpaClaimDetails.getProcessInitiated(), tpaClaimDetails.getClaimNo());
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - updatedClaimDetails" + e.getMessage());
		}
		log.info("ClaimDetailsDaoImpl - Success - updatedClaimDetails - Updated ClaimDetails");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - updatedClaimDetails");
	}

	/*
	 * public void updatedRemarksInClaimData(ClaimsData tpaClaimDetails) {
	 * log.debug("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData");
	 * log.
	 * info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData Data ClaimsData"
	 * + tpaClaimDetails); try { jdbcTemplateClaims.update(
	 * "update dbo.CLAIMS_DATA set  ClaimCancelRemark = ? , SOURCE_Claim_Status = ? where ClaimNo = ?"
	 * , tpaClaimDetails.getClaimCancelRemark(),
	 * tpaClaimDetails.getSourceClaimStatus(), tpaClaimDetails.getClaimNo()); }
	 * catch (Exception e) {
	 * log.error("ClaimDetailsDaoImpl - Error - updatedRemarksInClaimData" +
	 * e.getMessage()); } log.
	 * info("ClaimDetailsDaoImpl - Success - updatedRemarksInClaimData - Updated ClaimDetails"
	 * ); log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - updatedRemarksInClaimData"
	 * ); }
	 */
	@Override
	public ClaimDataNewModel getClaimData(String claimNo) throws DAOException {
		log.debug("ClaimDetailsDaoImpl - In Method - getPendingTriggerClaims");
		List<ClaimDataNewModel> tpaClaimDetails = new ArrayList<ClaimDataNewModel>();
		try {
			tpaClaimDetails = jdbcTemplateClaims.query(
					"Select * from dbo.CLAIMDATA where ClaimNumber = '" + claimNo + "'",
					new BeanPropertyRowMapper<ClaimDataNewModel>(ClaimDataNewModel.class));
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getClaimData - " + e.getMessage());
			throw new DAOException(e.getMessage());
		}
		log.info("ClaimDetailsDaoImpl - Success - getClaimData - Retreived ClaimData");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - getClaimData");
		if (Optional.ofNullable(tpaClaimDetails).isPresent() && !tpaClaimDetails.isEmpty())
			return tpaClaimDetails.get(0);
		else
			return null;
	}
	@Override
	public POSDataModel getPOSData(String posReqNumber) throws DAOException{
		log.debug("ClaimDetailsDaoImpl - In Method - getPOSData");
		List<POSDataModel> posData = new ArrayList<POSDataModel>();
		try {
			posData = jdbcTemplateClaims.query(
					"Select * from POSDATA where POSRequestNumber = '" + posReqNumber + "'",
					new BeanPropertyRowMapper<POSDataModel>(POSDataModel.class));
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPOSData - " + e.getMessage());
			throw new DAOException(e.getMessage());
		}
		log.info("ClaimDetailsDaoImpl - Success - getPOSData - RetreivedPOSData");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - getPOSData");
		if (Optional.ofNullable(posData).isPresent() && !posData.isEmpty())
			return posData.get(0);
		else
			return null;
	}
	public ClaimsData getClaimDataByTpaClaimNo(String tpaClaimNo) throws Exception {
		log.debug("ClaimDetailsDaoImpl - In Method - getClaimDataByTpaClaimNo");
		List<ClaimsData> tpaClaimDetails = new ArrayList<ClaimsData>();
		try {
			tpaClaimDetails = jdbcTemplateClaims.query(
					"Select * from dbo.CLAIMS_DATA where SOURCE_Claim_ID = '" + tpaClaimNo + "'",
					new BeanPropertyRowMapper<ClaimsData>(ClaimsData.class));
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getClaimDataByTpaClaimNo - " + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(e.getMessage() + "Unable to get Claim data for tpa claim number " + tpaClaimNo);
		}
		log.info("ClaimDetailsDaoImpl - Success - getClaimDataByTpaClaimNo - Retreived Trigger Pending Claims");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - getClaimDataByTpaClaimNo");
		if (Optional.ofNullable(tpaClaimDetails).isPresent() && !tpaClaimDetails.isEmpty())
			return tpaClaimDetails.get(0);
		else
			return null;
	}

	public ClaimsData updatedRequirementProcessor(ClaimsData tpaClaimDetails) {
		log.debug("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData");
		log.info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData Data ClaimsData" + tpaClaimDetails);
		try {
			jdbcTemplateClaims.update(
					"update [dbo].[BENEFICIARY DETAILS] set  CLIENTNO = ? , CLIENTNAME = ? where POLICYNUMBER = ?",
					tpaClaimDetails.getClaimCancelRemark(), tpaClaimDetails.getSourceClaimStatus(),
					tpaClaimDetails.getClaimNo());
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - updatedRemarksInClaimData" + e.getMessage());
		}
		log.info("ClaimDetailsDaoImpl - Success - updatedRemarksInClaimData - Updated ClaimDetails");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - updatedRemarksInClaimData");

		if (null != tpaClaimDetails && !tpaClaimDetails.isEmpty())
			return tpaClaimDetails.get(0);
		else
			return null;
	}

	/*@Override
	public void updatedRemarksInClaimData(WSUpdateClaimStatusDataIn jsonData, String CancelClaimRemark)
			throws DAOException {
		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;
		log.info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData");
		log.info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData Data ClaimsData" + jsonData);

		try {
			log.info(
					"ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData - jsonData.getClaimCancelRemark() is - "
							+ CancelClaimRemark);

			log.info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData - jsonData.getClaimNo() is - "
					+ jsonData.getClaimNo());
			sql = "update CLAIMDATA set ClaimCancelRemark = '" + CancelClaimRemark + "' ";
			sql = sql + "where ClaimNumber = '" + jsonData.getClaimNo().toString() + "'";

			log.info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData - SQL Query = " + sql.toString());

			queryUpdateCount = jdbcTemplateClaims.update(sql);

			/// transactionManager.commit(txStatus);

			sql = "update CLAIMDATA set ClaimStatus = '" + jsonData.getClaimstatus().toString() + "' ";
			sql = sql + "where ClaimNumber = '" + jsonData.getClaimNo().toString() + "'";

			log.info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData - SQL Query = " + sql.toString());

			queryUpdateCount = jdbcTemplateClaims.update(sql);

			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info(
						"ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData - No rows updated for claim number = "
								+ jsonData.getClaimNo().toString());
				throw new DAOException("No Row Updated in Claim Data table, with claimNo= " + jsonData.getClaimNo());
			} else {
				log.info(
						"ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData - Rows updated for claim number = "
								+ jsonData.getClaimNo().toString() + " are = " + queryUpdateCount);
			}

		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - updatedRemarksInClaimData" + e.getMessage());
			throw new DAOException(e.getMessage() + " SQL = " + sql);
		}
		log.info("ClaimDetailsDaoImpl - Success - updatedRemarksInClaimData - Updated ClaimDetails");
		log.info("ClaimDetailsDaoImpl - Return From  Method - updatedRemarksInClaimData");

	}*/

	@Override
	public void updatedRemarksInClaimData(WSUpdateClaimStatusDataIn jsonData, String CancelClaimRemark)
			throws DAOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int registerRequirement(JsonWSRegisterRequirementDataIn jsonData) {

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;
		log.info("ClaimDetailsDaoImpl - In Method - registerRequirement");
		log.info("ClaimDetailsDaoImpl - In Method - registerRequirement " + jsonData);
		/*
		 * sql =
		 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
		 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
		 * +" VALUES ? ,?,)";
		 *
		 */
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try {
			queryUpdateCount = jdbcTemplateClaims.update(
					"INSERT INTO REQUIREMENTSDETAILS(FK_ClaimNo,PolicyNumber ,DocID ,"
							+ "RequirementText,RequirementRequestTS," + "RequirementStatus,RequirementNotificationSent)"
							+ " VALUES (?,?,?,?,CONVERT(DATETIME,?,105),?,?)",
							jsonData.getClaimNo(), jsonData.getPolNo(), jsonData.getDocId(), jsonData.getRequirementText(),
							timestamp, jsonData.getRequirementStatus(), "N");
			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - registerRequirement - "
						+ "No rows inserted for claim number = " + jsonData.getClaimNo().toString());

			} else {
				log.info(
						"ClaimDetailsDaoImpl - In Method - registerRequirement - " + "Rows inserted for claim number = "
								+ jsonData.getClaimNo().toString() + " are = " + queryUpdateCount);
				log.info("ClaimDetailsDaoImpl - Success - registerRequirement - Inserted REQUIREMENTS");
			}

			log.debug("ClaimDetailsDaoImpl - Return From  Method - registerRequirement");
			return queryUpdateCount;
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - registerRequirement" + e.getMessage());
			return queryUpdateCount;
		}

	}

	@Override
	public int updatedRequirement(JsonWSUpdateRequirementDataIn jsonData) {

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;
		log.info("ClaimDetailsDaoImpl - In Method - updatedRequirement");
		log.info("ClaimDetailsDaoImpl - In Method - updatedRequirement " + jsonData);
		try {
			queryUpdateCount = jdbcTemplateClaims.update(
					"update REQUIREMENTS set  " + "RequirementStatus = ? , "
							+ "RequirementReceiveTS = CONVERT(DATETIME,?,105) " + "where FK_ClaimNo = ?",
							jsonData.getrequirementStatus(), jsonData.getrequirementReceiveTS(), jsonData.getClaimNo());
			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - updatedRequirement - "
						+ "No rows updated for claim number = " + jsonData.getClaimNo().toString());

			} else {
				log.info("ClaimDetailsDaoImpl - In Method - updatedRequirement - " + "Rows updated for claim number = "
						+ jsonData.getClaimNo().toString() + " are = " + queryUpdateCount);
				log.info("ClaimDetailsDaoImpl - Success - updatedRequirement - Updated REQUIREMENTS");
			}

			log.debug("ClaimDetailsDaoImpl - Return From  Method - updatedRequirement");
			return queryUpdateCount;
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - updatedRequirement" + e.getMessage());
			return queryUpdateCount;
		}

	}

	/*
	 * public void updatedRegisterclaim(JsonWSLARegisterClaimDataIn jsonData) {
	 *
	 * log.debug("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData");
	 * log.
	 * info("ClaimDetailsDaoImpl - In Method - updatedRemarksInClaimData Data ClaimsData"
	 * +jsonData); try{ jdbcTemplateClaims.
	 * update("update dbo.CLAIMS_DATA set  ClaimCancelRemark = ? , SOURCE_Claim_Status = ? where ClaimNo = ?"
	 * , jsonData.getClaimCancelRemark(),jsonData.getClaimStatus(),jsonData.
	 * getClaimNo ()); }catch(Exception e){
	 * log.error("ClaimDetailsDaoImpl - Error - updatedRemarksInClaimData" +
	 * e.getMessage()); } log.
	 * info("ClaimDetailsDaoImpl - Success - updatedRemarksInClaimData - Updated ClaimDetails"
	 * ); log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - updatedRemarksInClaimData"
	 * );
	 *
	 * }
	 */

	public void gererateTPADuplicateCheck(JsonWSPolicyDataRequestorDataIn jsonData) {

		log.debug("ClaimDetailsDaoImpl - In Method - updatedPolicyDataRequestorData");
		log.info("ClaimDetailsDaoImpl - In Method - updatedPolicyDataRequestor Data ClaimsData" + jsonData);
		/*
		 * try { jdbcTemplateClaims.update(
		 * "update [dbo].[BENEFICIARY DETAILS] set  CLIENTNO = ? , CLIENTNAME = ? where POLICYNUMBER = ?"
		 * , jsonData.getCLIENTNO(), jsonData.getCLIENTNAME(),
		 * jsonData.getPOLICYNUMBER()); } catch (Exception e) {
		 * log.error("ClaimDetailsDaoImpl - Error - updatedRemarksInClaimData" +
		 * e.getMessage()); }
		 */
		log.info("ClaimDetailsDaoImpl - Success - updatedPolicyDataRequestor - Updated ClaimDetails");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - updatedPolicyDataRequestor");

	}

	@Override
	/*
	 * public void
	 * gererateTPADuplicateCheck(JsonINTSRVGenerateTPADuplicateCheckdatain
	 * jsonData) { log.
	 * debug("ClaimDetailsDaoImpl - In Method - updatedPolicyDataRequestorData"
	 * ); log.
	 * info("ClaimDetailsDaoImpl - In Method - updatedPolicyDataRequestor Data ClaimsData"
	 * +jsonData); try{ jdbcTemplateOds.
	 * update("SELECT		POLICYNUMBER,CLIENTNO,CLIENTNAME,RELATIONSHIP,PERCENTAGE, COUNT(1) as CNT\r "
	 * + "FROM		[dbo].[BENEFICIARY DETAILS]\r " +
	 * "GROUP BY	POLICYNUMBER,CLIENTNO,CLIENTNAME,RELATIONSHIP,PERCENTAGE\r "
	 * + "HAVING		COUNT(1) > 1",
	 * jsonData.getPOLICYNUMBER(),jsonData.getCLIENTNO(),jsonData.getCLIENTNAME(
	 * ), jsonData.getRELATIONSHIP(),jsonData.getPERCENTAGE()); }catch(Exception
	 * e){ log.error("ClaimDetailsDaoImpl - Error - updatedRemarksInClaimData" +
	 * e.getMessage()); } log.
	 * info("ClaimDetailsDaoImpl - Success - updatedPolicyDataRequestor - Updated ClaimDetails"
	 * ); log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - updatedPolicyDataRequestor"
	 * );
	 *
	 * }
	 */

	/*
	 * public int
	 * invesCompleteNotification(JsonWSInvesCompleteNotificationDataIn jsonData)
	 * {
	 *
	 * TransactionDefinition txDef = new DefaultTransactionDefinition();
	 * TransactionStatus txStatus = transactionManager.getTransaction(txDef);
	 * int queryUpdateCount = 0;
	 * log.info("ClaimDetailsDaoImpl - In Method - invesCompleteNotification");
	 * log.info("ClaimDetailsDaoImpl - In Method - invesCompleteNotification " +
	 * jsonData);
	 *
	 * sql =
	 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
	 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
	 * +" VALUES ? ,?,)";
	 *
	 * try { queryUpdateCount = jdbcTemplateClaims.update(
	 * "INSERT INTO  NOTIFICATION(ClaimNo," + " NotificationCategory)" +
	 * " VALUES (?,?)", jsonData.getClaimNo(),
	 * jsonData.getNotificationCategory()); transactionManager.commit(txStatus);
	 * if (queryUpdateCount == 0) {
	 * log.info("ClaimDetailsDaoImpl - In Method - invesCompleteNotification - "
	 * + "No rows inserted for claim number = " +
	 * jsonData.getClaimNo().toString());
	 *
	 * } else {
	 * log.info("ClaimDetailsDaoImpl - In Method - invesCompleteNotification - "
	 * + "Rows inserted for claim number = " + jsonData.getClaimNo().toString()
	 * + " are = " + queryUpdateCount); log.
	 * info("ClaimDetailsDaoImpl - Success - invesCompleteNotification - Inserted Notifications"
	 * ); }
	 *
	 * log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - invesCompleteNotification"
	 * ); return queryUpdateCount; } catch (Exception e) {
	 * log.error("ClaimDetailsDaoImpl - Error - invesCompleteNotification" +
	 * e.getMessage()); return queryUpdateCount; }
	 *
	 * }
	 */

	/*
	 * public int custPendingNotification(JsonWSInvesCompleteNotificationDataIn
	 * jsonData) {
	 *
	 * TransactionDefinition txDef = new DefaultTransactionDefinition();
	 * TransactionStatus txStatus = transactionManager.getTransaction(txDef);
	 * int queryUpdateCount = 0;
	 *
	 * log.info("ClaimDetailsDaoImpl - In Method - custPendingNotification");
	 * log.info("ClaimDetailsDaoImpl - In Method - custPendingNotification " +
	 * jsonData);
	 *
	 * sql =
	 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
	 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
	 * +" VALUES ? ,?,)";
	 *
	 * try { queryUpdateCount = jdbcTemplateClaims.update(
	 * "INSERT INTO  NOTIFICATION(ClaimNo," + " NotificationCategory)" +
	 * " VALUES (?,?),(?,?)", jsonData.getClaimNo(), "EMAILONLY",
	 * jsonData.getClaimNo(), "SMSONLY"); transactionManager.commit(txStatus);
	 * if (queryUpdateCount == 0) {
	 * log.info("ClaimDetailsDaoImpl - In Method - custPendingNotification - " +
	 * "No rows inserted for claim number = " +
	 * jsonData.getClaimNo().toString());
	 *
	 * } else {
	 * log.info("ClaimDetailsDaoImpl - In Method - custPendingNotification - " +
	 * "Rows inserted for claim number = " + jsonData.getClaimNo().toString() +
	 * " are = " + queryUpdateCount); log.
	 * info("ClaimDetailsDaoImpl - Success - custPendingNotification - Inserted Notifications"
	 * ); }
	 *
	 * log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - custPendingNotification"
	 * ); return queryUpdateCount; } catch (Exception e) {
	 * log.error("ClaimDetailsDaoImpl - Error - custPendingNotification" +
	 * e.getMessage()); return queryUpdateCount; }
	 *
	 * }
	 */

	public int generateFiles(JsonWSGenerateFilesDataIn jsonData) {

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;

		log.info("ClaimDetailsDaoImpl - In Method - generateFiles");
		log.info("ClaimDetailsDaoImpl - In Method - generateFiles " + jsonData);
		/*
		 * sql =
		 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
		 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
		 * +" VALUES ? ,?,)";
		 */
		try {
			queryUpdateCount = jdbcTemplateClaims.update(
					"INSERT INTO  GENERATEFILEREQUEST(ClaimNo," + " GenerateFile)" + " VALUES (?,?)",
					jsonData.getClaimNo(), jsonData.getGenerateFile());
			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - generateFiles - " + "No rows inserted for claim number = "
						+ jsonData.getClaimNo().toString());

			} else {
				log.info("ClaimDetailsDaoImpl - In Method - generateFiles - " + "Rows inserted for claim number = "
						+ jsonData.getClaimNo().toString() + " are = " + queryUpdateCount);
				log.info("ClaimDetailsDaoImpl - Success - generateFiles - Inserted records");
			}

			log.debug("ClaimDetailsDaoImpl - Return From  Method - generateFiles");
			return queryUpdateCount;
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - generateFiles" + e.getMessage());
			return queryUpdateCount;
		}

	}

	/*
	 * public int dULActualClaimAmount(JsonWSDULActualClaimAmountProcessorDataIn
	 * jsonData, String msgtosend) {
	 *
	 * TransactionDefinition txDef = new DefaultTransactionDefinition();
	 * TransactionStatus txStatus = transactionManager.getTransaction(txDef);
	 * int queryUpdateCount = 0;
	 *
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualClaimAmount");
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualClaimAmount " +
	 * jsonData);
	 *
	 * sql =
	 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
	 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
	 * +" VALUES ? ,?,)";
	 *
	 * Timestamp timestamp = new Timestamp(System.currentTimeMillis()); try {
	 * queryUpdateCount = jdbcTemplateClaims.update(
	 * "INSERT INTO  LAREQUESTTRACKER(claimnumber," +
	 * " activityid,boidentifier,msgcreatets,msgtosend,futuremesg)" +
	 * " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?)", jsonData.getCLAIMNO(),
	 * jsonData.getACITVITYID(), jsonData.getBOIDEN(), timestamp, msgtosend,
	 * jsonData.getFUTUREMSG()); transactionManager.commit(txStatus); if
	 * (queryUpdateCount == 0) {
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualClaimAmount - " +
	 * "No rows inserted for claim number = " +
	 * jsonData.getCLAIMNO().toString());
	 *
	 * } else {
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualClaimAmount - " +
	 * "Rows inserted for claim number = " + jsonData.getCLAIMNO().toString() +
	 * " are = " + queryUpdateCount); log.
	 * info("ClaimDetailsDaoImpl - Success - dULActualClaimAmount - Inserted Notifications"
	 * ); }
	 *
	 * log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - dULActualClaimAmount"
	 * ); return queryUpdateCount; } catch (Exception e) {
	 * log.error("ClaimDetailsDaoImpl - Error - dULActualClaimAmount" +
	 * e.getMessage()); return queryUpdateCount; }
	 *
	 * }
	 */

	// public int
	// dULUpdClaimRejectStatus(JsonWSDULUpdClaimRejectStatusProcessorDataIn
	// jsonData, String msgtosend) {
	/*
	 * public int
	 * dULUpdClaimRejectStatus(JsonWSDULUpdClaimRejectStatusProcessorDataIn
	 * jsonData, String msgtosend) { TransactionDefinition txDef = new
	 * DefaultTransactionDefinition(); TransactionStatus txStatus =
	 * transactionManager.getTransaction(txDef); int queryUpdateCount = 0;
	 *
	 * log.info("ClaimDetailsDaoImpl - In Method - dULUpdClaimRejectStatus");
	 * log.info("ClaimDetailsDaoImpl - In Method - dULUpdClaimRejectStatus " +
	 * jsonData);
	 *
	 * sql =
	 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
	 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
	 * +" VALUES ? ,?,)";
	 *
	 * Timestamp timestamp = new Timestamp(System.currentTimeMillis()); try {
	 * queryUpdateCount = jdbcTemplateClaims.update(
	 * "INSERT INTO  LAREQUESTTRACKER(claimnumber," +
	 * " activityid,boidentifier,msgcreatets,msgtosend,futuremesg)" +
	 * " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?)", jsonData.getCLAIMNO(),
	 * jsonData.getACITVITYID(), jsonData.getBOIDEN(), timestamp, msgtosend,
	 * jsonData.getFUTUREMSG()); transactionManager.commit(txStatus); if
	 * (queryUpdateCount == 0) {
	 * log.info("ClaimDetailsDaoImpl - In Method - dULUpdClaimRejectStatus - " +
	 * "No rows inserted for claim number = " +
	 * jsonData.getCLAIMNO().toString());
	 *
	 * } else {
	 * log.info("ClaimDetailsDaoImpl - In Method - dULUpdClaimRejectStatus - " +
	 * "Rows inserted for claim number = " + jsonData.getCLAIMNO().toString() +
	 * " are = " + queryUpdateCount); log.
	 * info("ClaimDetailsDaoImpl - Success - dULUpdClaimRejectStatus - Inserted Notifications"
	 * ); }
	 *
	 * log.
	 * debug("ClaimDetailsDaoImpl - Return From  Method - dULUpdClaimRejectStatus"
	 * ); return queryUpdateCount; } catch (Exception e) {
	 * log.error("ClaimDetailsDaoImpl - Error - dULUpdClaimRejectStatus" +
	 * e.getMessage()); return queryUpdateCount; }
	 *
	 * }
	 */

	// public int dULActualAmount(JsonWSDULActualAmountProcessorDataIn jsonData,
	// String msgtosend) {
	/*
	 * public int dULActualAmount(JsonWSDULActualAmountProcessorDataIn jsonData,
	 * String msgtosend) { TransactionDefinition txDef = new
	 * DefaultTransactionDefinition(); TransactionStatus txStatus =
	 * transactionManager.getTransaction(txDef); int queryUpdateCount = 0;
	 *
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualAmount");
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualAmount " +
	 * jsonData);
	 *
	 * sql =
	 * "INSERT INTO REQUIREMENTS(FK_ClaimNo,AppNumber,PolicyNumber ,DocID ,RequirementText,RequirementRequestTS,"
	 * + "RequirementReceiveTS,RequirementStatus,RequirementNotification)"
	 * +" VALUES ? ,?,)";
	 *
	 * Timestamp timestamp = new Timestamp(System.currentTimeMillis()); try {
	 * queryUpdateCount = jdbcTemplateClaims.update(
	 * "INSERT INTO  LAREQUESTTRACKER(claimnumber," +
	 * " activityid,boidentifier,msgcreatets,msgtosend,futuremesg)" +
	 * " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?)", jsonData.getCLAIMNO(),
	 * jsonData.getACITVITYID(), jsonData.getBOIDEN(), timestamp, msgtosend,
	 * jsonData.getFUTUREMSG()); transactionManager.commit(txStatus); if
	 * (queryUpdateCount == 0) {
	 * log.info("ClaimDetailsDaoImpl - In Method - dULActualAmount - " +
	 * "No rows inserted for claim number = " +
	 * jsonData.getCLAIMNO().toString());
	 *
	 * } else { log.info("ClaimDetailsDaoImpl - In Method - dULActualAmount - "
	 * + "Rows inserted for claim number = " + jsonData.getCLAIMNO().toString()
	 * + " are = " + queryUpdateCount); log.
	 * info("ClaimDetailsDaoImpl - Success - dULActualAmount - Inserted Notifications"
	 * ); }
	 *
	 * log.debug("ClaimDetailsDaoImpl - Return From  Method - dULActualAmount");
	 * return queryUpdateCount; } catch (Exception e) {
	 * log.error("ClaimDetailsDaoImpl - Error - dULActualAmount" +
	 * e.getMessage()); return queryUpdateCount; }
	 *
	 * }
	 */

	public int lARequestTracker_Insert(String claimnumber, String activityid, String boidentifier, String msgtosend,
			String futuremesg) throws DAOException {

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;

		log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert");
		log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert " + claimnumber + " " + activityid + " "
				+ boidentifier + " " + msgtosend + " " + futuremesg);

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try {
			queryUpdateCount = jdbcTemplateClaims.update(
					"INSERT INTO  LAREQUESTTRACKER(claimnumber,"
							+ " activityid,boidentifier,msgcreatets,msgtosend,futuremesg,flag_processed)"
							+ " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?,?)",
							claimnumber, activityid, boidentifier, timestamp, msgtosend, futuremesg, 0);
			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert - "
						+ "No rows inserted for claim number = " + claimnumber);

			} else {
				log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert - "
						+ "Rows inserted for claim number = " + claimnumber + " are = " + queryUpdateCount);
				log.info("ClaimDetailsDaoImpl - Success - lARequestTracker_Insert - Inserted BO ");
			}

			log.debug("ClaimDetailsDaoImpl - Return From  Method - lARequestTracker_Insert");
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - lARequestTracker_Insert" + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(e.getMessage() + "Unable to insert data into LAREQUESTTRACKER table, SQL = "
					+ "INSERT INTO  LAREQUESTTRACKER(claimnumber,"
					+ " activityid,boidentifier,msgcreatets,msgtosend,futuremesg)"
					+ " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?)");
		}
		return queryUpdateCount;

	}

	@Override
	public void updateClaimUiData(JsonWSSaveClaimUIDataIn jsonWSSaveClaimUIDataIn) throws Exception {
		log.info("ClaimDetailsDaoImpl - In Method - updateClaimUiData");
		log.info("ClaimDetailsDaoImpl - In Method - updateClaimUiData Data ClaimsData" + jsonWSSaveClaimUIDataIn);
		int claimDataUpdatedCount = 0;
		try {

			claimDataUpdatedCount = jdbcTemplateClaims.update(
					" update CLAIMS_DATA set SOURCE_Claim_Type = ?, SOURCE_Plan_ID = ?, SOURCE_Received_Date = ?, "
							+ " SOURCE_Submission_Date = ?, SOURCE_Diagnosis_Code = ?, SOURCE_Admission_Date = ?, SOURCE_Discharge_Date = ?, SOURCE_Provider_Code = ?, "
							+ " SOURCE_Doctor_Name = ?, SOURCE_Amt_Incurred = ?, SOURCE_Currency = ?, SOURCE_Payee_Name = ?, SOURCE_Bank_Name = ?, "
							+ " SOURCE_Bank_Account_Number = ?, SOURCE_Benefit_Code = ? where SOURCE_Claim_ID = ?",
							jsonWSSaveClaimUIDataIn.getClaimType(), jsonWSSaveClaimUIDataIn.getPlanCode(),
							Optional.ofNullable(jsonWSSaveClaimUIDataIn.getClaimReceivedDate()).isPresent()
							? new Date(Long.valueOf(jsonWSSaveClaimUIDataIn.getClaimReceivedDate()))
									: jsonWSSaveClaimUIDataIn.getClaimReceivedDate(),
									Optional.ofNullable(jsonWSSaveClaimUIDataIn.getClaimRegisteredDate()).isPresent()
									? new Date(Long.valueOf(jsonWSSaveClaimUIDataIn.getClaimRegisteredDate()))
											: jsonWSSaveClaimUIDataIn.getClaimRegisteredDate(),
											jsonWSSaveClaimUIDataIn.getDiagnoseCode(),
											Optional.ofNullable(jsonWSSaveClaimUIDataIn.getAdmissionDate()).isPresent()
											? new Date(Long.valueOf(jsonWSSaveClaimUIDataIn.getAdmissionDate()))
													: jsonWSSaveClaimUIDataIn.getAdmissionDate(),
													Optional.ofNullable(jsonWSSaveClaimUIDataIn.getDischargeDate()).isPresent()
													? new Date(Long.valueOf(jsonWSSaveClaimUIDataIn.getDischargeDate()))
															: jsonWSSaveClaimUIDataIn.getDischargeDate(),
															jsonWSSaveClaimUIDataIn.getProvider(), jsonWSSaveClaimUIDataIn.getDoctorName(),
															jsonWSSaveClaimUIDataIn.getInvoiceAmount(), jsonWSSaveClaimUIDataIn.getCurrency(),
															jsonWSSaveClaimUIDataIn.getPayeeName(), jsonWSSaveClaimUIDataIn.getBankAccountName(),
															jsonWSSaveClaimUIDataIn.getBankAccountNumber(), jsonWSSaveClaimUIDataIn.getBenefitCategory(),
															jsonWSSaveClaimUIDataIn.getTpaClaimNumber());

			if (claimDataUpdatedCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - updateClaimUiData - No rows updated for claim number = "
						+ jsonWSSaveClaimUIDataIn.getClaimNumber());
			} else {
				log.info("ClaimDetailsDaoImpl - In Method - updateClaimUiData - " + claimDataUpdatedCount
						+ " Rows updated for claim number = " + jsonWSSaveClaimUIDataIn.getTpaClaimNumber());
			}

		} catch (Exception e) {
			log.error(
					"ClaimDetailsDaoImpl - Error - updateClaimUiData - unable to update ClaimDataUI data into ClaimsData table "
							+ e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to update ClaimDataUI data into ClaimsData table for TPA claim number "
							+ jsonWSSaveClaimUIDataIn.getTpaClaimNumber());
		}

		log.info("ClaimDetailsDaoImpl - Success - updateClaimUiData - Updated ClaimDetails");
		log.info("ClaimDetailsDaoImpl - Return From  Method - updateClaimUiData");

	}

	@Override
	public WSGENRegClaimBO getWSGENRegClaimData(String claimNumber) throws Exception {

		log.debug("ClaimDetailsDaoImpl - In Method - getWSGENRegClaimData");

		List<WSGENRegClaimBO> wsGENRegClaimBO = new ArrayList<WSGENRegClaimBO>();
		Object[] queryParams = new Object[]{claimNumber};
		;

		try {
			wsGENRegClaimBO = jdbcTemplateClaims.query(
					" Select ClaimNo as CLAIMNO, SOURCE_Diagnosis_Code as ZDIAGCDE, SOURCE_Provider_Code as ZMEDPRV, SOURCE_Doctor_Name as ZDOCTOR, "
							+ " SOURCE_Currency as ClaimCur, SOURCE_Payee_Name as PAYCLT, SOURCE_Bank_Account_Number as BANKACOUNT, SOURCE_Bank_Name as BANKACCDSC, "
							+ " SOURCE_Received_Date as ZCLMRECD, SOURCE_Admission_Date as INCURDT, SOURCE_Discharge_Date as DISCHDT, SOURCE_Benefit_Code as HOSBEN "
							+ " from dbo.CLAIMS_DATA where ClaimNo = ?",
							queryParams, new BeanPropertyRowMapper<WSGENRegClaimBO>(WSGENRegClaimBO.class));
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSGENRegClaimData - " + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to get Generic registration claim BO data from FWDeServices DB");
		}

		log.info(
				"ClaimDetailsDaoImpl - Success - getWSGENRegClaimData - Retreived Generic registration claim BO data from FWDeServices DB");
		log.debug("ClaimDetailsDaoImpl - Return From  Method - getWSGENRegClaimData");
		if (Optional.ofNullable(wsGENRegClaimBO).isPresent() && !wsGENRegClaimBO.isEmpty())
			return wsGENRegClaimBO.get(0);
		else
			return null;

	}

	public WSCLRegClaimClaimsDataBOData getWSCLRegClaimClaimsData(String ClaimNo) {
		List<WSCLRegClaimClaimsDataBOData> listMapClaimsData = new ArrayList<WSCLRegClaimClaimsDataBOData>();

		log.info("ClaimDetailsDaoImpl - In Method - getClaimsData - ClaimNo - is" + ClaimNo);
		Object[] inputs = new Object[]{ClaimNo};
		try {
			listMapClaimsData = jdbcTemplateClaims.query(
					"select ClaimNo,SOURCE_Policy_Number as ChdrSel,SOURCE_Plan_ID as CrTable,SOURCE_Member_ID as ClamParty,SOURCE_Claim_Type as ClType,SOURCE_Diagnosis_Code as ZdiagCode,SOURCE_Provider_Code as ZmedPrv,SOURCE_Admission_Date as Datefrom,SOURCE_Received_Date as ZclmRecd,SOURCE_Admission_Date as IncurDt,SOURCE_Discharge_Date as Dischdt,SOURCE_Benefit_Code as Hosben,SOURCE_Admission_Date as DateFrm,SOURCE_Discharge_Date as DateTo,SOURCE_Amt_Incurred as ActExp,SOURCE_Duration_Days as ZnoDay,SOURCE_Amt_Incurred as TactExp \r "
							+ " from CLAIMS_DATA where SOURCE_Claim_ID = ?",
							inputs,
							new BeanPropertyRowMapper<WSCLRegClaimClaimsDataBOData>(WSCLRegClaimClaimsDataBOData.class));

			log.info(
					"ClaimDetailsDaoImpl - In Method - getClaimsData - result from databse query result in list of object is"
							+ listMapClaimsData);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getClaimsData - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapClaimsData).isPresent() && !listMapClaimsData.isEmpty())
			return listMapClaimsData.get(0);
		else
			return null;
	}

	public WSCLUpdAppClaimStatusClaimsDataBOData getWSCLUpdAppClaimStatusClaimsData(String ClaimNo) {
		List<WSCLUpdAppClaimStatusClaimsDataBOData> listMapClaimsDetails = new ArrayList<WSCLUpdAppClaimStatusClaimsDataBOData>();

		log.info("ClaimDetailsDaoImpl - In Method - getWSCLUpdAppClaimStatusClaimsData - ClaimNo - is" + ClaimNo);
		Object[] inputs = new Object[]{ClaimNo};
		try {
			listMapClaimsDetails = jdbcTemplateClaims.query(
					"select ClaimNo,SOURCE_Policy_Number as ChdrSel,SOURCE_Plan_ID as CrTable,SOURCE_Member_ID as ClamParty,SOURCE_Claim_ID as RgpyNum\r "
							+ " from CLAIMS_DATA where SOURCE_Claim_ID = ?",
							inputs, new BeanPropertyRowMapper<WSCLUpdAppClaimStatusClaimsDataBOData>(
									WSCLUpdAppClaimStatusClaimsDataBOData.class));

			log.info(
					"ClaimDetailsDaoImpl - In Method - getWSCLUpdAppClaimStatusClaimsData - result from databse query result in list of object is"
							+ listMapClaimsDetails);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSCLUpdAppClaimStatusClaimsData - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapClaimsDetails).isPresent() && !listMapClaimsDetails.isEmpty())
			return listMapClaimsDetails.get(0);
		else
			return null;
	}

	public WSDNULRegClaimClaimsDataBOData getWSDNULRegClaimClaimsData(String ClaimNo) {
		List<WSDNULRegClaimClaimsDataBOData> listMapClaimsDetails = new ArrayList<WSDNULRegClaimClaimsDataBOData>();

		log.info("ClaimDetailsDaoImpl - In Method - getWSDNULRegClaimClaimsData - ClaimNo - is" + ClaimNo);
		Object[] inputs = new Object[]{ClaimNo};
		try {
			listMapClaimsDetails = jdbcTemplateClaims.query(
					"select ClaimNo,SOURCE_Policy_Number as ChdrSel,SOURCE_Member_ID as ClntNum,SOURCE_Plan_ID as CrTable,SOURCE_Member_ID as ClamParty \r "
							+ " from CLAIMS_DATA where SOURCE_Claim_ID = ?",
							inputs,
							new BeanPropertyRowMapper<WSDNULRegClaimClaimsDataBOData>(WSDNULRegClaimClaimsDataBOData.class));

			log.info(
					"ClaimDetailsDaoImpl - In Method - getWSDNULRegClaimClaimsData - result from databse query result in list of object is"
							+ listMapClaimsDetails);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSDNULRegClaimClaimsData - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapClaimsDetails).isPresent() && !listMapClaimsDetails.isEmpty())
			return listMapClaimsDetails.get(0);
		else
			return null;
	}

	public WSDNULRegClaimClaimsDetailsBOData getWSDNULRegClaimClaimsDetails(String ClaimNo) {
		List<WSDNULRegClaimClaimsDetailsBOData> listMapClaimsDetails = new ArrayList<WSDNULRegClaimClaimsDetailsBOData>();

		log.info("ClaimDetailsDaoImpl - In Method - getWSDNULRegClaimClaimsDetails - ClaimNo - is" + ClaimNo);
		Object[] inputs = new Object[]{ClaimNo};
		try {
			listMapClaimsDetails = jdbcTemplateClaims.query(
					"select OtherAdjst,ReasonCD,CauseOfDth,DtOfDeath \r "
							+ " from CLAIMS_DATA_DETAILS where SOURCE_Claim_ID = ?",
							inputs, new BeanPropertyRowMapper<WSDNULRegClaimClaimsDetailsBOData>(
									WSDNULRegClaimClaimsDetailsBOData.class));

			log.info(
					"ClaimDetailsDaoImpl - In Method - getWSDNULRegClaimClaimsDetails - result from databse query result in list of object is"
							+ listMapClaimsDetails);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSDNULRegClaimClaimsDetails - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapClaimsDetails).isPresent() && !listMapClaimsDetails.isEmpty())
			return listMapClaimsDetails.get(0);
		else
			return null;
	}

	public WSDNULUpdClaimRejectStatusClaimsDataBOData getWSDNULUpdClaimRejectStatusClaimsData(String ClaimNo) {
		List<WSDNULUpdClaimRejectStatusClaimsDataBOData> listMapClaimsDetails = new ArrayList<WSDNULUpdClaimRejectStatusClaimsDataBOData>();

		log.info("ClaimDetailsDaoImpl - In Method - getWSDNULUpdClaimRejectStatusClaimsData - ClaimNo - is" + ClaimNo);
		Object[] inputs = new Object[]{ClaimNo};
		try {
			listMapClaimsDetails = jdbcTemplateClaims.query(
					"select ClaimNo,SOURCE_Policy_Number as ChdrSel \r "
							+ " from CLAIMS_DATA where SOURCE_Claim_ID = ?",
							inputs, new BeanPropertyRowMapper<WSDNULUpdClaimRejectStatusClaimsDataBOData>(
									WSDNULUpdClaimRejectStatusClaimsDataBOData.class));

			log.info(
					"ClaimDetailsDaoImpl - In Method - getWSDNULUpdClaimRejectStatusClaimsData - result from databse query result in list of object is"
							+ listMapClaimsDetails);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSDNULUpdClaimRejectStatusClaimsData - " + e.getMessage());
		}
		if (Optional.ofNullable(listMapClaimsDetails).isPresent() && !listMapClaimsDetails.isEmpty())
			return listMapClaimsDetails.get(0);
		else
			return null;
	}

	public WSDNULUpdClaimRejectStatusClaimsDetailsBOData getWSDNULUpdClaimRejectStatusClaimsDetails(String ClaimNo) {
		List<WSDNULUpdClaimRejectStatusClaimsDetailsBOData> listMapClaimsDetails = new ArrayList<WSDNULUpdClaimRejectStatusClaimsDetailsBOData>();

		log.info("ClaimDetailsDaoImpl - In Method - getWSDNULUpdClaimRejectStatusClaimsDetails - ClaimNo - is"
				+ ClaimNo);
		Object[] inputs = new Object[]{ClaimNo};
		try {
			listMapClaimsDetails = jdbcTemplateClaims.query(
					"select OfCharge,CancelDate \r " + " from CLAIMS_DATA_DETAILS where SOURCE_Claim_ID = ?", inputs,
					new BeanPropertyRowMapper<WSDNULUpdClaimRejectStatusClaimsDetailsBOData>(
							WSDNULUpdClaimRejectStatusClaimsDetailsBOData.class));

			log.info(
					"ClaimDetailsDaoImpl - In Method - getWSDNULUpdClaimRejectStatusClaimsDetails - result from databse query result in list of object is"
							+ listMapClaimsDetails);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSDNULUpdClaimRejectStatusClaimsDetails - " + e.getMessage());
		}

		if (Optional.ofNullable(listMapClaimsDetails).isPresent() && !listMapClaimsDetails.isEmpty())
			return listMapClaimsDetails.get(0);
		else
			return null;
	}

	public String getInsuredClientNo(String PolNo) {
		String insuredClientNo = null;
		String sql = "SELECT insured_client_id FROM CLAIMS_DATA WHERE SOURCE_Policy_Number=?";

		insuredClientNo = (String) jdbcTemplateClaims.queryForObject(sql, new Object[]{PolNo}, String.class);

		return insuredClientNo;
	}

	public List<Map<String, Object>> getNotificationCategory(String notificationDecision) {
		List<Map<String, Object>> notcat = new ArrayList<>();

		String sql = "SELECT NotificationCategory FROM NOTIFICATIONDECISIONMASTER WHERE " + "NotificationDecision=?";

		notcat = jdbcTemplateClaims.queryForList(sql, notificationDecision);

		return notcat;
	}

	public int insertNotification(JsonWSRegisterRequirementDataIn jsonData, String notificationDecision,
			List notificationCategory, String pHEmailAddress, String pHPhoneNumber, String agentEmailAddress,
			String claimUserEmailAddress) {

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int insrtdcnt = 0;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		log.info("ClaimDetailsDaoImpl - In Method - insertNotification");
		log.info("ClaimDetailsDaoImpl - In Method - insertNotification " + jsonData.getClaimNo() + " "
				+ jsonData.getProcessName() + " " + notificationDecision + " "
				+ notificationCategory.get(0).toString().substring(
						(notificationCategory.get(0).toString().indexOf("=") + 1),
						(notificationCategory.get(0).toString().length() - 1))
				+ " " +
				// notificationCategory.get(1).toString().substring((notificationCategory.get(1).toString().indexOf("=")+1),
				// (notificationCategory.get(1).toString().length()-1))+ " " +
				pHEmailAddress + " " + agentEmailAddress + " " + pHPhoneNumber + " " + claimUserEmailAddress);
		// int i = 0;

		if (notificationCategory.size() > 1) {
			/// below for inserting two rows
			sql = "INSERT INTO NOTIFICATION(ClaimNo" + ",ProcessName" + ",NotificationDecision"
					+ ",NotificationCategory" + ",NotificationFlag" + ",COBNotification" + ",NotificationCreateTS"
					+ ",PDFLetterGenerated" + ",EmailSent" + ",UploadtoCasepedia" + ",PHEmailAddress" + ",PHPhoneNumber"
					+ ",AgentEmailAddress" + ",ClaimUserEmailAddress" + ")VALUES" + "(?,?,?,?,'N','N'"
					+ ",CONVERT(DATETIME,?,105),'N','N','N',?,?,?,?" + ")," + "(?,?,?,?,'N','N'"
					+ ",CONVERT(DATETIME,?,105),'N','N','N',?,?,?,?" + ")";

			try {
				insrtdcnt = jdbcTemplateClaims.update(sql, jsonData.getClaimNo(), jsonData.getProcessName(),
						notificationDecision,
						notificationCategory.get(0).toString().substring(
								(notificationCategory.get(0).toString().indexOf("=") + 1),
								(notificationCategory.get(0).toString().length() - 1)),
						timestamp, pHEmailAddress, pHPhoneNumber, agentEmailAddress, claimUserEmailAddress,
						jsonData.getClaimNo(), jsonData.getProcessName(), notificationDecision,
						notificationCategory.get(1).toString().substring(
								(notificationCategory.get(1).toString().indexOf("=") + 1),
								(notificationCategory.get(1).toString().length() - 1)),
						timestamp, pHEmailAddress, pHPhoneNumber, agentEmailAddress, claimUserEmailAddress);
				transactionManager.commit(txStatus);
				if (insrtdcnt == 0) {
					log.info("ClaimDetailsDaoImpl - In Method - insertNotification - "
							+ "No rows inserted for claim number = " + jsonData.getClaimNo());

				} else {
					log.info("ClaimDetailsDaoImpl - In Method - insertNotification - "
							+ "Rows inserted for claim number = " + jsonData.getClaimNo() + " are = " + insrtdcnt);
					log.info("ClaimDetailsDaoImpl - Success - insertNotification - Inserted BO ");
				}

				log.debug("ClaimDetailsDaoImpl - Return From  Method - insertNotification");
			} catch (Exception e) {
				log.error("ClaimDetailsDaoImpl - Error - insertNotification" + e.getMessage());

			}

		} else {
			// below for inserting single row
			sql = "INSERT INTO NOTIFICATION(ClaimNo" + ",ProcessName" + ",NotificationDecision"
					+ ",NotificationCategory" + ",NotificationFlag" + ",COBNotification" + ",NotificationCreateTS"
					+ ",PDFLetterGenerated" + ",EmailSent" + ",UploadtoCasepedia" + ",PHEmailAddress" + ",PHPhoneNumber"
					+ ",AgentEmailAddress" + ",ClaimUserEmailAddress)" + "VALUES					"
					+ "(?,?,?,?,'N','N',CONVERT(DATETIME,?,105),'N','N','N',?,?,?,?)";
			try {
				insrtdcnt = jdbcTemplateClaims.update(sql, jsonData.getClaimNo(), jsonData.getProcessName(),
						notificationDecision,
						notificationCategory.get(0).toString().substring(
								(notificationCategory.get(0).toString().indexOf("=") + 1),
								(notificationCategory.get(0).toString().length() - 1)),
						timestamp, pHEmailAddress, pHPhoneNumber, agentEmailAddress, claimUserEmailAddress);
				transactionManager.commit(txStatus);
				if (insrtdcnt == 0) {
					log.info("ClaimDetailsDaoImpl - In Method - insertNotification - "
							+ "No rows inserted for claim number = " + jsonData.getClaimNo());

				} else {
					log.info("ClaimDetailsDaoImpl - In Method - insertNotification - "
							+ "Rows inserted for claim number = " + jsonData.getClaimNo() + " are = " + insrtdcnt);
					log.info("ClaimDetailsDaoImpl - Success - insertNotification - Inserted BO ");
				}

				log.debug("ClaimDetailsDaoImpl - Return From  Method - insertNotification");
			} catch (Exception e) {
				log.error("ClaimDetailsDaoImpl - Error - insertNotification" + e.getMessage());

			}
		}

		return insrtdcnt;
	}

	@Override
	public int dULActualAmount(JsonWSDULActualAmountProcessorDataIn jsonData, String msgtosend) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getCountMandatoryRequirementTableByClaimTypeUI(String claimTypeUI, String bool) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "select count(*) from dbo.MANDATORYREQUIREMENTS where CLAIMTYPEUI = ? and MANDATORYDOC= ?";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimTypeUI, bool}, Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return count;
	}

	@Override
	public int getClaimListCount(String polNumber) throws DAOException {
		// TODO Auto-generated method stub
		log.info(
				"getClaimListCount sql is SELECT COUNT(*) FROM REPORTDETAILS WHERE PolicyNumber = '" + polNumber + "'");
		String sql = "SELECT COUNT(*) FROM REPORTDETAILS WHERE PolicyNumber = ?";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{polNumber}, Integer.class);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return count;
	}

	@Override
	public int getClaimListCountByClaimNo(String Claimno) throws DAOException {
		// TODO Auto-generated method stub
		log.info("getClaimListCountByClaimNo sql is SELECT COUNT(*) FROM REPORTDETAILS WHERE ClaimNumber'" + Claimno
				+ "'");
		String sql = "SELECT COUNT(*) FROM REPORTDETAILS WHERE ClaimNumber = ?";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{Claimno}, Integer.class);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return count;
	}

	@Override
	public int getClaimListCountWithCompOpr(String Claimno, String compOpr, String polNo) throws DAOException {
		// TODO Auto-generated method stub
		String newClaimNumber = Claimno + compOpr + polNo;
		log.info("getClaimListCountWithCompOpr sql is SELECT COUNT(*) FROM REPORTDETAILS WHERE ClaimNumber ='" + Claimno
				+ "'" + compOpr + "  POLICYNUMBER = '" + polNo + "'");
		String sql = "SELECT COUNT(*) FROM REPORTDETAILS WHERE ClaimNumber = ? " + compOpr + " POLICYNUMBER = ?";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{Claimno, polNo}, Integer.class);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return count;
	}

	@Override
	public int getCountRequiremntDetailTable(String claimTypeUI, String claimNumber, String bool) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT count(DISTINCT DOCID) from REQUIREMENTSDETAILS WHERE CLAIMNUMBER = ?	"
				+ " and DOCID in( SELECT DOCID FROM	MANDATORYREQUIREMENTS WHERE	MANDATORYDOC = ? "
				+ " AND CLAIMTYPEUI = ?)";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber, bool, claimTypeUI},
					Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return count;
	}

	@Override
	// @Transactional(propagation = Propagation.REQUIRED, rollbackFor =
	// Exception.class)
	public int updateReportDetailTable(ReportDetailModel reportDetail) throws Exception {
		// TODO Auto-generated method stub
		// String sql = "UPDATE master.dbo.REPORTDETAILS SET EndTime= ?,
		// UserDecision= ? where ClaimNumber = ? "
		// + "and (EndTime is NULL or EndTime='')";
		String sql = "UPDATE " + "dbo.REPORTDETAILS " + "SET " + "PolicyRequestNumber =(CASE "
				+ "WHEN ? IS NULL then PolicyRequestNumber " + "WHEN ? = '' then PolicyRequestNumber " + "ELSE ? "
				+ "END), " + "  " + "AppNumber =(CASE " + "WHEN ? IS NULL then AppNumber "
				+ "WHEN ? = '' then AppNumber " + "ELSE ? " + "END), " + "  " + "PolicyNumber =(  CASE "
				+ "WHEN ? IS NULL then PolicyNumber " + "WHEN ? = '' then PolicyNumber " + "ELSE ? END ), " + "  "
				+ "ProcessInstanceID =(CASE " + "WHEN ? IS NULL then ProcessInstanceID "
				+ "WHEN ? = '' then ProcessInstanceID " + "ELSE ? END  ), " + "  " + "ProcessName =(CASE "
				+ "WHEN ? IS NULL then ProcessName " + "WHEN ? = '' then ProcessName " + "ELSE ? END), " + "  "
				+ "ActivityType =( CASE " + "WHEN ? IS NULL then ActivityType " + "WHEN ? = '' then ActivityType "
				+ "ELSE ? END), " + "  " + "ActivityName =(  CASE " + "WHEN ? IS NULL then ActivityName "
				+ "WHEN ? = '' then ActivityName " + "ELSE ? END ), " + "  " + " EndTime = ?, " + "  "
				+ "ActionedUser =(  CASE " + "WHEN ? IS NULL then ActionedUser " + "WHEN ? = '' then ActionedUser "
				+ "ELSE ? END ), " + "  " + "UserDecision =( CASE " + "WHEN ? IS NULL then UserDecision "
				+ "WHEN ? = '' then UserDecision " + "ELSE ? END  ) " + "where " + "ClaimNumber = ? "
				+ " and ActivityName = ? and (EndTime is NULL  or EndTime = '') ";
		int i = 0;

		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{reportDetail.getPolicyRequestNumber(),
					reportDetail.getPolicyRequestNumber(), reportDetail.getPolicyRequestNumber(),
					reportDetail.getAppNumber(), reportDetail.getAppNumber(), reportDetail.getAppNumber(),
					reportDetail.getPolicyNumber(), reportDetail.getPolicyNumber(), reportDetail.getPolicyNumber(),
					reportDetail.getProcessInstanceID(), reportDetail.getProcessInstanceID(),
					reportDetail.getProcessInstanceID(), reportDetail.getProcessName(), reportDetail.getProcessName(),
					reportDetail.getProcessName(), reportDetail.getActivityType(), reportDetail.getActivityType(),
					reportDetail.getActivityType(), reportDetail.getActivityName(), reportDetail.getActivityName(),
					reportDetail.getActivityName(), reportDetail.getEndTime(), reportDetail.getActionedUser(),
					reportDetail.getActionedUser(), reportDetail.getActionedUser(), reportDetail.getUserDecision(),
					reportDetail.getUserDecision(), reportDetail.getUserDecision(), reportDetail.getClaimNumber(),
					reportDetail.getActivityName()});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	// @Transactional(propagation = Propagation.REQUIRED, rollbackFor =
	// Exception.class)
	public int insertReportDetailTable(ReportDetailModel reportDetail) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO dbo.REPORTDETAILS (ClaimNumber, PolicyRequestNumber, AppNumber, "
				+ "PolicyNumber, ProcessInstanceID, ProcessName, ActivityType, ActivityName, StartTime, "
				+ "EndTime, ActionedUser, UserDecision) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{reportDetail.getClaimNumber(),
					reportDetail.getPolicyRequestNumber(), reportDetail.getAppNumber(), reportDetail.getPolicyNumber(),
					reportDetail.getProcessInstanceID(), reportDetail.getProcessName(), reportDetail.getActivityType(),
					reportDetail.getActivityName(), reportDetail.getStartTime(), reportDetail.getEndTime(),
					reportDetail.getActionedUser(), reportDetail.getUserDecision()});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public ClaimDataNewModel getClaimDataNew(String claimNo) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM dbo.CLAIMDATA WHERE ClaimNumber = ?";
		log.info("getClaimDataNew(String claimNo) sql is -" + sql);
		ClaimDataNewModel claimData = new ClaimDataNewModel();
		try {
			claimData = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNo},
					new BeanPropertyRowMapper<ClaimDataNewModel>(ClaimDataNewModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",ClaimNumber = " + claimNo + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<ClaimDataDetailsModel> getClaimDataDetailsByClaimNo(String claimNo) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM dbo.CLAIMDATADETAILS WHERE ClaimNumber = ?";
		List<ClaimDataDetailsModel> claimDataDetails = new ArrayList<ClaimDataDetailsModel>();
		try {
			claimDataDetails = this.jdbcTemplateClaims.query(sql, new Object[]{claimNo},
					new BeanPropertyRowMapper<ClaimDataDetailsModel>(ClaimDataDetailsModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",ClaimNumber = " + claimNo + ", SQL Query = " + sql);
		}

		return claimDataDetails;
	}

	@Override
	public List<PortalsGetClaimDetailsData> getClaimDetails(String strOwnerClientID) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT ClaimNumber, PortalClaimRequestNumber, PolicyNumber, ClaimTypeLA, ClaimStatus,  "
				+ "InsuredClientName, "
				+ "InsuredClientID, RegisterDate ,ClaimDecision,ProcessInstanceID "
				+ "FROM CLAIMDATA WHERE OwnerClientID = ? order by Receiveddate desc";
		log.info("getClaimDetails - sql is - " + sql);
		List<PortalsGetClaimDetailsData> claimData = new ArrayList<PortalsGetClaimDetailsData>();
		try {
			claimData = this.jdbcTemplateClaims.query(sql, new Object[]{strOwnerClientID},
					new BeanPropertyRowMapper<PortalsGetClaimDetailsData>(PortalsGetClaimDetailsData.class));

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ",strOwnerClientID = " + strOwnerClientID
					+ ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<ClaimRecord> getClaimList(String PolNumber) throws DAOException {
		/*
		 * String sql =
		 * "SELECT ClaimNumber, PolicyNumber, ActivityName, StartTime, EndTime,ActionedUser, "
		 * + "UserDecision FROM REPORTDETAILS WHERE PolicyNumber = ? order by" +
		 * " PolicyNumber, ClaimNumber, StartTime";
		 */
		String sql = "SELECT DISTINCT(ClaimNumber) " + "FROM REPORTDETAILS WHERE PolicyNumber = ? order by"
				+ " ClaimNumber";
		log.info("getClaimList - sql is - " + sql);
		List<ClaimRecord> claimData = null;
		claimData = new ArrayList<ClaimRecord>();
		try {
			claimData = this.jdbcTemplateClaims.query(sql, new Object[]{PolNumber},
					new BeanPropertyRowMapper<ClaimRecord>(ClaimRecord.class));

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",PolNumber = " + PolNumber + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<ClaimRecord> getClaimListDetails(String PolNumber, String ClaimNo) throws DAOException {
		sql = "SELECT ClaimNumber, PolicyNumber, ActivityName, StartTime, EndTime,ActionedUser, "
				+ "UserDecision FROM REPORTDETAILS WHERE PolicyNumber = ? and ClaimNumber =? order by"
				+ " PolicyNumber, ClaimNumber, StartTime";

		log.info("getClaimListDetails - sql is - " + sql);
		List<ClaimRecord> claimData = null;
		claimData = new ArrayList<ClaimRecord>();
		try {
			claimData = this.jdbcTemplateClaims.query(sql, new Object[]{PolNumber, ClaimNo},
					new BeanPropertyRowMapper<ClaimRecord>(ClaimRecord.class));

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",PolNumber = " + PolNumber + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<ClaimRecord> getClaimListByClaimNo(String claimNumber) throws DAOException {
		String sql = "SELECT ClaimNumber, PolicyNumber, ActivityName, StartTime, EndTime,ActionedUser, UserDecision"
				+ " FROM REPORTDETAILS WHERE ClaimNumber = ? " + "order by ClaimNumber,StartTime";
		log.info("getClaimListByClaimNo - sql is - " + sql);
		List<ClaimRecord> claimData = null;
		claimData = new ArrayList<ClaimRecord>();
		try {
			claimData = this.jdbcTemplateClaims.query(sql, new Object[]{claimNumber},
					new BeanPropertyRowMapper<ClaimRecord>(ClaimRecord.class));

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",claimNumber = " + claimNumber + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<ClaimRecord> getClaimListWithCompOpr(String claimNumber, String compOpr, String polNo)
			throws DAOException {

		sql = "SELECT DISTINCT(ClaimNumber) " + "FROM REPORTDETAILS WHERE  ClaimNumber = '" + claimNumber + "'   "
				+ compOpr + " POLICYNUMBER = '" + polNo + "'  " + "order by ClaimNumber";

		log.info("getClaimListWithCompOpr - sql is - " + sql);

		List<ClaimRecord> claimData = new ArrayList<ClaimRecord>();

		try {

			claimData = jdbcTemplateClaims.query(sql, new BeanPropertyRowMapper<ClaimRecord>(ClaimRecord.class));
			log.info("getClaimListWithCompOpr after executing claimData is - " + claimData.toString());
			log.info("getClaimListWithCompOpr after executing claimData is - " + claimData.get(0).toString());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",claimNumber = " + claimNumber + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<ClaimRecord> getClaimListWithCompOprDetails(String claimNumber, String compOpr, String polNo)
			throws DAOException {

		sql = "SELECT ClaimNumber, PolicyNumber, ActivityName, StartTime, EndTime,ActionedUser, "
				+ "UserDecision FROM REPORTDETAILS WHERE  ClaimNumber = '" + claimNumber + "'   " + compOpr
				+ " POLICYNUMBER = '" + polNo + "'  " + "order by PolicyNumber,ClaimNumber,StartTime";

		log.info("getClaimListWithCompOprDetails - sql is - " + sql);

		List<ClaimRecord> claimData = new ArrayList<ClaimRecord>();

		try {

			claimData = jdbcTemplateClaims.query(
					"SELECT ClaimNumber, PolicyNumber, ActivityName, StartTime, EndTime,ActionedUser, "
							+ "UserDecision FROM REPORTDETAILS WHERE  ClaimNumber = '" + claimNumber + "'   " + compOpr
							+ " POLICYNUMBER = '" + polNo + "'  " + "order by PolicyNumber,ClaimNumber,StartTime",
							new BeanPropertyRowMapper<ClaimRecord>(ClaimRecord.class));
			log.info("getClaimListWithCompOprDetails after executing claimData is - " + claimData.toString());
			log.info("getClaimListWithCompOprDetails after executing claimData is - " + claimData.get(0).toString());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("getClaimListWithCompOprDetails--Error at execution query = " + e + ",claimNumber = "
					+ claimNumber + ", SQL Query = " + sql);
		}

		return claimData;
	}
	/*
	 * @Override public ClaimRecord getClaimListWithCompOprObject(String
	 * claimNumber, String compOpr, String polNo) throws DAOException { sql =
	 * "SELECT ClaimNumber, PolicyNumber, ActivityName, StartTime, EndTime,ActionedUser, "
	 * + "UserDecision FROM REPORTDETAILS WHERE  ClaimNumber = '" + claimNumber
	 * + "'   " + compOpr + " POLICYNUMBER = '" + polNo + "'  " +
	 * "order by PolicyNumber,ClaimNumber,StartTime";
	 *
	 * log.info("getClaimListByClaimNo - sql is - " + sql);
	 *
	 * List<ClaimRecord> claimData = new ArrayList<ClaimRecord>(); // claimData
	 * = new //
	 * ArrayList<WSFetchRecordEntriesDataOutClaimRecordListClaimRecord>(); try {
	 *
	 * claimData = this.jdbcTemplateClaims.query(sql, new Object[] {
	 * claimNumber, polNo}, new BeanPropertyRowMapper<
	 * WSFetchRecordEntriesDataOutClaimRecordListClaimRecord>(
	 * WSFetchRecordEntriesDataOutClaimRecordListClaimRecord.class));
	 *
	 * claimData = jdbcTemplateClaims.query(sql, new
	 * BeanPropertyRowMapper<ClaimRecord>(ClaimRecord.class));
	 * log.info("getClaimListByClaimNo after executing claimData is - " +
	 * claimData.toString()); } catch (DataAccessException e) { // TODO
	 * Auto-generated catch block throw new DAOException(
	 * "Error at execution query = " + e + ",claimNumber = " + claimNumber +
	 * ", SQL Query = " + sql);
	 *
	 *
	 * if (Optional.ofNullable(claimData).isPresent() && !claimData.isEmpty())
	 * return claimData.get(0); else return null; }}
	 */

	/*
	 * @Override public List<ClaimDataPortalOutputDocumentList>
	 * getRequirementDocList(String claimNo) throws DAOException { // TODO
	 * Auto-generated method stub String sql =
	 * "SELECT DocID, PolicyNumber,RequirementStatus FROM REQUIREMENTSDETAILS WHERE ClaimNumber = ?"
	 * ;
	 *
	 * log.info("getRequirementDocList - sql is - " + sql + "claimnumber is - "+
	 * claimNo);
	 *
	 * List<ClaimDataPortalOutputDocumentList> claimData = null; claimData = new
	 * ArrayList<ClaimDataPortalOutputDocumentList>(); try { claimData=
	 * jdbcTemplateClaims.query(
	 * "SELECT DocID, PolicyNumber,RequirementStatus FROM REQUIREMENTSDETAILS WHERE ClaimNumber = '"
	 * +claimNo+"'", new
	 * BeanPropertyRowMapper<ClaimDataPortalOutputDocumentList>(
	 * ClaimDataPortalOutputDocumentList.class)); claimData =
	 * jdbcTemplateClaims.query(sql, new Object[] { claimNo }, new
	 * BeanPropertyRowMapper<ClaimDataPortalOutputDocumentList>(
	 * ClaimDataPortalOutputDocumentList.class));
	 *
	 * } catch (DataAccessException e) { // TODO Auto-generated catch block
	 * throw new
	 * DAOException("getRequirementDocList Error at execution query = " + e +
	 * ",claimNo = " + claimNo + ", SQL Query = " + sql); }
	 *
	 * return claimData; }
	 */
	@Override
	public List<ReqDocList> getRequirementDocList(String claimNo) throws DAOException {
		String sql = "SELECT DocID, PolicyNumber,RequirementStatus FROM REQUIREMENTSDETAILS WHERE ClaimNumber = ?";

		log.info("getRequirementDocList - sql is - " + sql + "claimnumber is - " + claimNo);

		List<ReqDocList> claimData = null;
		claimData = new ArrayList<ReqDocList>();

		try {
			claimData = jdbcTemplateClaims.query(sql, new Object[]{claimNo},
					new BeanPropertyRowMapper<ReqDocList>(ReqDocList.class));

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("getRequirementDocList Error at execution query = " + e + ",claimNo = " + claimNo
					+ ", SQL Query = " + sql);
		}

		return claimData;
	}

	/*
	 * @Override public List<WSPortalServiceDataOutClaimData> getClaimDetails
	 * (String strOwnerClientID)throws DAOException{
	 *
	 * String sql = "SELECT * FROM dbo.CLAIMDATADETAILS WHERE ClaimNumber = ?";
	 * List<WSPortalServiceDataOutClaimData> claimDataDetails = new
	 * ArrayList<WSPortalServiceDataOutClaimData>(); try { claimDataDetails =
	 * this.jdbcTemplateClaims.query(sql, new Object[] { strOwnerClientID }, new
	 * BeanPropertyRowMapper<WSPortalServiceDataOutClaimData>(
	 * WSPortalServiceDataOutClaimData.class));
	 *
	 * } catch (DataAccessException e) { // TODO Auto-generated catch block
	 * throw new DAOException( "Error at execution query = " + e +
	 * ",ClaimNumber = " + strOwnerClientID + ", SQL Query = " + sql); }
	 *
	 * return claimDataDetails; }
	 */
	@Override
	public LaRequestTrackerModel getLaRequestTrackerByClaimNo(String claimNo, String activityId, String boIdentfier)
			throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM dbo.LAREQUESTTRACKER where claimnumber= ? and activityid= ? and boidentifier= ?";
		List<LaRequestTrackerModel> laRequestTrackerModel_li = new ArrayList<LaRequestTrackerModel>();
		LaRequestTrackerModel laRequestTrackerModel = new LaRequestTrackerModel();
		try {
			laRequestTrackerModel_li = this.jdbcTemplateClaims.query(sql,
					new Object[]{claimNo, activityId, boIdentfier},
					new BeanPropertyRowMapper<LaRequestTrackerModel>(LaRequestTrackerModel.class));
			if (laRequestTrackerModel_li.size() > 0) {
				laRequestTrackerModel = laRequestTrackerModel_li.get(0);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",ClaimNumber = " + claimNo + ", SQL Query = " + sql);
		}

		// LaRequestTrackerModel laRequestTrackerModel =
		// this.jdbcTemplateClaims.queryForObject(sql,
		// new Object[] { claimNo, activityId, boIdentfier },
		// new
		// BeanPropertyRowMapper<LaRequestTrackerModel>(LaRequestTrackerModel.class));
		return laRequestTrackerModel;
	}

	public WSDULRegClaimClaimsDataBOData getWSDULClaimDetailsByClaimNo(String claimNo) {
		log.info("ClaimDetailsDaoImpl - In Method - getDULClaimDetailsByClaimNo - ClaimNo - is" + claimNo);
		Object[] claimNumber = new Object[]{claimNo};
		List<WSDULRegClaimClaimsDataBOData> dulClaimDetails = new ArrayList<WSDULRegClaimClaimsDataBOData>();
		try {
			dulClaimDetails = jdbcTemplateClaims.query(
					"Select ClaimNo,SOURCE_Policy_Number as ChdrSel,SOURCE_Member_ID as ClntNum,SOURCE_Received_Date as ZCLMRECD,SOURCE_Benefit_Code as ZCATCLMBEN,SOURCE_Submission_Date as EFDATE from CLAIMS_DATA where SOURCE_Claim_ID = ?",
					claimNumber,
					new BeanPropertyRowMapper<WSDULRegClaimClaimsDataBOData>(WSDULRegClaimClaimsDataBOData.class));
			log.info(
					"ClaimDetailsDaoImpl - In Method - getDULClaimDetailsByClaimNo - result from databse query result in list of object is"
							+ dulClaimDetails);

		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getDULClaimDetailsByClaimNo - " + e.getMessage());
		}
		if (Optional.ofNullable(dulClaimDetails).isPresent() && !dulClaimDetails.isEmpty())
			return dulClaimDetails.get(0);
		else
			return null;
	}

	public WSDULRegClaimClaimsDetailsBOData getWSDULClaimClaimsDetailsByClaimNo(String claimNo) {
		log.info("ClaimDetailsDaoImpl - In Method - getWSDULClaimClaimsDetailsByClaimNo - ClaimNo - is" + claimNo);
		Object[] claimNumber = new Object[]{claimNo};
		List<WSDULRegClaimClaimsDetailsBOData> dulClaimDetails = new ArrayList<WSDULRegClaimClaimsDetailsBOData>();
		try {
			dulClaimDetails = jdbcTemplateClaims.query(
					"select OtherAdjst,CauseOfDth,DtOfDeath from CLAIMS_DATA_DETAILS where SOURCE_Claim_ID = ?",
					claimNumber, new BeanPropertyRowMapper<WSDULRegClaimClaimsDetailsBOData>(
							WSDULRegClaimClaimsDetailsBOData.class));
			log.info(
					"ClaimDetailsDaoImpl - In Method - getWSDULClaimClaimsDetailsByClaimNo - result from databse query result in list of object is"
							+ dulClaimDetails);

		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getWSDULClaimClaimsDetailsByClaimNo - " + e.getMessage());
		}
		if (Optional.ofNullable(dulClaimDetails).isPresent() && !dulClaimDetails.isEmpty())
			return dulClaimDetails.get(0);
		else
			return null;
	}

	@Override
	public List<NotificationDecisionMasterModel> getNotifCategoryResults(String decision) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT NotificationCategory FROM NOTIFICATIONDECISIONMASTER WHERE NotificationDecision = ?";
		List<NotificationDecisionMasterModel> notifResult_li = new ArrayList<NotificationDecisionMasterModel>();
		try {
			notifResult_li = jdbcTemplateClaims.query(sql, new Object[]{decision},
					new BeanPropertyRowMapper<NotificationDecisionMasterModel>(NotificationDecisionMasterModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return notifResult_li;
	}

	@Override
	public Integer getSMSTemplateIdByDecision(String decision) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT SMSTemplateID FROM NOTIFICATIONSMSMASTER WHERE NotificationDecision = ?";
		Integer smsTemplateId = null;
		List<Integer> smsTemplateId_li = new ArrayList<Integer>();
		try {
			smsTemplateId_li = this.jdbcTemplateClaims.queryForList(sql, new Object[]{decision}, Integer.class);
			if (smsTemplateId_li != null && smsTemplateId_li.size() > 0) {
				smsTemplateId = smsTemplateId_li.get(0);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return smsTemplateId;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertNotificationTable(String type, String claimNumber, String processName, String decision,
			String notifCategory, int templateId, String notifFlag, String pdfLetterGen, String emailSent,
			String uploadToCase, String smsCreated) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO NOTIFICATION(ClaimNumber,ProcessName,NotificationDecision,NotificationCategory,"
				+ type
				+ ", NotificationCreateTS, NotificationFlag, PDFLetterGenerated, EmailSent, UploadToCasepedia, SMSCreated) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?) ";
		Timestamp curTS = new Timestamp(System.currentTimeMillis());
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, processName, decision, notifCategory,
					templateId, curTS, notifFlag, pdfLetterGen, emailSent, uploadToCase, smsCreated});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception(
					"Error at execution query = " + e + ",ClaimNumber = " + claimNumber + ", SQL Query = " + sql);
		}

		return i;

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertNotificationTable2(String type, String claimNumber, String processName, String decision,
			String notifCategory, int templateId, String notifFlag, String pdfLetterGen, String emailSent,
			String uploadToCase, String smsCreated, String procCategory
			,String ApplicationNumber, String PolicyNumber) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO NOTIFICATION"
				+ "(ClaimNumber,ApplicationNumber,PolicyNumber,ProcessCategory,ProcessName,NotificationDecision,NotificationCategory,"
				+ type
				+ ", NotificationCreateTS, NotificationFlag, PDFLetterGenerated, EmailSent, LetterUploadToECM, SMSCreated) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		Timestamp curTS = new Timestamp(System.currentTimeMillis());
		int i = 0;

		i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber,ApplicationNumber,PolicyNumber, procCategory, processName, decision,
				notifCategory, templateId, curTS, notifFlag, pdfLetterGen, emailSent, uploadToCase, smsCreated});

		return i;

	}

	@Override
	public Integer getEmailTemplateIdByDecision(String decision, int emailTemplateFlag) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT EmailTemplateID FROM NOTIFICATIONEMAILMASTER WHERE NOTIFICATIONDECISION = ? AND EmailTemplateFlag = "
				+ emailTemplateFlag;
		Integer mailTemplateId = null;
		List<Integer> mailTemplateId_li = new ArrayList<Integer>();
		try {
			mailTemplateId_li = this.jdbcTemplateClaims.queryForList(sql, new Object[]{decision}, Integer.class);
			if (mailTemplateId_li != null && mailTemplateId_li.size() > 0) {
				mailTemplateId = mailTemplateId_li.get(0);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return mailTemplateId;
	}

	@Override
	public List<Integer> getLetterTemplateId(String decision, ClaimDataNewModel claimData, String COBLetter)
			throws DAOException {
		// TODO Auto-generated method stub
		List<Integer> i = new ArrayList<Integer>();
		if (claimData.getFullyClaimed() == null) {
			String sql = "SELECT LetterTemplateID FROM NOTIFICATIONLETTERMASTER WHERE NotificationDecision = ? AND ClaimTypeUI = ? "
					+ " AND ComponentCode = ? AND FullyPaidFlag IS NULL AND COBLetter = '" + COBLetter + "'";
			try {
				i = this.jdbcTemplateClaims.queryForList(sql,
						new Object[]{decision, claimData.getClaimTypeUI(), claimData.getComponentCode()},
						Integer.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else {
			String sql = "SELECT LetterTemplateID FROM NOTIFICATIONLETTERMASTER WHERE NotificationDecision = ? AND ClaimTypeUI = ? "
					+ " AND ComponentCode = ? AND FullyPaidFlag = ? AND COBLetter = 'N'";
			try {
				i = this.jdbcTemplateClaims.queryForList(sql, new Object[]{decision, claimData.getClaimTypeUI(),
						claimData.getComponentCode(), claimData.getFullyClaimed()}, Integer.class);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertNotificationTable(String claimNumber, String processName, String decision, String notifCategory,
			int mailTemplateId, int letterTemplateId, String notifFlag, String pdfLetterGen, String emailSent,
			String uploadToCase, String smsCreated) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO NOTIFICATION(ClaimNumber,ProcessName,NotificationDecision,NotificationCategory,LetterTemplateID,"
				+ " EmailTemplateID,NotificationCreateTS, NotificationFlag, PDFLetterGenerated, EmailSent, UploadToCasepedia, "
				+ " SMSCreated) " + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ";
		int i = 0;
		Timestamp curTs = new Timestamp(System.currentTimeMillis());
		try {
			i = this.jdbcTemplateClaims.update(sql,
					new Object[]{claimNumber, processName, decision, notifCategory, letterTemplateId, mailTemplateId,
							curTs, notifFlag, pdfLetterGen, emailSent, uploadToCase, smsCreated});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int countCOBRequestByClaimNumber(String claimNumber) throws DAOException {
		// TODO Auto-generated method stub
		int i = 0;
		String sql = "SELECT COUNT(*) FROM COBREQUEST WHERE CLAIMNUMBER = ? AND Processed = 0";
		try {
			i = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber}, Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int countCOBRequestByClaimNumber(String claimNumber, String policyNumber) throws DAOException {
		// TODO Auto-generated method stub
		int i = 0;
		String sql = "SELECT COUNT(*) FROM COBREQUEST WHERE CLAIMNUMBER = ? and PolicyNumber = ?";
		try {
			i = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber, policyNumber}, Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateCOBRequest(String claimNumber, int processed, String processedRemarks) throws Exception {
		// TODO Auto-generated method stub
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String sql = "UPDATE COBREQUEST set Processed = ? , RequestProcessedTS = ?, " + " ProcessedRemarks = ? "
				+ " WHERE CLAIMNUMBER = ? AND Processed = 0";
		int i = 0;

		i = this.jdbcTemplateClaims.update(sql, new Object[]{processed, timestamp, processedRemarks, claimNumber});

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateSTPStatus(String claimNumber, int STPStatus) throws Exception {
		// TODO Auto-generated method stub

		sql = "UPDATE CLAIMDATA SET ClaimSTPFlag = ? WHERE ClaimNumber = ? ";
		log.info("updateSTPStatus sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{STPStatus, claimNumber});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}
	@Override
	public int updateAgentEmailAddr(String agentEmailAddr,String claimNumber) throws Exception{
		sql = "UPDATE NOTIFICATIONDATA SET AgentEmailAddress = ? WHERE ClaimNumber = ?";
		log.info("updateAgentEmailAddr sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{agentEmailAddr, claimNumber});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("updateAgentEmailAddr--Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}
	@Override
	public int updateSDEmailAddr( String agentEmailAddr,String claimNumber) throws Exception{
		sql = "UPDATE NOTIFICATIONDATA SET SDEmailAddress = ? WHERE ClaimNumber = ?";
		log.info("updateSDEmailAddr sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{agentEmailAddr, claimNumber});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("updateSDEmailAddr--Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}
	@Override
	public int updateNSDEmailAddr( String agentEmailAddr,String claimNumber) throws Exception{
		sql = "UPDATE NOTIFICATIONDATA SET NSDEmailAddress = ? WHERE ClaimNumber = ?";
		log.info("updateNSDEmailAddr sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{agentEmailAddr, claimNumber});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("updateNSDEmailAddr--Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}
	@Override
	public int updateNotificationFlag(String errorMessage, String claimNumber) throws Exception{
		sql = "update notification set NotificationFlag ='9', GeneralExceptionMessage =? where claimnumber =?";
		log.info("updateNotificationFlag sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{errorMessage, claimNumber});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("updateNotificationFlag--Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;	
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateClaimStatusofClaimDataByProcessInstance(String processInstanceId, String claimStatus)
			throws Exception {
		// TODO Auto-generated method stub
		String sql = "update dbo.CLAIMDATA SET ClaimStatus = ? WHERE ProcessInstanceID = ? ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{claimStatus, processInstanceId});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int getTotalAuthorityMatrix(ClaimDataNewModel claimData, String claimUser, String userDecision)
			throws DAOException {
		// TODO Auto-generated method stub
		int i = 0;
		String sql = "SELECT COUNT(*) FROM AUTHORITYMATRIX WHERE ClaimUser = ? AND UserDecision = ? "
				+ " AND TotalClaimAmount >= ? AND ClaimTypeUI = ? ";
		try {
			i = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimUser, userDecision,
					claimData.getClaimInvoiceAmount(), claimData.getClaimTypeUI()}, Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public List<String> getClaimDataNew(String polNum, String claimStatus) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "select ClaimNumber from dbo.CLAIMDATA where PolicyNumber = ? and ClaimStatus <> ?";
		List<String> claimData = new ArrayList<String>();
		try {
			claimData = this.jdbcTemplateClaims.queryForList(sql, new Object[]{polNum, claimStatus}, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public ClaimDataPortalOutput getClaimDataForPortal(String strPencilClaimNumber) throws DAOException {
		String sql = "SELECT PortalClaimRequestNumber, ClaimNumber, PolicyNumber, ClaimTypeLA, ClaimStatus, ClaimDecision,"
				+ " SurgeryPerformed, AdmissionDate, DischargeDate, ProviderName, DoctorName, ClaimApprovedAmountUI, "
				+ "ClaimInvoiceAmount, ClaimCurrency, InsuredClientName, InsuredClientID, BankAccountNumber, BankAccountName, "
				+ "BankCode, RegisterDate FROM CLAIMDATA WHERE ClaimNumber = ?";
		log.info("getClaimDataForPortal sql is - " + sql);
		ClaimDataPortalOutput claimData = null;
		claimData = new ClaimDataPortalOutput();
		try {
			claimData = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{strPencilClaimNumber},
					new BeanPropertyRowMapper<ClaimDataPortalOutput>(ClaimDataPortalOutput.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return claimData;
	}

	@Override
	public List<RequirementDetailsModel> getRequirementDetailsList(String claimNumber) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "select * from dbo.REQUIREMENTSDETAILS where ClaimNumber = ?";
		List<RequirementDetailsModel> reqDetails_li = new ArrayList<RequirementDetailsModel>();
		try {
			reqDetails_li = this.jdbcTemplateClaims.query(sql, new Object[]{claimNumber},
					new BeanPropertyRowMapper<RequirementDetailsModel>(RequirementDetailsModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException(
					"Error at execution query = " + e + ",ClaimNumber = " + claimNumber + ", SQL Query = " + sql);
		}

		return reqDetails_li;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateClaimData(ClaimDataNewModel claimData) throws Exception {
		// TODO Auto-generated method stub
		int updatedRow = 0;
		String sql = "";
		if (claimData.getClaimTypeUI() != null && (claimData.getClaimTypeUI().equalsIgnoreCase("HS"))) {
			sql = "UPDATE dbo.CLAIMDATA SET UISaveFlag= ?,PolicyNumber= ?, ClaimTypeLA=?, "
					+ " ComponentCode=?, ComponentSumAssured=?, " + " AdmissionDate=?, DischargeDate=?, "
					+ " DiagnosisCode=?, DiagnosisName=?, ClaimInvoiceAmountUI=?, "
					+ " ClaimApprovedAmountUI=?, ClaimSumAssuredPercentage=?, FullyClaimed=?,  "
					+ " ProviderNameSource=?,  ProviderName=?, PayeeName=?, BankCode=?, "
					+ " BankType=?, BankAccountNumber=?, BankAccountName=?, DoctorName=?, "
					+ " FactoringHouse=?, CurrentFrom=?, BenefitCategory=?, OSCharges=?, OtherAdjustments=?, "
					+ " AdjustmentCode=?, PolicyLoanAmount=?, PolicyDebtAmount=?, ProviderCode=? "
					+ " where ClaimNumber = ?";

			updatedRow = this.jdbcTemplateClaims.update(sql, new Object[]{claimData.getUISaveFlag(),
					claimData.getPolicyNumber(), claimData.getClaimTypeLA(), claimData.getComponentCode(),
					claimData.getComponentSumAssured(), claimData.getAdmissionDate(), claimData.getDischargeDate(),
					claimData.getDiagnosisCode(), claimData.getDiagnosisName(),
					claimData.getClaimInvoiceAmountUI(), claimData.getClaimApprovedAmountUI(),
					claimData.getClaimSumAssuredPercentage(),
					claimData.getFullyClaimed() == null ? null : claimData.getFullyClaimed(),
							claimData.getProviderNameSource(), claimData.getProviderName(), claimData.getPayeeName(),
							claimData.getBankCode(), claimData.getBankType(), claimData.getBankAccountNumber(),
							claimData.getBankAccountName(), claimData.getDoctorName(), claimData.getFactoringHouse(),
							claimData.getCurrentFrom(), claimData.getBenefitCategory(), claimData.getOSCharges(),
							claimData.getOtherAdjustments(), claimData.getAdjustmentCode(), claimData.getPolicyLoanAmount(),
							claimData.getPolicyDebtAmount(), claimData.getProviderCode(), claimData.getClaimNumber()});

		} else if (claimData.getClaimTypeUI() != null && (claimData.getClaimTypeUI().equalsIgnoreCase("HSR")
				|| claimData.getClaimTypeUI().equalsIgnoreCase("HC"))) {
			sql = "UPDATE dbo.CLAIMDATA SET UISaveFlag= ?,PolicyNumber= ?, ClaimTypeLA=?, "
					+ " ComponentCode=?, ComponentSumAssured=?, " + " AdmissionDate=?, DischargeDate=?, "
					+ " DiagnosisCode=?, DiagnosisName=?, ClaimInvoiceAmountUI=?, "
					+ " ClaimApprovedAmountUI=?, ClaimSumAssuredPercentage=?, FullyClaimed=?,  "
					+ " ProviderNameSource=?,  ProviderName=?, PayeeName=?, BankCode=?, "
					+ " BankType=?, BankAccountNumber=?, BankAccountName=?, DoctorName=?, "
					+ " FactoringHouse=?, CurrentFrom=?, BenefitCategory=?, OSCharges=?, OtherAdjustments=?, "
					+ " AdjustmentCode=?, PolicyLoanAmount=?, PolicyDebtAmount=?, ProviderCode=?, StayDuration=? "
					+ " where ClaimNumber = ?";

			updatedRow = this.jdbcTemplateClaims.update(sql, new Object[]{claimData.getUISaveFlag(),
					claimData.getPolicyNumber(), claimData.getClaimTypeLA(), claimData.getComponentCode(),
					claimData.getComponentSumAssured(), claimData.getAdmissionDate(), claimData.getDischargeDate(),
					claimData.getDiagnosisCode(), claimData.getDiagnosisName(),
					claimData.getClaimInvoiceAmountUI(), claimData.getClaimApprovedAmountUI(),
					claimData.getClaimSumAssuredPercentage(),
					claimData.getFullyClaimed() == null ? null : claimData.getFullyClaimed(),
							claimData.getProviderNameSource(), claimData.getProviderName(), claimData.getPayeeName(),
							claimData.getBankCode(), claimData.getBankType(), claimData.getBankAccountNumber(),
							claimData.getBankAccountName(), claimData.getDoctorName(), claimData.getFactoringHouse(),
							claimData.getCurrentFrom(), claimData.getBenefitCategory(), claimData.getOSCharges(),
							claimData.getOtherAdjustments(), claimData.getAdjustmentCode(), claimData.getPolicyLoanAmount(),
							claimData.getPolicyDebtAmount(), claimData.getProviderCode(), claimData.getStayDuration(),
							claimData.getClaimNumber()});

		} else {
			if (claimData.getClaimTypeLA() != null && (claimData.getClaimTypeLA().equalsIgnoreCase("CI")
					|| claimData.getClaimTypeLA().equalsIgnoreCase("C1"))) {
				sql = "UPDATE dbo.CLAIMDATA SET UISaveFlag= ?,PolicyNumber= ?, ClaimTypeLA=?, "
						+ " ComponentCode=?, ComponentSumAssured=?, " + " AdmissionDate=?, DischargeDate=?, "
						+ " DiagnosisCode=?, DiagnosisName=?, ClaimInvoiceAmountUI=?, "
						+ " ClaimApprovedAmountUI=?, ClaimSumAssuredPercentage=?, FullyClaimed=?,  "
						+ " ProviderNameSource=?,  ProviderName=?, PayeeName=?, BankCode=?, "
						+ " BankType=?, BankAccountNumber=?, BankAccountName=?, DoctorName=?, "
						+ " FactoringHouse=?, CurrentFrom=?, BenefitCategory=?, OSCharges=?, OtherAdjustments=?, "
						+ " AdjustmentCode=?, PolicyLoanAmount=?, PolicyDebtAmount=?, ProviderCode=?, ClaimTypeUI=? "
						+ " where ClaimNumber = ?";

				updatedRow = this.jdbcTemplateClaims.update(sql, new Object[]{claimData.getUISaveFlag(),
						claimData.getPolicyNumber(), claimData.getClaimTypeLA(), claimData.getComponentCode(),
						claimData.getComponentSumAssured(), claimData.getAdmissionDate(),
						claimData.getDischargeDate(), claimData.getDiagnosisCode(), claimData.getDiagnosisName(),
						claimData.getClaimInvoiceAmountUI(),
						claimData.getClaimApprovedAmountUI(), claimData.getClaimSumAssuredPercentage(),
						claimData.getFullyClaimed() == null ? null : claimData.getFullyClaimed(),
								claimData.getProviderNameSource(), claimData.getProviderName(), claimData.getPayeeName(),
								claimData.getBankCode(), claimData.getBankType(), claimData.getBankAccountNumber(),
								claimData.getBankAccountName(), claimData.getDoctorName(), claimData.getFactoringHouse(),
								claimData.getCurrentFrom(), claimData.getBenefitCategory(), claimData.getOSCharges(),
								claimData.getOtherAdjustments(), claimData.getAdjustmentCode(),
								claimData.getPolicyLoanAmount(), claimData.getPolicyDebtAmount(),
								claimData.getProviderCode(), claimData.getClaimTypeLA(),
								claimData.getClaimNumber()});

			} else {
				sql = "UPDATE dbo.CLAIMDATA SET UISaveFlag= ?,PolicyNumber= ?, ClaimTypeLA=?, "
						+ " ComponentCode=?, ComponentSumAssured=?, " + " AdmissionDate=?, DischargeDate=?, "
						+ " DiagnosisCode=?, DiagnosisName=?, ClaimInvoiceAmountUI=?, "
						+ " ClaimApprovedAmountUI=?, ClaimSumAssuredPercentage=?, FullyClaimed=?,  "
						+ " ProviderNameSource=?,  ProviderName=?, PayeeName=?, BankCode=?, "
						+ " BankType=?, BankAccountNumber=?, BankAccountName=?, DoctorName=?, "
						+ " FactoringHouse=?, CurrentFrom=?, BenefitCategory=?, OSCharges=?, OtherAdjustments=?, "
						+ " AdjustmentCode=?, PolicyLoanAmount=?, PolicyDebtAmount=?, ProviderCode=? "
						+ " where ClaimNumber = ?";

				updatedRow = this.jdbcTemplateClaims.update(sql, new Object[]{claimData.getUISaveFlag(),
						claimData.getPolicyNumber(), claimData.getClaimTypeLA(), claimData.getComponentCode(),
						claimData.getComponentSumAssured(), claimData.getAdmissionDate(),
						claimData.getDischargeDate(), claimData.getDiagnosisCode(), claimData.getDiagnosisName(),
						claimData.getClaimInvoiceAmountUI(),
						claimData.getClaimApprovedAmountUI(), claimData.getClaimSumAssuredPercentage(),
						claimData.getFullyClaimed() == null ? null : claimData.getFullyClaimed(),
								claimData.getProviderNameSource(), claimData.getProviderName(), claimData.getPayeeName(),
								claimData.getBankCode(), claimData.getBankType(), claimData.getBankAccountNumber(),
								claimData.getBankAccountName(), claimData.getDoctorName(), claimData.getFactoringHouse(),
								claimData.getCurrentFrom(), claimData.getBenefitCategory(), claimData.getOSCharges(),
								claimData.getOtherAdjustments(), claimData.getAdjustmentCode(),
								claimData.getPolicyLoanAmount(), claimData.getPolicyDebtAmount(),
								claimData.getProviderCode(), claimData.getClaimNumber()});

			}

		}

		return updatedRow;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateClaimDataDetails(String claimNumber, ClaimDataUIClaimDetailsBenefitfundDetails benefitFundDetails)
			throws Exception {
		// TODO Auto-generated method stub
		ClaimDataDetailsModel claimDataDetailData = new ClaimDataDetailsModel();

		claimDataDetailData.setBenefitFromDate(Util.objToDate(benefitFundDetails.getBenefitDateFrom(), "dd/MM/yyyy"));
		claimDataDetailData.setBenefitToDate(Util.objToDate(benefitFundDetails.getBenefitDateTo(), "dd/MM/yyyy"));
		claimDataDetailData.setBenefitActualDays(benefitFundDetails.getBenefitNoOfDays());
		claimDataDetailData.setBenefitUserApprovedAmount(benefitFundDetails.getBenefitAactualPayableByClaimUser());
		claimDataDetailData.setBenefitCode(benefitFundDetails.getBenefitCode());
		claimDataDetailData.setBenefitApprovedAmount(benefitFundDetails.getBenefitApprovedAmount());
		claimDataDetailData.setBenefitNotApprovedAmount(benefitFundDetails.getBenefitNotApprovedAmount() == null
				? new BigDecimal(0) : benefitFundDetails.getBenefitNotApprovedAmount());
		claimDataDetailData.setBenefitTotalIncurredAmount(benefitFundDetails.getBenefitTotalIncurredAmount());
		claimDataDetailData.setBenefitAmountPerDay(benefitFundDetails.getBenefitdailyIncurred());
		// String sql = "UPDATE dbo.CLAIMDATADETAILS SET BenefitInvoiceAmount=?,
		// BenefitApprovedAmount=?, "
		// + " BenefitUserApprovedAmount=?, BenefitDaysClaimed=?,
		// BenefitAmountClaimed=?, BenefitAmountApplicable=?, "
		// + " BenefitAmountPerDay=?, BenefitFromDate=?, BenefitToDate=?,
		// BenefitActualDays=?, BenefitPercentage=?, "
		// + " BenefitName=?, FundComponentCode=?, FundComponentName=?,
		// FundCode=?, "
		// + " FundDescription=?, FundEstimatedAmount=?, FundActualAmount=?
		// where ClaimNumber = ? and BenefitCode = ?";
		String sql = "UPDATE dbo.CLAIMDATADETAILS SET " + " BenefitUserApprovedAmount=?,  "
				+ " BenefitFromDate=?, BenefitToDate=?, BenefitActualDays=?, BenefitApprovedAmount=?,"
				+ " BenefitNotApprovedAmount=?, BenefitTotalIncurredAmount=?, BenefitAmountPerDay=? "
				+ " where ClaimNumber = ?  and RTRIM(LTRIM(BenefitCode)) = '"
				+ claimDataDetailData.getBenefitCode().trim() + "' ";
		int i = 0;

		i = this.jdbcTemplateClaims.update(sql, new Object[]{claimDataDetailData.getBenefitUserApprovedAmount(),
				claimDataDetailData.getBenefitFromDate(), claimDataDetailData.getBenefitToDate(),
				claimDataDetailData.getBenefitActualDays(), claimDataDetailData.getBenefitApprovedAmount(),
				claimDataDetailData.getBenefitNotApprovedAmount(), claimDataDetailData.getBenefitTotalIncurredAmount(),
				claimDataDetailData.getBenefitAmountPerDay(), claimNumber});

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertClaimDataDetails(String claimNumber, ClaimDataUIClaimDetailsBenefitfundDetails benefitFundDetails)
			throws Exception {
		// TODO Auto-generated method stub
		ClaimDataDetailsModel claimDataDetailData = new ClaimDataDetailsModel();
		claimDataDetailData.setBenefitCode(benefitFundDetails.getBenefitCode());
		claimDataDetailData.setBenefitName(benefitFundDetails.getBenefitName());
		claimDataDetailData.setBenefitDaysClaimed(new BigDecimal(benefitFundDetails.getBenefitnoOfDaysClaimed()));
		claimDataDetailData.setBenefitAmountClaimed(benefitFundDetails.getBenefittotalAmountClaimedAsOfNow());
		claimDataDetailData.setBenefitAmountApplicable(benefitFundDetails.getBenefitTotalAvailableAmountForClaim());
		claimDataDetailData.setBenefitAmountPerDay(benefitFundDetails.getBenefitdailyIncurred());
		claimDataDetailData.setBenefitFromDate(Util.objToDate(benefitFundDetails.getBenefitDateFrom(), "dd/MM/yyyy"));
		claimDataDetailData.setBenefitToDate(Util.objToDate(benefitFundDetails.getBenefitDateTo(), "dd/MM/yyyy"));
		claimDataDetailData.setBenefitActualDays(benefitFundDetails.getBenefitNoOfDays());
		claimDataDetailData.setBenefitInvoiceAmount(benefitFundDetails.getBenefitAmountIncurred());
		claimDataDetailData.setBenefitApprovedAmount(benefitFundDetails.getBenefitApprovedAmount());
		claimDataDetailData.setBenefitUserApprovedAmount(benefitFundDetails.getBenefitAactualPayableByClaimUser());
		claimDataDetailData.setBenefitPercentage(benefitFundDetails.getBenefitPercentage());
		claimDataDetailData.setBenefitAnnualTotalDays(benefitFundDetails.getBenefitAnnualTotalDays());
		claimDataDetailData.setBenefitNotApprovedAmount(benefitFundDetails.getBenefitNotApprovedAmount() == null
				? new BigDecimal(0) : benefitFundDetails.getBenefitNotApprovedAmount());
		claimDataDetailData.setBenefitMaxAmountPerDay(benefitFundDetails.getBenefitMaxAmountPerDay());
		claimDataDetailData.setBenefitTotalIncurredAmount(benefitFundDetails.getBenefitTotalIncurredAmount());
		claimDataDetailData.setBenefitNoOfUnits(benefitFundDetails.getBenefitNoOfUnits());
		claimDataDetailData.setFundComponentCode(benefitFundDetails.getFundComponentCode());
		claimDataDetailData.setFundComponentName(benefitFundDetails.getFundComponentName());
		claimDataDetailData.setFundCode(benefitFundDetails.getFundCode());
		claimDataDetailData.setFundDescription(benefitFundDetails.getFundDescription());
		claimDataDetailData.setFundEstimatedAmount(benefitFundDetails.getFundEstimatedAmount());
		claimDataDetailData.setFundActualAmount(benefitFundDetails.getFundActualAmount());

		String sql = "INSERT INTO dbo.CLAIMDATADETAILS "
				+ " ( ClaimNumber, BenefitCode, BenefitInvoiceAmount, BenefitApprovedAmount, BenefitUserApprovedAmount, "
				+ " BenefitDaysClaimed, BenefitAmountClaimed, BenefitAmountApplicable, BenefitAmountPerDay, BenefitFromDate, "
				+ " BenefitToDate, BenefitActualDays, BenefitPercentage, BenefitName, FundComponentCode, "
				+ " FundComponentName, FundCode, FundDescription, FundEstimatedAmount, FundActualAmount,BenefitComponentUnit,"
				+ " BenefitAnnualTotalDays,BenefitNotApprovedAmount,BenefitMaxAmountPerDay,BenefitTotalIncurredAmount,BenefitNoOfUnits ) "
				+ " VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?, ?, ?, ?, ?) ";
		int i = 0;

		i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, claimDataDetailData.getBenefitCode(),
				claimDataDetailData.getBenefitInvoiceAmount(), claimDataDetailData.getBenefitApprovedAmount(),
				claimDataDetailData.getBenefitUserApprovedAmount(), claimDataDetailData.getBenefitDaysClaimed(),
				claimDataDetailData.getBenefitAmountClaimed(), claimDataDetailData.getBenefitAmountApplicable(),
				claimDataDetailData.getBenefitAmountPerDay(), claimDataDetailData.getBenefitFromDate(),
				claimDataDetailData.getBenefitToDate(), claimDataDetailData.getBenefitActualDays(),
				claimDataDetailData.getBenefitPercentage(), claimDataDetailData.getBenefitName(),
				claimDataDetailData.getFundComponentCode(), claimDataDetailData.getFundComponentName(),
				claimDataDetailData.getFundCode(), claimDataDetailData.getFundDescription(),
				claimDataDetailData.getFundEstimatedAmount(), claimDataDetailData.getFundActualAmount(),
				claimDataDetailData.getBenefitAnnualTotalDays(), claimDataDetailData.getBenefitNotApprovedAmount(),
				claimDataDetailData.getBenefitMaxAmountPerDay(), claimDataDetailData.getBenefitTotalIncurredAmount(),
				claimDataDetailData.getBenefitNoOfUnits()});

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int deleteClaimDataDetails(String claimNumber, String benefitCode) throws Exception {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM  dbo.CLAIMDATADETAILS WHERE ClaimNumber = ? and BenefitCode = ?";
		int i = 0;

		i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, benefitCode});

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateRequirementDetails(String claimNumber, String polNum, String type, String typeValue)
			throws Exception {
		// TODO Auto-generated method stub
		String sql = " UPDATE dbo.REQUIREMENTSDETAILS SET ClaimNumber = ?, PolicyNumber = ? " + " where " + type
				+ " = ? ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, polNum, typeValue});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertRequirementDetails(String claimNumber, String docId,
			ClaimDataUIClaimDetailsAdditionalRequirement addReqData) throws Exception {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int deleteRequirementDetails(String claimNumber, String docId) throws Exception {
		// TODO Auto-generated method stub
		String sql = "delete from dbo.REQUIREMENTSDETAILS where ClaimNumber= ? and DocID= ?";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, docId});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveClaimDataDetails(String claimNumber,
			List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitFundDetails_li,
			List<ClaimDataDetailsModel> claimDataDetails_li) throws Exception {
		// TODO Auto-generated method stub
		boolean valid = true;
		List<Map<String, ClaimDataUIClaimDetailsBenefitfundDetails>> mapBenefit_li = new ArrayList<Map<String, ClaimDataUIClaimDetailsBenefitfundDetails>>();
		List<String> inputBenefitCode_li = new ArrayList<String>();
		for (ClaimDataUIClaimDetailsBenefitfundDetails benFunDetail : benefitFundDetails_li) {
			Map<String, ClaimDataUIClaimDetailsBenefitfundDetails> mapBenefit = new HashMap<String, ClaimDataUIClaimDetailsBenefitfundDetails>();
			if (!((benFunDetail.getBenefitCode() != null && !benFunDetail.getBenefitCode().trim().equalsIgnoreCase(""))
					^ ((benFunDetail.getBenefitName() != null
					&& !benFunDetail.getBenefitName().trim().equalsIgnoreCase(""))))) {
				if ((benFunDetail.getBenefitCode() != null
						&& !benFunDetail.getBenefitCode().trim().equalsIgnoreCase(""))) {
					mapBenefit.put(benFunDetail.getBenefitCode().trim(), benFunDetail);
					mapBenefit_li.add(mapBenefit);
					inputBenefitCode_li.add(benFunDetail.getBenefitCode().trim());
				}
			} else {
				valid = false;
			}
		}
		List<Map<String, ClaimDataDetailsModel>> mapClaimDataDetails_li = new ArrayList<Map<String, ClaimDataDetailsModel>>();
		List<String> DBBenefitCode_li = new ArrayList<String>();
		for (ClaimDataDetailsModel claimDataDetail : claimDataDetails_li) {
			Map<String, ClaimDataDetailsModel> mapClaimDataDetails = new HashMap<String, ClaimDataDetailsModel>();
			if (claimDataDetail.getBenefitCode() != null
					&& !claimDataDetail.getBenefitCode().trim().equalsIgnoreCase("")) {
				mapClaimDataDetails.put(claimDataDetail.getBenefitCode().trim(), claimDataDetail);
				mapClaimDataDetails_li.add(mapClaimDataDetails);
				DBBenefitCode_li.add(claimDataDetail.getBenefitCode().trim());
			}
		}
		// Compare benefit code between two list
		// For update
		List<String> benUpdate_li = inputBenefitCode_li.stream().filter(elem -> DBBenefitCode_li.contains(elem))
				.collect(Collectors.toList());
		// For insert
		List<String> benInsert_li = inputBenefitCode_li.stream().filter(elem -> !DBBenefitCode_li.contains(elem))
				.collect(Collectors.toList());
		// For delete
		List<String> benDelete_li = DBBenefitCode_li.stream().filter(elem -> !inputBenefitCode_li.contains(elem))
				.collect(Collectors.toList());
		// doUpdation
		if (benUpdate_li.size() > 0) {
			for (String data : benUpdate_li) {
				for (Map<String, ClaimDataUIClaimDetailsBenefitfundDetails> mapInput : mapBenefit_li) {
					if (mapInput.containsKey(data)) {
						// doUpdation
						int i = updateClaimDataDetails(claimNumber, mapInput.get(data));
						if (i == 0) {
							throw new DAOException("Claim Data Details not updated for ClaimNumber = " + claimNumber
									+ " and benefitCode = " + mapInput.get(data).getBenefitCode());
						}
					}
				}
			}
		}
		// doInsertion
		if (benInsert_li.size() > 0) {
			for (String data : benInsert_li) {
				for (Map<String, ClaimDataUIClaimDetailsBenefitfundDetails> mapInput : mapBenefit_li) {
					if (mapInput.containsKey(data)) {
						// doInsertion
						int i = insertClaimDataDetails(claimNumber, mapInput.get(data));
						if (i == 0) {
							throw new DAOException("Claim Data Details not inserted for ClaimNumber = " + claimNumber
									+ " and benefitCode = " + mapInput.get(data).getBenefitCode());
						}
					}
				}
			}
		}
		// doDeletion
		if (valid == true) {
			if (benDelete_li.size() > 0) {
				for (String data : benDelete_li) {
					for (Map<String, ClaimDataDetailsModel> mapDB : mapClaimDataDetails_li) {
						if (mapDB.containsKey(data)) {
							// doDeletion

							int i = deleteClaimDataDetails(claimNumber, mapDB.get(data).getBenefitCode());
							if (i == 0) {
								throw new DAOException("Claim Data Details not deleted for ClaimNumber = " + claimNumber
										+ " and benefitCode = " + mapDB.get(data).getBenefitCode());
							}
						}
					}
				}
			}
		}

	}

	/*
	 * @Transactional(propagation = Propagation.REQUIRED, rollbackFor =
	 * Exception.class) public void saveRequirementDetails(String claimNumber,
	 * List<ClaimDataUIClaimDetailsAdditionalRequirement> additionalReq_li,
	 * List<RequirementDetailsModel> reqDetails_li) throws Exception { // move
	 * to Map Object List<Map<String,
	 * ClaimDataUIClaimDetailsAdditionalRequirement>> mapAddReq_li = new
	 * ArrayList<Map<String, ClaimDataUIClaimDetailsAdditionalRequirement>>();
	 * List<String> inputDocId_li = new ArrayList<String>(); for
	 * (ClaimDataUIClaimDetailsAdditionalRequirement addReqData :
	 * additionalReq_li) { Map<String,
	 * ClaimDataUIClaimDetailsAdditionalRequirement> mapAddReq = new
	 * HashMap<String, ClaimDataUIClaimDetailsAdditionalRequirement>();
	 * mapAddReq.put(addReqData.getReqtDropDownText().split("-")[0],
	 * addReqData); mapAddReq_li.add(mapAddReq);
	 * inputDocId_li.add(addReqData.getReqtDropDownText().split("-")[0]); }
	 *
	 * List<String> DBDocId_li = new ArrayList<String>(); for
	 * (RequirementDetailsModel reqDetailData : reqDetails_li) {
	 * DBDocId_li.add(reqDetailData.getDocID()); }
	 *
	 * // For update List<String> docIdUpdate_li =
	 * inputDocId_li.stream().filter(elem -> DBDocId_li.contains(elem))
	 * .collect(Collectors.toList()); // For insert List<String> docIdInsert_li
	 * = inputDocId_li.stream().filter(elem -> !DBDocId_li.contains(elem))
	 * .collect(Collectors.toList()); // For delete List<String> docIdDelete_li
	 * = DBDocId_li.stream().filter(elem -> !inputDocId_li.contains(elem))
	 * .collect(Collectors.toList());
	 *
	 * // doUpdation if (docIdUpdate_li.size() > 0) { for (String data :
	 * docIdUpdate_li) { for (Map<String,
	 * ClaimDataUIClaimDetailsAdditionalRequirement> mapInput : mapAddReq_li) {
	 * if (mapInput.containsKey(data)) { // doUpdation int i =
	 * updateRequirementDetails(claimNumber, data, mapInput.get(data)); if (i ==
	 * 0) { throw new
	 * DAOException("Requirement Details not updated for ClaimNumber = " +
	 * claimNumber + " and docId = " + data); } } } } }
	 *
	 * // Insertion if (docIdInsert_li.size() > 0) { for (String data :
	 * docIdInsert_li) { for (Map<String,
	 * ClaimDataUIClaimDetailsAdditionalRequirement> mapInput : mapAddReq_li) {
	 * if (mapInput.containsKey(data)) {
	 *
	 * int i = insertRequirementDetails(claimNumber, data, mapInput.get(data));
	 * if (i == 0) { throw new
	 * DAOException("Requirement Details not inserted for ClaimNumber = " +
	 * claimNumber + " and docId = " + data); } } } } }
	 *
	 * // Deletion if (docIdDelete_li.size() > 0) { for (String data :
	 * docIdDelete_li) { // doDeletion int i =
	 * deleteRequirementDetails(claimNumber, data); if (i == 0) { throw new
	 * DAOException("Requirement Details not deleted for ClaimNumber = " +
	 * claimNumber + " and docId = " + data);
	 *
	 * } } } }
	 */

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveClaimDataUI(String claimNumber,
			List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitFundDetails_li,
			List<ClaimDataDetailsModel> claimDataDetails_li, ClaimDataNewModel claimData,
			ClaimDataUIClaimDetails dataIn, String claimTypeUI, String listLifename, String listSubCategory,
			String listBenefitName, java.sql.Date compRiskDate, Log log, String benNotPaidAmountList) throws Exception {
		saveClaimDataDetails(claimNumber, benefitFundDetails_li, claimDataDetails_li);
		// saveRequirementDetails(claimNumber, additionalReq_li, reqDetails_li);
		int roeClaimData = updateClaimData(claimData);
		if (roeClaimData == 0) {
			throw new Exception("Not Updted for ClaimData with claimNumber = " + claimNumber);
		}

		log.info("Getting the benNameList");
		listBenefitName = getBenefitIDList(dataIn.getClaimNumber());
		log.info("Got the benNameList = " + listBenefitName);

		//		log.info("Getting the benNotPaidAmtList");
		//		List<BenefitNotPaidAmount> benNotPaidAmt_li = new ArrayList<BenefitNotPaidAmount>();
		//		benNotPaidAmt_li = getBenefitNotPaidAmountList(dataIn.getClaimNumber());
		//		benNotPaidAmountList = Util.listToString(benNotPaidAmt_li, ";");
		//		log.info("Got the benNotPaidAmtList = " + benNotPaidAmountList);

		log.info("Updating the NotificationData Table");
		int rowUpdate = updateNotificationData(dataIn, claimTypeUI, listLifename, listSubCategory, listBenefitName,
				compRiskDate, benNotPaidAmountList);
		if (rowUpdate == 0) {
			throw new Exception("Not Updated for NotificationData with claimNumber = " + dataIn.getClaimNumber());
		}
		log.info(" Successfully Updating the NotificationData Table");

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int deleteRequirementDetails(String claimNumber, String docId_li, String reqNotifSent, String reqStatus)
			throws Exception {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM dbo.REQUIREMENTSDETAILS WHERE CLAIMNUMBER = ? AND DOCID NOT IN ( ? ) "
				+ " AND RequirementNotificationSent = ? AND RequirementStatus = ?";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, docId_li, reqNotifSent, reqStatus});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertRequirementDetails(String claimNumber, String tpaClaimNum, String batchNum,
			String portalClaimReqNum, String policyNumber, String docId, String requirementText,
			Timestamp requirementRequestTs, String requirementStatus, String custInitiatedReq) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO dbo.REQUIREMENTSDETAILS (ClaimNumber,PolicyNumber,DocID,RequirementText, "
				+ " RequirementRequestTS,RequirementStatus, "
				+ " CustomerInitiatedRequirement,RequirementNotificationSent,TPAClaimNumber,BatchNumber,PortalClaimRequestNumber) "
				+ " VALUES (?,?,?,?,?,?,?,'N',?,?,?) ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql,
					new Object[]{claimNumber, policyNumber, docId, requirementText, requirementRequestTs,
							requirementStatus, custInitiatedReq, tpaClaimNum, batchNum, portalClaimReqNum});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateClaimDataGeneral(String claimNumber, String type, String typeValue) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update dbo.CLAIMDATA set " + type + " = ? WHERE ClaimNumber = ?";
		int i = this.jdbcTemplateClaims.update(sql, new Object[]{typeValue, claimNumber});
		return i;
	}

	@Override
	public int countRequiremnetDetails(String claimNumber, String requirementStatus) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT count(*) FROM dbo.REQUIREMENTSDETAILS WHERE CLAIMNUMBER = ? "
				+ " AND RequirementStatus = ? AND RequirementReceiveTS IS NULL AND RequirementNotificationSentTS IS NULL ";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber, requirementStatus},
					Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateRequirementDetailsWithRecId(String claimNumber, String reqNotificationSent, String reqStatus,String reqtReqActivityId)
			throws Exception {
		// TODO Auto-generated method stub
		String sql = "Update dbo.REQUIREMENTSDETAILS set RequirementNotificationSentTS = getDate(), "
				+ " RequirementNotificationSent = ?, RequirementRequestActivity=? WHERE ClaimNumber = ? "
				+ " AND RequirementStatus = ? AND RequirementReceiveTS IS NULL AND RequirementNotificationSentTS IS NULL";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{reqNotificationSent,reqtReqActivityId, claimNumber, reqStatus});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimUser(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimUser = ?  where ClaimNumber = ? and ClaimUser is NULL "
				+ "or  ClaimUser =''";
		log.info("updateClaimUser - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimApprovalHAUser(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimApprovalHAUser = ?  where ClaimNumber = ? and"
				+ " (ClaimApprovalHAUser is NULL " + "or  ClaimApprovalHAUser ='')";
		log.info("updateClaimApprovalHAUser - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimApprovalHAUserName(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimApprovalHAUserName = ?  where ClaimNumber = ? "
				+ "and (ClaimApprovalHAUserName is NULL " + "or  ClaimApprovalHAUserName ='')";
		log.info("updateClaimApprovalHAUserName - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimRejectHAUser(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimRejectHAUser = ?  where ClaimNumber = ? and"
				+ "( ClaimRejectHAUser is NULL " + "or  ClaimRejectHAUser ='')";
		log.info("updateClaimApprovalHAUserName - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimRejectHAUserName(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimRejectHAUserName = ?  where ClaimNumber = ? and"
				+ "( ClaimRejectHAUserName is NULL " + "or  ClaimRejectHAUserName ='')";
		log.info("updateClaimApprovalHAUserName - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimUWUser(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimUWUser = ?  where ClaimNumber = ? and " + "(ClaimUWUser is NULL "
				+ "or  ClaimUWUser ='')";
		log.info("updateClaimApprovalHAUserName - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimHUWUser(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimHUWUser = ?  where ClaimNumber = ? and " + "(ClaimHUWUser is NULL "
				+ "or  ClaimHUWUser ='')";
		log.info("updateClaimHUWUser - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimProcessorName(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimProcessorName = ?  where ClaimNumber = ? "
				+ "and (ClaimProcessorName is NULL " + "or  ClaimProcessorName ='')";
		log.info("updateClaimProcessorName - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimProcessorEmailAddress(String ClaimNo, String emailadd) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update NOTIFICATIONDATA set  ClaimProcessorEmailAddress = ?  " + "where "
				+ "ClaimNumber = ? and( ClaimProcessorEmailAddress is NULL " + "or  ClaimProcessorEmailAddress ='')";
		log.info("updateClaimProcessorEmailAddress - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{emailadd, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateRemarks(String ClaimNo, String remarks) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update NOTIFICATIONDATA set  Remark = ?  where ClaimNumber = ?";
		log.info("updateRemarks - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{remarks, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimProcessorInvestigationEmailAddres(String ClaimNo, String emailadd) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update NOTIFICATIONDATA set  ClaimProcessorInvestigationEmailAddress = ? "
				+ " where ClaimNumber = ? and " + "(ClaimProcessorInvestigationEmailAddress is NULL "
				+ "or  ClaimProcessorInvestigationEmailAddress ='')";
		log.info("updateClaimProcessorInvestigationEmailAddres - sql is -" + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{emailadd, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimProcessor(String ClaimNo, String activityUser) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimProcessor = ?  where ClaimNumber = ? and" + "( ClaimProcessor is NULL "
				+ "or  ClaimProcessor ='')";
		log.info("updateClaimProcessor - sql is -" + sql);
		int i = 0;

		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{activityUser, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimDecisionDate(String ClaimNo) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimDecisionDate = CONVERT(DATETIME,?,105) " + " where ClaimNumber = ?  ";
		log.info("updateClaimDecisionDate - sql is -" + sql);
		int i = 0;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{timestamp, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateNotificationClaimDecisionDate(String ClaimNo) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update NOTIFICATIONDATA set  ClaimDecisionDate = CONVERT(DATETIME,?,105) "
				+ " where ClaimNumber = ?  ";
		log.info("updateNotificationClaimDecisionDate - sql is -" + sql);
		int i = 0;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{timestamp, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateClaimDecision(String ClaimNo, String decision) throws Exception {
		// TODO Auto-generated method stub
		String sql = "update CLAIMDATA set  ClaimDecision = ? " + " where ClaimNumber = ?  ";
		log.info("updateClaimDecision - sql is -" + sql);
		int i = 0;

		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{decision, ClaimNo});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int countRequiremnetDetails(String claimNumber, String docId, String requirementStatus) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "SELECT COUNT(*) FROM dbo.REQUIREMENTSDETAILS WHERE DOCID = ? AND CLAIMNUMBER = ? "
				+ " AND RequirementStatus = ? AND RequirementReceiveTS IS NULL ";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{docId, claimNumber, requirementStatus},
					Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertRequirementDetails(String claimNumber, String policyNumber, String appNumber,
			String tpaClaimNumber, String batchNumber, String portalClaimRequestNum, String posRequestNum, String docId,
			String reqText, Timestamp reqRequestTs, Timestamp reqReceiveTs, String reqStatus, String custIniRequirement,
			String reqNotifSent, String reqtReqActivityId) throws Exception {
		// TODO Auto-generated method stub
		// String sql = "INSERT INTO dbo.REQUIREMENTSDETAILS
		// (ClaimNumber,PolicyNumber,DocID,RequirementRequestTS,RequirementReceiveTS,
		// "
		// + "
		// RequirementStatus,RequirementNotificationSent,CustomerInitiatedRequirement)
		// "
		// + " VALUES (?,?,?,getDate(),?,?,?,?) ";
		sql = "INSERT INTO dbo.REQUIREMENTSDETAILS (ClaimNumber,AppNumber,PolicyNumber,TPAClaimNumber,BatchNumber, "
				+ " PortalClaimRequestNumber,POSRequestNumber,DocID,RequirementText,RequirementRequestTS,RequirementReceiveTS, "
				+ " RequirementStatus,  CustomerInitiatedRequirement,RequirementNotificationSent,RequirementRequestActivity) " +
				" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql,
					new Object[]{claimNumber, appNumber, policyNumber, tpaClaimNumber, batchNumber,
							portalClaimRequestNum, posRequestNum, docId, reqText, reqRequestTs, reqReceiveTs, reqStatus,
							custIniRequirement, reqNotifSent,reqtReqActivityId});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateRequirementDetails(String claimNumber, String DocId, Timestamp reqReceivedTS, String reqStatus)
			throws Exception {
		// TODO Auto-generated method stub
		String sql = "UPDATE dbo.REQUIREMENTSDETAILS SET RequirementReceiveTS = ?, RequirementStatus=? "
				+ " WHERE ClaimNumber = ? AND DOCID = ? "
				+ " AND RequirementStatus = 'PENDING' AND RequirementReceiveTS IS NULL ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{reqReceivedTS, reqStatus, claimNumber, DocId});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertCOBREquest(String claimNumber, String polNum, int processed, Timestamp reqProcessedTS,
			String processRemarks) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO dbo.COBREQUEST "
				+ " (ClaimNumber, PolicyNumber, Processed, RequestRaisedTS, RequestProcessedTS, ProcessedRemarks) "
				+ " VALUES(?, ?, ?, (getdate()), ?, ?) ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql,
					new Object[]{claimNumber, polNum, processed, reqProcessedTS, processRemarks});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertCOBREquest(String claimNumber, String polNum) throws Exception {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO dbo.COBREQUEST " + " (ClaimNumber, PolicyNumber ) " + " VALUES(?, ?) ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{claimNumber, polNum,});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public String getDocIDList(String claimNumber) throws DAOException {
		// TODO Auto-generated method stub
		String result = "('')";
		List<String> result_li = new ArrayList<String>();

		//        String sql = "(SELECT STUFF (( SELECT ', ''' + RQ2.DOCID + '''' "
		//                + "                     FROM dbo.REQUIREMENTSDETAILS RQ2 "
		//                + "                                         WHERE RQ1.CLAIMNUMBER = RQ2.CLAIMNUMBER AND RQ2.RequirementNotificationSent = 'N' AND RQ2.RequirementReceiveTS IS NULL "
		//                + "                                         ORDER BY RQ2.DocID "
		//                + "                                         FOR XML PATH('')), 1,1,'') AS DOCUMENTID "
		//                + " FROM REQUIREMENTSDETAILS RQ1 GROUP By RQ1.CLAIMNUMBER HAVING RQ1.CLAIMNUMBER = ? ) ";
		String sql = "(SELECT STUFF (( SELECT ',( ''' + RQ2.DOCID + ''')' "
				+ "                     FROM dbo.REQUIREMENTSDETAILS RQ2 "
				+ "                                         WHERE RQ1.CLAIMNUMBER = RQ2.CLAIMNUMBER AND RQ2.RequirementNotificationSent = 'N' AND RQ2.RequirementReceiveTS IS NULL "
				+ "                                         ORDER BY RQ2.DocID "
				+ "                                         FOR XML PATH('')), 1,1,'') AS DOCUMENTID "
				+ " FROM REQUIREMENTSDETAILS RQ1 GROUP By RQ1.CLAIMNUMBER HAVING RQ1.CLAIMNUMBER = ? ) ";
		try {
			result_li = this.jdbcTemplateClaims.queryForList(sql, new Object[]{claimNumber}, String.class);
			if (result_li != null && result_li.size() > 0) {
				result = result_li.get(0);
			}
			if (result == null || result.trim() == "" || result.trim().equalsIgnoreCase("")) {
				result = "('')";
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL	 Query = " + sql);
		}

		return result;
	}

	@Override
	public String getBenefitIDList(String claimNumber) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		List<String> result_li = new ArrayList<String>();
		String sql = "SELECT STUFF (( SELECT '; ' + CDD2.BenefitName + '' " + " FROM dbo.CLAIMDATADETAILS CDD2 "
				+ " WHERE CDD2.CLAIMNUMBER = ? "
				+ " ORDER BY CDD2.BENEFITCODE                                          FOR XML PATH('')), 1,1,'') ";
		;
		try {
			// result = this.jdbcTemplateClaims.queryForObject(sql, new Object[]
			// { claimNumber }, String.class);
			result_li = this.jdbcTemplateClaims.queryForList(sql, new Object[]{claimNumber}, String.class);
			if (result_li != null && result_li.size() > 0) {
				result = result_li.get(0);
			}
			if (result == null || result.trim() == "" || result.trim().equalsIgnoreCase("")) {
				result = "";
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateNotificationData(ClaimDataUIClaimDetails dataIn, String claimTypeUI, String listLifeName,
			String listSubCategory, String listBenefitName, java.sql.Date compRiskCesDate, String benNotPaidAmountList)
					throws Exception {
		// TODO Auto-generated method stub
		String claimNumber = dataIn.getClaimNumber().trim();
		String sqlAmtIncurred = "";
		String sqlBenefitApprovedAmt = "";
		String sqlBenefitUsrApprovedAmt = "";
		String sqlBenefitNotPaidAmt = "";
		if (claimTypeUI.equalsIgnoreCase("HS")) {
			sqlAmtIncurred = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitTotalIncurredAmount as VARCHAR(50)) + '' " +
					" FROM dbo.CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitInvAmt " +
					" FROM dbo.CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitUsrApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitNotPaidAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount - CDD2.BenefitTotalIncurredAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
		} else if (claimTypeUI.equalsIgnoreCase("HSR")) {
			sqlAmtIncurred = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitTotalIncurredAmount as VARCHAR(50)) + '' " +
					" FROM dbo.CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitInvAmt " +
					" FROM dbo.CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitUsrApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitNotPaidAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitNotApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
		} else if (claimTypeUI.equalsIgnoreCase("HC")) {
			sqlAmtIncurred = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitTotalIncurredAmount as VARCHAR(50)) + '' " +
					" FROM dbo.CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitInvAmt " +
					" FROM dbo.CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitUsrApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitNotPaidAmt = "'0'";
		} else {
			sqlAmtIncurred = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitTotalIncurredAmount as VARCHAR(50)) + '' " +
					" FROM dbo.CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitInvAmt " +
					" FROM dbo.CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitUsrApprovedAmt = "(SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitUserApprovedAmount as VARCHAR(50)) + '' " +
					" FROM CLAIMDATADETAILS CDD2 " +
					" WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER " +
					" ORDER BY CDD2.BENEFITCODE " +
					" FOR XML PATH('')), 1,1,'') AS BenefitApprovedAmt " +
					" FROM CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '" + claimNumber + "') ";
			sqlBenefitNotPaidAmt = "'0'";
		}
		;
		//        List<String> resultReq_li = new ArrayList<>();
		//        String resultReq = "";
		//        resultReq_li = this.jdbcTemplateClaims.queryForList("(SELECT STUFF (( SELECT '~#@ ' +RQ2.RequirementText FROM dbo.REQUIREMENTSDETAILS RQ2 " +
		//                "WHERE RQ1.CLAIMNUMBER = RQ2.CLAIMNUMBER AND RQ2.RequirementNotificationSent = 'N' " +
		//                "AND RQ2.RequirementReceiveTS IS NULL ORDER BY RQ2.DocID FOR XML PATH('')), 1,3,'') AS REQTEXT " +
		//                "FROM dbo.REQUIREMENTSDETAILS RQ1 GROUP By RQ1.CLAIMNUMBER HAVING RQ1.CLAIMNUMBER = '"+claimNumber+"') ",String.class);
		//        if (resultReq_li != null && resultReq_li.size() > 0 ) {
		//            resultReq = resultReq_li.get(0);
		//        }
		//        resultReq = StringEscapeUtils.escapeXml10(resultReq);
		//        if (resultReq == null ) {
		//            resultReq = "";
		//        }
		//        resultReq = Matcher.quoteReplacement(resultReq);

		String sql = "UPDATE dbo.NOTIFICATIONDATA SET AdmissionDate = ? " + " ,DischargeDate = ? "
				+ " ,LengthOfStay = (SELECT StayDuration FROM dbo.CLAIMDATA WHERE CLAIMNUMBER = '" + claimNumber
				+ "' ) " + " ,ClaimUITotalClaimAmount = ? " + " ,PayeeName = (CASE ?  "
				+ " when 'HS' then  (SELECT ProviderName FROM dbo.CLAIMDATA WHERE CLAIMNUMBER = '" + claimNumber + "') "
				+ " else ? END ) " + " ,ProviderName = ? " + " ,DiagnosisCode= ? "
				+ " ,ClaimInvoiceAmount =  (SELECT ClaimInvoiceAmount FROM dbo.CLAIMDATA WHERE CLAIMNUMBER = '"
				+ claimNumber + "') "
				+ " ,RequirementTextList = (SELECT STUFF (( SELECT '~#@ ' +RQ2.RequirementText FROM dbo.REQUIREMENTSDETAILS RQ2 WHERE RQ1.CLAIMNUMBER = RQ2.CLAIMNUMBER AND RQ2.RequirementNotificationSent = 'N' AND RQ2.RequirementReceiveTS IS NULL ORDER BY RQ2.DocID FOR XML PATH('')), 1,3,'') AS REQTEXT FROM dbo.REQUIREMENTSDETAILS RQ1 GROUP By RQ1.CLAIMNUMBER HAVING RQ1.CLAIMNUMBER = '"
				+ claimNumber + "' ) " + " ,RequirementDocSubCategoryList = ? "
				+ " ,PendingDocRequestDate30 = (select tab.RequirementRequestTS+30 from (SELECT  DISTINCT RequirementRequestTS  FROM dbo.REQUIREMENTSDETAILS WHERE CLAIMNUMBER = '"
				+ claimNumber + "' AND RequirementNotificationSent = 'N' AND RequirementReceiveTS IS NULL) tab) "
				+ " ,BenefitDescription = ? "
				+ " ,BenefitLengthOfStay = (SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitActualDays as VARCHAR(40)) + '' "
				+ "                     FROM dbo.CLAIMDATADETAILS CDD2 "
				+ "                                         WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER  "
				+ "                                         ORDER BY CDD2.BENEFITCODE "
				+ "                                         FOR XML PATH('')), 1,1,'') AS BENEFITID                   "
				+ "                                         FROM dbo.CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '"
				+ claimNumber + "') "
				+ " ,BenefitAmountIncurred = " + sqlAmtIncurred
				+ " ,BenefitApproveAmount = " + sqlBenefitApprovedAmt
				+ " ,BenefitUserApprovedAmount = " + sqlBenefitUsrApprovedAmt
				+ " ,BenefitNotPaidAmout = " + sqlBenefitNotPaidAmt
				+ " ,ComponentRiskCessDate = ?                                           " + " WHERE CLAIMNUMBER = '"
				+ claimNumber + "' ";
		//        String sql = "UPDATE dbo.NOTIFICATIONDATA SET AdmissionDate = ? " + " ,DischargeDate = ? "
		//                + " ,LengthOfStay = (SELECT StayDuration FROM dbo.CLAIMDATA WHERE CLAIMNUMBER = '" + claimNumber
		//                + "' ) " + " ,ClaimUITotalClaimAmount = ? " + " ,PayeeName = (CASE ?  "
		//                + " when 'HS' then  (SELECT ProviderName FROM dbo.CLAIMDATA WHERE CLAIMNUMBER = '" + claimNumber + "') "
		//                + " else ? END ) " + " ,ProviderName = ? " + " ,DiagnosisCode= ? "
		//                + " ,ClaimInvoiceAmount =  (SELECT ClaimInvoiceAmount FROM dbo.CLAIMDATA WHERE CLAIMNUMBER = '"
		//                + claimNumber + "') "
		//                + " ,RequirementTextList = '"+resultReq+"'" + " ,RequirementDocSubCategoryList = ? "
		//                + " ,PendingDocRequestDate30 = (select tab.RequirementRequestTS+30 from (SELECT  DISTINCT RequirementRequestTS  FROM dbo.REQUIREMENTSDETAILS WHERE CLAIMNUMBER = '"
		//                + claimNumber + "' AND RequirementNotificationSent = 'N' AND RequirementReceiveTS IS NULL) tab) "
		//                + " ,BenefitDescription = ? "
		//                + " ,BenefitLengthOfStay = (SELECT STUFF (( SELECT '; ' + cast(CDD2.BenefitActualDays as VARCHAR(40)) + '' "
		//                + "                     FROM dbo.CLAIMDATADETAILS CDD2 "
		//                + "                                         WHERE CDD1.CLAIMNUMBER = CDD2.CLAIMNUMBER  "
		//                + "                                         ORDER BY CDD2.BENEFITCODE "
		//                + "                                         FOR XML PATH('')), 1,1,'') AS BENEFITID                   "
		//                + "                                         FROM dbo.CLAIMDATADETAILS CDD1 GROUP By CDD1.CLAIMNUMBER HAVING CDD1.CLAIMNUMBER = '"
		//                + claimNumber + "') "
		//                + " ,BenefitAmountIncurred = " + sqlAmtIncurred
		//                + " ,BenefitApproveAmount = " + sqlBenefitApprovedAmt
		//                + " ,BenefitUserApprovedAmount = " + sqlBenefitUsrApprovedAmt
		//                + " ,BenefitNotPaidAmout = " + sqlBenefitNotPaidAmt
		//                + " ,ComponentRiskCessDate = ?                                           " + " WHERE CLAIMNUMBER = '"
		//                + claimNumber + "' ";
		int i = 0;

		i = this.jdbcTemplateClaims.update(sql,
				new Object[]{Util.objToDate(dataIn.getClaimAdmissionDate(), "dd"),
						Util.objToDate(dataIn.getClaimDischargeDate(), ""), dataIn.getClaimUITotalAmount(),
						claimTypeUI, listLifeName, dataIn.getClaimProvider(), dataIn.getClaimDiagnoseCode(),
						listSubCategory, listBenefitName, compRiskCesDate});

		return i;
	}
	@Override
	public int updateClaimDataClaimPayDate( java.sql.Date ClaimPayDate, String claimNo)
			throws Exception{
		int i=0;
		String sql = "UPDATE CLAIMDATA SET ClaimPayDate = '"+ClaimPayDate+"'  WHERE CLAIMNUMBER = '" + claimNo + "' ";

		log.info("updateClaimDataClaimPayDate - sql is :"+ sql);

		//i = this.jdbcTemplateClaims.update(sql,new Object[]{Util.objToDate(ClaimPayDate, "")});
		i = this.jdbcTemplateClaims.update(sql);
		return i;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateClaimData(String claimNumber, String claimStatus, Timestamp processEndTS) throws Exception {
		// TODO Auto-generated method stub
		int i = 0;
		if (processEndTS != null) {
			sql = "update dbo.CLAIMDATA SET ClaimStatus = ?, ProcessEndTS = ? where ClaimNumber =  ?";
			try {
				i = this.jdbcTemplateClaims.update(sql, new Object[]{claimStatus, processEndTS, claimNumber});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else {
			sql = "update dbo.CLAIMDATA SET ClaimStatus = ? where ClaimNumber = ? ";
			try {
				i = this.jdbcTemplateClaims.update(sql, new Object[]{claimStatus, claimNumber});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int insertGenerateFileRequest(String claimNumber, String generateFile) throws Exception {
		// TODO Auto-generated method stub
		int i = 0;
		if (generateFile.equalsIgnoreCase("GENPAYMENTVOUCHER")) {
			int j = 0;
			sql = "select count(ClaimNumber) from GENERATEFILEREQUEST where GenerateFile = 'GENPAYMENTVOUCHER' and "
					+ " ClaimNumber = '" + claimNumber + "'";
			try {
				j = this.jdbcTemplateClaims.queryForObject(sql, Integer.class);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
			}

			if (j == 0) {
				sql = "INSERT into dbo.GENERATEFILEREQUEST(ClaimNumber,GenerateFile,RequestTS,ProcessedFlag,ExceptionRecord) "
						+ " VALUES(?, ?, ?,'N','N')";
				try {
					i = this.jdbcTemplateClaims.update(sql,
							new Object[]{claimNumber, generateFile, new Timestamp(System.currentTimeMillis())});
				} catch (Exception e) {
					// TODO Auto-generated catch block
					throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
				}
			}
		} else {

			sql = "INSERT into dbo.GENERATEFILEREQUEST(ClaimNumber,GenerateFile,RequestTS,ProcessedFlag,ExceptionRecord) "
					+ " VALUES(?, ?, ?,'N','N')";
			try {
				i = this.jdbcTemplateClaims.update(sql,
						new Object[]{claimNumber, generateFile, new Timestamp(System.currentTimeMillis())});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}
		return i;
	}

	@Override
	public String getClaimTypeUI(String claimNumber) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "select ClaimTypeUI from dbo.CLAIMDATA where ClaimNumber = '" + claimNumber + "'";
		String result = "";
		try {
			result = this.jdbcTemplateClaims.queryForObject(sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public int updateClaimApprovedAmountUI(BigDecimal iClaimApprovedAmtTPA, String claimNumber) throws DAOException {
		String sql = "update dbo.CLAIMDATA set ClaimApprovedAmountUI = ? WHERE ClaimNumber = ?";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{iClaimApprovedAmtTPA, claimNumber});
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int countRecords(String claimNumber) throws DAOException {
		String sql = "SELECT COUNT(*) FROM NOTIFICATIONDATA WHERE CLAIMNUMBER = ? ";
		log.info("countRecords sql is " + sql);
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber}, Integer.class);
			log.info("countRecords countis " + count);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}
@Override
public int countRecordsbasedOnPOSReqNo(String Posrqno) throws DAOException{
	String sql = "SELECT COUNT(*) FROM NOTIFICATIONDATA WHERE POSRequestNumber = ? ";
	log.info("countRecordsbasedOnPOSReqNo sql is " + sql);
	int count = 0;
	try {
		count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{Posrqno}, Integer.class);
		log.info("countRecordsbasedOnPOSReqNo record count is " + count);
	} catch (DataAccessException e) {
		// TODO Auto-generated catch block
		throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
	}
	return count;
}
	@Override
	public int countgetClaimDetails(String strOwnerClientID) throws DAOException {
		String sql = "SELECT Count(*) FROM CLAIMDATA WHERE OwnerClientID = ? ";
		log.info("countgetClaimDetails  sql is " + sql);
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{strOwnerClientID}, Integer.class);
			log.info("countRecords countis " + count);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	public int insertNotificationData(String POSRequestNumber, String PolicyNumber, String PlanCode, String ClaimTypeLA,
			String ClientInsuredName, String ClientInsuredNumber, String ProductCode,
			BigDecimal ClaimUITotalClaimAmount, String PolicyOwnerName, String PolicyOwnerAddress1,
			String PolicyOwnerAddress2, String PolicyOwnerAddress3, String PolicyOwnerAddress4,
			String PolicyOwnerAddress5, String PolicyOwnerEmailAddress, String PolicyOwnerPhoneNumber,
			String BeneficiaryName, String BeneficiaryAddress1, String BeneficiaryAddress2, String BeneficiaryAddress3,
			String BeneficiaryAddress4, String BeneficiaryAddress5, java.util.Date ClaimProcessInitiationDate,
			BigDecimal PolicyRegularTopup, String PolicyPaymentFreq, String AgentEmailAddress, String SDEmailAddress,
			String NSDEmailAddress, String ApplicationNumber,String claimTypeLAName) throws DAOException {
		// TODO Auto-generated method stub
		log.info("insertNotificationData   PolicyOwnerName is " + PolicyOwnerName);
		String sql = "INSERT INTO NOTIFICATIONDATA (POSRequestNumber,PolicyNumber,ApplicationNumber,PlanCode,ClaimTypeLA,"
				+ "ClientInsuredName,ClientInsuredNumber,ProductCode,"
				+ "ClaimUITotalClaimAmount,PolicyOwnerName,PolicyOwnerAddress1," + "PolicyOwnerAddress2,"
				+ "PolicyOwnerAddress3," + "PolicyOwnerAddress4," + "PolicyOwnerAddress5,"
				+ "PolicyOwnerEmailAddress,PolicyOwnerPhoneNumber,BeneficiaryName,"
				+ "BeneficiaryAddress1,BeneficiaryAddress2,BeneficiaryAddress3,BeneficiaryAddress4,"
				+ "BeneficiaryAddress5,ClaimProcessInitiationDate,PolicyRegularTopup,PolicyPaymentFreq,AgentEmailAddress,"
				+ "SDEmailAddress,NSDEmailAddress,ClaimTypeLAName)  "
				+ " VALUES (?," + "?,?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?,?," + "?,?,"
				+ "?,?," + "?,?," + "?,?," + "?,?," + "?,?," + "?,?,?,?) ";
		log.info("in insertNotificationData - sql is - " + sql);
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql,
					new Object[]{POSRequestNumber, PolicyNumber,ApplicationNumber, PlanCode, ClaimTypeLA, ClientInsuredName,
							ClientInsuredNumber, ProductCode, ClaimUITotalClaimAmount, PolicyOwnerName,
							PolicyOwnerAddress1, PolicyOwnerAddress2, PolicyOwnerAddress3, PolicyOwnerAddress4,
							PolicyOwnerAddress5, PolicyOwnerEmailAddress, PolicyOwnerPhoneNumber, BeneficiaryName,
							BeneficiaryAddress1, BeneficiaryAddress2, BeneficiaryAddress3, BeneficiaryAddress4,
							BeneficiaryAddress5, ClaimProcessInitiationDate, PolicyRegularTopup, PolicyPaymentFreq,
							AgentEmailAddress, SDEmailAddress, NSDEmailAddress,claimTypeLAName});
			log.info("in insertNotificationData - sql is - " + sql);
			log.info("in insertNotificationData - after firing sql result  is - " + i);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateNotificationDataFromfetchPol(String PolicyNumber, String PlanCode, String ClaimTypeLA,
			String ClientInsuredName, String ClientInsuredNumber, String ProductCode,
			BigDecimal ClaimUITotalClaimAmount, String PolicyOwnerName, String PolicyOwnerAddress1,
			String PolicyOwnerAddress2, String PolicyOwnerAddress3, String PolicyOwnerAddress4,
			String PolicyOwnerAddress5, String PolicyOwnerEmailAddress, String PolicyOwnerPhoneNumber,
			String BeneficiaryName, String BeneficiaryAddress1, String BeneficiaryAddress2, String BeneficiaryAddress3,
			String BeneficiaryAddress4, String BeneficiaryAddress5, java.util.Date ClaimProcessInitiationDate,
			BigDecimal PolicyRegularTopup, String PolicyPaymentFreq, String AgentEmailAddress, String SDEmailAddress,
			String NSDEmailAddress, String POSRequestNumber,String ApplicationNumber,String claimTypeLAName) {
		// TODO Auto-generated method stub
		log.info("updateNotificationDataFromfetchPol PolicyOwnerName is - " + PolicyOwnerName);
		String sql = "UPDATE NOTIFICATIONDATA SET PolicyNumber=?,ApplicationNumber=?," + "PlanCode=?," + "ClaimTypeLA=?,"
				+ "ClientInsuredName=?," + "ClientInsuredNumber=?," + "ProductCode=?," + "ClaimUITotalClaimAmount=?,"
				+ "PolicyOwnerName=?,"
				+ "PolicyOwnerAddress1=?,PolicyOwnerAddress2=?,PolicyOwnerAddress3=?,PolicyOwnerAddress4=?,"
				+ "PolicyOwnerAddress5=?," + "PolicyOwnerEmailAddress=?,"
				+ "PolicyOwnerPhoneNumber=?,BeneficiaryName=?,BeneficiaryAddress1=?,BeneficiaryAddress2=?,"
				+ "BeneficiaryAddress3=?," + "BeneficiaryAddress4=?,BeneficiaryAddress5=?,ClaimProcessInitiationDate=?,"
				+ "PolicyRegularTopup=?,PolicyPaymentFreq=?,AgentEmailAddress=?,SDEmailAddress=?,"
				+ "NSDEmailAddress=?,ClaimTypeLAName=?  WHERE CLAIMNUMBER =?";
		log.info("in updateNotificationDataFromfetchPol - sql is - " + sql);
		int i = this.jdbcTemplateClaims.update(sql,
				new Object[]{PolicyNumber, ApplicationNumber,PlanCode, ClaimTypeLA, ClientInsuredName, ClientInsuredNumber, ProductCode,
						ClaimUITotalClaimAmount, PolicyOwnerName, PolicyOwnerAddress1, PolicyOwnerAddress2,
						PolicyOwnerAddress3, PolicyOwnerAddress4, PolicyOwnerAddress5, PolicyOwnerEmailAddress,
						PolicyOwnerPhoneNumber, BeneficiaryName, BeneficiaryAddress1, BeneficiaryAddress2,
						BeneficiaryAddress3, BeneficiaryAddress4, BeneficiaryAddress5, ClaimProcessInitiationDate,
						PolicyRegularTopup, PolicyPaymentFreq, AgentEmailAddress, SDEmailAddress, NSDEmailAddress,claimTypeLAName,
						POSRequestNumber});
		log.info("in updateNotificationDataFromfetchPol - sql result is - " + i);
		return i;

	}

	@Override
	public String getInProgressClaimNo(String claimNumber, String claimStatus, String Polno) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		try {

			int i = 0;
			Integer myInteger;
			sql = "SELECT COUNT(*) FROM CLAIMDATA WHERE ClaimNumber <> ? AND ClaimStatus <> ? AND PolicyNumber=?";
			log.info("first sql is - " + sql);
			myInteger = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber, claimStatus, Polno},
					Integer.class);
			i = myInteger.intValue();
			log.info("after first sql  i is - " + String.valueOf(i));
			if (i > 0) {
				// sql = "SELECT ClaimNumber FROM CLAIMDATA WHERE" + "
				// ClaimNumber <> ? AND "
				// + "ClaimStatus <> ? AND PolicyNumber=? ";
				sql = "SELECT STUFF((SELECT ',' + ClaimNumber + ''  FROM CLAIMDATA WHERE ClaimNumber <> ? "
						+ " AND ClaimStatus <> ? AND PolicyNumber= ? FOR XML PATH ('')), 1,1,'') "
						+ " AS CLAIMNUMBERLIST";

				log.info("getInProgressClaimNo sql is - " + sql);
				result = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNumber, claimStatus, Polno},
						String.class);
				log.info("getInProgressClaimNo sql is -" + sql);
			}

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			log.error("getInProgressClaimNo - error ");
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}

		return result;
	}
@Override
public String getInProgressPOSRequestNo(String posreqno, String posStatus, String Polno) throws DAOException{
	// TODO Auto-generated method stub
			String result = "";
			try {

				int i = 0;
				Integer myInteger;
				sql = "SELECT COUNT(*) FROM POSDATA WHERE POSRequestNumber <> ? AND POSStatus <> ? AND POSStatus=?";
				log.info("getInProgressPOSRequestNo: first sql is - " + sql);
				myInteger = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{posreqno, posStatus, Polno},
						Integer.class);
				i = myInteger.intValue();
				log.info("after first sql  i is - " + String.valueOf(i));
				if (i > 0) {
					// sql = "SELECT ClaimNumber FROM CLAIMDATA WHERE" + "
					// ClaimNumber <> ? AND "
					// + "ClaimStatus <> ? AND PolicyNumber=? ";
					sql = "SELECT STUFF((SELECT ',' + POSRequestNumber + ''  FROM POSDATA WHERE POSRequestNumber <> ? "
							+ " AND POSStatus <> ? AND PolicyNumber= ? FOR XML PATH ('')), 1,1,'') "
							+ " AS CLAIMNUMBERLIST";

					log.info("getInProgressPOSRequestNo 2nd sql is - " + sql);
					result = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{posreqno, posStatus, Polno},
							String.class);
					log.info("getInProgressPOSRequestNo sql is -" + sql);
				}

			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				log.error("getInProgressPOSRequestNo - error ");
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

			}

			return result;
}
	@Override
	public String getComponentCodeStatus(String ClientNo, String PolNo, String CompCode) throws DAOException {
		// TODO Auto-generated method stub
		String result = "";
		try {

			int i = 0;
			Integer myInteger;
			String sql = "SELECT COUNT(*) FROM V_COMPONENTDETAILS WHERE POLICYNUMBER=? and "
					+ "CLIENTNUMBER =? and COMPONENTCODE = ? and COMPONENTSTATUS is not NULL ";
			log.info("first sql is - " + sql);
			myInteger = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{PolNo, ClientNo, CompCode},
					Integer.class);
			i = myInteger.intValue();
			log.info("getComponentCodeStatus after first sql  i is - " + String.valueOf(i));
			if (i > 0) {
				sql = "SELECT COMPONENTSTATUS FROM V_COMPONENTDETAILS WHERE POLICYNUMBER=? and "
						+ "CLIENTNUMBER =? and COMPONENTCODE = ?";
				log.info("getComponentCodeStatus sql is - " + sql);
				result = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{PolNo, ClientNo, CompCode},
						String.class);
				log.info("getComponentCodeStatus sql is -" + sql);
			}

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			log.error("getComponentCodeStatus - error ");
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);

		}

		return result;
	}

	@Override
	public int getPendingClaimCount(String claimNo, String claimStatus, String polNo) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "select count(*) from dbo.CLAIMDATA where ClaimNumber <> ? and ClaimStatus<> ? and "
				+ "POLICYNUMBER = ?";
		int count = 0;
		try {
			count = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimNo, claimStatus, polNo},
					Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	public List<UWCommentsModel> getUWComments(String polNum, String sourceOfComment) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "select * from dbo.UWCOMMENTS where PolicyNumber = '" + polNum + "' and SourceOfComment= '"
				+ sourceOfComment + "' order by UWCommentTS desc ";
		List<UWCommentsModel> comments_li = new ArrayList<UWCommentsModel>();

		try {
			comments_li = this.jdbcTemplateClaims.query(sql,
					new BeanPropertyRowMapper<UWCommentsModel>(UWCommentsModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return comments_li;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public int insertUWComments(UWCommentsModel uwModel) throws DAOException {
		// TODO Auto-generated method stub
		String sql = "";
		int i = 0;
		try {
			if (uwModel.getSourceOfComment().equalsIgnoreCase("CL")) {
				sql = "INSERT INTO dbo.UWCOMMENTS "
						+ " ( PolicyNumber, ClaimNumber,  UWComment, UWCommentBy, UWCommentTS, SourceOfComment) "
						+ " VALUES(?, ?, ?, ?, ?, ?) ";
				i = this.jdbcTemplateClaims.update(sql,
						new Object[]{uwModel.getPolicyNumber(), uwModel.getClaimNumber(), uwModel.getUWComment(),
								uwModel.getUWCommentBy(), new Timestamp(System.currentTimeMillis()),
								uwModel.getSourceOfComment()});
			} else if (uwModel.getSourceOfComment().equalsIgnoreCase("PS")) {
				sql = "INSERT INTO dbo.UWCOMMENTS "
						+ " (POSRequestNumber,PolicyNumber,  UWComment, UWCommentBy, UWCommentTS, SourceOfComment) "
						+ " VALUES(?, ?, ?, ?, ?, ?) ";
				i = this.jdbcTemplateClaims.update(sql,
						new Object[]{uwModel.getPOSRequestNumber(), uwModel.getPolicyNumber(), uwModel.getUWComment(),
								uwModel.getUWCommentBy(), new Timestamp(System.currentTimeMillis()),
								uwModel.getSourceOfComment()});

			} else if (uwModel.getSourceOfComment().equalsIgnoreCase("NB")) {
				sql = "INSERT INTO dbo.UWCOMMENTS "
						+ " (AppNumber,PolicyNumber,  UWComment, UWCommentBy, UWCommentTS, SourceOfComment) "
						+ " VALUES(?, ?, ?, ?, ?, ?) ";
				i = this.jdbcTemplateClaims.update(sql,
						new Object[]{uwModel.getAppNumber(), uwModel.getPolicyNumber(), uwModel.getUWComment(),
								uwModel.getUWCommentBy(), new Timestamp(System.currentTimeMillis()),
								uwModel.getSourceOfComment()});

			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public int updateNotificationData(String claimNumber, String requirementDocNRSubCategoryList) throws DAOException {
		// TODO Auto-generated method stub
		String sql = " UPDATE NOTIFICATIONDATA SET RequirementDocNRTextList =  "
				+ " (SELECT STUFF (( SELECT '; ' +RQ2.RequirementText FROM REQUIREMENTSDETAILS RQ2 WHERE RQ1.CLAIMNUMBER = RQ2.CLAIMNUMBER  "
				+ " AND RQ2.RequirementNotificationSent = 'Y' AND RQ2.RequirementReceiveTS IS NULL ORDER BY RQ2.RequirementText  "
				+ " FOR XML PATH('')), 1,1,'') AS REQTEXT FROM REQUIREMENTSDETAILS RQ1 GROUP By RQ1.CLAIMNUMBER  "
				+ " HAVING RQ1.CLAIMNUMBER = '" + claimNumber + "') " + ", RequirementDocNRSubCategoryList = ? "
				+ " , PDFLetterGeneratedDate = (SELECT Max(PDFLETTERGENERATEDTS) FROM NOTIFICATION WHERE  "
				+ " CLAIMNUMBER =  '" + claimNumber
				+ "' AND NOTIFICATIONDECISION = 'PENDING DOCUMENT' AND PDFLetterGenerated = 'Y') "
				+ " , DocumentNotReceivedDate30 = (( SELECT DISTINCT(RequirementRequestTS)  FROM REQUIREMENTSDETAILS WHERE CLAIMNUMBER =  '"
				+ claimNumber + "'  "
				+ "  AND RequirementNotificationSent = 'N' AND RequirementReceiveTS IS NULL) + 30) "
				+ " WHERE CLAIMNUMBER = '" + claimNumber + "' ";
		int i = 0;

		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{requirementDocNRSubCategoryList});
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}
	@Override
	public int updateNotificationDataClientDetails(String PolicyOwnerAddress1, String PolicyOwnerAddress2,
			String PolicyOwnerAddress3,String PolicyOwnerAddress4,String PolicyOwnerAddress5,
			String PolicyOwnerEmailAddress,String PolicyOwnerPhoneNumber,
			String ClaimNo) throws DAOException{
		String sql = "update NOTIFICATIONDATA set PolicyOwnerAddress1 = ?,"
				+ " PolicyOwnerAddress2 = ?, PolicyOwnerAddress3 = ?, "
				+ "PolicyOwnerAddress4 = ?, PolicyOwnerAddress5 = ?, "
				+ " PolicyOwnerEmailAddress =?, PolicyOwnerPhoneNumber = ? "
				+ "where ClaimNumber = ?"
				;
		int i = 0;
		log.info("updateNotificationDataClientDetails sql query is ---"+sql);
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{PolicyOwnerAddress1,PolicyOwnerAddress2,
					PolicyOwnerAddress3,PolicyOwnerAddress4,PolicyOwnerAddress5,
					PolicyOwnerEmailAddress,PolicyOwnerPhoneNumber,ClaimNo});
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("updateNotificationDataClientDetails  "
					+ "Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}
	@Override
	public int updateNotificationDataBenDetails(String BeneficiaryAddress1, String BeneficiaryAddress2,
			String BeneficiaryAddress3,String BeneficiaryAddress4,String BeneficiaryAddress5,
			String ClaimNo) throws DAOException{
		String sql = "update NOTIFICATIONDATA set BeneficiaryAddress1 = ?,"
				+ " BeneficiaryAddress2 = ?, BeneficiaryAddress3 = ?, "
				+ "BeneficiaryAddress4 = ?, BeneficiaryAddress5 = ?  "
				+ "where ClaimNumber = ?"
				;
		int i = 0;
		log.info("updateNotificationDataBenDetails sql query is ---"+sql);
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{BeneficiaryAddress1,BeneficiaryAddress2,
					BeneficiaryAddress3,BeneficiaryAddress4,BeneficiaryAddress5,
					ClaimNo});
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("updateNotificationDataBenDetails  "
					+ "Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;	
	}
	@Override
	public String getDocIDList(String claimNumber, String reqNotifSent) throws DAOException {
		// TODO Auto-generated method stub

		String result = "('')";
		List<String> result_li = new ArrayList<String>();

		String sql = "(SELECT STUFF (( SELECT ',( ''' + RQ2.DOCID + ''')' "
				+ "                     FROM dbo.REQUIREMENTSDETAILS RQ2 "
				+ "                                         WHERE RQ1.CLAIMNUMBER = RQ2.CLAIMNUMBER AND RQ2.RequirementNotificationSent = ? AND RQ2.RequirementReceiveTS IS NULL "
				+ "                                         ORDER BY RQ2.DOCID "
				+ "                                         FOR XML PATH('')), 1,1,'') AS DOCUMENTID "
				+ " FROM REQUIREMENTSDETAILS RQ1 GROUP By RQ1.CLAIMNUMBER HAVING RQ1.CLAIMNUMBER = ? ) ";
		try {
			result_li = this.jdbcTemplateClaims.queryForList(sql, new Object[]{reqNotifSent, claimNumber},
					String.class);
			if (result_li != null && result_li.size() > 0) {
				result = result_li.get(0);
			}
			if (result == null || result.trim() == "" || result.trim().equalsIgnoreCase("")) {
				result = "('')";
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL	 Query = " + sql);
		}

		return result;
	}

	@Override
	public int lARequestTracker_Insert(String claimnumber, String activityid, String boidentifier, String msgtosend,
			String futuremesg, java.sql.Date futureDate) throws DAOException {
		// TODO Auto-generated method stub

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;

		log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert");
		log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert " + claimnumber + " " + activityid + " "
				+ boidentifier + " " + msgtosend + " " + futuremesg);

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try {
			queryUpdateCount = jdbcTemplateClaims.update(
					"INSERT INTO  LAREQUESTTRACKER(claimnumber,"
							+ " activityid,boidentifier,msgcreatets,msgtosend,futuremesg,futuredate,flag_processed)"
							+ " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?,?,0)",
							claimnumber, activityid, boidentifier, timestamp, msgtosend, futuremesg, futureDate);
			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert - "
						+ "No rows inserted for claim number = " + claimnumber);

			} else {
				log.info("ClaimDetailsDaoImpl - In Method - lARequestTracker_Insert - "
						+ "Rows inserted for claim number = " + claimnumber + " are = " + queryUpdateCount);
				log.info("ClaimDetailsDaoImpl - Success - lARequestTracker_Insert - Inserted BO ");
			}

			log.debug("ClaimDetailsDaoImpl - Return From  Method - lARequestTracker_Insert");
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - lARequestTracker_Insert" + e.getMessage());
			log.error(e.fillInStackTrace());
			throw new DAOException(e.getMessage() + "Unable to insert data into LAREQUESTTRACKER table, SQL = "
					+ "INSERT INTO  LAREQUESTTRACKER(claimnumber,"
					+ " activityid,boidentifier,msgcreatets,msgtosend,futuremesg)"
					+ " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?)");
		}
		return queryUpdateCount;
	}

	@Override
	public int insertCLAIMDATASOURCEPORTAL(String PortalClaimRequestNumber, Date PortalClaimRequestCreationDate,
			String PolicyNumber, String OwnerClientID, String PayeeName, String InsuredClientName,
			String InsuredClientID, String FormClaimType, String FormBenefitType, String SurgeryPerformed,
			Date TreatmentStartDate, Date TreatmentCompletionDate, String HospitalName, String FormCurrency,
			BigDecimal TotalBillingAmount, String Physician, String BankAccountNumber, String PaymentAccountOwnerName,
			String ProcessRecord) throws DAOException {
		// TODO Auto-generated method stub

		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		int queryInsertCount = 0;
		log.info("ClaimDetailsDaoImpl - In Method - insertCLAIMDATASOURCEPORTAL");
		System.out.println("ClaimDetailsDaoImpl - In Method - insertCLAIMDATASOURCEPORTAL");
		try {
			/*
			 * queryUpdateCount = jdbcTemplateClaims.update(
			 * "INSERT INTO  LAREQUESTTRACKER(claimnumber," +
			 * " activityid,boidentifier,msgcreatets,msgtosend,futuremesg,futuredate)"
			 * + " VALUES (?,?,?,CONVERT(DATETIME,?,105),?,?,?)", claimnumber,
			 * activityid, boidentifier, timestamp, msgtosend, futuremesg,
			 * futureDate);
			 */
			String expRecord = "N";
			sql = "INSERT INTO CLAIMDATASOURCEPORTAL(ExceptionRecord,PortalClaimRequestNumber,PortalClaimRequestCreationDate,PolicyNumber,OwnerClientID,PayeeName,"
					+ "InsuredClientName,InsuredClientID,FormClaimType,FormBenefitType,SurgeryPerformed,TreatmentStartDate,"
					+ "TreatmentCompletionDate, HospitalName,FormCurrency,TotalBillingAmount,Physician,BankAccountNumber,PaymentAccountOwnerName,ProcessRecord,RecordCreateTS) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,CONVERT(DATETIME,?,105))";
			log.info("ClaimDetailsDaoImpl - In Method - insertCLAIMDATASOURCEPORTAL b4 executing sql is -" + sql);
			queryInsertCount = jdbcTemplateClaims.update(sql, expRecord, PortalClaimRequestNumber,
					PortalClaimRequestCreationDate, PolicyNumber, OwnerClientID, PayeeName, InsuredClientName,
					InsuredClientID, FormClaimType, FormBenefitType, SurgeryPerformed, TreatmentStartDate,
					TreatmentCompletionDate, HospitalName, FormCurrency, TotalBillingAmount, Physician,
					BankAccountNumber, PaymentAccountOwnerName, ProcessRecord, timestamp);
			System.out.println("queryInsertCount" + queryInsertCount);
			transactionManager.commit(txStatus);
			if (queryInsertCount == 0) {
				log.info("ClaimDetailsDaoImpl - In Method - insertCLAIMDATASOURCEPORTAL - " + "No rows inserted");
				System.out.println("queryInsertCount No rows inserted ");
			} else {
				log.info("ClaimDetailsDaoImpl - In Method - insertCLAIMDATASOURCEPORTAL - " + "Number  rows inserted -"
						+ queryInsertCount);
				System.out.println("queryInsertCount rows inserted ");
			}

		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - In Method - insertCLAIMDATASOURCEPORTAL" + e.getMessage());
			log.error(e.fillInStackTrace());
			System.out.println(e.fillInStackTrace());
			throw new DAOException(
					e.getMessage() + "Unable to insert data into CLAIMDATASOURCEPORTAL table, SQL = " + sql);
		}
		return queryInsertCount;
	}

	@Override
	public String getComparisonOperation(String claimTypeUI, String userDecision, String claimUser)
			throws DAOException {
		// TODO Auto-generated method stub
		this.sql = "SELECT ComparisionOperation FROM AUTHORITYMATRIX WHERE ClaimTypeUI = '" + claimTypeUI
				+ "' AND UserDecision = '" + userDecision + "' AND ClaimUser = '" + claimUser + "'";
		String result = "";
		try {
			result = this.jdbcTemplateClaims.queryForObject(this.sql, String.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return result;
	}

	@Override
	public List<AuthorityMatrixModel> getAuthorityMatrixData(String claimTypeUI, String userDecision, String claimUser)
			throws DAOException {
		// TODO Auto-generated method stub]
		log.info("getAuthorityMatrixData claimTypeUI is : "+ claimTypeUI+
				"  UserDecision  is : "+userDecision+ " claimUser is : "+ claimUser);
		sql = "SELECT * FROM AUTHORITYMATRIX WHERE ClaimTypeUI = '" + claimTypeUI + "' AND UserDecision = '"
				+ userDecision + "' AND lower(ClaimUser) = lower('" + claimUser + "')";
		log.info("getAuthorityMatrixData sql is :"+ sql );
		List<AuthorityMatrixModel> matrixModel_li = new ArrayList<AuthorityMatrixModel>();

		try {
			matrixModel_li = this.jdbcTemplateClaims.query(sql,
					new BeanPropertyRowMapper<AuthorityMatrixModel>(AuthorityMatrixModel.class));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return matrixModel_li;
	}

	@Override
	public int getTotalAuthorityMatrix(ClaimDataNewModel claimData, String claimUser, String userDecision,
			String comparison) throws DAOException {
		// TODO Auto-generated method stub
		int i = 0;
		sql = "SELECT COUNT(*) FROM AUTHORITYMATRIX WHERE lower(ClaimUser) = lower(?) AND UserDecision = ? "
				+ " AND  ? " + comparison + " TotalClaimAmount AND ClaimTypeUI = ? ";
		log.info("getTotalAuthorityMatrix - sql string = " + sql);
		try {
			i = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{claimUser, userDecision,
					claimData.getClaimApprovedAmountUI(), claimData.getClaimTypeUI()}, Integer.class);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;

	}

	@Override
	public List<BenefitNotPaidAmount> getBenefitNotPaidAmountList(String claimNumber) throws DAOException {
		// TODO Auto-generated method stub
		List<BenefitNotPaidAmount> result = new ArrayList<BenefitNotPaidAmount>();
		sql = "select (case when CDD2.BenefitNotApprovedAmount is null then 0 "
				+ " else CDD2.BenefitNotApprovedAmount end) as BenefitNotPaidAmount "
				+ " FROM CLAIMDATADETAILS CDD2  WHERE CDD2.CLAIMNUMBER = '" + claimNumber
				+ "'  ORDER BY CDD2.BenefitCode ";
		try {
			result = this.jdbcTemplateClaims.query(sql,
					new BeanPropertyRowMapper<BenefitNotPaidAmount>(BenefitNotPaidAmount.class));

		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return result;
	}

	@Override
	public int updateClaimStatus(String claimNumber, String claimStatus) throws Exception {
		// TODO Auto-generated method stub
		sql = "update dbo.CLAIMDATA SET ClaimStatus = 'COMPLETE', ProcessEndTS = getDate() where ClaimNumber =  '"
				+ claimNumber + "'";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return 0;
	}

/*	@Override
	public void updatedClaimStatusInClaimData(WSUpdateClaimStatusDataIn jsonData, Timestamp piTs) throws DAOException {
		// TODO Auto-generated method stub
		TransactionDefinition txDef = new DefaultTransactionDefinition();
		TransactionStatus txStatus = transactionManager.getTransaction(txDef);
		int queryUpdateCount = 0;
		log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData");
		log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData Data ClaimsData" + jsonData);

		try {
			if (jsonData.getClaimstatus().trim().equalsIgnoreCase("RUNNING") && piTs == null) {
				sql = "update CLAIMDATA set ClaimStatus = '" + jsonData.getClaimstatus().toString()
						+ "', ProcessInitiatonTS = ?";
				sql = sql + " where ClaimNumber = '" + jsonData.getClaimNo().toString() + "'";
				log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData - SQL Query = "
						+ sql.toString());
				piTs = new Timestamp(System.currentTimeMillis());
				queryUpdateCount = jdbcTemplateClaims.update(sql, new Object[]{piTs});
			} else {
				sql = "update CLAIMDATA set ClaimStatus = '" + jsonData.getClaimstatus().toString() + "' ";
				sql = sql + "where ClaimNumber = '" + jsonData.getClaimNo().toString() + "'";
				log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData - SQL Query = "
						+ sql.toString());

				queryUpdateCount = jdbcTemplateClaims.update(sql);
			}

			transactionManager.commit(txStatus);
			if (queryUpdateCount == 0) {
				log.info(
						"ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData - No rows updated for claim number = "
								+ jsonData.getClaimNo().toString());
				throw new DAOException("No Row Updated in Claim Data table, with claimNo= " + jsonData.getClaimNo());
			} else {
				log.info(
						"ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData - Rows updated for claim number = "
								+ jsonData.getClaimNo().toString() + " are = " + queryUpdateCount);
			}

		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - updatedClaimStatusInClaimData" + e.getMessage());
			throw new DAOException(e.getMessage() + " SQL = " + sql);
		}
		log.info("ClaimDetailsDaoImpl - Success - updatedClaimStatusInClaimData - Updated ClaimDetails");
		log.info("ClaimDetailsDaoImpl - Return From  Method - updatedClaimStatusInClaimData");

	}*/
@Override

public void  updatedClaimStatusInPOSData(WSUpdateClaimStatusDataIn jsonData, Timestamp piTs) throws DAOException
{
	TransactionDefinition txDef = new DefaultTransactionDefinition();
	TransactionStatus txStatus = transactionManager.getTransaction(txDef);
	int queryUpdateCount = 0;
	log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInPOSData");
	log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInPOSData Data POSData" + jsonData);

	try {
		if (jsonData.getClaimstatus().trim().equalsIgnoreCase("RUNNING") && piTs == null) {
			sql = "update POSDATA set POSStatus = '" + jsonData.getClaimstatus().toString()
					+ "', ProcessInitiatonTS = ?";
			sql = sql + " where POSRequestNumber = '" + jsonData.getPosRequestNumber().toString() + "'";
			log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInClaimData - SQL Query = "
					+ sql.toString());
			piTs = new Timestamp(System.currentTimeMillis());
			queryUpdateCount = jdbcTemplateClaims.update(sql, new Object[]{piTs});
		} else {
			sql = "update POSDATA set POSStatus = '" + jsonData.getClaimstatus().toString() + "' ";
			sql = sql + "where POSRequestNumber = '" + jsonData.getPosRequestNumber().toString() + "'";
			log.info("ClaimDetailsDaoImpl - In Method - updatedClaimStatusInPOSData - SQL Query = "
					+ sql.toString());

			queryUpdateCount = jdbcTemplateClaims.update(sql);
		}

		transactionManager.commit(txStatus);
		if (queryUpdateCount == 0) {
			log.info(
					"ClaimDetailsDaoImpl - In Method - updatedClaimStatusInPOSData - No rows updated for claim number = "
							+ jsonData.getPosRequestNumber().toString());
			throw new DAOException("No Row Updated in Claim Data table, with claimNo= " + jsonData.getPosRequestNumber());
		} else {
			log.info(
					"ClaimDetailsDaoImpl - In Method - updatedClaimStatusInPOSData - Rows updated for claim number = "
							+ jsonData.getPosRequestNumber().toString() + " are = " + queryUpdateCount);
		}

	} catch (Exception e) {
		log.error("ClaimDetailsDaoImpl - Error - updatedClaimStatusInPOSData" + e.getMessage());
		throw new DAOException(e.getMessage() + " SQL = " + sql);
	}
	log.info("ClaimDetailsDaoImpl - Success - updatedClaimStatusInPOSData - Updated ClaimDetails");
	log.info("ClaimDetailsDaoImpl - Return From  Method - updatedClaimStatusInPOSData");

}
	@Override
	public int updatedClaimStatusInClaimData(String claimNumber, String status, String activityId) throws DAOException {
		// TODO Auto-generated method stub
		sql = "update dbo.CLAIMDATA SET PendingRequirementActivityID = '" + activityId + "', ClaimStatus = '" + status
				+ "' where ClaimNumber =  '" + claimNumber + "'";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.update(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return 0;
	}

	@Override
	public int getCountRequirementDetailTable(String searchValue, String docID, String searchtype) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select count(*) from dbo.REQUIREMENTSDETAILS where " + searchtype + " = '" + searchValue
				+ "' and DocID = '" + docID + "' and RequirementStatus <> 'RECEIVED' ";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.queryForObject(sql, Integer.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int getCountRequirementDetailTable(String searchValue, String policyNumber, String docID, String searchtype)
			throws DAOException {
		// TODO Auto-generated method stub
		sql = "select count(*) from dbo.REQUIREMENTSDETAILS where " + searchtype + " = '" + searchValue
				+ "' and DocID = '" + docID + "' and PolicyNumber = '" + policyNumber + "' and RequirementStatus <> 'RECEIVED'";
		int i = 0;
		try {
			i = this.jdbcTemplateClaims.queryForObject(sql, Integer.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateRequirementDetails(String claimNumber, Timestamp reqReceiveTs, String reqStatus, String docId)
			throws Exception {
		// TODO Auto-generated method stub
		int i = 0;
		sql = "update dbo.REQUIREMENTSDETAILS set RequirementReceiveTS = ?, RequirementStatus = ? "
				+ " where ClaimNumber = ? and DocID = ? and RequirementStatus <> '"+reqStatus+"' ";
		try {
			i = this.jdbcTemplateClaims.update(sql, new Object[]{reqReceiveTs, reqStatus, claimNumber, docId});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int updateRequirementDetails(String claimNumber, String polNum, Timestamp reqReceiveTs, String reqStatus,
			String docId, String type, String typeValue) throws Exception {
		// TODO Auto-generated method stub
		int i = 0;
		sql = "update dbo.REQUIREMENTSDETAILS set RequirementReceiveTS = ?, RequirementStatus = ?, ClaimNumber  = ?, PolicyNumber = ? "
				+ " where " + type + " = ? and DocID = ? and RequirementStatus <> 'RECEIVED' ";
		try {
			i = this.jdbcTemplateClaims.update(sql,
					new Object[]{reqReceiveTs, reqStatus, claimNumber, polNum, typeValue, docId});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return i;
	}

	@Override
	public int getCountClaimDataTable(String type, Object typeValue, String typeDataType) throws DAOException {
		// TODO Auto-generated method stub
		int i = 0;
		if (typeDataType.equalsIgnoreCase("String")) {
			String str = (String) typeValue;
			sql = "select count(*) from dbo.CLAIMDATA where " + type + " = ?";
			try {
				i = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{str}, Integer.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		} else if (typeDataType.equalsIgnoreCase("int")) {
			int str = (int) typeValue;
			sql = "select count(*) from dbo.CLAIMDATA where " + type + " = ?";
			try {
				i = this.jdbcTemplateClaims.queryForObject(sql, new Object[]{str}, Integer.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
		}

		return i;
	}

	@Override
	public String getClaimNumberInClaimData(String type, Object typeValue, String dataTypeType) throws DAOException {
		// TODO Auto-generated method stub
		sql = "select ClaimNumber from dbo.CLAIMDATA where " + type + " = ?";
		if (dataTypeType.equalsIgnoreCase("String")) {
			String str = (String) typeValue;
			List<String> str_li = new ArrayList<String>();
			try {
				str_li = this.jdbcTemplateClaims.queryForList(sql, new Object[]{str}, String.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
			}
			if (str_li != null && str_li.size() > 0) {
				return str_li.get(0);
			} else {
				return "";
			}
		} else {
			return "";
		}

	}

	@Override
	public int updateClaimDataGeneral(String claimNumber, String type, Object typeValue, String typetype)
			throws Exception {
		// TODO Auto-generated method stub
		String sql = "update dbo.CLAIMDATA set " + type + " = ? WHERE ClaimNumber = '" + claimNumber + "'";
		int i = 0;

		try {
			if (typetype.equalsIgnoreCase("String")) {
				String obj = (String) typeValue;
				i = this.jdbcTemplateClaims.update(sql, new Object[]{obj});
			} else if (typetype.equalsIgnoreCase("BigDecimal")) {
				Double obj = (Double) typeValue;
				BigDecimal objdec = new BigDecimal(obj);
				i = this.jdbcTemplateClaims.update(sql, new Object[]{objdec});
			} else if (typetype.equalsIgnoreCase("int")) {
				int obj = (int) typeValue;
				i = this.jdbcTemplateClaims.update(sql, new Object[]{obj});
			} else if (typetype.equalsIgnoreCase("Timestamp") || typetype.equalsIgnoreCase("date")) {
				String obj = (String) typeValue;
				Timestamp ts = Util.convertToTs(obj);
				i = this.jdbcTemplateClaims.update(sql, new Object[]{ts});
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return i;
	}

	@Override
	public List<NotificationProcessMasterModel> getNotificationProcess(String processName, String processActivityName,
			String decision) throws DAOException {
		// TODO Auto-generated method stub
		List<NotificationProcessMasterModel> notifProcess_li = new ArrayList<NotificationProcessMasterModel>();
		this.sql = "select * from  NOTIFICATIONPROCESSMASTER where NotificationProcessName = '" + processName
				+ "' and NotificationActivityName = '" + processActivityName + "' " + "  and NotificationDecision = '"
				+ decision + "' ";
		try {
			notifProcess_li = this.jdbcTemplateClaims.query(this.sql,
					new BeanPropertyRowMapper<NotificationProcessMasterModel>(NotificationProcessMasterModel.class));

		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return notifProcess_li;
	}

	@Override
	public List<String> getNotifCategoryResultset(int notifProcessConfigID) throws DAOException {
		// TODO Auto-generated method stub
		List<String> result_li = new ArrayList<String>();
		this.sql = "select NotificationCategory from NOTIFICATIONCATEGORYMASTER where " +
				" NotificationProcessConfigID = ? and  ActiveConfiguration = 'Y'";
		try {
			result_li = this.jdbcTemplateClaims.queryForList(this.sql, new Object[]{notifProcessConfigID},
					String.class);
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
					+ " ,notifProcessConfigID = " + notifProcessConfigID);
		}
		return result_li;
	}

	@Override
	public List<Integer> getSMSTemplateResultSet(int notifProcessConfigID) throws DAOException {
		// TODO Auto-generated method stub
		List<Integer> result_li = new ArrayList<Integer>();
		this.sql = "select nsc.SMSTemplateID from NOTIFICATIONSMSCONFIGURATION nsc "
				+ " inner join NOTIFICATIONSMSTEMPLATEMASTER nsstm ON " + " nsstm.SMSTemplateID = nsc.SMSTemplateID "
				+ " and (nsstm.SMSTemplateName <> '' AND nsstm.SMSTemplateName is not null) "
				+ " where nsc.NotificationProcessConfigID = ? ";
		try {
			result_li = this.jdbcTemplateClaims.queryForList(this.sql, new Object[]{notifProcessConfigID},
					Integer.class);
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
					+ " ,notifProcessConfigID = " + notifProcessConfigID);
		}
		return result_li;
	}

	@Override
	public List<Integer> getMailTemplateResultSet(int notifProcessConfigID, int mailTemplateFlag) throws DAOException {
		// TODO Auto-generated method stub
		List<Integer> result_li = new ArrayList<Integer>();
		this.sql = " select nec.EmailTemplateID from NOTIFICATIONEMAILCONFIGURATION nec "
				+ " inner join NOTIFICATIONEMAILTEMPLATEMASTER netm ON "
				+ " netm.EmailTemplateID = nec.EmailTemplateID " + " and netm.EmailTemplateFlag = ? "
				+ " and (netm.EmailTemplateName <> '' AND netm.EmailTemplateName is not null) "
				+ " where nec.NotificationProcessConfigID = ? ";
		try {
			result_li = this.jdbcTemplateClaims.queryForList(this.sql,
					new Object[]{mailTemplateFlag, notifProcessConfigID}, Integer.class);
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
					+ " ,notifProcessConfigID = " + notifProcessConfigID);
		}
		return result_li;
	}

	@Override
	public List<Integer> getLetterTemplateResultSet(int notifProcessConfigID, ClaimDataNewModel claimData,
			String COBLetter) throws DAOException {
		// TODO Auto-generated method stub
		List<Integer> result_li = new ArrayList<Integer>();
		if (claimData.getFullyClaimed() == null || claimData.getFullyClaimed().trim().equalsIgnoreCase("")) {
			this.sql = "select nlc.LetterTemplateID from NOTIFICATIONLETTERCONFIGURATION as nlc, NOTIFICATIONLETTERTEMPLATEMASTER as nltm "
					+ " where nlc.LetterTemplateID = nltm.LetterTemplateID and nltm.LetterCOBFlag = '" + COBLetter
					+ "' and " + " (nltm.LetterTemplateName is not null AND nltm.LetterTemplateName <> '') AND "
					+ " nlc.NotificationProcessConfigID = ? and nlc.LetterTemplateID <> 0 and  "
					+ " nlc.ClaimTypeUI = ? and nlc.ComponentCode = ? and nlc.FullyPaidFlag is NULL ";
		} else {
			this.sql = "select nlc.LetterTemplateID from NOTIFICATIONLETTERCONFIGURATION as nlc, NOTIFICATIONLETTERTEMPLATEMASTER as nltm "
					+ " where nlc.LetterTemplateID = nltm.LetterTemplateID and nltm.LetterCOBFlag = '" + COBLetter
					+ "' and " + " (nltm.LetterTemplateName is not null AND nltm.LetterTemplateName <> '') AND "
					+ " nlc.NotificationProcessConfigID = ? and nlc.LetterTemplateID <> 0 and  "
					+ " nlc.ClaimTypeUI = ? and nlc.ComponentCode = ? and nlc.FullyPaidFlag = '"
					+ claimData.getFullyClaimed() + "' ";
		}
		try {
			result_li = this.jdbcTemplateClaims.queryForList(this.sql,
					new Object[]{notifProcessConfigID, claimData.getClaimTypeUI(), claimData.getComponentCode()},
					Integer.class);
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
					+ " ,notifProcessConfigID = " + notifProcessConfigID);
		}
		return result_li;
	}

	@Override
	public List<Integer> getLetterTemplateResultSet(int mailTemplateId, int notifProcessConfigID,
			ClaimDataNewModel claimData, String COBLetter) throws DAOException {
		// TODO Auto-generated method stub
		List<Integer> result_li = new ArrayList<Integer>();
		if (claimData.getFullyClaimed() == null || claimData.getFullyClaimed().trim().equalsIgnoreCase("")) {
			this.sql = "select nlc.LetterTemplateID from NOTIFICATIONLETTERCONFIGURATION as nlc, NOTIFICATIONLETTERTEMPLATEMASTER as nltm, "
					+ " NOTIFICATIONEMAILLETTERCONFIGURATION as nelc "
					+ " where nelc.EmailTemplateID = ? and nelc.LetterTemplateID = nlc.LetterTemplateID "
					+ " and nlc.NotificationProcessConfigID = ? and nlc.LetterTemplateID <> 0 and nlc.ClaimTypeUI = ? and "
					+ " nlc.ComponentCode = ? and nlc.FullyPaidFlag is null and nlc.LetterTemplateID = nltm.LetterTemplateID and "
					+ " nltm.LetterCOBFlag = ? and (nltm.LetterTemplateName is not null or nltm.LetterTemplateName <> '') ";
		} else {
			this.sql = "select nlc.LetterTemplateID from NOTIFICATIONLETTERCONFIGURATION as nlc, NOTIFICATIONLETTERTEMPLATEMASTER as nltm, "
					+ " NOTIFICATIONEMAILLETTERCONFIGURATION as nelc "
					+ " where nelc.EmailTemplateID = ? and nelc.LetterTemplateID = nlc.LetterTemplateID "
					+ " and nlc.NotificationProcessConfigID = ? and nlc.LetterTemplateID <> 0 and nlc.ClaimTypeUI = ? and "
					+ " nlc.ComponentCode = ? and nlc.FullyPaidFlag = '" + claimData.getFullyClaimed()
					+ "' and nlc.LetterTemplateID = nltm.LetterTemplateID and "
					+ " nltm.LetterCOBFlag = ? and (nltm.LetterTemplateName is not null or nltm.LetterTemplateName <> '') ";
		}

		try {
			result_li = this.jdbcTemplateClaims.queryForList(this.sql, new Object[]{mailTemplateId,
					notifProcessConfigID, claimData.getClaimTypeUI(), claimData.getComponentCode(), COBLetter},
					Integer.class);
		} catch (Exception e) {
			// TODO: handle exception
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
					+ " ,notifProcessConfigID = " + notifProcessConfigID);
		}
		return result_li;
	}

	@Override
	public int insertToNotificationEmailLetter(String claimNumber, String decision, int mailTemplateId,
			int letterTemplateId) {
		// TODO Auto-generated method stub
		int i = 0;
		this.sql = "INSERT INTO NOTIFICATIONEMAILLETTER "
				+ " (ClaimNumber,NotificationDecision, EmailTemplateID, LetterTemplateID, PDFLetterGenerated, LetterUploadToECM) "
				+ " VALUES (?, ?, ?, ?, 'N', 'N')";
		i = this.jdbcTemplateClaims.update(sql,
				new Object[]{claimNumber, decision, mailTemplateId, letterTemplateId});

		return i;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateGenericDB(String sqlQuery, Object[] obj_li) throws Exception {
		if (obj_li != null && obj_li.length > 0) {
			return this.jdbcTemplateClaims.update(sqlQuery, obj_li);
		} else {
			return this.jdbcTemplateClaims.update(sqlQuery);
		}
	}

	@Override
	public List<LetterConfigResultSet> getLetterConfigResultSet(int notifProcessConfigID, ClaimDataNewModel claimData, String COBLetter)
			throws DAOException {
		List<LetterConfigResultSet> letConfig_li = new ArrayList<LetterConfigResultSet>();
		if (claimData.getFullyClaimed() == null || claimData.getFullyClaimed().trim().equalsIgnoreCase("")) {
			this.sql = "SELECT NLC.LetterConfigurationID,NLC.LetterTemplateID " +
					" FROM NOTIFICATIONLETTERTEMPLATEMASTER AS NLTM, NOTIFICATIONLETTERCONFIGURATION AS NLC " +
					" WHERE NLC.NotificationProcessConfigID = ? AND NLC.LetterTemplateID <> 0 " +
					" AND NLC.ClaimTypeUI = ? " +
					" AND NLC.ComponentCode = ? AND NLC.FullyPaidFlag IS NULL AND NLC.LetterTemplateID = NLTM.LetterTemplateID " +
					" AND NLTM.LetterCOBFlag = '" + COBLetter + "' AND NLTM.LetterTemplateName <> '' and NLTM.LetterTemplateName is not null ";
		} else {
			this.sql = "SELECT NLC.LetterConfigurationID,NLC.LetterTemplateID " +
					" FROM NOTIFICATIONLETTERTEMPLATEMASTER AS NLTM, NOTIFICATIONLETTERCONFIGURATION AS NLC " +
					" WHERE NLC.NotificationProcessConfigID = ? AND NLC.LetterTemplateID <> 0 " +
					" AND NLC.ClaimTypeUI = ? " +
					" AND NLC.ComponentCode = ? AND NLC.FullyPaidFlag = '" + claimData.getFullyClaimed() + "' AND NLC.LetterTemplateID = NLTM.LetterTemplateID " +
					" AND NLTM.LetterCOBFlag = '" + COBLetter + "' AND NLTM.LetterTemplateName <> '' and NLTM.LetterTemplateName is not null ";
		}
		try {
			letConfig_li = this.jdbcTemplateClaims.query(this.sql, new Object[]{notifProcessConfigID,
					claimData.getClaimTypeUI(), claimData.getComponentCode()}, new BeanPropertyRowMapper<LetterConfigResultSet>(LetterConfigResultSet.class));
		} catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
					+ " ,notifProcessConfigID = " + notifProcessConfigID);
		}

		return letConfig_li;
	}

	@Override
	public Integer getMailTemplateId(int notifLetterConfigId) throws DAOException {
		Integer let_li = null;
		this.sql = "SELECT DISTINCT NELC.EmailTemplateID FROM NOTIFICATIONEMAILLETTERCONFIGURATION AS NELC " +
				" WHERE LetterConfigurationID = ? ";;
				try {
					let_li = this.jdbcTemplateClaims.queryForObject(this.sql,new Object[] {notifLetterConfigId},Integer.class);
				} catch (Exception e) {
					throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql
							+ " ,notifLetterConfigId = " + notifLetterConfigId);
				}
				return let_li;
	}

	@Override
	public int insertClaimDataSourceCapture(String BatchNumber, Date DocumentScannedDate, String PolicyNumber,
			String OwnerIDType, String OwnerIDNumber, String PayeeName, String InsuredClientName, String InsuredIDType,
			String InsuredIDNumber, String FormClaimType, String FormBenefitType, String SurgeryPerformed,
			Date TreatmentStartDate, Date TreatmentCompletionDate, String HospitalName, String FormCurrency,
			BigDecimal TotalBillingAmount, String Physician, String BankAccountNumber, String PaymentAccountOwnerName,
			String ExceptionRecord, String ProcessRecord) throws DAOException {

		int count = 0;

		try {
			//			log.debug("SELECT COUNT(*) FROM CLAIMDATASOURCECAPTURE WHERE PolicyNumber='"+PolicyNumber+"' AND "
			//					+ "BatchNumber='"+BatchNumber+"'");
			//			this.sql = "SELECT COUNT(*) FROM CLAIMDATASOURCECAPTURE WHERE PolicyNumber=? AND BatchNumber=?";
			//			count = jdbcTemplateClaims.queryForObject(this.sql, new Object[] { PolicyNumber, BatchNumber }, Integer.class);

			//if(count == 0) {
			count = jdbcTemplateClaims.update("INSERT INTO CLAIMDATASOURCECAPTURE (BatchNumber,DocumentScannedDate,PolicyNumber,OwnerIDType,"
					+ "OwnerIDNumber,PayeeName,InsuredClientName,InsuredIDType,InsuredIDNumber,FormClaimType,FormBenefitType,"
					+ "SurgeryPerformed,TreatmentStartDate,TreatmentCompletionDate,HospitalName,FormCurrency,TotalBillingAmount,"
					+ "Physician,BankAccountNumber,PaymentAccountOwnerName,ProcessRecord,ExceptionRecord,RecordCreateTS) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					BatchNumber,DocumentScannedDate,PolicyNumber,OwnerIDType,OwnerIDNumber,PayeeName,InsuredClientName,
					InsuredIDType,InsuredIDNumber,FormClaimType,FormBenefitType,SurgeryPerformed,TreatmentStartDate,
					TreatmentCompletionDate,HospitalName,FormCurrency,TotalBillingAmount,Physician,BankAccountNumber,
					PaymentAccountOwnerName,ProcessRecord,ExceptionRecord,new Date());
			//			} else {
			//				log.error("Claim record already exist in CLAIMDATASOURCECAPTURE table");
			//			}	
		} catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	public int checkDuplicateCaptureRecord(String BatchNumber, String PolicyNumber) throws DAOException {

		int count = 0;

		try {
			log.debug("SELECT COUNT(*) FROM CAPTUREBATCHDETAILS WHERE POLICYNUMBER='"+PolicyNumber+"' AND "
					+ "BATCHID='"+BatchNumber+"'");
			this.sql = "SELECT COUNT(*) FROM CAPTUREBATCHDETAILS WHERE POLICYNUMBER=? AND BATCHID=?";
			count = jdbcTemplateClaims.queryForObject(this.sql, new Object[] { PolicyNumber, BatchNumber }, Integer.class);
		} catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}

		return count;
	}

	@Override
	public int insertIntoCaptureBatchDetails(String BatchNumber, String PolicyNumber, String DuplicateMessage, String DuplicateFlag) throws DAOException {

		int count = 0;

		try {
			count = jdbcTemplateClaims.update("INSERT INTO CAPTUREBATCHDETAILS (BATCHID,DOCID,POLICYNUMBER,RecordCreateTS,ProcessExceptionMessage,DuplicateFlag,DocumentID) "
					+ "VALUES (?,?,?,?,?,?,?)", 
					BatchNumber, "DMU", PolicyNumber, new Date(), DuplicateMessage, DuplicateFlag, "DMU");
		} catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	public List<STPReportDataXls> getClaimsSTPReportData(WSGenerateSTPReportDataIn dataIn) throws DAOException {

		List<STPReportDataXls> STPReportDataList = new ArrayList<STPReportDataXls>();

		try {
			if(dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("xls")) {
				this.sql = "SELECT cd.PolicyNumber, cd.ClaimNumber, cd.Registerdate AS ClaimRegisterDate, cd.ClaimStatus, cd.Receiveddate  "
						+ "AS ClaimReceiveDate, cd.ClaimTypeUI AS claimType, cd.ClaimSTPFlag AS STPStatus, cd.ClaimDecision, "
						+ "replace(replace(STUFF((SELECT '; ' +crd.RuleReason FROM CASERULEDETAILS as crd WHERE crd.RuleStatus = 'Fail' AND "
						+ "crd.ProcessType = 'CLAIMS' AND crd.ProcessKeyReferenceNumber = cd.ClaimNumber FOR XML PATH('')), 1, 1, ''), "
						+ "'&gt;', '>'), '&lt;', '<') reasonForSTPFail, CASE WHEN TPAClaimNumber IS NOT NULL AND TPAClaimNumber != '' THEN "
						+ "'TPA' WHEN PortalClaimRequestNumber IS NOT NULL AND PortalClaimRequestNumber != '' THEN 'Portal' WHEN BatchNumber "
						+ "IS NOT NULL AND BatchNumber != '' THEN 'HardCopy' ELSE 'Unknown Channel' END as channel FROM CLAIMDATA cd WHERE "
						+ "cd.ClaimSTPFlag = 0 AND cd.Receiveddate BETWEEN ? AND ? and cd.ClaimNumber in "
						+ "(select distinct ProcessKeyReferenceNumber from CASERULEDETAILS where Rulestatus = 'Fail' and ProcessType = "
						+ "'CLAIMS') ORDER BY cd.ClaimNumber";
				
				STPReportDataList = this.jdbcTemplateClaims.query(this.sql, new Object[]{ dataIn.getWsInput().getDtFromDate(), dataIn.getWsInput().getDtToDate() }, new BeanPropertyRowMapper<STPReportDataXls>(STPReportDataXls.class));
			}
			if(dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("web")) {
				this.sql = "SELECT cd.PolicyNumber, cd.ClaimNumber, cd.Registerdate AS ClaimRegisterDate, cd.ClaimStatus, cd.Receiveddate  "
						+ "AS ClaimReceiveDate, cd.ClaimTypeUI AS claimType, cd.ClaimSTPFlag AS STPStatus, cd.ClaimDecision, "
						+ "replace(replace(STUFF((SELECT '; ' +crd.RuleReason FROM CASERULEDETAILS as crd WHERE crd.RuleStatus = 'Fail' AND "
						+ "crd.ProcessType = 'CLAIMS' AND crd.ProcessKeyReferenceNumber = cd.ClaimNumber FOR XML PATH('')), 1, 1, ''), "
						+ "'&gt;', '>'), '&lt;', '<') reasonForSTPFail, CASE WHEN TPAClaimNumber IS NOT NULL AND TPAClaimNumber != '' THEN "
						+ "'TPA' WHEN PortalClaimRequestNumber IS NOT NULL AND PortalClaimRequestNumber != '' THEN 'Portal' WHEN BatchNumber "
						+ "IS NOT NULL AND BatchNumber != '' THEN 'HardCopy' ELSE 'Unknown Channel' END as channel FROM CLAIMDATA cd WHERE "
						+ "cd.ClaimSTPFlag = 0 AND cd.Receiveddate BETWEEN ? AND ? and cd.ClaimNumber in "
						+ "(select distinct ProcessKeyReferenceNumber from CASERULEDETAILS where Rulestatus = 'Fail' and ProcessType = "
						+ "'CLAIMS') ORDER BY cd.ClaimNumber";
				
				STPReportDataList = this.jdbcTemplateClaims.query(this.sql, new Object[]{ dataIn.getWsInput().getDtFromDate(), dataIn.getWsInput().getDtToDate() }, new BeanPropertyRowMapper<STPReportDataXls>(STPReportDataXls.class));
			}	
		}  catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return STPReportDataList;
	}

	@Override
	public List<ClaimDataTATReport> getTATReportClaimData(String reportDateType, String productCode, String fromDate, String toDate) throws DAOException {

		List<ClaimDataTATReport> TATReportDataList = new ArrayList<ClaimDataTATReport>();

		try {
			if(reportDateType.equalsIgnoreCase("Register Date")) {
				this.sql = "SELECT * FROM CLAIMDATA WHERE ProductCode = ? AND Registerdate BETWEEN ? AND ?";
			}
			if(reportDateType.equalsIgnoreCase("Receive Date")) {
				this.sql = "SELECT * FROM CLAIMDATA WHERE ProductCode = ? AND Receiveddate BETWEEN ? AND ?";
			}

			TATReportDataList = this.jdbcTemplateClaims.query(this.sql, new Object[]{ productCode, fromDate, toDate }, new BeanPropertyRowMapper<ClaimDataTATReport>(ClaimDataTATReport.class));

		}  catch (Exception e) {
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return TATReportDataList;
	}

	@Override
	public boolean createPencilReportRequest(PencilReportRequest pencilReportRequestData) throws DAOException {
		try {
			this.sql = "INSERT INTO CASEREPORTREQUEST(ReportRequestID,ReportName,ReportRequestUser,ReportOutputFormat,"
					+ "ReportRequestTimeStamp,ReportType,ReportRequestInputData,ReportGenerated,ProcessedFlag) VALUES (?,?,?,?,?,?,?,?,?)";
			jdbcTemplateClaims.update(this.sql, pencilReportRequestData.getReportRequestID(),pencilReportRequestData.getReportName(),
					pencilReportRequestData.getReportRequestUser(),pencilReportRequestData.getReportOutputFormat(),
					pencilReportRequestData.getReportRequestTimeStamp(),pencilReportRequestData.getReportType(),
					pencilReportRequestData.getReportRequestInputData(),pencilReportRequestData.getReportGenerated(),
					pencilReportRequestData.getProcessedFlag());
			return true;
		}catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - registerRequirement" + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public String getPencilReportRequestId() throws DAOException {
		try {
			this.sql = "SELECT MAX(CONVERT(INT, ReportRequestID)) AS ReportRequestID FROM CASEREPORTREQUEST";
			String result = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { }, String.class);
			return result;
		}catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - registerRequirement" + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
	}

	@Override
	public boolean isCasePending(String claimNumber) throws DAOException{
		int i = 0;
		try {
			this.sql = "SELECT COUNT(ClaimNumber) FROM REQUIREMENTSDETAILS WHERE ClaimNumber=? AND RequirementReceiveTS IS NULL";
			i = this.jdbcTemplateClaims.queryForObject(sql, new Object[] { claimNumber },
					Integer.class);
		}catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - isCasePending" + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		if(i>0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Date getPendingCompletionDate(String claimNumber) throws DAOException {
		Date pendingCompletionDate = null;
		try {
			this.sql = "SELECT MAX(RequirementReceiveTS) FROM REQUIREMENTSDETAILS WHERE ClaimNumber=? AND RequirementReceiveTS IS NOT NULL";
			pendingCompletionDate = this.jdbcTemplateClaims.queryForObject(sql, new Object[] { claimNumber },
					Date.class);
		}catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendingCompletionDate" + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return pendingCompletionDate;
	}

	@Override
	public PencilReportRequest getReportRequestStatus(String reportRequestID) throws DAOException {
		PencilReportRequest pencilReportRequest = new PencilReportRequest();
		int count = 0;
		try {
			this.sql = "SELECT COUNT(ReportRequestID) AS ReportRequestID FROM CASEREPORTREQUEST WHERE ReportRequestID=?";
			count = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[]{ reportRequestID }, Integer.class);
			if(count == 0) {
				pencilReportRequest = null;
			}else {
				this.sql = "SELECT * FROM CASEREPORTREQUEST WHERE ReportRequestID = ?";
				pencilReportRequest = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[]{ reportRequestID }, new BeanPropertyRowMapper<PencilReportRequest>(PencilReportRequest.class));	
			}
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getReportRequestStatus - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return pencilReportRequest;
	}

	@Override
	public List<String> getReportOutuputFormat(String reportName, String reportType) throws DAOException {
		List<String> reportSelectionMaster = new ArrayList<String>();

		try {
			this.sql = "SELECT REPORTOUTPUTFORMAT FROM REPORTSELECTIONMASTER WHERE REPORTNAME=? AND REPORTCATEGORY=? AND REPORTGENERATIONSCHEDULE='0'";
			reportSelectionMaster = this.jdbcTemplateClaims.queryForList(this.sql, new Object[]{ reportName, reportType }, String.class);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getReportOutuputFormat - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return reportSelectionMaster;
	}

	@Override
	public int getPendingCompletionStatus(String claimNumber) throws DAOException {
		int count = 0;
		try {
			this.sql = "Select count(*) From REQUIREMENTSDETAILS Where "
					+ "(RequirementReceiveTS is null or RequirementReceiveTS = '') and ClaimNumber=?";
			count = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[]{ claimNumber }, Integer.class);
		} catch (Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendingCompletionStatus - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return count;
	}

	@Override
	public List<ReportTypeListData> getReportTypeList() throws DAOException {
		List<ReportTypeListData> reportTypeListData = new ArrayList<ReportTypeListData>();
		try {
			this.sql = "SELECT DISTINCT REPORTCATEGORY AS strReportTypeName FROM REPORTSELECTIONMASTER WHERE REPORTGENERATIONSCHEDULE='0'";
			reportTypeListData = this.jdbcTemplateClaims.query(this.sql, new Object[]{ }, new BeanPropertyRowMapper<ReportTypeListData>(ReportTypeListData.class));
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getProductList - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return reportTypeListData;
	}

	@Override
	public List<ReportNameListData> getReportNameListByCategory(String reportCategoryName) throws DAOException {
		List<ReportNameListData> reportNameListData = new ArrayList<ReportNameListData>();
		try {
			this.sql = "SELECT REPORTNAME AS strReportName, REPORTOUTPUTFORMAT AS strReportOutputFormat FROM "
					+ "REPORTSELECTIONMASTER WHERE REPORTCATEGORY=? AND REPORTGENERATIONSCHEDULE='0'";
			reportNameListData = this.jdbcTemplateClaims.query(this.sql, new Object[]{ reportCategoryName }, new BeanPropertyRowMapper<ReportNameListData>(ReportNameListData.class));
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getProductListByCategory - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + sql);
		}
		return reportNameListData;
	}

	@Override
	public int createDecisionTracker(String processType, String processTrxType, String userId, String caseRefNumber, String caseUserActivitName, String timeStamp) throws Exception {
		String sql="insert into CASEDECISIONTRACKER(ProcessType,ProcessTransactionType,UserID,CaseReferenceNumber,CaseUserActivityName,CaseDecisionStartTS) " +
				" VALUES(?,?,?,?,?,?)";
		return this.jdbcTemplateClaims.update(sql,new Object[]{processType,processTrxType,userId,caseRefNumber,caseUserActivitName,Timestamp.valueOf(timeStamp)});
	}

	@Override
	public int endDecisionTracker(String caseReferenceNumber, String userId, String caseDecision, String caseUserActName, String timestamp) throws Exception {
		String sql = "update CASEDECISIONTRACKER set UserID=?, CaseUserDecision=?, CaseDecisionCompletionTS=? " +
				" where CaseReferenceNumber=? and CaseUserActivityName=?";
		return this.jdbcTemplateClaims.update(sql,new Object[] {userId,caseDecision,Timestamp.valueOf(timestamp),caseReferenceNumber,caseUserActName});
	}

	@Override
	public List<PendingCaseClaimData> getPendingCases(String reportType, String fromDate, String toDate) throws DAOException {
		List<PendingCaseClaimData> pendingCaseClaimDataList = new ArrayList<PendingCaseClaimData>();
		try {
			if(reportType.equalsIgnoreCase("xls")) {
				this.sql="select CD.ClaimNumber,CD.PolicyNumber,CD.ClaimTypeUI,CD.InsuredClientName,CD.Receiveddate,CD.Registerdate,"
						+ "CD.Admissiondate,CD.Dischargedate,CD.ClaimInvoiceAmount,CD.ClaimStatus from CLAIMDATA CD where "
						+ "CD.ClaimNumber in (select distinct ClaimNumber from REQUIREMENTSDETAILS where RequirementStatus='PENDING' "
						+ "and RequirementReceiveTS is null and RequirementRequestTS <= DATEADD(day,-1,GETDATE())) and "
						+ "CD.ClaimStatus='PENDING'";
				pendingCaseClaimDataList = this.jdbcTemplateClaims.query(this.sql, new Object[] {}, new BeanPropertyRowMapper<PendingCaseClaimData>(PendingCaseClaimData.class));
			}
			if(reportType.equalsIgnoreCase("web")) {
				this.sql="select CD.ClaimNumber,CD.PolicyNumber,CD.ClaimTypeUI,CD.InsuredClientName,CD.Receiveddate,CD.Registerdate,"
						+ "CD.Admissiondate,CD.Dischargedate,CD.ClaimInvoiceAmount,CD.ClaimStatus from CLAIMDATA CD where "
						+ "CD.ClaimStatus='PENDING' and CD.Registerdate BETWEEN ? and ?";
				pendingCaseClaimDataList = this.jdbcTemplateClaims.query(this.sql, new Object[] { fromDate, toDate }, new BeanPropertyRowMapper<PendingCaseClaimData>(PendingCaseClaimData.class));
			}
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendingCases - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}

		return pendingCaseClaimDataList;
	}

	@Override
	public List<RequirementsDetails> getPendReqDetls(String claimNumber) throws DAOException {
		List<RequirementsDetails> requirementsDetails = new ArrayList<RequirementsDetails>();
		try {
			this.sql="select DocID,RequirementRequestTS,RequirementReceiveTS from REQUIREMENTSDETAILS where "
					+ "ClaimNumber=? and RequirementStatus='PENDING' and RequirementReceiveTS is null";
			requirementsDetails = this.jdbcTemplateClaims.query(this.sql, new Object[] { claimNumber }, new BeanPropertyRowMapper<RequirementsDetails>(RequirementsDetails.class));
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendReqDetls - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return requirementsDetails;
	}

	@Override
	public ReportDetails getDecissionUser(String claimNumber) throws DAOException {
		ReportDetails reportDetails = new ReportDetails();
		try {
			this.sql="select ActionedUser from REPORTDETAILS where StartTime=("
					+ "select MAX(StartTime) from REPORTDETAILS where ActivityName='Claim Processor' and "
					+ "UserDecision='REQUEST FOR ADDITIONAL DOCUMENTS' and ClaimNumber=?)";
			reportDetails = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, new BeanPropertyRowMapper<ReportDetails>(ReportDetails.class));
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getDecissionUser - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return reportDetails;
	}

	@Override
	public int getPendingDocCount(String claimNumber) throws DAOException {
		int pendingDocCount = 0;
		try {
			this.sql = "select count(*) from REQUIREMENTSDETAILS where ClaimNumber=? and RequirementReceiveTS is null "
					+ "and RequirementStatus='PENDING'";
			pendingDocCount = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, Integer.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendingDocCount - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return pendingDocCount;
	}

	@Override
	public int getVisitedActivityCount(String claimNumber, String activityName, String genParam) throws DAOException {
		int visitedActivityCount = 0;
		try {
			if(genParam.equalsIgnoreCase("ENDTIME")) {
				this.sql="select count(*) from REPORTDETAILS where ClaimNumber=? and ActivityName=? and EndTime is not null";
			}else {
				this.sql="select count(*) from REPORTDETAILS where ClaimNumber=? and ActivityName=?";	
			}
			visitedActivityCount = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, activityName }, Integer.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getVisitedActivityCount - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return visitedActivityCount;
	}

	@Override
	public int getDocUploadCount(String claimNumber, String docId, String genParam) throws DAOException {
		int docUploadCount = 0;
		try {
			if(genParam.equalsIgnoreCase("ALLPENDCASE1")) {
				this.sql = "select count(*) from REQUIREMENTSDETAILS where ClaimNumber=? and DocID=?";
			}else {
				this.sql = "select count(*) from REQUIREMENTSDETAILS where ClaimNumber=? and DocID=? and RequirementReceiveTS is not null";	
			}
			docUploadCount = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, docId }, Integer.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getDocUploadCount - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return docUploadCount;
	}

	@Override
	public int isAllPendingDocReceived(String claimNumber, String claimTypeUi, String genParam) throws DAOException {
		int pendDocCount = 0;
		try {
			if(genParam.equalsIgnoreCase("MANDATORYDOCS")) {
				this.sql = "select count(*) from REQUIREMENTSDETAILS where ClaimNumber=? "
						+ "and RequirementReceiveTS is null and RequirementStatus='PENDING'";

				pendDocCount = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, Integer.class);
			} else if(genParam.equalsIgnoreCase("NODOCSREQ")) {
				this.sql = "select count(*) from REQUIREMENTSDETAILS where ClaimNumber=? and DocID not in "
						+ "(select DocID from MANDATORYREQUIREMENTS where CLAIMTYPEUI=? and MANDATORYDOC='Y')";

				pendDocCount = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, claimTypeUi }, Integer.class);
			} else {
				this.sql = "select count(*) from REQUIREMENTSDETAILS where ClaimNumber=? and DocID not in "
						+ "(select DocID from MANDATORYREQUIREMENTS where CLAIMTYPEUI=? and MANDATORYDOC='Y') and "
						+ "RequirementReceiveTS is null and RequirementStatus='PENDING'";

				pendDocCount = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, claimTypeUi }, Integer.class);
			}



		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - isAllPendingDocReceived - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return pendDocCount;
	}

	@Override
	public Date getPendCompDateReptTbl(String claimNumber, String activityName, String genParam) throws DAOException {
		Date pendCompDate = new Date();
		try {
			if(genParam.equalsIgnoreCase("START TIME")) {
				this.sql="select max(StartTime) from REPORTDETAILS where ClaimNumber=? and ActivityName=? and StartTime is not null";	
			}else {
				this.sql="select max(EndTime) from REPORTDETAILS where ClaimNumber=? and ActivityName=? and EndTime is not null";
			}

			pendCompDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, activityName }, Date.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendCompDateReptTbl - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return pendCompDate;
	}

	@Override
	public Date getInvestDocUploadDate(String claimNumber, String docId) throws DAOException {
		Date pendDocUploadDate = new Date();
		try {
			this.sql="select max(RequirementReceiveTS) from REQUIREMENTSDETAILS where ClaimNumber=? and DocID=? and RequirementReceiveTS is not null";
			pendDocUploadDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, docId }, Date.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getInvestDocUploadDate - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return pendDocUploadDate;
	}

	@Override
	public Date getPendDocReceiveDate(String claimNumber, String claimTypeUi) throws DAOException {
		Date pendDocRecDate = new Date();
		try {
			if(claimTypeUi.equalsIgnoreCase("INCLUDEPENDDOCS")) {
				this.sql = "select max(RequirementReceiveTS) from REQUIREMENTSDETAILS where ClaimNumber=? "
						+ "and RequirementReceiveTS is not null";

				pendDocRecDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, Date.class);
			}else if(claimTypeUi.equalsIgnoreCase("PENDCOMPDATE")) {
				this.sql = "select max(RequirementReceiveTS) from REQUIREMENTSDETAILS where ClaimNumber=? "
						+ "and RequirementReceiveTS is not null and RequirementRequestTS is not null";

				pendDocRecDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, Date.class);
			}else if(claimTypeUi.equalsIgnoreCase("PCR PENDDAYS")) {
				this.sql = "select min(RequirementRequestTS) from REQUIREMENTSDETAILS where ClaimNumber=? "
						+ "and RequirementReceiveTS is null";

				pendDocRecDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, Date.class);
			} else if(claimTypeUi.equalsIgnoreCase("PCR PENDDAYS_MAX")) {
				this.sql = "select max(RequirementRequestTS) from REQUIREMENTSDETAILS where ClaimNumber=? "
						+ "and RequirementReceiveTS is null";

				pendDocRecDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, Date.class);
			} else {
				this.sql = "select max(RequirementReceiveTS) from REQUIREMENTSDETAILS where ClaimNumber=? and DocID not in "
						+ "(select DocID from MANDATORYREQUIREMENTS where CLAIMTYPEUI=? and MANDATORYDOC='Y') and "
						+ "RequirementReceiveTS is not null";

				pendDocRecDate = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, claimTypeUi }, Date.class);
			}

		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getInvestDocUploadDate - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return pendDocRecDate;
	}

	@Override
	public int getPendCompCountReptTbl(String claimNumber, String activityName, String genParam) throws DAOException {
		int count = 0;
		try {
			if(genParam.equalsIgnoreCase("END TIME NOT NULL")) {
				this.sql="select count(*) from REPORTDETAILS where ClaimNumber=? and ActivityName=? and EndTime is not null";
			}else {
				this.sql="select count(*) from REPORTDETAILS where ClaimNumber=? and ActivityName=? and EndTime is null";	
			}

			count = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber, activityName }, Integer.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendCompCountReptTbl - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return count;
	}

	@Override
	public String getPendingType(String claimNumber) throws DAOException {
		String pendingType = null;

		try {
			this.sql="select top 1 UserDecision from REPORTDETAILS where ActivityName='Claim Processor' and ClaimNumber=? "
					+ "and (UserDecision='REQUEST FOR ADDITIONAL DOCUMENTS' or UserDecision='START INVESTIGATION') "
					+ "order by StartTime desc";
			pendingType = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { claimNumber }, String.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getPendCompCountReptTbl - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}

		return pendingType;
	}

	@Override
	public List<RequirementsDetails> getFUPDescription(String claimNumber) throws DAOException {
		List<RequirementsDetails> requirementsDetails = new ArrayList<RequirementsDetails>();
		try {
			this.sql="select DocID,RequirementText,RequirementRequestTS,RequirementReceiveTS from REQUIREMENTSDETAILS where ClaimNumber=?";
			requirementsDetails = this.jdbcTemplateClaims.query(this.sql, new Object[] { claimNumber }, new BeanPropertyRowMapper<RequirementsDetails>(RequirementsDetails.class));
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getFUODescription - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return requirementsDetails;
	}

	@Override
	public String getReceiveActIdPendingDoc(String claimNumber) {
		this.sql = " select DISTINCT crat.CaseReceiveActivityID from CASERECEIVEACTIVITYTRACKER crat " +
				" inner join RECREQACTIVITYMAPPING rram on crat.CaseReceiveActivityID = rram.ReqtReceiveActivityID " +
				" inner join REQUIREMENTSDETAILS rd on rram.ReqtRequestActivityID = rd.RequirementRequestActivity " +
				" where crat.CaseReceiveActivityStatus= 'PENDING' and crat.CaseReferenceKey = rd.ClaimNumber " +
				" and crat.CaseReceiveActivityID <> 'recInvestigationReport' and crat.CaseReferenceKey= ? ";
		return this.jdbcTemplateClaims.queryForObject(sql,new Object[] {claimNumber},String.class);
	}

	@Override
	public int updateCaseReceiveActTracker(String caseRefKey, String recActId, String recActStatus, String updateReceiveActStatus) {
		this.sql = "update CASERECEIVEACTIVITYTRACKER set CaseReceiveActivityStatus= ?" +
				" where CaseReceiveActivityID=? and CaseReferenceKey=? and CaseReceiveActivityStatus=?";
		return this.jdbcTemplateClaims.update(this.sql,new Object[] {updateReceiveActStatus,recActId,caseRefKey,recActStatus});
	}

	@Override
	public String getReceiveActIdInvestDoc(String claimNumber) {
		this.sql = "select DISTINCT crat.CaseReceiveActivityID from CASERECEIVEACTIVITYTRACKER crat " +
				" where crat.CaseReceiveActivityStatus='PENDING' and crat.CaseReceiveActivityID = 'recInvestigationReport' " +
				" and crat.CaseReferenceKey='"+claimNumber+"'";
		return this.jdbcTemplateClaims.queryForObject(this.sql,String.class);
	}

	@Override
	public int getCountReceiveActIdInvestDoc(String claimNumber) {
		this.sql = "select count(DISTINCT crat.CaseReceiveActivityID) from CASERECEIVEACTIVITYTRACKER crat " +
				" where crat.CaseReceiveActivityStatus='PENDING' and crat.CaseReceiveActivityID = 'recInvestigationReport' " +
				" and crat.CaseReferenceKey='"+claimNumber+"'";
		return this.jdbcTemplateClaims.queryForObject(this.sql,Integer.class);
	}

	@Override
	public Timestamp getDULRegisterDateLA(String claimNumber, String boIdentifier) {
		this.sql = "select top 1 msgresponserects from LAREQUESTTRACKER  " +
				" where claimnumber ='"+claimNumber+"' and boidentifier = '"+boIdentifier+"' order by msgresponserects DESC";
		return this.jdbcTemplateClaims.queryForObject(this.sql,Timestamp.class);
	}

	@Override
	public Object getClaimDataVariable(String claimNumber, String queryString) {
		this.sql = "select "+queryString+" from CLAIMDATA where ClaimNumber='"+claimNumber+"' ";

		return this.jdbcTemplateClaims.queryForObject(sql,Object.class);
	}

	@Override
	public int getDocNotUploadCount(String claimNumber, String activityName) throws DAOException {
		int count = 0;
		try {
			this.sql = "select count(*) from dbo.REPORTDETAILS where ActivityName=? and EndTime is null and ClaimNumber=?";
			count = this.jdbcTemplateClaims.queryForObject(this.sql, new Object[] { activityName, claimNumber }, Integer.class);
		}catch(Exception e) {
			log.error("ClaimDetailsDaoImpl - Error - getDocNotUploadCount - " + e.getMessage());
			throw new DAOException("Error at execution query = " + e + ", SQL Query = " + this.sql);
		}
		return count;
	}

	@Override
	public int getCountActIdPendingDoc(String claimNumber) {
		this.sql = " select count(DISTINCT crat.CaseReceiveActivityID) from CASERECEIVEACTIVITYTRACKER crat  " +
				" inner join RECREQACTIVITYMAPPING rram on crat.CaseReceiveActivityID = rram.ReqtReceiveActivityID  " +
				" inner join REQUIREMENTSDETAILS rd on rram.ReqtRequestActivityID = rd.RequirementRequestActivity " +
				" where crat.CaseReceiveActivityStatus= 'PENDING' and crat.CaseReferenceKey = rd.ClaimNumber  " +
				" and crat.CaseReceiveActivityID <> 'recInvestigationReport' and crat.CaseReferenceKey= '"+claimNumber+"'";

		return this.jdbcTemplateClaims.queryForObject(sql,Integer.class);
	}

}