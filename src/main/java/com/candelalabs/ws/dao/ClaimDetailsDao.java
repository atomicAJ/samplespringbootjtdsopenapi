package com.candelalabs.ws.dao;

import java.math.BigDecimal;
//import java.sql.Date;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.candelalabs.ws.model.tables.*;
import org.apache.commons.logging.Log;

import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.*;
import com.candelalabs.api.model.ClaimDataPortalOutput;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.ClaimDataUIClaimDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsAdditionalRequirement;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsBenefitfundDetails;
import com.candelalabs.api.model.JsonWSRegisterRequirementDataIn;
import com.candelalabs.api.model.ProductListData;
import com.candelalabs.api.model.ReportNameListData;
import com.candelalabs.api.model.ReportTypeListData;
import com.candelalabs.api.model.WSGenerateSTPReportDataIn;
//import com.candelalabs.api.model.WSFetchRecordEntriesDataOutClaimRecordList;
//import com.candelalabs.api.model.WSFetchRecordEntriesDataOutClaimRecordListClaimRecord;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.api.model.WSUpdateClaimStatusDataIn;

public interface ClaimDetailsDao {

	public List<ClaimsData> getPendingTriggerClaims();

	public void updatedClaimDetails(ClaimsData tpaClaimDetails);

	/* public void updatedRemarksInClaimData(ClaimsData claimData); */

	// public ClaimsData
	// getClaimDataByClaimNo(Class<JsonINTSRVLARegisterClaimdatain> class1);

	public ClaimsData updatedRequirementProcessor(ClaimsData tpaClaimDetails);

	public void updatedRemarksInClaimData(WSUpdateClaimStatusDataIn jsonData, String CancelClaimRemark)
			throws DAOException;

	public int updatedRequirement(JsonWSUpdateRequirementDataIn jsonData);

	public int registerRequirement(JsonWSRegisterRequirementDataIn jsonData);

	public ClaimDataNewModel getClaimData(String claimNo) throws DAOException;
	public POSDataModel getPOSData(String posReqNumber) throws DAOException;
	public ClaimsData getClaimDataByTpaClaimNo(String tpaClaimNo) throws Exception;

	public int getPendingClaimCount(String claimNo, String claimStatus, String polNo) throws DAOException;
	// public int
	// invesCompleteNotification(JsonWSInvesCompleteNotificationDataIn
	// jsonData);

	// public int custPendingNotification(JsonWSInvesCompleteNotificationDataIn
	// jsonData);

	public int generateFiles(JsonWSGenerateFilesDataIn jsonData);

	// public int dULActualClaimAmount(JsonWSDULActualClaimAmountProcessorDataIn
	// jsonData, String msgtosend);

	// public int
	// dULUpdClaimRejectStatus(JsonWSDULUpdClaimRejectStatusProcessorDataIn
	// jsonData, String msgtosend);

	public int dULActualAmount(JsonWSDULActualAmountProcessorDataIn jsonData, String msgtosend);

	public void updateClaimUiData(JsonWSSaveClaimUIDataIn jsonWSSaveClaimUIDataIn) throws Exception;

	public int lARequestTracker_Insert(String claimnumber, String activityid, String boidentifier,
			// String msgcreatets, this value will be the current Timestamp
			// formulated in method itsef
			String msgtosend, String futuremesg) throws DAOException;

	public WSGENRegClaimBO getWSGENRegClaimData(String claimNumber) throws Exception;

	public WSCLRegClaimClaimsDataBOData getWSCLRegClaimClaimsData(String ClaimNo);

	public WSCLUpdAppClaimStatusClaimsDataBOData getWSCLUpdAppClaimStatusClaimsData(String ClaimNo);

	public WSDNULRegClaimClaimsDataBOData getWSDNULRegClaimClaimsData(String ClaimNo);

	public WSDNULRegClaimClaimsDetailsBOData getWSDNULRegClaimClaimsDetails(String ClaimNo);

	public WSDNULUpdClaimRejectStatusClaimsDataBOData getWSDNULUpdClaimRejectStatusClaimsData(String ClaimNo);

	public WSDNULUpdClaimRejectStatusClaimsDetailsBOData getWSDNULUpdClaimRejectStatusClaimsDetails(String ClaimNo);

	public String getInsuredClientNo(String PolNo);

	public int insertNotification(JsonWSRegisterRequirementDataIn jsonWSRegisterRequirementDataIn,
			String notificationDecision, List notificationCategory, String pHEmailAddress, String pHPhoneNumber,
			String agentEmailAddress, String claimUserEmailAddress);

	public List<Map<String, Object>> getNotificationCategory(String notificationDecision);

	public int getCountMandatoryRequirementTableByClaimTypeUI(String claimTypeUI, String bool) throws DAOException;

	public int getClaimListCount(String polNumber) throws DAOException;

	public int getClaimListCountByClaimNo(String Claimno) throws DAOException;

	public int getClaimListCountWithCompOpr(String Claimno, String compOpr, String polNo) throws DAOException;

	public int getCountRequiremntDetailTable(String claimTypeUI, String claimNumber, String bool) throws DAOException;

	public int updateReportDetailTable(ReportDetailModel reportDetail) throws Exception;

	public int insertReportDetailTable(ReportDetailModel reportDetail) throws Exception;

	public ClaimDataNewModel getClaimDataNew(String claimNo) throws DAOException;

	public List<String> getClaimDataNew(String polNum, String claimStatus) throws DAOException;

	public List<ClaimDataDetailsModel> getClaimDataDetailsByClaimNo(String claimNo) throws DAOException;

	// public List<ClaimDataPortalOutputDocumentList>
	// getRequirementDocList(String claimNo) throws DAOException;
	public List<ReqDocList> getRequirementDocList(String claimNo) throws DAOException;

	// public List<WSPortalServiceDataOutClaimData> getClaimDetails(String
	// strOwnerClientID) throws DAOException;
	public List<PortalsGetClaimDetailsData> getClaimDetails(String strOwnerClientID) throws DAOException;

	public List<ClaimRecord> getClaimList(String PolNumber) throws DAOException;

	public List<ClaimRecord> getClaimListDetails(String PolNumber, String ClaimNO) throws DAOException;

	public List<ClaimRecord> getClaimListByClaimNo(String claimNumber) throws DAOException;

	// public List<WSFetchRecordEntriesDataOutClaimRecordList>
	// getClaimListWithCompOpr(String claimNumber,String compOpr,String polNo)
	// throws DAOException;
	public List<ClaimRecord> getClaimListWithCompOpr(String claimNumber, String compOpr, String polNo)
			throws DAOException;

	public List<ClaimRecord> getClaimListWithCompOprDetails(String claimNumber, String compOpr, String polNo)
			throws DAOException;

	// public ClaimRecord getClaimListWithCompOprObject(String
	// claimNumber,String compOpr,String polNo) throws DAOException;
	public ClaimDataPortalOutput getClaimDataForPortal(String strPencilClaimNumber) throws DAOException;

	public LaRequestTrackerModel getLaRequestTrackerByClaimNo(String claimNo, String activityId, String boIdentfier)
			throws DAOException;

	public WSDULRegClaimClaimsDataBOData getWSDULClaimDetailsByClaimNo(String claimNo);

	public WSDULRegClaimClaimsDetailsBOData getWSDULClaimClaimsDetailsByClaimNo(String claimNo);

	public List<NotificationDecisionMasterModel> getNotifCategoryResults(String decision) throws DAOException;

	public Integer getSMSTemplateIdByDecision(String decision) throws DAOException;

	public Integer getEmailTemplateIdByDecision(String decision, int emailTemplateFlag) throws DAOException;

	public List<Integer> getLetterTemplateId(String decision, ClaimDataNewModel claimData, String COBLetter)
			throws DAOException;

	public int insertNotificationTable(String type, String claimNumber, String processName, String decision,
			String notifCategory, int TemplateId, String notifFlag, String pdfLetterGen, String emailSent,
			String uploadToCase, String smsCreated) throws Exception;
	
	public int insertNotificationTable2(String type, String claimNumber, String processName, String decision,
			String notifCategory, int TemplateId, String notifFlag, String pdfLetterGen, String emailSent,
			String uploadToCase, String smsCreated,String procCategory, String ApplicationNumber, String PolicyNumber) throws Exception;

	public int insertNotificationTable(String claimNumber, String processName, String decision, String notifCategory,
			int mailTemplateId, int letterTemplateId, String notifFlag, String pdfLetterGen, String emailSent,
			String uploadToCase, String smsCreated) throws Exception;

	public int countCOBRequestByClaimNumber(String claimNumber) throws DAOException;

	public int countCOBRequestByClaimNumber(String claimNumber, String policyNumber) throws DAOException;

	public int updateCOBRequest(String claimNumber, int processed, String processedRemarks) throws Exception;

	public int updateSTPStatus(String claimNumber, int STPStatus) throws Exception;
	
	public int updateAgentEmailAddr( String agentEmailAddr,String claimNumber) throws Exception;
	public int updateSDEmailAddr( String agentEmailAddr,String claimNumber) throws Exception;
	public int updateNSDEmailAddr( String agentEmailAddr,String claimNumber) throws Exception;
	
  public int updateNotificationFlag(String errorMessage, String claimNumber) throws Exception;
  
	public int insertCOBREquest(String claimNumber, String polNum, int processed, Timestamp reqProcessedTS,
			String processRemarks) throws Exception;

	public int insertCOBREquest(String claimNumber, String polNum) throws Exception;

	public int updateClaimStatusofClaimDataByProcessInstance(String processInstanceId, String claimStatus)
			throws Exception;

	public int getTotalAuthorityMatrix(ClaimDataNewModel claimData, String claimUser, String userDecision)
			throws DAOException;

	public int getTotalAuthorityMatrix(ClaimDataNewModel claimData, String claimUser, String userDecision,
			String comparison) throws DAOException;

	public String getComparisonOperation(String claimTypeUI, String userDecision, String claimUser) throws DAOException;

	public List<AuthorityMatrixModel> getAuthorityMatrixData(String claimTypeUI, String userDecision, String claimUser)
			throws DAOException;

	public List<RequirementDetailsModel> getRequirementDetailsList(String claimNumber) throws DAOException;

	public int updateClaimData(ClaimDataNewModel claimData) throws Exception;

	public int updateClaimApprovedAmountUI(BigDecimal iClaimApprovedAmtTPA, String claimNumber) throws DAOException;

	public int updateClaimDataGeneral(String claimNumber, String type, String typeValue) throws Exception;

	public int updateClaimDataGeneral(String claimNumber, String type, Object typeValue, String typetype)
			throws Exception;

	public int updateClaimData(String claimNumber, String claimStatus, Timestamp processEndTS) throws Exception;

	public int updateClaimDataDetails(String claimNumber, ClaimDataUIClaimDetailsBenefitfundDetails benefitFundDetails)
			throws Exception;

	public int insertClaimDataDetails(String claimNumber, ClaimDataUIClaimDetailsBenefitfundDetails benefitFundDetails)
			throws Exception;

	public int deleteClaimDataDetails(String claimNumber, String benefitCode) throws Exception;

	public int updateRequirementDetails(String claimNumber, String polNum, String type, String typeValue)
			throws Exception;

	public int updateRequirementDetailsWithRecId(String claimNumber, String reqNotificationSent, String reqStatus, String reqtReqActivityId)
			throws Exception;

	public int updateRequirementDetails(String claimNumber, Timestamp reqReceiveTs, String reqStatus, String docId)
			throws Exception;

	public int updateRequirementDetails(String claimNumber, String polNum, Timestamp reqReceiveTs, String reqStatus,
			String docId, String type, String typeValue) throws Exception;

	public int updateClaimUser(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimApprovalHAUser(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimApprovalHAUserName(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimRejectHAUser(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimRejectHAUserName(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimUWUser(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimHUWUser(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimProcessorName(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimProcessor(String ClaimNo, String activityUser) throws Exception;

	public int updateClaimDecisionDate(String ClaimNo) throws Exception;

	public int updateNotificationClaimDecisionDate(String ClaimNo) throws Exception;

	public int updateClaimDecision(String ClaimNo, String decision) throws Exception;

	public int updateClaimProcessorEmailAddress(String ClaimNo, String emailadd) throws Exception;

	public int updateRemarks(String ClaimNo, String remarks) throws Exception;

	public int updateClaimProcessorInvestigationEmailAddres(String ClaimNo, String emailadd) throws Exception;

	public int updateRequirementDetails(String claimNumber, String DocId, Timestamp reqReceivedTS, String reqStatus)
			throws Exception;

	public int updateNotificationDataFromfetchPol(String PolicyNumber, String PlanCode, String ClaimTypeLA,
			String ClientInsuredName, String ClientInsuredNumber, String ProductCode,
			BigDecimal ClaimUITotalClaimAmount, String PolicyOwnerName, String PolicyOwnerAddress1,
			String PolicyOwnerAddress2, String PolicyOwnerAddress3, String PolicyOwnerAddress4,
			String PolicyOwnerAddress5, String PolicyOwnerEmailAddress, String PolicyOwnerPhoneNumber,
			String BeneficiaryName, String BeneficiaryAddress1, String BeneficiaryAddress2, String BeneficiaryAddress3,
			String BeneficiaryAddress4, String BeneficiaryAddress5, Date ClaimProcessInitiationDate,
			BigDecimal PolicyRegularTopup, String PolicyPaymentFreq, String AgentEmailAddress, String SDEmailAddress,
			String NSDEmailAddress, String POSRequestNumber, String ApplicationNumber,String claimTypeLAName);

	public int insertRequirementDetails(String claimNumber, String docId,
			ClaimDataUIClaimDetailsAdditionalRequirement addReqData) throws Exception;

	public int insertRequirementDetails(String claimNumber, String tpaClaimNum, String batchNum,
			String portalClaimReqNum, String policyNumber, String docId, String requirementText,
			Timestamp requirementRequestTs, String requirementStatus, String custInitiatedReq) throws Exception;

	public int insertRequirementDetails(String claimNumber, String policyNumber, String appNumber,
			String tpaClaimNumber, String batchNumber, String portalClaimRequestNum, String posRequestNum, String docId,
			String reqText, Timestamp reqRequestTs, Timestamp reqReceiveTs, String reqStatus, String custIniRequirement,
			String reqNotifSent,String reqtReqActivityId) throws Exception;

	public int insertNotificationData(String POSRequestNumber, String PolicyNumber, String PlanCode, String ClaimTypeLA,
			String ClientInsuredName, String ClientInsuredNumber, String ProductCode,
			BigDecimal ClaimUITotalClaimAmount, String PolicyOwnerName, String PolicyOwnerAddress1,
			String PolicyOwnerAddress2, String PolicyOwnerAddress3, String PolicyOwnerAddress4,
			String PolicyOwnerAddress5, String PolicyOwnerEmailAddress, String PolicyOwnerPhoneNumber,
			String BeneficiaryName, String BeneficiaryAddress1, String BeneficiaryAddress2, String BeneficiaryAddress3,
			String BeneficiaryAddress4, String BeneficiaryAddress5, Date ClaimProcessInitiationDate,
			BigDecimal PolicyRegularTopup, String PolicyPaymentFreq, String AgentEmailAddress, String SDEmailAddress,
			String NSDEmailAddress , String ApplicationNumber,String claimTypeLAName) throws DAOException;

	public int updateNotificationData(String claimNumber, String requirementDocNRSubCategoryList) throws DAOException;

	public int updateNotificationDataClientDetails(String PolicyOwnerAddress1, String PolicyOwnerAddress2,
			String PolicyOwnerAddress3,String PolicyOwnerAddress4,String PolicyOwnerAddress5,
			String PolicyOwnerEmailAddress,String PolicyOwnerPhoneNumber,
			 String ClaimNo) throws DAOException;
	
	public int updateNotificationDataBenDetails(String BeneficiaryAddress1, String BeneficiaryAddress2,
			String BeneficiaryAddress3,String BeneficiaryAddress4,String BeneficiaryAddress5,
			 String ClaimNo) throws DAOException;
	
	public int deleteRequirementDetails(String claimNumber, String docId) throws Exception;

	public int deleteRequirementDetails(String claimNumber, String docId_li, String reqNotifSent, String reqStatus)
			throws Exception;

	public int countRequiremnetDetails(String claimNumber, String requirementStatus) throws DAOException;

	public int countRequiremnetDetails(String claimNumber, String docId, String requirementStatus) throws DAOException;

	public int countRecords(String claimNumber) throws DAOException;
	public int countRecordsbasedOnPOSReqNo(String Posrqno) throws DAOException;
	public int countgetClaimDetails(String strOwnerClientID) throws DAOException;

	void saveClaimDataUI(String claimNumber, List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitFundDetails_li,
			List<ClaimDataDetailsModel> claimDataDetails_li, ClaimDataNewModel claimData,
			ClaimDataUIClaimDetails dataIn, String claimTypeUI, String listLifename, String listSubCategory,
			String listBenefitName, java.sql.Date compRiskDate, Log log, String benNotPaidAmountList) throws Exception;

	public String getDocIDList(String claimNumber) throws DAOException;

	public String getDocIDList(String claimNumber, String reqNotifSent) throws DAOException;

	public String getBenefitIDList(String claimNumber) throws DAOException;

	public String getInProgressClaimNo(String claimNumber, String claimStatus, String Polno) throws DAOException;
	public String getInProgressPOSRequestNo(String posreqno, String posStatus, String Polno) throws DAOException;
	
	public String getComponentCodeStatus(String ClientNo, String PolNo, String CompCode) throws DAOException;

	public int updateNotificationData(ClaimDataUIClaimDetails dataIn, String claimTypeUI, String listLifeName,
			String listSubCategory, String listBenefitName, java.sql.Date compRiskCesDate, String benNotPaidAmountList)
			throws Exception;
	
	public int updateClaimDataClaimPayDate( java.sql.Date compRiskCesDate, String claimNo)
			throws Exception;

	public int insertGenerateFileRequest(String claimNumber, String generateFile) throws Exception;

	public String getClaimTypeUI(String claimNumber) throws DAOException;

	public List<UWCommentsModel> getUWComments(String polNum, String sourceOfComment) throws DAOException;

	public int insertUWComments(UWCommentsModel uwModel) throws DAOException;

	public int lARequestTracker_Insert(String claimnumber, String activityid, String boidentifier, String msgtosend,
			String futuremesg, java.sql.Date futureDate) throws DAOException;

	public int insertCLAIMDATASOURCEPORTAL(String PortalClaimRequestNumber, Date PortalClaimRequestCreationDate,
			String PolicyNumber, String OwnerClientID, String PayeeName, String InsuredClientName,
			String InsuredClientID, String FormClaimType, String FormBenefitType, String SurgeryPerformed,
			Date TreatmentStartDate, Date TreatmentCompletionDate, String HospitalName, String FormCurrency,
			BigDecimal TotalBillingAmount, String Physician, String BankAccountNumber, String PaymentAccountOwnerName,
			String ProcessRecord) throws DAOException;

	public List<BenefitNotPaidAmount> getBenefitNotPaidAmountList(String claimNumber) throws DAOException;

	int updateClaimStatus(String claimNumber, String claimStatus) throws Exception;

	//void updatedClaimStatusInClaimData(WSUpdateClaimStatusDataIn jsonData, Timestamp piTs) throws DAOException;
	void updatedClaimStatusInPOSData(WSUpdateClaimStatusDataIn jsonData, Timestamp piTs) throws DAOException;
	
	int updatedClaimStatusInClaimData(String claimNumber, String status, String activityId) throws DAOException;

	public int getCountRequirementDetailTable(String searchValue, String docID, String searchtype) throws DAOException;

	public int getCountRequirementDetailTable(String searchValue, String policyNumber, String docID, String searchtype)
			throws DAOException;

	public int getCountClaimDataTable(String type, Object typeValue, String typeDataType) throws DAOException;

	public String getClaimNumberInClaimData(String type, Object typeValue, String dataTypeType) throws DAOException;

	public List<NotificationProcessMasterModel> getNotificationProcess(String processName, String processActivityName,
			String decision) throws DAOException;
	
	public List<String> getNotifCategoryResultset(int notifProcessConfigID) throws DAOException;
	
	public List<Integer> getSMSTemplateResultSet(int notifProcessConfigID) throws DAOException;
	
	public List<Integer> getMailTemplateResultSet(int notifProcessConfigID,int mailTemplateFlag) throws DAOException;

	public List<LetterConfigResultSet> getLetterConfigResultSet(int notifProcessConfigID,ClaimDataNewModel claimData, String COBLetter) throws DAOException;
	
	public List<Integer> getLetterTemplateResultSet(int notifProcessConfigID, ClaimDataNewModel claimData, String COBLetter) throws DAOException;
	
	public List<Integer> getLetterTemplateResultSet(int mailTemplateId,int notifProcessConfigID, ClaimDataNewModel claimData, String COBLetter) throws DAOException;

    public Integer getMailTemplateId(int notifLetterConfigId) throws DAOException;
	
	public int insertToNotificationEmailLetter(String claimNumber, String decision, int mailTemplateId, int letterTemplateId);

	public int updateGenericDB(String sqlQuery, Object[] obj_li) throws Exception;
	
	public int insertClaimDataSourceCapture(String BatchNumber, Date DocumentScannedDate, String PolicyNumber, 
			String OwnerIDType, String OwnerIDNumber, String PayeeName, String InsuredClientName, 
			String InsuredIDType, String InsuredIDNumber, String FormClaimType, String FormBenefitType, 
			String SurgeryPerformed, Date TreatmentStartDate, Date TreatmentCompletionDate, String HospitalName, 
			String FormCurrency, BigDecimal TotalBillingAmount, String Physician, String BankAccountNumber, 
			String PaymentAccountOwnerName, String ExceptionRecord, String ProcessRecord) throws DAOException;
	
	public int checkDuplicateCaptureRecord(String BatchNumber, String PolicyNumber) throws DAOException;
	
	public int insertIntoCaptureBatchDetails(String BatchNumber, String PolicyNumber, String DuplicateMessage, String DuplicateFlag) throws DAOException;;
	
	public List<STPReportDataXls> getClaimsSTPReportData(WSGenerateSTPReportDataIn dataIn) throws DAOException;
	public List<ClaimDataTATReport> getTATReportClaimData(String reportDateType, String productCode, String fromDate, String toDate) throws DAOException;
	public boolean createPencilReportRequest(PencilReportRequest pencilReportRequestData) throws DAOException;
	public String getPencilReportRequestId() throws DAOException;
	public boolean isCasePending(String claimNumber) throws DAOException;
	public Date getPendingCompletionDate(String claimNumber) throws DAOException;
	public PencilReportRequest getReportRequestStatus(String reportRequestID) throws DAOException;
	public List<String> getReportOutuputFormat(String reportName, String reportType) throws DAOException;
	public int getPendingCompletionStatus(String claimNumber) throws DAOException;
	public List<ReportTypeListData> getReportTypeList() throws DAOException;
	public List<ReportNameListData> getReportNameListByCategory(String reportCategoryName) throws DAOException;

	public int createDecisionTracker(String processType, String processTrxType, String userId, String caseRefNumber, String caseUserActivitName,String timeStamp) throws Exception;
	public int endDecisionTracker(String caseReferenceNumber, String userId,String caseDecision, String caseUserActName,String timestamp) throws Exception;

	public List<PendingCaseClaimData> getPendingCases(String reportType, String fromDate, String toDate) throws DAOException;
	public List<RequirementsDetails> getPendReqDetls(String claimNumber) throws DAOException;
	public ReportDetails getDecissionUser(String claimNumber) throws DAOException;
	public int getPendingDocCount(String claimNumber) throws DAOException;
	public int getVisitedActivityCount(String claimNumber, String activityName, String genParam) throws DAOException;
	public int getDocUploadCount(String claimNumber, String docId, String genParam)  throws DAOException;
	public int  isAllPendingDocReceived(String claimNumber, String claimTypeUi, String genParam) throws DAOException;
	public Date getPendCompDateReptTbl(String claimNumber, String activityName, String genParam) throws DAOException;
	public int getPendCompCountReptTbl(String claimNumber, String activityName, String genParam) throws DAOException;
	public Date getInvestDocUploadDate(String claimNumber, String docId) throws DAOException;
	public Date getPendDocReceiveDate(String claimNumber, String claimTypeUi) throws DAOException;
	public String getPendingType(String claimNumber) throws DAOException;
	public List<RequirementsDetails> getFUPDescription(String claimNumber) throws DAOException;
	public String getReceiveActIdPendingDoc(String claimNumber) ;
	public String getReceiveActIdInvestDoc(String claimNumber) ;
	public int updateCaseReceiveActTracker(String caseRefKey, String recActId, String recActStatus, String updateReceiveActStatus);
	public Timestamp getDULRegisterDateLA(String claimNumber, String boIdentifier);
	public Object getClaimDataVariable(String claimNumber, String queryString);
	public int getDocNotUploadCount(String claimNumber, String activityName) throws DAOException;
	public int getCountActIdPendingDoc(String claimNumber);
	public int getCountReceiveActIdInvestDoc(String claimNumber);
}
