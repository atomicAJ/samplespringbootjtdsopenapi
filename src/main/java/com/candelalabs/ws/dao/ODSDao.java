package com.candelalabs.ws.dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.candelalabs.api.model.ProductListData;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.*;
import com.candelalabs.ws.model.tables.BankCodeMasterModel;
import com.candelalabs.ws.model.tables.BenCltAddDetails;
import com.candelalabs.ws.model.tables.Beneficiarydetails;
import com.candelalabs.ws.model.tables.BenefitPlanMaster;
import com.candelalabs.ws.model.tables.BenefitSummaryModel;
import com.candelalabs.ws.model.tables.ClaimHistoryModel;
import com.candelalabs.ws.model.tables.CleintDetPolNoBased;
import com.candelalabs.ws.model.tables.DiagnosisMasterModel;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.ComponentCode;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.HospitalMasterDetails;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.model.tables.PolicyClientBeneficiaryAddressList;
import com.candelalabs.ws.model.tables.PolicyClientOwnerPhoneResultSet;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.model.tables.PolicyRelatedModel;
import com.candelalabs.ws.model.tables.UWCommentsModel;

public interface ODSDao {

	public PolicyDetails getPayeeNumber(String polNo) throws DAOException;

	public PolicyDetails getPolicyDetail(String polNo) throws DAOException;

	public WSGENRegClaimBO getWSGENRegClaimData(String policyNumber) throws Exception;

	public WSCLRegClaimBnfcDetailsBOData getWSCLRegClmBeneficiaryDetails(String PolicyNo);

	public WSGENUpdClaimRejectStatusBO getWSGENUpdClaimRejectData(String policyNumber) throws Exception;

	public void updateClaimUiData(JsonWSSaveClaimUIDataIn jsonWSSaveClaimUIDataIn) throws Exception;

	public Beneficiarydetails getPolicyDataRequestorByPOLICYNUMBER(String POLICYNUMBER) throws DAOException;


	public String getComponentCode(String polNo, String clientNo, String comp_status);
	public String getAgentEmailAddr(String polNo)throws DAOException;
	public String getEmailAddrSD(String polNo)throws DAOException;
	public String getEmailAddrNSD(String polNo)throws DAOException;
	
	public String getPaymentFreq(String polNo) throws DAOException;
	
	public String getPolicyClientOwnerAddress1(String polNo);
	public String getPolicyClientOwnerAddress2(String polNo);
	public String getPolicyClientOwnerAddress3(String polNo);
	public String getPolicyClientOwnerAddress4(String polNo);
	public String getPolicyClientOwnerAddress5(String polNo);
	public String getPolicyClientOwnerEmailAddress(String polNo);
	public PolicyClientOwnerPhoneResultSet getPolicyClientOwnerPhoneNos(String polNo) throws DAOException;
	
	public String getPolicyClientBeneficiaryNumber(String polNo) throws DAOException;
	public PolicyClientBeneficiaryAddressList getPolicyClientBeneficiaryAddressList(String clientNo) throws DAOException;
	
	public ComponentDetailsBO getComponentDetailsBO(String PolicyNo, String ComponentCode, String ClientID) throws Exception;

	public ComponentDetailsBO getComponentDetailsBO(String PolicyNo, String ComponentCode, String ClientID,
			String planCode) throws DAOException;
	
	public String getLifefromComponentDetails(String polNum, String clientNo) throws DAOException;
	
	public List<String> getClientNumberList(String policyNo) throws DAOException;
	public List<ComponentDetailsBO> getComponentDetailsBO(String PolicyNo, String clientNumber) throws DAOException;
	public List<BENEFICIARYLIST> getBENEFICIARYLIST(String polNo)throws DAOException;
	
	public BENEFICIARYCLIENTINFO getBENEFICIARYCLIENTINFO(String strBeneficiaryClientNo)throws DAOException;
	public HospitalMasterDetails getHospitalMasterDetails(String ProviderCode) throws DAOException;

	public PolicyDetails getWSPolicyDetails(String PolicyNo);

	public String getpHEmailAddress(String polNo) throws DAOException;

	public String getpHPhoneNo(String polNo) throws DAOException;

	public String getAgentemailadd(String polNo) throws DAOException;

	public ClientDetails getClientDetails(String CLIENTNO) throws Exception;
	
	public ComponentCode getComponentDetails(String PolicyNo, String compCode, String ins_clnt_no) throws DAOException;

	public WSDULRegClaimClaimPolicyDetailsBOData getWSDULClaimPolicyDetails(String policyNo);

	public List<String> getDiagnoseCodeList(String claimTypeUI) throws DAOException;

	public List<String> getDiagnoseNameList(String claimTypeUI) throws DAOException;

	public DiagnosisMasterModel getDiagnosisModelByCode(String diagnosisCode, String claimTypeUI) throws DAOException;

	public List<DiagnosisMasterModel> getDiagnosisModelByName(String diagnosisName, String claimTypeUI) throws DAOException;

	public List<String> getProviderNameList() throws DAOException;

	public List<String> getFactoringHouseList(String currency, String polNum) throws DAOException;

	public BankCodeMasterModel getBankCodeMasterByFactHouse(String factHouse) throws DAOException;

	public List<String> getBenefitCodeList(String compCode,String claimTypeUI) throws DAOException;

	public List<String> getBenefitNameList(String compCode,String claimTypeUI) throws DAOException;

	public String getBenefitCodeByName(String benefitName,String claimTypeUI) throws DAOException;
	
	public String getBenefitNameByCode(String benefitCode,String claimTypeUI) throws DAOException;

	public List<BenefitSummaryModel> getBenefitSummaryResultSet(String benefitCode, String polNum, String planCode,
			Date admissionDate, Date receivedDate) throws DAOException;
	
	public List<BenefitPlanMaster> getBenefitPlanResultSet(String benefitCode, String planCode, String componentCode) throws DAOException;	
	
	public BigDecimal getBenefitPercentage(String compCode, String benefitCode) throws DAOException;
	
	public List<String> getAdjustmentCodeList() throws DAOException;
	
	public List<String> getRequirementList() throws DAOException;
	
	public ODSRequirementMasterModel getRequirementMaster(String docid) throws DAOException;
	public ODSRequirementMasterModel getRequirementMasterbySubCategory(String subcat) throws DAOException, Exception;
	
	public String getListLifeNameString(String polNum) throws DAOException;
	public String getLifenameFromClientDetails(String clientNo) throws DAOException;
	public String getListSubcategory(String docIdList) throws DAOException;
	public String getListBenefitName(String benefitCodeList) throws DAOException;
	public String getSubCategory(String docid)throws DAOException;
	public Date getCompRiskCesDate(String polNum, String componentCode, String clientNumber) throws DAOException;
	
	public Date getCOMPPREMCESDATE(String polNum) throws DAOException;
	public Date getPAIDTODATE(String polNum) throws DAOException;
	Beneficiarydetails getTopClntNameNO(String polNo) throws DAOException;

	String getAddress2(String BenCnltNo) throws DAOException;

	String getAddress3(String BenCnltNo) throws DAOException;

	String getAddress4(String BenCnltNo) throws DAOException;

	String getAddress5(String BenCnltNo) throws DAOException;

	String getAgentEmailAddress(String polNo) throws DAOException;

	String getSDEmailAddress(String polNo) throws DAOException;

	String getNSDEmailAddress(String polNo) throws DAOException;

	CleintDetPolNoBased getCommdetails(String polNo) throws DAOException;

	String getAddress1(String BenCnltNo) throws DAOException;
	BenCltAddDetails getAddress(String BenCnltNo) throws DAOException;
	
	String getSumAssured(String polNum, String lifeClientNo,String componentCode) throws DAOException;

	BigDecimal getAMOUNTANNUALLY(String Compcode, String Benfitcode) throws DAOException;

	BigDecimal getTOTALAMOUNTUSED(String Compcode, String Polno,String Plancode) throws DAOException;
	
	public List<PolicyRelatedModel> getPolicyRelatedData(String insuredClientID) throws DAOException;
	
	public List<ClientDetails> getClientDetailsli(String clietnNo) throws DAOException;
	public List<ClientDetails> getClientDetailsli_2(String polNum) throws DAOException;
	public List<ClientDetails> getClientDetailsli_3(String polNum) throws DAOException;
	
	public String getRelationshipFromBenDetails(String polNum, String clientNo) ;
	public String getPercentageFromBenDetails(String polNum, String clientNo) ;	
	
	public List<ClaimHistoryModel> getClaimHistoryData(String polNum) throws DAOException;
	
	public String getOwnerName(String clientNo) throws DAOException;

	String getProviderCode(String providerName) throws DAOException;
	
	String getComponentCodeStatusUserExit(String ClientNo, String PolNo, String CompCode) throws DAOException;
	
	public PolicyDetails getPolicyDetailsForReport(String PolicyNo) throws DAOException;
	
	public List<ProductListData> getProductList() throws DAOException;

	public CleintDetPolNoBased getClientDetailsData(String clientNo);
	
	public String getReqDocName(String docId) throws DAOException;
	
}
