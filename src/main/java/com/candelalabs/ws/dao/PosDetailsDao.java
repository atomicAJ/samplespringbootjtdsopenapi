package com.candelalabs.ws.dao;

import java.util.List;

import com.candelalabs.epos.model.AdditionalDocument;
import com.candelalabs.epos.model.FundAllocationDB;
import com.candelalabs.epos.model.POSDataSourceCapture;
import com.candelalabs.epos.model.PersonalInfo;
import com.candelalabs.epos.model.PolicyInfo;
import com.candelalabs.epos.model.TransactionCategory;
import com.candelalabs.epos.model.TransactionHistory;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.model.tables.POSDataModel;
import com.candelalabs.ws.model.tables.PolicyDetails;

public interface PosDetailsDao
{

	/**
	 * @param policyNumber
	 * @param transactionId
	 * @return POSDataModel
	 * @throws DAOException
	 */
	public POSDataModel getPosDataModel(String policyNumber, String transactionId) throws DAOException;

	/**
	 * @param tranxId
	 * @return TransactionCategory
	 * @throws DAOException
	 */
	public TransactionCategory getTransactionDetails(String tranxId) throws DAOException;

	/**
	 * @param policyNumber
	 * @return RBAScore
	 * @throws DAOException
	 */
	public Integer getRBAScore(String policyNumber) throws DAOException;

	/**
	 * @param clientNo
	 * @return ClientDetails
	 * @throws Exception
	 */
	public ClientDetails getClientDetails(String clientNo) throws DAOException;

	/**
	 * @param clientNo
	 * @return Client personal info
	 * @throws DAOException
	 */
	public PersonalInfo getClientPersonalInfo(String clientNo) throws DAOException;

	/**
	 * @param policyNumber
	 * @return PolicyInfo
	 * @throws DAOException
	 */
	public PolicyInfo getPolicyInfo(String policyNumber) throws DAOException;

	/**
	 * @param policyNumber
	 * @return Last 14 days transactions
	 * @throws DAOException
	 */
	public List<TransactionHistory> listTransactionHistory(String policyNumber) throws DAOException;

	public List<String> getPolicyRoles(String clientNumber) throws DAOException;

	public List<AdditionalDocument> listAdditionalDocuments(String posRequestNumber) throws DAOException;

	public ODSRequirementMasterModel getRequirementMaster(String docid) throws DAOException;

	public int insertPosDataSourceCapture(POSDataSourceCapture posDataSourceCapture) throws DAOException;

	public PolicyDetails getPolicyDetails(String policyNo) throws DAOException;

	public List<FundAllocationDB> getFundAllocation(String policyNumber) throws DAOException;

	public List<BENEFICIARYLIST> getBenificaryList(String policyNumber) throws DAOException;
}
