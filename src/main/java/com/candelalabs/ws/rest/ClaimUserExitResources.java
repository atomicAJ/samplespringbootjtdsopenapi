/**
 * 
 */
package com.candelalabs.ws.rest;

import javax.validation.Valid;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.candelalabs.api.UserExitApi;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSCheckUISaveFlagDataIn;
import com.candelalabs.api.model.WSCheckUISaveFlagDataOut;
import com.candelalabs.api.model.WSGetProcessCaseSheetCommentsDataIn;
import com.candelalabs.api.model.WSGetProcessCaseSheetCommentsDataOut;
import com.candelalabs.api.model.WSgetComponentCodeStatusDataIn;
import com.candelalabs.api.model.WSgetComponentCodeStatusDataOut;
import com.candelalabs.api.model.WSupdateClaimDecisionDataIn;
import com.candelalabs.api.model.WSupdateClaimDecisionDateDataIn;
import com.candelalabs.api.model.WSupdateClaimProcessorEmailAddDataIn;
import com.candelalabs.api.model.WSupdateClaimUserDataIn;
import com.candelalabs.api.model.WSupdateClaimUserDataOut;
import com.candelalabs.api.model.WSupdateRemarksDataIn;

/**
 * @author Triaji
 *
 */
@RestController
public class ClaimUserExitResources implements UserExitApi {

	@Autowired
	ProducerTemplate producerTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimUIApi#getDiagnosisCodeList(com.candelalabs.api.
	 * model.DataInGetDiagnosisCodeList)
	 */
	@Override
	public ResponseEntity<WSupdateClaimUserDataOut> wSupdateClaimUser(
			@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		WSupdateClaimUserDataOut response = producerTemplate.requestBody("direct:WSupdateClaimUser", dataIn,
				WSupdateClaimUserDataOut.class);

		return new ResponseEntity<WSupdateClaimUserDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimApprovalHAUser(
			@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimApprovalHAUser", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimApprovalHAUserName(
			@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimApprovalHAUserName", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimRejectHAUser(@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimRejectHAUser", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimRejectHAUserName(
			@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimRejectHAUserName", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimUWUser(@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimUWUser", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimHUWUser(@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimHUWUser", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimProcessorName(@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimProcessorName", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimProcessor(@Valid @RequestBody WSupdateClaimUserDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimProcessor", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimDecisionDate(
			@Valid @RequestBody WSupdateClaimDecisionDateDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimDecisionDate", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<JsonResponse> wSupdateNotificationClaimDecisionDate(
			@Valid @RequestBody WSupdateClaimDecisionDateDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateNotificationClaimDecisionDate", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimProcessorEmailAddress(
			@Valid @RequestBody WSupdateClaimProcessorEmailAddDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimProcessorEmailAddress", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}
	
	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimDecision(
			@Valid @RequestBody WSupdateClaimDecisionDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimDecision", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<JsonResponse> wSupdateClaimProcessorInvestigationEmailAddres(
			@Valid @RequestBody WSupdateClaimProcessorEmailAddDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateClaimProcessorInvestigationEmailAddres",
				dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSupdateRemarks(@Valid @RequestBody WSupdateRemarksDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSupdateRemarks", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSgetComponentCodeStatusDataOut> wSgetComponentCodeStatus(
			@Valid @RequestBody WSgetComponentCodeStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		WSgetComponentCodeStatusDataOut response = producerTemplate.requestBody("direct:WSgetComponentCodeStatus",
				dataIn, WSgetComponentCodeStatusDataOut.class);

		return new ResponseEntity<WSgetComponentCodeStatusDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSGetProcessCaseSheetCommentsDataOut> wSGetProcessCaseSheetComments(
			@Valid @RequestBody WSGetProcessCaseSheetCommentsDataIn dataIn) {
		// TODO Auto-generated method stub
		WSGetProcessCaseSheetCommentsDataOut response = producerTemplate.requestBody(
				"direct:WSGetProcessCaseSheetComments", dataIn, WSGetProcessCaseSheetCommentsDataOut.class);
		return new ResponseEntity<WSGetProcessCaseSheetCommentsDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSCheckUISaveFlagDataOut> wSCheckUISaveFlag(
			@Valid @RequestBody WSCheckUISaveFlagDataIn dataIn) {
		// TODO Auto-generated method stub
		WSCheckUISaveFlagDataOut response = producerTemplate.requestBody("direct:WSCheckUISaveFlagRoute", dataIn,
				WSCheckUISaveFlagDataOut.class);
		return new ResponseEntity<WSCheckUISaveFlagDataOut>(response, HttpStatus.OK);
	}
}
