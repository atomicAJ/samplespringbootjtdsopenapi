/**
 * 
 */
package com.candelalabs.ws.rest;

import javax.validation.Valid;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.candelalabs.api.ClaimUIApi;
import com.candelalabs.api.model.ClaimDataUI;
import com.candelalabs.api.model.ClaimDataUIClaimDetails;
import com.candelalabs.api.model.ClaimWorksheetUIData;
import com.candelalabs.api.model.DataInGetBankCodeTypebyFactoringHouse;
import com.candelalabs.api.model.DataInGetBankCodeTypebyFactoringHouseWSInput;
import com.candelalabs.api.model.DataInGetBenefitCodeList;
import com.candelalabs.api.model.DataInGetBenefitCodeListWSInput;
import com.candelalabs.api.model.DataInGetBenefitCodeNameList;
import com.candelalabs.api.model.DataInGetBenefitCodeNameListWSInput1;
import com.candelalabs.api.model.DataInGetBenefitDatabyCode;
import com.candelalabs.api.model.DataInGetBenefitDatabyCodeWSInput;
import com.candelalabs.api.model.DataInGetBenefitDatabyName;
import com.candelalabs.api.model.DataInGetBenefitDatabyNameWSInput;
import com.candelalabs.api.model.DataInGetDiagnosisCodeByName;
import com.candelalabs.api.model.DataInGetDiagnosisCodeByNameWSInput;
import com.candelalabs.api.model.DataInGetDiagnosisCodeList;
import com.candelalabs.api.model.DataInGetDiagnosisCodeListWSInput;
import com.candelalabs.api.model.DataInGetDiagnosisNameByCode;
import com.candelalabs.api.model.DataInGetDiagnosisNameByCodeWSInput;
import com.candelalabs.api.model.DataInGetFactoringHouseList;
import com.candelalabs.api.model.DataInGetFactoringHouseListWSInput;
import com.candelalabs.api.model.DataInGetPolicyDatabyPolicyNumber;
import com.candelalabs.api.model.DataInGetPolicyDatabyPolicyNumberWSInput;
import com.candelalabs.api.model.DataInGetWSGetClaimUIData;
import com.candelalabs.api.model.DataInGetWSGetClaimUIDataWSInput;
import com.candelalabs.api.model.DataInGetWSGetWorksheet;
import com.candelalabs.api.model.DataOutGetAdjustmentCodeList;
import com.candelalabs.api.model.DataOutGetAdjustmentCodeListWSResponse;
import com.candelalabs.api.model.DataOutGetBankCodeTypebyFactoringHouse;
import com.candelalabs.api.model.DataOutGetBankCodeTypebyFactoringHouseWSResponse;
import com.candelalabs.api.model.DataOutGetBenefitCodeList;
import com.candelalabs.api.model.DataOutGetBenefitCodeListWSResponse;
import com.candelalabs.api.model.DataOutGetBenefitDatabyCode;
import com.candelalabs.api.model.DataOutGetBenefitDatabyCodeWSResponse;
import com.candelalabs.api.model.DataOutGetBenefitDatabyName;
import com.candelalabs.api.model.DataOutGetBenefitDatabyNameWSResponse;
import com.candelalabs.api.model.DataOutGetBenefitNameList;
import com.candelalabs.api.model.DataOutGetBenefitNameListWSResponse;
import com.candelalabs.api.model.DataOutGetDiagnosisCodeByName;
import com.candelalabs.api.model.DataOutGetDiagnosisCodeByNameWSResponse;
import com.candelalabs.api.model.DataOutGetDiagnosisCodeList;
import com.candelalabs.api.model.DataOutGetDiagnosisCodeListWSResponse;
import com.candelalabs.api.model.DataOutGetDiagnosisNameByCode;
import com.candelalabs.api.model.DataOutGetDiagnosisNameByCodeWSResponse;
import com.candelalabs.api.model.DataOutGetDiagnosisNameList;
import com.candelalabs.api.model.DataOutGetDiagnosisNameListWSResponse;
import com.candelalabs.api.model.DataOutGetFactoringHouseList;
import com.candelalabs.api.model.DataOutGetFactoringHouseListWSResponse;
import com.candelalabs.api.model.DataOutGetPolicyDatabyPolicyNumber;
import com.candelalabs.api.model.DataOutGetPolicyDatabyPolicyNumberWSResponse;
import com.candelalabs.api.model.DataOutGetProviderNameList;
import com.candelalabs.api.model.DataOutGetProviderNameListWSResponse;
import com.candelalabs.api.model.DataOutGetRequirementList;
import com.candelalabs.api.model.DataOutGetRequirementListWSResponse;
import com.candelalabs.api.model.DataOutWSGetProductList;
import com.candelalabs.api.model.DataOutWSGetProductListWSResponse;
import com.candelalabs.api.model.DataOutWSGetReportTypeList;
import com.candelalabs.api.model.DataOutWSGetReportTypeListWSResponse;
import com.candelalabs.api.model.WSFetchRecordEntriesDataIn;
import com.candelalabs.api.model.WSFetchRecordEntriesDataOut;
import com.candelalabs.api.model.WSGeneratePendingCaseReportDataOut;
import com.candelalabs.api.model.WSGeneratePendingCaseReportDataOutWSResponse;
import com.candelalabs.api.model.WSGetReportNameListDataIn;
import com.candelalabs.api.model.WSGetReportNameListDataOut;

/**
 * @author Triaji
 *
 */
@RestController
public class ClaimUIResources implements ClaimUIApi {

	@Autowired
	ProducerTemplate producerTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimUIApi#getDiagnosisCodeList(com.candelalabs.api.
	 * model.DataInGetDiagnosisCodeList)
	 */
	@Override
	public ResponseEntity<DataOutGetDiagnosisCodeList> getDiagnosisCodeList(
			@Valid @RequestBody DataInGetDiagnosisCodeList dataIn) {
		// TODO Auto-generated method stub
		DataInGetDiagnosisCodeListWSInput wsinput = dataIn.getWsInput();
		DataOutGetDiagnosisCodeListWSResponse wsresponse = producerTemplate.requestBody("direct:WSUIgetDiagCodeList",
				wsinput, DataOutGetDiagnosisCodeListWSResponse.class);
		DataOutGetDiagnosisCodeList response = new DataOutGetDiagnosisCodeList();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetDiagnosisCodeList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetDiagnosisNameList> getDiagnosisNameList(
			@Valid @RequestBody DataInGetDiagnosisCodeList dataIn) {
		// TODO Auto-generated method stub
		DataInGetDiagnosisCodeListWSInput wsinput = dataIn.getWsInput();
		DataOutGetDiagnosisNameListWSResponse wsresponse = producerTemplate
				.requestBody("direct:WSUIgetDiagNameListRoute", wsinput, DataOutGetDiagnosisNameListWSResponse.class);
		DataOutGetDiagnosisNameList response = new DataOutGetDiagnosisNameList();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetDiagnosisNameList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetDiagnosisNameByCode> getDiagnosisNameByCode(
			@Valid @RequestBody DataInGetDiagnosisNameByCode dataIn) {
		// TODO Auto-generated method stub
		DataInGetDiagnosisNameByCodeWSInput wsinput = dataIn.getWsInput();
		DataOutGetDiagnosisNameByCodeWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetDiagNameByCodeRoute", wsinput, DataOutGetDiagnosisNameByCodeWSResponse.class);
		DataOutGetDiagnosisNameByCode response = new DataOutGetDiagnosisNameByCode();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetDiagnosisNameByCode>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetDiagnosisCodeByName> getDiagnosisCodeByName(
			@Valid @RequestBody DataInGetDiagnosisCodeByName dataIn) {
		// TODO Auto-generated method stub
		DataInGetDiagnosisCodeByNameWSInput wsinput = dataIn.getWsInput();
		DataOutGetDiagnosisCodeByNameWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetDiagCodeByNameRoute", wsinput, DataOutGetDiagnosisCodeByNameWSResponse.class);
		DataOutGetDiagnosisCodeByName response = new DataOutGetDiagnosisCodeByName();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetDiagnosisCodeByName>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetProviderNameList> getProviderNameList() {
		// TODO Auto-generated method stub
		DataOutGetProviderNameListWSResponse wsresponse = producerTemplate
				.requestBody("direct:WSUIgetProviderNameListRoute", "Tes", DataOutGetProviderNameListWSResponse.class);
		DataOutGetProviderNameList response = new DataOutGetProviderNameList();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetProviderNameList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetFactoringHouseList> getFactoringHouseList(
			@Valid @RequestBody DataInGetFactoringHouseList dataIn) {
		// TODO Auto-generated method stub
		DataInGetFactoringHouseListWSInput wsinput = dataIn.getWsInput();
		DataOutGetFactoringHouseListWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetFactoringHouseListRoute", wsinput, DataOutGetFactoringHouseListWSResponse.class);
		DataOutGetFactoringHouseList response = new DataOutGetFactoringHouseList();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetFactoringHouseList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetBankCodeTypebyFactoringHouse> getBankCodeTypebyFactoringHouse(
			@Valid @RequestBody DataInGetBankCodeTypebyFactoringHouse dataIn) {
		// TODO Auto-generated method stub
		DataInGetBankCodeTypebyFactoringHouseWSInput wsinput = dataIn.getWsInput();
		DataOutGetBankCodeTypebyFactoringHouseWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetBankCodeTypebyFactoringHouseRoute", wsinput,
				DataOutGetBankCodeTypebyFactoringHouseWSResponse.class);
		DataOutGetBankCodeTypebyFactoringHouse response = new DataOutGetBankCodeTypebyFactoringHouse();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetBankCodeTypebyFactoringHouse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetBenefitNameList> getBenefitNameList(
			@Valid @RequestBody DataInGetBenefitCodeNameList dataIn) {
		// TODO Auto-generated method stub
		DataInGetBenefitCodeNameListWSInput1 wsInput = dataIn.getWsInput1();
		DataOutGetBenefitNameListWSResponse wsresponse = producerTemplate
				.requestBody("direct:WSUIgetBenefitNameListRoute", wsInput, DataOutGetBenefitNameListWSResponse.class);
		DataOutGetBenefitNameList response = new DataOutGetBenefitNameList();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetBenefitNameList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetBenefitDatabyName> getBenefitDatabyName(
			@Valid @RequestBody DataInGetBenefitDatabyName dataIn) {
		// TODO Auto-generated method stub
		DataInGetBenefitDatabyNameWSInput wsinput = dataIn.getWsInput();
		DataOutGetBenefitDatabyNameWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetBenefitDatabyNameRoute", wsinput, DataOutGetBenefitDatabyNameWSResponse.class);
		DataOutGetBenefitDatabyName response = new DataOutGetBenefitDatabyName();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetBenefitDatabyName>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetBenefitDatabyCode> getBenefitDatabyCode(
			@Valid @RequestBody DataInGetBenefitDatabyCode dataIn) {
		// TODO Auto-generated method stub
		DataInGetBenefitDatabyCodeWSInput wsinput = dataIn.getWsInput();
		DataOutGetBenefitDatabyCodeWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetBenefitDatabyCodeRoute", wsinput, DataOutGetBenefitDatabyCodeWSResponse.class);
		DataOutGetBenefitDatabyCode response = new DataOutGetBenefitDatabyCode();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetBenefitDatabyCode>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetPolicyDatabyPolicyNumber> getPolicyDatabyPolicyNumber(
			@Valid @RequestBody DataInGetPolicyDatabyPolicyNumber dataIn) {
		// TODO Auto-generated method stub
		DataInGetPolicyDatabyPolicyNumberWSInput wsinput = dataIn.getWsInput();
		DataOutGetPolicyDatabyPolicyNumberWSResponse wsresponse = producerTemplate.requestBody(
				"direct:WSUIgetPolicyDatabyPolicyNumberRoute", wsinput,
				DataOutGetPolicyDatabyPolicyNumberWSResponse.class);
		DataOutGetPolicyDatabyPolicyNumber response = new DataOutGetPolicyDatabyPolicyNumber();
		response.setWsResponse(wsresponse);

		return new ResponseEntity<DataOutGetPolicyDatabyPolicyNumber>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetAdjustmentCodeList> getAdjustmentCodeList() {
		// TODO Auto-generated method stub
		DataOutGetAdjustmentCodeListWSResponse wsreponse = producerTemplate
				.requestBody("direct:WSUIgetAdjustmentCodeListRoute", "", DataOutGetAdjustmentCodeListWSResponse.class);
		DataOutGetAdjustmentCodeList response = new DataOutGetAdjustmentCodeList();
		response.setWsResponse(wsreponse);
		return new ResponseEntity<DataOutGetAdjustmentCodeList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClaimDataUI> wSGetUIClaimData(@Valid @RequestBody DataInGetWSGetClaimUIData dataIn) {
		// TODO Auto-generated method stub
		DataInGetWSGetClaimUIDataWSInput wsinput = dataIn.getWsInput();
		ClaimDataUIClaimDetails wsresponse = producerTemplate.requestBody("direct:WSUIGetUIClaimDataRoute", wsinput,
				ClaimDataUIClaimDetails.class);
		ClaimDataUI response = new ClaimDataUI();
		response.setClaimDetails(wsresponse);

		return new ResponseEntity<ClaimDataUI>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClaimDataUI> wSSaveUIClaimData(@Valid @RequestBody ClaimDataUI dataIn) {
		// TODO Auto-generated method stub
		ClaimDataUIClaimDetails wsinput = dataIn.getClaimDetails();

		ClaimDataUIClaimDetails wsresponse = producerTemplate.requestBody("direct:WSUISaveUIClaimDataRoute", wsinput,
				ClaimDataUIClaimDetails.class);

		ClaimDataUI response = new ClaimDataUI();
		response.setClaimDetails(wsresponse);

		return new ResponseEntity<ClaimDataUI>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetRequirementList> getRequirementList() {
		// TODO Auto-generated method stub
		DataOutGetRequirementListWSResponse wsresponse = producerTemplate
				.requestBody("direct:WSUIGetRequirementListRoute", "", DataOutGetRequirementListWSResponse.class);
		DataOutGetRequirementList response = new DataOutGetRequirementList();
		response.setWsResponse(wsresponse);
		return new ResponseEntity<DataOutGetRequirementList>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClaimWorksheetUIData> wSSaveWorksheetData(@Valid @RequestBody ClaimWorksheetUIData dataIn) {
		// TODO Auto-generated method stub
		ClaimWorksheetUIData response = producerTemplate.requestBody("direct:WSUISaveWorksheetDataRoute", dataIn,
				ClaimWorksheetUIData.class);
		return new ResponseEntity<ClaimWorksheetUIData>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClaimWorksheetUIData> wSGetWorksheetData(@Valid @RequestBody DataInGetWSGetWorksheet dataIn) {
		// TODO Auto-generated method stub
		ClaimWorksheetUIData response = producerTemplate.requestBody("direct:WSUIGetWorksheetDataRoute", dataIn,
				ClaimWorksheetUIData.class);

		return new ResponseEntity<ClaimWorksheetUIData>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSFetchRecordEntriesDataOut> wSFetchRecordEntries(
			@Valid @RequestBody WSFetchRecordEntriesDataIn dataIn) {
		// TODO Auto-generated method stub
		WSFetchRecordEntriesDataOut response = producerTemplate.requestBody("direct:WSFetchRecordEntries", dataIn,
				WSFetchRecordEntriesDataOut.class);

		return new ResponseEntity<WSFetchRecordEntriesDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutGetBenefitCodeList> getBenefitCodeList(
			@Valid @RequestBody DataInGetBenefitCodeList dataIn) {
		// TODO Auto-generated method stub
		DataInGetBenefitCodeListWSInput wsInput = dataIn.getWsInput();
		DataOutGetBenefitCodeListWSResponse response = producerTemplate
				.requestBody("direct:WSUIgetBenefitCodeListRoute", wsInput, DataOutGetBenefitCodeListWSResponse.class);
		DataOutGetBenefitCodeList wsresponse = new DataOutGetBenefitCodeList();
		wsresponse.setWsResponse(response);
		return new ResponseEntity<DataOutGetBenefitCodeList>(wsresponse, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutWSGetProductList> wSGetProductList() {
		// TODO Auto-generated method stub
		DataOutWSGetProductListWSResponse wsResponse = producerTemplate.requestBody("direct:WSGetProductListProcessor", "Tes",DataOutWSGetProductListWSResponse.class);
		DataOutWSGetProductList response = new DataOutWSGetProductList();
		response.setWsResponse(wsResponse);
		
		return new ResponseEntity<DataOutWSGetProductList>(response, HttpStatus.OK);
	}	

	@Override
	public ResponseEntity<WSGetReportNameListDataOut> wSGetReportNameList(@Valid @RequestBody WSGetReportNameListDataIn dataIn) {
		// TODO Auto-generated method stub
		WSGetReportNameListDataOut response = producerTemplate.requestBody("direct:WSGetReportNameListProcessor", dataIn, WSGetReportNameListDataOut.class);
		return new ResponseEntity<WSGetReportNameListDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DataOutWSGetReportTypeList> wSGetReportTypeList() {
		// TODO Auto-generated method stub
		DataOutWSGetReportTypeListWSResponse wsResponse = producerTemplate.requestBody("direct:WSGetReportTypeListProcessor", "", DataOutWSGetReportTypeListWSResponse.class);
		DataOutWSGetReportTypeList response = new DataOutWSGetReportTypeList();
		response.setWsResponse(wsResponse);
		
		return new ResponseEntity<DataOutWSGetReportTypeList>(response, HttpStatus.OK);
	}
	
}
