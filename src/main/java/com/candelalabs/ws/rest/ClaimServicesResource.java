/**
 * 
 */
package com.candelalabs.ws.rest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.candelalabs.api.ClaimServicesApi;
import com.candelalabs.api.model.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Triaji
 *
 */
@RestController
public class ClaimServicesResource implements ClaimServicesApi {

	@Autowired
	ProducerTemplate producerTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimServicesApi#signalReceiveTask(com.candelalabs.
	 * api.model.SignalDetails)
	 */
	@Override
	public ResponseEntity<JsonResponse> signalReceiveTask(@Valid @RequestBody SignalDetails signalDetails) {
		// TODO Auto-generated method stub

		JsonResponse signal = producerTemplate.requestBody("direct:SignalTaskRoute", signalDetails, JsonResponse.class);
		// producerTemplate.sendBody("direct:firstRoute", "tes123");
		return new ResponseEntity<JsonResponse>(signal, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimServicesApi#wSDNULAdjClaimAmtUserDecision(com.
	 * candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataIn)
	 */
	@Override
	public ResponseEntity<WSDNULAdjClaimAmtUserDecisionDataOut> wSDNULAdjClaimAmtUserDecision(
			@Valid @RequestBody WSDNULAdjClaimAmtUserDecisionDataIn wsDNULDataIn) {
		// TODO Auto-generated method stub
		WSDNULAdjClaimAmtUserDecisionDataOut response = producerTemplate.requestBody("direct:WSDNUlAdjClaimRoute",
				wsDNULDataIn, WSDNULAdjClaimAmtUserDecisionDataOut.class);
		return new ResponseEntity<WSDNULAdjClaimAmtUserDecisionDataOut>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimServicesApi#wSCLRegClaim(com.candelalabs.api.
	 * model.WSCLRegClaimDataIn)
	 */
	@Override
	public ResponseEntity<JsonResponse> wSCLRegClaim(@Valid @RequestBody WSCLRegClaimDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSCLRegClaimRoute", dataIn, JsonResponse.class);
		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.candelalabs.api.ClaimServicesApi#wSCLUpdAppClaimStatus(com.
	 * candelalabs.api.model.WSCLUpdAppClaimStatusDataIn)
	 */
	@Override
	public ResponseEntity<JsonResponse> wSCLUpdAppClaimStatus(@Valid @RequestBody WSCLUpdAppClaimStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSCLUpdAppClaimStatus", dataIn,
				JsonResponse.class);
		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimServicesApi#wSDNULRegClaim(com.candelalabs.api.
	 * model.WSDNULRegClaimDataIn)
	 */
	@Override
	public ResponseEntity<JsonResponse> wSDNULRegClaim(@Valid @RequestBody WSDNULRegClaimDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDNULRegClaimRoute", dataIn, JsonResponse.class);
		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.candelalabs.api.ClaimServicesApi#wSDNULUpdClaimRejectStatus(com.
	 * candelalabs.api.model.WSDNULUpdClaimRejectStatusDataIn)
	 */
	@Override
	public ResponseEntity<JsonResponse> wSDNULUpdClaimRejectStatus(
			@Valid @RequestBody WSDNULUpdClaimRejectStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDNULUpdClaimRejectStatusRoute", dataIn,
				JsonResponse.class);
		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.candelalabs.api.ClaimServicesApi#wSPolicyDataRequestorProcessor
	 * (com.candelalabs.api.model.JsonWSPolicyDataRequestorDataIn)
	 */
	@Override
	public ResponseEntity<JsonWSPolicyDataRequestorDataOut> wSPolicyDataRequestorProcessor(
			@Valid @RequestBody JsonWSPolicyDataRequestorDataIn wsInputObject) {
		// TODO Auto-generated method stub
		JsonWSPolicyDataRequestorDataOut response = producerTemplate.requestBody(
				"direct:WSPolicyDataRequestorProcessor", wsInputObject, JsonWSPolicyDataRequestorDataOut.class);
		return new ResponseEntity<JsonWSPolicyDataRequestorDataOut>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.candelalabs.api.ClaimServicesApi#wSPolicyDataRequestorProcessor
	 * (com.candelalabs.api.model.JsonWSPolicyDataRequestorDataIn)
	 */
	@Override
	public ResponseEntity<JsonWSChkExcSrcProcessorDataOut> wSChkExcSrc(
			@Valid @RequestBody JsonWSChkExcSrcProcessorDataIn wsInputObject) {
		// TODO Auto-generated method stub
		JsonWSChkExcSrcProcessorDataOut response = producerTemplate.requestBody("direct:WSChkExcSrc", wsInputObject,
				JsonWSChkExcSrcProcessorDataOut.class);
		return new ResponseEntity<JsonWSChkExcSrcProcessorDataOut>(response, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.api.ClaimServicesApi#wSCheckMandatoryDocs(com.candelalabs
	 * .api.model.WSCheckMandatoryDocsDataIn)
	 */
	@Override
	public ResponseEntity<WSCheckMandatoryDocsDataOut> wSCheckMandatoryDocs(
			@Valid @RequestBody WSCheckMandatoryDocsDataIn dataIn) {
		// TODO Auto-generated method stub
		WSCheckMandatoryDocsDataOut response = producerTemplate.requestBody("direct:WSCheckMandatoryDocsRoute", dataIn,
				WSCheckMandatoryDocsDataOut.class);
		return new ResponseEntity<WSCheckMandatoryDocsDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSCustPendingNotification(
			@Valid @RequestBody JsonWSRegisterRequirementDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSCustPendingNotification", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDULAdjClaimAmtUserDecision(
			@Valid @RequestBody WSDULAdjClaimAmtUserDecisionDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDULAdjClaimAmtUserDecisionRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDULRegClaim(@Valid @RequestBody WSDULRegClaimDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDULRegClaimRoute", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDULActualAmount(@Valid @RequestBody WSDULActualAmountDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDULActualAmountRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDULUpdClaimRejectStatus(
			@Valid @RequestBody WSDULUpdClaimRejectStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDULUpdClaimRejectStatusRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDULActualClaimAmount(
			@Valid @RequestBody WSDULActualClaimAmountDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDULActualClaimAmountRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDNULUpdClaimApproveStatus(
			@Valid @RequestBody WSDNULUpdClaimApproveDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDNULUpdClaimApproveRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSManageReportEntriesDataOut> wSManageReportEntries(
			@Valid @RequestBody WSManageReportEntriesDataIn dataIn) {
		// TODO Auto-generated method stub
		WSManageReportEntriesDataOut response = producerTemplate.requestBody("direct:WSManageReportEntriesRoute",
				dataIn, WSManageReportEntriesDataOut.class);

		return new ResponseEntity<WSManageReportEntriesDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSRegisterNotification(
			@Valid @RequestBody WSRegisterNotificationDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSRegisterNotificationRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSDetermineAuthorityLimitDataOut> wSDetermineAuthorityLimit(
			@Valid @RequestBody WSDetermineAuthorityLimitDataIn dataIn) {
		// TODO Auto-generated method stub
		WSDetermineAuthorityLimitDataOut response = producerTemplate
				.requestBody("direct:WSDetermineAuthorityLimitRoute", dataIn, WSDetermineAuthorityLimitDataOut.class);

		return new ResponseEntity<WSDetermineAuthorityLimitDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSUpdateClaimStatus(@Valid @RequestBody WSUpdateClaimStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSUpdateClaimStatusRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSGENUpdClaimRejectStatus(
			@Valid @RequestBody WSGENUpdClaimRejectDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSGENUpdClaimRejectRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSGENUpdClaimApproveStatus(
			@Valid @RequestBody WSGENUpdClaimApproveStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSGENUpdClaimApproveStatusRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSGENRegClaim(@Valid @RequestBody WSGENRegClaimDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSGENRegClaimRoute", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSUpdateRequirement(@Valid @RequestBody WSUpdateRequirementDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSUpdateRequirementRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSRegisterRequirement(@Valid @RequestBody WSRegisterRequirementDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSGenerateFiles(@Valid @RequestBody JsonWSGenerateFilesDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSGenerateFilesRoute", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSGenericUpdateService(@Valid @RequestBody WSGenericServiceDataIn dataIn) {
		JsonResponse response = producerTemplate.requestBody("direct:WSGenericUpdateService", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSDocReIndex(@Valid @RequestBody DataInDocReindex dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:WSDocReIndexRoute", dataIn, JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSPortalServiceDataOut> wSPortalService(@Valid @RequestBody WSPortalServiceDataIn dataIn) {
		// TODO Auto-generated method stub
		WSPortalServiceDataOut response = producerTemplate.requestBody("direct:WSDocReIndexRoute", dataIn,
				WSPortalServiceDataOut.class);

		return new ResponseEntity<WSPortalServiceDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSPortalServiceDataIn> wSPortalServiceCreateClaim(
			@Valid @RequestBody WSPortalServiceDataIn dataIn) {
		// TODO Auto-generated method stub
		WSPortalServiceDataIn response = producerTemplate.requestBody("direct:WSPortalServiceCreateClaim", dataIn,
				WSPortalServiceDataIn.class);

		return new ResponseEntity<WSPortalServiceDataIn>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSPortalServiceDataOut> wSPortalServiceGetClaimDetails(
			@Valid @RequestBody ClaimDetailsPortalInput dataIn) {
		// TODO Auto-generated method stub
		WSPortalServiceDataOut response = producerTemplate.requestBody("direct:WSPortalServiceGetClaimDetails", dataIn,
				WSPortalServiceDataOut.class);

		return new ResponseEntity<WSPortalServiceDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ClaimDataPortalOutput> wSPortalServiceGetClaimData(
			@Valid @RequestBody ClaimDataPortalInput dataIn) {
		// TODO Auto-generated method stub
		ClaimDataPortalOutput response = producerTemplate.requestBody("direct:WSPortalServiceGetClaimData", dataIn,
				ClaimDataPortalOutput.class);

		return new ResponseEntity<ClaimDataPortalOutput>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DetCL06CompCodeDataOut> wSDNULDetCL06CompCode(
			@Valid @RequestBody DetRegDeathCompCodeDataIn dataIn) {
		// TODO Auto-generated method stub
		DetCL06CompCodeDataOut response = producerTemplate.requestBody("direct:wSDNULDetCL06CompCodeRoute", dataIn,
				DetCL06CompCodeDataOut.class);

		return new ResponseEntity<DetCL06CompCodeDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DetDeathCompCodeDataOut> wSDULDetAppDeathCompCode(
			@Valid @RequestBody DetAppDeathCompCodeDataIn dataIn) {
		// TODO Auto-generated method stub
		DetDeathCompCodeDataOut response = producerTemplate.requestBody("direct:wSDULDetAppDeathCompCodeRoute", dataIn,
				DetDeathCompCodeDataOut.class);

		return new ResponseEntity<DetDeathCompCodeDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DetDeathCompCodeDataOut> wSDULDetRegDeathCompCode(
			@Valid @RequestBody DetRegDeathCompCodeDataIn dataIn) {
		// TODO Auto-generated method stub
		DetDeathCompCodeDataOut response = producerTemplate.requestBody("direct:wSDULDetRegDeathCompCodeRoute", dataIn,
				DetDeathCompCodeDataOut.class);

		return new ResponseEntity<DetDeathCompCodeDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSUpdateClaimStatusInClaimData(
			@Valid @RequestBody WSUpdateClaimStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse response = producerTemplate.requestBody("direct:wSUpdateClaimStatusInClaimDataRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}

	//
	@Override
	public ResponseEntity<WSUpdateClaimSTPStatusDataOut> wSUpdateClaimSTPStatus(
			@Valid @RequestBody WSUpdateClaimSTPStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		WSUpdateClaimSTPStatusDataOut response = producerTemplate.requestBody("direct:WSUpdateClaimSTPStatus", dataIn,
				WSUpdateClaimSTPStatusDataOut.class);

		return new ResponseEntity<WSUpdateClaimSTPStatusDataOut>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSUpdateReqtActivityID(
			@Valid @RequestBody WSUpdateReqtActivityIDDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse responseBoi = producerTemplate.requestBody("direct:WSUpdateReqtActivityIDRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(responseBoi, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSUpdateClaimDataGeneric(
			@Valid @RequestBody WSUpdateClaimDataGenericDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse responseBoi1 = producerTemplate.requestBody("direct:wSUpdateClaimDataGenericRoute", dataIn,
				JsonResponse.class);
		return new ResponseEntity<JsonResponse>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonWSGetAgentDetailsDataOut> wSGetAgentDetails(
			@Valid @RequestBody JsonWSGetAgentDetailsDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonWSGetAgentDetailsDataOut responseBoi1 = producerTemplate.requestBody("direct:WSGetAgentDetails", dataIn,
				JsonWSGetAgentDetailsDataOut.class);
		return new ResponseEntity<JsonWSGetAgentDetailsDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonWSGetAgentDetailsDataOut> wSUpdateAgentDetails(
			@Valid @RequestBody JsonWSGetAgentDetailsDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonWSGetAgentDetailsDataOut responseBoi1 = producerTemplate.requestBody("direct:WSUpdateAgentDetails", dataIn,
				JsonWSGetAgentDetailsDataOut.class);
		return new ResponseEntity<JsonWSGetAgentDetailsDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonWSGetAgentDetailsDataOut> wSUpdateClientDetails(
			@Valid  @RequestBody JsonWSUpdateClientDetailsDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonWSGetAgentDetailsDataOut responseBoi1 = producerTemplate.requestBody("direct:WSUpdateClientDetails", dataIn,
				JsonWSGetAgentDetailsDataOut.class);
		return new ResponseEntity<JsonWSGetAgentDetailsDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSCommonDataOut> wSCreateCaptureRecord(
			@Valid @RequestBody WSCreateCaptureRecordDataIn dataIn) {
		// TODO Auto-generated method stub
		WSCommonDataOut responseBoi1 = producerTemplate.requestBody("direct:WSCreateCaptureRecordProcessor", dataIn,
				WSCommonDataOut.class);
		return new ResponseEntity<WSCommonDataOut>(responseBoi1, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<JsonResponse> wSProcessRuleRecord(@Valid @RequestBody WSProcessRuleRecordDataIn dataIn) {
		JsonResponse response = producerTemplate.requestBody("direct:wSProcessRuleRecordRoute", dataIn,
				JsonResponse.class);

		return new ResponseEntity<JsonResponse>(response, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<WSGenerateSTPReportDataOut> wSGenerateSTPReportData(
			@Valid @RequestBody WSGenerateSTPReportDataIn dataIn) {
		// TODO Auto-generated method stub
		WSGenerateSTPReportDataOut responseBoi1 = producerTemplate.requestBody("direct:WSGenerateSTPReportDataProcessor", dataIn,
				WSGenerateSTPReportDataOut.class);
		return new ResponseEntity<WSGenerateSTPReportDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSGenerateTATReportDataOut> wSGenerateTATReportData(
			@Valid @RequestBody WSGenerateTATReportDataIn dataIn) {
		// TODO Auto-generated method stub
		WSGenerateTATReportDataOut responseBoi1 = producerTemplate.requestBody("direct:WSGenerateTATReportDataProcessor", dataIn,
				WSGenerateTATReportDataOut.class);
		return new ResponseEntity<WSGenerateTATReportDataOut>(responseBoi1, HttpStatus.OK);
	}

	@GetMapping("checkHealth")
	public ResponseEntity<String> checkHealth() {
		return new ResponseEntity<>("Health is OK", HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSReportRequestDataOut> wSReportRequest(@Valid @RequestBody WSReportRequestDataIn dataIn) {
		// TODO Auto-generated method stub
		WSReportRequestDataOut responseBoi1 = producerTemplate.requestBody("direct:WSReportRequestProcessor", dataIn,
				WSReportRequestDataOut.class);
		return new ResponseEntity<WSReportRequestDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSReportCheckStatusDataOut> wSReportCheckStatus(
			@Valid  @RequestBody WSReportCheckStatusDataIn dataIn) {
		// TODO Auto-generated method stub
		WSReportCheckStatusDataOut responseBoi1 = producerTemplate.requestBody("direct:WSReportCheckStatusProcessor", dataIn,
				WSReportCheckStatusDataOut.class);
		return new ResponseEntity<WSReportCheckStatusDataOut>(responseBoi1, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<JsonResponse> wSUpdateData(
			@Valid  @RequestBody WSUpdateDataDataIn dataIn) {
		// TODO Auto-generated method stub
		JsonResponse responseBoi1 = producerTemplate.requestBody("direct:WSUpdateDataProcessor", dataIn,
				JsonResponse.class);
		return new ResponseEntity<JsonResponse>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSReportOutputXLSDataOut> wSReportOutputXLS(
			@Valid @RequestBody WSReportOutputXLSDataIn dataIn) {
		// TODO Auto-generated method stub
		WSReportOutputXLSDataOut responseBoi1 =	producerTemplate.requestBody("direct:WSReportOutputXLSProcessor", dataIn,
				WSReportOutputXLSDataOut.class); 
		return new	ResponseEntity<WSReportOutputXLSDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSWSValidatePendingRequirementsDataOut> wSValidatePendingRequirements(
			@Valid @RequestBody WSWSValidatePendingRequirementsDataIn dataIn) {
		// TODO Auto-generated method stub
		WSWSValidatePendingRequirementsDataOut responseBoi1 = producerTemplate.requestBody("direct:WSValidatePendingRequirementsProcessor", dataIn,
				WSWSValidatePendingRequirementsDataOut.class);
		return new ResponseEntity<WSWSValidatePendingRequirementsDataOut>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<JsonResponse> wSCaseDecisionTracker(@Valid @RequestBody CaseDecisionTrackerModel dataIn) {
		JsonResponse responseBoi1 = producerTemplate.requestBody("direct:wSCaseDecisionTracker", dataIn,
				JsonResponse.class);
		return new ResponseEntity<JsonResponse>(responseBoi1, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSGeneratePendingCaseReportDataOut> wSGeneratePendingCaseReport(
			@Valid @RequestBody WSGeneratePendingCaseReportDataIn dataIn) {
		WSGeneratePendingCaseReportDataOutWSResponse wsResponse = producerTemplate.requestBody("direct:WSGeneratePendingCaseReportProcessor", dataIn, WSGeneratePendingCaseReportDataOutWSResponse.class);
		WSGeneratePendingCaseReportDataOut response = new WSGeneratePendingCaseReportDataOut();
		response.setWsResponse(wsResponse);
		return new ResponseEntity<WSGeneratePendingCaseReportDataOut>(response, HttpStatus.OK);
	}
	@Override
	public ResponseEntity<JsonResponse> wSUpdateBusinessVarProcessor(@Valid @RequestBody SignalDetails signalDetails) {
		// TODO Auto-generated method stub

		JsonResponse signal = producerTemplate.requestBody("direct:wSUpdateBusinessVarProcessor", signalDetails, JsonResponse.class);
		// producerTemplate.sendBody("direct:firstRoute", "tes123");
		return new ResponseEntity<JsonResponse>(signal, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<WSDownloadGeneratedReportDataOut> wSDownloadGeneratedReport(
			@Valid @RequestBody WSDownloadGeneratedReportDataIn wsDownloadGeneratedReportDataIn) {
		
		WSDownloadGeneratedReportDataOutWSResponse wsResponse = producerTemplate.requestBody("direct:WSDownloadGeneratedReportProcessor", wsDownloadGeneratedReportDataIn, WSDownloadGeneratedReportDataOutWSResponse.class);
		WSDownloadGeneratedReportDataOut response = new WSDownloadGeneratedReportDataOut();
		response.setWsResponse(wsResponse);
		
		return new ResponseEntity<WSDownloadGeneratedReportDataOut>(response, HttpStatus.OK);
		
	}

	@Override
	public ResponseEntity<WsResponseDataOut> wSGetClaimDataVariable(@RequestParam @NotNull @Valid String claimnumber,@RequestParam @NotNull @Valid String queryString) {
		Map<String,String> dataIn = new HashMap<>();
		dataIn.put("claimnumber",claimnumber);
		dataIn.put("queryString",queryString);
		WsResponseDataOut dataOut = producerTemplate.requestBody("direct:wsGetClaimVariableProcessor", dataIn, WsResponseDataOut.class);


		return new ResponseEntity<WsResponseDataOut>(dataOut, HttpStatus.OK);
	}
}
