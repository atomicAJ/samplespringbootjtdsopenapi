/**
 * 
 */
package com.candelalabs.ws.rest;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.ws.services.*;

/**
 * @author Triaji
 *
 */
@Component
public class CamelRoutes extends RouteBuilder
{
	@Autowired
	WsGetClaimVariableProcessor wsGetClaimVariableProcessor;
	@Autowired
	private WSSignalReceiveTaskProcessor wSSignalReceiveTaskProcessor;
	@Autowired
	private WSDNULAdjClaimAmtUserDecisionProcessor wSDNULAdjClaimAmtUserDecisionProcessor;
	@Autowired
	private WSCLRegClaimProcessor wSCLRegClaimProcessor;
	@Autowired
	private WSCLUpdAppClaimStatusProcessor wSCLUpdAppClaimStatusProcessor;
	@Autowired
	private WSDNULRegClaimProcessor wSDNULRegClaimProcessor;
	@Autowired
	private WSDNULUpdClaimRejectStatusProcessor wSDNULUpdClaimRejectStatusProcessor;
	@Autowired
	private WSPolicyDataRequestorProcessor wSPolicyDataRequestorProcessor;
	@Autowired
	private WSChkExcSrcProcessor wSChkExcSrcProcessor;
	@Autowired
	private WSCheckMandatoryDocsProcessor wSCheckMandatoryDocsProcessor;
	@Autowired
	private WSManageReportEntriesProcessor wSManageReportEntriesProcessor;
	@Autowired
	private WSCustPendingNotificationProcessor wSCustPendingNotificationProcessor;
	@Autowired
	private WSDULAdjClaimAmtUserDecisionProcessor wSDULAdjClaimAmtUserDecisionProcessor;
	@Autowired
	private WSDULRegClaimDataProcessor wSDULRegClaimDataProcessor;
	@Autowired
	private WSDULActualAmountProcessor wSDULActualAmountProcessor;
	@Autowired
	private WSDULUpdClaimRejectStatusProcessor wSDULUpdClaimRejectStatusProcessor;
	@Autowired
	private WSDULActualClaimAmountProcessor wSDULActualClaimAmountProcessor;
	@Autowired
	private WSDNULUpdClaimApproveStatusProcessor wSDNULUpdClaimApproveStatusProcessor;
	@Autowired
	private WSRegisterNotificationProcesssor wSRegisterNotificationProcesssor;
	@Autowired
	private WSDetermineAuthorityLimitProcessor wSDetermineAuthorityLimitProcessor;
	@Autowired
	private WSUpdateClaimStatusProcessor wSUpdateClaimStatusProcessor;
	@Autowired
	private WSGENUpdClaimRejectProcessor wSGENUpdClaimRejectProcessor;
	@Autowired
	private WSGENUpdClaimApproveStatusProcesssor wSGENUpdClaimApproveStatusProcesssor;
	@Autowired
	private WSGENRegClaimProcessor wSGENRegClaimProcessor;
	@Autowired
	private WSUIGetDiagCodeList wSUIGetDiagCodeList;
	@Autowired
	private WSUIGetDiagNameListProcessor wSUIGetDiagNameListProcessor;
	@Autowired
	private WSUIGetDiagNameByCodeProcessor wSUIGetDiagNameByCodeProcessor;
	@Autowired
	private WSUIGetDiagCodeByNameProcessor wSUIGetDiagCodeByNameProcessor;
	@Autowired
	private WSUIGetProviderNameListProcessor wSUIGetProviderNameListProcessor;
	@Autowired
	private WSUIGetFactoringHouseListProcessor wSUIGetFactoringHouseListProcessor;
	@Autowired
	private WSUIGetBankCodeTypeByFactHouseProcessor wSUIGetBankCodeTypeByFactHouseProcessor;
	@Autowired
	private WSUIGetBenefitCodeListProcessor wSUIGetBenefitCodeListProcessor;
	@Autowired
	private WSUIGetBenefitNameListProcessor wSUIGetBenefitNameListProcessor;
	@Autowired
	private WSUIGetBenefitDataByNameProcessor wSUIGetBenefitDataByNameProcessor;
	@Autowired
	private WSUIGetBenefitDataByCodeProcessor wSUIGetBenefitDataByCodeProcessor;
	@Autowired
	private WSUIGetPolicyDatabyPolicyNumberProcessor wSUIGetPolicyDatabyPolicyNumberProcessor;
	@Autowired
	private WSUIGetAdjustmentCodeListProcessor wSUIGetAdjustmentCodeListProcessor;
	@Autowired
	private WSUpdateRequirementProcessor wSUpdateRequirementProcessor;
	@Autowired
	private WSRegisterRequirementProcessor wSRegisterRequirementProcessor;
	@Autowired
	private WSUIGetRequirementListProcessor wSUIGetRequirementListProcessor;
	@Autowired
	private WSUIGetClaimData wSUIGetClaimData;
	@Autowired
	private WSUISaveClaimData wSUISaveClaimData;
	@Autowired
	private WSGenerateFilesProcessor wSGenerateFilesProcessor;
	@Autowired
	private WSUISaveWorksheetDataProcessor wSUISaveWorksheetDataProcessor;
	@Autowired
	private WSUIGetWorksheetDataProcessor wSUIGetWorksheetDataProcessor;
	@Autowired
	private WSDocReIndexProcessor wSDocReIndexProcessor;
	@Autowired
	private WSPortalServiceCreateClaim wSPortalServiceCreateClaim;
	@Autowired
	private WSPortalServiceGetClaimDetails wSPortalServiceGetClaimDetails;
	@Autowired
	private WSPortalServiceGetClaimData wSPortalServiceGetClaimData;
	@Autowired
	private WSupdateClaimUser wSupdateClaimUser;
	@Autowired
	private WSupdateClaimApprovalHAUser wSupdateClaimApprovalHAUser;
	@Autowired
	private WSupdateClaimApprovalHAUserName wSupdateClaimApprovalHAUserName;
	@Autowired
	private WSupdateClaimRejectHAUser wSupdateClaimRejectHAUser;
	@Autowired
	private WSupdateClaimRejectHAUserName wSupdateClaimRejectHAUserName;
	@Autowired
	private WSupdateClaimUWUser wSupdateClaimUWUser;
	@Autowired
	private WSupdateClaimHUWUser wSupdateClaimHUWUser;
	@Autowired
	private WSupdateClaimProcessorName wSupdateClaimProcessorName;
	@Autowired
	private WSupdateClaimProcessor wSupdateClaimProcessor;
	@Autowired
	private WSupdateClaimDecisionDate wSupdateClaimDecisionDate;
	@Autowired
	private WSupdateClaimProcessorEmailAddress wSupdateClaimProcessorEmailAddress;
	@Autowired
	private WSupdateClaimProcessorInvestigationEmailAddres wSupdateClaimProcessorInvestigationEmailAddres;
	@Autowired
	private WSupdateRemarks wSupdateRemarks;
	@Autowired
	private WSgetComponentCodeStatus wSgetComponentCodeStatus;
	@Autowired
	private WSGetProcessCaseSheetCommentsProcessor wSGetProcessCaseSheetCommentsProcessor;
	@Autowired
	private WSCheckUISaveFlagProcessor wSCheckUISaveFlagProcessor;
	@Autowired
	private WSFetchRecordEntries wSFetchRecordEntries;
	@Autowired
	private WSDNULDetCL06CompCodeProcessor wSDNULDetCL06CompCodeProcessor;
	@Autowired
	private WSDULDetAppDeathCompCodeProcessor wSDULDetAppDeathCompCodeProcessor;
	@Autowired
	private WSDULDetRegDeathCompCodeProcessor wSDULDetRegDeathCompCodeProcessor;
	@Autowired
	private WSUpdateClaimStatusInClaimDataProcessor wSUpdateClaimStatusInClaimDataProcessor;
	@Autowired
	private WSUpdateClaimSTPStatus wSUpdateClaimSTPStatus;
	@Autowired
	private WSProcessInstanceMigrator wSProcessInstanceMigrator;
	@Autowired
	private WSupdateNotificationClaimDecisionDate wSupdateNotificationClaimDecisionDate;
	@Autowired
	private WSupdateClaimDecision wSupdateClaimDecision;
	@Autowired
	private WSUpdateReqActivityIDProcessor wSUpdateReqActivityIDProcessor;
	@Autowired
	private WSUpdateClaimDataGeneralProcessor wSUpdateClaimDataGeneralProcessor;
	@Autowired
	private WSGetAgentDetails wSGetAgentDetails;
	@Autowired
	private WSUpdateAgentDetails wSUpdateAgentDetails;
	@Autowired
	private WSUpdateClientDetails wSUpdateClientDetails;
	@Autowired
	private WSGenericUpdateServiceProcessor wSGenericUpdateServiceProcessor;
	@Autowired
	private WSCreateCaptureRecordProcessor wSCreateCaptureRecordProcessor;
	@Autowired
	private WSGenerateSTPReportDataProcessor wSGenerateSTPReportDataProcessor;
	@Autowired
	private WSGenerateTATReportDataProcessor wSGenerateTATReportDataProcessor;
	@Autowired
	private WSProcessRuleRecordProcessor wsProcessRuleRecordProcessor;
	@Autowired
	private WSReportRequestProcessor wsReportRequestProcessor;
	@Autowired
	private WSReportCheckStatusProcessor wsReportCheckStatusProcessor;
	@Autowired
	private WSUpdateDataProcessor wSUpdateDataProcessor;
	@Autowired
	WSReportOutputXLSProcessor wsReportOutputXLSProcessor;
	@Autowired
	WSValidatePendingRequirementsProcessor wsValidatePendingRequirementsProcessor;
	@Autowired
	WSGetProductListProcessor wsGetProductListProcessor;
	@Autowired
	WSGetReportNameListProcessor wsGetReportNameListProcessor;
	@Autowired
	WSGetReportTypeListProcessor wsGetReportTypeListProcessor;
	@Autowired
	WSCaseDecisionTrackerProcessor wsCaseDecisionTrackerProcessor;
	@Autowired
	WSGeneratePendingCaseReportProcessor wsGeneratePendingCaseReportProcessor;
	@Autowired
	WSUpdateBusinessVarProcessor wSUpdateBusinessVarProcessor;
	@Autowired
	WSDownloadGeneratedReportProcessor wsDownloadGeneratedReportProcessor;
	@Autowired
	WSPolicyServiceProcessor wsPolicyServiceProcesseor;
	@Autowired
	WSCreateCaptureRecordPosProcessor wsCreateCaptureRecordPosProcessor;
	
	@Override
	public void configure() throws Exception
	{

		// TODO Auto-generated method stub
		from("direct:PolicyService").process(wsPolicyServiceProcesseor);

		from("direct:SignalTaskRoute").process(wSSignalReceiveTaskProcessor);

		from("direct:WSDNUlAdjClaimRoute").process(wSDNULAdjClaimAmtUserDecisionProcessor);

		from("direct:WSCLRegClaimRoute").process(wSCLRegClaimProcessor);

		from("direct:WSCLUpdAppClaimStatus").process(wSCLUpdAppClaimStatusProcessor);

		from("direct:WSDNULRegClaimRoute").process(wSDNULRegClaimProcessor);

		from("direct:WSDNULUpdClaimRejectStatusRoute").process(wSDNULUpdClaimRejectStatusProcessor);

		from("direct:WSPolicyDataRequestorProcessor").process(wSPolicyDataRequestorProcessor);

		from("direct:WSChkExcSrc").process(wSChkExcSrcProcessor);

		from("direct:WSCheckMandatoryDocsRoute").process(wSCheckMandatoryDocsProcessor);

		from("direct:WSManageReportEntriesRoute").process(wSManageReportEntriesProcessor);

		from("direct:WSCustPendingNotification").process(wSCustPendingNotificationProcessor);

		from("direct:WSDULAdjClaimAmtUserDecisionRoute").process(wSDULAdjClaimAmtUserDecisionProcessor);

		from("direct:WSDULRegClaimRoute").process(wSDULRegClaimDataProcessor);

		from("direct:WSDULActualAmountRoute").process(wSDULActualAmountProcessor);

		from("direct:WSDULUpdClaimRejectStatusRoute").process(wSDULUpdClaimRejectStatusProcessor);

		from("direct:WSDULActualClaimAmountRoute").process(wSDULActualClaimAmountProcessor);

		from("direct:WSDNULUpdClaimApproveRoute").process(wSDNULUpdClaimApproveStatusProcessor);

		from("direct:WSRegisterNotificationRoute").process(wSRegisterNotificationProcesssor);

		from("direct:WSDetermineAuthorityLimitRoute").process(wSDetermineAuthorityLimitProcessor);

		from("direct:WSUpdateClaimStatusRoute").process(wSUpdateClaimStatusProcessor);

		from("direct:WSGENUpdClaimRejectRoute").process(wSGENUpdClaimRejectProcessor);

		from("direct:WSGENUpdClaimApproveStatusRoute").process(wSGENUpdClaimApproveStatusProcesssor);

		from("direct:WSGENRegClaimRoute").process(wSGENRegClaimProcessor);

		from("direct:WSUpdateRequirementRoute").process(wSUpdateRequirementProcessor);

		from("direct:WSRegisterRequirementRoute").process(wSRegisterRequirementProcessor);

		from("direct:WSGenerateFilesRoute").process(wSGenerateFilesProcessor);

		from("direct:WSDocReIndexRoute").process(wSDocReIndexProcessor);

		from("direct:wSDNULDetCL06CompCodeRoute").process(wSDNULDetCL06CompCodeProcessor);

		from("direct:wSDULDetAppDeathCompCodeRoute").process(wSDULDetAppDeathCompCodeProcessor);

		from("direct:wSDULDetRegDeathCompCodeRoute").process(wSDULDetRegDeathCompCodeProcessor);

		from("direct:wSUpdateClaimStatusInClaimDataRoute").process(wSUpdateClaimStatusInClaimDataProcessor);

		from("direct:WSUpdateClaimSTPStatus").process(wSUpdateClaimSTPStatus);

		from("direct:WSUpdateReqtActivityIDRoute").process(wSUpdateReqActivityIDProcessor);

		from("direct:wSUpdateClaimDataGenericRoute").process(wSUpdateClaimDataGeneralProcessor);

		from("direct:WSGenericUpdateService").process(wSGenericUpdateServiceProcessor);

		from("direct:wSProcessRuleRecordRoute").process(wsProcessRuleRecordProcessor);

		from("direct:wSCaseDecisionTracker").process(wsCaseDecisionTrackerProcessor);
		// UI PROCESSOR*****************************************************

		from("direct:WSUIgetDiagCodeList").process(wSUIGetDiagCodeList);

		from("direct:WSUIgetDiagNameListRoute").process(wSUIGetDiagNameListProcessor);

		from("direct:WSUIgetDiagNameByCodeRoute").process(wSUIGetDiagNameByCodeProcessor);

		from("direct:WSUIgetDiagCodeByNameRoute").process(wSUIGetDiagCodeByNameProcessor);

		from("direct:WSUIgetProviderNameListRoute").process(wSUIGetProviderNameListProcessor);

		from("direct:WSUIgetFactoringHouseListRoute").process(wSUIGetFactoringHouseListProcessor);

		from("direct:WSUIgetBankCodeTypebyFactoringHouseRoute").process(wSUIGetBankCodeTypeByFactHouseProcessor);

		from("direct:WSUIgetBenefitCodeListRoute").process(wSUIGetBenefitCodeListProcessor);

		from("direct:WSUIgetBenefitNameListRoute").process(wSUIGetBenefitNameListProcessor);

		from("direct:WSUIgetBenefitDatabyNameRoute").process(wSUIGetBenefitDataByNameProcessor);

		from("direct:WSUIgetBenefitDatabyCodeRoute").process(wSUIGetBenefitDataByCodeProcessor);

		from("direct:WSUIgetPolicyDatabyPolicyNumberRoute").process(wSUIGetPolicyDatabyPolicyNumberProcessor);

		from("direct:WSUIgetAdjustmentCodeListRoute").process(wSUIGetAdjustmentCodeListProcessor);

		from("direct:WSUIGetRequirementListRoute").process(wSUIGetRequirementListProcessor);

		from("direct:WSUIGetUIClaimDataRoute").process(wSUIGetClaimData);

		from("direct:WSUISaveUIClaimDataRoute").process(wSUISaveClaimData);

		from("direct:WSUISaveWorksheetDataRoute").process(wSUISaveWorksheetDataProcessor);

		from("direct:WSUIGetWorksheetDataRoute").process(wSUIGetWorksheetDataProcessor);

		from("direct:WSFetchRecordEntries").process(wSFetchRecordEntries);

		// Portal PROCESSOR*****************************************************
		from("direct:WSPortalServiceCreateClaim").process(wSPortalServiceCreateClaim);

		from("direct:WSPortalServiceGetClaimDetails").process(wSPortalServiceGetClaimDetails);
		from("direct:WSPortalServiceGetClaimData").process(wSPortalServiceGetClaimData);
		/// userexit services*******************************88888888

		from("direct:WSupdateClaimUser").process(wSupdateClaimUser);
		from("direct:WSupdateClaimApprovalHAUser").process(wSupdateClaimApprovalHAUser);

		from("direct:WSupdateClaimApprovalHAUserName").process(wSupdateClaimApprovalHAUserName);

		from("direct:WSupdateClaimRejectHAUser").process(wSupdateClaimRejectHAUser);

		from("direct:WSupdateClaimRejectHAUserName").process(wSupdateClaimRejectHAUserName);

		from("direct:WSupdateClaimUWUser").process(wSupdateClaimUWUser);

		from("direct:WSupdateClaimHUWUser").process(wSupdateClaimHUWUser);

		from("direct:WSupdateClaimProcessorName").process(wSupdateClaimProcessorName);

		from("direct:WSupdateClaimProcessor").process(wSupdateClaimProcessor);

		from("direct:WSupdateClaimDecisionDate").process(wSupdateClaimDecisionDate);

		from("direct:WSupdateClaimProcessorEmailAddress").process(wSupdateClaimProcessorEmailAddress);

		from("direct:WSupdateClaimProcessorInvestigationEmailAddres").process(wSupdateClaimProcessorInvestigationEmailAddres);

		from("direct:WSupdateRemarks").process(wSupdateRemarks);

		from("direct:WSgetComponentCodeStatus").process(wSgetComponentCodeStatus);

		from("direct:WSGetProcessCaseSheetComments").process(wSGetProcessCaseSheetCommentsProcessor);

		from("direct:WSCheckUISaveFlagRoute").process(wSCheckUISaveFlagProcessor);

		from("direct:WSProcessInstanceMigrator").process(wSProcessInstanceMigrator);

		from("direct:WSupdateNotificationClaimDecisionDate").process(wSupdateNotificationClaimDecisionDate);

		from("direct:WSupdateClaimDecision").process(wSupdateClaimDecision);

		from("direct:WSGetAgentDetails").process(wSGetAgentDetails);

		from("direct:WSUpdateAgentDetails").process(wSUpdateAgentDetails);

		from("direct:WSUpdateClientDetails").process(wSUpdateClientDetails);

		from("direct:WSCreateCaptureRecordProcessor").process(wSCreateCaptureRecordProcessor);

		from("direct:WSGenerateSTPReportDataProcessor").process(wSGenerateSTPReportDataProcessor);

		from("direct:WSGenerateTATReportDataProcessor").process(wSGenerateTATReportDataProcessor);

		from("direct:WSReportRequestProcessor").process(wsReportRequestProcessor);

		from("direct:WSReportCheckStatusProcessor").process(wsReportCheckStatusProcessor);

		from("direct:WSUpdateDataProcessor").process(wSUpdateDataProcessor);

		from("direct:WSReportOutputXLSProcessor").process(wsReportOutputXLSProcessor);

		from("direct:WSValidatePendingRequirementsProcessor").process(wsValidatePendingRequirementsProcessor);

		from("direct:WSGetProductListProcessor").process(wsGetProductListProcessor);

		from("direct:WSGetReportNameListProcessor").process(wsGetReportNameListProcessor);

		from("direct:WSGetReportTypeListProcessor").process(wsGetReportTypeListProcessor);

		from("direct:WSGeneratePendingCaseReportProcessor").process(wsGeneratePendingCaseReportProcessor);

		from("direct:WSUpdateBusinessVarProcessor").process(wSUpdateBusinessVarProcessor);

		from("direct:WSDownloadGeneratedReportProcessor").process(wsDownloadGeneratedReportProcessor);

		from("direct:wsGetClaimVariableProcessor").process(wsGetClaimVariableProcessor);
		
		from("direct:WSCreateCaptureRecordPosProcessor").process(wsCreateCaptureRecordPosProcessor);
		
	}

}
