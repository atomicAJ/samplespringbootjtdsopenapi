package com.candelalabs.ws.rest;

import javax.validation.Valid;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.candelalabs.api.POSServicesApi;
import com.candelalabs.api.model.EPOSRequest;
import com.candelalabs.api.model.EPOSResponse;
import com.candelalabs.api.model.WSCommonDataOut;
import com.candelalabs.api.model.WSCommonDataOutWSResponse;
import com.candelalabs.api.model.WSCreateCaptureRecordPosDataIn;

@RestController
public class EPOSServicesResource implements POSServicesApi
{

	@Autowired
	ProducerTemplate producerTemplate;

	@Override public ResponseEntity<WSCommonDataOut>
	wSCreateCaptureRecordPos(@Valid @RequestBody WSCreateCaptureRecordPosDataIn	dataIn) { 
		WSCommonDataOutWSResponse wsResponse = producerTemplate.requestBody("direct:WSCreateCaptureRecordPosProcessor",	dataIn, WSCommonDataOutWSResponse.class); 
		WSCommonDataOut response = new WSCommonDataOut(); 
		response.setWsResponse(wsResponse); 
		return new ResponseEntity<WSCommonDataOut>(response, HttpStatus.OK); 
	}

	@Override
	public ResponseEntity<EPOSResponse> wSGetPosData(@Valid @RequestBody EPOSRequest request)
	{
		EPOSResponse response = producerTemplate.requestBody("direct:PolicyService", request, EPOSResponse.class);
		return new ResponseEntity<EPOSResponse>(response, HttpStatus.OK);
	}
}
