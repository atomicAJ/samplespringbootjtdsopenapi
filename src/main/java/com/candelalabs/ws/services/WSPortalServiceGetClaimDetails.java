package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataPortalOutput;
//import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.ClaimDetailsPortalInput;
import com.candelalabs.api.model.ClaimDetailsPortalOutput;
import com.candelalabs.api.model.ClaimDetailsPortalOutputClaimData;
import com.candelalabs.api.model.GetClaimDetailsList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.PortalsGetClaimDetailsData;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSPortalServiceGetClaimDetails implements Processor {

		private static final Log log = LogFactory.getLog("WSPortalServiceGetClaimDetails");
	
		@Autowired
		ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;
		/**
		 * 
		 */
		public WSPortalServiceGetClaimDetails() {
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 */
		
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			
		//	ClaimDetailsPortalOutput response = new ClaimDetailsPortalOutput();
			WSPortalServiceDataOut response =new WSPortalServiceDataOut();
			log.info("WSPortalServiceGetClaimDetails - Service is invoked");
		//	ClaimDetailsPortalOutputClaimData claimData = new ClaimDetailsPortalOutputClaimData();
			List<GetClaimDetailsList> claimData = new ArrayList<GetClaimDetailsList>();
			String strClaimStatus="";
			String strClaimTypeLA="";
			String strInsuredClientID="";
			String strPolicyNumber="";
			String strPortalClaimRequestNumber="";
			String strInsuredClientName="";
			String  strClaimProcessingReason ="";
			String strClaimSubStatus ="";
			try {
				  ClaimDetailsPortalInput dataIn = exchange.getIn().getBody(ClaimDetailsPortalInput.class);
				  String strOwnerClientID = "";
				  strOwnerClientID=dataIn.getStrOwnerClientID();
				  log.info("WSPortalServiceGetClaimDetails - strOwnerClientID is -"+ strOwnerClientID);
				  int recordCount = 0;
					recordCount = claimDetailsDao.countgetClaimDetails(strOwnerClientID);
					if (recordCount==0)
					{
						log.info("WSPortalServiceGetClaimDetails - recordCount is 0");
						GetClaimDetailsList e = new GetClaimDetailsList();
						e.setDtRegisterDate("");
						e.setStrClaimNumber("");
						e.setStrClaimStatus("");
						e.setStrClaimTypeLA("");
						e.setStrInsuredClientID("");
						e.setStrPolicyNumber("");
						e.setStrPortalClaimRequestNumber("");
						e.setStrInsuredClientName("");
						e.setStrClaimSubStatus("");
						e.setStrClaimProcessingReason("");
						//claimData.add(e);
						response.setClaimData(claimData);
						
						response.setClaimData(claimData);
						response.setWsExceptionMessage("");
						response.setWsProcessingStatus("1");
						response.setWsSuccessMessage("There are no Claim History for any policies belonging to Owner");
					}
					else
					{
						//List<WSPortalServiceDataOutClaimData> claimDatarcvd = null;
						List<PortalsGetClaimDetailsData> claimDatarcvd = null;
						log.info("before fetching getClaimDetails()");
						claimDatarcvd =claimDetailsDao.getClaimDetails(strOwnerClientID);
						log.info("after fetching getClaimDetails()");
						String strClaimNumber ="";
						if (claimDatarcvd!=null){
							log.info("claimDatarcvd!=null");
							for (PortalsGetClaimDetailsData temp : claimDatarcvd) 
							{
								GetClaimDetailsList e = new GetClaimDetailsList();
								
								/*log.info("in for loop temp.getDtRegisterDate() is -"+ temp.getRegisterDate());
								e.setDtRegisterDate(temp.getRegisterDate());*/
								Date regDate = null;
								String strDate="";
								log.info("in for loop temp.getDtRegisterDate() is -"+ temp.getRegisterDate());
								regDate = temp.getRegisterDate();
								/*log.info("in for loop temp.getDtRegisterDate() is -"+ Util.objToDate(temp.getRegisterDate(),"dd/mm/yyyy"));
								e.setDtRegisterDate(Util.objToDate(temp.getRegisterDate(),"dd/mm/yyyy").toString());*/
								if(regDate!=null  ){
								 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
								 strDate = formatter.format(regDate);
							   
							    }
								log.info("setDtRegisterDate is -"+ strDate);
							    e.setDtRegisterDate(strDate);
										
								log.info("in for loop temp.getStrClaimNumber() is -"+ temp.getClaimNumber());
								e.setStrClaimNumber(temp.getClaimNumber());
								strClaimNumber = temp.getClaimNumber();
								
								strClaimStatus=temp.getClaimStatus();
								if(strClaimStatus!=null && strClaimStatus!="")
									{e.setStrClaimStatus(strClaimStatus);}
								else
								{
									e.setStrClaimStatus("");
								}
								log.info("in for loop temp.getStrClaimStatus() is -"+ strClaimStatus);
								
								strClaimTypeLA=temp.getClaimTypeLA();
								if(strClaimTypeLA!=null && strClaimTypeLA!="")
								{
									e.setStrClaimTypeLA(strClaimTypeLA);
								}
								else
								{
									e.setStrClaimTypeLA("");
								}
								log.info("in for loop temp.getStrClaimTypeLA() is -"+ strClaimTypeLA);
								
								strInsuredClientID=temp.getInsuredClientID();
								if(strInsuredClientID!=null && strInsuredClientID!="")
								{
									e.setStrInsuredClientID(strInsuredClientID);
								}
								else
								{
									e.setStrInsuredClientID("");
								}
								log.info("in for loop temp.getStrInsuredClientID() is -"+ strInsuredClientID);
								//e.setStrInsuredClientID(temp.getInsuredClientID());
								
								strPolicyNumber= temp.getPolicyNumber();
								if(strPolicyNumber!=null && strPolicyNumber!="")
								{
									e.setStrPolicyNumber(strPolicyNumber);
								}
								else
								{
									e.setStrPolicyNumber("");
								}
								log.info("in for loop temp.getStrPolicyNumber() is -"+ strPolicyNumber);
								//e.setStrPolicyNumber(temp.getPolicyNumber());
								
								strPortalClaimRequestNumber=temp.getPortalClaimRequestNumber();
								if(strPortalClaimRequestNumber!=null && strPortalClaimRequestNumber!="")
								{
									e.setStrPortalClaimRequestNumber(strPortalClaimRequestNumber);
								}
								else
								{
									e.setStrPortalClaimRequestNumber("");
								}
								log.info("in for loop temp.getStrPortalClaimRequestNumbe() is -"+ strPortalClaimRequestNumber);
								//e.setStrPortalClaimRequestNumber(temp.getPortalClaimRequestNumber());
								
								strInsuredClientName=temp.getInsuredClientName();
								if(strInsuredClientName!=null && strInsuredClientName!="")
								{
									e.setStrInsuredClientName(strInsuredClientName);
								}
								else
								{
									e.setStrInsuredClientName("");
								}
								log.info("in for loop temp.getInsuredClientName() is -"+ strInsuredClientName);

							     strClaimSubStatus = temp.getClaimDecision();
								if(strClaimSubStatus!=null && strClaimSubStatus!="")
								{
									e.setStrClaimSubStatus(strClaimSubStatus);
								}
								else
								{
									e.setStrClaimSubStatus("");
								}
								log.info("in for loop temp.getClaimDecision() is -"+ strClaimSubStatus);
								
								if (strClaimStatus.equalsIgnoreCase("PENDING")){
									 strClaimProcessingReason = "Pending Requirement from customer";
								}
								else if (strClaimStatus .equalsIgnoreCase("COMPLETED"))
								{
								   //// strClaimSubStatus have the value from claim Decision so using strClaimSubStatus instead of strClaimDecision ////
								   if (strClaimSubStatus.equalsIgnoreCase("REJECT"))
								   {
									   ///<fetch data from ProcessCasesheet as per the same logic used in GenerateFile and Notification Modules>
									    UtilitiesServiceImpl UtilitiesServic = new UtilitiesServiceImpl();
									    String processCaseSheetComments = "";
										DsProcessCaseSheet df = null;
										df = UtilitiesServic.getCommentsByActivity("REJECTION HIGHER AUTHORITY", temp.getProcessInstanceID(), strClaimNumber);
										processCaseSheetComments = df.getComments();
										
								        if ((processCaseSheetComments==null)||processCaseSheetComments.trim().isEmpty()){
								        	df = UtilitiesServic.getCommentsByActivity("CLAIM PROCESSOR", temp.getProcessInstanceID(), strClaimNumber);
											processCaseSheetComments = df.getComments();
								        }
									   strClaimProcessingReason = processCaseSheetComments;
								   }
								   else
								   {
								      strClaimProcessingReason = "";
								   }
								}
								else
								   {
								      strClaimProcessingReason = "";
								   }
								e.setStrClaimProcessingReason(strClaimProcessingReason);
								
								claimData.add(e);
								
							}
							response.setClaimData(claimData);
							response.setWsExceptionMessage("");
							response.setWsProcessingStatus("1");
							response.setWsSuccessMessage("");
						}
						else
						{
							log.info("claimDatarcvd==null   -- Cannot retrieve the Claim History for the given Owner at the current moment");
							GetClaimDetailsList e = new GetClaimDetailsList();
							e.setDtRegisterDate("");
							e.setStrClaimNumber("");
							e.setStrClaimStatus("");
							e.setStrClaimTypeLA("");
							e.setStrInsuredClientID("");
							e.setStrPolicyNumber("");
							e.setStrPortalClaimRequestNumber("");
							e.setStrInsuredClientName("");
							//claimData.add(e);
							response.setClaimData(claimData);
							
							response.setClaimData(claimData);
							response.setWsExceptionMessage("");
							response.setWsProcessingStatus("2");
							response.setWsSuccessMessage("Cannot retrieve the Claim History for the given Owner at the current moment");
						}
					   
					}
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				 	response.setWsExceptionMessage( e.getMessage());
				    response.setWsProcessingStatus("2");
				    response.setWsSuccessMessage("");
				
			}
			log.info("Final Response " + response.toString());

			exchange.getOut().setBody(response);

		}

	}
