package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSCLUpdAppClaimStatusDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSCLUpdAppClaimStatusMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;

@Component
@PropertySource("classpath:application.properties")
public class WSCLUpdAppClaimStatusProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;
	private static final Log log = LogFactory.getLog("WSCLUpdAppClaimStatusProcessor");

	// TODO Auto-generated method stub
	@Value("${USRPRF}")
	private String USRPRF;

	public void process(Exchange exchange) throws Exception {

		log.debug("WSCLUpdAppClaimStatusProcessor - In Method - process");
		JsonResponse jsonoutData = new JsonResponse();

		try {

			WSCLUpdAppClaimStatusMessageModel boMessage = new WSCLUpdAppClaimStatusMessageModel();
			WSCLUpdAppClaimStatusDataIn jsonWSCLUpdAppClaimStatusDataIn = (exchange.getIn()
					.getBody(WSCLUpdAppClaimStatusDataIn.class));
			log.debug("Input payload= " + jsonWSCLUpdAppClaimStatusDataIn.toString());
			if (jsonWSCLUpdAppClaimStatusDataIn.getACTIVITYID() != null
					&& jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO() != null
					&& jsonWSCLUpdAppClaimStatusDataIn.getACTIVITYID() != ""
					&& jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO() != "") {

//				log.info("Get LaRequestTrackerModel table with claimNo= "
//						+ jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
//				LaRequestTrackerModel laRequestTrackerData = this.claimDetailsDao.getLaRequestTrackerByClaimNo(
//						jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO(), jsonWSCLUpdAppClaimStatusDataIn.getACTIVITYID(),
//						FWDConstants.BO_IDENTIFIER_Cashless_ApprovedHS);
//				log.info("Successfully Got LaRequestTrackerModel table ");
//
//				if (laRequestTrackerData.getBoidentifier() == null && laRequestTrackerData.getMsgtosend() == null
//						&& laRequestTrackerData.getMsgcreatets() == null
//						&& laRequestTrackerData.getFuturemesg() == null) {

					boMessage.setUSRPRF(this.USRPRF);
					log.debug("USRPRF= " + this.USRPRF);

					log.info("Get ClaimData Table with claimNO= " + jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					ClaimDataNewModel claimData = this.claimDetailsDao
							.getClaimDataNew(jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					log.info("Successfully Retrieved the ClaimData Table ");

					log.info("Get PolicyDetails Table with polNum= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully Retrieved the PolicyDetails Table ");

					if (!(Optional.ofNullable(claimData).isPresent()
							&& Optional.ofNullable(policyDetail).isPresent())) {
						throw new DAOException(
								"ClaimData is null with Claim NO= " + jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					}
					;

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					log.debug("CHDRCOY= " + policyDetail.getCOMPANY());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					log.debug("CHDRSEL=" + claimData.getPolicyNumber());
					boMessage.setCLAIMNO(jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					log.debug("CLAIMNO=" + jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					boMessage.setCRTABLE(claimData.getComponentCode());
					log.debug("CRTABLE=" + claimData.getComponentCode());

					log.info("Get ComponentDetatil Table  ");
					ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
							claimData.getComponentCode(), claimData.getInsuredClientID());
					log.info("Successfully Retrieved the ComponentDetatil Table ");

					if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
						throw new DAOException("componentDetailsBO is null with Claim NO= "
								+ jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					}
					;

					boMessage.setLIFE(componentDetailsBO.getLife());
					log.debug("LIFE=" + componentDetailsBO.getLife());
					boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
					log.debug("COVERAGE=" + componentDetailsBO.getCoverage());
					boMessage.setRIDER(componentDetailsBO.getRider());
					log.debug("RIDER=" + componentDetailsBO.getRider());
					boMessage.setRGPYNUM(claimData.getClaimNumberLA());
					log.debug("RGPYNUM=" + claimData.getClaimNumberLA());

					log.info("BOMESSAGE RESULT = " + boMessage.getBOMeesage());
					log.info("Inserting to LAREQUESTTRACKER table with claimNO= "
							+ jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
					int rowUpdated = this.claimDetailsDao.lARequestTracker_Insert(
							jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO(),
							jsonWSCLUpdAppClaimStatusDataIn.getACTIVITYID(), boMessage.getBOIDEN(),
							boMessage.getBOMeesage(), "0");
					log.info("Successfully Inserted to LAREQUESTTRACKER table with rowUpdated= " + rowUpdated);
					if (rowUpdated == 0)
						throw new DAOException("No Updated Row for LAREQUESTTRACKER table for claimNumber = "
								+ jsonWSCLUpdAppClaimStatusDataIn.getCLAIMNO());
				//}

				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");
			} else {
				throw new CamelException("Request Input message is empty");
			}

		} catch (Exception e) {
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e.getMessage());
			log.error("WSCLUpdAppClaimStatusProcessor - In Method - process - error message is" + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for(StackTraceElement tr :e.getStackTrace()) {
				log.error("\tat "+tr);
			}
		}

		String finalresult = jsonoutData.toString();
		log.info("WSCLUpdAppClaimStatusProcessor - In Method - process - response in String # " + finalresult);
		exchange.getOut().setBody(jsonoutData);

	}

}
