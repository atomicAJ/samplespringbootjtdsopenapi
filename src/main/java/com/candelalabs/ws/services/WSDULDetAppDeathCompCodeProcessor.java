/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DetAppDeathCompCodeDataIn;
import com.candelalabs.api.model.DetAppDeathCompCodeDataInWSInput;
import com.candelalabs.api.model.DetDeathCompCodeDataOut;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSDULDetAppDeathCompCodeProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSDULDetAppDeathCompCodeProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Value("${ApprovedCodes}")
	private String ApprovedCodes;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DetDeathCompCodeDataOut response = new DetDeathCompCodeDataOut();
		try {
			DetAppDeathCompCodeDataIn dataIn = exchange.getIn().getBody(DetAppDeathCompCodeDataIn.class);
			DetAppDeathCompCodeDataInWSInput wsInput = dataIn.getWsInput();
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());

			if (wsInput != null) {
				String[] approvedCode_arr = this.ApprovedCodes.trim().split(",");
				List<String> approvedCode_li = Arrays.asList(approvedCode_arr);

				log.info("Getting the claimData table with claimNumber: " + wsInput.getClaimNumber());
				ClaimDataNewModel claimData = new ClaimDataNewModel();
				claimData = this.claimDetailsDao.getClaimDataNew(wsInput.getClaimNumber());
				log.info("Successfully Getting the claimData table with claimNumber: " + wsInput.getClaimNumber());

				String compCode = claimData.getComponentCode().trim();
				response.setComponentCode(compCode);
				if (approvedCode_li.contains(compCode)) {
					response.setDecision("APPROVED DEATH CODES");
				} else {
					response.setDecision("REGISTERED DEATH CODES");
				}

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Successfully Compared the data");

			} else {
				throw new Exception("Request Input is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error " + e);
			// log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("ERROR Detail = " + e.getMessage());
		}

		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);

	}

}
