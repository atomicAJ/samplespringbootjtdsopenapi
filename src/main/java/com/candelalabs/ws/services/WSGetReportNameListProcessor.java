package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ReportNameListData;
import com.candelalabs.api.model.WSGetReportNameListDataIn;
import com.candelalabs.api.model.WSGetReportNameListDataOut;
import com.candelalabs.api.model.WSGetReportNameListDataOutWSResponse;
import com.candelalabs.ws.dao.ClaimDetailsDao;

@Component
public class WSGetReportNameListProcessor implements Processor {
	
	private static final Log log = LogFactory.getLog("WSGetReportNameListProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		WSGetReportNameListDataOutWSResponse wsResponse = new WSGetReportNameListDataOutWSResponse();
		WSGetReportNameListDataOut response = new WSGetReportNameListDataOut();
		
		try {
			WSGetReportNameListDataIn dataIn = (exchange.getIn().getBody(WSGetReportNameListDataIn.class));
			log.info(dataIn.toString());
			List<ReportNameListData> reportNameListData = new ArrayList<ReportNameListData>();
			
			reportNameListData = claimDetailsDao.getReportNameListByCategory(dataIn.getWsInput().getStrReportCategoryName());
			
			wsResponse.setWsProcessingStatus("1");
			wsResponse.setWsSuccessMessage("Success");
			wsResponse.setStrReportNameList(reportNameListData);
			response.setWsResponse(wsResponse);
		}catch(Exception e) {
			log.error("WSGetReportTypeListProcessor Error" + e);
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsExceptionMessage(e.getMessage());
			response.setWsResponse(wsResponse);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("Final Response " + response.toString());
		exchange.getOut().setBody(response);
		
	}

}
