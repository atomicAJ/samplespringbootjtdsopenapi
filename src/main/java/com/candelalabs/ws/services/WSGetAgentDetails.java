package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataPortalOutput;
import com.candelalabs.api.model.JsonWSGetAgentDetailsDataIn;
import com.candelalabs.api.model.JsonWSGetAgentDetailsDataOut;
//import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.dao.impl.ClaimDetailsDaoImpl;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;
import java.text.ParseException;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSGetAgentDetails implements Processor {

		private static final Log log = LogFactory.getLog("WSGetAgentDetails");
	
		@Autowired
	    ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;
		/**
		 * 
		 */
		public WSGetAgentDetails() {
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 */
		
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			log.info("WSGetAgentDetails - Service is invoked");
			JsonWSGetAgentDetailsDataOut response = new JsonWSGetAgentDetailsDataOut();
			JsonWSGetAgentDetailsDataIn dataIn = exchange.getIn().getBody(JsonWSGetAgentDetailsDataIn.class);
			
			
			 
			try {
				
				String strAgentEmailAddr = "";
				String polNo="";
				String claimNo="";
				int i =0;
				polNo=dataIn.getStrPolicyNumber();
				claimNo =dataIn.getStrClaimNumber();
				if(polNo!=""&& polNo!=null)
				{
					strAgentEmailAddr = odsDao.getAgentEmailAddr(polNo);
					i = claimDetailsDao.updateAgentEmailAddr(strAgentEmailAddr, claimNo);
				}
				
				response.setDecision("SUCCESS");
				response.setExceptionMessage("");
			    response.setWsProcessingStatus("1");
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
				int i = claimDetailsDao.updateNotificationFlag(e.getMessage(), dataIn.getStrClaimNumber());
			    response.setDecision("ERROR RESPONSE");;
			    response.setExceptionMessage("Exception in retrieving Agent Email Address");
			    response.setWsProcessingStatus("2");
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				
				
			}
			log.info("Final Response  is " + response.toString());
			log.info("response.getDecision() --"+ response.getDecision());
			log.info("response.getExceptionMessage() --"+ response.getExceptionMessage());
			log.info("response.getWsProcessingStatus() --"+ response.getWsProcessingStatus());
			exchange.getOut().setBody(response);

		}

	}
