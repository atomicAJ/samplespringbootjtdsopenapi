package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSUpdateClaimStatusDataIn;
//import org.apache.commons.logging.Log;
import com.candelalabs.ws.dao.ClaimDetailsDao;
//import com.candelalabs.ws.model.JsonWSUpdateRequirementdatain;
//import com.candelalabs.ws.model.JsonWSUpdateRequirementdataout;
//import com.candelalabs.ws.schema.stubs.WSUpdateRequirementRequestType;
//import com.candelalabs.ws.schema.stubs.WSUpdateRequirementResponseType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
@PropertySource("classpath:application.properties")
public class WSUpdateClaimStatusProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	private static Log log = LogFactory.getLog("WSUpdateClaimStatusProcessor");// .getLog(WSUpdateClaimStatusProcessor.class);

	// below working config
	// private static final Logger log =
	// LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());
	// @Autowired
	// private ClaimDetailsDao updatedRemarksInClaimData;

	int updateSuccess = 0;
	@Value("${CancelClaimRemark}")
	private String CancelClaimRemark;
	public void process(Exchange exchange) throws Exception {

		log.info(" WSUpdateClaimStatusProcessor -- testing log");
		log.info(" WSUpdateClaimStatusProcessor.class.getName() --" + WSUpdateClaimStatusProcessor.class.getName());
		log.info(" WSUpdateClaimStatusProcessor.class.getName() --" + WSUpdateClaimStatusProcessor.class.toString());
		System.out.println("WSUpdateClaimStatusProcessor -- testing console log");
		System.out.println(WSUpdateClaimStatusProcessor.class.getName());
		System.out.println(WSUpdateClaimStatusProcessor.class.toString());
		Gson gson = new GsonBuilder().serializeNulls().create();
		
		JsonResponse jsonoutData = new JsonResponse();
		try {

			WSUpdateClaimStatusDataIn jsonData = (exchange.getIn().getBody(WSUpdateClaimStatusDataIn.class));
			log.info("WSUpdateClaimStatusProcessor - In Method - process - Input Payload = " + jsonData.toString());
			
			if (Optional.ofNullable(jsonData).isPresent())
				claimDetailsDao.updatedRemarksInClaimData(jsonData, CancelClaimRemark);
			else
				throw new Exception("Input JSON is empty");

			jsonoutData.setWsProcessingStatus("1");
			jsonoutData.setWsSuccessMessage("Successfully updated the cancelRemark in ClaimData table");

			log.info("WSUpdateClaimStatusProcessor - In Method - process - getBody");

		} catch (Exception e) {
			log.error("ERROR Detail= " + e);
			for (StackTraceElement tr :e.getStackTrace()) {
				log.error("\tat "+tr);
			}
			jsonoutData.setWsExceptionMessage("ERROR =" + e.getMessage());
			jsonoutData.setWsProcessingStatus("2");

		}

		String json = gson.toJson(jsonoutData);
		log.info("JSON RESPONSE PAYLOAD = " + json);
		// WSUpdateClaimStatusResponseType requirementResponse = new
		// WSUpdateClaimStatusResponseType();
		// requirementResponse.setWSOutputObject(json);
		exchange.getOut().setBody(jsonoutData);

	}

}
