/**
 *
 */
package com.candelalabs.ws.services;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.SignalDetails;
import com.candelalabs.api.model.SignalDetailsVariables;
import com.candelalabs.api.model.WSRegisterNotificationDataIn;
import com.candelalabs.api.model.WSRegisterRequirementDataIn;
import com.candelalabs.api.model.WSUpdateRequirementDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSUpdateRequirementProcessor implements Processor {

    private static Log log = LogFactory.getLog("WSUpdateRequirementProcessor");

    @Autowired
    ClaimDetailsDao claimDetailsDao;

    @Autowired
    ProducerTemplate producerTemplate;

    @Override
    public void process(Exchange exchange) throws Exception {
        // TODO Auto-generated method stub
        JsonResponse response = new JsonResponse();
        response.setWsExceptionMessage("");
        response.setWsSuccessMessage("");

        try {
            WSUpdateRequirementDataIn dataIn = exchange.getIn().getBody(WSUpdateRequirementDataIn.class);
            log.info("JSON INPUT PAYLOAD = " + dataIn.toString());


            int claimDocCount = 0;
            ClaimDataNewModel claimData = new ClaimDataNewModel();
            if (dataIn.getTpaClaimNumber() != null && !dataIn.getTpaClaimNumber().trim().equalsIgnoreCase("")) {
                if (dataIn.getClaimNumber() == null || dataIn.getClaimNumber().trim().equalsIgnoreCase("")) {
                    dataIn.setClaimNumber("");
                }
                if (dataIn.getPolicyNumber() == null || dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) {
                    dataIn.setPolicyNumber("");
                    claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getTpaClaimNumber(),
                            dataIn.getDocId(), "TPAClaimNumber");
                } else {
                    claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getTpaClaimNumber(),
                            dataIn.getPolicyNumber(), dataIn.getDocId(), "TPAClaimNumber");
                }

                if (claimDocCount == 0) {
                    if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00025")) {
                        checkCOBLetterRequestNewRequest(claimData, dataIn);
                        registerRequirementClaimDocs3(dataIn);
                    } else if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00017")) {
                        registerRequirementClaimDocs31(dataIn);
                    } else {
                        registerRequirementClaimDocs3(dataIn);
                    }
                } else {
                    if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00025")) {
                        checkCOBLetterRequestDuplicate(claimData, dataIn);
                    }
                    registerRequirementClaimDocs4(dataIn);
                }
            } else if (dataIn.getBatchNumber() != null && !dataIn.getBatchNumber().trim().equalsIgnoreCase("")) {
                if (dataIn.getClaimNumber() == null || dataIn.getClaimNumber().trim().equalsIgnoreCase("")) {
                    String claimNumber = this.claimDetailsDao.getClaimNumberInClaimData("BatchNumber",
                            dataIn.getBatchNumber(), "String");
                    if (claimNumber != null && !claimNumber.trim().equalsIgnoreCase("")) {
                        dataIn.setClaimNumber(claimNumber);
                    } else {
                        dataIn.setClaimNumber("");
                    }

                }

                if (dataIn.getPolicyNumber() == null || dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) {
                    dataIn.setPolicyNumber("");
                    claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getBatchNumber(),
                            dataIn.getDocId(), "BatchNumber");
                } else {
                    claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getBatchNumber(),
                            dataIn.getPolicyNumber(), dataIn.getDocId(), "BatchNumber");
                }
                /////////////////////////////////////still to be planned
				/*if (claimDocCount == 0) {
					if ((dataIn.getPolicyNumber() == null || dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) &&
							(dataIn.getClaimNumber() != null || !dataIn.getClaimNumber().trim().equalsIgnoreCase("")) ) {
						dataIn.setPolicyNumber("");
						claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getClaimNumber(),
								dataIn.getDocId(), "ClaimNumber");
					} else {
						if (dataIn.getClaimNumber() != null || !dataIn.getClaimNumber().trim().equalsIgnoreCase("")) {
							claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getClaimNumber(),
									dataIn.getPolicyNumber(), dataIn.getDocId(), "ClaimNumber");
						}
					}
				}*/
                ////////////////////////////
                if (claimDocCount == 0) {
                    if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00025")) {
                        checkCOBLetterRequestNewRequest(claimData, dataIn);
                        registerRequirementClaimDocs5(dataIn);
                    } else if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00017")) {
                        registerRequirementClaimDocs51(dataIn);
                        resumeClaimProcessInvestigation(claimData, dataIn.getClaimNumber());
                    } else {
                        registerRequirementClaimDocs5(dataIn);
                    }

                } else {
                    if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00025")) {
                        checkCOBLetterRequestDuplicate(claimData, dataIn);
                    }
                    registerRequirementClaimDocs6(dataIn);
                }

            } else if (dataIn.getPortalRequestNumber() != null
                    && !dataIn.getPortalRequestNumber().trim().equalsIgnoreCase("")) {
                if (dataIn.getClaimNumber() == null || dataIn.getClaimNumber().trim().equalsIgnoreCase("")) {
                    String claimNumber = this.claimDetailsDao.getClaimNumberInClaimData("PortalClaimRequestNumber",
                            dataIn.getPortalRequestNumber(), "String");
                    if (claimNumber != null && !claimNumber.trim().equalsIgnoreCase("")) {
                        dataIn.setClaimNumber(claimNumber);
                    } else {
                        dataIn.setClaimNumber("");
                    }
                }

                if (dataIn.getPolicyNumber() == null || dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) {
                    dataIn.setPolicyNumber("");
                    claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getPortalRequestNumber(),
                            dataIn.getDocId(), "PortalClaimRequestNumber");
                } else {
                    claimDocCount = this.claimDetailsDao.getCountRequirementDetailTable(dataIn.getPortalRequestNumber(),
                            dataIn.getPolicyNumber(), dataIn.getDocId(), "PortalClaimRequestNumber");
                }

                if (claimDocCount == 0) {
                    if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00025")) {
                        checkCOBLetterRequestNewRequest(claimData, dataIn);
                        registerRequirementClaimDocs7(dataIn);
                        resumeClaimProcess(claimData, dataIn.getClaimNumber());
                    } else if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00017")) {
                        registerRequirementClaimDocs71(dataIn);
                        resumeClaimProcess(claimData, dataIn.getClaimNumber());
                    } else {
                        registerRequirementClaimDocs7(dataIn);
                        if (dataIn.getClaimNumber() != null && !dataIn.getClaimNumber().trim().equalsIgnoreCase("")) {
                            resumeClaimProcess(claimData, dataIn.getClaimNumber());
                        }
                    }

                } else {
                    if (dataIn.getDocId().trim().equalsIgnoreCase("bnbCLMDOC-00025")) {
                        checkCOBLetterRequestDuplicate(claimData, dataIn);
                    }
                    registerRequirementClaimDocs8(dataIn);
                    resumeClaimProcess(claimData, dataIn.getClaimNumber());

                }

            } else {
                if (dataIn.getClaimNumber() == null || dataIn.getClaimNumber().trim().equalsIgnoreCase("")) {
                    if (dataIn.getPolicyNumber() == null || dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) {
                        throw new Exception(
                                "POLICY NUMBER and CLAIMNUMBER OR either of TPAClaimNumber or BatchNumber or PortalRequestNumber is not available to process the recevied document");
                    } else {
                        List<String> claimNumberList = new ArrayList<String>();
                        claimNumberList = this.claimDetailsDao.getClaimDataNew(dataIn.getPolicyNumber(), "COMPLETE");
                        if (claimNumberList == null || claimNumberList.size() == 0) {
                            throw new Exception(
                                    "CLAIMNUMBER not available to process the recevied document with PolicyNumber and no Claim Process running for the given PolicyNumber = "
                                            + dataIn.getPolicyNumber());
                        } else if (claimNumberList.size() > 1) {
                            throw new Exception(
                                    "Multiple CLAIMNUMBER available for the received documents via Policy Number = "
                                            + dataIn.getPolicyNumber()
                                            + ". Cannot associate the received document to any running process instances");
                        } else {
                            int checkDocRegisterStatusCount = 0;
                            dataIn.setClaimNumber(claimNumberList.get(0));
                            checkDocRegisterStatusCount = this.claimDetailsDao.getCountRequirementDetailTable(
                                    dataIn.getClaimNumber(), dataIn.getPolicyNumber(), dataIn.getDocId(),
                                    "ClaimNumber");
                            if (checkDocRegisterStatusCount == 0) {
                                if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00025")) {
                                    checkCOBLetterRequestNewRequest(claimData, dataIn);
                                    registerRequirementClaimDocs1(dataIn);
                                    resumeClaimProcess(claimData, dataIn.getClaimNumber());
                                } else if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00017")) {
                                    registerRequirementClaimDocs11(dataIn);
                                    resumeClaimProcessInvestigation(claimData, dataIn.getClaimNumber());
                                } else {
                                    registerRequirementClaimDocs1(dataIn);
                                    resumeClaimProcess(claimData, dataIn.getClaimNumber());
                                }

                            } else {
                                if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00025")) {
                                    checkCOBLetterRequestDuplicate(claimData, dataIn);
                                }
                                registerRequirementClaimDocs2(dataIn);
                                resumeClaimProcess(claimData, dataIn.getClaimNumber());
                            }
                        }
                    }
                } else {
                    if (dataIn.getPolicyNumber() == null || dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) {
                        ClaimDataNewModel claimDataNew = new ClaimDataNewModel();
                        claimDataNew = this.claimDetailsDao.getClaimData(dataIn.getClaimNumber());
                        if (claimDataNew == null) {
                            throw new Exception(
                                    "Claim Record not found for the given Claim Number based on the recevied document");
                        } else {
                            int checkDocIdRegisterStatusCount = 0;
                            dataIn.setPolicyNumber(claimDataNew.getPolicyNumber());
                            if (dataIn.getPolicyNumber() == null) {
                                dataIn.setPolicyNumber("");
                            }

                            if (dataIn.getPolicyNumber().trim().equalsIgnoreCase("")) {
                                checkDocIdRegisterStatusCount = this.claimDetailsDao.getCountRequirementDetailTable(
                                        dataIn.getClaimNumber(), dataIn.getDocId(), "ClaimNumber");
                            } else {
                                checkDocIdRegisterStatusCount = this.claimDetailsDao.getCountRequirementDetailTable(
                                        dataIn.getClaimNumber(), dataIn.getPolicyNumber(), dataIn.getDocId(),
                                        "ClaimNumber");

                            }
                            if (checkDocIdRegisterStatusCount == 0) {
                                if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00025")) {
                                    checkCOBLetterRequestNewRequest(claimData, dataIn);
                                    registerRequirementClaimDocs1(dataIn);
                                    resumeClaimProcess(claimData, dataIn.getClaimNumber());
                                } else if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00017")) {
                                    registerRequirementClaimDocs11(dataIn);
                                    resumeClaimProcessInvestigation(claimData, dataIn.getClaimNumber());
                                } else {
                                    registerRequirementClaimDocs1(dataIn);
                                    resumeClaimProcess(claimData, dataIn.getClaimNumber());
                                }

                            } else {
                                if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00025")) {
                                    checkCOBLetterRequestDuplicate(claimData, dataIn);
                                }
                                registerRequirementClaimDocs2(dataIn);
                                resumeClaimProcess(claimData, dataIn.getClaimNumber());
                            }

                        }
                    } else {

                        int checkDocIdRegisterStatusCount = 0;

                        checkDocIdRegisterStatusCount = this.claimDetailsDao.getCountRequirementDetailTable(
                                dataIn.getClaimNumber(), dataIn.getPolicyNumber(), dataIn.getDocId(), "ClaimNumber");

                        if (checkDocIdRegisterStatusCount == 0) {
                            if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00025")) {
                                checkCOBLetterRequestNewRequest(claimData, dataIn);
                                registerRequirementClaimDocs1(dataIn);
                                resumeClaimProcess(claimData, dataIn.getClaimNumber());
                            } else if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00017")) {
                                registerRequirementClaimDocs11(dataIn);
                                resumeClaimProcessInvestigation(claimData, dataIn.getClaimNumber());
                            } else {
                                registerRequirementClaimDocs1(dataIn);
                                resumeClaimProcess(claimData, dataIn.getClaimNumber());
                            }

                        } else {
                            if (dataIn.getDocId().equalsIgnoreCase("bnbCLMDOC-00025")) {
                                checkCOBLetterRequestDuplicate(claimData, dataIn);
                            }
                            registerRequirementClaimDocs2(dataIn);
                            resumeClaimProcess(claimData, dataIn.getClaimNumber());
                        }

                    }
                }
            }

            response.setWsProcessingStatus("1");
            response.setWsSuccessMessage("Sucessfully process the updated	 requirement steps for Claim Number ="
                    + dataIn.getClaimNumber() + " and / or PolicyNumber =" + dataIn.getPolicyNumber());

        } catch (Exception e) {
            // TODO: handle exception
            log.error("Error" + e);
            for (StackTraceElement tr : e.getStackTrace()) {
                log.error("\tat " + tr);
            }
            response.setWsProcessingStatus("2");
            response.setWsExceptionMessage("ERROR DETAIL = " + e);
        }

        log.info("JSON RESPONSE PAYLOAD = " + response.toString());
        exchange.getOut().setBody(response);
    }

    private void checkCOBLetterRequestNewRequest(ClaimDataNewModel claimData, WSUpdateRequirementDataIn dataIn)
            throws Exception {
        int countcob = this.claimDetailsDao.countCOBRequestByClaimNumber(dataIn.getClaimNumber(),
                dataIn.getPolicyNumber());
        if (countcob == 0) {
            claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getClaimNumber());
            log.info("Insert COBREQUEST");
            int processed = 0;
            String processedRemarks = null;
            Timestamp ts = null;
            if (claimData != null && claimData.getClaimDecision() != null) {
                if (claimData.getClaimDecision().equalsIgnoreCase("REJECT")) {
                    processed = 3;
                    processedRemarks = "Claim Decision is Rejected or Claim is not of type 'Hospital Surgical'. System will not generate the COB Letter for customer";
                    ts = new Timestamp(System.currentTimeMillis());
                } else {
                    processed = 0;
                }
            } else {
                processed = 0;
            }
            int rowUpdated = this.claimDetailsDao.insertCOBREquest(dataIn.getClaimNumber(), dataIn.getPolicyNumber(),
                    processed, ts, processedRemarks);
            if (rowUpdated == 0) {
                throw new DAOException("COBREquest Table fail to insert data with CLaimNumber = "
                        + dataIn.getClaimNumber() + " and PolicyNumber = " + dataIn.getPolicyNumber());
            }
            log.info("Successfully Inserted  the COBREQUEST");

            if (claimData != null && claimData.getClaimDecision() != null) {
                if (claimData.getClaimDecision().equalsIgnoreCase("APPROVE")
                        || claimData.getClaimDecision().equalsIgnoreCase("REJECT")) {
                    WSRegisterNotificationDataIn dataInNotif = new WSRegisterNotificationDataIn();
                    dataInNotif.setCLAIMNUMBER(dataIn.getClaimNumber());
                    dataInNotif.setPROCESSNAME("Requirement Received");
                    dataInNotif.setDECISION("COBREQUESTPOSTPROCESS");
                    dataInNotif.setPROCESSACTIVITYNAME("Process Documents");
                    dataInNotif.setPROCESSCATEGORY(dataIn.getDocId().contains("bnbCLMDOC") ? "Claim" : "Others");
                    dataInNotif.APPLICATIONNUMBER("");
                    dataInNotif.POLICYNUMBER(dataIn.getPolicyNumber());
                    log.info("Invoking WSRegisterNotification with dataIn = " + dataInNotif.toString());
                    JsonResponse responseNotif = producerTemplate.requestBody("direct:WSRegisterNotificationRoute",
                            dataInNotif, JsonResponse.class);
                    if (responseNotif.getWsProcessingStatus().trim().equalsIgnoreCase("2")) {
                        throw new CamelException("Error when invoking WSRegisterNotification with error detail = "
                                + responseNotif.getWsExceptionMessage());
                    }
                    log.info("Successfully Invoked the WSRegisterNotification");
                }
            }
        }
    }

    private void checkCOBLetterRequestDuplicate(ClaimDataNewModel claimData, WSUpdateRequirementDataIn dataIn)
            throws Exception {
        int countcob = this.claimDetailsDao.countCOBRequestByClaimNumber(dataIn.getClaimNumber(),
                dataIn.getPolicyNumber());
        if (countcob > 0) {
            WSRegisterNotificationDataIn dataInNotif = new WSRegisterNotificationDataIn();

            dataInNotif.setCLAIMNUMBER(dataIn.getClaimNumber());
            dataInNotif.setPROCESSNAME("Requirement Received");
            dataInNotif.setDECISION("DUPLICATECOB");
            dataInNotif.setPROCESSACTIVITYNAME("Process Documents");
            dataInNotif.setPROCESSCATEGORY(dataIn.getDocId().contains("bnbCLMDOC") ? "Claim" : "Others");
            dataInNotif.setAPPLICATIONNUMBER("");
            dataInNotif.setPOLICYNUMBER(dataIn.getPolicyNumber());
            log.info("Invoking WSRegisterNotification with dataIn = " + dataInNotif.toString());
            JsonResponse responseNotif = producerTemplate.requestBody("direct:WSRegisterNotificationRoute", dataInNotif,
                    JsonResponse.class);

            if (responseNotif.getWsProcessingStatus().trim().equalsIgnoreCase("2")) {
                throw new CamelException("Error when invoking WSRegisterNotification with error detail = "
                        + responseNotif.getWsExceptionMessage());
            }
            log.info("Successfully Invoked the WSRegisterNotification");
            log.info("Insert COBREQUEST");
            int rowUpdated = this.claimDetailsDao.insertCOBREquest(dataIn.getClaimNumber(), dataIn.getPolicyNumber(), 2,
                    new Timestamp(System.currentTimeMillis()),
                    "Duplicate COB Request Received for the given PolicyNumber and Claim Number");
            if (rowUpdated == 0) {
                throw new DAOException("COBREquest Table fail to insert data with CLaimNumber = "
                        + dataIn.getClaimNumber() + " and PolicyNumber = " + dataIn.getPolicyNumber());
            }
            log.info("Successfully Inserted  the COBREQUEST");
        } else {
            checkCOBLetterRequestNewRequest(claimData, dataIn);
        }
    }

    private void registerRequirementClaimDocs1(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setREQTREQACTIVITYID("-");
        dataRegister.setCUSTOMERINITIATEDREQ("Y");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs11(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("N");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs2(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(null);
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("2");
        dataRegister.setCUSTOMERINITIATEDREQ("");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs3(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER(dataIn.getTpaClaimNumber());
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("Y");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs31(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER(dataIn.getTpaClaimNumber());
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("N");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs4(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER(dataIn.getTpaClaimNumber());
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(null);
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("24");
        dataRegister.setCUSTOMERINITIATEDREQ("");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs5(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER(dataIn.getBatchNumber());
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("Y");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");

    }

    private void registerRequirementClaimDocs51(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER(dataIn.getBatchNumber());
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("N");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs6(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER(dataIn.getBatchNumber());
        dataRegister.setPORTALREQUESTNUMBER("");
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(null);
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("25");
        dataRegister.setCUSTOMERINITIATEDREQ("");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");

    }

    private void registerRequirementClaimDocs7(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER(dataIn.getPortalRequestNumber());
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("Y");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs71(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER(dataIn.getPortalRequestNumber());
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(OffsetDateTime.now());
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("1");
        dataRegister.setCUSTOMERINITIATEDREQ("N");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void registerRequirementClaimDocs8(WSUpdateRequirementDataIn dataIn) throws CamelException {
        WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
        dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
        dataRegister.setAPPNUMBER("");
        dataRegister.setPOLICYNUMBER(dataIn.getPolicyNumber());
        dataRegister.setTPACLAIMNUMBER("");
        dataRegister.setBATCHNUMBER("");
        dataRegister.setPORTALREQUESTNUMBER(dataIn.getPortalRequestNumber());
        dataRegister.setPOSREQUESTNUMBER("");
        dataRegister.setREQDOCID(dataIn.getDocId());
        dataRegister.setREQTTEXT("");
        dataRegister.setREQTREQTS(null);
        dataRegister.setREQTRCDTS(OffsetDateTime.now());
        dataRegister.setREQTSUBCATEGORY("");
        dataRegister.setREQTNOTIFYFLAG("26");
        dataRegister.setCUSTOMERINITIATEDREQ("");
        dataRegister.setREQTREQACTIVITYID("-");

        log.info("invoking the WSRegisterRequirement with dataIn = " + dataRegister.toString());
        JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
                JsonResponse.class);
        if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
            throw new CamelException(
                    "Invoking WSRegisterRequirement is failing, with reason = " + responseReg.getWsExceptionMessage());
        }
        ;
        log.info("Successfully invoked the WSRegisterRequirement");
    }

    private void resumeClaimProcess(ClaimDataNewModel claimData, String claimNumber) throws Exception {
        if (claimNumber != null && !claimNumber.trim().equalsIgnoreCase("")) {
            if (this.claimDetailsDao.getCountActIdPendingDoc(claimNumber) > 0) {
                claimData = this.claimDetailsDao.getClaimDataNew(claimNumber);
			/*if (claimData.getClaimStatus() != null && !claimData.getClaimStatus().trim().equalsIgnoreCase("")
					&& claimData.getClaimStatus().trim().equalsIgnoreCase("PENDING")) {*/
                String recActId =this.claimDetailsDao.getReceiveActIdPendingDoc(claimNumber);

                SignalDetails signalDetails = new SignalDetails();
                signalDetails.setActivityId(recActId);
                signalDetails.setProcessInstanceId(claimData.getProcessInstanceID());

                List<SignalDetailsVariables> sigVariables_li = new ArrayList<SignalDetailsVariables>();
                SignalDetailsVariables sigVariables = new SignalDetailsVariables();
                sigVariables.setName("DECISION");
                sigVariables.setValue("DOCUMENT RECEIVED");
                sigVariables_li.add(sigVariables);
                signalDetails.setVariables(sigVariables_li);
                if (null != claimData.getClaimStatus() && claimData.getClaimStatus().equalsIgnoreCase("PENDING")) {
                    log.info("Updating claimStatus to Running with claimNUmber= " + claimNumber);
                    int o = this.claimDetailsDao.updateClaimData(claimNumber, "RUNNING", null);
                    if (o == 0) {
                        throw new DAOException("ClaimData is not updated with claimNo = " + claimNumber);
                    }
                    log.info("Successfully Updated ClaimData with claimNo = " + claimNumber + " to Running the claimStatus");
                }

                log.info("invoking the WSSIgnalReceiveTask with dataIn = " + signalDetails.toString());
                JsonResponse signal = producerTemplate.requestBody("direct:SignalTaskRoute", signalDetails,
                        JsonResponse.class);

                if (signal.getWsProcessingStatus().equalsIgnoreCase("2")) {
                    log.info("Updating claimStatus to Running with claimNUmber= " + claimNumber);
                    int p = this.claimDetailsDao.updateClaimData(claimNumber, "PENDING", null);

                    throw new CamelException("Invoking WSSignalReceiveTask is failing, with reason = "
                            + signal.getWsExceptionMessage() + "  AND INPUT = " + signalDetails.toString());
                }
                ;
                log.info("Successfully invoked the WSSIgnalReceiveTask");
                if (this.claimDetailsDao.updateCaseReceiveActTracker(claimNumber,recActId,"PENDING","COMPLETE") == 0){
                    throw new Exception("Failed to update CaseReceiveActivityTracker with claimNumber:"+claimNumber);
                }
            } else {
                log.info("Not found any Pending Document to be resumed");
            }

            //}
        } else {
            log.info("ClaimNumber is empty or Null, so no ResumeClaimProcess is being done");
        }
    }

    private void resumeClaimProcessInvestigation(ClaimDataNewModel claimData, String claimNumber) throws Exception {
        if (claimNumber != null && !claimNumber.trim().equalsIgnoreCase("")) {
            if (this.claimDetailsDao.getCountReceiveActIdInvestDoc(claimNumber) > 0) {
                claimData = this.claimDetailsDao.getClaimDataNew(claimNumber);
                String recActId = this.claimDetailsDao.getReceiveActIdInvestDoc(claimNumber);

                SignalDetails signalDetails = new SignalDetails();
                signalDetails.setActivityId(recActId);
                signalDetails.setProcessInstanceId(claimData.getProcessInstanceID());

                List<SignalDetailsVariables> sigVariables_li = new ArrayList<SignalDetailsVariables>();
                SignalDetailsVariables sigVariables = new SignalDetailsVariables();
                sigVariables.setName("DECISION");
                sigVariables.setValue("DOCUMENT RECEIVED");
                sigVariables_li.add(sigVariables);
                signalDetails.setVariables(sigVariables_li);

                if (null != claimData.getClaimStatus() && claimData.getClaimStatus().equalsIgnoreCase("PENDING")) {
                    log.info("Updating claimStatus to Running with claimNUmber= " + claimNumber);
                    int o = this.claimDetailsDao.updateClaimData(claimNumber, "RUNNING", null);
                    if (o == 0) {
                        throw new DAOException("ClaimData is not updated with claimNo = " + claimNumber);
                    }
                    log.info("Successfully Updated ClaimData with claimNo = " + claimNumber + " to Running the claimStatus");
                }

                log.info("invoking the WSSIgnalReceiveTask with dataIn = " + signalDetails.toString());
                JsonResponse signal = producerTemplate.requestBody("direct:SignalTaskRoute", signalDetails, JsonResponse.class);

                if (signal.getWsProcessingStatus().equalsIgnoreCase("2")) {
                    log.info("Updating claimStatus to Running with claimNUmber= " + claimNumber);
                    int p = this.claimDetailsDao.updateClaimData(claimNumber, "PENDING", null);

                    throw new CamelException("Invoking WSSignalReceiveTask is failing, with reason = "
                            + signal.getWsExceptionMessage() + "  AND INPUT = " + signalDetails.toString());
                }
                ;
                log.info("Successfully invoked the WSSIgnalReceiveTask");

                if (this.claimDetailsDao.updateCaseReceiveActTracker(claimNumber,recActId,"PENDING","COMPLETE") == 0){
                    throw new Exception("Failed to update CaseReceiveActivityTracker with claimNumber:"+claimNumber);
                }
            } else {
                log.info("Not found any Pending Investigation Report activity to be resumed");
            }

        } else {
            log.info("ClaimNumber is empty or Null, so no ResumeClaimProcessInvestigation is being done");
        }
    }
}
