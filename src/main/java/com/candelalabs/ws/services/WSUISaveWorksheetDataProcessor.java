/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataUIClaimDetailsWSResponse;
import com.candelalabs.api.model.ClaimWorksheetUIData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheet;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClaimHistory;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClients;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClientsOwnerData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetComments;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetComponentsListData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.model.tables.UWCommentsModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSUISaveWorksheetDataProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSUISaveWorksheetDataProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		ClaimDataUIClaimDetailsWSResponse wsresponse = new ClaimDataUIClaimDetailsWSResponse();
		ClaimWorksheetUIData response = exchange.getIn().getBody(ClaimWorksheetUIData.class);
		log.info("JSON INPUT PAYLOAD = " + response.toString());
		try {
			ClaimWorksheetUIDataClaimWorksheet resClaimWorksheet = response.getClaimWorksheet();
//			List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> respCompList_li = resClaimWorksheet
//					.getComponentsListData();
//			List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> respPolRelatedDetails_li = resClaimWorksheet
//					.getPolicyRelatedDetails();
//			List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> respClaimHistory = resClaimWorksheet.getClaimHistory();
//
//			ClaimWorksheetUIDataClaimWorksheetClients respClients = resClaimWorksheet.getClients();
//			List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsOwnerData_li = respClients
//					.getOwnerData();
//			List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsLifeInsured_li = respClients
//					.getLifeInsuredData();
//			List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsBenData_li = respClients
//					.getBeneficiaryData();

			ClaimWorksheetUIDataClaimWorksheetComments respComments = resClaimWorksheet.getComments();
			List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsNBUW_li = respComments
					.getNbUWComments();
			List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsPSUW_li = respComments
					.getPsUWComments();
			List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsCLUW_li = respComments
					.getClUWComments();

			UWCommentsModel uwModel = new UWCommentsModel();

			if (resClaimWorksheet.getStrProcessType() != null && resClaimWorksheet.getStrProcessType().trim() != "") {
				log.info("Process Type = " + resClaimWorksheet.getStrProcessType());
				if (resClaimWorksheet.getStrProcessType().trim().equalsIgnoreCase("CL")) {
					if (respCommentsCLUW_li != null && respCommentsCLUW_li.size() > 0) {
						log.info("Size of respCommentsCLUW_li = " + respCommentsCLUW_li.size());
						for (ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments uwIn : respCommentsCLUW_li) {
							if (uwIn.getStrExistingRecord() != null && uwIn.getStrExistingRecord().trim() != "") {
								if (uwIn.getStrExistingRecord().trim().equalsIgnoreCase("N")) {
									uwModel.setClaimNumber(resClaimWorksheet.getStrClaimNumber());
									uwModel.setPolicyNumber(resClaimWorksheet.getStrPolicyNumber());
									uwModel.setUWComment(uwIn.getStrComment());
									uwModel.setUWCommentBy(uwIn.getStrCommentBY());
									uwModel.setSourceOfComment(resClaimWorksheet.getStrProcessType());
									this.claimDetailsDao.insertUWComments(uwModel);
								}
							}
						}
					}

				} else if (resClaimWorksheet.getStrProcessType().trim().equalsIgnoreCase("PS")) {
					if (respCommentsPSUW_li != null && respCommentsPSUW_li.size() > 0) {
						log.info("Size of respCommentsPSUW_li = " + respCommentsPSUW_li.size());
						for (ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments uwIn : respCommentsPSUW_li) {
							if (uwIn.getStrExistingRecord() != null && uwIn.getStrExistingRecord().trim() != "") {
								if (uwIn.getStrExistingRecord().trim().equalsIgnoreCase("N")) {
									uwModel.setPOSRequestNumber(resClaimWorksheet.getStrPOSRequestNumber());
									uwModel.setPolicyNumber(resClaimWorksheet.getStrPolicyNumber());
									uwModel.setUWComment(uwIn.getStrComment());
									uwModel.setUWCommentBy(uwIn.getStrCommentBY());
									uwModel.setSourceOfComment(resClaimWorksheet.getStrProcessType());
									this.claimDetailsDao.insertUWComments(uwModel);
								}
							}
						}
					}

				} else if (resClaimWorksheet.getStrProcessType().trim().equalsIgnoreCase("NB")) {
					if (respCommentsNBUW_li != null && respCommentsNBUW_li.size() > 0) {
						log.info("Size of respCommentsNBUW_li = " + respCommentsNBUW_li.size());
						for (ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments uwIn : respCommentsNBUW_li) {
							if (uwIn.getStrExistingRecord() != null && uwIn.getStrExistingRecord().trim() != "") {
								if (uwIn.getStrExistingRecord().trim().equalsIgnoreCase("N")) {
									uwModel.setAppNumber(resClaimWorksheet.getStrApplicationNumber());
									uwModel.setPolicyNumber(resClaimWorksheet.getStrPolicyNumber());
									uwModel.setUWComment(uwIn.getStrComment());
									uwModel.setUWCommentBy(uwIn.getStrCommentBY());
									uwModel.setSourceOfComment(resClaimWorksheet.getStrProcessType());
									this.claimDetailsDao.insertUWComments(uwModel);
								}
							}
						}
					}
				} else {
					throw new Exception("Unknown StrProcessType given = " + resClaimWorksheet.getStrProcessType());
				}

			} else {
				throw new Exception("StrProcessType is Empty Cannot be processed");
			}
			log.info("Successfully processed for the WSSaveWorksheetUI");
			wsresponse.setWsProcessingStatus("1");
			wsresponse.setWsSuccessMessage("Successfully");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERRROR Message = " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL = " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("WSRESPONSE PAYLOAD = " + wsresponse.toString());
		response.setWsResponse(wsresponse);
		exchange.getOut().setBody(response);

	}

}
