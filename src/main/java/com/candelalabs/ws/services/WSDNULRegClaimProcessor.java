package com.candelalabs.ws.services;



import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDNULRegClaimDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSDNULRegClaimMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.PolicyDetails;

import com.candelalabs.ws.util.Util;

@Component
@PropertySource("classpath:application.properties")
public class WSDNULRegClaimProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	private static Log log = LogFactory.getLog("WSDNULRegClaimProcessor");
	// TODO Auto-generated method stub
	@Value("${USRPRF}")
	private String USRPRF;

	public void process(Exchange exchange) throws Exception {

		log.debug("WSDNULRegClaimProcessor - In Method - process");
		JsonResponse jsonoutData = new JsonResponse();

		try {

			WSDNULRegClaimMessageModel boMessage = new WSDNULRegClaimMessageModel();
			WSDNULRegClaimDataIn jsonWSDNULRegClaimDataInDataIn = (exchange.getIn()
					.getBody(WSDNULRegClaimDataIn.class));
			log.info("Input Payload =" + jsonWSDNULRegClaimDataInDataIn.toString());
			if (jsonWSDNULRegClaimDataInDataIn.getACTIVITYID() != null
					&& jsonWSDNULRegClaimDataInDataIn.getCLAIMNO() != null
					&& jsonWSDNULRegClaimDataInDataIn.getACTIVITYID().trim() != ""
					&& jsonWSDNULRegClaimDataInDataIn.getCLAIMNO().trim() != "") {

//				log.info(
//						"Get LaRequestTrackerModel table with claimNo= " + jsonWSDNULRegClaimDataInDataIn.getCLAIMNO());
//				LaRequestTrackerModel laRequestTrackerData = this.claimDetailsDao.getLaRequestTrackerByClaimNo(
//						jsonWSDNULRegClaimDataInDataIn.getCLAIMNO(), jsonWSDNULRegClaimDataInDataIn.getACTIVITYID(),
//						FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimRegister);
//				log.info("Successfully Got LaRequestTrackerModel table ");
//
//				if (laRequestTrackerData.getBoidentifier() == null && laRequestTrackerData.getMsgtosend() == null
//						&& laRequestTrackerData.getMsgcreatets() == null
//						&& laRequestTrackerData.getFuturemesg() == null) {

					boMessage.setUSRPRF(this.USRPRF);

					log.info("Get ClaimData table with claimNo= " + jsonWSDNULRegClaimDataInDataIn.getCLAIMNO());
					ClaimDataNewModel claimData = this.claimDetailsDao
							.getClaimDataNew(jsonWSDNULRegClaimDataInDataIn.getCLAIMNO());
					log.info("Successfully retrieved ClaimData table");

					log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully retrieved PolicyDetail table");

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					log.debug("CHDRSEL=" + claimData.getPolicyNumber());

					boMessage.setDTOFDEATH(Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));
					log.debug("DTOFDEATH ="+boMessage.getDTOFDEATH());
					boMessage.setZCATCLMBEN(claimData.getBenefitCategory());
					log.debug("ZCATCLMBEN = "+boMessage.getZCATCLMBEN());

					boMessage.setCLAIMNO(jsonWSDNULRegClaimDataInDataIn.getCLAIMNO());
					log.debug("CLAIMNO = "+boMessage.getCLAIMNO());

					boMessage.setOTHERADJST(claimData.getOtherAdjustments());
					log.debug("OTHERADJST = "+ boMessage.getOTHERADJST());
					boMessage.setREASONCD(claimData.getAdjustmentCode());
					log.debug("REASONCD ="+boMessage.getREASONCD());
					boMessage.setZDIAGCDE(claimData.getDiagnosisCode());
					log.debug("ZDIAGCDE = "+boMessage.getZDIAGCDE());

					boMessage.setCLNTNUM(claimData.getInsuredClientID());
					log.debug("CLNTNUM= "+boMessage.getCLNTNUM());

					log.info("BOMESSAGE RESULT = " + boMessage.getBOMessage());
					log.info("Inserting to LAREQUESTTRACKER table with claimNO= "
							+ jsonWSDNULRegClaimDataInDataIn.getCLAIMNO());
					int i = this.claimDetailsDao.lARequestTracker_Insert(jsonWSDNULRegClaimDataInDataIn.getCLAIMNO(),
							jsonWSDNULRegClaimDataInDataIn.getACTIVITYID(), boMessage.getBOIDEN(),
							boMessage.getBOMessage(), "0");
					if (i == 0) {
						throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
					}
					
					log.info("Successfully inserted to the LAREQUESTTRACKER table");
				//}
				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");

			} else {
				throw new CamelException("Request Input message is empty");
			}

		} catch (Exception e) {
			log.error("WSDNULRegClaimProcessor - In Method - process - error message is" + e);
			for(StackTraceElement tr :e.getStackTrace()) {
				log.error("\tat "+tr);
			}
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e.getMessage());
		}

		String finalresult = jsonoutData.toString();
		log.info("WSDNULRegClaimProcessor - In Method - process - response in String # " + finalresult);
		exchange.getOut().setBody(jsonoutData);

	}

}
