package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataPortalOutput;
import com.candelalabs.api.model.JsonWSGetAgentDetailsDataIn;
import com.candelalabs.api.model.JsonWSGetAgentDetailsDataOut;
//import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.dao.impl.ClaimDetailsDaoImpl;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;
import java.text.ParseException;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSUpdateAgentDetails implements Processor {

		private static final Log log = LogFactory.getLog("WSUpdateAgentDetails");
	
		@Autowired
	    ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;
		/**
		 * 
		 */
		public WSUpdateAgentDetails() {
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 */
		
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			log.info("WSUpdateAgentDetails - Service is invoked");
			JsonWSGetAgentDetailsDataOut response = new JsonWSGetAgentDetailsDataOut();
			JsonWSGetAgentDetailsDataIn dataIn = exchange.getIn().getBody(JsonWSGetAgentDetailsDataIn.class);
			
			
			 
			try {
				
				String strAgentEmailAddr = "";
				String strSDEmailAddr="";
				String strNSDEmailAddr="";
				String polNo="";
				String claimNo="";
				String strEmailCategory="";
				int i =0;
				polNo=dataIn.getStrPolicyNumber();
				claimNo =dataIn.getStrClaimNumber();
				strEmailCategory= dataIn.getStrEmailCategory();
				if(polNo!=""&& polNo!=null)
				{   
					if (strEmailCategory.equalsIgnoreCase("AGENT"))
					{
						try {strAgentEmailAddr = odsDao.getAgentEmailAddr(polNo);}
						catch(DAOException er){
							strAgentEmailAddr="";
						}
						log.info("Agent Email addr : "+strAgentEmailAddr);
						if (strAgentEmailAddr==null||strAgentEmailAddr.isEmpty()==true){
							strAgentEmailAddr="";
						}
						i = claimDetailsDao.updateAgentEmailAddr(strAgentEmailAddr, claimNo);
					}
					else if (strEmailCategory.equalsIgnoreCase("SD"))
					{
						//strSDEmailAddr = odsDao.getEmailAddrSD(polNo);
						try {strSDEmailAddr = odsDao.getEmailAddrSD(polNo);}
						catch(DAOException er){
							strSDEmailAddr="";
						}
						log.info("SD Email addr : "+strSDEmailAddr);
						if (strSDEmailAddr==null||strSDEmailAddr.isEmpty()==true){
						
							strSDEmailAddr="";
						}
						i = claimDetailsDao.updateSDEmailAddr(strSDEmailAddr, claimNo);
						
					}
					else if (strEmailCategory.equalsIgnoreCase("NSD"))
					{
						//strNSDEmailAddr = odsDao.getEmailAddrNSD(polNo);
						try {strNSDEmailAddr = odsDao.getEmailAddrNSD(polNo);}
						catch(DAOException er){
							strNSDEmailAddr="";
						}
						log.info("NSD Email addr : "+strNSDEmailAddr);
						if (strNSDEmailAddr==null||strNSDEmailAddr.isEmpty()==true){
							strNSDEmailAddr="";
						}
						i = claimDetailsDao.updateNSDEmailAddr(strNSDEmailAddr, claimNo);
						
					}
					
				}
				
				response.setDecision("SUCCESS");
				response.setExceptionMessage("");
			    response.setWsProcessingStatus("1");
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
				int i = claimDetailsDao.updateNotificationFlag("Exception in retrieving Agent/SD/NSD Email Address. "+e.getMessage(), dataIn.getStrClaimNumber());
			    response.setDecision("ERROR RESPONSE");;
			    response.setExceptionMessage("Exception in retrieving Agent Email Address");
			    response.setWsProcessingStatus("2");
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				
				
			}
			log.info("Final Response  is " + response.toString());
			log.info("response.getDecision() --"+ response.getDecision());
			log.info("response.getExceptionMessage() --"+ response.getExceptionMessage());
			log.info("response.getWsProcessingStatus() --"+ response.getWsProcessingStatus());
			exchange.getOut().setBody(response);

		}

	}
