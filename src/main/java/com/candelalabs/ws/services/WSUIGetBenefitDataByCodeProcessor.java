/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetBenefitDatabyCodeWSInput;
import com.candelalabs.api.model.DataOutGetBenefitDatabyCodeWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.BenefitPlanMaster;
import com.candelalabs.ws.model.tables.BenefitSummaryModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetBenefitDataByCodeProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetBenefitDataByCodeProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetBenefitDatabyCodeWSResponse wsresponse = new DataOutGetBenefitDatabyCodeWSResponse();
		wsresponse.setIBenefitAmtApplicable(new BigDecimal(0));
		wsresponse.setIBenefitAmtPerDay(new BigDecimal(0));
		wsresponse.setIBenefitDaysClaimed(new BigDecimal(0));
		wsresponse.setIBenefitTotAmtUsed(new BigDecimal(0));
		wsresponse.setIBenefitPercentage(new BigDecimal(0));
		try {
			DataInGetBenefitDatabyCodeWSInput dataIn = exchange.getIn()
					.getBody(DataInGetBenefitDatabyCodeWSInput.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());

			if (Optional.ofNullable(dataIn).isPresent()) {
				log.info("Getting benefit name with benefitCode= " + dataIn.getStrBenefitCode());
				String benefitName = this.oDSDao.getBenefitNameByCode(dataIn.getStrBenefitCode(),
						dataIn.getStrClaimTypeUI());
				log.info("BENEFIT Name = " + benefitName);

				wsresponse.setStrBenefitName(benefitName);

				if (benefitName == null || benefitName.trim() == "") {
					throw new DAOException("No benefitName Available for BenefitCode = " + dataIn.getStrBenefitCode());
				}

				if (dataIn.getStrClaimTypeUI().equalsIgnoreCase("HS")
						|| dataIn.getStrClaimTypeUI().equalsIgnoreCase("HSR")
						|| dataIn.getStrClaimTypeUI().equalsIgnoreCase("HC")) {
					ComponentDetailsBO compDetailsData = this.oDSDao.getComponentDetailsBO(dataIn.getStrPolicyNumber(),
							dataIn.getStrComponentCode(), dataIn.getStrInsuredClientID(), dataIn.getStrPlanCode());
					if (!Optional.ofNullable(compDetailsData).isPresent()) {
						throw new DAOException("Not found any record in ComponentDetails table ");
					}
					log.info("Got the componentDetails " + dataIn.getStrBenefitCode());
					Date dtComponentRCD = compDetailsData.getCOMPONENTRCD();
					log.info("Got the dtComponentRCD " + dtComponentRCD);
					List<BenefitSummaryModel> benSummary_li = new ArrayList<BenefitSummaryModel>();
					benSummary_li = this.oDSDao.getBenefitSummaryResultSet(dataIn.getStrBenefitCode(),
							dataIn.getStrPolicyNumber(), dataIn.getStrPlanCode(),
							Util.objToDate(dataIn.getDtAdmissionDate(), "dd/MM/yyyy"),
							compDetailsData.getCOMPONENTRCD());
					log.info("Got benefitSummery with size =  " + benSummary_li.size());
					if (benSummary_li.size() <= 0) {
						wsresponse.setIBenefitDaysClaimed(new BigDecimal(0));
						wsresponse.setIBenefitTotAmtUsed(new BigDecimal(0));
					} else {
						wsresponse.setIBenefitDaysClaimed(benSummary_li.get(0).getTOTALDAYSUSED());
						wsresponse.setIBenefitTotAmtUsed(benSummary_li.get(0).getTOTALAMOUNTUSED());
					}

					List<BenefitPlanMaster> benPlan_li = new ArrayList<BenefitPlanMaster>();
					benPlan_li = this.oDSDao.getBenefitPlanResultSet(dataIn.getStrBenefitCode(),
							dataIn.getStrPlanCode(), dataIn.getStrComponentCode());
					log.info("Got BenefitPlan with size =  " + benPlan_li.size());
					if (benPlan_li.size() <= 0) {
						wsresponse.setIBenefitAmtApplicable(new BigDecimal(0));
						wsresponse.setIBenefitAmtPerDay(new BigDecimal(0));
						wsresponse.setIBenefitAnnualTotalDays(new BigDecimal(0));
						wsresponse.setIBenefitMaxAmountPerDay(new BigDecimal(0));
					} else {
						wsresponse.setIBenefitAmtApplicable(benPlan_li.get(0).getAMOUNTANNUALLY());
						wsresponse.setIBenefitAmtPerDay(benPlan_li.get(0).getAMOUNTPERDAY());
						wsresponse.setIBenefitAnnualTotalDays(benPlan_li.get(0).getTOTALNOOFDAYS());
						wsresponse.setIBenefitMaxAmountPerDay(benPlan_li.get(0).getAMOUNTPERDAY());

						if (dataIn.getStrClaimTypeUI().trim().equalsIgnoreCase("HC")) {
							// BigDecimal amtperday =
							// benPlan_li.get(0).getAMOUNTPERDAY()
							// .multiply(new
							// BigDecimal(compDetailsData.getUNIT()));
							// wsresponse.setIBenefitAmtPerDay(amtperday);
							wsresponse.setIBenefitNoOfUnits(compDetailsData.getUNIT());
						}

					}

				} else if (dataIn.getStrClaimTypeUI().trim().equalsIgnoreCase("ADDB")) {
					BigDecimal benefitPercentage = this.oDSDao.getBenefitPercentage(dataIn.getStrComponentCode(),
							dataIn.getStrBenefitCode());
					wsresponse.setIBenefitPercentage(benefitPercentage);
				}
				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Success");

			} else {
				throw new CamelException("Request Input Is Empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.info("ERROR DETAIL: " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL " + e.getMessage());
		}

		log.info("JSON RESPONSE PAYLOAD: " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);
	}

}
