/**
 * 
 */
package com.candelalabs.ws.services;

import java.sql.Timestamp;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.JsonWSGenerateFilesDataIn;
import com.candelalabs.api.model.WSUpdateClaimStatusDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSGenerateFilesProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSGenerateFilesProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ProducerTemplate producerTemplate;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse response = new JsonResponse();
		try {
			JsonWSGenerateFilesDataIn dataIn = exchange.getIn().getBody(JsonWSGenerateFilesDataIn.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
			if (dataIn.getClaimNo() != null && dataIn.getGenerateFile() != null && dataIn.getClaimNo().trim() != ""
					&& dataIn.getGenerateFile().trim() != "") {
				if (dataIn.getGenerateFile().trim().equalsIgnoreCase("GENPAYMENTVOUCHER")) {
					log.info("GENPAYMENTVOUCHER started");
					log.info("Updating ClaimData with claimNo = " + dataIn.getClaimNo()
							+ " to Complete the claimStatus");

					int i = this.claimDetailsDao.updateClaimData(dataIn.getClaimNo(), "COMPLETE",
							new Timestamp(System.currentTimeMillis()));
					if (i == 0) {
						throw new DAOException("ClaimData is not updated with claimNo = " + dataIn.getClaimNo());
					}

					log.info("Successfully Updated ClaimData with claimNo = " + dataIn.getClaimNo()
							+ " to Complete the claimStatus");
				} else if (dataIn.getGenerateFile().trim().equalsIgnoreCase("TPADUPCHECK")
						|| dataIn.getGenerateFile().trim().equalsIgnoreCase("TPAMODIFYDATA")) {
					log.info("TPAMODIFYDATA started");
					log.info(
							"Updating ClaimData with claimNo = " + dataIn.getClaimNo() + " to Waiting the claimStatus");
					int j = this.claimDetailsDao.updateClaimData(dataIn.getClaimNo(), "WAITING", null);
					if (j == 0) {
						throw new DAOException("ClaimData is not updated with claimNo = " + dataIn.getClaimNo());
					}
					// WSUpdateClaimStatusDataIn jsonData = new
					// WSUpdateClaimStatusDataIn();
					// jsonData.setClaimNo(dataIn.getClaimNo());
					// jsonData.setClaimstatus("WAITING");
					// JsonResponse wsUpdateResponse = producerTemplate
					// .requestBody("direct:wSUpdateClaimStatusInClaimDataRoute",
					// jsonData, JsonResponse.class);
					// if
					// (wsUpdateResponse.getWsProcessingStatus().equalsIgnoreCase("2"))
					// {
					// throw new Exception("Failed on invoking the
					// wSUpdateClaimStatusInClaimData with reason = "
					// + wsUpdateResponse.getWsExceptionMessage());
					// }
					// log.info("Successfully Updated ClaimData with claimNo = "
					// + dataIn.getClaimNo()
					// + " to Waiting the claimStatus");
				} else {
					throw new CamelException("Not Found the GenerateFile Request Type = " + dataIn.getGenerateFile());
				}

				log.info("Inserting GenerateFileRequestTable");
				int i = this.claimDetailsDao.insertGenerateFileRequest(dataIn.getClaimNo(), dataIn.getGenerateFile());
				if (i == 0) {
					throw new DAOException(
							"GenerateFileRequest Table is not updated with claimNo = " + dataIn.getClaimNo());
				}
				log.info("Succesfully Inserting GenerateFileRequestTable");

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Successfully Generate File Request");

			} else {
				throw new CamelException("Request Input String is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.error("ERROR DETAIL = " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error Detail = " + e);
		}
		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);

	}

}
