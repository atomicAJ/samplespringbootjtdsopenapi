/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataUIClaimDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsAdditionalRequirement;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsBenefitfundDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsWSResponse;
import com.candelalabs.api.model.DataInGetWSGetClaimUIDataWSInput;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.model.tables.RequirementDetailsModel;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSUIGetClaimData implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	private static final String dateFormat = "dd/MM/yyyy";

	private static Log log = LogFactory.getLog("WSUIGetClaimData");

	@Value("${AUTOSAVETIME}")
	private long autosaveTime;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		// log.info("************************************************************************************************");
		ClaimDataUIClaimDetails dataOut = new ClaimDataUIClaimDetails();
		ClaimDataUIClaimDetailsWSResponse wsresponse = new ClaimDataUIClaimDetailsWSResponse();
		List<ClaimDataUIClaimDetailsAdditionalRequirement> additionalReq_li = new ArrayList<ClaimDataUIClaimDetailsAdditionalRequirement>();
		List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitFundDetails_li = new ArrayList<ClaimDataUIClaimDetailsBenefitfundDetails>();

		try {
			DataInGetWSGetClaimUIDataWSInput dataIn = exchange.getIn().getBody(DataInGetWSGetClaimUIDataWSInput.class);
			log.info("JSON RESPONSE INPUT PAYLOAD = " + dataIn.toString());

			if (dataIn.getStrClaimNumber() != null && dataIn.getStrClaimNumber().trim() != "") {
				dataIn.setStrClaimNumber(dataIn.getStrClaimNumber().trim());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getStrClaimNumber());

				if (!Optional.ofNullable(claimData).isPresent()) {
					throw new DAOException(
							"There is no record in CLAIMDATA for claimNumber = " + dataIn.getStrClaimNumber());
				}

				log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
				PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
				log.info("Successfully retrieved PolicyDetail table");

				if (!(Optional.ofNullable(claimData).isPresent() && Optional.ofNullable(policyDetail).isPresent())) {
					throw new DAOException("PolicyDetails is null with Pol NO= " + claimData.getPolicyNumber());
				}
				;

				dataOut.setClaimNumber(dataIn.getStrClaimNumber());
				dataOut.setClaimPolicyNumber(claimData.getPolicyNumber());
				dataOut.setClaimProductName(claimData.getProductCode());
				dataOut.setClaimTypeLA(claimData.getClaimTypeLA());
				dataOut.setClaimPlanCode(claimData.getPlanCode());
				dataOut.setClaimReceivedDate(Util.dateToBigDecimal(claimData.getReceivedDate()));
				dataOut.setClaimRegisterDate(Util.dateToBigDecimal(claimData.getRegisterDate()));
				dataOut.setClaimInsuredName(claimData.getInsuredClientName());
				dataOut.setClaimInsuredClientID(claimData.getInsuredClientID());
				dataOut.setClaimDiagnoseCode(claimData.getDiagnosisCode());
				dataOut.setClaimDiagnoseName(claimData.getDiagnosisName());
				dataOut.setClaimAdmissionDate(Util.dateToBigDecimal(claimData.getAdmissionDate()));
				dataOut.setClaimDischargeDate(Util.dateToBigDecimal(claimData.getDischargeDate()));
				dataOut.setClaimProvider(claimData.getProviderName() == null ? claimData.getProviderName()
						: claimData.getProviderName().trim());
				
				dataOut.setClaimProviderSource(claimData.getProviderNameSource());
				dataOut.setClaimDoctorName(claimData.getDoctorName());
				dataOut.setClaimInvoiceAmount(claimData.getClaimInvoiceAmount());
				dataOut.setClaimInvoiceAmountUI((claimData.getClaimInvoiceAmountUI() == null ||
						claimData.getClaimInvoiceAmountUI().compareTo(new BigDecimal(0)) == 0) ? claimData.getClaimInvoiceAmount()
						: claimData.getClaimInvoiceAmountUI());
				dataOut.setClaimPolicyCurrency(policyDetail.getCURRENCY());
				dataOut.setClaimSourcecurrency(claimData.getClaimCurrency());
				dataOut.setClaimDeathBenefitCategory(claimData.getBenefitCategory());
				dataOut.setClaimPayeeName(claimData.getPayeeName());
				dataOut.setClaimFactoringHouse(claimData.getFactoringHouse());
				dataOut.setClaimBankCode(claimData.getBankCode());
				dataOut.setClaimBankAccountName(claimData.getBankAccountName());
				dataOut.setClaimBankAccountNumber(claimData.getBankAccountNumber());
				dataOut.setClaimBankType(claimData.getBankType());
				dataOut.setComment1_("claimSumAssuredPercent is used in ADDB,CI,TERM,WAIVER UI Component Name Field");
				//"__comment1__":"claimSumAssuredPercent is used in ADDB,CI,TERM,WAIVER UI Component Name Field",
				dataOut.setClaimSumAssuredPercent(claimData.getClaimSumAssuredPercentage());
				dataOut.setComment2_("claimComponentName is used in ADDB,CI,TERM,WAIVER UI Component Name Field");
				//"__comment2__":"claimComponentName is used in ADDB,CI,TERM,WAIVER UI Component Name Field",
				dataOut.setClaimComponentCode(claimData.getComponentCode());
				dataOut.setClaimComponentName(claimData.getComponentCode());
				dataOut.setComment3_("claimComponentSumAssured is used in ADDB,CI,TERM,WAIVER UI Component SA Field");
				//"__comment3__":"claimComponentSumAssured is used in ADDB,CI,TERM,WAIVER UI Component SA Field",
				dataOut.setClaimComponentSumAssured(claimData.getComponentSumAssured());
				dataOut.setClaimOSCharges(claimData.getOSCharges());
				dataOut.setClaimOtherAdjustment(claimData.getOtherAdjustments());
				dataOut.setClaimAdjustmentCode(claimData.getAdjustmentCode());
				dataOut.setClaimPolicyLoan(claimData.getPolicyLoanAmount());
				dataOut.setClaimPolicyDebt(claimData.getPolicyDebtAmount());
				dataOut.setClaimFullyClaim(claimData.getFullyClaimed());
				dataOut.setClaimUITotalAmount(claimData.getClaimApprovedAmountUI());
				dataOut.setClaimCurrentFrom(Util.dateToBigDecimal(claimData.getCurrentFrom()));
				dataOut.setAutosavetime(new BigDecimal(TimeUnit.MINUTES.toMillis(this.autosaveTime)));
				dataOut.setClaimStayDuration(claimData.getStayDuration());
				dataOut.setAutosaveflag("Y");
				log.info("claimData.getClaimPayDate() is -" +claimData.getClaimPayDate());
				dataOut.setClaimPaymentDate(Util.dateToBigDecimal(claimData.getClaimPayDate()));
				//"autosaveflag":"Y"
				//claimPaymentDate
				//claimPaidToDate
				//claimCompPremCessDate
				/************/
				//Fetching Additinal data for UI Validations  (on top of existing functionality)
				//eClaims CR in August 2019 ");
				log.info("WSUIGetClaimData- claimData.getClaimTypeUI() is: "+claimData.getClaimTypeUI());
				String strComponentPremiumFrequency ="";
				Date dtPremiumCessationDate=null;
				Date dtFinalPaymentDate=null;
				if (claimData.getClaimTypeUI().equalsIgnoreCase("WAIVER") || 
						(claimData.getClaimTypeUI().equalsIgnoreCase("WOP"))) {
					log.info("inside claimData.getClaimTypeUI().equalsIgnoreCase(WAIVER) ||"
							+ " (claimData.getClaimTypeUI().equalsIgnoreCase(WOP))");
					   strComponentPremiumFrequency = odsDao.getPaymentFreq(claimData.getPolicyNumber());
					   log.info("WSUIGetClaimData-strComponentPremiumFrequency is : "+strComponentPremiumFrequency);
					   
					   dtPremiumCessationDate = odsDao.getCOMPPREMCESDATE(claimData.getPolicyNumber());
					   log.info("WSUIGetClaimData-dtPremiumCessationDate is : "+dtPremiumCessationDate);
					   
					 
					   int month = 0;
					   
					   if (strComponentPremiumFrequency.equalsIgnoreCase("01")) {
						  // dtPremiumCessationDate =  dtPremiumCessationDate.getYear()-1
						   month = dtPremiumCessationDate.getMonth()-12;
						   dtPremiumCessationDate.setMonth(month);
						   dtFinalPaymentDate = dtPremiumCessationDate;
					   }
					  else if (strComponentPremiumFrequency.equalsIgnoreCase("02")) {
						  month = dtPremiumCessationDate.getMonth()-6;
						   dtPremiumCessationDate.setMonth(month);
						   dtFinalPaymentDate = dtPremiumCessationDate;
					  }
					  else if (strComponentPremiumFrequency.equalsIgnoreCase("04")){
						  month = dtPremiumCessationDate.getMonth()-3;
						   dtPremiumCessationDate.setMonth(month);
						   dtFinalPaymentDate = dtPremiumCessationDate;
					  }
					  else if (strComponentPremiumFrequency.equalsIgnoreCase("12")){
						  month = dtPremiumCessationDate.getMonth()-1;
						   dtPremiumCessationDate.setMonth(month);
						   dtFinalPaymentDate = dtPremiumCessationDate;
					  }
					  else if (strComponentPremiumFrequency.equalsIgnoreCase("00")){
						  dtFinalPaymentDate = dtPremiumCessationDate; 
					  }
					   log.info("WSUIGetClaimData-dtFinalPaymentDate is : "+dtFinalPaymentDate);
					   
					   //dataOut.setClaimAdmissionDate(Util.dateToBigDecimal(claimData.getAdmissionDate()));
					   BigDecimal decClaimCompPremCessDate = Util.dateToBigDecimal( dtFinalPaymentDate);
					   log.info("decimal ClaimCompPremCessDate is :-"+ decClaimCompPremCessDate);
					   dataOut.setClaimCompPremCessDate(decClaimCompPremCessDate);
					   
					   Date    claimPaidToDate =null;
					   claimPaidToDate = odsDao.getPAIDTODATE(claimData.getPolicyNumber());
					   log.info("WSUIGetClaimData-claimPaidToDate is : "+claimPaidToDate);
					   BigDecimal decclaimPaidToDate = Util.dateToBigDecimal( claimPaidToDate);
					   log.info("WSUIGetClaimData-decclaimPaidToDate  is : "+decclaimPaidToDate);
					   dataOut.setClaimPaidToDate( decclaimPaidToDate);
					 //  dataOut.setClaimPaidToDate(claimPaidToDate);
					   //= SELECT PAIDTODATE FROM V_POLICYDETAILS WHERE POLICYNUMBER= '<value of PolicyNumber for Claim Case>'
					   
				}
				else
				{
					 log.info("in else  setting dates as null");
					 dataOut.setClaimCompPremCessDate( null);
					 dataOut.setClaimPaidToDate(null);
				}
				/************/
				log.info("Get claimDataDetails table wiodsDaoth claimNo= " + dataIn.getStrClaimNumber());
				List<ClaimDataDetailsModel> claimDataDetails_li = new ArrayList<ClaimDataDetailsModel>();
				claimDataDetails_li = this.claimDetailsDao.getClaimDataDetailsByClaimNo(dataIn.getStrClaimNumber());
				log.info("Successfully retrieved claimDataDetails table with size = " + claimDataDetails_li.size());

				if (claimDataDetails_li != null && claimDataDetails_li.size() > 0) {
					for (ClaimDataDetailsModel claimDataDetailsModel : claimDataDetails_li) {
						ClaimDataUIClaimDetailsBenefitfundDetails benFUndModel = new ClaimDataUIClaimDetailsBenefitfundDetails();
						benFUndModel.setBenefitCode(claimDataDetailsModel.getBenefitCode());
						benFUndModel.setBenefitName(claimDataDetailsModel.getBenefitName());
						benFUndModel.setBenefitnoOfDaysClaimed(claimDataDetailsModel.getBenefitDaysClaimed() != null
								? claimDataDetailsModel.getBenefitDaysClaimed().intValue() : 0);
						benFUndModel.setBenefittotalAmountClaimedAsOfNow(claimDataDetailsModel.getBenefitAmountClaimed());
						benFUndModel.setBenefitTotalAvailableAmountForClaim(claimDataDetailsModel.getBenefitAmountApplicable());
						benFUndModel.setBenefitdailyIncurred(claimDataDetailsModel.getBenefitAmountPerDay());
						benFUndModel.setBenefitDateFrom(Util.dateToBigDecimal(claimDataDetailsModel.getBenefitFromDate()));
						benFUndModel.setBenefitDateTo(Util.dateToBigDecimal(claimDataDetailsModel.getBenefitToDate()));
						benFUndModel.setBenefitNoOfDays(claimDataDetailsModel.getBenefitActualDays());
						benFUndModel.setBenefitAmountIncurred(claimDataDetailsModel.getBenefitInvoiceAmount());
						// benFUndModel.setBenefitAmountApproveFromTPA(claimDataDetailsModel.getBenefitApprovedAmount());
						benFUndModel.setBenefitApprovedAmount(claimDataDetailsModel.getBenefitApprovedAmount());
						benFUndModel.setBenefitAactualPayableByClaimUser(claimDataDetailsModel.getBenefitUserApprovedAmount());
						benFUndModel.setBenefitPercentage(claimDataDetailsModel.getBenefitPercentage());
						benFUndModel.setBenefitAnnualTotalDays(claimDataDetailsModel.getBenefitAnnualTotalDays());
						benFUndModel.setBenefitNotApprovedAmount(claimDataDetailsModel.getBenefitNotApprovedAmount());
						benFUndModel.setBenefitMaxAmountPerDay(claimDataDetailsModel.getBenefitMaxAmountPerDay());
						benFUndModel.setBenefitTotalIncurredAmount(claimDataDetailsModel.getBenefitTotalIncurredAmount());
						benFUndModel.setBenefitNoOfUnits(claimDataDetailsModel.getBenefitNoOfUnits());
						benFUndModel.setFundComponentCode(claimDataDetailsModel.getFundComponentCode());
						benFUndModel.setFundComponentName(claimDataDetailsModel.getFundComponentName());
						benFUndModel.setFundCode(claimDataDetailsModel.getFundCode());
						benFUndModel.setFundDescription(claimDataDetailsModel.getFundDescription());
						benFUndModel.setFundEstimatedAmount(claimDataDetailsModel.getFundEstimatedAmount());
						benFUndModel.setFundActualAmount(claimDataDetailsModel.getFundActualAmount());

						benefitFundDetails_li.add(benFUndModel);
					}
				}

				log.info("Get RequirementDetails table with claimNo= " + dataIn.getStrClaimNumber());
				List<RequirementDetailsModel> reqDetails_li = new ArrayList<RequirementDetailsModel>();
				reqDetails_li = this.claimDetailsDao.getRequirementDetailsList(dataIn.getStrClaimNumber());
				log.info("Successfully retrieved RequirementDetails table with size = " + reqDetails_li.size());

				if (reqDetails_li != null && reqDetails_li.size() > 0) {
					for (RequirementDetailsModel requirementDetailsModel : reqDetails_li) {
						ODSRequirementMasterModel reqMasterData = new ODSRequirementMasterModel();
						reqMasterData = this.odsDao.getRequirementMaster(requirementDetailsModel.getDocID());

						ClaimDataUIClaimDetailsAdditionalRequirement addRequirement = new ClaimDataUIClaimDetailsAdditionalRequirement();
						addRequirement.setReqtDropDownText(reqMasterData.getDROPDOWNTEXT());
						addRequirement.setSubCategory(reqMasterData.getSUBCATEGORY());
						addRequirement.setAdditionaltext(requirementDetailsModel.getRequirementText());
						//__notificationcomment__
						addRequirement.setNotificationsent(requirementDetailsModel.getRequirementNotificationSent());

						additionalReq_li.add(addRequirement);
					}
				}

				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Successfully Retrieved the ClaimDataUI table");

			} else {
				throw new CamelException("Request Input String is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR = " + e);
		}

		dataOut.setWsResponse(wsresponse);
		dataOut.setAdditionalRequirement(additionalReq_li);
		dataOut.setBenefitfundDetails(benefitFundDetails_li);
		log.info("JSON RESPONSE PAYLOAD = " + dataOut.toString());
		exchange.getOut().setBody(dataOut);
	}

}
