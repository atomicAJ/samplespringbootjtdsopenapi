package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataOutWSGetReportTypeListWSResponse;
import com.candelalabs.api.model.ReportTypeListData;
import com.candelalabs.ws.dao.ClaimDetailsDao;

@Component
public class WSGetReportTypeListProcessor implements Processor {
	
	private static final Log log = LogFactory.getLog("WSGetReportTypeListProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		DataOutWSGetReportTypeListWSResponse wsResponse = new DataOutWSGetReportTypeListWSResponse();
		
		try {
			List<ReportTypeListData> reportTypeListData = new ArrayList<ReportTypeListData>();
			
			reportTypeListData = claimDetailsDao.getReportTypeList();
			
			wsResponse.setWsProcessingStatus("1");
			wsResponse.setWsSuccessMessage("Success");
			wsResponse.setStrReportTypeList(reportTypeListData);
		}catch(Exception e) {
			log.error("WSGetReportTypeListProcessor Error" + e);
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsExceptionMessage(e.getMessage());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("Final Response " + wsResponse.toString());
		exchange.getOut().setBody(wsResponse);
	}

}
