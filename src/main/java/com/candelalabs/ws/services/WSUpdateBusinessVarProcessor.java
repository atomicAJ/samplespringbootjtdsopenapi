package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.SignalDetails;
import com.candelalabs.api.model.WSCLUpdAppClaimStatusDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSCLUpdAppClaimStatusMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.services.activitirest.RestRequest;

@Component
@PropertySource("classpath:application.properties")
public class WSUpdateBusinessVarProcessor implements Processor {

	@Autowired
	RestRequest restRequest;
	
	private static final Log log = LogFactory.getLog("WSUpdateBusinessVarProcessor");

	// TODO Auto-generated method stub
	@Value("${USRPRF}")
	private String USRPRF;

	public void process(Exchange exchange) throws Exception {

		log.info("**WSUpdateBusinessVarProcessor******************************************* \n"
				+ "Processing request");
		SignalDetails input = exchange.getIn().getBody(SignalDetails.class);
		log.debug("Processing request with Payload: " + input.toString());
		JsonResponse response = new JsonResponse();
		
			if (input.getProcessInstanceId() != null && input.getProcessInstanceId() != "")
					{
				log.debug("Variables size isEmpty=" + input.getVariables() == null ? true : false);
				if (!(input.getVariables() == null)) {
					log.info("Invoking Activiti BPM to Update Variables");
					this.restRequest.updateVariables(input.getVariables(), input.getProcessInstanceId());
					log.info("Finished Invoking Activiti BPM to Update Variables");
				}
			}
		
	}

}
