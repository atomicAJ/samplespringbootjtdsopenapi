/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSDetermineAuthorityLimitDataIn;
import com.candelalabs.api.model.WSDetermineAuthorityLimitDataOut;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.AuthorityMatrixModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSDetermineAuthorityLimitProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSDetermineAuthorityLimitProcessor");

	@Autowired
	private ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("*************************************************************************** \n "
				+ "Service is invoked");
		WSDetermineAuthorityLimitDataOut response = new WSDetermineAuthorityLimitDataOut();
		response.setAPPROVALAUTHORITY(false);
		response.setREJECTIONAUTHORITY(false);
		response.setWsExceptionMessage("");
		response.setWsProcessingStatus("");
		response.setWsSuccessMessage("");
		int approvalAuthorityCount = 0;
		int rejectAuthorityCount = 0;
		try {
			WSDetermineAuthorityLimitDataIn dataIn = exchange.getIn().getBody(WSDetermineAuthorityLimitDataIn.class);
			log.info("Data Input Payload= " + dataIn.toString());
			if (Optional.ofNullable(dataIn).isPresent()) {
				log.info("Fetching the CLAIMDATA table with claimNumber = " + dataIn.getCLAIMNUMBER());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getCLAIMNUMBER());
				if (Optional.ofNullable(claimData).isPresent()) {
					List<AuthorityMatrixModel> matrix_li =  new ArrayList<AuthorityMatrixModel>();
					log.info(" matrix_li.size() for before checking is :"+  matrix_li.size() );
					matrix_li = this.claimDetailsDao.getAuthorityMatrixData(claimData.getClaimTypeUI(), "APPROVE",
							dataIn.getCLAIMUSER());
					//matrix_li = this.claimDetailsDao.getAuthorityMatrixData(claimData.getClaimApprovedAmountUI().toString(),
					//"APPROVE",
					//dataIn.getCLAIMUSER());
					log.info(" matrix_li.size() for Approve is :"+  matrix_li.size() );
					if (matrix_li == null || matrix_li.size() <= 0) {
						approvalAuthorityCount = 0;
					} 
					else 
					{
						String comparisonOperation = matrix_li.get(0).getComparisonOperation();
						approvalAuthorityCount = this.claimDetailsDao.getTotalAuthorityMatrix(claimData,
								dataIn.getCLAIMUSER(), "APPROVE", comparisonOperation);
					}

					matrix_li = this.claimDetailsDao.getAuthorityMatrixData(claimData.getClaimTypeUI(), "REJECT",
							dataIn.getCLAIMUSER());
					//matrix_li = this.claimDetailsDao.getAuthorityMatrixData(claimData.getClaimApprovedAmountUI().toString()
					//	, "REJECT",
					//dataIn.getCLAIMUSER());
					log.info(" matrix_li.size() for Reject is :"+  matrix_li.size() );
					if (matrix_li == null || matrix_li.size() <= 0) {
						rejectAuthorityCount = 0;
					} 
					else 
					{
						String comparisonOperation = matrix_li.get(0).getComparisonOperation();
						rejectAuthorityCount = this.claimDetailsDao.getTotalAuthorityMatrix(claimData,
								dataIn.getCLAIMUSER(), "REJECT", comparisonOperation);
					}

					if (approvalAuthorityCount > 0) {
						response.setAPPROVALAUTHORITY(true);
					}

					if (rejectAuthorityCount > 0) {
						response.setREJECTIONAUTHORITY(true);
					}
				} 
				else 
				{
					throw new DAOException("ClaimData table is empty with Claimnumber= " + dataIn.getCLAIMNUMBER());

				}

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Approval and / or Reject Authority determined for the user");

			} 
			else 
			{
				throw new CamelException("Request String is Empty");
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = " + e + " \n " + e.fillInStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage(
					"Could not determine Approval and / or Rejection Authority for the user. Case cannot be checked out: "
							+ e.getMessage());
			response.setAPPROVALAUTHORITY(false);
			response.setREJECTIONAUTHORITY(false);
		}

		log.info("JSON RESPONSE PAYLOAD= " + response.toString());
		exchange.getOut().setBody(response);

	}

}
