/**
 * 
 */
package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSUpdateClaimDataGenericDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;

/**
 * @author Triaji
 *
 */
@Component
public class WSUpdateClaimDataGeneralProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSUpdateClaimDataGeneral");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse response = new JsonResponse();
		try {
			WSUpdateClaimDataGenericDataIn dataIn = exchange.getIn().getBody(WSUpdateClaimDataGenericDataIn.class);
			log.info("Data Input = " + dataIn.toString());
			int i = this.claimDetailsDao.updateClaimDataGeneral(dataIn.getClaimNumber(), dataIn.getStrField(),
					dataIn.getStrFieldValue(), dataIn.getStrFieldValueType());
			if (i == 0) {
				throw new Exception("No Claim is to be Updated for ClaimNumber= " + dataIn.getClaimNumber());
			}
			response.setWsProcessingStatus("1");
			response.setWsSuccessMessage("Claim Data is updated Successfully");
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error " + e);
			// log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("ERROR Detail = " + e.getMessage());
		}
		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);
	}

}
