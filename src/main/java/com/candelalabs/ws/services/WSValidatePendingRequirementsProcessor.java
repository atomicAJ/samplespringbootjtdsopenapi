package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSWSValidatePendingRequirementsDataIn;
import com.candelalabs.api.model.WSWSValidatePendingRequirementsDataOut;
import com.candelalabs.ws.dao.ClaimDetailsDao;

@Component
public class WSValidatePendingRequirementsProcessor implements Processor {
	
	private static final Log log = LogFactory.getLog("WSValidatePendingRequirementsProcessor");
	
	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		log.info("WSValidatePendingRequirementsProcessor - Service is invoked");
		WSWSValidatePendingRequirementsDataOut response = new WSWSValidatePendingRequirementsDataOut();
		String strClaimNumber ="";
		try {
			WSWSValidatePendingRequirementsDataIn dataIn = (exchange.getIn().getBody(WSWSValidatePendingRequirementsDataIn.class));
			log.info(dataIn.toString());
			if(dataIn.getWsInput().getStrClaimNumber() == null || dataIn.getWsInput().getStrClaimNumber() == "") {
				response.setWsExceptionMessage("Claim number can not be null or empty");
				response.setWsProcessingStatus("2");
			}else {
				strClaimNumber = dataIn.getWsInput().getStrClaimNumber();
				int iRecordCount = claimDetailsDao.getPendingCompletionStatus(strClaimNumber);
				
				if(iRecordCount != 0) {
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Record count available");
					response.setStrPendingRequirementStatus(true);
				}else {
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Additional Requirements not raised. Please raise atleast one pending requirement "
							+ "before taking 'REQUEST FOR ADDITIONAL DOCUMENTS' decision");
					response.setStrPendingRequirementStatus(false);
				}
				
			}
			
		} catch(Exception e) {
			log.error("WSValidatePendingRequirementsProcessor Error" + e);
			response.setWsExceptionMessage("Unable to fetch Pending Requirement Details for ClaimNumber = " + strClaimNumber +". "+ e.getMessage());
			response.setWsProcessingStatus("2");
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("Final Response " + response.toString());
		exchange.getOut().setBody(response);
	}

}
