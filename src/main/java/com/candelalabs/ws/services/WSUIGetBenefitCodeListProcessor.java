/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetBenefitCodeListWSInput;
import com.candelalabs.api.model.DataOutGetBenefitCodeListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetBenefitCodeListProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetBenefitCodeListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetBenefitCodeListWSResponse wsresponseCode = new DataOutGetBenefitCodeListWSResponse();
		try {
			DataInGetBenefitCodeListWSInput dataIn = exchange.getIn().getBody(DataInGetBenefitCodeListWSInput.class);
			log.info("DATA INPUT: "+dataIn.toString());
			List<String> benList = new ArrayList<String>();
			benList = this.oDSDao.getBenefitCodeList(dataIn.getStrComponentCode().trim(),dataIn.getStrClaimTypeUI());
			if (benList.size() > 0 && benList != null) {
				wsresponseCode.setStrBenefitCodeList(benList);
				wsresponseCode.setWsProcessingStatus("1");
				wsresponseCode.setWsSuccessMessage("Successfully retrieved the Benefit Code List");
			} else {
				throw new DAOException("Benefit Code List is not available");
			}


		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			wsresponseCode.setWsProcessingStatus("2");
			wsresponseCode.setWsExceptionMessage("ERROR DETAIL " + e.getMessage());
		}
		
		log.info("JSON RESPONSE PAYLOAD: "+wsresponseCode.toString());
		exchange.getOut().setBody(wsresponseCode);

	}

}
