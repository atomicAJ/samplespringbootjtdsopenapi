package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataPortalOutput;
//import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.dao.impl.ClaimDetailsDaoImpl;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;
import java.text.ParseException;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSPortalServiceCreateClaim implements Processor {

		private static final Log log = LogFactory.getLog("WSPortalServiceCreateClaim");
	
		@Autowired
	    ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;
		/**
		 * 
		 */
		public WSPortalServiceCreateClaim() {
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 */
		
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			
			WSPortalServiceDataIn response = new WSPortalServiceDataIn();
			log.info("WSPortalServiceCreateClaim - Service is invoked");
		
			 
			try {
				WSPortalServiceDataIn dataIn = exchange.getIn().getBody(WSPortalServiceDataIn.class);
				
				 
				log.info("WSPortalServiceProcessor - Json Object In= " + dataIn.toString());
				String strInputExceptionFlag = "N";
				String strExceptionMessage="";
				String strClaimNumber="";
				if (dataIn.getStrPortalClaimRequestNumber() == null || (dataIn.getStrPortalClaimRequestNumber().trim()) == "") 
				{
				   log.info("WSPortalServiceProcessor - StrPortalClaimRequestNumber() == null ");
				   strExceptionMessage = "Cannot Create Claim Record without Claim Request Number for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getDtPortalClaimRequestCreationDate()== null || (dataIn.getDtPortalClaimRequestCreationDate().trim() ) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Claim Request Date for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrPolicyNumber() == null || (dataIn.getStrPolicyNumber().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Policy Number for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrOwnerClientID() == null || (dataIn.getStrOwnerClientID().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Owner Client ID for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrInsuredClientID() == null || (dataIn.getStrInsuredClientID().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Insured ID for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrInsuredClientName() == null || (dataIn.getStrInsuredClientName().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Insured Name for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrFormClaimType() == null || (dataIn.getStrFormClaimType().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Claim Type for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrFormBenefitType() == null || (dataIn.getStrFormBenefitType().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Claim Benefit Type for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				   
				}
				if (strInputExceptionFlag == "Y") 
				{
					log.info("Inside strInputExceptionFlag == Y before creating json response");
					response.setStrPortalClaimRequestNumber(dataIn.getStrPortalClaimRequestNumber().toString());
					response.setDtPortalClaimRequestCreationDate( dataIn.getDtPortalClaimRequestCreationDate());
		            response.setStrPolicyNumber(dataIn.getStrPolicyNumber());
		            response.setStrOwnerClientID(dataIn.getStrOwnerClientID());  
		            response.setStrPayeeName( dataIn.getStrPayeeName());
		            response.setStrInsuredClientName( dataIn.getStrInsuredClientName());
		            response.setStrInsuredClientID( dataIn.getStrInsuredClientID());
		            response.setStrFormClaimType( dataIn.getStrFormClaimType());
		            response.setStrFormBenefitType( dataIn.getStrFormBenefitType());
		            response.setStrSurgeryPerformed( dataIn.getStrSurgeryPerformed());
		            response.setDtTreatmentStartDate(dataIn.getDtTreatmentStartDate());
		            response.setDtTreatmentCompletionDate( dataIn.getDtTreatmentCompletionDate());
		            response.setStrHospitalName( dataIn.getStrHospitalName());
		            response.setStrCurrency( dataIn.getStrCurrency());
		            response.setDblTotalBillingAmount(dataIn.getDblTotalBillingAmount());  
		            response.setStrPhysician( dataIn.getStrPhysician());
		            response.setStrBankAccountNumber(dataIn.getStrBankAccountNumber());
		            response.setStrPaymentAccountOwnerName( dataIn.getStrPaymentAccountOwnerName());
		            response.setWsProcessingStatus("2");
		            response.setWsExceptionMessage( strExceptionMessage);
		            response.setWsSuccessMessage( "");
		            log.info("Inside strInputExceptionFlag == Y after creating json response");
				}

				else
				{
					log.info("Inside else of strInputExceptionFlag == Y before insert into CLAIMDATASOURCEPORTAL");
					int inserted =0;
					inserted = claimDetailsDao.insertCLAIMDATASOURCEPORTAL(dataIn.getStrPortalClaimRequestNumber(),
							Util.objToDate(dataIn.getDtPortalClaimRequestCreationDate(),"dd/MM/yyyy"), 
							dataIn.getStrPolicyNumber(),
							dataIn.getStrOwnerClientID(),
							dataIn.getStrPayeeName(), 
							dataIn.getStrInsuredClientName(),
							dataIn.getStrInsuredClientID(),
							dataIn.getStrFormClaimType(), 
							dataIn.getStrFormBenefitType(), dataIn.getStrSurgeryPerformed(), 
							Util.objToDate(dataIn.getDtTreatmentStartDate(),"dd/MM/yyyy"), 
							Util.objToDate(dataIn.getDtTreatmentCompletionDate(),"dd/MM/yyyy"),
							dataIn.getStrHospitalName(), 
							dataIn.getStrCurrency(),//.getStrFormCurrency(),
							new BigDecimal(dataIn.getDblTotalBillingAmount()),
						    dataIn.getStrPhysician(), dataIn.getStrBankAccountNumber(), dataIn.getStrPaymentAccountOwnerName(), "N");
					log.info("Inside else of strInputExceptionFlag == Y After insert into CLAIMDATASOURCEPORTAL");
					if (inserted>0)
					{
						log.info("One record inserted into CLAIMDATASOURCEPORTAL, Before setting response  to Poratl");
						
						response.setStrPortalClaimRequestNumber(dataIn.getStrPortalClaimRequestNumber().toString());
						response.setDtPortalClaimRequestCreationDate( dataIn.getDtPortalClaimRequestCreationDate());
			            response.setStrPolicyNumber(dataIn.getStrPolicyNumber());
			            response.setStrOwnerClientID(dataIn.getStrOwnerClientID());  
			            response.setStrPayeeName( dataIn.getStrPayeeName());
			            response.setStrInsuredClientName( dataIn.getStrInsuredClientName());
			            response.setStrInsuredClientID( dataIn.getStrInsuredClientID());
			            response.setStrFormClaimType( dataIn.getStrFormClaimType());
			            response.setStrFormBenefitType( dataIn.getStrFormBenefitType());
			            response.setStrSurgeryPerformed( dataIn.getStrSurgeryPerformed());
			            response.setDtTreatmentStartDate(dataIn.getDtTreatmentStartDate());
			            response.setDtTreatmentCompletionDate( dataIn.getDtTreatmentCompletionDate());
			            response.setStrHospitalName( dataIn.getStrHospitalName());
			            response.setStrCurrency( dataIn.getStrCurrency());
			            response.setDblTotalBillingAmount(dataIn.getDblTotalBillingAmount());  
			            response.setStrPhysician( dataIn.getStrPhysician());
			            response.setStrBankAccountNumber(dataIn.getStrBankAccountNumber());
			            response.setStrPaymentAccountOwnerName( dataIn.getStrPaymentAccountOwnerName());
			            //response.setWsProcessingStatus("2");
			            response.setWsProcessingStatus("1");
			            response.setWsExceptionMessage( "");
			            response.setWsSuccessMessage( "Claim Request Accepted in the System");
			            log.info("One record inserted into CLAIMDATASOURCEPORTAL,After setting response  to Poratl");
					}
				}
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
			    response.setWsExceptionMessage( e.getMessage());
			    response.setWsProcessingStatus("2");
			    response.setWsSuccessMessage("");
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				
				
			}
			log.info("Final Response " + response.toString());

			exchange.getOut().setBody(response);

		}

	}
