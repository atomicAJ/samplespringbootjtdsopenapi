package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSReportCheckStatusDataIn;
import com.candelalabs.api.model.WSReportCheckStatusDataOut;
import com.candelalabs.api.model.WSReportCheckStatusDataOutWSResponse;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.model.PencilReportRequest;

@Component
public class WSReportCheckStatusProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSReportCheckStatusProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("WSReportCheckStatusProcessor - Service is invoked");

		WSReportCheckStatusDataIn dataIn = (exchange.getIn().getBody(WSReportCheckStatusDataIn.class));
		WSReportCheckStatusDataOut response = new WSReportCheckStatusDataOut();
		WSReportCheckStatusDataOutWSResponse wsResponse = new WSReportCheckStatusDataOutWSResponse();
		try {
			PencilReportRequest reportStatus = claimDetailsDao.getReportRequestStatus(dataIn.getWsInput().getStrReportRequestID());
			if(reportStatus != null) {
				wsResponse.setWsProcessingStatus("1");
				wsResponse.setStrReportOutuputFormat(reportStatus.getReportOutputFormat());
				wsResponse.setStrReportRequestID(reportStatus.getReportRequestID());
				wsResponse.setStrReportName(reportStatus.getReportName());
				wsResponse.setStrReportRequestTime(reportStatus.getReportRequestTimeStamp() != null ? reportStatus.getReportRequestTimeStamp().toString() : "");
				wsResponse.setStrReportResponseTime(reportStatus.getReportEndTimeStamp() != null ? reportStatus.getReportEndTimeStamp().toString() : "");
				wsResponse.setStrReportGenerationStatus(reportStatus.getReportGenerated());
				wsResponse.setStrReportGenerationRemarks(reportStatus.getReportGenerationMessage());
				wsResponse.setStrReportFileLocation(reportStatus.getReportFileLocation());
				response.setWsResponse(wsResponse);
			} else {
				wsResponse.setWsProcessingStatus("2");
				wsResponse.setWsExceptionMessage("Could not fetch the Report Status for the report request id: "+dataIn.getWsInput().getStrReportRequestID());
				response.setWsResponse(wsResponse);
			}
		} catch (Exception e) {
			log.error("WSReportCheckStatusProcessor Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsExceptionMessage("Could not fetch the Report Status: "+e.getMessage());
			response.setWsResponse(wsResponse);
		}

		log.info("Final Response " + response.toString());
		exchange.getOut().setBody(response);
	}

}
