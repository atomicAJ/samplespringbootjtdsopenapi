/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataIn;
import com.candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataOut;
import com.candelalabs.api.model.WSupdateClaimDecisionDateDataIn;
import com.candelalabs.api.model.WSupdateClaimUserDataIn;
import com.candelalabs.api.model.WSupdateClaimUserDataOut;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSDNULAdjClaimMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;


@Component
@PropertySource("classpath:application.properties")
public class WSupdateClaimDecisionDate implements Processor {

	private static Log log = LogFactory.getLog("WSupdateClaimDecisionDate");

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub  
		log.info("WSupdateClaimDecisionDate -- Service is invoked");
		JsonResponse jsonObjOut = new JsonResponse();
		try {
			WSupdateClaimDecisionDateDataIn jsonObjIn = (exchange.getIn().getBody(WSupdateClaimDecisionDateDataIn.class));
			String Claimno ="";
			
			
			int result =0;
			if (jsonObjIn!=null){
				Claimno= jsonObjIn.getCLAIMNO();
				log.info("WSupdateClaimDecisionDate -- Claimno is -"+ Claimno);
				result = claimDetailsDao.updateClaimDecisionDate(Claimno);
				jsonObjOut.setWsSuccessMessage("Success");
			}
			
			//jsonObjOut.setUpdateStatus(String.valueOf(result));
			jsonObjOut.setWsExceptionMessage("");
			jsonObjOut.setWsProcessingStatus("1");
				
//			if (result==0){
//				
//					jsonObjOut.setWsSuccessMessage("");
//					jsonObjOut.setWsExceptionMessage("ClaimDecisionDateentry not updated for the given Claim Number = "+Claimno);
//					jsonObjOut.setWsProcessingStatus("2");			
//							}
			

			

		} catch (Exception e) {
			log.error("Error at processing: " + e);
			for(StackTraceElement tr :e.getStackTrace()) {
				log.error("\tat "+tr);
			}
			/*jsonObjOut.setWsExceptionMessage("Error " + e.getMessage());
			jsonObjOut.setWsProcessingStatus("2");*/
			//jsonObjOut.setUpdateStatus(null);
			jsonObjOut.setWsSuccessMessage(null);
			jsonObjOut.setWsExceptionMessage(e.getMessage());
			jsonObjOut.setWsProcessingStatus("2");
		}

		log.info("Json Response = " + jsonObjOut.toString());

		exchange.getOut().setBody(jsonObjOut);

	}

}
