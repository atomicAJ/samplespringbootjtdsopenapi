package com.candelalabs.ws.services;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDULActualClaimAmountDataIn;
import com.candelalabs.ws.FWDConstants;

//import org.apache.commons.logging.Log;
import com.candelalabs.ws.dao.*;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.*;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;

@Component
@PropertySource("classpath:application.properties")
public class WSDULActualClaimAmountProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao odsDao;
	@Value("${USRPRF}")
	public String USRPRF; // reading USRPRF's value from
							// "classpath:application.properties" file
	private static Log log = LogFactory.getLog("WSDULActualClaimAmountProcessor");// .getLog(WSUpdateClaimStatusProcessor.class);

	// below working config
	// private static final Logger log =
	// LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());
	/*
	 * @Autowired private ClaimDetailsDao updatedRemarksInClaimData;
	 */

	int updateSuccess = 0;

	public void process(Exchange exchange) throws Exception {

		log.info(" WSDULActualClaimAmountProcessor -- testing log");
		log.info(" WSDULActualClaimAmountProcessor.class.getName() --"
				+ WSDULActualClaimAmountProcessor.class.getName());
		log.info(" WSDULActualClaimAmountProcessor.class.getName() --"
				+ WSDULActualClaimAmountProcessor.class.toString());
		System.out.println("WSDULActualClaimAmountProcessor -- testing console log");
		System.out.println(WSDULActualClaimAmountProcessor.class.getName());
		System.out.println(WSDULActualClaimAmountProcessor.class.toString());

		Gson gson = new GsonBuilder().serializeNulls().create();
		WSDULActualClaimAmountDataIn jsonData = new WSDULActualClaimAmountDataIn(); // for
																					// input
																					// ->
																					// jsonData
		JsonResponse jsonoutData = new JsonResponse(); // for output ->
														// jsonoutData
		try {

			jsonData = (exchange.getIn().getBody(WSDULActualClaimAmountDataIn.class));

			PaddingUtil pdngutl = new PaddingUtil();
			if (null != jsonData) {
//				log.info("Get LaRequestTrackerModel table with claimNo= " + jsonData.getCLAIMNO());
//				LaRequestTrackerModel laRequestTrackerData = this.claimDetailsDao.getLaRequestTrackerByClaimNo(
//						jsonData.getCLAIMNO(), jsonData.getACITVITYID(),
//						FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAmount);
//				log.info("Successfully Got LaRequestTrackerModel table ");
//
//				if (laRequestTrackerData.getBoidentifier() == null && laRequestTrackerData.getMsgtosend() == null
//						&& laRequestTrackerData.getMsgcreatets() == null
//						&& laRequestTrackerData.getFuturemesg() == null) {
					ClaimDataNewModel claimsData = new ClaimDataNewModel();
					claimsData = this.claimDetailsDao.getClaimDataNew(jsonData.getCLAIMNO());
					PolicyDetails policyData = new PolicyDetails();
					String msgtosend = null;
					if (claimsData != null) {
						policyData = odsDao.getPolicyDetail(claimsData.getPolicyNumber());
						msgtosend = pdngutl.padRight(USRPRF, 9) + // from
																	// property
																	// file
								pdngutl.padZero(Integer.parseInt(policyData.getCOMPANY()), 1) +

								pdngutl.padRight(FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAmount, 20)

								+ pdngutl.padRight(FWDConstants.MESSAGE_LANGUAGE, 1)

								+ pdngutl.padZero(FWDConstants.BO_UnitLink_DeathClaimActualAmount_MESSAGE_LENGTH, 5)

								+ pdngutl.padRight(FWDConstants.MORE_INDICATOR, 1)

								+ pdngutl.padRight(claimsData.getPolicyNumber(), 10)

								+ pdngutl.padRight(jsonData.getCLAIMNO().toString(), 11);
					}


					java.sql.Date futureDate = null;
					//String boRegis = BO_IDENTIFIER_UnitLink_DeathClaimRegistration

					if (jsonData.getApproveDate() != null) {
						futureDate = Util.objToDate(jsonData.getApproveDate(), "");
					} else {
						Timestamp tsRegister = this.claimDetailsDao.getDULRegisterDateLA(jsonData.getCLAIMNO(),FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimRegistration);
						java.sql.Date tgl = new java.sql.Date(tsRegister.getTime());
						futureDate = Util.addDays(tgl, 3);
					}
					int result = claimDetailsDao.lARequestTracker_Insert(jsonData.getCLAIMNO(),
							jsonData.getACITVITYID(), FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAmount,
							msgtosend, "1", futureDate);
					if (result == 0) {
						throw new DAOException("No Updated row for LARequestTracker Table");
					}

			//	}
				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");
			}
			

			else {
				throw new Exception("Input String is Empty");
			}

			log.info("WSDULActualClaimAmountProcessor - In Method - process - getBody");

		} catch (Exception e) {

			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			// jsonoutData.setSOURCEEXCEPTIONPROCESS("Null");
			// jsonoutData.setSOURCEEXCEPTIONSTEP("Null");

		}

		String json = gson.toJson(jsonoutData);
		log.info("WSDULActualClaimAmountProcessor - In Method - process - jsonOut= " + json);
		exchange.getOut().setBody(jsonoutData);

	}

}
