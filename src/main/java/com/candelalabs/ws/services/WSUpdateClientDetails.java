package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataPortalOutput;
import com.candelalabs.api.model.JsonWSGetAgentDetailsDataIn;
import com.candelalabs.api.model.JsonWSGetAgentDetailsDataOut;
import com.candelalabs.api.model.JsonWSUpdateClientDetailsDataIn;
//import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.dao.impl.ClaimDetailsDaoImpl;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;
import java.text.ParseException;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.PolicyClientBeneficiaryAddressList;
import com.candelalabs.ws.model.tables.PolicyClientOwnerPhoneResultSet;
import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSUpdateClientDetails implements Processor {

		private static final Log log = LogFactory.getLog("WSUpdateClientDetails");
	
		@Autowired
	    ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;
		/**
		 * 
		 */
		public WSUpdateClientDetails() {
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 */
		
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			log.info("WSUpdateClientDetails - Service is invoked");
			JsonWSGetAgentDetailsDataOut response = new JsonWSGetAgentDetailsDataOut();
			JsonWSUpdateClientDetailsDataIn dataIn = exchange.getIn().getBody(JsonWSUpdateClientDetailsDataIn.class);
			
			
			 
			try {
				
				String  strPolicyClientOwnerAddress1 = "";
				String  strPolicyClientOwnerAddress2 = "";
				String  strPolicyClientOwnerAddress3 = "";
				String  strPolicyClientOwnerAddress4 = "";
				String  strPolicyClientOwnerAddress5 = "";
				String strPolicyClientOwnerEmailAddress="";
				String polNo="";
				String claimNo="";
				String strClientType="";
				String   strPolicyClientOwnerPhoneNo01="";
				String   strPolicyClientOwnerPhoneNo02="";
				String   strPolicyClientOwnerPhoneNo03="";
				String strPolicyClientOwnerPhoneNo="";
				String strPolicyClientBeneficiaryNumber="";
				String strPolicyClientBeneficiaryAddress1="";
				String strPolicyClientBeneficiaryAddress2="";
				String strPolicyClientBeneficiaryAddress3="";
				String strPolicyClientBeneficiaryAddress4="";
				String strPolicyClientBeneficiaryAddress5="";
				int i =0;
				polNo=dataIn.getStrPolicyNumber();
				claimNo =dataIn.getStrClaimNumber();
				strClientType= dataIn.getStrClientType();
				if(polNo!=""&& polNo!=null)
				{   
					if (strClientType.equalsIgnoreCase("PolicyClientOwner"))
					{
						strPolicyClientOwnerAddress1 = odsDao.getPolicyClientOwnerAddress1(polNo);
						strPolicyClientOwnerAddress2 = odsDao.getPolicyClientOwnerAddress2(polNo);
						strPolicyClientOwnerAddress3 = odsDao.getPolicyClientOwnerAddress3(polNo);
						strPolicyClientOwnerAddress4 = odsDao.getPolicyClientOwnerAddress4(polNo);
						strPolicyClientOwnerAddress5 = odsDao.getPolicyClientOwnerAddress5(polNo);
						
						
						   if (strPolicyClientOwnerAddress1 == null)
							   strPolicyClientOwnerAddress1="";
						   if (strPolicyClientOwnerAddress2 == null)
							   strPolicyClientOwnerAddress2="";
						   if (strPolicyClientOwnerAddress3 == null)
							   strPolicyClientOwnerAddress3="";
						   if (strPolicyClientOwnerAddress4 == null)
							   strPolicyClientOwnerAddress4="";
						   if (strPolicyClientOwnerAddress5 == null)
							   strPolicyClientOwnerAddress5="";
					 
					   strPolicyClientOwnerEmailAddress = odsDao.getPolicyClientOwnerEmailAddress(polNo);
					   if (strPolicyClientOwnerEmailAddress == null)
						   strPolicyClientOwnerEmailAddress="";
					 
					   PolicyClientOwnerPhoneResultSet polClientOwnerPhoneResultSet = new PolicyClientOwnerPhoneResultSet();
					   polClientOwnerPhoneResultSet = odsDao.getPolicyClientOwnerPhoneNos(polNo);
					   if (polClientOwnerPhoneResultSet!=null){
						   	 strPolicyClientOwnerPhoneNo01 = polClientOwnerPhoneResultSet.getPHONENO01();
						     strPolicyClientOwnerPhoneNo02 = polClientOwnerPhoneResultSet.getPHONENO02();
						     strPolicyClientOwnerPhoneNo03 = polClientOwnerPhoneResultSet.getPHONENO03();
						   if (strPolicyClientOwnerPhoneNo01 == null)
							   strPolicyClientOwnerPhoneNo01="";
						   if (strPolicyClientOwnerPhoneNo02 == null)
							   strPolicyClientOwnerPhoneNo02="";
						   if (strPolicyClientOwnerPhoneNo03 == null)
							   strPolicyClientOwnerPhoneNo03="";
					   }
					   if (strPolicyClientOwnerPhoneNo01.trim().equals("")){
						   if (strPolicyClientOwnerPhoneNo02.trim().equals("")){
							   if (strPolicyClientOwnerPhoneNo03.trim().equals("")){
								   strPolicyClientOwnerPhoneNo = "0";
							   }
							   else{ /// else of  if (strPolicyClientOwnerPhoneNo03.trim().equals("")){
								   strPolicyClientOwnerPhoneNo = strPolicyClientOwnerPhoneNo03;
							   }
								   
						   }
						   else {////else of  if (strPolicyClientOwnerPhoneNo02.trim().equals("")){
							       strPolicyClientOwnerPhoneNo = strPolicyClientOwnerPhoneNo02;
						   }
					   }
					   else { ////else of if (strPolicyClientOwnerPhoneNo01.trim().equals("")){
						           strPolicyClientOwnerPhoneNo = strPolicyClientOwnerPhoneNo01;
					   }
					 ///updating NotificationData Table
						i = claimDetailsDao.updateNotificationDataClientDetails(strPolicyClientOwnerAddress1,strPolicyClientOwnerAddress2,strPolicyClientOwnerAddress3,
								strPolicyClientOwnerAddress4,strPolicyClientOwnerAddress5,
								strPolicyClientOwnerEmailAddress,strPolicyClientOwnerPhoneNo,claimNo);
					}
					
					else if (strClientType.equalsIgnoreCase("PolicyClientBeneficiary"))
					{
						try
						{strPolicyClientBeneficiaryNumber = odsDao.getPolicyClientBeneficiaryNumber(polNo);
						}
						catch(DAOException er){
							strPolicyClientBeneficiaryNumber="";
						}
						PolicyClientBeneficiaryAddressList polClientBeneficiaryAddressList = new PolicyClientBeneficiaryAddressList();
						try
						{polClientBeneficiaryAddressList= odsDao.getPolicyClientBeneficiaryAddressList(strPolicyClientBeneficiaryNumber);
						}
						catch(DAOException er){
							polClientBeneficiaryAddressList=null;
						}
						if(polClientBeneficiaryAddressList!=null){
							
								if (polClientBeneficiaryAddressList.getADDRESS1()==null)
									  strPolicyClientBeneficiaryAddress1 = "";
								else 
									strPolicyClientBeneficiaryAddress1 = polClientBeneficiaryAddressList.getADDRESS1();
								
								if (polClientBeneficiaryAddressList.getADDRESS2()==null)
									  strPolicyClientBeneficiaryAddress2 = "";
								else 
									strPolicyClientBeneficiaryAddress2 = polClientBeneficiaryAddressList.getADDRESS2();
								
								if (polClientBeneficiaryAddressList.getADDRESS3()==null)
									  strPolicyClientBeneficiaryAddress3 = "";
								else 
									strPolicyClientBeneficiaryAddress3 = polClientBeneficiaryAddressList.getADDRESS3();
								
								if (polClientBeneficiaryAddressList.getADDRESS4()==null)
									  strPolicyClientBeneficiaryAddress4 = "";
								else 
									strPolicyClientBeneficiaryAddress4 = polClientBeneficiaryAddressList.getADDRESS4();
								
								if (polClientBeneficiaryAddressList.getADDRESS5()==null)
									  strPolicyClientBeneficiaryAddress5 = "";
								else 
									strPolicyClientBeneficiaryAddress5 = polClientBeneficiaryAddressList.getADDRESS5();
							}
						///updating NotificationData Table
						i= claimDetailsDao.updateNotificationDataBenDetails
								(strPolicyClientBeneficiaryAddress1, strPolicyClientBeneficiaryAddress2, strPolicyClientBeneficiaryAddress3,
										strPolicyClientBeneficiaryAddress4, strPolicyClientBeneficiaryAddress5, claimNo);
						}
				
				}
				
				response.setDecision("SUCCESS");
				response.setExceptionMessage("");
			    response.setWsProcessingStatus("1");
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
				int i = claimDetailsDao.updateNotificationFlag("Exception in retrieving the Owner / Beneficiary Client Information. "+e.getMessage(), dataIn.getStrClaimNumber());
			    response.setDecision("ERROR RESPONSE");;
			    response.setExceptionMessage("Exception in retrieving the Owner / Beneficiary Client Information");
			    response.setWsProcessingStatus("2");
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				
				
			}
			log.info("Final Response  is " + response.toString());
			log.info("response.getDecision() --"+ response.getDecision());
			log.info("response.getExceptionMessage() --"+ response.getExceptionMessage());
			log.info("response.getWsProcessingStatus() --"+ response.getWsProcessingStatus());
			exchange.getOut().setBody(response);

		}

	}
