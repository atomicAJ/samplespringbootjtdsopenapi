/*package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataPortalOutput;
import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.GetClaimDetailsList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSPortalServiceProcessor implements Processor {

		private static final Log log = LogFactory.getLog("WSPortalServiceProcessor");

		@Autowired
		ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;
		*//**
		 * 
		 *//*
		public WSPortalServiceProcessor() {
			// TODO Auto-generated constructor stub
		}

		
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			
			WSPortalServiceDataOut response = new WSPortalServiceDataOut();
			log.info("WSPortalServiceProcessor - Service is invoked");
			//WSPortalServiceDataOutClaimData claimData = new WSPortalServiceDataOutClaimData();
			List<GetClaimDetailsList> claimData = null; 
			try {
				WSPortalServiceDataIn dataIn = exchange.getIn().getBody(WSPortalServiceDataIn.class);
				log.info("WSPortalServiceProcessor - Json Object In= " + dataIn.toString());
				String strInputExceptionFlag = "N";
				String strExceptionMessage="";
				String strClaimNumber="";
				if (dataIn.getStrPortalClaimRequestNumber() == null || (dataIn.getStrPortalClaimRequestNumber().trim()) == "") 
				{
				   log.info("WSPortalServiceProcessor - StrPortalClaimRequestNumber() == null ");
				   strExceptionMessage = "Cannot Create Claim Record without Claim Request Number for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getDtPortalClaimRequestCreationDate()== null || (dataIn.getDtPortalClaimRequestCreationDate().trim() ) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Claim Request Date for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrPolicyNumber() == null || (dataIn.getStrPolicyNumber().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Policy Number for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrOwnerClientID() == null || (dataIn.getStrOwnerClientID().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Owner Client ID for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrInsuredClientID() == null || (dataIn.getStrInsuredClientID().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Insured Name for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (strInsuredClientID == null || trim(strInsuredClientID) == "") then
				{
				   strExceptionMessage = "Cannot Create Claim Record without Insured Client ID for which Claim registration request is submitted"
				   strInputExceptionFlag = "Y"
				}
				else if (dataIn.getStrFormClaimType() == null || (dataIn.getStrFormClaimType().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Claim Type for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				}
				else if (dataIn.getStrFormBenefitType() == null || (dataIn.getStrFormBenefitType().trim()) == "") 
				{
				   strExceptionMessage = "Cannot Create Claim Record without Claim Benefit Type for which Claim registration request is submitted";
				   strInputExceptionFlag = "Y";
				   
				}
				if (strInputExceptionFlag == "Y") 
				{
					log.info("Inside strInputExceptionFlag == Y before creating json response");
					dataIn.setStrPortalClaimRequestNumber("12323");
					dataIn.setDtPortalClaimRequestCreationDate( "01/10/2018");
		            dataIn.setStrPolicyNumber("123123");
		            dataIn.setStrOwnerClientID( "123123");  
		            dataIn.setStrPayeeName( "abdnbcd");
		            dataIn.setStrInsuredClientName( "abcdere");
		            dataIn.setStrInsuredClientID( "123123");
		            dataIn.setStrFormClaimType( "Living Benefit");
		            dataIn.setStrFormBenefitType( "Waiver");
		            dataIn.setStrSurgeryPerformed( "N");
		            dataIn.setDtTreatmentStartDate( "01/10/2018");
		            dataIn.setDtTreatmentCompletionDate( "01/10/2018");
		            dataIn.setStrHospitalName( "asdff");
		            dataIn.setStrCurrency( "IDR");
		            dataIn.setDblTotalBillingAmount(" 12312321.11");  
		            dataIn.setStrPhysician( "dfddf");
		            dataIn.setStrBankAccountNumber( "adf1123");
		            dataIn.setStrPaymentAccountOwnerName( "asdfasdf");
		            dataIn.setWsProcessingStatus("2");
		            dataIn.setWsExceptionMessage( strExceptionMessage);
		            dataIn.setWsSuccessMessage( "");
		            log.info("Inside strInputExceptionFlag == Y after creating json response");
				}

				else
				{
					log.info("Inside else of strInputExceptionFlag == Y before insert into CLAIMDATASOURCEPORTAL");
					int inserted =0;
					inserted = claimDetailsDao.insertCLAIMDATASOURCEPORTAL(dataIn.getStrPortalClaimRequestNumber(),
							Util.objToDate(dataIn.getDtPortalClaimRequestCreationDate(),"dd/mm/yyyy"), 
							dataIn.getStrPolicyNumber(),
							dataIn.getStrOwnerClientID(),
							dataIn.getStrPayeeName(), 
							dataIn.getStrInsuredClientName(),
							dataIn.getStrInsuredClientID(),
							dataIn.getStrFormClaimType(), 
							dataIn.getStrFormBenefitType(), dataIn.getStrSurgeryPerformed(), 
							Util.objToDate(dataIn.getDtTreatmentStartDate(),"dd/mm/yyyy"), 
							Util.objToDate(dataIn.getDtTreatmentCompletionDate(),"dd/mm/yyyy"),
							dataIn.getStrHospitalName(), 
							dataIn.getStrCurrency(),//.getStrFormCurrency(),
							new BigDecimal(dataIn.getDblTotalBillingAmount()),
						    dataIn.getStrPhysician(), dataIn.getStrBankAccountNumber(), dataIn.getStrPaymentAccountOwnerName(), "N");
					log.info("Inside else of strInputExceptionFlag == Y After insert into CLAIMDATASOURCEPORTAL");
					if (inserted>0)
					{
						log.info("One record inserted into CLAIMDATASOURCEPORTAL, Before setting response  to Poratl");
						dataIn.setStrPortalClaimRequestNumber("12323");
						dataIn.setDtPortalClaimRequestCreationDate( "01/10/2018");
			            dataIn.setStrPolicyNumber("123123");
			            dataIn.setStrOwnerClientID( "123123");  
			            dataIn.setStrPayeeName( "abdnbcd");
			            dataIn.setStrInsuredClientName( "abcdere");
			            dataIn.setStrInsuredClientID( "123123");
			            dataIn.setStrFormClaimType( "Living Benefit");
			            dataIn.setStrFormBenefitType( "Waiver");
			            dataIn.setStrSurgeryPerformed( "N");
			            dataIn.setDtTreatmentStartDate( "01/10/2018");
			            dataIn.setDtTreatmentCompletionDate( "01/10/2018");
			            dataIn.setStrHospitalName( "asdff");
			            dataIn.setStrCurrency( "IDR");
			            dataIn.setDblTotalBillingAmount("12312321.11");  
			            dataIn.setStrPhysician( "dfddf");
			            dataIn.setStrBankAccountNumber( "adf1123");
			            dataIn.setStrPaymentAccountOwnerName( "asdfasdf");
			            dataIn.setWsProcessingStatus("1");
			            dataIn.setWsExceptionMessage( "");
			            dataIn.setWsSuccessMessage( "Claim Request Accepted in the System");
			            log.info("One record inserted into CLAIMDATASOURCEPORTAL,After setting response  to Poratl");
					}
					else
					{
						log.info("No records inserted into CLAIMDATASOURCEPORTAL !!!");
						String strOwnerClientID = "";
						strOwnerClientID=dataIn.getStrOwnerClientID();
					
						int recordCount = 0;
						recordCount = claimDetailsDao.countgetClaimDetails(strOwnerClientID);
						if (recordCount==0)
						{
							claimData.setDtRegisterDate("");
							claimData.setStrClaimNumbe("");
							claimData.setStrClaimStatus("");
							claimData.setStrClaimTypeLA("");
							claimData.setStrInsuredClientID("");
							claimData.setStrPolicyNumber("");
							claimData.setStrPortalClaimRequestNumbe("");
							
							response.setClaimData(claimData);
							response.setWsExceptionMessage("");
							response.setWsProcessingStatus("1");
							response.setWsSuccessMessage("There are no Claim History for any policies belonging to Owner");
						}
						else
						{
							List<WSPortalServiceDataOutClaimData> claimDatarcvd = null;
							log.info("before fetching getClaimDetails()");
							claimDatarcvd =claimDetailsDao.getClaimDetails(strOwnerClientID);
							log.info("after fetching getClaimDetails()");
							if (claimDatarcvd!=null){
								log.info("claimDatarcvd!=null");
								for (WSPortalServiceDataOutClaimData temp : claimDatarcvd) {
									log.info("in for loop temp.getDtRegisterDate() is -"+ temp.getDtRegisterDate());
									claimData.setDtRegisterDate(temp.getDtRegisterDate());
									log.info("in for loop temp.getStrClaimNumbe() is -"+ temp.getStrClaimNumbe());
									claimData.setStrClaimNumbe(temp.getStrClaimNumbe());
									strClaimNumber = temp.getStrClaimNumbe();
									log.info("in for loop temp.getStrClaimStatus() is -"+ temp.getStrClaimStatus());
									claimData.setStrClaimStatus(temp.getStrClaimStatus());
									log.info("in for loop temp.getStrClaimTypeLA() is -"+ temp.getStrClaimTypeLA());
									claimData.setStrClaimTypeLA(temp.getStrClaimTypeLA());
									log.info("in for loop temp.getStrInsuredClientID() is -"+ temp.getStrInsuredClientID());
									claimData.setStrInsuredClientID(temp.getStrInsuredClientID());
									log.info("in for loop temp.getStrPolicyNumber() is -"+ temp.getStrPolicyNumber());
									claimData.setStrPolicyNumber(temp.getStrPolicyNumber());
									log.info("in for loop temp.getStrPortalClaimRequestNumbe() is -"+ temp.getStrPortalClaimRequestNumbe());
									claimData.setStrPortalClaimRequestNumbe(temp.getStrPortalClaimRequestNumbe());
								}
								response.setClaimData(claimData);
								response.setWsExceptionMessage("");
								response.setWsProcessingStatus("1");
								response.setWsSuccessMessage("");
							}
							else
							{
								log.info("claimDatarcvd==null   -- Cannot retrieve the Claim History for the given Owner at the current moment");
								claimData.setDtRegisterDate("");
								claimData.setStrClaimNumbe("");
								claimData.setStrClaimStatus("");
								claimData.setStrClaimTypeLA("");
								claimData.setStrInsuredClientID("");
								claimData.setStrPolicyNumber("");
								claimData.setStrPortalClaimRequestNumbe("");
								
								response.setClaimData(claimData);
								response.setWsExceptionMessage("");
								response.setWsProcessingStatus("2");
								response.setWsSuccessMessage("Cannot retrieve the Claim History for the given Owner at the current moment");
							}
						   
						}
						
						
						
					}
					///getClaimData	ClaimDataPortalOutput
					log.info("before getClaimData");
					ClaimDataPortalOutput claimDataPortalOutput = null;
					claimDataPortalOutput = new ClaimDataPortalOutput();
					ClaimDataNewModel claimDataOutput = null;
					claimDataOutput = new ClaimDataNewModel();
					log.info("before calling getClaimDataForPortal strClaimNumber is - "+ strClaimNumber);
					//claimDataPortalOutput= claimDetailsDao.getClaimDataForPortal(strClaimNumber);
					claimDataOutput = claimDetailsDao.getClaimDataNew(strClaimNumber);
					
					log.info("after  calling getClaimDataForPortal strClaimNumber is - "+ strClaimNumber);
					if (claimDataOutput!=null)
					{
						String strPortalClaimRequestNumber = "";
						strPortalClaimRequestNumber = claimDataOutput.getPortalClaimRequestNumber();
						strClaimNumber = claimDataOutput.getClaimNumber();
						String strPolicyNumber ="";
						strPolicyNumber = claimDataOutput.getPolicyNumber();
						String strClaimTypeLA ="";
						strClaimTypeLA = claimDataOutput.getClaimTypeLA();
						String  strClaimStatus ="";
						strClaimStatus = claimDataOutput.getClaimStatus();
						String strClaimSubStatus ="";
						strClaimSubStatus = claimDataOutput.getClaimDecision();
						String strSurgeryPerformed ="";
						strSurgeryPerformed = claimDataOutput.getSurgreryPerformed();
						Date dtAdmissionDate ;
						dtAdmissionDate = claimDataOutput.getAdmissionDate();
						Date	dtDischargeDate;
						dtDischargeDate = claimDataOutput.getDischargeDate();
						String strHospitalName ="";
						strHospitalName = claimDataOutput.getProviderName();
						String	strDoctorName ="";
						strDoctorName = claimDataOutput.getDoctorName();
						BigDecimal ClaimApprovedAmountUI = null;
						BigDecimal  dblTotalBilling = null;
						ClaimApprovedAmountUI = claimDataOutput.getClaimApprovedAmountUI();
						if ((ClaimApprovedAmountUI==null)||(ClaimApprovedAmountUI == BigDecimal.ZERO)){
							dblTotalBilling = claimDataOutput.getClaimInvoiceAmount();
						}
						else{
							dblTotalBilling = ClaimApprovedAmountUI;
						}
						String strTotalBillingCurrency = "";
					    String strLifeInsuredName =  "";
						String strInsuredClientID =  "";
						String strAccountNumber = "";
						String strAccountHolderName =  "";
						String strBankName = "";
						Date dtClaimSubmissionDate =  null;
						Date dtModifiedDate = null;
						strTotalBillingCurrency = claimDataOutput.getClaimCurrency();
						strLifeInsuredName= claimDataOutput.getInsuredClientName();
						strInsuredClientID = claimDataOutput.getInsuredClientID();
						strAccountNumber = claimDataOutput.getBankAccountNumber();
						strAccountHolderName = claimDataOutput.getBankAccountName();
						strBankName = claimDataOutput.getBankCode();
						dtClaimSubmissionDate = claimDataOutput.getRegisterDate();
						String  strClaimProcessingReason ="";
						if (strClaimStatus.equalsIgnoreCase("PENDING")){
							
							   strClaimProcessingReason = "Pending Requirement from customer";
						}
						else if (strClaimStatus .equalsIgnoreCase("COMPLETED"))
							{
							   //// strClaimSubStatus have the value from claim Decision so using strClaimSubStatus instead of strClaimDecision ////
							   if (strClaimSubStatus.equalsIgnoreCase("REJECT"))
							   {
								   ///<fetch data from ProcessCasesheet as per the same logic used in GenerateFile and Notification Modules>
								   UtilitiesServiceImpl UtilitiesServic = new UtilitiesServiceImpl();
									DsProcessCaseSheet df = UtilitiesServic.getCommentsByActivity(ACTIVITYNAME, PROCESSNAME, CLAIMNUMBER);
									String processCaseSheetComments = df.getComments();
							
								   strClaimProcessingReason = processCaseSheetComments;
							   }
							   else
							   {
							      strClaimProcessingReason = "";
							   }
							}
							
					}
					/////Retrieve Requirement Data//////////
					ClaimDataPortalOutputDocumentList doclst = new ClaimDataPortalOutputDocumentList();
					ClaimDataPortalOutput claimDataPortalOtpt = null;
					claimDataPortalOtpt = new ClaimDataPortalOutput();
					List<ClaimDataPortalOutputDocumentList> reqDocList = null;
					
					log.info("before calling getRequirementDocList ");
					reqDocList = claimDetailsDao.getRequirementDocList(strClaimNumber);
					log.info("after calling getRequirementDocList "); 
					
					if(reqDocList!=null){
						log.info("reqDocList is not null");
						for (ClaimDataPortalOutputDocumentList doclist : reqDocList) {
							String docid ="";
							docid = doclist.getStrDocID();
							doclst.setStrDocID(docid);
							doclst.setStrDocReceiveStatus(doclist.getStrDocReceiveStatus());
							String subCategory="";
							log.info("in for loop b4 calling getSubCategory ");
							subCategory = odsDao.getSubCategory(docid);
							log.info("in for loop after calling getSubCategory ");
							doclst.setStrSubCategory(subCategory);
							claimDataPortalOtpt.setDocumentList(doclst);
						}
					}
					else{
						log.info("reqDocList is  null");
						doclst.setStrDocID("");
						doclst.setStrDocReceiveStatus("");
						doclst.setStrSubCategory("");
						claimDataPortalOtpt.setDocumentList(doclst);
					}
					//////////////////strPolicyNumber Retrieve Beneficiary Data//////////////////////////
					ClaimDataPortalOutputBeneficiaryList portalbenlist = new ClaimDataPortalOutputBeneficiaryList();
					List<BENEFICIARYLIST> benList = null;
					BENEFICIARYCLIENTINFO benclientinfo = new BENEFICIARYCLIENTINFO ();
					log.info("b4 calling odsDao.getBENEFICIARYLIST ");
					benList =  odsDao.getBENEFICIARYLIST(dataIn.getStrPolicyNumber());
					log.info("after calling odsDao.getBENEFICIARYLIST ");
					if(benList!=null){
						log.info("benList!=null ");
						for (BENEFICIARYLIST benfitlist : benList) {
							String strBeneficiaryClientNo ="";
							strBeneficiaryClientNo = benfitlist.getCLIENTNO();
							portalbenlist.setStrBeneficiaryClientNo(strBeneficiaryClientNo);
							
							String strBeneficiaryClientName ="";
							strBeneficiaryClientName = benfitlist.getCLIENTNAME();
							portalbenlist.setStrBeneficiaryClientName(strBeneficiaryClientName);
							
							String strBeneficiaryRelationship ="";
							strBeneficiaryRelationship = benfitlist.getRELATIONSHIP();
							portalbenlist.setStrBeneficiaryRelationship(strBeneficiaryRelationship);
							
							String strBenefitPercentage ="";
							strBenefitPercentage = benfitlist.getPERCENTAGE();
							portalbenlist.setStrBenefitPercentage(strBenefitPercentage);
							
							log.info("b4 calling odsDao.getBENEFICIARYCLIENTINFO ");
							benclientinfo =  odsDao.getBENEFICIARYCLIENTINFO(strBeneficiaryClientNo);
							log.info("after calling odsDao.getBENEFICIARYCLIENTINFO ");
							
							String strBeneficiaryIDNumber="";
							String strBeneficiaryGender ="";
							Date dtDateOfBirth=null;
							if(benclientinfo!=null){
								log.info("benclientinfo!=null ");
								strBeneficiaryIDNumber = benclientinfo.getIDNUMBER();
								strBeneficiaryGender = benclientinfo.getSEX();
								dtDateOfBirth = benclientinfo.getDOB();
							}
							portalbenlist.setStrBeneficiaryGender(strBeneficiaryGender);
							portalbenlist.setStrBeneficiaryIDNumber(strBeneficiaryIDNumber);
							portalbenlist.setDtDateOfBirth(dtDateOfBirth.toString());
							
							claimDataPortalOtpt.setBeneficiaryList(portalbenlist);
						}
					}
					else{
						log.info("benList is null ");
						portalbenlist.setStrBeneficiaryClientNo("");
						portalbenlist.setStrBeneficiaryClientName("");
						portalbenlist.setStrBeneficiaryRelationship("");
						portalbenlist.setStrBenefitPercentage("");
						portalbenlist.setStrBeneficiaryGender("");
						portalbenlist.setStrBeneficiaryIDNumber("");
						portalbenlist.setDtDateOfBirth("");
						claimDataPortalOtpt.setBeneficiaryList(portalbenlist);
					}
					response.setClaimData(claimData);
					response.setWsExceptionMessage("");
					response.setWsProcessingStatus("");
					response.setWsSuccessMessage("");
				}
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				claimData.setDtRegisterDate("");
				claimData.setStrClaimNumbe("");
				claimData.setStrClaimStatus("");
				claimData.setStrClaimTypeLA("");
				claimData.setStrInsuredClientID("");
				claimData.setStrPolicyNumber("");
				claimData.setStrPortalClaimRequestNumbe("");
				
				response.setClaimData(claimData);
				response.setWsExceptionMessage("");
				response.setWsProcessingStatus("2");
				response.setWsSuccessMessage("Error Response");
			
			}
			log.info("Final Response " + response.toString());

			exchange.getOut().setBody(response);

		}

	}
*/