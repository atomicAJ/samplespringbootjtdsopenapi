package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.impl.Log4JLogger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

//import org.apache.commons.logging.Log;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.api.model.*;
//import com.candelalabs.ws.schema.stubs.*;
//import com.candelalabs.ws.schema.stubs.WSUpdateClaimStatusResponseType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class WSChkExcSrcProcessor implements Processor {
	
	@Autowired
	//ClaimDetailsDao claimDetailsDao;
    private static Log log = LogFactory.getLog("WSChkExcSrcProcessor");//.getLog(WSUpdateClaimStatusProcessor.class);
	 
	//below working config
	//private static final Logger log = LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());
/*	@Autowired
	private ClaimDetailsDao updatedRemarksInClaimData;*/

	int updateSuccess = 0;

	public void process(Exchange exchange) throws Exception {
		
		   log.info(" WSChkExcSrcProcessor -- testing log");
		   log.info(" WSChkExcSrcProcessor.class.getName() --"+ WSChkExcSrcProcessor.class.getName());
		   log.info(" WSChkExcSrcProcessor.class.getName() --"+ WSChkExcSrcProcessor.class.toString());
		   System.out.println("WSChkExcSrcProcessor -- testing console log");
		   System.out.println(WSChkExcSrcProcessor.class.getName());
		   System.out.println(WSChkExcSrcProcessor.class.toString());
			Gson gson = new GsonBuilder().serializeNulls().create();
			JsonWSChkExcSrcProcessorDataIn jsonData = new JsonWSChkExcSrcProcessorDataIn();   // for input -> jsonData
			JsonWSChkExcSrcProcessorDataOut jsonoutData = new JsonWSChkExcSrcProcessorDataOut(); // for output -> jsonoutData
			try {

				
				/*String jsonInString = (exchange.getIn().getBody(WSChkExcSrcRequestType.class).getWSInputObject());
				jsonData = gson.fromJson(jsonInString, JsonWSChkExcSrcProcessorDataIn.class); //converting json string to class object
*/
				jsonData = (exchange.getIn()
						.getBody(JsonWSChkExcSrcProcessorDataIn.class));
				if (null != jsonData)
				{
					jsonoutData.setProcessmsg("Success");
					jsonoutData.setDecision("Success");
					jsonoutData.setSOURCEEXCEPTIONPROCESS(jsonData.getSOURCEEXCEPTIONPROCESS().toString());
					System.out.println("jsonData.getSOURCEEXCEPTIONPROCESS().toString()   " + jsonData.getSOURCEEXCEPTIONPROCESS().toString());
					jsonoutData.setSOURCEEXCEPTIONSTEP(jsonData.getSOURCEEXCEPTIONSTEP().toString());
					System.out.println("jsonData.getSOURCEEXCEPTIONSTEP().toString()  " + jsonData.getSOURCEEXCEPTIONSTEP().toString());
				}
				else
					
					throw new Exception();

				

				log.info("WSChkExcSrcProcessor - In Method - process - getBody");

			} catch (Exception e) {
				e.printStackTrace();
				jsonoutData.setProcessmsg("Failure" + e);
				jsonoutData.setDecision("ERROR RESPONSE");
				jsonoutData.setSOURCEEXCEPTIONPROCESS("Null");
				jsonoutData.setSOURCEEXCEPTIONSTEP("Null");

			}

			/*String json = gson.toJson(jsonoutData);
			WSChkExcSrcResponseType requirementResponse = new WSChkExcSrcResponseType();
			requirementResponse.setWSOutputObject(json);*/ //
			exchange.getOut().setBody(jsonoutData);

		}
	
	}

