/**
 * 
 */
package com.candelalabs.ws.services;

import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.awpl.ecm.api.AWDocumentstoreException_Exception;
import com.awpl.ecm.api.AWInvalidPasswordException_Exception;
import com.awpl.ecm.api.AwdItem;
import com.awpl.ecm.api.AwdItem.ItemInfo;
import com.awpl.ecm.api.AwdItem.ItemInfo.Entry;
import com.awpl.ecm.api.ContentEngine;
import com.awpl.ecm.api.HashtableEntry;
import com.awpl.ecm.server.ContentEngineImplService;
import com.candelalabs.api.model.DataInDocReindex;
import com.candelalabs.api.model.DataInDocReindexWSInput;
import com.candelalabs.api.model.DataInDocReindexWSInputDocumentSearchInfo;
import com.candelalabs.api.model.DataInDocReindexWSInputDocumentUpdateInfo;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.ws.services.impl.CasepediaService;

/**
 * @author Triaji
 *
 */
@Component
public class WSDocReIndexProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSDocReIndexProcessor");

	@Autowired
	private CasepediaService casepediaService;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse response = new JsonResponse();
		try {
			DataInDocReindex wsIn = exchange.getIn().getBody(DataInDocReindex.class);
			if (wsIn != null) {
				DataInDocReindexWSInput dataIn = wsIn.getWsInput();
				log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
				DataInDocReindexWSInputDocumentSearchInfo searchData = dataIn.getDocumentSearchInfo();
				DataInDocReindexWSInputDocumentUpdateInfo updateData = dataIn.getDocumentUpdateInfo();
				if (searchData != null) {
					Map<String, String> inputMap = new HashMap<String, String>();
					if (searchData.getClaimNumber() != null
							&& !searchData.getClaimNumber().trim().equalsIgnoreCase("")) {
						inputMap.put("ClaimNumber", searchData.getClaimNumber());
					}
					if (searchData.getPolicyNumber() != null
							&& !searchData.getPolicyNumber().trim().equalsIgnoreCase("")) {
						inputMap.put("PolicyNumber", searchData.getPolicyNumber());
					}
					if (searchData.getTpAClaimNo() != null && !searchData.getTpAClaimNo().trim().equalsIgnoreCase("")) {
						inputMap.put("TPAClaimNo", searchData.getTpAClaimNo());
					}
					if (searchData.getBatchNumber() != null
							&& !searchData.getBatchNumber().trim().equalsIgnoreCase("")) {
						inputMap.put("BatchNumber", searchData.getBatchNumber());
					}
					if (searchData.getPortalClaimRequestNumber() != null
							&& !searchData.getPortalClaimRequestNumber().trim().equalsIgnoreCase("")) {
						inputMap.put("PortalClaimRequestNumber", searchData.getPortalClaimRequestNumber());
					}

					String searchString = formulateSearchString(inputMap);
					log.info("Formulated Search String = " + searchString);

					if (searchString != null && searchString.trim() != "") {
						ContentEngine ce = null;

						log.info("Connecting to Casepedia");
						ce = this.casepediaService.connectToCasePpedia();
						log.info("Successfully connecting to Casepedia");
						log.info("Search Query in Casepedia");
						List<AwdItem> awdItem_li = this.casepediaService.searchQueryCasepedia(ce, searchString, log);
						log.info("Successfully got the AwdItem List from search with size = " + awdItem_li.size());
						Map<String, String> updateMap = new HashMap<String, String>();
						if (updateData != null) {
							if (updateData.getBatchNumber() != null && updateData.getBatchNumber().trim() != "") {
								updateMap.put("BatchNumber", updateData.getBatchNumber());
							}
							if (updateData.getClaimNumber() != null && updateData.getClaimNumber().trim() != "") {
								updateMap.put("ClaimNumber", updateData.getClaimNumber());
							}
							if (updateData.getPolicyNumber() != null && updateData.getPolicyNumber().trim() != "") {
								updateMap.put("PolicyNumber", updateData.getPolicyNumber());
							}
							if (updateData.getPortalClaimRequestNumber() != null
									&& updateData.getPortalClaimRequestNumber().trim() != "") {
								updateMap.put("PortalClaimRequestNumber", updateData.getPortalClaimRequestNumber());
							}
							if (updateData.getTpAClaimNo() != null && updateData.getTpAClaimNo().trim() != "") {
								updateMap.put("TPAClaimNo", updateData.getTpAClaimNo());
							}

						}

						if (awdItem_li != null && awdItem_li.size() > 0) {
							if (updateMap != null && updateMap.size() > 0) {
								for (AwdItem awdItem : awdItem_li) {
									String docId = awdItem.getIdentifier();
									log.info("docId = " + docId);
									List<HashtableEntry> he_li = new ArrayList<HashtableEntry>();
									for (Map.Entry<String, String> entry : updateMap.entrySet()) {
										HashtableEntry he = new HashtableEntry();
										he.setKey(entry.getKey());
										he.setValue(entry.getValue());
										he_li.add(he);
									}
									log.info("Updating Atrribute");
									this.casepediaService.updateAndSetAttribute(docId, ce, he_li);
									log.info("Successfully updated the attribute");
								}
							}
						}
						log.info("Successfully doing the DocReindexProcess");
						ce.disconnect();
						response.setWsProcessingStatus("1");
						response.setWsSuccessMessage("Successfully reindex the Document(s)");

					} else {
						throw new Exception("Formulated Search String is null");
					}

				}

			} else {
				throw new Exception("Request Input Message is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error " + e.getClass().getSimpleName() + " " + e.getMessage());
			log.info("Error: ", e);
			log.info("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.info("\tat " + tr);
			}

		}
		// response.setWsProcessingStatus("1");
		// response.setWsSuccessMessage("Success");
		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);
	}

	private String formulateSearchString(Map<String, String> inputMap) {

		String result = "";
		int i = 0;
		if (inputMap.size() > 0) {
			for (Map.Entry<String, String> entry : inputMap.entrySet()) {
				if (entry.getValue() != null && entry.getValue().trim() != "") {
					result = result + wrapTheString(entry.getKey(), entry.getValue());
					if (i < inputMap.size() - 1) {
						result = result + " AND ";
					}
				}
				i = i + 1;
			}
		}
		return result;
	}

	private String wrapTheString(String inputkey, String inputValue) {
		String inputResult = "";
		if (inputValue != null && inputValue.trim() != "") {
			inputResult = "@" + inputkey + "[=](" + inputValue + ") ";
		}

		return inputResult;
	}

	public static void main(String[] args) throws MalformedURLException {
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication
				// (claimDetailsDao.getEmailConfig().get(ClaimDetailsConstants.CASEPEDIA_USER_NAME),getCasepediaPassword().toCharArray());
				("user1", "tes".toCharArray());
			}
		});

		URL url = new URL("http://10.17.60.16:8080/ecm-web/services/ECM?wsdl");
		ContentEngineImplService contentEngineImplService = new ContentEngineImplService(url);

		ContentEngine ce = contentEngineImplService.getContentEngineImplPort();
		try {
			ce.connect("idn_full_user1", "12");
			List<AwdItem> awdItem = ce.searchDocuments("LooseMails",
					"@ApplicationNumber[=](12345) AND @PolicyNumber[=](70891)");
			ListIterator<AwdItem> iter = awdItem.listIterator();
			while (iter.hasNext()) {
				// Fetch Document Object and print document properties
				AwdItem item = (AwdItem) iter.next();
				ItemInfo itemInfo = item.getItemInfo();
				List<Entry> entries = itemInfo.getEntry();
				ListIterator<Entry> entryIter = entries.listIterator();
				while (entryIter.hasNext()) {
					Entry entry = entryIter.next();
					System.out.println("Entry: " + entry.getKey() + "::" + entry.getValue());
				}
			}

		} catch (AWInvalidPasswordException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AWDocumentstoreException_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Connected");

	}

}
