/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSGENRegClaimDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSCLRegClaimRecuringMessageModel;
import com.candelalabs.ws.model.bomessages.WSGENRegCITermBOMessage;
import com.candelalabs.ws.model.bomessages.WSGENRegClaimADDBMessageModel;
import com.candelalabs.ws.model.bomessages.WSGENRegClaimADDBRecurringMessageModel;
import com.candelalabs.ws.model.bomessages.WSGENRegClaimHSHCMessageModel;
import com.candelalabs.ws.model.bomessages.WSGENRegClaimHSHCRecuringMessageModel;
import com.candelalabs.ws.model.bomessages.WSGENRegClaimWPMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.HospitalMasterDetails;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSGENRegClaimProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSGENRegClaimProcessor");

	@Value("${USRPRF}")
	private String USRPRF;

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		WSGENRegClaimDataIn dataIn = new WSGENRegClaimDataIn();
		JsonResponse response = new JsonResponse();
		log.info("********************************************************************************* \n"
				+ "Service is invoked");
		try {
			PaddingUtil paddingUtil = new PaddingUtil();

			dataIn = exchange.getIn().getBody(WSGENRegClaimDataIn.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());

			if (dataIn.getClaimNo() != null && dataIn.getClaimNo() != "" && dataIn.getActivityId() != null
					&& dataIn.getActivityId() != "") {
				log.info("Get ClaimData table with claimNo= " + dataIn.getClaimNo());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getClaimNo());
				log.info("Successfully retrieved ClaimData table");

				if (!Optional.ofNullable(claimData).isPresent()) {
					throw new DAOException("CLAIMDATA table not found for claimNumber= " + dataIn.getClaimNo());
				}

				if (claimData.getClaimTypeUI().equalsIgnoreCase("HSR")
						|| claimData.getClaimTypeUI().equalsIgnoreCase("HC")) {
					WSGENRegClaimHSHCMessageModel boMessage = new WSGENRegClaimHSHCMessageModel();
					boMessage.setUSRPRF(this.USRPRF);

					// log.info("Get LaRequestTrackerModel table with claimNo= "
					// + dataIn.getClaimNo());
					// LaRequestTrackerModel laRequestTrackerData =
					// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
					// dataIn.getClaimNo(), dataIn.getActivityId(),
					// boMessage.getBOIDEN());
					// log.info("Successfully Got LaRequestTrackerModel table
					// ");
					//
					// if (laRequestTrackerData.getBoidentifier() == null &&
					// laRequestTrackerData.getMsgtosend() == null
					// && laRequestTrackerData.getMsgcreatets() == null
					// && laRequestTrackerData.getFuturemesg() == null) {

					log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully retrieved PolicyDetail table");

					if (!(Optional.ofNullable(claimData).isPresent()
							&& Optional.ofNullable(policyDetail).isPresent())) {
						throw new DAOException("ClaimData is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
					boMessage.setCLAIMNO(dataIn.getClaimNo());
					log.debug("CLAIMNO=" + dataIn.getClaimNo());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					log.debug("CHDRSEL=" + claimData.getPolicyNumber());

					log.info("Get ComponentDetails table");
					ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
							claimData.getComponentCode(), claimData.getInsuredClientID());
					log.info("Successfully retrieved ComponentDetails table");

					if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
						throw new DAOException("componentDetailsBO is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setLIFE(componentDetailsBO.getLife());
					log.debug("LIFE=" + componentDetailsBO.getLife());

					boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
					log.debug("COVERAGE=" + componentDetailsBO.getCoverage());

					boMessage.setRIDER(componentDetailsBO.getRider());
					log.debug("RIDER=" + componentDetailsBO.getRider());

					boMessage.setCRTABLE(claimData.getComponentCode());
					log.debug("CRTABLE=" + claimData.getComponentCode());

					boMessage.setCLAMPARTY(claimData.getInsuredClientID());
					log.debug("CLAMPARTY=" + claimData.getInsuredClientID());

					boMessage.setCLTYPE(claimData.getClaimTypeLA());
					log.debug("CLTYPE=" + claimData.getClaimTypeLA());

					boMessage.setZDIAGCDE(claimData.getDiagnosisCode());
					log.debug("ZDIAGCDE=" + claimData.getDiagnosisCode());

					log.info("Get HospitalMaster table with providercode= " + claimData.getProviderCode());
					HospitalMasterDetails hospitalMaster = odsDao.getHospitalMasterDetails(claimData.getProviderCode());
					log.info("Successfully retrieved HospitalMaster table");
					if (!(Optional.ofNullable(hospitalMaster).isPresent())) {
						throw new DAOException(
								"hospitalMaster is null with ProviderCode= " + claimData.getProviderCode());
					}
					;

					boMessage.setZMEDPRV(claimData.getProviderCode());
					log.debug("ZMEDPRV=" + claimData.getProviderCode());

					boMessage.setCLAIMCUR(claimData.getClaimCurrency());
					log.debug("CLAIMCUR=" + claimData.getClaimCurrency());

					boMessage.setPAYCLT(hospitalMaster.getPROVIDERCLIENTNUMBER());
					log.debug("PAYCLT=" + hospitalMaster.getPROVIDERCLIENTNUMBER());

					boMessage.setBANKKEY(claimData.getBankCode());
					log.debug("BANKKEY=" + claimData.getBankCode());

					boMessage.setFACTHOUS(claimData.getFactoringHouse());
					log.debug("FACTHOUS=" + claimData.getFactoringHouse());

					boMessage.setBANKACOUNT(claimData.getBankAccountNumber());
					log.debug("BANKACOUNT=" + claimData.getBankAccountNumber());

					boMessage.setBANKACCDSC(claimData.getBankAccountName());
					log.debug("BANKACCDSC=" + claimData.getBankAccountName());

					boMessage.setBNKACTYP(claimData.getBankType());
					log.debug("BNKACTYP=" + paddingUtil.Type(boMessage.getBANKKEY()));

					boMessage.setZCLMRECD(Util.dateToString(claimData.getReceivedDate(), "yyyyMMdd"));
					log.debug("ZCLMRECD=" + Util.dateToString(claimData.getReceivedDate(), "yyyyMMdd"));

					boMessage.setINCURDT(Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));
					log.debug("INCURDT=" + Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));

					boMessage.setDISCHDT(Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));
					log.debug("DISCHDT=" + Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));

					log.info("Get claimDataDetails table with claimNo= " + dataIn.getClaimNo());
					List<ClaimDataDetailsModel> claimDataDetails_li = new ArrayList<ClaimDataDetailsModel>();
					claimDataDetails_li = this.claimDetailsDao.getClaimDataDetailsByClaimNo(dataIn.getClaimNo());
					log.info("Successfully retrieved claimDataDetails table");
					if (claimDataDetails_li.size() <= 0) {
						throw new DAOException("ClaimDataDetails is null with claimNo= " + dataIn.getClaimNo());
					}
					;

					List<WSGENRegClaimHSHCRecuringMessageModel> WSGenReg_li = new ArrayList<WSGENRegClaimHSHCRecuringMessageModel>();

					int count = 0;
					for (ClaimDataDetailsModel claimDataDetails : claimDataDetails_li) {
						WSGENRegClaimHSHCRecuringMessageModel model = new WSGENRegClaimHSHCRecuringMessageModel();
						model.setHOSBEN(claimDataDetails.getBenefitCode());
						log.debug("HOSBEN=" + claimDataDetails.getBenefitCode());
						model.setDATEFRM(Util.dateToString(claimDataDetails.getBenefitFromDate(), "yyyyMMdd"));
						log.debug("DATEFRM=" + Util.dateToString(claimDataDetails.getBenefitFromDate(), "yyyyMMdd"));
						model.setDATETO(Util.dateToString(claimDataDetails.getBenefitToDate(), "yyyyMMdd"));
						log.debug("DATEtO=" + Util.dateToString(claimDataDetails.getBenefitToDate(), "yyyyMMdd"));
						// model.setACTEXP((claimDataDetails.getBenefitAmountPerDay()
						// == null
						// ||
						// (claimDataDetails.getBenefitAmountPerDay().compareTo(new
						// BigDecimal(0)) == 0))
						// ? claimDataDetails.getBenefitInvoiceAmount()
						// : claimDataDetails.getBenefitAmountPerDay());
						// log.debug("ACTEXP=" +
						// claimDataDetails.getBenefitAmountPerDay());
						model.setACTEXP(claimDataDetails.getBenefitAmountPerDay());
						log.debug("ACTEXP=" + claimDataDetails.getBenefitAmountPerDay());

						model.setZNODAY(claimDataDetails.getBenefitActualDays() == null ? new BigDecimal(0)
								: claimDataDetails.getBenefitActualDays());
						log.debug("ZNODAY=" + claimDataDetails.getBenefitActualDays());

						model.setTACTEXP(claimDataDetails.getBenefitTotalIncurredAmount() == null ? new BigDecimal(0)
								: claimDataDetails.getBenefitTotalIncurredAmount());
						log.debug("TACTEXP=" + claimDataDetails.getBenefitTotalIncurredAmount());

						model.setGCNETPY(claimDataDetails.getBenefitUserApprovedAmount() == null ? new BigDecimal(0)
								: claimDataDetails.getBenefitUserApprovedAmount());
						log.debug("GCNETPY=" + claimDataDetails.getBenefitUserApprovedAmount());

						WSGenReg_li.add(model);
						count = count + 1;
					}

					if (count < 28) {
						for (int t = 0; t < (28 - count); t++) {
							WSGENRegClaimHSHCRecuringMessageModel model = new WSGENRegClaimHSHCRecuringMessageModel();
							model.setHOSBEN("");
							model.setDATEFRM("");
							model.setDATETO("");
							model.setACTEXP(new BigDecimal(0));
							model.setZNODAY(new BigDecimal(0));
							model.setTACTEXP(new BigDecimal(0));
							model.setGCNETPY(new BigDecimal(0));
							WSGenReg_li.add(model);
						}
					}

					boMessage.setwSCLRegClaimRecuring_li(WSGenReg_li);

					updateNotification(dataIn.getClaimNo());

					log.info("BOMESSAGE RESULT = " + boMessage.getBOMessage());
					log.info("Inserting to LAREQUESTTRACKER table with claimNO= " + dataIn.getClaimNo());
					int i = this.claimDetailsDao.lARequestTracker_Insert(dataIn.getClaimNo(), dataIn.getActivityId(),
							boMessage.getBOIDEN(), boMessage.getBOMessage(), "0");
					if (i == 0) {
						throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
					}
					// }

				} else if (claimData.getClaimTypeUI().equalsIgnoreCase("WAIVER")) {
					WSGENRegClaimWPMessageModel boMessage = new WSGENRegClaimWPMessageModel();
					boMessage.setUSRPRF(this.USRPRF);

					// log.info("Get LaRequestTrackerModel table with claimNo= "
					// + dataIn.getClaimNo());
					// LaRequestTrackerModel laRequestTrackerData =
					// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
					// dataIn.getClaimNo(), dataIn.getActivityId(),
					// boMessage.getBOIDEN());
					// log.info("Successfully Got LaRequestTrackerModel table
					// ");
					//
					// if (laRequestTrackerData.getBoidentifier() == null &&
					// laRequestTrackerData.getMsgtosend() == null
					// && laRequestTrackerData.getMsgcreatets() == null
					// && laRequestTrackerData.getFuturemesg() == null) {

					log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully retrieved PolicyDetail table");

					if (!(Optional.ofNullable(claimData).isPresent()
							&& Optional.ofNullable(policyDetail).isPresent())) {
						throw new DAOException("ClaimData is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
					boMessage.setCLAIMNO(dataIn.getClaimNo());
					log.debug("CLAIMNO=" + dataIn.getClaimNo());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					log.debug("CHDRSEL=" + claimData.getPolicyNumber());
					boMessage.setCLAIMCUR(policyDetail.getCURRENCY());
					log.debug("CLAIMCUR= "+boMessage.getCLAIMCUR());
					boMessage.setCLAIMNO(dataIn.getClaimNo());
					log.debug("CLAIMNO= "+boMessage.getCLAIMNO());
					boMessage.setCLTYPE(claimData.getClaimTypeLA());
					log.debug("CLTYPE= "+boMessage.getCLTYPE());
					boMessage.setINCURDT(Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));
					log.debug("INCURDT= "+boMessage.getINCURDT());
					boMessage.setPRCNT(claimData.getClaimSumAssuredPercentage().doubleValue());
					log.debug("PRCNT= "+boMessage.getPRCNT());
					boMessage.setPYMT(claimData.getClaimApprovedAmountUI());
					log.debug("PYMT= "+boMessage.getPYMT());
					boMessage.setZCLMRECD(Util.dateToString(claimData.getReceivedDate(), "yyyyMMdd"));
					log.debug("ZCLMRECD= "+boMessage.getZCLMRECD());
					boMessage.setZDIAGCDE(claimData.getDiagnosisCode());
					log.debug("ZDIAGCDE= "+boMessage.getZDIAGCDE());
					boMessage.setZREGPCOD("");
					log.debug("ZREGPCOD=  "+boMessage.getZREGPCOD());
					boMessage.setCRTABLE(claimData.getComponentCode());
					log.debug("CRTABLE= "+boMessage.getCRTABLE());

					//20191001- CR eCLaimUpdate
					boMessage.setPAYDATE(Util.dateToString(claimData.getClaimPayDate(), "yyyyMMdd"));

					log.info("Get ComponentDetails table");
					ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
							claimData.getComponentCode(), claimData.getInsuredClientID());
					log.info("Successfully retrieved ComponentDetails table");

					if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
						throw new DAOException("componentDetailsBO is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setLIFE(componentDetailsBO.getLife());
					log.debug("LIFE= "+boMessage.getLIFE());
					boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
					log.debug("COVERAGE= "+boMessage.getCOVERAGE());
					boMessage.setRIDER(componentDetailsBO.getRider());
					log.debug("RIDER= "+boMessage.getRIDER());

					updateNotification(dataIn.getClaimNo());

					log.info("BOMESSAGE RESULT = " + boMessage.getBoMessage());
					log.info("Inserting to LAREQUESTTRACKER table with claimNO= " + dataIn.getClaimNo());
					int i = this.claimDetailsDao.lARequestTracker_Insert(dataIn.getClaimNo(), dataIn.getActivityId(),
							boMessage.getBOIDEN(), boMessage.getBoMessage(), "0");
					if (i == 0) {
						throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
					}
					// }

				} else if (claimData.getClaimTypeUI().equalsIgnoreCase("CI")
						|| claimData.getClaimTypeUI().equalsIgnoreCase("C1")
						|| claimData.getClaimTypeUI().equalsIgnoreCase("TERM")) {
					WSGENRegCITermBOMessage boMessage = new WSGENRegCITermBOMessage();
					boMessage.setUSRPRF(this.USRPRF);

					// log.info("Get LaRequestTrackerModel table with claimNo= "
					// + dataIn.getClaimNo());
					// LaRequestTrackerModel laRequestTrackerData =
					// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
					// dataIn.getClaimNo(), dataIn.getActivityId(),
					// boMessage.getBOIDEN());
					// log.info("Successfully Got LaRequestTrackerModel table
					// ");
					//
					// if (laRequestTrackerData.getBoidentifier() == null &&
					// laRequestTrackerData.getMsgtosend() == null
					// && laRequestTrackerData.getMsgcreatets() == null
					// && laRequestTrackerData.getFuturemesg() == null) {

					log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully retrieved PolicyDetail table");

					if (!(Optional.ofNullable(claimData).isPresent()
							&& Optional.ofNullable(policyDetail).isPresent())) {
						throw new DAOException("ClaimData is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					boMessage.setCLAIMCUR(policyDetail.getCURRENCY());
					boMessage.setCLAIMNO(dataIn.getClaimNo());
					boMessage.setCLTYPE(claimData.getClaimTypeLA());
					boMessage.setINCURDT(Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));
					boMessage.setPAYCLT(claimData.getOwnerClientID());
					boMessage.setPRCNT(claimData.getClaimSumAssuredPercentage().doubleValue());
					boMessage.setPYMT(claimData.getClaimApprovedAmountUI());
					boMessage.setZCLMRECD(Util.dateToString(claimData.getProcessInitiatonTS(), "yyyyMMdd"));
					boMessage.setZDIAGCDE(claimData.getClaimTypeLA().equalsIgnoreCase("CI")
							|| claimData.getClaimTypeLA().equalsIgnoreCase("C1") ? "" : claimData.getDiagnosisCode());
					boMessage.setZREGPCOD(claimData.getClaimTypeLA().equalsIgnoreCase("CI")
							|| claimData.getClaimTypeLA().equalsIgnoreCase("C1") ? claimData.getDiagnosisCode() : "");
					boMessage.setBANKACOUNT(claimData.getBankAccountNumber());
					boMessage.setBANKKEY(claimData.getBankCode());
					boMessage.setCRTABLE(claimData.getComponentCode());

					log.info("Get ComponentDetails table");
					ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
							claimData.getComponentCode(), claimData.getInsuredClientID());
					log.info("Successfully retrieved ComponentDetails table");

					if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
						throw new DAOException("componentDetailsBO is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setLIFE(componentDetailsBO.getLife());
					boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
					boMessage.setRIDER(componentDetailsBO.getRider());

					boMessage.setFACTHOUS(Integer.valueOf(claimData.getFactoringHouse()));
					boMessage.setBANKACCDSC(claimData.getBankAccountName());
					boMessage.setBNKACTYP(claimData.getBankType());
					boMessage.setCURRCODE(policyDetail.getCURRENCY());
					boMessage.setDATEFROM(Util.dateToString(claimData.getCurrentFrom(), "yyyyMMdd"));
					boMessage.setDATETO("");

					updateNotification(dataIn.getClaimNo());

					log.info("BOMESSAGE RESULT = " + boMessage.getBoMessage());
					log.info("Inserting to LAREQUESTTRACKER table with claimNO= " + dataIn.getClaimNo());
					int i = this.claimDetailsDao.lARequestTracker_Insert(dataIn.getClaimNo(), dataIn.getActivityId(),
							boMessage.getBOIDEN(), boMessage.getBoMessage(), "0");
					if (i == 0) {
						throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
					}
					// }

				} else if (claimData.getClaimTypeUI().equalsIgnoreCase("ADDB")) {
					WSGENRegClaimADDBMessageModel boMessage = new WSGENRegClaimADDBMessageModel();
					boMessage.setUSRPRF(this.USRPRF);

					// log.info("Get LaRequestTrackerModel table with claimNo= "
					// + dataIn.getClaimNo());
					// LaRequestTrackerModel laRequestTrackerData =
					// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
					// dataIn.getClaimNo(), dataIn.getActivityId(),
					// boMessage.getBOIDEN());
					// log.info("Successfully Got LaRequestTrackerModel table
					// ");
					//
					// if (laRequestTrackerData.getBoidentifier() == null &&
					// laRequestTrackerData.getMsgtosend() == null
					// && laRequestTrackerData.getMsgcreatets() == null
					// && laRequestTrackerData.getFuturemesg() == null) {

					log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully retrieved PolicyDetail table");

					if (!(Optional.ofNullable(claimData).isPresent()
							&& Optional.ofNullable(policyDetail).isPresent())) {
						throw new DAOException("ClaimData is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
					boMessage.setCLAIMNO(dataIn.getClaimNo());
					log.debug("CLAIMNO=" + dataIn.getClaimNo());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					log.debug("CHDRSEL=" + claimData.getPolicyNumber());
					boMessage.setCLAIMEVD("");
					boMessage.setCLAIMNO(dataIn.getClaimNo());
					boMessage.setCLTYPE(claimData.getClaimTypeLA());
					boMessage.setINCURDT(Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));
					boMessage.setPAYCLT(claimData.getOwnerClientID());
					boMessage.setPRCNT(claimData.getClaimSumAssuredPercentage().doubleValue());
					boMessage.setPYMT(claimData.getClaimApprovedAmountUI().doubleValue());
					boMessage.setZCLMRECD(Util.dateToString(claimData.getProcessInitiatonTS(), "yyyyMMdd"));
					boMessage.setZDIAGCDE("");
					boMessage.setBANKACOUNT(claimData.getBankAccountNumber());
					boMessage.setBANKKEY(claimData.getBankCode());
					boMessage.setDISCHDT(Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));
					boMessage.setZDOCTOR("");
					boMessage.setZMEDPRV(claimData.getProviderCode());
					boMessage.setLIFENUM(claimData.getInsuredClientID());
					boMessage.setCRTABLE(claimData.getComponentCode());

					log.info("Get ComponentDetails table");
					ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
							claimData.getComponentCode(), claimData.getInsuredClientID());
					log.info("Successfully retrieved ComponentDetails table");

					if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
						throw new DAOException("componentDetailsBO is null with Claim NO= " + dataIn.getClaimNo());
					}
					;

					boMessage.setLIFE(componentDetailsBO.getLife());
					boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
					boMessage.setRIDER(componentDetailsBO.getRider());
					boMessage.setFACTHOUS(claimData.getFactoringHouse());
					boMessage.setBANKACCDSC(claimData.getBankAccountName());
					boMessage.setBNKACTYP(claimData.getBankType());
					boMessage.setCURRCODE(policyDetail.getCURRENCY());

					List<WSGENRegClaimADDBRecurringMessageModel> WSGENRecur_li = new ArrayList<WSGENRegClaimADDBRecurringMessageModel>();

					log.info("Get claimDataDetails table with claimNo= " + dataIn.getClaimNo());
					List<ClaimDataDetailsModel> claimDataDetails_li = new ArrayList<ClaimDataDetailsModel>();
					claimDataDetails_li = this.claimDetailsDao.getClaimDataDetailsByClaimNo(dataIn.getClaimNo());
					log.info("Successfully retrieved claimDataDetails table");
					if (claimDataDetails_li.size() <= 0) {
						throw new DAOException("ClaimDataDetails is null with claimNo= " + dataIn.getClaimNo());
					}
					;
					int count = 0;

					for (ClaimDataDetailsModel claimDataDetailsModel : claimDataDetails_li) {
						WSGENRegClaimADDBRecurringMessageModel model = new WSGENRegClaimADDBRecurringMessageModel();
						model.setDATEFROM(Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));
						model.setDATETO(Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));
						model.setACDBEN(claimDataDetailsModel.getBenefitCode());
						model.setBENPRCNT(claimDataDetailsModel.getBenefitPercentage().doubleValue());

						WSGENRecur_li.add(model);
						count = count + 1;
					}

					if (count < 15) {
						for (int i = 0; i < 15 - count; i++) {
							WSGENRegClaimADDBRecurringMessageModel model = new WSGENRegClaimADDBRecurringMessageModel();
							model.setACDBEN("");
							model.setBENPRCNT(0.0);

							WSGENRecur_li.add(model);
						}
					} else if (count > 15) {
						throw new CamelException("ClaimDataDetails exceed size of 15 = " + count);
					}

					boMessage.setwSGenRecLaimADDB_li(WSGENRecur_li);

					updateNotification(dataIn.getClaimNo());

					log.info("BOMESSAGE RESULT = " + boMessage.getBOMessage());
					log.info("Inserting to LAREQUESTTRACKER table with claimNO= " + dataIn.getClaimNo());
					int i = this.claimDetailsDao.lARequestTracker_Insert(dataIn.getClaimNo(), dataIn.getActivityId(),
							boMessage.getBOIDEN(), boMessage.getBOMessage(), "0");
					if (i == 0) {
						throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
					}
					// }

				} else {
					throw new CamelException("Unknown ClaimTypeUI= " + claimData.getClaimTypeUI());
				}
				;

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Successfull processsed ");

			} else {
				throw new CamelException("JSON INPUT STRING is Empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR detail " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("ERROR: " + e.getMessage());
		}
		log.info("JSON OUTPUT PAYLOAD: " + response.toString());

		exchange.getOut().setBody(response);

	}

	private void updateNotification(String claimNo) throws DAOException {
		String docIdList = this.claimDetailsDao.getDocIDList(claimNo, "Y");
		if (docIdList != null && !docIdList.trim().equalsIgnoreCase("")) {
			String subCategoryList = this.odsDao.getListSubcategory(docIdList);

			int rowUp = this.claimDetailsDao.updateNotificationData(claimNo, subCategoryList);
			if (rowUp == 0) {
				throw new DAOException("No row is updated for NOTIFICATIONDATA TABLE");
			}
		}

	}

}
