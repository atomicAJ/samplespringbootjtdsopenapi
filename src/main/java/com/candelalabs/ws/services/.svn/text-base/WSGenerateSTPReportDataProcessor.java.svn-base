package com.candelalabs.ws.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.impl.util.json.JSONArray;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ArgValuePairs;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSGenerateSTPReportDataIn;
import com.candelalabs.api.model.WSGenerateSTPReportDataOut;
import com.candelalabs.api.model.WSReportOutputXLSDataIn;
import com.candelalabs.api.model.WSReportOutputXLSDataInWSInput;
import com.candelalabs.api.model.WSReportOutputXLSDataOut;
import com.candelalabs.api.model.WSUpdateDataDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.model.STPReportDataXls;
import com.candelalabs.ws.model.XlsColValMap;

@Component
public class WSGenerateSTPReportDataProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSGenerateSTPReportDataProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ProducerTemplate producerTemplate;

	@SuppressWarnings("unchecked")
	@Override
	public void process(Exchange exchange) throws Exception {

		log.info("WSGenerateSTPReportDataProcessor - Service is invoked");
		WSGenerateSTPReportDataOut response = new WSGenerateSTPReportDataOut();

		List<STPReportDataXls> STPReportDataList = new ArrayList<STPReportDataXls>();

		try {
			WSGenerateSTPReportDataIn dataIn = (exchange.getIn().getBody(WSGenerateSTPReportDataIn.class));
			log.info(dataIn.toString());
			if(dataIn.getWsInput().getDtFromDate() != null && dataIn.getWsInput().getDtFromDate().trim() != "" && 
					dataIn.getWsInput().getDtToDate() != null && dataIn.getWsInput().getDtToDate().trim() != "") {
				log.debug("StrReportType : "+dataIn.getWsInput().getStrReportType());

				String pattern1 = "yyyy-MM-dd hh:mm:ss"; 
				SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

				WSUpdateDataDataIn updateDataIn = new WSUpdateDataDataIn();
				List<ArgValuePairs> argValuePairsList = new ArrayList<ArgValuePairs>();

				if(dataIn.getWsInput().getStrReportType().equalsIgnoreCase("claims")) {
					log.debug("StrReportOutuputFormat : "+dataIn.getWsInput().getStrReportOutuputFormat());
					if(dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("xls")) {
						STPReportDataList = claimDetailsDao.getClaimsSTPReportData(dataIn);

						log.debug("STPReportDataList : "+STPReportDataList.toString());
						if(STPReportDataList.size() == 0) {
							String currDtString = simpleDateFormat1.format(new Date());

							updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
									+ "ReportGenerated=?,ReportEndTimeStamp=? WHERE ReportRequestID=?");

							argValuePairsList.add(prepareArgValuePairsObj("0", "string", "No Records found for the search criteria for the reports"));
							argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
							argValuePairsList.add(prepareArgValuePairsObj("2", "string", "0"));
							argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
							argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportRequestID()));

							updateDataIn.setArgValuePairs(argValuePairsList);

							boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

							if(updateGenRepStatus) {
								response.setWsProcessingStatus("2");
								response.setWsExceptionMessage("No Records found for the search criteria for the reports");
								response.setStrReportRequestID(dataIn.getWsInput().getStrReportRequestID());
							} else {
								response.setWsProcessingStatus("2");
								response.setWsExceptionMessage("No Records found for the search criteria for the reports and Exception response form WSUpdateData service");
								response.setStrReportRequestID(dataIn.getWsInput().getStrReportRequestID());
							}	
						} else {							
							@SuppressWarnings("rawtypes")
							List reportList = new ArrayList();
							
								List<XlsColValMap> xlsColValMapHeaderList1 = new ArrayList<XlsColValMap>();
								List<XlsColValMap> xlsColValMapHeaderList2 = new ArrayList<XlsColValMap>();
								List<XlsColValMap> xlsColValMapHeaderList3 = new ArrayList<XlsColValMap>();
								List<XlsColValMap> xlsColValMapHeaderList4 = new ArrayList<XlsColValMap>();

								xlsColValMapHeaderList1.add(prepGenericClassObj(0, "Details Report of STP Fail"));
								
								xlsColValMapHeaderList2.add(prepGenericClassObj(0, "Report Date : "+dataIn.getWsInput().getDtFromDate()+" to "+dataIn.getWsInput().getDtToDate()));
								
								for (int i = 0; i < 8; i++) {
									xlsColValMapHeaderList1.add(prepGenericClassObj(i+1, ""));
									xlsColValMapHeaderList2.add(prepGenericClassObj(i+1, ""));
								}
								
								for (int j = 0; j < 9; j++) {
									xlsColValMapHeaderList3.add(prepGenericClassObj(j, ""));
								}
								
								xlsColValMapHeaderList4.add(prepGenericClassObj(0, "Policy Number"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(1, "Claim Number"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(2, "Claim Type"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(3, "Claim Status"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(4, "Claim Received Date"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(5, "Claim Register Date"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(6, "STP Status"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(7, "Reason of STP Failed"));
								xlsColValMapHeaderList4.add(prepGenericClassObj(8, "Channel (Portal / HardCopy)"));

								reportList.add(xlsColValMapHeaderList1);
								reportList.add(xlsColValMapHeaderList2);
								reportList.add(xlsColValMapHeaderList3);
								reportList.add(xlsColValMapHeaderList4);
							

							for (STPReportDataXls stpReportData : STPReportDataList) {
								int invDocNotUploadCount = claimDetailsDao.getDocNotUploadCount(stpReportData.getClaimNumber(), "Wait For Investigation Report");
								int reqInvDocNotUploadCount = claimDetailsDao.getDocNotUploadCount(stpReportData.getClaimNumber(), "Wait For Investigation Requirement Document");
								int reqDocNotUploadCount = claimDetailsDao.getDocNotUploadCount(stpReportData.getClaimNumber(), "Wait For Requested Documents");
								String stpStatus = null;
								if(invDocNotUploadCount > 0 || reqInvDocNotUploadCount > 0) {
									stpReportData.setClaimStatus("Pending Investigation");
								}else if(reqDocNotUploadCount > 0) {
									stpReportData.setClaimStatus("Pending Document");
								}else if(stpReportData.getClaimDecision() != null && !stpReportData.getClaimDecision().equalsIgnoreCase("")) {
									stpReportData.setClaimStatus(stpReportData.getClaimDecision());
								}else {
									stpReportData.setClaimStatus(stpReportData.getClaimStatus());
								}

								if(stpReportData.getStPStatus().equalsIgnoreCase("0")) {
									stpStatus = "Fail";
								}
								if(stpReportData.getStPStatus().equalsIgnoreCase("1")) {
									stpStatus = "Pass";
								}


								List<XlsColValMap> xlsColValMapList = new ArrayList<XlsColValMap>();

								xlsColValMapList.add(prepGenericClassObj(0, stpReportData.getPolicyNumber() != null ? 
										stpReportData.getPolicyNumber().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(1, stpReportData.getClaimNumber() != null ? 
										stpReportData.getClaimNumber().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(2, stpReportData.getClaimType() != null ? 
										stpReportData.getClaimType().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(3, stpReportData.getClaimStatus() != null ? 
										stpReportData.getClaimStatus().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(4, stpReportData.getClaimReceiveDate() != null ? 
										stpReportData.getClaimReceiveDate().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(5, stpReportData.getClaimRegisterDate() != null ? 
										stpReportData.getClaimRegisterDate().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(6, stpStatus));
								xlsColValMapList.add(prepGenericClassObj(7, stpReportData.getReasonForSTPFail() != null ? 
										stpReportData.getReasonForSTPFail().toString() : ""));
								xlsColValMapList.add(prepGenericClassObj(8, stpReportData.getChannel() != null ? 
										stpReportData.getChannel().toString() : ""));

								reportList.add(xlsColValMapList);
							}

							JSONArray jsArray = new JSONArray(reportList);

							WSReportOutputXLSDataIn jsonDataIn = new WSReportOutputXLSDataIn();
							WSReportOutputXLSDataInWSInput wsDataIn = new WSReportOutputXLSDataInWSInput();
							wsDataIn.setStrTemplateFileLocation(dataIn.getWsInput().getStrTemplateFileLocation());
							wsDataIn.setStrTemplateFileName(dataIn.getWsInput().getStrTemplateFileName());
							wsDataIn.setStrReportFileLocation(dataIn.getWsInput().getStrReportFileLocation());
							wsDataIn.setReportData(jsArray.toString());
							jsonDataIn.setWsInput(wsDataIn);

							log.info("Invoking the WSReportOutputXLSProcessor Service with payload = " + jsonDataIn.toString());

							WSReportOutputXLSDataOut dataOutReportXLS = producerTemplate.requestBody("direct:WSReportOutputXLSProcessor", jsonDataIn,
									WSReportOutputXLSDataOut.class);
							if(dataOutReportXLS.getWsResponse().getWsProcessingStatus().equalsIgnoreCase("1")) {

								String currDtString = simpleDateFormat1.format(new Date());

								updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
										+ "ReportGenerated=?,ReportEndTimeStamp=?,ReportFileLocation=? WHERE ReportRequestID=?");

								argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Report generated Successfully"));
								argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
								argValuePairsList.add(prepareArgValuePairsObj("2", "string", "1"));
								argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
								argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportFileLocation()+"\\"+dataOutReportXLS.getWsResponse().getStrFileName()));
								argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportRequestID()));

								updateDataIn.setArgValuePairs(argValuePairsList);

								boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

								if(updateGenRepStatus) {
									response.setWsProcessingStatus("1");
									response.setWsSuccessMessage("Report generated Successfully");
								} else {
									response.setWsProcessingStatus("2");
									response.setWsExceptionMessage("Report generated successfully, but Exception response form WSUpdateData service");
								}							
							}
							if(dataOutReportXLS.getWsResponse().getWsProcessingStatus().equalsIgnoreCase("2")) {

								String currDtString = simpleDateFormat1.format(new Date());

								updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
										+ "ReportGenerated=?,ReportEndTimeStamp=?,ReportFileLocation=? WHERE ReportRequestID=?");

								argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Exception while report generation: "+dataOutReportXLS.getWsResponse().getWsExceptionMessage()));
								argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
								argValuePairsList.add(prepareArgValuePairsObj("2", "string", "0"));
								argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
								argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportFileLocation()));
								argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportRequestID()));

								updateDataIn.setArgValuePairs(argValuePairsList);

								boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

								if(updateGenRepStatus) {
									response.setWsProcessingStatus("2");
									response.setWsExceptionMessage("Exception while report generation: "+dataOutReportXLS.getWsResponse().getWsExceptionMessage());		
								} else {
									response.setWsProcessingStatus("2");
									response.setWsExceptionMessage("Exception while report generation and WSUpdateData service: "+dataOutReportXLS.getWsResponse().getWsExceptionMessage());
								}
							} 
						}						
					} else if(dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("web")) {
						STPReportDataList = claimDetailsDao.getClaimsSTPReportData(dataIn);
						response.setWsProcessingStatus("1");
						response.setWsSuccessMessage("Success");
						response.setWsExceptionMessage("");
					} else {
						response.setWsProcessingStatus("2");
						response.setWsSuccessMessage("");
						response.setWsExceptionMessage("Only 'xls' and 'web' report types are supported. Unknown report type request - "+dataIn.getWsInput().getStrReportType());
					}
				}
				if(dataIn.getWsInput().getStrReportType().equalsIgnoreCase("ps")) {

				}
			} else {
				response.setWsProcessingStatus("2");
				response.setWsSuccessMessage("");
				response.setWsExceptionMessage("Invalid Input to generate the report data");
			}
		} catch (Exception e) {
			log.error("WSGenerateSTPReportDataProcessor Error" + e);
			response.setWsExceptionMessage( e.getMessage());
			response.setWsProcessingStatus("2");
			response.setWsSuccessMessage("");
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("Final Response " + response.toString());
		exchange.getOut().setBody(response);
	}

	public static XlsColValMap prepGenericClassObj(int colNum, String colValue) {
		XlsColValMap xlsColValMap = new XlsColValMap();
		xlsColValMap.setColNum(colNum);
		xlsColValMap.setColValue(colValue);
		return xlsColValMap;
	}

	public ArgValuePairs prepareArgValuePairsObj(String index, String paramType,  String value) {
		ArgValuePairs argValuePairsObj = new ArgValuePairs();
		argValuePairsObj.setIndex(index);
		argValuePairsObj.setTypeParam(paramType);
		argValuePairsObj.setValue(value);
		return argValuePairsObj;
	}

	public boolean invokeUpdateDataService(WSUpdateDataDataIn updateDataIn) {
		log.info("Invoking the WSUpdateData Service with payload = " + updateDataIn.toString());

		JsonResponse wsUpdateDataResponse = producerTemplate.requestBody("direct:WSUpdateDataProcessor", updateDataIn,
				JsonResponse.class);

		if(wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("1")) {
			log.info("Status updated in CASEREPORTREQUEST table successfully");
			return true;
		}
		if(wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("2")) {
			log.error("Exception updating status in CASEREPORTREQUEST table");
			return false;
		}
		return false;
	}
}
