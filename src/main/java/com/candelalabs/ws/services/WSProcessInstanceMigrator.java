/**
 * 
 */
package com.candelalabs.ws.services;


import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSCheckMandatoryDocsDataIn;
import com.candelalabs.api.model.WSProcessInstanceMigratorDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;


import java.io.Serializable;

import org.activiti.engine.impl.cmd.SetProcessDefinitionVersionCmd;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
@Component
public class WSProcessInstanceMigrator extends Object implements Processor {

	private static final Log log = LogFactory.getLog("WSProcessInstanceMigrator");

	/*@Autowired
	private ClaimDetailsDao claimDetailsDao;*/

	/**
	 * 
	 */
	public WSProcessInstanceMigrator() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		JsonResponse response = new JsonResponse();
		log.info("WSProcessInstanceMigrator - Service is invoked"); 
		try {
			/*WSProcessInstanceMigratorDataIn dataIn = exchange.getIn().getBody
													(WSProcessInstanceMigratorDataIn.class);*/
			WSProcessInstanceMigratorDataIn dataIn=null;
			dataIn= exchange.getIn().getBody(WSProcessInstanceMigratorDataIn.class);
			
			//processInstanceId  processDefinitionVersion
			log.info(" dataIn.getProcessDefinitionVersion()) - " + dataIn.getProcessDefinitionVersion());
			log.info(" dataIn.getProcessInstanceId() - " + dataIn.getProcessInstanceId() );
			Integer processDefver = 0;
			processDefver = Integer.valueOf(dataIn.getProcessDefinitionVersion().toString());
			String processInstanceId ="";
			processInstanceId = dataIn.getProcessInstanceId().toString();
			if ((processInstanceId!=null && processInstanceId!="") &&
					(processDefver!=null && processDefver!=0)
			   )
			{
			processDefver = Integer.valueOf(dataIn.getProcessDefinitionVersion().toString());
			
			processInstanceId = dataIn.getProcessInstanceId().toString();
			
			new SetProcessDefinitionVersionCmd(processInstanceId,processDefver);
			log.info("WSProcessInstanceMigrator - processInstanceId,processDefver are - " + processInstanceId + processDefver); 
			response.setWsExceptionMessage("");
			response.setWsProcessingStatus("1");
			response.setWsSuccessMessage("Instance SuccessFully Migrated!!!");
			}
			else
			{
				log.info("WSProcessInstanceMigrator - WSProcessInstanceMigrator ");
				response.setWsExceptionMessage("Input payload not set properly.");
				response.setWsProcessingStatus("2");
				response.setWsSuccessMessage("");
			}
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			log.error("Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error While Processsing = " + e.getMessage());
			response.setWsSuccessMessage("");
		}
		log.info("WSProcessInstanceMigrator - Final Response " + response.toString());

		exchange.getOut().setBody(response);

	}

	/*@Override
	public Void execute(CommandContext arg0) {
		// TODO Auto-generated method stub
		return null;
	}
*/
}
