package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSCommonDataOut;
import com.candelalabs.api.model.WSCommonDataOutWSResponse;
import com.candelalabs.api.model.WSCreateCaptureRecordDataIn;
import com.candelalabs.api.model.WSCreateCaptureRecordDataInWSInput;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.util.Util;

@Component
public class WSCreateCaptureRecordProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSCreateCaptureRecordProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("WSPortalServiceCreateClaim - Service is invoked");
		WSCommonDataOut response = new WSCommonDataOut();
		WSCommonDataOutWSResponse wsResponse = new WSCommonDataOutWSResponse();
		try {
			WSCreateCaptureRecordDataIn dataInput = exchange.getIn().getBody(WSCreateCaptureRecordDataIn.class);
			List<WSCreateCaptureRecordDataInWSInput> wsDataInput = dataInput.getWsInput();

			log.info("WSPortalServiceProcessor - Json Object In= " + dataInput.toString());

			String exceptionRecord;
			boolean isAllClaimsValid = true;
			String exceptionMessage = "";
			String duplicateMessage = "";

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String strDate;

			for (WSCreateCaptureRecordDataInWSInput dataIn : wsDataInput) {

				exceptionRecord = "N";

				if(dataIn.getStrPolicyNumber() == null || dataIn.getStrPolicyNumber().trim().equalsIgnoreCase("")) {
					exceptionRecord = "Y";
				}
				if(dataIn.getStrBatchNumber() == null || dataIn.getStrBatchNumber().trim().equalsIgnoreCase("")) {
					exceptionRecord = "Y";
				}
				if(dataIn.getDtDocumentScannedDate() == null || dataIn.getDtDocumentScannedDate().trim().equalsIgnoreCase("")) {  
					strDate= formatter.format(date); 
					dataIn.setDtDocumentScannedDate(strDate);
				}
				if(dataIn.getStrOwnerIdType() == null || dataIn.getStrOwnerIdType().trim().equalsIgnoreCase("")) {  
					dataIn.setStrOwnerIdType("");
				}
				if(dataIn.getStrOwnerIdNumber() == null || dataIn.getStrOwnerIdNumber().trim().equalsIgnoreCase("")) {  
					dataIn.setStrOwnerIdNumber("");
				}
				if(dataIn.getStrPayeeName() == null || dataIn.getStrPayeeName().trim().equalsIgnoreCase("")) {  
					dataIn.setStrPayeeName("");
				}
				if(dataIn.getStrInsuredClientName() == null || dataIn.getStrInsuredClientName().trim().equalsIgnoreCase("")) {  
					dataIn.setStrInsuredClientName("");
				}
				if(dataIn.getStrInsuredIdType() == null || dataIn.getStrInsuredIdType().trim().equalsIgnoreCase("")) {  
					dataIn.setStrInsuredIdType("");
				}
				if(dataIn.getStrInsuredIdNumber() == null || dataIn.getStrInsuredIdNumber().trim().equalsIgnoreCase("")) {  
					dataIn.setStrInsuredIdNumber("");
				}
				if(dataIn.getStrFormClaimType() == null || dataIn.getStrFormClaimType().trim().equalsIgnoreCase("")) {  
					dataIn.setStrFormClaimType("");
				}
				if(dataIn.getStrFormBenefitType() == null || dataIn.getStrFormBenefitType().trim().equalsIgnoreCase("")) {  
					dataIn.setStrFormBenefitType("");
				}
				if(dataIn.getStrSurgeryPerformed() == null || dataIn.getStrSurgeryPerformed().trim().equalsIgnoreCase("")) {  
					dataIn.setStrSurgeryPerformed("");
				}
				if(dataIn.getDtTreatmentStartDate() == null || dataIn.getDtTreatmentStartDate().trim().equalsIgnoreCase("")) {  
					dataIn.setDtTreatmentStartDate("");
				}
				if(dataIn.getDtTreatmentCompletionDate() == null || dataIn.getDtTreatmentCompletionDate().trim().equalsIgnoreCase("")) {  
					dataIn.setDtTreatmentCompletionDate("");
				}
				if(dataIn.getStrHospitalName() == null || dataIn.getStrHospitalName().trim().equalsIgnoreCase("")) {  
					dataIn.setStrHospitalName("");
				}
				if(dataIn.getStrFormCurrency() == null || dataIn.getStrFormCurrency().trim().equalsIgnoreCase("")) {  
					dataIn.setStrFormCurrency("");
				}
				if(dataIn.getDecTotalBillingAmount() == null || dataIn.getDecTotalBillingAmount().trim().equalsIgnoreCase("")) {  
					dataIn.setDecTotalBillingAmount("0");
				}
				if(dataIn.getStrPhysician() == null || dataIn.getStrPhysician().trim().equalsIgnoreCase("")) {  
					dataIn.setStrPhysician("");
				}
				if(dataIn.getStrBankAccountNumber() == null || dataIn.getStrBankAccountNumber().trim().equalsIgnoreCase("")) {  
					dataIn.setStrBankAccountNumber("");
				}
				if(dataIn.getStrPaymentAccountOwnerName() == null || dataIn.getStrPaymentAccountOwnerName().trim().equalsIgnoreCase("")) {  
					dataIn.setStrPaymentAccountOwnerName("");
				}

				if(exceptionRecord.trim() == "Y") {
					isAllClaimsValid = false;
					if(exceptionMessage.trim() == "") {
						exceptionMessage = "Input with following Data is not stored due incorrect / incomplete data. ";
					}

					exceptionMessage += "Policy Number = "+dataIn.getStrPolicyNumber()+", Batch Number = "+dataIn.getStrBatchNumber();
				} else {
					int count = claimDetailsDao.checkDuplicateCaptureRecord(dataIn.getStrBatchNumber(), dataIn.getStrPolicyNumber());
					if(count > 0) {
						isAllClaimsValid = false;
						if(duplicateMessage.trim() == "") {
							duplicateMessage = "Duplicate Request found for following data";
						}
						duplicateMessage += ":: Policy Number = "+dataIn.getStrPolicyNumber()+" and Batch Number = "+ dataIn.getStrBatchNumber();
						claimDetailsDao.insertIntoCaptureBatchDetails(dataIn.getStrBatchNumber(), dataIn.getStrPolicyNumber(), duplicateMessage, "Y");
					} else {
						
						int recordInsertCount = 0;
						recordInsertCount = claimDetailsDao.insertClaimDataSourceCapture(dataIn.getStrBatchNumber(),Util.
								objToDate(dataIn.getDtDocumentScannedDate(),"dd/MM/yyyy"),dataIn.
								getStrPolicyNumber(),dataIn.getStrOwnerIdType(),dataIn.getStrOwnerIdNumber(),dataIn.
								getStrPayeeName(),dataIn.getStrInsuredClientName(),dataIn.getStrInsuredIdType(),
								dataIn.getStrInsuredIdNumber(),dataIn.getStrFormClaimType(),dataIn.getStrFormBenefitType(),
								dataIn.getStrSurgeryPerformed(),Util.objToDate(dataIn.getDtTreatmentStartDate(),"dd/MM/yyyy"),
								Util.objToDate(dataIn.getDtTreatmentCompletionDate(),"dd/MM/yyyy"),dataIn.getStrHospitalName(),
								dataIn.getStrFormCurrency(),new BigDecimal(dataIn.getDecTotalBillingAmount()),
								dataIn.getStrPhysician(),dataIn.getStrBankAccountNumber(), dataIn.getStrPaymentAccountOwnerName(),"N","N");
						if(recordInsertCount > 0) {
							claimDetailsDao.insertIntoCaptureBatchDetails(dataIn.getStrBatchNumber(), dataIn.getStrPolicyNumber(), "", "N");
						}
					}
				}

			}

			if(isAllClaimsValid) {
				wsResponse.setWsProcessingStatus("1");
				wsResponse.setWsExceptionMessage("");
				wsResponse.setWsSuccessMessage("Successfully Created Records for the Input received");
				response.setWsResponse(wsResponse);
			} else {
				wsResponse.setWsProcessingStatus("2");
				wsResponse.setWsExceptionMessage("Some of the input were processed successfully. Some input could not be processed due to the missing Information. Details for records which could not created are listed with this message :: "+exceptionMessage +","+ duplicateMessage);
				wsResponse.setWsSuccessMessage("");
				response.setWsResponse(wsResponse);
			}

		}
		catch (Exception e) {
			log.error("WSPortalServiceCreateClaim Error - " + e);
			wsResponse.setWsExceptionMessage("Some of the input were processed successfully. Some input could not be processed due to the missing Information. Details for records which could not created are listed with this message :: "+e+" - "+e.getMessage());
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsSuccessMessage("");
			response.setWsResponse(wsResponse);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}

		log.info("Final Response " + response.toString());

		exchange.getOut().setBody(response);
	}

}
