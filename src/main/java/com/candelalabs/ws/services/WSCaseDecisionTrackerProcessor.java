package com.candelalabs.ws.services;

import com.candelalabs.api.model.CaseDecisionTrackerModel;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Triaji K.
 */
@Component
public class WSCaseDecisionTrackerProcessor implements Processor {

    private static Log log = LogFactory.getLog("WSCaseDecisionTrackerProcessor");

    @Autowired
    ClaimDetailsDao claimDetailsDao;

    @Override
    public void process(Exchange exchange) throws Exception {
        JsonResponse response = new JsonResponse();
        try {
            CaseDecisionTrackerModel input = exchange.getIn().getBody(CaseDecisionTrackerModel.class);
            log.debug("Input Body"+input.toString());
            if (input != null) {
                if (input.getOperation() != null && !input.getOperation().trim().equalsIgnoreCase("") && input.getOperation().trim().equalsIgnoreCase("start")) {
                    if (this.claimDetailsDao.createDecisionTracker(input.getProcessType(),input.getProcessTransactionType(),input.getUserID(),input.getCaseReferenceNumber(),
                            input.getCaseUserActivityName(),input.getCaseOperationTs()) == 0) {
                        throw  new Exception("Can't create a record in the CaseDecisionTracker Table");
                    }
                } else if (input.getOperation() != null && !input.getOperation().trim().equalsIgnoreCase("") && input.getOperation().trim().equalsIgnoreCase("end")) {
                    if (this.claimDetailsDao.endDecisionTracker(input.getCaseReferenceNumber(),input.getUserID(),input.getCaseUserDecision(),input.getCaseUserActivityName(),input.getCaseOperationTs()) == 0) {
                        throw  new Exception("Record is not updated in the CaseDecisionTracker Table");
                    }
                } else {
                    throw new Exception("No Operation Defined for this request");
                }
            } else {
                throw new Exception("No Input Object Defined for this request");
            }
            response.setWsSuccessMessage("Successfully create/update a record in the table");
            response.setWsProcessingStatus("1");
        } catch (Exception e) {
            for(StackTraceElement tr :e.getStackTrace()) {
                log.error("\tat "+tr);
            }
            response.setWsProcessingStatus("2");
            response.setWsExceptionMessage("Error " + e.getMessage());
        }
        log.info("Final Response " + response.toString());

        exchange.getOut().setBody(response);
    }
}
