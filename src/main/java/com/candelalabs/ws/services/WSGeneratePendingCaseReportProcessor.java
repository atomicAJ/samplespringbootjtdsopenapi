package com.candelalabs.ws.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.impl.util.json.JSONArray;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ArgValuePairs;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSGeneratePendingCaseReportDataIn;
import com.candelalabs.api.model.WSGeneratePendingCaseReportDataOutWSResponse;
import com.candelalabs.api.model.WSReportOutputXLSDataIn;
import com.candelalabs.api.model.WSReportOutputXLSDataInWSInput;
import com.candelalabs.api.model.WSReportOutputXLSDataOut;
import com.candelalabs.api.model.WSUpdateDataDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.model.PendingCaseClaimData;
import com.candelalabs.ws.model.PendingCaseReport;
import com.candelalabs.ws.model.ReportDetails;
import com.candelalabs.ws.model.RequirementsDetails;
import com.candelalabs.ws.model.WebReportDataModel;
import com.candelalabs.ws.model.XlsColValMap;
import com.candelalabs.ws.model.tables.PolicyDetails;

@Component
public class WSGeneratePendingCaseReportProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSGeneratePendingCaseReportProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ProducerTemplate producerTemplate;

	@Autowired
	ODSDao odsDao;

	@SuppressWarnings("unchecked")
	@Override
	public void process(Exchange exchange) throws Exception {

		WSGeneratePendingCaseReportDataOutWSResponse wsResponse = new WSGeneratePendingCaseReportDataOutWSResponse();

		try {
			WSGeneratePendingCaseReportDataIn dataIn = (exchange.getIn().getBody(WSGeneratePendingCaseReportDataIn.class));
			log.debug("----- dataIn -----");
			log.debug(dataIn.toString());

			String pattern = "dd-MM-yyyy"; 
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

			String pattern1 = "yyyy-MM-dd hh:mm:ss"; 
			SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

			if(dataIn.getWsInput().getStrReportType().equalsIgnoreCase("claims")) {
				if(dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("xls")) {
					int fupMaxCount = 0;
					List<PendingCaseClaimData> pendingCaseClaimDataList = new ArrayList<PendingCaseClaimData>();
					pendingCaseClaimDataList = claimDetailsDao.getPendingCases("xls", "", "");

					List<PendingCaseReport> pendingCaseReportList = new ArrayList<PendingCaseReport>();

					WSUpdateDataDataIn updateDataIn = new WSUpdateDataDataIn();
					List<ArgValuePairs> argValuePairsList = new ArrayList<ArgValuePairs>();

					for (PendingCaseClaimData pendingCaseClaimData : pendingCaseClaimDataList) {
						log.debug("***** pendingCaseClaimData *****");
						log.debug(pendingCaseClaimData.toString());
						List<RequirementsDetails> requirementsDetailsList = new ArrayList<RequirementsDetails>();

						PolicyDetails policyDetails = new PolicyDetails();
						PendingCaseReport pendingCaseReport = new PendingCaseReport();
						ReportDetails reportDetails = new ReportDetails();
						policyDetails = odsDao.getPolicyDetailsForReport(pendingCaseClaimData.getPolicyNumber());
						requirementsDetailsList = claimDetailsDao.getPendReqDetls(pendingCaseClaimData.getClaimNumber());
						reportDetails = claimDetailsDao.getDecissionUser(pendingCaseClaimData.getClaimNumber());
						Date pendDaysDate = claimDetailsDao.getPendDocReceiveDate(pendingCaseClaimData.getClaimNumber(), "PCR PENDDAYS");						
						String pendingType = claimDetailsDao.getPendingType(pendingCaseClaimData.getClaimNumber());
						List<RequirementsDetails> fupDescriptionList = claimDetailsDao.getFUPDescription(pendingCaseClaimData.getClaimNumber());

						pendingCaseReport.setPolicyNumber(pendingCaseClaimData.getPolicyNumber());
						pendingCaseReport.setClaimNumber(pendingCaseClaimData.getClaimNumber());
						pendingCaseReport.setClaimType(pendingCaseClaimData.getClaimTypeUI());
						pendingCaseReport.setProductName(policyDetails.getPRODUCTNAME());
						pendingCaseReport.setPolicyStatus(policyDetails.getPOLICYSTATUS());
						pendingCaseReport.setChannel(policyDetails.getCHANNEL());
						pendingCaseReport.setAgentName(policyDetails.getAGENTNAME());
						//below fields yet to be populated
						pendingCaseReport.setSalesOfficeName("");
						pendingCaseReport.setSalesOfficeAddress("");
						pendingCaseReport.setUserId(reportDetails.getActionedUser());
						pendingCaseReport.setPolicyOwner(policyDetails.getOWNERNAME());
						pendingCaseReport.setInsuredName(pendingCaseClaimData.getInsuredClientName());
						pendingCaseReport.setReceiveDate(pendingCaseClaimData.getReceiveddate() != null ? 
								simpleDateFormat.format(pendingCaseClaimData.getReceiveddate()).toString() : "");
						pendingCaseReport.setRegisterDate(pendingCaseClaimData.getRegisterdate() != null ? 
								simpleDateFormat.format(pendingCaseClaimData.getRegisterdate()).toString() : "");
						pendingCaseReport.setAdmissionDate(pendingCaseClaimData.getAdmissiondate() != null ? 
								simpleDateFormat.format(pendingCaseClaimData.getAdmissiondate()).toString() : "");
						pendingCaseReport.setDischargeDate(pendingCaseClaimData.getDischargedate() != null ? 
								simpleDateFormat.format(pendingCaseClaimData.getDischargedate()).toString() : "");
						pendingCaseReport.setDateOfDeath(pendingCaseClaimData.getDischargedate() != null ? 
								simpleDateFormat.format(pendingCaseClaimData.getDischargedate()).toString() : "");
						pendingCaseReport.setAmountIncurred(pendingCaseClaimData.getClaimInvoiceAmount() != null ? 
								pendingCaseClaimData.getClaimInvoiceAmount().toString() : "");

						if(pendDaysDate != null) {
							log.debug("pendDaysDate = "+pendDaysDate.toString());
							int diffInDays = getDiffDays(pendDaysDate, new Date());
							pendingCaseReport.setPendingDays(String.valueOf(diffInDays));
							pendingCaseReport.setFupEntryDate(simpleDateFormat.format(pendDaysDate).toString());
						}else {
							log.debug("pendDaysDate id null");
							pendingCaseReport.setPendingDays("");
						}

						pendingCaseReport.setPendingType(pendingType);

						if(fupDescriptionList != null && fupDescriptionList.size() > 0) {
							List<String> strFUPList = new ArrayList<String>();
							for (RequirementsDetails fupDescription : fupDescriptionList) {
								String fupDescritpion = new String();
								fupDescritpion += fupDescription.getDocID()+" - "+fupDescription.getRequirementText();
								strFUPList.add(fupDescritpion);
							}
							pendingCaseReport.setFupDescription(strFUPList);
							if(strFUPList.size() > fupMaxCount) {
								fupMaxCount = strFUPList.size();
							}
						}

						pendingCaseReportList.add(pendingCaseReport);

					}

					int i = 1;
					List reportList = new ArrayList();
					for(PendingCaseReport pendingCaseReport : pendingCaseReportList) {
						int curObjFUPCount = 0;
						if(pendingCaseReport.getFupDescription() != null) {
							curObjFUPCount = pendingCaseReport.getFupDescription().size();	
						}

						List<XlsColValMap> xlsColValMapList = new ArrayList<XlsColValMap>();

						xlsColValMapList.add(prepGenericClassObj(0, Integer.toString(i)));
						xlsColValMapList.add(prepGenericClassObj(1, pendingCaseReport.getPolicyNumber()));
						xlsColValMapList.add(prepGenericClassObj(2, pendingCaseReport.getClaimNumber()));
						xlsColValMapList.add(prepGenericClassObj(3, pendingCaseReport.getClaimType()));
						xlsColValMapList.add(prepGenericClassObj(4, pendingCaseReport.getProductName()));
						xlsColValMapList.add(prepGenericClassObj(5, pendingCaseReport.getPolicyStatus()));
						xlsColValMapList.add(prepGenericClassObj(6, pendingCaseReport.getChannel()));
						xlsColValMapList.add(prepGenericClassObj(7, pendingCaseReport.getAgentName()));
						xlsColValMapList.add(prepGenericClassObj(8, pendingCaseReport.getSalesOfficeName()));
						xlsColValMapList.add(prepGenericClassObj(9, pendingCaseReport.getSalesOfficeAddress()));
						xlsColValMapList.add(prepGenericClassObj(10, pendingCaseReport.getUserId()));
						xlsColValMapList.add(prepGenericClassObj(11, pendingCaseReport.getPolicyOwner()));
						xlsColValMapList.add(prepGenericClassObj(12, pendingCaseReport.getInsuredName()));
						xlsColValMapList.add(prepGenericClassObj(13, pendingCaseReport.getReceiveDate()));
						xlsColValMapList.add(prepGenericClassObj(14, pendingCaseReport.getRegisterDate()));
						xlsColValMapList.add(prepGenericClassObj(15, pendingCaseReport.getAdmissionDate()));
						xlsColValMapList.add(prepGenericClassObj(16, pendingCaseReport.getDischargeDate()));
						xlsColValMapList.add(prepGenericClassObj(17, pendingCaseReport.getDateOfDeath()));
						xlsColValMapList.add(prepGenericClassObj(18, pendingCaseReport.getAmountIncurred()));
						xlsColValMapList.add(prepGenericClassObj(19, pendingCaseReport.getPendingDays()));
						xlsColValMapList.add(prepGenericClassObj(20, pendingCaseReport.getFupEntryDate()));
						xlsColValMapList.add(prepGenericClassObj(21, pendingCaseReport.getPendingType()));

						int fupColBeginNum = 22;
						for (String fupDescritpion : pendingCaseReport.getFupDescription()) {
							xlsColValMapList.add(prepGenericClassObj(fupColBeginNum, fupDescritpion));
							fupColBeginNum++;
						}

						if(curObjFUPCount < fupMaxCount) {
							int addOnCol = fupMaxCount-curObjFUPCount;
							for (int j = 0; j < addOnCol; j++) {
								xlsColValMapList.add(prepGenericClassObj(fupColBeginNum, ""));
								fupColBeginNum++;
							}
						}

						reportList.add(xlsColValMapList);

						i++;
					}

					JSONArray jsArray = new JSONArray(reportList);

					WSReportOutputXLSDataIn jsonDataIn = new WSReportOutputXLSDataIn();
					WSReportOutputXLSDataInWSInput wsDataIn = new WSReportOutputXLSDataInWSInput();
					wsDataIn.setStrTemplateFileLocation(dataIn.getWsInput().getStrTemplateFileLocation());
					wsDataIn.setStrTemplateFileName(dataIn.getWsInput().getStrTemplateFileName());
					wsDataIn.setStrReportFileLocation(dataIn.getWsInput().getStrReportFileLocation());
					wsDataIn.setReportData(jsArray.toString());
					jsonDataIn.setWsInput(wsDataIn);

					log.info("Invoking the WSReportOutputXLSProcessor Service with payload = " + jsonDataIn.toString());

					WSReportOutputXLSDataOut dataOutReportXLS = producerTemplate.requestBody("direct:WSReportOutputXLSProcessor", jsonDataIn,
							WSReportOutputXLSDataOut.class);
					if(dataOutReportXLS.getWsResponse().getWsProcessingStatus().equalsIgnoreCase("1")) {

						String currDtString = simpleDateFormat1.format(new Date());

						updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
								+ "ReportGenerated=?,ReportEndTimeStamp=?,ReportFileLocation=? WHERE ReportRequestID=?");

						argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Report generated Successfully"));
						argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
						argValuePairsList.add(prepareArgValuePairsObj("2", "string", "1"));
						argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
						argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportFileLocation()+"\\"+dataOutReportXLS.getWsResponse().getStrFileName()));
						argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportRequestID()));

						updateDataIn.setArgValuePairs(argValuePairsList);

						boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

						if(updateGenRepStatus) {
							wsResponse.setWsProcessingStatus("1");
							wsResponse.setWsSuccessMessage("Report generated Successfully");
						} else {
							wsResponse.setWsProcessingStatus("2");
							wsResponse.setWsExceptionMessage("Report generated successfully, but Exception response form WSUpdateData service");
						}							
					}
					if(dataOutReportXLS.getWsResponse().getWsProcessingStatus().equalsIgnoreCase("2")) {

						String currDtString = simpleDateFormat1.format(new Date());

						updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
								+ "ReportGenerated=?,ReportEndTimeStamp=?,ReportFileLocation=? WHERE ReportRequestID=?");

						argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Exception while report generation: "+dataOutReportXLS.getWsResponse().getWsExceptionMessage()));
						argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
						argValuePairsList.add(prepareArgValuePairsObj("2", "string", "0"));
						argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
						argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportFileLocation()));
						argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportRequestID()));

						updateDataIn.setArgValuePairs(argValuePairsList);

						boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

						if(updateGenRepStatus) {
							wsResponse.setWsProcessingStatus("2");
							wsResponse.setWsExceptionMessage("Exception while report generation: "+dataOutReportXLS.getWsResponse().getWsExceptionMessage());		
						} else {
							wsResponse.setWsProcessingStatus("2");
							wsResponse.setWsExceptionMessage("Exception while report generation and WSUpdateData service: "+dataOutReportXLS.getWsResponse().getWsExceptionMessage());
						}
					}
				}
				if(dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("web")) {
					if(dataIn.getWsInput().getDtFromDate() != null && !dataIn.getWsInput().getDtFromDate().equalsIgnoreCase("") && 
							dataIn.getWsInput().getDtToDate() != null && !dataIn.getWsInput().getDtToDate().equalsIgnoreCase("")) {
						List<PendingCaseClaimData> pendingCaseClaimDataList = new ArrayList<PendingCaseClaimData>();
						pendingCaseClaimDataList = claimDetailsDao.getPendingCases("web", dataIn.getWsInput().getDtFromDate(), dataIn.getWsInput().getDtToDate());
						List webReportDataModelList = new ArrayList<WebReportDataModel>();
						List wsResponseDataList = new ArrayList<List>();

						for (PendingCaseClaimData pendingCaseClaimData : pendingCaseClaimDataList) {
							log.debug("***** pendingCaseClaimData web *****");
							log.debug(pendingCaseClaimData.toString());

							PolicyDetails policyDetails = new PolicyDetails();
							policyDetails = odsDao.getPolicyDetailsForReport(pendingCaseClaimData.getPolicyNumber());
							String pendingType = claimDetailsDao.getPendingType(pendingCaseClaimData.getClaimNumber());
							List<RequirementsDetails> requirementsDetailsList = claimDetailsDao.getFUPDescription(pendingCaseClaimData.getClaimNumber());

							for (RequirementsDetails requirementsDetails : requirementsDetailsList) {
								String docName = odsDao.getReqDocName(requirementsDetails.getDocID());
								webReportDataModelList.add(prepereWebReport("Policy Number", pendingCaseClaimData.getPolicyNumber()));
								webReportDataModelList.add(prepereWebReport("Policy Status", policyDetails.getPOLICYSTATUS()));
								webReportDataModelList.add(prepereWebReport("Claim Number", pendingCaseClaimData.getClaimNumber()));
								webReportDataModelList.add(prepereWebReport("Claim Type", pendingCaseClaimData.getClaimTypeUI()));
								webReportDataModelList.add(prepereWebReport("Claim Status", pendingType));

								webReportDataModelList.add(prepereWebReport("Pending request date", requirementsDetails.getRequirementRequestTS() != null ? 
										simpleDateFormat.format(requirementsDetails.getRequirementRequestTS()).toString() : ""));
								webReportDataModelList.add(prepereWebReport("Pending complete date", requirementsDetails.getRequirementReceiveTS() != null ? 
										simpleDateFormat.format(requirementsDetails.getRequirementReceiveTS()).toString() : ""));

								if(requirementsDetails.getRequirementRequestTS() != null && requirementsDetails.getRequirementReceiveTS() != null) {
									int pendDAys = getDiffDays(requirementsDetails.getRequirementReceiveTS(), requirementsDetails.getRequirementRequestTS());
									webReportDataModelList.add(prepereWebReport("Pending Days", String.valueOf(pendDAys)));
								}else {
									webReportDataModelList.add(prepereWebReport("Pending Days", ""));
								}

								webReportDataModelList.add(prepereWebReport("DocID", requirementsDetails.getDocID()));
								webReportDataModelList.add(prepereWebReport("Document Name", docName));	
							}

							wsResponseDataList.add(webReportDataModelList);
						}
						
						JSONArray jsArray = new JSONArray(wsResponseDataList);
						wsResponse.setWsProcessingStatus("1");
						wsResponse.setWsSuccessMessage("Success");
						wsResponse.setStrReportData(jsArray.toString());

					}else {
						wsResponse.setWsProcessingStatus("2");
						wsResponse.setWsExceptionMessage("From date and to date are mandatory for generating the report");
					}
				}
			}
			if(dataIn.getWsInput().getStrReportType().equalsIgnoreCase("ps")) {

			}
		}catch(Exception e) {
			log.error("WSGeneratePendingCaseReportProcessor Error" + e);
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsExceptionMessage(e.getMessage());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("Final Response " + wsResponse.toString());
		exchange.getOut().setBody(wsResponse);
	}

	public int getDiffDays(Date higherDate, Date lowerDate) {
		Calendar higherDateCalendar = Calendar.getInstance();
		Calendar lowerDateDateCalendar = Calendar.getInstance();
		higherDateCalendar.setTime(higherDate);
		lowerDateDateCalendar.setTime(lowerDate);

		DateTime higherDateTs = new DateTime(higherDateCalendar.get(higherDateCalendar.YEAR), higherDateCalendar.get(higherDateCalendar.MONTH)+1, higherDateCalendar.getTime().getDate(), 0, 0);
		DateTime lowerDateTs = new DateTime(lowerDateDateCalendar.get(lowerDateDateCalendar.YEAR), lowerDateDateCalendar.get(lowerDateDateCalendar.MONTH)+1, lowerDateDateCalendar.getTime().getDate(), 0, 0);
		int diffInDays = Days.daysBetween(higherDateTs, lowerDateTs).getDays();
		return diffInDays;
	}

	public static XlsColValMap prepGenericClassObj(int colNum, String colValue) {
		XlsColValMap xlsColValMap = new XlsColValMap();
		xlsColValMap.setColNum(colNum);
		xlsColValMap.setColValue(colValue);
		return xlsColValMap;
	}

	public ArgValuePairs prepareArgValuePairsObj(String index, String paramType,  String value) {
		ArgValuePairs argValuePairsObj = new ArgValuePairs();
		argValuePairsObj.setIndex(index);
		argValuePairsObj.setTypeParam(paramType);
		argValuePairsObj.setValue(value);
		return argValuePairsObj;
	}

	public boolean invokeUpdateDataService(WSUpdateDataDataIn updateDataIn) {
		log.info("Invoking the WSUpdateData Service with payload = " + updateDataIn.toString());

		JsonResponse wsUpdateDataResponse = producerTemplate.requestBody("direct:WSUpdateDataProcessor", updateDataIn,
				JsonResponse.class);

		if(wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("1")) {
			log.info("Status updated in CASEREPORTREQUEST table successfully");
			return true;
		}
		if(wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("2")) {
			log.error("Exception updating status in CASEREPORTREQUEST table");
			return false;
		}
		return false;
	}

	public WebReportDataModel prepereWebReport(String colName, String colValue) {
		WebReportDataModel webReportDataModel = new WebReportDataModel();

		webReportDataModel.setStrColName(colName);
		webReportDataModel.setStrColValue(colValue);

		return webReportDataModel;
	}

}
