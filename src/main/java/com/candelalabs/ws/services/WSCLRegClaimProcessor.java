package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
//removable packages
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSCLRegClaimDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSCLRegClaimMessageModel;
import com.candelalabs.ws.model.bomessages.WSCLRegClaimRecuringMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.HospitalMasterDetails;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
@PropertySource("classpath:application.properties")
public class WSCLRegClaimProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	private static Log log = LogFactory.getLog("WSCLRegClaimProcessor");

	@Value("${USRPRF}")
	private String USRPRF;

	public void process(Exchange exchange) throws Exception {
		log.debug("WSCLRegClaimProcessor - In Method - process");

		Gson gson = new GsonBuilder().serializeNulls().create();
		JsonResponse jsonoutData = new JsonResponse();

		try {
			PaddingUtil paddingUtil = new PaddingUtil();
			WSCLRegClaimDataIn jsonWSCLRegClaimDataIn = (exchange.getIn().getBody(WSCLRegClaimDataIn.class));
			log.debug("Received the payload= " + jsonWSCLRegClaimDataIn.toString());
			if (jsonWSCLRegClaimDataIn.getACTIVITYID() != null && jsonWSCLRegClaimDataIn.getCLAIMNO() != null
					&& jsonWSCLRegClaimDataIn.getACTIVITYID() != "" && jsonWSCLRegClaimDataIn.getCLAIMNO() != "") {

				// log.info("Get LaRequestTrackerModel table with claimNo= " +
				// jsonWSCLRegClaimDataIn.getCLAIMNO());
				// LaRequestTrackerModel laRequestTrackerData =
				// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
				// jsonWSCLRegClaimDataIn.getCLAIMNO(),
				// jsonWSCLRegClaimDataIn.getACTIVITYID(),
				// FWDConstants.BO_IDENTIFIER_Cashless_RegisterRegularClaimHS);
				// log.info("Successfully Got LaRequestTrackerModel table ");
				//
				// if (laRequestTrackerData.getBoidentifier() == null &&
				// laRequestTrackerData.getMsgtosend() == null
				// && laRequestTrackerData.getMsgcreatets() == null
				// && laRequestTrackerData.getFuturemesg() == null) {

				WSCLRegClaimMessageModel boMessage = new WSCLRegClaimMessageModel();

				boMessage.setUSRPRF(this.USRPRF);
				log.info("Get ClaimData table with claimNo= " + jsonWSCLRegClaimDataIn.getCLAIMNO());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(jsonWSCLRegClaimDataIn.getCLAIMNO());
				log.info("Successfully retrieved ClaimData table");

				log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
				PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
				log.info("Successfully retrieved PolicyDetail table");

				if (!(Optional.ofNullable(claimData).isPresent() && Optional.ofNullable(policyDetail).isPresent())) {
					throw new DAOException("ClaimData is null with Claim NO= " + jsonWSCLRegClaimDataIn.getCLAIMNO());
				}
				;

				boMessage.setCHDRCOY(policyDetail.getCOMPANY());
				log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
				boMessage.setCLAIMNO(jsonWSCLRegClaimDataIn.getCLAIMNO());
				log.debug("CLAIMNO=" + jsonWSCLRegClaimDataIn.getCLAIMNO());
				boMessage.setCHDRSEL(claimData.getPolicyNumber());
				log.debug("CHDRSEL=" + claimData.getPolicyNumber());

				log.info("Get ComponentDetails table");
				ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
						claimData.getComponentCode(), claimData.getInsuredClientID());
				log.info("Successfully retrieved ComponentDetails table");

				if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
					throw new DAOException(
							"componentDetailsBO is null with Claim NO= " + jsonWSCLRegClaimDataIn.getCLAIMNO());
				}
				;

				boMessage.setLIFE(componentDetailsBO.getLife());
				log.debug("LIFE=" + componentDetailsBO.getLife());

				boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
				log.debug("COVERAGE=" + componentDetailsBO.getCoverage());

				boMessage.setRIDER(componentDetailsBO.getRider());
				log.debug("RIDER=" + componentDetailsBO.getRider());

				boMessage.setCRTABLE(claimData.getComponentCode());
				log.debug("CRTABLE=" + claimData.getComponentCode());

				boMessage.setCLAMPARTY(claimData.getInsuredClientID());
				log.debug("CLAMPARTY=" + claimData.getInsuredClientID());

				boMessage.setCLTYPE(claimData.getClaimTypeLA());
				log.debug("CLTYPE=" + claimData.getClaimTypeLA());

				boMessage.setZDIAGCDE(claimData.getDiagnosisCode());
				log.debug("ZDIAGCDE=" + claimData.getDiagnosisCode());

				log.info("Get HospitalMaster table with providercode= " + claimData.getProviderCode());
				HospitalMasterDetails hospitalMaster = odsDao.getHospitalMasterDetails(claimData.getProviderCode());
				log.info("Successfully retrieved HospitalMaster table");
				if (!(Optional.ofNullable(hospitalMaster).isPresent())) {
					throw new DAOException("hospitalMaster is null with ProviderCode= " + claimData.getProviderCode());
				}
				;

				boMessage.setZMEDPRV(claimData.getProviderCode());
				log.debug("ZMEDPRV=" + claimData.getProviderCode());

				boMessage.setCLAIMCUR(policyDetail.getCURRENCY());
				log.debug("CLAIMCUR=" + policyDetail.getCURRENCY());

				boMessage.setPAYCLT(hospitalMaster.getPROVIDERCLIENTNUMBER());
				log.debug("PAYCLT=" + hospitalMaster.getPROVIDERCLIENTNUMBER());

				boMessage.setBANKKEY(claimData.getBankCode());
				log.debug("BANKKEY=" + claimData.getBankCode());

				boMessage.setFACTHOUS(claimData.getFactoringHouse());
				log.debug("FACTHOUS=" + claimData.getFactoringHouse());

				boMessage.setBANKACOUNT(claimData.getBankAccountNumber());
				log.debug("BANKACOUNT=" + claimData.getBankAccountNumber());

				boMessage.setBANKACCDSC(claimData.getBankAccountName());
				log.debug("BANKACCDSC=" + claimData.getBankAccountName());

				boMessage.setBNKACTYP(claimData.getBankType());
				log.debug("BNKACTYP=" + claimData.getBankType());

				boMessage.setZCLMRECD(Util.dateToString(claimData.getReceivedDate(), "yyyyMMdd"));
				log.debug("ZCLMRECD=" + Util.dateToString(claimData.getReceivedDate(), "yyyyMMdd"));

				boMessage.setINCURDT(Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));
				log.debug("INCURDT=" + Util.dateToString(claimData.getAdmissionDate(), "yyyyMMdd"));

				boMessage.setDISCHDT(Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));
				log.debug("DISCHDT=" + Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));

				log.info("Get claimDataDetails table with claimNo= " + jsonWSCLRegClaimDataIn.getCLAIMNO());
				List<ClaimDataDetailsModel> claimDataDetails_li = new ArrayList<ClaimDataDetailsModel>();
				claimDataDetails_li = this.claimDetailsDao
						.getClaimDataDetailsByClaimNo(jsonWSCLRegClaimDataIn.getCLAIMNO());
				log.info("Successfully retrieved claimDataDetails table");
				if (claimDataDetails_li.size() <= 0) {
					throw new DAOException(
							"ClaimDataDetails is null with claimNo= " + jsonWSCLRegClaimDataIn.getCLAIMNO());
				}
				;
				List<WSCLRegClaimRecuringMessageModel> WSCLRegClaim_li = new ArrayList<WSCLRegClaimRecuringMessageModel>();
				int count = 0;
				for (ClaimDataDetailsModel claimDataDetails : claimDataDetails_li) {
					WSCLRegClaimRecuringMessageModel model = new WSCLRegClaimRecuringMessageModel();
					model.setHOSBEN(claimDataDetails.getBenefitCode() == null ? "" : claimDataDetails.getBenefitCode());
					log.debug("HOSBEN=" + claimDataDetails.getBenefitCode());
					model.setDATEFRM(Util.dateToString(claimDataDetails.getBenefitFromDate(), "yyyyMMdd"));
					log.debug("DATEFRM=" + Util.dateToString(claimDataDetails.getBenefitFromDate(), "yyyyMMdd"));
					model.setDATETO(Util.dateToString(claimDataDetails.getBenefitToDate(), "yyyyMMdd"));
					log.debug("DATEtO=" + Util.dateToString(claimDataDetails.getBenefitToDate(), "yyyyMMdd"));

					model.setACTEXP((claimDataDetails.getBenefitAmountPerDay() == null ? new BigDecimal(0)
							: claimDataDetails.getBenefitAmountPerDay()));
					log.debug("ACTEXP=" + claimDataDetails.getBenefitAmountPerDay());

					model.setZNODAY(claimDataDetails.getBenefitActualDays() == null ? new BigDecimal(0)
							: claimDataDetails.getBenefitActualDays());
					log.debug("ZNODAY=" + claimDataDetails.getBenefitActualDays());

					model.setTACTEXP(claimDataDetails.getBenefitTotalIncurredAmount() == null ? new BigDecimal(0)
							: claimDataDetails.getBenefitTotalIncurredAmount());
					log.debug("TACTEXP=" + claimDataDetails.getBenefitTotalIncurredAmount());

					model.setGCNETPY(claimDataDetails.getBenefitUserApprovedAmount() == null ? new BigDecimal(0)
							: claimDataDetails.getBenefitUserApprovedAmount());
					log.debug("GCNETPY=" + claimDataDetails.getBenefitUserApprovedAmount());

					WSCLRegClaim_li.add(model);
					count = count + 1;
				}
				if (count < 28) {
					for (int t = 0; t < (28 - count); t++) {
						WSCLRegClaimRecuringMessageModel model = new WSCLRegClaimRecuringMessageModel();
						model.setHOSBEN("");
						model.setDATEFRM("");
						model.setDATETO("");
						model.setACTEXP(new BigDecimal(0));
						model.setZNODAY(new BigDecimal(0));
						model.setTACTEXP(new BigDecimal(0));
						model.setGCNETPY(new BigDecimal(0));
						WSCLRegClaim_li.add(model);
					}
				} else if (count > 28) {
					throw new CamelException("ClaimDataDetails data is exceed 28 size = " + count);
				}

				boMessage.setwSCLRegClaimRecuring_li(WSCLRegClaim_li);

				log.info("BOMESSAGE RESULT = " + boMessage.getBOMessage());
				log.info("Inserting to LAREQUESTTRACKER table with claimNO= " + jsonWSCLRegClaimDataIn.getCLAIMNO());
				int i = this.claimDetailsDao.lARequestTracker_Insert(jsonWSCLRegClaimDataIn.getCLAIMNO(),
						jsonWSCLRegClaimDataIn.getACTIVITYID(), boMessage.getBOIDEN(), boMessage.getBOMessage(), "0");
				if (i == 0) {
					throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
				}
				// }

				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");

			} else {
				throw new CamelException("Request Input message is empty");
			}

		} catch (Exception e) {
			log.error("Error " + e);
			// log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}

			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error processing= " + e.getMessage());

		}
		log.info("DataOut= " + jsonoutData.toString());
		log.info("##################################################################################3");

		exchange.getOut().setBody(jsonoutData);

	}

}
