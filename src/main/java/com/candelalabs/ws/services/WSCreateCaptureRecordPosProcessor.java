package com.candelalabs.ws.services;

import java.util.Date;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSCommonDataOutWSResponse;
import com.candelalabs.api.model.WSCreateCaptureRecordPosDataIn;
import com.candelalabs.epos.model.POSDataSourceCapture;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.PosDetailsDao;

@Component
public class WSCreateCaptureRecordPosProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSCreateCaptureRecordPosProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	PosDetailsDao posDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("WSCreateCaptureRecordPos - Service is invoked");
		WSCommonDataOutWSResponse wsResponse = new WSCommonDataOutWSResponse();
		WSCreateCaptureRecordPosDataIn dataInput = exchange.getIn().getBody(WSCreateCaptureRecordPosDataIn.class);
		log.info("WSCreateCaptureRecordPosProcessor - Json Object as string = " + dataInput.toString());
		try {
			JSONArray wsInput = new JSONArray(dataInput.getWsInput());
			log.debug("wsInput : "+wsInput.toString());
			String exceptionRecord = null;
			String exceptionMessage = null;
			String duplicateMessage = null;
			boolean isAllClaimsValid = true;
			for (int j = 0; j < wsInput.length(); j++){
				POSDataSourceCapture posDataSourceCapture = new POSDataSourceCapture();
				JSONObject trnxData = wsInput.getJSONObject(j);
				log.debug("Current record been processed : "+ trnxData.toString());
				if(trnxData.getString("strPolicyNumber").equalsIgnoreCase("") || trnxData.getString("strPolicyNumber") == null ||
						trnxData.getString("strRequestNumber").equalsIgnoreCase("") || trnxData.getString("strRequestNumber") == null) {
					exceptionRecord = "Y";
				}

				if(exceptionRecord == "Y") {
					isAllClaimsValid = false;
					if(exceptionMessage == null) {
						exceptionMessage = "Input with following Data is not stored due incorrect / incomplete data. ";
					}

					exceptionMessage += "Policy Number = "+posDataSourceCapture.getPolicyNumber()+", Batch Number = "+posDataSourceCapture.getCaseReferenceNumber();
				}else {
					log.info("Current record been processed, PolicyNumber : "+trnxData.getString("strPolicyNumber")+" and RequestNumber : "+trnxData.getString("strRequestNumber"));
					posDataSourceCapture.setPolicyNumber(trnxData.getString("strPolicyNumber"));
					posDataSourceCapture.setCaseReferenceNumber(trnxData.getString("strRequestNumber"));
					posDataSourceCapture.setTransactionType(trnxData.getString("strTransactionType"));
					posDataSourceCapture.setTransactionSource(trnxData.getString("strChannelType"));
					posDataSourceCapture.setRecordCreationTS(new Date());
					posDataSourceCapture.setExceptionRecord("N");
					posDataSourceCapture.setProcessRecord("N");
					posDataSourceCapture.setTransactionData(trnxData.toString());

					int count = claimDetailsDao.checkDuplicateCaptureRecord(posDataSourceCapture.getCaseReferenceNumber(), posDataSourceCapture.getPolicyNumber());
					if(count > 0) {
						isAllClaimsValid = false;
						if(duplicateMessage == null) {
							duplicateMessage = "Duplicate Request found for following data";
						}
						duplicateMessage += ":: Policy Number = "+posDataSourceCapture.getPolicyNumber()+" and Batch Number = "+ posDataSourceCapture.getCaseReferenceNumber();
						claimDetailsDao.insertIntoCaptureBatchDetails(posDataSourceCapture.getCaseReferenceNumber(), posDataSourceCapture.getPolicyNumber(), duplicateMessage, "Y");
					}else {
						int recordInsertCount = 0;
						recordInsertCount = posDetailsDao.insertPosDataSourceCapture(posDataSourceCapture);
						if(recordInsertCount > 0) {
							claimDetailsDao.insertIntoCaptureBatchDetails(posDataSourceCapture.getCaseReferenceNumber(), posDataSourceCapture.getPolicyNumber(), "", "N");
						}
					}
				}

			}

			if(isAllClaimsValid) {
				wsResponse.setWsProcessingStatus("1");
				wsResponse.setWsExceptionMessage("");
				wsResponse.setWsSuccessMessage("Successfully Created Records for the Input received");
			} else {
				wsResponse.setWsProcessingStatus("2");
				wsResponse.setWsExceptionMessage("Some of the input were processed successfully. Some input could not be processed due to the missing Information. Details for records which could not created are listed with this message :: "+exceptionMessage +","+ duplicateMessage);
				wsResponse.setWsSuccessMessage("");
			}
		}catch(Exception e) {
			log.error("WSPortalServiceCreateClaim Error - " + e);
			wsResponse.setWsExceptionMessage("Some of the input were processed successfully. Some input could not be processed due to the missing Information. Details for records which could not created are listed with this message :: "+e+" - "+e.getMessage());
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsSuccessMessage("");
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}

		log.info("Final Response " + wsResponse.toString());

		exchange.getOut().setBody(wsResponse);
	}

}
