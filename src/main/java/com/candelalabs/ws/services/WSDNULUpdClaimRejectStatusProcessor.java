package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDNULUpdClaimRejectStatusDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.Output;
import com.candelalabs.ws.model.WSDNULUpdClaimRejectStatusClaimsDataBOData;
import com.candelalabs.ws.model.WSDNULUpdClaimRejectStatusClaimsDetailsBOData;
import com.candelalabs.ws.model.bomessages.WSDNULUpdClaimRejectStatusMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
@PropertySource("classpath:application.properties")
public class WSDNULUpdClaimRejectStatusProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	private static Log log = LogFactory.getLog("WSDNULUpdClaimRejectStatusProcessor");
	ObjectMapper mapper = new ObjectMapper();
	// TODO Auto-generated method stub

	@Value("${USRPRF}")
	private String USRPRF;

	public void process(Exchange exchange) throws Exception {

		Gson gson = new GsonBuilder().serializeNulls().create();
		JsonResponse jsonoutData = new JsonResponse();

		try {

			WSDNULUpdClaimRejectStatusMessageModel boMessage = new WSDNULUpdClaimRejectStatusMessageModel();
			WSDNULUpdClaimRejectStatusDataIn jsonWSDNULUpdClaimRejectStatusDataIn = (exchange.getIn()
					.getBody(WSDNULUpdClaimRejectStatusDataIn.class));
			log.info("Input Payload =" + jsonWSDNULUpdClaimRejectStatusDataIn.toString());
			if (jsonWSDNULUpdClaimRejectStatusDataIn.getACTIVITYID() != null
					&& jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO() != null
					&& jsonWSDNULUpdClaimRejectStatusDataIn.getACTIVITYID() != ""
					&& jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO() != "") {

//				log.info("Get LaRequestTrackerModel table with claimNo= "
//						+ jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO());
//				LaRequestTrackerModel laRequestTrackerData = this.claimDetailsDao.getLaRequestTrackerByClaimNo(
//						jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO(),
//						jsonWSDNULUpdClaimRejectStatusDataIn.getACTIVITYID(),
//						FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimRejected);
//				log.info("Successfully Got LaRequestTrackerModel table ");
//
//				if (laRequestTrackerData.getBoidentifier() == null && laRequestTrackerData.getMsgtosend() == null
//						&& laRequestTrackerData.getMsgcreatets() == null
//						&& laRequestTrackerData.getFuturemesg() == null) {

					boMessage.setUSRPRF(this.USRPRF);

					log.info("Get ClaimData table with claimNo= " + jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO());
					ClaimDataNewModel claimData = this.claimDetailsDao
							.getClaimDataNew(jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO());
					log.info("Successfully retrieved ClaimData table");

					log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
					PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
					log.info("Successfully retrieved PolicyDetail table");

					boMessage.setCHDRCOY(policyDetail.getCOMPANY());
					log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
					boMessage.setCHDRSEL(claimData.getPolicyNumber());
					log.debug("CHDRSEL=" + claimData.getPolicyNumber());

					boMessage.setCANCELDATE(Util.dateToString(claimData.getClaimDecisionDate(), "yyyyMMdd"));
					log.debug("CANCELDATE=" + Util.dateToString(claimData.getClaimDecisionDate(), "yyyyMMdd"));

					boMessage.setCLAIMNO(jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO());
					log.debug("CLAIMNO=" + jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO());

					log.info("Getting te DocIDList");;
					String docIdList = this.claimDetailsDao
							.getDocIDList(jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO(), "Y");
					log.info("Successfully got the docIdList = "+docIdList);
					log.info("Getting the subCategoryList");
					String subCategoryList = this.odsDao.getListSubcategory(docIdList);
					log.info("SUccessfully got the subCategoryList = "+subCategoryList);
					log.info("Updating Notification Data");
					int rowUp = this.claimDetailsDao
							.updateNotificationData(jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO(), subCategoryList);
					if (rowUp == 0) {
						throw new DAOException("No row is updated for NOTIFICATIONDATA TABLE");
					}
					log.info("Successfully updating the NotificationData");

				log.info("BOMESSAGE RESULT = " + boMessage.getBoMessage());
				log.info("Inserting to LAREQUESTTRACKER table with claimNO= "
						+ jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO());
				int i = this.claimDetailsDao.lARequestTracker_Insert(
						jsonWSDNULUpdClaimRejectStatusDataIn.getCLAIMNO(),
						jsonWSDNULUpdClaimRejectStatusDataIn.getACTIVITYID(), boMessage.getBOIDEN(),
						boMessage.getBoMessage(), "0");
				if (i == 0) {
					throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
				}
				log.info("Successfully inserted the LAREQUESTTRACKER table");
			//	}

				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");

			} else {
				throw new CamelException("Request Input message is empty");
			}

		} catch (Exception e) {
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}

		String finalresult = jsonoutData.toString();
		log.info("WSDNULUpdClaimProcessor - In Method - process - response in String # " + finalresult);
		exchange.getOut().setBody(jsonoutData);

	}

}
