package com.candelalabs.ws.services;

import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.impl.Log4JLogger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

//import org.apache.commons.logging.Log;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.api.model.*;
//import com.candelalabs.ws.schema.stubs.*;
//import com.candelalabs.ws.schema.stubs.WSUpdateClaimStatusResponseType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class WSUpdateClaimSTPStatus implements Processor {
	
	private static final Log log = LogFactory.getLog("WSUpdateClaimSTPStatus");

	@Autowired
	private ODSDao oDSDao;
	
	@Autowired
	private ClaimDetailsDao claimDAO;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("***********************************WSUpdateClaimSTPStatus************************************************** \n"
				+ "Service is Invoked");

		WSUpdateClaimSTPStatusDataOut wsresponse = new WSUpdateClaimSTPStatusDataOut();
		WSUpdateClaimSTPStatusDataOutWSResponse response =null;
		response = new WSUpdateClaimSTPStatusDataOutWSResponse();
		try {
			int statusUpdated =0;
			WSUpdateClaimSTPStatusDataIn dataIn = exchange.getIn()
					.getBody(WSUpdateClaimSTPStatusDataIn.class);
			log.info("WSUpdateClaimSTPStatus---JSON INPUT PAYLOAD = " + dataIn.toString());

			if (dataIn.getWsInput().getStrClaimNumber() != "" && dataIn.getWsInput().getStrClaimNumber() != null) {
				log.info("Updating the STP status ");
				statusUpdated=claimDAO.updateSTPStatus(dataIn.getWsInput().getStrClaimNumber(),
						Integer.valueOf(dataIn.getWsInput().getStrClaimSTPStatus().equalsIgnoreCase("STP") ? "1":"0"));
				
				log.info("ClaimSTPStatus updated with return value  = " + statusUpdated);

				if (statusUpdated > 0 ) {
					response.setWsExceptionMessage("");
					response.setWsProcessStatus("1");
					response.setWsSuccessMessage("Claim STP Status updated");
					wsresponse.setWsResponse(response);
					

				} 
				else
				{
					
					throw new DAOException("STP Status not updated!!!");
				}
			}
			else 
			{
				throw new CamelException("Request Input is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = " + e);
			response.setWsProcessStatus("2");
			response.setWsSuccessMessage("");
			response.setWsExceptionMessage("Exception in updating Claim STP Status : " + e.getMessage());
			wsresponse.setWsResponse(response);

		}
		log.info("JSONRESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);
	}

}
