package com.candelalabs.ws.services;

import com.candelalabs.api.model.*;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triaji K.
 */
@Component
public class WSProcessRuleRecordProcessor implements Processor {

    /*@Autowired
    ProducerTemplate producerTemplate;*/
    private static final Log log = LogFactory.getLog("WSProcessRuleRecordProcessor");

    @Autowired
    ClaimDetailsDao claimDetailsDao;

    @Override
    public void process(Exchange exchange) throws Exception {
        JsonResponse response = new JsonResponse();
        try {
            WSProcessRuleRecordDataIn dataIn =  exchange.getIn().getBody(WSProcessRuleRecordDataIn.class);
            log.info("Input object:"+dataIn.toString());
            List<WSProcessRuleRecordDataInWSInput> data_li = dataIn.getWsInput();

            String sqlQuery = "insert into CASERULEDETAILS(ProcessType,ProcessKeyReferenceNumber,RuleType,RuleDescription," +
                    "RuleStatus,RuleReason,RuleProcessingStartTimeStamp,RuleProcessingEndTimeStamp)" +
                    " values(?,?,?,?,?,?,?,?)";
            for (WSProcessRuleRecordDataInWSInput data : data_li) {
                Object[] obj_ar = new Object[8];
                obj_ar[0] = data.getStrProcessType();
                obj_ar[1] = data.getStrProcessKeyReferenceNumber();
                obj_ar[2] = data.getStrRuleType();
                obj_ar[3] = data.getStrRuleDescription();
                obj_ar[4] = data.getStrRuleStatus();
                obj_ar[5] = data.getStrRuleReason();
                obj_ar[6] = data.getStrRuleProcessingStartTimeStamp();
                obj_ar[7] = data.getStrRuleProcessingEndTimeStamp();

                if (this.claimDetailsDao.updateGenericDB(sqlQuery,obj_ar) == 0) {
                    throw new Exception("Failing at inserting the record into the table");
                }

               /* WSGenericServiceDataIn dataInGeneric  = new WSGenericServiceDataIn();
                dataInGeneric.setSqlQuery(sqlQuery);
                List<WSGenericServiceDataInArgValuePairs> argValue_li = new ArrayList<>();
                WSGenericServiceDataInArgValuePairs argValue = new WSGenericServiceDataInArgValuePairs();
                argValue.setIndex(0);
                argValue.setTypeParam("string");
                argValue.setValue(dataIn.getProcessType());
                argValue_li.add(argValue);

                argValue.setIndex(1);
                argValue.setTypeParam("string");
                argValue.setValue(dataIn.getProcessKeyReferenceNumber());
                argValue_li.add(argValue);

                argValue.setIndex(2);
                argValue.setTypeParam("string");
                argValue.setValue(dataIn.getRuleType());
                argValue_li.add(argValue);

                argValue.setIndex(3);
                argValue.setTypeParam("string");
                argValue.setValue(ruleKeyValue.getRuleDescription());
                argValue_li.add(argValue);


                JsonResponse response1 = producerTemplate.requestBody("direct:WSGenericUpdateService", dataInGeneric,
                        JsonResponse.class);*/

            }
            response.setWsProcessingStatus("1");
            response.setWsSuccessMessage("All the record(s) have been successfully inserted into the table");

        } catch (Exception e) {
            response.setWsProcessingStatus("2");
            response.setWsExceptionMessage("Error processing= " + e.getMessage());
            log.error("Error Detail= " + e);
            log.error("ERROR DETAIL: " + e.getStackTrace());
            for (StackTraceElement tr : e.getStackTrace()) {
                log.error("\tat " + tr);
            }
        }
        log.info("Response: "+response.toString());

        exchange.getOut().setBody(response);

    }
}
