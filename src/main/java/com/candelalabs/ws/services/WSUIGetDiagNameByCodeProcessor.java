/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetDiagnosisNameByCodeWSInput;
import com.candelalabs.api.model.DataOutGetDiagnosisNameByCodeWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.DiagnosisMasterModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetDiagNameByCodeProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetDiagNameByCodeProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("************************************************************************************* \n"
				+ "Service is Invoked");
		DataOutGetDiagnosisNameByCodeWSResponse wsresponse = new DataOutGetDiagnosisNameByCodeWSResponse();

		try {
			DataInGetDiagnosisNameByCodeWSInput dataIn = exchange.getIn()
					.getBody(DataInGetDiagnosisNameByCodeWSInput.class);
			log.info("JSON INPUT PAYLOAD: "+dataIn.toString());
			if (dataIn.getStrClaimTypeUI().trim() != "" && dataIn.getStrClaimTypeUI() != null
					&& dataIn.getStrDiagnosisCode() != null && dataIn.getStrDiagnosisCode().trim() != "") {
				DiagnosisMasterModel ciMasterModel = this.oDSDao.getDiagnosisModelByCode(dataIn.getStrDiagnosisCode(),
						dataIn.getStrClaimTypeUI());
				if (dataIn.getStrClaimTypeUI().equalsIgnoreCase("CI")
						|| dataIn.getStrClaimTypeUI().equalsIgnoreCase("C1")) {
					if (Optional.ofNullable(ciMasterModel).isPresent()) {
						wsresponse.setIClaimSumAssuredPercent(ciMasterModel.getCLAIMPERCENTAGE());
						wsresponse.setStrClaimTypeUI(ciMasterModel.getCLAIMTYPE());
						wsresponse.setStrDiagnosisName(ciMasterModel.getDIAGNOSISNAME());
					} else {
						throw new DAOException(
								"Not found the Diagnosis Name by code = " + dataIn.getStrDiagnosisCode());
					}
				} else {
					wsresponse.setStrDiagnosisName(ciMasterModel.getDIAGNOSISNAME());
					wsresponse.setIClaimSumAssuredPercent(dataIn.getIClaimSumAssuredPercent());
					wsresponse.setStrClaimTypeUI(dataIn.getStrClaimTypeUI());
				}

				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Success");

			} else {
				throw new CamelException("Request String is empty");

			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR: "+e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR = " + e.getMessage());
		}

		log.info("JSON RESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);
	}

}
