/**
 * 
 */
package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSUpdateReqtActivityIDDataIn;
import com.candelalabs.api.model.WSUpdateReqtActivityIDDataInWSInput;
import com.candelalabs.ws.dao.ClaimDetailsDao;

/**
 * @author Triaji
 *
 */
@Component
public class WSUpdateReqActivityIDProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSUpdateReqActivityIDProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse responseBoi = new JsonResponse();
		try {
			WSUpdateReqtActivityIDDataIn wsInput = exchange.getIn().getBody(WSUpdateReqtActivityIDDataIn.class);
			log.info("JSON INPUT PAYLOAD = " + wsInput.toString());
			WSUpdateReqtActivityIDDataInWSInput dataIn = wsInput.getWsInput();
			if (dataIn.getStrReqtActivityID() != null && dataIn.getStrClaimNumber() != null
					&& !dataIn.getStrReqtActivityID().equalsIgnoreCase("")
					&& !dataIn.getStrClaimNumber().equalsIgnoreCase("")) {
				if (dataIn.getStrReqtActivityID().equalsIgnoreCase("recInvestigationReport")) {
					this.claimDetailsDao.updateClaimDataGeneral(dataIn.getStrClaimNumber(),
							"PendingInvesReptActivityID", dataIn.getStrReqtActivityID());
				} else {
					this.claimDetailsDao.updateClaimDataGeneral(dataIn.getStrClaimNumber(),
							"PendingRequirementActivityID", dataIn.getStrReqtActivityID());
				}
				responseBoi.setWsProcessingStatus("1");
				responseBoi.setWsSuccessMessage("Successfully Updated the RequirementActivityID");
			} else {
				throw new Exception("ReqtActivityID String is null or empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			responseBoi.setWsProcessingStatus("2");
			responseBoi.setWsExceptionMessage("Error =" + e.getMessage());
		}
		log.info("JSON RESPONSE PAYLOAD = " + responseBoi.toString());
		exchange.getOut().setBody(responseBoi);

	}

}
