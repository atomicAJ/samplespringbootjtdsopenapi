/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetDiagnosisCodeByNameWSInput;
import com.candelalabs.api.model.DataOutGetDiagnosisCodeByNameWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.DiagnosisMasterModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetDiagCodeByNameProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetDiagCodeByNameProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetDiagnosisCodeByNameWSResponse wsresponse = new DataOutGetDiagnosisCodeByNameWSResponse();

		try {
			DataInGetDiagnosisCodeByNameWSInput dataIn = exchange.getIn()
					.getBody(DataInGetDiagnosisCodeByNameWSInput.class);
			if (dataIn.getStrClaimTypeUI().trim() != "" && dataIn.getStrClaimTypeUI() != null
					&& dataIn.getStrDiagnosisName() != null && dataIn.getStrDiagnosisName().trim() != "") {
				List<DiagnosisMasterModel> diagModel = new ArrayList<DiagnosisMasterModel>();
				diagModel = this.oDSDao.getDiagnosisModelByName(dataIn.getStrDiagnosisName(),
						dataIn.getStrClaimTypeUI());
				if (dataIn.getStrClaimTypeUI().trim().equalsIgnoreCase("CI")
						|| dataIn.getStrClaimTypeUI().trim().equalsIgnoreCase("C1")) {
					if (Optional.ofNullable(diagModel).isPresent()) {
						wsresponse.setIClaimSumAssuredPercent(diagModel.get(0).getCLAIMPERCENTAGE());
						wsresponse.setStrClaimTypeUI(diagModel.get(0).getCLAIMTYPE());
						List<String> diagCode_li = new ArrayList<String>();
						diagCode_li.add(diagModel.get(0).getDIAGNOSISCODE());
						wsresponse.setStrDiagnosisCode(diagCode_li.get(0));

					} else {
						throw new DAOException(
								"Not found the Diagnosis Code by name = " + dataIn.getStrDiagnosisName());
					}

				} else {
					if (Optional.ofNullable(diagModel).isPresent()) {
						List<String> diagCode_li = new ArrayList<String>();
						for (DiagnosisMasterModel diagMst_mdl : diagModel) {
							diagCode_li.add(diagMst_mdl.getDIAGNOSISCODE());
						}
						wsresponse.setStrDiagnosisCode(diagCode_li.get(0));
						wsresponse.setIClaimSumAssuredPercent(dataIn.getIClaimSumAssuredPercent());
						wsresponse.setStrClaimTypeUI(dataIn.getStrClaimTypeUI());
					} else {
						throw new DAOException(
								"Not found the Diagnosis Code by name = " + dataIn.getStrDiagnosisName());
					}
				}
				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Success");

			} else {
				throw new CamelException("Request String is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL: " + e.getMessage());
		}
		log.info("JSON RESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);

	}

}
