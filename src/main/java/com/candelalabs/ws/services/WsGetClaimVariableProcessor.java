package com.candelalabs.ws.services;

import com.candelalabs.api.model.WsResponseDataOut;
import com.candelalabs.api.model.WsResponseDataOutWSResponse;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Map;

/**
 * @author Triaji K.
 */
@Component
public class WsGetClaimVariableProcessor implements Processor {

    private static Log log = LogFactory.getLog("WSPolicyDataRequestorProcessor");

    @Autowired
    ClaimDetailsDao claimDetailsDao;

    @Override
    public void process(Exchange exchange) throws Exception {
        WsResponseDataOut dataOut = new WsResponseDataOut();
        WsResponseDataOutWSResponse response = new WsResponseDataOutWSResponse();
        try {
            Map<String,String> dataIn = exchange.getIn().getBody(Map.class);
            log.info("=============================================" +
                    "DataInput, claimNumber:"+dataIn.get("claimnumber")+", Query String: "+dataIn.get("queryString"));

            Object obj = this.claimDetailsDao.getClaimDataVariable(dataIn.get("claimnumber"),dataIn.get("queryString"));
            response.setData(obj);
            response.setWsProcessingStatus("1");
            response.setWsSuccessMessage("Successfully fetch the variable");
        } catch (Exception e) {
            for(StackTraceElement tr :e.getStackTrace()) {
                log.error("\tat "+tr);
            }
            response.setWsProcessingStatus("2");
            response.setWsExceptionMessage("ERROR : " + e.getMessage());
        }
        log.info("JSON RESPONSE PAYLOAD: "+response.toString());
        dataOut.setWsResponse(response);
        exchange.getOut().setBody(dataOut);

    }
}
