/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataIn;
import com.candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataOut;
import com.candelalabs.api.model.WSgetComponentCodeStatusDataIn;
import com.candelalabs.api.model.WSgetComponentCodeStatusDataOut;
import com.candelalabs.api.model.WSupdateClaimProcessorEmailAddDataIn;
import com.candelalabs.api.model.WSupdateClaimUserDataIn;
import com.candelalabs.api.model.WSupdateClaimUserDataOut;
import com.candelalabs.api.model.WSupdateRemarksDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSDNULAdjClaimMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;


@Component
@PropertySource("classpath:application.properties")
public class WSgetComponentCodeStatus implements Processor {

	private static Log log = LogFactory.getLog("WSgetComponentCodeStatus");

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub  
		log.info("WSgetComponentCodeStatus -- Service is invoked");
		WSgetComponentCodeStatusDataOut jsonObjOut = new WSgetComponentCodeStatusDataOut();
		try {
			WSgetComponentCodeStatusDataIn jsonObjIn = (exchange.getIn().getBody(WSgetComponentCodeStatusDataIn.class));
			String ClientNo ="";
			String CompCode ="";
			String PolNo="";
			String  result ="";
			if (jsonObjIn!=null){
				ClientNo= jsonObjIn.getClientNo();
				log.info("WSgetComponentCodeStatus -- ClientNo is -"+ ClientNo);
				PolNo = jsonObjIn.getPolNo();
				log.info("WSgetComponentCodeStatus -- PolNo is -"+ PolNo);
				CompCode = jsonObjIn.getCompCode();
				log.info("WSgetComponentCodeStatus -- CompCode is -"+ CompCode);
				result = this.oDSDao.getComponentCodeStatusUserExit(ClientNo,PolNo,CompCode);
				jsonObjOut.setCOMPONENTSTATUS(result);
				jsonObjOut.setWsSuccessMessage("Success");
			}
			
			//jsonObjOut.setUpdateStatus(String.valueOf(result));
			jsonObjOut.setWsExceptionMessage("");
			jsonObjOut.setWsProcessingStatus("1");
				
			if ((result==null)||(result.trim().isEmpty())){
				
					jsonObjOut.setWsSuccessMessage("");
					jsonObjOut.setWsExceptionMessage("COMPONENTSTATUS is not fetched");
					jsonObjOut.setWsProcessingStatus("2");			
							}
			

			

		} catch (Exception e) {
			log.error("Error at processing: " + e);
			for(StackTraceElement tr :e.getStackTrace()) {
				log.error("\tat "+tr);
			}
			/*jsonObjOut.setWsExceptionMessage("Error " + e.getMessage());
			jsonObjOut.setWsProcessingStatus("2");*/
			//jsonObjOut.setUpdateStatus(null);
			jsonObjOut.setWsSuccessMessage(null);
			jsonObjOut.setWsExceptionMessage(e.getMessage());
			jsonObjOut.setWsProcessingStatus("2");
		}

		log.info("Json Response = " + jsonObjOut.toString());

		exchange.getOut().setBody(jsonObjOut);

	}

}
