/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSRegisterRequirementDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
public class WSRegisterRequirementProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	private static Log log = LogFactory.getLog("WSRegisterRequirementProcessor");

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse response = new JsonResponse();

		try {
			WSRegisterRequirementDataIn dataIn = exchange.getIn().getBody(WSRegisterRequirementDataIn.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
			int iLoop = 0;

			int updatedReq = 0;
			ClaimDataNewModel claimData = new ClaimDataNewModel();
			if (Optional.ofNullable(dataIn).isPresent()) {
				if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("1")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					log.info("Updating the RequirementDetail Table");
					updatedReq = this.claimDetailsDao.insertRequirementDetails(dataIn.getCLAIMNUMBER(),
							dataIn.getPOLICYNUMBER(), dataIn.getAPPNUMBER(), dataIn.getTPACLAIMNUMBER(),
							dataIn.getBATCHNUMBER(), dataIn.getPORTALREQUESTNUMBER(), dataIn.getPOSREQUESTNUMBER(),
							dataIn.getREQDOCID(), dataIn.getREQTTEXT(), Util.convertToTs(dataIn.getREQTREQTS()),
							Util.convertToTs(dataIn.getREQTRCDTS()), "RECEIVED", dataIn.getCUSTOMERINITIATEDREQ(), "Y",dataIn.getREQTREQACTIVITYID());
					if (updatedReq == 0) {
						throw new DAOException(
								"RequirementDetail is not inserted with claimNo = " + dataIn.getCLAIMNUMBER());
					}
					log.info("Successfully inserted the RequirementDetail table");

					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Inserted the Requirement as raised from WSUPdateRequirement");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("11")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					log.info("Fetching the ClaimData, with ClaimNo= "+dataIn.getCLAIMNUMBER());
					claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getCLAIMNUMBER());
					log.info("Succesfully fetched the claimData");
					if (claimData.getTPAClaimNumber() != null
							&& !claimData.getTPAClaimNumber().trim().equalsIgnoreCase("")) {
						dataIn.setTPACLAIMNUMBER(claimData.getTPAClaimNumber());
					} else {
						dataIn.setTPACLAIMNUMBER("");
					}
					if (claimData.getBatchNumber() != null && !claimData.getBatchNumber().trim().equalsIgnoreCase("")) {
						dataIn.setBATCHNUMBER(claimData.getBatchNumber());
					} else {
						dataIn.setBATCHNUMBER("");
					}
					if (claimData.getPortalClaimRequestNumber() != null
							&& !claimData.getPortalClaimRequestNumber().trim().equalsIgnoreCase("")) {
						dataIn.setPORTALREQUESTNUMBER(claimData.getPortalClaimRequestNumber());
					} else {
						dataIn.setPORTALREQUESTNUMBER("");
					}

					String[] reqtSubCategory = {};
					log.info("REQTSUBCATEGORY -|" + dataIn.getREQTSUBCATEGORY() + "|");
					if (dataIn.getREQTSUBCATEGORY() != null && dataIn.getREQTSUBCATEGORY().trim() != ""
							&& dataIn.getREQTSUBCATEGORY().trim().length() > 0) {
						log.info("Enter REQTSUB");
						reqtSubCategory = dataIn.getREQTSUBCATEGORY().trim().split("~#@");
						String strDocIDList = "";
						log.info("reqtSubCategory length-" + reqtSubCategory.length);
						for (String string : reqtSubCategory) {
							log.info("reqtSubCategory content - " + string);
						}
						log.info("reqtSubCategory length- > " + reqtSubCategory.length);
						for (iLoop = 0; iLoop < reqtSubCategory.length; iLoop++) {
							ODSRequirementMasterModel reqMaster = new ODSRequirementMasterModel();
							try {
								reqMaster = this.odsDao
										.getRequirementMasterbySubCategory(reqtSubCategory[iLoop].trim());
							} catch (Exception e) {
								e.printStackTrace();
							}
							strDocIDList = strDocIDList + "'" + reqMaster.getDOCID() + "'";
							if (iLoop < reqtSubCategory.length - 1) {
								strDocIDList = strDocIDList + ",";
							}
						}
						int deletedRow = this.claimDetailsDao.deleteRequirementDetails(dataIn.getCLAIMNUMBER(),
								strDocIDList, "N", "PENDING");
						String[] reqText_ar = dataIn.getREQTTEXT().split("~#@");
						iLoop = 0;
						for (iLoop = 0; iLoop < reqtSubCategory.length; iLoop++) {
							ODSRequirementMasterModel reqMaster = new ODSRequirementMasterModel();
							reqMaster = this.odsDao.getRequirementMasterbySubCategory(reqtSubCategory[iLoop].trim());
							int i = this.claimDetailsDao.insertRequirementDetails(dataIn.getCLAIMNUMBER(),
									dataIn.getTPACLAIMNUMBER(), dataIn.getBATCHNUMBER(),
									dataIn.getPORTALREQUESTNUMBER(), dataIn.getPOLICYNUMBER(), reqMaster.getDOCID(),
									reqText_ar[iLoop].trim(), Util.convertToTs(dataIn.getREQTREQTS()), "PENDING", "N");
							if (i == 0) {
								throw new DAOException("Data is not inserted in RequirementDetails with claimumber = "
										+ dataIn.getCLAIMNUMBER() + " and DOCID = " + reqMaster.getDOCID());
							}
						}
					

					} else {
						String strDocIDList = "''";
						int deletedRow = this.claimDetailsDao.deleteRequirementDetails(dataIn.getCLAIMNUMBER(),
								strDocIDList, "N", "PENDING");
					}
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Inserted the Requirement as raised from CLAIMDATAUI");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("2")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
							Util.convertToTs(dataIn.getREQTRCDTS()), "RECEIVED", dataIn.getREQDOCID());
					// if (updatedReq == 0) {
					// throw new DAOException(
					// "RequirementDetail is not updated with claimNo = " +
					// dataIn.getCLAIMNUMBER());
					// }
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Updated the Requirement as raised from WSUPDATEREQUIREMENT");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("24")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
							dataIn.getPOLICYNUMBER(), Util.convertToTs(dataIn.getREQTRCDTS()), "RECEIVED",
							dataIn.getREQDOCID(), "TPAClaimNumber", dataIn.getTPACLAIMNUMBER());
					// if (updatedReq == 0) {
					// throw new DAOException(
					// "RequirementDetail is not updated with claimNo = " +
					// dataIn.getCLAIMNUMBER());
					// }
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Updated the Requirement as raised from WSUPDATEREQUIREMENT");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("25")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
							dataIn.getPOLICYNUMBER(), Util.convertToTs(dataIn.getREQTRCDTS()), "RECEIVED",
							dataIn.getREQDOCID(), "BatchNumber", dataIn.getBATCHNUMBER());
					// if (updatedReq == 0) {
					// throw new DAOException(
					// "RequirementDetail is not updated with claimNo = " +
					// dataIn.getCLAIMNUMBER());
					// }
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Updated the Requirement as raised from WSUPDATEREQUIREMENT");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("26")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
							dataIn.getPOLICYNUMBER(), Util.convertToTs(dataIn.getREQTRCDTS()), "RECEIVED",
							dataIn.getREQDOCID(), "PortalClaimRequestNumber", dataIn.getPORTALREQUESTNUMBER());
					// if (updatedReq == 0) {
					// throw new DAOException(
					// "RequirementDetail is not updated with claimNo = " +
					// dataIn.getCLAIMNUMBER());
					// }
					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Updated the Requirement as raised from WSUPDATEREQUIREMENT");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("3")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					int reqtCount = this.claimDetailsDao.countRequiremnetDetails(dataIn.getCLAIMNUMBER(), "PENDING");
					if (reqtCount > 0) {
						int updatedRow1 = this.claimDetailsDao.updateRequirementDetailsWithRecId(dataIn.getCLAIMNUMBER(), "Y",
								"PENDING",dataIn.getREQTREQACTIVITYID());
						int updatedRow2 = this.claimDetailsDao.updateClaimDataGeneral(dataIn.getCLAIMNUMBER(),
								"ClaimStatus", "PENDING");
						response.setWsProcessingStatus("1");
						response.setWsSuccessMessage("Updated the Notification Flag for requirements");
					} else {
						response.setWsProcessingStatus("2");
						response.wsExceptionMessage("Could not find any requirement to update the notification");
					}
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("4")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
							dataIn.getPOLICYNUMBER(), "TPAClaimNumber", dataIn.getTPACLAIMNUMBER());

					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage("Updated the Requirement as raised from WRCLFecthPolicyData");
				} else if (dataIn.getREQTNOTIFYFLAG().trim().equalsIgnoreCase("5")) {
					log.info("NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
					if (dataIn.getBATCHNUMBER() != null && !dataIn.getBATCHNUMBER().trim().equalsIgnoreCase("")) {
						updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
								dataIn.getPOLICYNUMBER(), "BatchNumber", dataIn.getBATCHNUMBER());
					} else if (dataIn.getPORTALREQUESTNUMBER() != null
							&& !dataIn.getPORTALREQUESTNUMBER().trim().equalsIgnoreCase("")) {
						updatedReq = this.claimDetailsDao.updateRequirementDetails(dataIn.getCLAIMNUMBER(),
								dataIn.getPOLICYNUMBER(), "PortalClaimRequestNumber", dataIn.getPORTALREQUESTNUMBER());
					} else {
						throw new Exception("BatchNumber and TPAClaimNumber is null for this NotifyFlag = 5");
					}

					response.setWsProcessingStatus("1");
					response.setWsSuccessMessage(
							"Updated the Requirement as raised from WRDHNULFetchPolicyData/ WRDHULFetchPolicyData/ WRGENFetchPolicyData");
				} else {
					throw new Exception(
							"Unknown NotifyFlag Request for this NotifyFlag = " + dataIn.getREQTNOTIFYFLAG());
				}
			} else {
				throw new CamelException("Request Input Object is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsExceptionMessage("ERROR DETAIL: " + e.getMessage());
			response.setWsProcessingStatus("2");
		}

		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);

	}

}
