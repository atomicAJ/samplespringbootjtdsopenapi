/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSGENUpdClaimApproveStatusDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.BOGenericApproveMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSGENUpdClaimApproveStatusProcesssor implements Processor {

	private static Log log = LogFactory.getLog("WSGENUpdClaimApproveStatusProcesssor");

	@Value("${USRPRF}")
	private String USRPRF;

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse response = new JsonResponse();

		try {
			WSGENUpdClaimApproveStatusDataIn dataIn = exchange.getIn().getBody(WSGENUpdClaimApproveStatusDataIn.class);

			if (dataIn.getClaimNo() != null && dataIn.getClaimNo() != "" && dataIn.getActivityId() != null
					&& dataIn.getActivityId() != "") {

				log.info("Get ClaimData table with claimNo= " + dataIn.getClaimNo());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getClaimNo());
				log.info("Successfully retrieved ClaimData table");

				if (!Optional.ofNullable(claimData).isPresent()) {
					throw new DAOException("CLAIMDATA table not found for claimNumber= " + dataIn.getClaimNo());
				}
				BOGenericApproveMessageModel boMessage = new BOGenericApproveMessageModel();
				if (claimData.getFullyClaimed() != null && !claimData.getFullyClaimed().trim().equalsIgnoreCase("")) {
					if (claimData.getFullyClaimed().trim().equalsIgnoreCase("Y")) {
						boMessage.setBOIDEN(FWDConstants.BO_IDENTIFIER_Generic_ApprovedFullyPaid);
						boMessage.setCLMYN(claimData.getFullyClaimed().trim());
					} else {
						boMessage.setBOIDEN(FWDConstants.BO_IDENTIFIER_Generic_Approved);
					}
				} else {
					boMessage.setBOIDEN(FWDConstants.BO_IDENTIFIER_Generic_Approved);
				}

				// log.info("Get LaRequestTrackerModel table with claimNo= " +
				// dataIn.getClaimNo());
				// LaRequestTrackerModel laRequestTrackerData =
				// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
				// dataIn.getClaimNo(), dataIn.getActivityId(),
				// boMessage.getBOIDEN());
				// log.info("Successfully Got LaRequestTrackerModel table ");
				//
				// if (laRequestTrackerData.getBoidentifier() == null &&
				// laRequestTrackerData.getMsgtosend() == null
				// && laRequestTrackerData.getMsgcreatets() == null
				// && laRequestTrackerData.getFuturemesg() == null) {

				boMessage.setUSRPRF(this.USRPRF);
				log.debug("USRPRF = "+boMessage.getUSRPRF());

				log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
				PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
				log.info("Successfully retrieved PolicyDetail table");

				if (!(Optional.ofNullable(claimData).isPresent() && Optional.ofNullable(policyDetail).isPresent())) {
					throw new DAOException("ClaimData is null with Claim NO= " + dataIn.getClaimNo());
				}
				;

				boMessage.setCHDRCOY(policyDetail.getCOMPANY());
				log.debug("CHDRCOY = "+boMessage.getCHDRCOY());
				boMessage.setCHDRSEL(claimData.getPolicyNumber());
				log.debug("CHDRSEL = "+boMessage.getCHDRSEL());
				boMessage.setCLAIMNO(dataIn.getClaimNo());
				log.debug("CLAIMNO = "+boMessage.getCLAIMNO());
				boMessage.setCRTABLE(claimData.getComponentCode());
				log.debug("CRTABLE = "+boMessage.getCRTABLE());

				log.info("Get ComponentDetails table");
				ComponentDetailsBO componentDetailsBO = odsDao.getComponentDetailsBO(claimData.getPolicyNumber(),
						claimData.getComponentCode(), claimData.getInsuredClientID());
				log.info("Successfully retrieved ComponentDetails table");

				if (!(Optional.ofNullable(componentDetailsBO).isPresent())) {
					throw new DAOException("componentDetailsBO is null with Claim NO= " + dataIn.getClaimNo());
				}
				;

				boMessage.setLIFE(componentDetailsBO.getLife());
				log.debug("LIFE = "+boMessage.getLIFE());
				boMessage.setCOVERAGE(componentDetailsBO.getCoverage());
				log.debug("COVERAGE= "+boMessage.getCOVERAGE());
				boMessage.setRIDER(componentDetailsBO.getRider());
				log.debug("RIDER = "+boMessage.getRIDER());
				boMessage.setRGPYNUM(claimData.getClaimNumberLA());
				log.debug("RGPYNUM= "+boMessage.getRGPYNUM());

				log.info("BOMESSAGE RESULT = " + boMessage.getBoMessage());
				log.info("Inserting to LAREQUESTTRACKER table with claimNO= " + dataIn.getClaimNo());
				int i = this.claimDetailsDao.lARequestTracker_Insert(dataIn.getClaimNo(), dataIn.getActivityId(),
						boMessage.getBOIDEN(), boMessage.getBoMessage(), "0");
				if (i == 0) {
					throw new DAOException("No row is updated for LAREQUESTTRACKER TABLE");
				}
				log.info("Successfully inserted to LAREQUESTTRACKER table");

				// }

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Successfully Processed");

			} else {
				throw new CamelException("JSON INPUT PAYLOAD IS EMPTY");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("ERROR: " + e.getMessage());
		}
		log.info("JSON OUTPUT PAYLOAD= " + response.toString());
		exchange.getOut().setBody(response);

	}

}
