package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.BeneficiaryList;
import com.candelalabs.api.model.ClaimDataPortalInput;
import com.candelalabs.api.model.ClaimDataPortalOutput;
//import com.candelalabs.api.model.ClaimDataPortalOutputBeneficiaryList;
//import com.candelalabs.api.model.ClaimDataPortalOutputDocumentList;
import com.candelalabs.api.model.ClaimRecordList;
import com.candelalabs.api.model.DocumentList;
import com.candelalabs.api.model.GetClaimDetailsList;
import com.candelalabs.api.model.WSPortalServiceDataIn;
import com.candelalabs.api.model.WSPortalServiceDataOut;
//import com.candelalabs.api.model.WSPortalServiceDataOutClaimData;
import com.candelalabs.utilities.model.DsProcessCaseSheet;
import com.candelalabs.utilities.service.impl.UtilitiesServiceImpl;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

import com.candelalabs.ws.model.BENEFICIARYCLIENTINFO;
import com.candelalabs.ws.model.BENEFICIARYLIST;
import com.candelalabs.ws.model.ReqDocList;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;


	@Component
	public class WSPortalServiceGetClaimData implements Processor {

		private static final Log log = LogFactory.getLog("WSPortalServiceGetClaimData");
	
		@Autowired
		ClaimDetailsDao claimDetailsDao;

		@Autowired
		ODSDao odsDao;

		//private List<DocumentList> documentList;

		//private DocumentList e;

		

		//private BeneficiaryList be;
		/**
		 * 
		 */
		public WSPortalServiceGetClaimData() {
			// TODO Auto-generated constructor stub
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
		 */
		
		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
		//	List<DocumentList> documentList;
			 List<DocumentList> documentList = new ArrayList();
			 List<BeneficiaryList> beneficiaryList = new ArrayList();;
			ClaimDataPortalOutput response = new ClaimDataPortalOutput();
			log.info("WSPortalServiceProcessor - Service is invoked");
			//WSPortalServiceDataOutClaimData claimData = new WSPortalServiceDataOutClaimData();
			List<GetClaimDetailsList> claimData = new ArrayList<GetClaimDetailsList>();
			try {
				ClaimDataPortalInput dataIn = exchange.getIn().getBody(ClaimDataPortalInput.class);
				//ClaimDataPortalOutputDocumentList doclst = new ClaimDataPortalOutputDocumentList();
				DocumentList doclst = new DocumentList();
			//	ClaimDataPortalOutputBeneficiaryList portalbenlist = new ClaimDataPortalOutputBeneficiaryList();
				BeneficiaryList portalbenlist = new BeneficiaryList();
				String strClaimNumber ="";
				strClaimNumber = dataIn.getStrPencilClaimNumber();
				
				ClaimDataNewModel claimDataOutput = null;
				claimDataOutput = new ClaimDataNewModel();
				log.info("before calling getClaimDataForPortal strClaimNumber is - "+ strClaimNumber);
				claimDataOutput = claimDetailsDao.getClaimDataNew(strClaimNumber);
				log.info("after  calling getClaimDataForPortal strClaimNumber is - "+ strClaimNumber);
				if (claimDataOutput!=null)
				{
					String strPortalClaimRequestNumber = "";
					if (claimDataOutput.getPortalClaimRequestNumber()!=null)
					{
						strPortalClaimRequestNumber = claimDataOutput.getPortalClaimRequestNumber();
					}
					log.info("strPortalClaimRequestNumber is -"+ strPortalClaimRequestNumber);
					response.setStrPortalClaimRequestNumber(strPortalClaimRequestNumber);
					
					if(claimDataOutput.getClaimNumber()!=null)
					{
						strClaimNumber = claimDataOutput.getClaimNumber();
					}
					log.info("strClaimNumber is -"+ strClaimNumber);
					response.setStrClaimNumber(strClaimNumber);
					
					String strPolicyNumber ="";
					if(claimDataOutput.getPolicyNumber()!=null){
						strPolicyNumber = claimDataOutput.getPolicyNumber();
					}
					log.info("strPolicyNumber is -"+ strPolicyNumber);
					response.setStrPolicyNumber(strPolicyNumber);
					
					String strClaimTypeLA ="";
					if(claimDataOutput.getClaimTypeLA()!=null)
					{
						strClaimTypeLA = claimDataOutput.getClaimTypeLA();
					}
					log.info("strClaimTypeLA is -"+ strClaimTypeLA);
					response.setStrClaimTypeLA(strClaimTypeLA);
					
					String  strClaimStatus ="";
					if(claimDataOutput.getClaimStatus()!=null)
					{
						strClaimStatus = claimDataOutput.getClaimStatus();
					}
					log.info("strClaimStatus is -"+ strClaimStatus);
					response.setStrClaimStatus(strClaimStatus);
					
					String strClaimSubStatus ="";
					if(claimDataOutput.getClaimDecision()!=null){
						strClaimSubStatus = claimDataOutput.getClaimDecision();
					}
					log.info("strClaimSubStatus is -"+ strClaimSubStatus);
					response.setStrClaimSubStatus(strClaimSubStatus);
					
					String strSurgeryPerformed ="";
					if(claimDataOutput.getSurgreryPerformed()!=null){
					strSurgeryPerformed = claimDataOutput.getSurgreryPerformed();}
					log.info("strSurgeryPerformed is -"+ strSurgeryPerformed);
					response.setStrSurgeryPerformed(strSurgeryPerformed);
					
					Date dtAdmissionDate =null;
					 String strDate="";
					 if(claimDataOutput.getAdmissionDate()!=null){
						 dtAdmissionDate = claimDataOutput.getAdmissionDate();
					 }
					/*log.info("dtAdmissionDate is -"+ Util.objToDate(dtAdmissionDate,"DD/MM/YYYY").toString());
					
					response.setDtAdmissionDate(Util.objToDate(dtAdmissionDate,"DD/MM/YYYY").toString());*/
					  
					    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
					    if(dtAdmissionDate!=null)
					    {
					    strDate = formatter.format(dtAdmissionDate);}
					    
					    log.info("dtAdmissionDate is -"+ strDate);
						response.setDtAdmissionDate(strDate);
						
					Date	dtDischargeDate=null;
					if(claimDataOutput.getDischargeDate()!=null){
					dtDischargeDate = claimDataOutput.getDischargeDate();}
					/*log.info("dtDischargeDate is -"+ dtDischargeDate.toString());
					response.setDtDischargeDate(dtDischargeDate.toString());*/
					/*log.info("dtDischargeDate is -"+  Util.objToDate(dtDischargeDate,"DD/MM/YYYY").toString());
					response.setDtDischargeDate(Util.objToDate(dtDischargeDate,"DD/MM/YYYY").toString());*/
					if (dtDischargeDate!=null){
					strDate= formatter.format(dtDischargeDate);
					}
					
				    log.info("dtDischargeDate is -"+ strDate);
					response.setDtDischargeDate(strDate);
					
					String strHospitalName ="";
					if(claimDataOutput.getProviderName()!=null){
						strHospitalName = claimDataOutput.getProviderName();}
					
					log.info("strHospitalName is -"+ strHospitalName);
					response.setStrHospitalName(strHospitalName);
					
					String	strDoctorName ="";
					if(claimDataOutput.getDoctorName()!=null){
					strDoctorName = claimDataOutput.getDoctorName();}
					log.info("strDoctorName is -"+ strDoctorName);
					response.setStrDoctorName(strDoctorName);
					
					BigDecimal ClaimApprovedAmountUI = null;
					BigDecimal  dblTotalBilling = BigDecimal.ZERO;
					if(claimDataOutput.getClaimApprovedAmountUI()!=null){
						ClaimApprovedAmountUI = claimDataOutput.getClaimApprovedAmountUI();
					}
					if ((ClaimApprovedAmountUI==null)||(ClaimApprovedAmountUI == BigDecimal.ZERO)){
						if(claimDataOutput.getClaimInvoiceAmount()!=null){
						dblTotalBilling = claimDataOutput.getClaimInvoiceAmount();}
						
					}
					else{
						dblTotalBilling = ClaimApprovedAmountUI;
					}
					log.info("dblTotalBilling is -"+ dblTotalBilling.toString());
					response.setDblTotalBilling(dblTotalBilling.toString());
					
					String strTotalBillingCurrency = "";
				    String strLifeInsuredName =  "";
					String strInsuredClientID =  "";
					String strAccountNumber = "";
					String strAccountHolderName =  "";
					String strBankName = "";
					Date dtClaimSubmissionDate =  null;
					Date dtModifiedDate = null;
					
					if(claimDataOutput.getClaimCurrency()!=null){
						strTotalBillingCurrency = claimDataOutput.getClaimCurrency();
					}
					log.info("strTotalBillingCurrency is -"+ strTotalBillingCurrency);
					response.setStrTotalBillingCurrency(strTotalBillingCurrency);
					
					if(claimDataOutput.getInsuredClientName()!=null){
					strLifeInsuredName= claimDataOutput.getInsuredClientName();}
					log.info("strLifeInsuredName is -"+ strLifeInsuredName);
					response.setStrLifeInsuredName(strLifeInsuredName);
					
					if(claimDataOutput.getInsuredClientID()!=null){
					strInsuredClientID = claimDataOutput.getInsuredClientID();}
					log.info("strInsuredClientID is -"+ strInsuredClientID);
					response.setStrInsuredClientID(strInsuredClientID);
					
					if(claimDataOutput.getBankAccountNumber()!=null){
					strAccountNumber = claimDataOutput.getBankAccountNumber();}
					log.info("strAccountNumber is -"+ strAccountNumber);
					response.setStrAccountNumber(strAccountNumber);
					
					if(claimDataOutput.getBankAccountName()!=null){
					strAccountHolderName = claimDataOutput.getBankAccountName();}
					log.info("strAccountHolderName is -"+ strAccountHolderName);
					response.setStrAccountHolderName(strAccountHolderName);
					
					if(claimDataOutput.getBankCode()!=null){
					strBankName = claimDataOutput.getBankCode();}
					log.info("strBankName is -"+ strBankName);
					response.setStrBankName(strBankName);
					
					dtClaimSubmissionDate = claimDataOutput.getRegisterDate();
					/*log.info("dtClaimSubmissionDate is -"+ dtClaimSubmissionDate.toString());
					response.setDtClaimSubmissionDate(dtClaimSubmissionDate.toString());*/
					/*log.info("dtClaimSubmissionDate is -"+Util.objToDate(dtClaimSubmissionDate,"DD/MM/YYYY").toString());
					response.setDtClaimSubmissionDate(Util.objToDate(dtClaimSubmissionDate,"DD/MM/YYYY").toString());*/
					if(dtClaimSubmissionDate!=null){
					strDate= formatter.format(dtClaimSubmissionDate);}
				    log.info("dtClaimSubmissionDate is -"+ strDate);
					response.setDtClaimSubmissionDate(strDate);
					
					String  strClaimProcessingReason ="";
					if (strClaimStatus.equalsIgnoreCase("PENDING")){
						
						   strClaimProcessingReason = "Pending Requirement from customer";
					}
					else if (strClaimStatus .equalsIgnoreCase("COMPLETED"))
						{
						   //// strClaimSubStatus have the value from claim Decision so using strClaimSubStatus instead of strClaimDecision ////
						   if (strClaimSubStatus.equalsIgnoreCase("REJECT"))
						   {
							   ///<fetch data from ProcessCasesheet as per the same logic used in GenerateFile and Notification Modules>
							    UtilitiesServiceImpl UtilitiesServic = new UtilitiesServiceImpl();
							    String processCaseSheetComments = "";
								DsProcessCaseSheet df = null;
								df = UtilitiesServic.getCommentsByActivity("REJECTION HIGHER AUTHORITY", claimDataOutput.getProcessInstanceID(), strClaimNumber);
								processCaseSheetComments = df.getComments();
								
						        if ((processCaseSheetComments==null)||processCaseSheetComments.trim().isEmpty()){
						        	df = UtilitiesServic.getCommentsByActivity("CLAIM PROCESSOR", claimDataOutput.getProcessInstanceID(), strClaimNumber);
									processCaseSheetComments = df.getComments();
						        }
							   strClaimProcessingReason = processCaseSheetComments;
						   }
						   else
						   {
						      strClaimProcessingReason = "";
						   }
						}
					log.info("strClaimProcessingReason is -"+ strClaimProcessingReason);
					response.setStrClaimProcessingReason(strClaimProcessingReason);
				/////Retrieve Requirement Data//////////
					
				
				//	List<ClaimDataPortalOutputDocumentList> reqDocList = null;
					
					log.info("before calling getRequirementDocList strClaimNumber is - "+ strClaimNumber);
					//List<ClaimDataPortalOutputDocumentList> reqDocList  = claimDetailsDao.getRequirementDocList(strClaimNumber);
					List<ReqDocList> reqDocList  = new ArrayList<ReqDocList>();
					reqDocList = claimDetailsDao.getRequirementDocList(strClaimNumber);
					log.info("after calling getRequirementDocList "); 
				//	log.info("after calling getRequirementDocList ------" +reqDocList.get(0));
					if(reqDocList!=null){
						log.info("reqDocList is not null reqDocList.size() --"+ reqDocList.size());
					//	for (ClaimDataPortalOutputDocumentList doclist : reqDocList) {
						for (ReqDocList doclist : reqDocList) {
							String docid ="";
							String RequirementStatus="";
							DocumentList e = new DocumentList();
							if (doclist.getDocID()!=null&&doclist.getDocID().isEmpty()==false)
							{
								docid= doclist.getDocID();}
							//doclst.setStrDocID(docid);
							e.setStrDocID(docid);
							
							
							//doclst.setStrDocReceiveStatus(doclist.getRequirementStatus());
							if (doclist.getDocID()!=null&&doclist.getDocID().isEmpty()==false){
								RequirementStatus= doclist.getRequirementStatus();
							}
							e.setStrDocReceiveStatus(RequirementStatus);
							String subCategory="";
							
							log.info("in for loop b4 calling getSubCategory for docid- "+ docid);
							if(docid!=""){
							subCategory = odsDao.getSubCategory(docid);}
							log.info("in for loop after calling getSubCategory ");
							//doclst.setStrSubCategory(subCategory);
							e.setStrSubCategory(subCategory);
							log.info("in for loop after calling getSubCategory--subCategory -- "+ subCategory);
							documentList.add(e);
						}
					}
					/*else{
						DocumentList e = new DocumentList();
						log.info("reqDocList is  null");
						e.setStrDocID("");
						e.setStrDocReceiveStatus("");
						e.setStrSubCategory("");
						documentList.add(e);
						doclst.setStrDocID("");
						doclst.setStrDocReceiveStatus("");
						doclst.setStrSubCategory("");
						//claimDataPortalOtpt.setDocumentList(doclst);
					}*/
					//response.setDocumentList(doclst);
					response.setDocumentList(documentList);
					//////////////////////////
					
					List<BENEFICIARYLIST> benList = null;
					BENEFICIARYCLIENTINFO benclientinfo = new BENEFICIARYCLIENTINFO ();
					log.info("b4 calling odsDao.getBENEFICIARYLIST ");
					benList =  odsDao.getBENEFICIARYLIST(strPolicyNumber);
					log.info("after calling odsDao.getBENEFICIARYLIST ");
					if(benList!=null){
						log.info("benList!=null ");
						for (BENEFICIARYLIST benfitlist : benList) {
							BeneficiaryList be = new BeneficiaryList();
							String strBeneficiaryClientNo ="";
							if (benfitlist.getCLIENTNO()!=null&&benfitlist.getCLIENTNO().isEmpty()==false){
							strBeneficiaryClientNo = benfitlist.getCLIENTNO();}
							log.info("strBeneficiaryClientNo - "+ strBeneficiaryClientNo);
							//portalbenlist.setStrBeneficiaryClientNo(strBeneficiaryClientNo);
							be.setStrBeneficiaryClientNo(strBeneficiaryClientNo);
							
							String strBeneficiaryClientName ="";
							if (benfitlist.getCLIENTNAME()!=null&&benfitlist.getCLIENTNAME().isEmpty()==false){
							strBeneficiaryClientName = benfitlist.getCLIENTNAME();}
							log.info("strBeneficiaryClientName - "+ strBeneficiaryClientName);
							//portalbenlist.setStrBeneficiaryClientName(strBeneficiaryClientName);
							be.setStrBeneficiaryClientName(strBeneficiaryClientName);
							
							String strBeneficiaryRelationship ="";
							if (benfitlist.getRELATIONSHIP()!=null&&benfitlist.getRELATIONSHIP().isEmpty()==false){
							strBeneficiaryRelationship = benfitlist.getRELATIONSHIP();}
							log.info("strBeneficiaryRelationship - "+ strBeneficiaryRelationship);
							//portalbenlist.setStrBeneficiaryRelationship(strBeneficiaryRelationship);
							be.setStrBeneficiaryRelationship(strBeneficiaryRelationship);
							
							String strBenefitPercentage ="";
							if (benfitlist.getPERCENTAGE()!=null&&benfitlist.getPERCENTAGE().isEmpty()==false){
							strBenefitPercentage = benfitlist.getPERCENTAGE();}
							log.info("strBenefitPercentage - "+ strBenefitPercentage);
							//portalbenlist.setStrBenefitPercentage(strBenefitPercentage);
							be.setStrBenefitPercentage(strBenefitPercentage);
							
							log.info("b4 calling odsDao.getBENEFICIARYCLIENTINFO ");
							benclientinfo =  odsDao.getBENEFICIARYCLIENTINFO(strBeneficiaryClientNo);
							log.info("after calling odsDao.getBENEFICIARYCLIENTINFO ");
							
							String strBeneficiaryIDNumber="";
							String strBeneficiaryGender ="";
							Date dtDateOfBirth=null;
							if(benclientinfo!=null){
								log.info("benclientinfo!=null ");
								if (benclientinfo.getIDNUMBER()!=null&&benclientinfo.getIDNUMBER().isEmpty()==false){
								strBeneficiaryIDNumber = benclientinfo.getIDNUMBER();}
								
								log.info("strBeneficiaryIDNumber - "+ strBeneficiaryIDNumber);
								if (benclientinfo.getSEX()!=null&&benclientinfo.getSEX().isEmpty()==false){
								strBeneficiaryGender = benclientinfo.getSEX();}
								log.info("strBeneficiaryGender - "+ strBeneficiaryGender);
								if (benclientinfo.getSEX()!=null){
								dtDateOfBirth = benclientinfo.getDOB();}
								
								log.info("dtDateOfBirth - "+ dtDateOfBirth);
							}
							//portalbenlist.setStrBeneficiaryGender(strBeneficiaryGender);
							be.setStrBeneficiaryGender(strBeneficiaryGender);
							//portalbenlist.setStrBeneficiaryIDNumber(strBeneficiaryIDNumber);
							be.setStrBeneficiaryIDNumber(strBeneficiaryIDNumber);
							
							if(dtDateOfBirth!=null){
							strDate= formatter.format(dtDateOfBirth);}
							
						    log.info("dtDateOfBirth is -"+ strDate);
						   // portalbenlist.setDtDateOfBirth(strDate);
						    be.setDtDateOfBirth(strDate);
						    
						    beneficiaryList.add(be);
							//response.setBeneficiaryList(portalbenlist);
							
						}response.setBeneficiaryList(beneficiaryList);
					}
					else{
						/*log.info("benList is null ");
						portalbenlist.setStrBeneficiaryClientNo("");
						portalbenlist.setStrBeneficiaryClientName("");
						portalbenlist.setStrBeneficiaryRelationship("");
						portalbenlist.setStrBenefitPercentage("");
						portalbenlist.setStrBeneficiaryGender("");
						portalbenlist.setStrBeneficiaryIDNumber("");
						portalbenlist.setDtDateOfBirth("");
						be.setStrBeneficiaryClientNo("");
						be.setStrBeneficiaryClientName("");
						be.setStrBeneficiaryRelationship("");
						be.setStrBenefitPercentage("");
						be.setStrBeneficiaryGender("");
						be.setStrBeneficiaryIDNumber("");
						be.setDtDateOfBirth("");
						 beneficiaryList.add(be);
						//response.setBeneficiaryList(portalbenlist);
						response.setBeneficiaryList(beneficiaryList);*/
					}
					response.setWsExceptionMessage("1");
					response.setWsProcessingStatus("");
					response.setWsSuccessMessage("");
				}
				else
				{
					//// else of if (claimDataOutput!=null)
					log.info("claimDataOutput is null ");
					response.setStrPortalClaimRequestNumber("");
					response.setStrClaimNumber("");
					response.setStrPolicyNumber("");
					response.setStrClaimTypeLA("");
					response.setStrClaimStatus("");
					response.setStrClaimSubStatus("");
					response.setStrSurgeryPerformed("");
					response.setDtAdmissionDate("");
					response.setDtDischargeDate("");
					response.setStrHospitalName("");
					response.setStrDoctorName("");
					response.setDblTotalBilling("");
					response.setStrTotalBillingCurrency("");
					response.setStrLifeInsuredName("");
					response.setStrInsuredClientID("");
					response.setStrAccountHolderName("");
					response.setStrAccountHolderName("");
					response.setStrBankName("");
					response.setDtClaimSubmissionDate("");
					response.setStrClaimProcessingReason("");
					//response.setDocumentList(doclst);
					response.setDocumentList(documentList);
				//	response.setBeneficiaryList(portalbenlist);
					response.setBeneficiaryList(beneficiaryList);
					response.setWsExceptionMessage("2");
					response.setWsProcessingStatus("Could Not Retrieve ClaimData for the given Claim Number");
					response.setWsSuccessMessage("");
					
				}
				
			} 
			catch (Exception e) {
				// TODO( handle exception
				log.error("Error" + e);
				for (StackTraceElement tr : e.getStackTrace()) {
					log.error("\tat " + tr);
				}
				
				    response.setWsExceptionMessage( e.getMessage());
				    response.setWsProcessingStatus("2");
				    response.setWsSuccessMessage("");
			}
			log.info("Final Response " + response.toString());

			exchange.getOut().setBody(response);

		}

	}
