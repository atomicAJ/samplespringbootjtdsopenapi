/**
 * 
 */
package com.candelalabs.ws.services;

import java.io.IOException;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.SignalDetails;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.ActivitiException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.services.activitirest.RestRequest;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji Camel processor to signal Activiti BPM Receive Task
 *
 */
@Component
public class WSSignalReceiveTaskProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSSignalReceivedTaskProcessor");

	@Autowired
	RestRequest restRequest;

	@Autowired
	private ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("************************************************************************************************ \n"
				+ "Processing request");
		SignalDetails input = exchange.getIn().getBody(SignalDetails.class);
		log.debug("Processing request with Payload: " + input.toString());
		JsonResponse response = new JsonResponse();
		try {
			if (input.getProcessInstanceId() != null && input.getProcessInstanceId() != ""
					&& input.getActivityId() != null && input.getActivityId() != "") {
				log.debug("Variables size isEmpty=" + input.getVariables() == null ? true : false);
				if (!(input.getVariables() == null)) {
					log.info("Invoking Activiti BPM to Update Variables");
					this.restRequest.updateVariables(input.getVariables(), input.getProcessInstanceId());
					log.info("Finished Invoking Activiti BPM to Update Variables");
				}

				log.info("Invoking Activiti BPM to Query Execution ID");
				Map<String, String> mapS = this.restRequest.queryExecutionId(input.getProcessInstanceId(),
						input.getActivityId());
				log.info("Finished Invoking Activiti BPM to Query Execution ID");

				log.info("Updating ClaimData Table with processInstance= " + input.getProcessInstanceId()
						+ " with ClaimStatus= RUNNING");
				int i = this.claimDetailsDao.updateClaimStatusofClaimDataByProcessInstance(input.getProcessInstanceId(),
						"RUNNING");
				if (i == 0) {
					throw new DAOException(
							"ClaimData Table not updated with processInstanceId= " + input.getProcessInstanceId());
				}
				log.info("Successfully updated the ClaimData Table");

				log.info("Invoking Activiti BPM to Signal Receive Task with execution ID: " + mapS.get("executionId"));
				this.restRequest.signalReceiveTask(mapS.get("executionId"));
				log.info("Successfully Activiti BPM to Signal Receive Task with execution ID: "
						+ mapS.get("executionId"));

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Receive Task= " + mapS.get("activityId") + " is processsed Successfully");
			} else {
				log.error("Request string is empty");
				throw new ActivitiException("Request string is empty");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error " + e.getClass().getSimpleName() + " " + e.getMessage());
			log.error("Error: ", e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("JSON RESPONSE payload = " + response.toString());
		exchange.getOut().setBody(response);
		// System.out.print("Sdhmasuk sini yang ini");
		// SuccessResponse out = new SuccessResponse();
		// SignalDetails input = exchange.getIn().getBody(SignalDetails.class);
		// System.out.print("Sdhmasuk sini");
		// out.setStatus("1");
		// out.setMessage(input.getActivityId());
		// exchange.getOut().setBody(out);

	}

}
