/**
 *
 */
package com.candelalabs.ws.services;

import java.util.*;

//import com.candelalabs.ws.model.tables.LetterConfigResultSet;
import com.candelalabs.ws.model.tables.LetterConfigResultSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSRegisterNotificationDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.NotificationDecisionMasterModel;
import com.candelalabs.ws.model.tables.NotificationProcessMasterModel;

/**
 * @author Admin
 */
@Service
public class WSRegisterNotificationProcesssorTransactional {

    private static final Log log = LogFactory.getLog("WSRegisterNotificationProcesssor");

    @Autowired
    private ClaimDetailsDao claimDetailsDao;

    @Transactional(rollbackFor = Exception.class)
    public void letterProcess(ClaimDataNewModel claimData, JsonResponse response,
                              WSRegisterNotificationDataIn dataIn, NotificationProcessMasterModel notProcMasterModel, String notifCateg)
            throws Exception {
        if (claimData == null || !Optional.ofNullable(claimData).isPresent()) {
            setExceptionMessage(response,
                    "Could not retrieve Claim Data Information for Letter for CLAIMNUMBER",
                    dataIn.getCLAIMNUMBER());
        }
        log.info("ClaimTypeUI = "+claimData.getClaimTypeUI());
        if ((claimData.getClaimTypeUI().equalsIgnoreCase("ADDB")
                || claimData.getClaimTypeUI().equalsIgnoreCase("CI")
                || claimData.getClaimTypeUI().equalsIgnoreCase("C1"))
                && dataIn.getDECISION().trim().equalsIgnoreCase("APPROVED")) {

        } else {
            claimData.setFullyClaimed(null);
        }
        log.info("ClaimData FullyClaim= "+claimData.getFullyClaimed());
        if (!dataIn.getDECISION().equalsIgnoreCase("COBREQUESTPOSTPROCESS")) {
            log.info("Fetching the letterTemplateId");
            List<Integer> letterId_li = this.claimDetailsDao.getLetterTemplateResultSet(
                    notProcMasterModel.getConfigurationID(), claimData, "N");
            log.info("Successfully fetched the letterId, with size= "+letterId_li.size());
            if (letterId_li == null || letterId_li.size() == 0) {
                log.info(" letterTemplateId is null then set the ExceptionMessage");

                setExceptionMessage(response,
                        "No Letter Template ID found to generate Notification entry for CLAIMNUMBER=",
                        dataIn.getCLAIMNUMBER());
            } else {
                for (Integer letId : letterId_li) {
                    log.info("Insert mailTemplateId & letterTemplateId to NotificationTable, LetterId= "+letId);
                    /*int i = this.claimDetailsDao.insertNotificationTable2("LetterTemplateID",
                            dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(),
                            dataIn.getDECISION(), notifCateg, letId, "N", "N", "N", "N", "N",
                            dataIn.getPROCESSCATEGORY());*/
                    int i = this.claimDetailsDao.insertNotificationTable2("LetterTemplateID",
                            dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(),
                            dataIn.getDECISION(), notifCateg, letId, "N", "N", "N", "N", "N",
                            dataIn.getPROCESSCATEGORY(),dataIn.getAPPLICATIONNUMBER(),dataIn.getPOLICYNUMBER());
                    if (i == 0) {
                        throw new DAOException(
                                "Not Inserted to Notification Table with Notification Category: "
                                        + notifCateg);
                    }
                    log.info(
                            "Successfully inserted mailTemplateId & letterTemplateId to NotificationTable");
                    setSuccessfullMessage(response,
                            "Notification Record created for Letter with TemplateID = " + letId,
                            dataIn.getCLAIMNUMBER());
                }

                cobRequestLetter(claimData, dataIn, response,
                        notProcMasterModel.getConfigurationID(), notifCateg);

            }
        } else {
            cobRequestLetter(claimData, dataIn, response,
                    notProcMasterModel.getConfigurationID(), notifCateg);
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public void emailLetterProcess(ClaimDataNewModel claimData, JsonResponse response,
                                   WSRegisterNotificationDataIn dataIn, NotificationProcessMasterModel notProcMasterModel, String notifCateg)
            throws Exception {
        Map<Integer, List<LetterConfigResultSet>> emailTemplate_Map = new HashMap<Integer, List<LetterConfigResultSet>>();
        if (claimData == null || !Optional.ofNullable(claimData).isPresent()) {
            setExceptionMessage(response, "Could not retrieve Claim Data Information for Letter for CLAIMNUMBER",
                    dataIn.getCLAIMNUMBER());
        }
        log.info("ClaimTypeUI = "+claimData.getClaimTypeUI());
        if ((claimData.getClaimTypeUI().equalsIgnoreCase("ADDB") || claimData.getClaimTypeUI().equalsIgnoreCase("CI")
                || claimData.getClaimTypeUI().equalsIgnoreCase("C1"))
                && dataIn.getDECISION().trim().equalsIgnoreCase("APPROVED")) {

        } else {
            claimData.setFullyClaimed(null);
        }
        log.info("ClaimData FullyClaim= "+claimData.getFullyClaimed());

        if (!dataIn.getDECISION().equalsIgnoreCase("COBREQUESTPOSTPROCESS")) {
            log.info("Fetch mailTemplateId from NOTIFICATIONEMAILMASTER Table where NotificationDecision= "
                    + dataIn.getDECISION());
            //List<Integer> mailIdD_li = new ArrayList<Integer>();
            List<LetterConfigResultSet> letConfig_li = new ArrayList<LetterConfigResultSet>();
            letConfig_li = this.claimDetailsDao.getLetterConfigResultSet(notProcMasterModel.getConfigurationID(), claimData, "N");
           // mailIdD_li = this.claimDetailsDao.getMailTemplateResultSet(notProcMasterModel.getConfigurationID(), 0);
            log.info("Successfully fetched the table, letConfig_li Size = " + letConfig_li.size());
            if (letConfig_li == null || letConfig_li.size() == 0) {
                log.info("letterConfigID  is nto found then set the ExceptionMessage");

                setExceptionMessage(response,
                        "No letterConfigID found to generate Notification entry for CLAIMNUMBER=",
                        dataIn.getCLAIMNUMBER());
            } else {
                Integer mailTempId = null;
                for (LetterConfigResultSet letConfigID : letConfig_li) {
                    log.info("Fetch mailTemplate from NOTIFICATIONEMAILLETTERCONFIGURATION Table where ConfigID= "
                            + letConfigID.getLetterConfigurationID());

//					List<Integer> letterTempResult_li = this.claimDetailsDao.getLetterTemplateResultSet(maidIdD,
//							notProcMasterModel.getConfigurationID(), claimData, "N");
                    mailTempId = this.claimDetailsDao.getMailTemplateId(letConfigID.getLetterConfigurationID());
                    log.info("Successfully fetched the table, mailTempId = " + mailTempId);
                    if (mailTempId == null || mailTempId == 0) {
                        setExceptionMessage(response,
                                "No Email Template ID found to generate Notification entry for CLAIMNUMBER=",
                                dataIn.getCLAIMNUMBER());
                    } else {
                        log.info("Insert to NOTIFICATIONEMAILLETTER table, with mailId = "+mailTempId+", and " +
                                "LetterTemplateId= "+letConfigID.getLetterTemplateID());
                        if (this.claimDetailsDao.insertToNotificationEmailLetter(dataIn.getCLAIMNUMBER(),
                                dataIn.getDECISION(), mailTempId, letConfigID.getLetterTemplateID()) == 0) {
                            throw new Exception("insertToNotificationEmailLetter not inserted");
                        }
                        log.info("Successfully inserted the data");
                    }
                }
                emailTemplate_Map.putIfAbsent(mailTempId, letConfig_li);
                cobRequest(claimData, dataIn, response, notProcMasterModel.getConfigurationID(), notifCateg, emailTemplate_Map);
            }
        } else {
            cobRequest(claimData, dataIn, response, notProcMasterModel.getConfigurationID(), notifCateg,emailTemplate_Map);
        }
        log.info("EmailTemplateMap size= "+emailTemplate_Map.size());
        if (emailTemplate_Map != null && emailTemplate_Map.size() > 0 ) {
            for (Map.Entry<Integer, List<LetterConfigResultSet>> entry : emailTemplate_Map.entrySet()) {
                log.info("Inserting to Notification Table with mailTempId= "+entry.getKey());
                /*if (this.claimDetailsDao.insertNotificationTable2("EmailTemplateID", dataIn.getCLAIMNUMBER(),
                            dataIn.getPROCESSNAME(), dataIn.getDECISION(), notifCateg, entry.getKey(), "N", "N", "N", "N",
                            "N", dataIn.getPROCESSCATEGORY()) == 0) {
                        throw new Exception("Notification Table not inserted");
                    }*/
                if (this.claimDetailsDao.insertNotificationTable2("EmailTemplateID", dataIn.getCLAIMNUMBER(),
                        dataIn.getPROCESSNAME(), dataIn.getDECISION(), notifCateg, entry.getKey(), "N", "N", "N", "N",
                        "N", dataIn.getPROCESSCATEGORY(),dataIn.getAPPLICATIONNUMBER(),dataIn.getPOLICYNUMBER()) == 0) {
                    throw new Exception("Notification Table not inserted");
                }
                log.info("Successfully Inserted to the NotificationTable");
                setSuccessfullMessage(response,
                        "Notification Record created for Letter with EmailTemplateID = " + entry.getKey(),
                        dataIn.getCLAIMNUMBER());
            }

        }

    }

    private void setExceptionMessage(JsonResponse response, String message, String claimNumber) throws Exception {
        response.setWsProcessingStatus("2");
        if (response.getWsExceptionMessage() == "") {
            response.setWsExceptionMessage(message + " " + claimNumber);
        } else {
            response.setWsExceptionMessage(response.getWsExceptionMessage() + " And " + message + " " + claimNumber);
        }
        throw new Exception(response.getWsExceptionMessage());
    }

    private void setSuccessfullMessage(JsonResponse response, String message, String claimNumber) {
        response.setWsProcessingStatus("1");
        if (response.getWsSuccessMessage() == "") {
            response.setWsSuccessMessage(message + " " + claimNumber);
        } else {
            response.setWsSuccessMessage(response.getWsSuccessMessage() + " And " + message + " " + claimNumber);
        }
    }

    private void cobRequest(ClaimDataNewModel claimData, WSRegisterNotificationDataIn dataIn, JsonResponse response,
                            int notifConfigID, String notifCateg, Map<Integer, List<LetterConfigResultSet>> emailTemplateId_Map) throws Exception {
        log.info("Inside the cobRequest method");
        List<Integer> cobLetterId = null;
        List<Integer> cobMailId_li = null;
        List<LetterConfigResultSet> letterCOnfig_li = null;
        int countCOBRequest = 0;

        if (dataIn.getDECISION().equalsIgnoreCase("APPROVED") || dataIn.getDECISION()
                .equalsIgnoreCase("COBREQUESTPOSTPROCESS")
                && (claimData.getClaimDecision() != null && !claimData.getClaimDecision().trim().equalsIgnoreCase("")
                && claimData.getClaimDecision().trim().equalsIgnoreCase("APPROVE"))) {

            log.info("Fetching the total of COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
            countCOBRequest = this.claimDetailsDao.countCOBRequestByClaimNumber(dataIn.getCLAIMNUMBER());
            log.info("Successfully fetched the data with total COBRequest= " + countCOBRequest);

            if (countCOBRequest > 0) {

                log.info("Fetch LetterCOonfig for COBRequest from NOTIFICATIONLETTERTEMPLATEMASTER Table where ConfigID= "
                        + notifConfigID);
                //cobMailId_li = this.claimDetailsDao.getMailTemplateResultSet(notifConfigID, 1);
                letterCOnfig_li = this.claimDetailsDao.getLetterConfigResultSet(notifConfigID, claimData, "Y");
                log.info("Successfully fetched the table, letterCOnfig_li = " + letterCOnfig_li.size());

                if (letterCOnfig_li == null || letterCOnfig_li.size() == 0) {
                    setExceptionMessage(response,
                            "letterCOnfig_li ID not found to generate Notification entry for COBRequest for CLAIMNUMBER=",
                            dataIn.getCLAIMNUMBER());

                } else {
                    Integer mailTemplateId = null;
                    for (LetterConfigResultSet letConfig : letterCOnfig_li) {
                        log.info("Processing LetterConfig= "+letConfig.getLetterConfigurationID());
//						List<Integer> letterTempResult_li = this.claimDetailsDao.getLetterTemplateResultSet(cobMailId,
//								notifConfigID, claimData, "Y");
                        mailTemplateId = this.claimDetailsDao.getMailTemplateId(letConfig.getLetterConfigurationID());
                        if (mailTemplateId == null || mailTemplateId == 0) {
                            throw new Exception(
                                    "mailTemplateId not found to generate Notification entry for COBRequest for LetterConfig= " + letConfig.getLetterConfigurationID());
                        } else {
                            log.info("Inserting to NotificationEmailLetter table with mailTemplateId= "+mailTemplateId+
                                    ", and LetterTemplateID= "+letConfig.getLetterTemplateID());
                            if (this.claimDetailsDao.insertToNotificationEmailLetter(dataIn.getCLAIMNUMBER(),
                                    dataIn.getDECISION(), mailTemplateId, letConfig.getLetterTemplateID()) == 0) {
                                throw new Exception("Insertion to Notification Email Letter doesn't work out");
                            }
                            log.info("Successfully inserted the NotificationEmailLetter table");
                         /*   if (this.claimDetailsDao.insertNotificationTable2("EmailTemplateID",
                                    dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(), dataIn.getDECISION(), notifCateg,
                                    cobMailId, "N", "N", "N", "N", "N", dataIn.getPROCESSCATEGORY()) == 0) {
                                setExceptionMessage(response, "Failed in Inserting Notification ",
                                        dataIn.getCLAIMNUMBER());
                            }*/
                        }
                    }
                    emailTemplateId_Map.putIfAbsent(mailTemplateId,letterCOnfig_li);
                    this.claimDetailsDao.updateCOBRequest(dataIn.getCLAIMNUMBER(), 1,
                            "COB Letter for Customer is generated");
                    setSuccessfullMessage(response,
                            "Successfully Created LETTER Notification for COBRequest for CLAIMNUMBER =",
                            dataIn.getCLAIMNUMBER());
                }

            }
        } else if (dataIn.getDECISION().equalsIgnoreCase("REJECT")
                || dataIn.getDECISION().equalsIgnoreCase("AUTO REJECT")
                || dataIn.getDECISION().equalsIgnoreCase("COBREQUESTPOSTPROCESS")
                && (claimData.getClaimDecision() != null
                && !claimData.getClaimDecision().trim().equalsIgnoreCase("")
                && claimData.getClaimDecision().trim().equalsIgnoreCase("REJECT"))) {

            log.info("Fetching the total of COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
            countCOBRequest = this.claimDetailsDao.countCOBRequestByClaimNumber(dataIn.getCLAIMNUMBER());
            log.info("Successfully fetched the data with total COBRequest= " + countCOBRequest);

            if (countCOBRequest > 0) {
                log.info("Update COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
                int i = this.claimDetailsDao.updateCOBRequest(dataIn.getCLAIMNUMBER(), 3,
                        "Claim Decision is Rejected or Claim is not of type Hospital Surgical. "
                                + "System will not generate the COB Letter for customer");
                if (i == 0) {
                    throw new DAOException(
                            "Not updated COBRequest when Rejected, ClaimNumber: " + dataIn.getCLAIMNUMBER());
                }
                log.info("Successfully Updated COBRequest Table");
                setSuccessfullMessage(response, "Update COBRequest for Claim Reject Decision for CLAIMNUMBER=",
                        dataIn.getCLAIMNUMBER());
            }
        }
    }

    private void cobRequestLetter(ClaimDataNewModel claimData, WSRegisterNotificationDataIn dataIn,
                                  JsonResponse response, int notifConfigID, String notifCateg) throws Exception {
        log.info("Inside cobRequestLetter method");
        List<Integer> cobLetterId = null;
        // Integer cobMailId = null;
        int countCOBRequest = 0;

        if ((dataIn.getDECISION().equalsIgnoreCase("APPROVED")
                || dataIn.getDECISION().equalsIgnoreCase("COBREQUESTPOSTPROCESS"))
                && claimData.getClaimDecision() != null && !claimData.getClaimDecision().trim().equalsIgnoreCase("")
                && claimData.getClaimDecision().equalsIgnoreCase("APPROVE")) {

            log.info("Fetching the total of COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
            countCOBRequest = this.claimDetailsDao.countCOBRequestByClaimNumber(dataIn.getCLAIMNUMBER());
            log.info("Successfully fetched the data with total COBRequest= " + countCOBRequest);

            if (countCOBRequest > 0) {

                log.info(
                        "Fetch letterTemplateId for COBRequest from NOTIFICATIONLETTERMASTER Table where NotificationDecision= "
                                + dataIn.getDECISION());
                cobLetterId = this.claimDetailsDao.getLetterTemplateResultSet(notifConfigID, claimData, "Y");
                log.info("Successfully fetched the table, cobLetterId = " + cobLetterId);

                if ((cobLetterId == null || cobLetterId.size() == 0)) {

                    log.info("Not found cobLetterId or cobMailId setExceptionMessage");
                    setExceptionMessage(response,
                            "No Email / Letter Template ID found to generate Notification entry for COBRequest for CLAIMNUMBER=",
                            dataIn.getCLAIMNUMBER());

                } else {
                    int i = 0;
                    int j = 0;
                    for (Integer coblet : cobLetterId) {
                        log.info("Insert cobMailId & cobLetterId to NotificationTable");
                       /* i = this.claimDetailsDao.insertNotificationTable2("LetterTemplateID", dataIn.getCLAIMNUMBER(),
                                dataIn.getPROCESSNAME(), dataIn.getDECISION(), notifCateg, coblet, "N", "N", "N", "N",
                                "N", dataIn.getPROCESSCATEGORY());*/
                        i = this.claimDetailsDao.insertNotificationTable2("LetterTemplateID", dataIn.getCLAIMNUMBER(),
                                dataIn.getPROCESSNAME(), dataIn.getDECISION(), notifCateg, coblet, "N", "N", "N", "N",
                                "N", dataIn.getPROCESSCATEGORY(),dataIn.getAPPLICATIONNUMBER(),dataIn.getPOLICYNUMBER());
                    }
                    log.info("Update COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
                    j = this.claimDetailsDao.updateCOBRequest(dataIn.getCLAIMNUMBER(), 1,
                            "COB Letter for Customer is generated");
                    if (i == 0 || j == 0) {
                        throw new DAOException("Not Inserted to Notification Table for COBRequest with Decision: "
                                + dataIn.getDECISION() + " or not Updated COBRequestTable with claimNumber: "
                                + dataIn.getCLAIMNUMBER());
                    }
                    log.info("Successfully Inserted Notification Table and Updated COBRequest Table");
                    setSuccessfullMessage(response,
                            "Successfully Created EMAIL & LETTER Notification for COBRequest for CLAIMNUMBER=",
                            dataIn.getCLAIMNUMBER());
                }
            }
        } else if ((dataIn.getDECISION().equalsIgnoreCase("REJECT")
                || dataIn.getDECISION().equalsIgnoreCase("AUTO REJECT")
                || dataIn.getDECISION().equalsIgnoreCase("COBREQUESTPOSTPROCESS"))
                && claimData.getClaimDecision() != null && !claimData.getClaimDecision().trim().equalsIgnoreCase("")
                && claimData.getClaimDecision().equalsIgnoreCase("REJECT")) {

            log.info("Fetching the total of COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
            countCOBRequest = this.claimDetailsDao.countCOBRequestByClaimNumber(dataIn.getCLAIMNUMBER());
            log.info("Successfully fetched the data with total COBRequest= " + countCOBRequest);

            if (countCOBRequest > 0) {
                log.info("Update COBRequest with claimNumber= " + dataIn.getCLAIMNUMBER());
                int i = this.claimDetailsDao.updateCOBRequest(dataIn.getCLAIMNUMBER(), 3,
                        "Claim Decision is Rejected or Claim is not of type Hospital Surgical. "
                                + "System will not generate the COB Letter for customer");
                if (i == 0) {
                    throw new DAOException(
                            "Not updated COBRequest when Rejected, ClaimNumber: " + dataIn.getCLAIMNUMBER());
                }
                log.info("Successfully Updated COBRequest Table");
                setSuccessfullMessage(response, "Update COBRequest for Claim Reject Decision for CLAIMNUMBER=",
                        dataIn.getCLAIMNUMBER());
            }
        }
    }

}
