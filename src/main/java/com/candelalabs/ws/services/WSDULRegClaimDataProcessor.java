package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDULRegClaimDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSDULRegClaimMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.Util;

@Component
@PropertySource("classpath:application.properties")
public class WSDULRegClaimDataProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSDULRegClaimDataProcessor");

	@Autowired
	private ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	@Value("${USRPRF}")
	private String USRPRF;

	@Override
	public void process(Exchange exchange) throws Exception {

		log.info(" WSDULRegClaimDataProcessor -- testing log");

		JsonResponse jsonoutData = new JsonResponse();

		try {

			WSDULRegClaimDataIn jsonInData = (exchange.getIn().getBody(WSDULRegClaimDataIn.class));
			log.info("JSON INPUT PAYLOAD = " + jsonInData.toString());
			// jsonInData = gson.fromJson(jsonInString,
			// WSDULRegClaimDataIn.class);

			WSDULRegClaimMessageModel boMessage = new WSDULRegClaimMessageModel();

			if (jsonInData.getCLAIMNO() != null && jsonInData.getCLAIMNO().trim() != "") {

				boMessage.setUSRPRF(this.USRPRF);

				log.info("Get ClaimData table with claimNo= " + jsonInData.getCLAIMNO());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(jsonInData.getCLAIMNO());
				log.info("Successfully retrieved ClaimData table");

				// claimClaimsDetailsBOData = claimDetailsDao
				// .getWSDULClaimClaimsDetailsByClaimNo(jsonInData.getCLAIMNO());

				log.info("Get PolicyDetail table with polNo= " + claimData.getPolicyNumber());
				PolicyDetails policyDetail = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
				log.info("Successfully retrieved PolicyDetail table");

				if (!(Optional.ofNullable(claimData).isPresent() && Optional.ofNullable(policyDetail).isPresent())) {
					throw new DAOException("ClaimData is null with Claim NO= " + jsonInData.getCLAIMNO());
				}
				;

				boMessage.setCHDRCOY(policyDetail.getCOMPANY());
				log.debug("CHDRCOY=" + policyDetail.getCOMPANY());
				boMessage.setCHDRSEL(claimData.getPolicyNumber());
				log.debug("CHDRSEL=" + claimData.getPolicyNumber());

				// PENDING
				boMessage.setDTOFDEATH(Util.dateToString(claimData.getDischargeDate(), "yyyyMMdd"));
				log.debug("DTOFDEATH= "+boMessage.getDTOFDEATH());
				boMessage.setZCATCLMBEN(claimData.getBenefitCategory());
				log.debug("ZCATCLMBEN= "+boMessage.getZCATCLMBEN());
				boMessage.setZDIAGCDE(claimData.getDiagnosisCode());
				log.debug("ZDIAGCDE= "+boMessage.getZDIAGCDE());

				boMessage.setEFDATE(Util.dateToString(claimData.getProcessInitiatonTS(), "yyyyMMdd"));
				log.debug("EFDATE = "+boMessage.getEFDATE());
				boMessage.setCLAIMNO(jsonInData.getCLAIMNO());
				log.debug("CLAIMNO= "+boMessage.getCLAIMNO());
				boMessage.setOTHERADJST(
						claimData.getOtherAdjustments() == null ? new BigDecimal(0) : claimData.getOtherAdjustments());
				log.debug("OTHERADJST= "+boMessage.getOTHERADJST());

				boMessage.setZCLMRECD(Util.dateToString(claimData.getProcessInitiatonTS(), "yyyyMMdd"));
				log.debug("ZCLMRECD=" + Util.dateToString(claimData.getProcessInitiatonTS(), "yyyyMMdd"));

				boMessage.setCLNTNUM(claimData.getInsuredClientID());

				int insertCount = claimDetailsDao.lARequestTracker_Insert(jsonInData.getCLAIMNO(),
						jsonInData.getACITVITYID(), FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimRegistration,
						boMessage.getBOMessage(), "0");

				if (insertCount == 0) {
					throw new CamelException("No Row updated for LAREquestTracker");
				}
				// }

				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");

			} else {
				throw new Exception("Input String is empty");
			}

			log.info("WSDULRegClaimDataProcessor - In Method - process - getBody");

		} catch (Exception e) {
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}

		log.info("WSDULRegClaimDataProcessor - In Method - process - response in String # " + jsonoutData.toString());
		exchange.getOut().setBody(jsonoutData);

	}

}
