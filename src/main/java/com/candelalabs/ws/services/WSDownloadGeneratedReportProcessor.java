package com.candelalabs.ws.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSDownloadGeneratedReportDataIn;
import com.candelalabs.api.model.WSDownloadGeneratedReportDataOutWSResponse;
import com.candelalabs.ws.dao.ClaimDetailsDao;

@Component
public class WSDownloadGeneratedReportProcessor implements Processor {
	
	private static final Log log = LogFactory.getLog("WSDownloadGeneratedReportProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {

		log.info("WSDownloadGeneratedReportProcessor - Service is invoked >>");
		WSDownloadGeneratedReportDataOutWSResponse wsResponse = new WSDownloadGeneratedReportDataOutWSResponse();
		
		FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
		
		try {
			
			WSDownloadGeneratedReportDataIn dataIn = (exchange.getIn().getBody(WSDownloadGeneratedReportDataIn.class));
			log.info("Input - " + dataIn.getWsInput().toString());
			
			String filePath = dataIn.getWsInput().getStrReportNamePath();
			log.info("filePath - " + filePath);
			
			File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];
            
            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);
            log.info("bytesArray length - " + bytesArray.length);
            
            String finalCOntent = Base64.getEncoder().encodeToString(bytesArray);
            log.info("finalCOntent - " + finalCOntent);
            
            wsResponse.setWsFileData(finalCOntent);
			wsResponse.setWsProcessingStatus("1");
			wsResponse.setWsSuccessMessage("File exists");
			
		} catch (Exception e) {
			
			log.error("WSDownloadGeneratedReportProcessor Error - " + e);
			wsResponse.setWsExceptionMessage( e.getMessage());
			wsResponse.setWsProcessingStatus("2");
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat "+tr);
			}
			
		} finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
		
		log.info("Final Response " + wsResponse.toString());
		exchange.getOut().setBody(wsResponse);		
	}


}
