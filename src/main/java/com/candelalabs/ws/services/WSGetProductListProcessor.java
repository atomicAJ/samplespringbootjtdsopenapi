package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataOutWSGetProductListWSResponse;
import com.candelalabs.api.model.ProductListData;
import com.candelalabs.ws.dao.ODSDao;

@Component
public class WSGetProductListProcessor implements Processor {
	
	private static final Log log = LogFactory.getLog("WSGetProductListProcessor");

	@Autowired
	ODSDao odsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		DataOutWSGetProductListWSResponse wsResponse = new DataOutWSGetProductListWSResponse();
		
		try {
			
			List<ProductListData> productListData = new ArrayList<ProductListData>();
			
			productListData = odsDao.getProductList();
			
			wsResponse.setWsProcessingStatus("1");
			wsResponse.setWsSuccessMessage("Success");
			wsResponse.setStrReportTypeList(productListData);
		}catch(Exception e) {
			log.error("WSGetProductListProcessor Error" + e);
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsSuccessMessage(e.getMessage());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		log.info("Final Response " + wsResponse.toString());
		exchange.getOut().setBody(wsResponse);
	}

}
