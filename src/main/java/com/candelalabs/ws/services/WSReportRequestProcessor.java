package com.candelalabs.ws.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ArgValuePairs;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSReportRequestDataIn;
import com.candelalabs.api.model.WSReportRequestDataOut;
import com.candelalabs.api.model.WSReportRequestDataOutWSResponse;
import com.candelalabs.api.model.WSUpdateDataDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WSReportRequestProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSReportRequestProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ProducerTemplate producerTemplate;

	@Override
	public void process(Exchange exchange) throws Exception {

		log.info("WSReportRequestProcessor - Service is invoked");
		WSReportRequestDataIn dataIn = (exchange.getIn().getBody(WSReportRequestDataIn.class));
		WSReportRequestDataOut response = new WSReportRequestDataOut();
		WSReportRequestDataOutWSResponse wsResponse = new WSReportRequestDataOutWSResponse();
		String strReportOutputFormat = "";

		String pattern = "yyyy-MM-dd hh:mm:ss"; 
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern); 
		String currDtString = simpleDateFormat.format(new Date());

		try {
			log.debug("----- dataIn -----");
			log.debug(dataIn.getWsInput().toString());
			if(dataIn.getWsInput().getStrReportName() != "" && dataIn.getWsInput().getStrReportName() != null && 
					dataIn.getWsInput().getStrReportType() != "" && dataIn.getWsInput().getStrReportType() != null && 
					dataIn.getWsInput().getStrReportRequestUser() !="" && dataIn.getWsInput().getStrReportRequestUser() !=null) {
				List<String> reportOutputFormatList = claimDetailsDao.getReportOutuputFormat(dataIn.getWsInput().getStrReportName(), dataIn.getWsInput().getStrReportType());
				if(reportOutputFormatList.size() > 0) {
					for (String reportOutputFormat : reportOutputFormatList) {

						strReportOutputFormat = reportOutputFormat;	

						if(reportOutputFormat.equalsIgnoreCase("xls")) {
							log.debug("reportOutputFormat : "+reportOutputFormat);
							String reportRequestId = null;
							String strCurrentMaxRequestId = claimDetailsDao.getPencilReportRequestId();
							if(strCurrentMaxRequestId == null) {
								reportRequestId = "1";
							} else {
								int reportRequestID = Integer.parseInt(strCurrentMaxRequestId);
								reportRequestID += 1;
								reportRequestId = String.valueOf(reportRequestID);
							}

							ObjectMapper mapper = new ObjectMapper();
							String wsInputJsonString = mapper.writeValueAsString(dataIn.getWsInput());

							WSUpdateDataDataIn updateDataIn = new WSUpdateDataDataIn();
							List<ArgValuePairs> argValuePairsList = new ArrayList<ArgValuePairs>();

							updateDataIn.setStrSQLQuery("INSERT INTO CASEREPORTREQUEST(ReportRequestID,ReportName,ReportRequestUser,ReportOutputFormat,"
									+ "ReportRequestTimeStamp,ReportType,ReportRequestInputData,ReportGenerated,ProcessedFlag,ReportGenerationMessage) VALUES (?,?,?,?,?,?,?,?,?,?)");

							argValuePairsList.add(prepareArgValuePairsObj("0", "string", reportRequestId));
							argValuePairsList.add(prepareArgValuePairsObj("1", "string", dataIn.getWsInput().getStrReportName()));
							argValuePairsList.add(prepareArgValuePairsObj("2", "string", dataIn.getWsInput().getStrReportRequestUser()));
							argValuePairsList.add(prepareArgValuePairsObj("3", "string", dataIn.getWsInput().getStrReportOutuputFormat()));
							argValuePairsList.add(prepareArgValuePairsObj("4", "date", currDtString));
							argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportType()));
							argValuePairsList.add(prepareArgValuePairsObj("6", "string", wsInputJsonString));
							argValuePairsList.add(prepareArgValuePairsObj("7", "string", "0"));
							argValuePairsList.add(prepareArgValuePairsObj("8", "string", "0"));
							argValuePairsList.add(prepareArgValuePairsObj("9", "string", "Report Generation in Progress"));

							updateDataIn.setArgValuePairs(argValuePairsList);

							log.info("Invoking the WSUpdateData Service with payload = " + updateDataIn.toString());

							JsonResponse wsUpdateDataResponse = producerTemplate.requestBody("direct:WSUpdateDataProcessor", updateDataIn,
									JsonResponse.class);

							if(wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("1")) {
								log.info("Record inserted in CASEREPORTREQUEST table successfully");
								wsResponse.wsProcessingStatus("1");
								wsResponse.setWsSuccessMessage("Report request submitted successfully");
								wsResponse.setStrReportRequestID(reportRequestId);
								wsResponse.setStrReportOutuputFormat(dataIn.getWsInput().getStrReportOutuputFormat());
								wsResponse.setStrReportName(dataIn.getWsInput().getStrReportName());
								wsResponse.setStrReportRequestTime(currDtString);
								wsResponse.setStrReportGenerationStatus("0");
								wsResponse.setStrReportGenerationRemarks("Report Generation in Progress");
								response.setWsResponse(wsResponse);
							}
							if(wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("2")) {
								log.error("Exception insert record in CASEREPORTREQUEST table");
								wsResponse.wsProcessingStatus("2");
								wsResponse.wsExceptionMessage(wsUpdateDataResponse.getWsExceptionMessage());
								wsResponse.setStrReportRequestTime(new Date().toString());
								wsResponse.setStrReportResponseTime(new Date().toString());
								wsResponse.setStrReportGenerationStatus("0");
								wsResponse.setStrReportGenerationRemarks("Report Request could not be registered / processed");
								response.setWsResponse(wsResponse);
							}
						}
						if(reportOutputFormat.equalsIgnoreCase("web")) {
							log.debug("reportOutputFormat : "+reportOutputFormat);
							//Logic for web report goes here, not in current scope
						}
					}
				} else {
					wsResponse.wsProcessingStatus("2");
					wsResponse.wsExceptionMessage("REPORTOUTPUTFORMAT not found in REPORTSELECTIONMASTER for REPORTNAME : "+dataIn.getWsInput().getStrReportName()+" and "
							+ "REPORTCATEGORY : "+dataIn.getWsInput().getStrReportType());
					wsResponse.setStrReportOutuputFormat(strReportOutputFormat);
					wsResponse.setStrReportName(dataIn.getWsInput().getStrReportName());
					wsResponse.setStrReportRequestTime(currDtString);
					wsResponse.setStrReportResponseTime(new Date().toString());
					wsResponse.setStrReportGenerationStatus("0");
					wsResponse.setStrReportGenerationRemarks("Report Request could not be registered / processed");
					response.setWsResponse(wsResponse);
				}
			} else {
				wsResponse.wsProcessingStatus("2");
				wsResponse.wsExceptionMessage("Report Name, Report Type and Report Request User are mandatory for generating report");
				wsResponse.setStrReportRequestTime(new Date().toString());
				wsResponse.setStrReportResponseTime(new Date().toString());
				wsResponse.setStrReportGenerationStatus("0");
				wsResponse.setStrReportGenerationRemarks("Report Request could not be registered / processed");
				response.setWsResponse(wsResponse);
			}
		}catch (Exception e) {
			log.error("WSReportRequestProcessor Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			wsResponse.wsProcessingStatus("2");
			wsResponse.wsExceptionMessage("Report Request could not be registered - "+e.getMessage());
			wsResponse.setStrReportOutuputFormat(strReportOutputFormat);
			wsResponse.setStrReportName(dataIn.getWsInput().getStrReportName());
			wsResponse.setStrReportRequestTime(currDtString);
			wsResponse.setStrReportResponseTime(new Date().toString());
			wsResponse.setStrReportGenerationStatus("0");
			wsResponse.setStrReportGenerationRemarks("Report Request could not be registered.");
			response.setWsResponse(wsResponse);
		}

		log.info("Final Response " + response.toString());
		exchange.getOut().setBody(response);
	}
	public ArgValuePairs prepareArgValuePairsObj(String index, String paramType,  String value) {
		ArgValuePairs argValuePairsObj = new ArgValuePairs();
		argValuePairsObj.setIndex(index);
		argValuePairsObj.setTypeParam(paramType);
		argValuePairsObj.setValue(value);
		return argValuePairsObj;
	}
}
