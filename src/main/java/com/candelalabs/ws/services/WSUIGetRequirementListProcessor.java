/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataOutGetRequirementListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetRequirementListProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetRequirementListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetRequirementListWSResponse wsresponse = new DataOutGetRequirementListWSResponse();
		try {
			List<String> reqList = new ArrayList<String>();
			reqList = this.oDSDao.getRequirementList();
			if (reqList.size() <= 0) {
				throw new DAOException("Not available any data in V_REQUIREMENTLIST table");
			}

			wsresponse.setStrRequirementList(reqList);
			wsresponse.setWsSuccessMessage("Successfully retrieved the RequirementDetails data");
			wsresponse.setWsProcessingStatus("1");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL " + e.getMessage());
		}

		log.info("JSON RESPONSE PAYLOAD: " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);

	}

}
