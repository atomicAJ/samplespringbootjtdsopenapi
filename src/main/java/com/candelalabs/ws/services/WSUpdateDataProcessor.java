/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ArgValuePairs;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.SignalDetails;
import com.candelalabs.api.model.SignalDetailsVariables;
import com.candelalabs.api.model.WSRegisterNotificationDataIn;
import com.candelalabs.api.model.WSRegisterRequirementDataIn;
import com.candelalabs.api.model.WSUpdateDataDataIn;
import com.candelalabs.api.model.WSUpdateRequirementDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSUpdateDataProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSUpdateDataProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;


	@Autowired
	JdbcTemplate jdbcTemplateClaims;
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		JsonResponse response = new JsonResponse();
		response.setWsExceptionMessage("");
		response.setWsSuccessMessage("");

		try {
			WSUpdateDataDataIn dataIn = exchange.getIn().getBody(WSUpdateDataDataIn.class);
			
			String sqlQuery="";
			sqlQuery=dataIn.getStrSQLQuery();
			List<ArgValuePairs> argValuePairs = dataIn.getArgValuePairs();
			
			if (sqlQuery!=null && sqlQuery!="" && sqlQuery.isEmpty()==false){
				
				if(argValuePairs!=null&&argValuePairs.size()>0 ){
					Object[] param_ar = new Object[argValuePairs.size()];
				      for (int i=0;i<argValuePairs.size();i++){
				    	  for (Object par_ar:param_ar){
				    		  log.info("Integer.valueOf(argValuePairs.get(0).getValue() -----"+ (argValuePairs.get(0).getIndex()));
				    		  if(i==Integer.valueOf(argValuePairs.get(i).getIndex())){
				    			   if (argValuePairs.get(i).getTypeParam() == "integer") {
				    				   log.info("type param integer");
				    				   param_ar[i]=Integer.valueOf(argValuePairs.get(i).getValue());
				    				   
				    			   }
				    			   else if (argValuePairs.get(i).getTypeParam()  == "number"){
				    				   log.info("type param number");
				    				   param_ar[i] = BigDecimal.valueOf(Integer.valueOf(argValuePairs.get(i).getValue()));  
				    			   }
				    			   else if (argValuePairs.get(i).getTypeParam()  == "string"){
				    				   log.info("type param string");
				    				   param_ar[i] =  argValuePairs.get(i).getValue().toString();
				    			   }
				    			   else if (argValuePairs.get(i).getTypeParam()  == "date"){
				    				   log.info("type param date");
				    				   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    				   String sDate1=argValuePairs.get(i).getValue().toString();
				    				   Date date = formatter.parse(sDate1);
				    				    log.info("date is : "+date);
				    				   param_ar[i] = date;
				    			   }
				    			   else {
				    				   log.info("type param blanl");
				                         param_ar[i] = argValuePairs.get(i).getValue();
				                     }
				    		  }
				    	  }
				      }
				      jdbcTemplateClaims.update(sqlQuery,param_ar) ;
				}
				else{
					jdbcTemplateClaims.update(sqlQuery);
				}
				 response.setWsSuccessMessage("Data has been successfully inserted/updated/deleted");
			}
			else{
				response.setWsSuccessMessage("SQL query is null");
			}
			response.setWsProcessingStatus("1");
			

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("ERROR DETAIL = " + e);
		}

		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);
	}

	
}
