package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDULUpdClaimRejectStatusDataIn;
import com.candelalabs.ws.FWDConstants;
//import org.apache.commons.logging.Log;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.*;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;

@Component
@PropertySource("classpath:application.properties")
public class WSDULUpdClaimRejectStatusProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao odsDao;
	@Value("${USRPRF}")
	public String USRPRF;
	private static Log log = LogFactory.getLog("WSDULUpdClaimRejectStatusProcessor");// .getLog(WSUpdateClaimStatusProcessor.class);

	// below working config
	// private static final Logger log =
	// LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());
	/*
	 * @Autowired private ClaimDetailsDao updatedRemarksInClaimData;
	 */

	int updateSuccess = 0;

	public void process(Exchange exchange) throws Exception {

		log.info(" WSDULUpdClaimRejectStatusProcessor -- testing log");
		log.info(" WSDULUpdClaimRejectStatusProcessor.class.getName() --"
				+ WSDULUpdClaimRejectStatusProcessor.class.getName());
		log.info(" WSDULUpdClaimRejectStatusProcessor.class.getName() --"
				+ WSDULUpdClaimRejectStatusProcessor.class.toString());
		System.out.println("WSDULUpdClaimRejectStatusProcessor -- testing console log");
		System.out.println(WSDULUpdClaimRejectStatusProcessor.class.getName());
		System.out.println(WSDULUpdClaimRejectStatusProcessor.class.toString());

		Gson gson = new GsonBuilder().serializeNulls().create();
		WSDULUpdClaimRejectStatusDataIn jsonData = new WSDULUpdClaimRejectStatusDataIn(); // for
																							// input
																							// ->
																							// jsonData
		JsonResponse jsonoutData = new JsonResponse(); // for output ->
														// jsonoutData
		try {

			jsonData = (exchange.getIn().getBody(WSDULUpdClaimRejectStatusDataIn.class));

			PaddingUtil pdngutl = new PaddingUtil();
			if (null != jsonData) {

//				log.info("Get LaRequestTrackerModel table with claimNo= " + jsonData.getCLAIMNO());
//				LaRequestTrackerModel laRequestTrackerData = this.claimDetailsDao.getLaRequestTrackerByClaimNo(
//						jsonData.getCLAIMNO(), jsonData.getACITVITYID(),
//						FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimRejected);
//				log.info("Successfully Got LaRequestTrackerModel table ");
//
//				if (laRequestTrackerData.getBoidentifier() == null && laRequestTrackerData.getMsgtosend() == null
//						&& laRequestTrackerData.getMsgcreatets() == null
//						&& laRequestTrackerData.getFuturemesg() == null) {
					log.info("Getting the data from CLAIMDATA table");
					ClaimDataNewModel claimsData = new ClaimDataNewModel();
					claimsData = this.claimDetailsDao.getClaimDataNew(jsonData.getCLAIMNO());
					log.info("Successfully got the data from Claim Data");
					PolicyDetails policyData = new PolicyDetails();
					String msgtosend = null;
					if (claimsData != null) {
						log.info("Getting the data from PolicyDetail Table");
						policyData = odsDao.getPolicyDetail(claimsData.getPolicyNumber());
						log.info("Successfully got the data from PolicyDetails table");
						String wthSgnOfChrg = null;
						log.info("Structuring the BOMessage data");
						msgtosend = pdngutl.padRight(USRPRF, 9) + 
								
								pdngutl.padZero(Integer.parseInt(policyData.getCOMPANY()), 1) +
								
								pdngutl.padRight(FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimRejected, 20)
								
								+ pdngutl.padRight(FWDConstants.MESSAGE_LANGUAGE, 1)
								
								+ pdngutl.padZero(Integer.parseInt(FWDConstants.BO_DEATH_CLAIM_REJECTED_MESSAGE_LENGTH),
										5)
								
								+ pdngutl.padRight(FWDConstants.MORE_INDICATOR, 1)
								
								+ pdngutl.padRight(claimsData.getPolicyNumber(), 10)

								+ Util.dateToString(claimsData.getClaimDecisionDate(), "yyyyMMdd")

								+ pdngutl.padRight(jsonData.getCLAIMNO().toString(), 11)
								
								+ pdngutl.PadForDoubleWithSym(0.0, 14);
						
						log.info("Getting the docIdList");
						String docIdList = this.claimDetailsDao.getDocIDList(jsonData.getCLAIMNO(), "Y");
						log.info("Successfully got the docIdList = "+docIdList);
						log.info("Getting the subCategoryList");
						String subCategoryList = this.odsDao.getListSubcategory(docIdList);
						log.info("Successfully got the subCategoryList = "+subCategoryList);
						log.info("Updating the NotificationData table");
						int rowUp = this.claimDetailsDao.updateNotificationData(jsonData.getCLAIMNO(), subCategoryList);
						if (rowUp == 0) {
							throw new DAOException("No row is updated for NOTIFICATIONDATA TABLE");
						}
						log.info("Successfully got the NotificationData table");

						log.info("Successfully structured the BOMessage = "+msgtosend);

						log.info("Inserting the data into LAREQUESTTRACKERTABLE");
						int result = claimDetailsDao.lARequestTracker_Insert(jsonData.getCLAIMNO(),
								jsonData.getACITVITYID(), FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimRejected,
								msgtosend, "0");
						if (result == 0) {
							throw new DAOException("No Updated Row for LARequestTracker");
						}
						log.info("Successfully inserted the data into LAREQUESTTRACKER table");

					} else {
						throw new DAOException("Claim Data table is null for Claim No= " + jsonData.getCLAIMNO());
					}

					// result =
					// claimDetailsDao.dULActualClaimAmount(jsonData,msgtosend);

				//}

				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");

				/*
				 * jsonoutData.setUSRPRF(pdngutl.padRight(jsonData.getUSRPRF().
				 * toString(),9)); jsonoutData.setCHDRCOY(pdngutl.padZero
				 * (Integer.parseInt(jsonData.getCHDRCOY().toString()), 1));
				 * jsonoutData.setBOIDEN(pdngutl.padRight(jsonData.getBOIDEN().
				 * toString(),20));
				 * jsonoutData.setMSGLNG(pdngutl.padRight(jsonData.getMSGLNG().
				 * toString(), 1)); jsonoutData.setMSGCNT(pdngutl.padZero
				 * (Integer.parseInt(jsonData.getMSGCNT().toString()), 5));
				 * jsonoutData.setINDC(pdngutl.padRight(jsonData.getINDC().
				 * toString(), 1));
				 * jsonoutData.setCLAIMNO(pdngutl.padRight(jsonData.getCLAIMNO()
				 * .toString(), 11));
				 * jsonoutData.setCHDRNUM(pdngutl.padRight(jsonData.getCHDRNUM()
				 * .toString(), 8));
				 */

			} else

				throw new Exception("Input String is Empty");

			log.info("WSDULUpdClaimRejectStatusProcessor - In Method - process - getBody");

		} catch (Exception e) {
			e.printStackTrace();
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			// jsonoutData.setSOURCEEXCEPTIONPROCESS("Null");
			// jsonoutData.setSOURCEEXCEPTIONSTEP("Null");

		}

		String json = gson.toJson(jsonoutData);
		log.info("WSDULUpdClaimRejectStatusProcessor - In Method - process - Payload= " + json);
		exchange.getOut().setBody(jsonoutData);

	}

}
