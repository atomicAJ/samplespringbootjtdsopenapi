/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataUIClaimDetailsWSResponse;
import com.candelalabs.api.model.ClaimWorksheetUIData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheet;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClaimHistory;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClientComponentList;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClients;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetClientsOwnerData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetComments;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetComponentsListData;
import com.candelalabs.api.model.ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails;
import com.candelalabs.api.model.DataInGetWSGetWorksheet;
import com.candelalabs.api.model.DataInGetWSGetWorksheetWSInput;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ClaimHistoryModel;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.ComponentDetailsBO;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.model.tables.PolicyRelatedModel;
import com.candelalabs.ws.model.tables.UWCommentsModel;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetWorksheetDataProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSUIGetWorksheetDataProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub

		ClaimWorksheetUIData response = new ClaimWorksheetUIData();
		ClaimDataUIClaimDetailsWSResponse wsresponse = new ClaimDataUIClaimDetailsWSResponse();

		ClaimWorksheetUIDataClaimWorksheet resClaimWorksheet = new ClaimWorksheetUIDataClaimWorksheet();
		List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> respCompList_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetComponentsListData>();
		List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> respPolRelatedDetails_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails>();
		List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> respClaimHistory = new ArrayList<ClaimWorksheetUIDataClaimWorksheetClaimHistory>();

		ClaimWorksheetUIDataClaimWorksheetClients respClients = new ClaimWorksheetUIDataClaimWorksheetClients();
		List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsOwnerData_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData>();
		List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsLifeInsured_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData>();
		List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsBenData_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData>();

		ClaimWorksheetUIDataClaimWorksheetComments respComments = new ClaimWorksheetUIDataClaimWorksheetComments();
		List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsNBUW_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments>();
		List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsPSUW_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments>();
		List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsCLUW_li = new ArrayList<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments>();

		try {
			log.info("Service is invoked");
			DataInGetWSGetWorksheet dataws = exchange.getIn().getBody(DataInGetWSGetWorksheet.class);
			DataInGetWSGetWorksheetWSInput dataIn = dataws.getWsInput();
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
			if (dataIn != null) {

				log.info("Getting the ClaimData table with claimNumber = " + dataIn.getStrClaimNumber());
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getStrClaimNumber());
				if (claimData == null) {
					throw new DAOException(
							"ClaimData is not available with claimNumber = " + dataIn.getStrClaimNumber());
				}
				log.info("Successfully retrieved the ClaimData table");

				log.info("Getting the V_POLICYDETAILS table with policyNumber = " + claimData.getPolicyNumber());
				PolicyDetails polData = this.odsDao.getPolicyDetail(claimData.getPolicyNumber());
				if (polData == null) {
					throw new DAOException(
							"PolicyDetail Data is not available with polNum = " + claimData.getPolicyNumber());
				}
				log.info("Successfully got the V_POLICYDETAILS table with polNum = " + claimData.getPolicyNumber());

				if (dataIn.getStrProcessType().trim().equalsIgnoreCase("CL")) {
					log.info("Getting the Lifename from V_CLIENTDETAILS table with InsuredClientID = "
							+ claimData.getInsuredClientID());
					String insuredClientName = this.odsDao.getLifenameFromClientDetails(claimData.getInsuredClientID());
					log.info("Successfully got the Lifename from V_CLIENTDETAILS  = " + insuredClientName);
					log.info("Setting the ClaimWorksheet data");
					resClaimWorksheet.setStrClaimNumber(dataIn.getStrClaimNumber());
					resClaimWorksheet.setStrApplicationNumber(dataIn.getStrApplicationNumber());
					resClaimWorksheet.setStrPOSRequestNumber(dataIn.getStrPOSRequestNumber());
					resClaimWorksheet.setStrProcessType(dataIn.getStrProcessType());
					resClaimWorksheet.setStrPolicyNumber(claimData.getPolicyNumber());
					resClaimWorksheet.setStrProductCode(polData.getPRODUCTCODE());
					resClaimWorksheet.setStrProductName(polData.getPRODUCTNAME());
					resClaimWorksheet.setStrPolicyStatus(polData.getPOLICYSTATUS());
					resClaimWorksheet.setStrPolicyCurrency(polData.getCURRENCY());
					resClaimWorksheet.setStrPremiumStatus(polData.getPREMIUMSTATUSCODE());
					resClaimWorksheet.setStrOwnerName(this.odsDao.getOwnerName(polData.getOWNERCLIENTNO()));
					resClaimWorksheet.setStrOwnerClientNumber(polData.getOWNERCLIENTNO());
					resClaimWorksheet.setStrLifeClientNumber(polData.getLIFECLIENTNO());
					resClaimWorksheet.setStrPayorClientNumber(polData.getPAYORCLIENTNO());
					resClaimWorksheet
							.setStrPayorName(this.odsDao.getLifenameFromClientDetails(polData.getPAYORCLIENTNO()));
					resClaimWorksheet.setDtPolicyIssuedDate(Util.dateToBigDecimal(polData.getPOLICYISSUEDATE()));
					resClaimWorksheet.setDtRiskCommencementDate(Util.dateToBigDecimal(polData.getRISKCOMMDATE()));
					resClaimWorksheet.setDtPaidToDate(Util.dateToBigDecimal(polData.getPAIDTODATE()));
					resClaimWorksheet.setDtBillToDate(Util.dateToBigDecimal(polData.getBILLTODATE()));
					resClaimWorksheet.setStrPaymentMethod(polData.getPAYMENTMETHOD());
					resClaimWorksheet.setStrPaymentFrequency(polData.getPAYMENTFREQ());
					resClaimWorksheet.setStrAgentName(polData.getAGENTNAME());
					resClaimWorksheet.setStrAgentNumber(polData.getAGENTID());
					resClaimWorksheet.setDblTotalPremium(polData.getPREMIUMTOTAL());
					resClaimWorksheet.setDblBasicPremium(polData.getBASICPREMIUM());
					resClaimWorksheet.setDblRegularTopup(polData.getREGULARTOPUP());
					resClaimWorksheet.setDtReinstatementDate(Util.dateToBigDecimal(polData.getREINSTATDATE()));
					log.info("First phase of the data has been completed");

					fetchComponentListData(claimData.getPolicyNumber(), respCompList_li);
					fetchPolicyRelatedDetails(claimData.getInsuredClientID(), insuredClientName,
							respPolRelatedDetails_li);
					fetchClientDetailsOwner(claimData.getPolicyNumber(), polData.getOWNERCLIENTNO(),
							respClientsOwnerData_li);
					fetchClientDetailsBeneficiary(claimData.getPolicyNumber(), respClientsBenData_li);
					fetchClientDetailsLifeInsured(claimData.getPolicyNumber(), respClientsLifeInsured_li);
					fetchClaimHistory(claimData.getPolicyNumber(), respClaimHistory);
					fetchNBUWComments(claimData.getPolicyNumber(), respCommentsNBUW_li);
					fetchPSUWComments(claimData.getPolicyNumber(), respCommentsPSUW_li);
					fetchCLUWComments(claimData.getPolicyNumber(), respCommentsCLUW_li);
				} else if (dataIn.getStrProcessType().trim().equalsIgnoreCase("PS")) {
					// RESERVED for POS
				} else if (dataIn.getStrProcessType().trim().equalsIgnoreCase("NB")) {
					// RESERVED for NB
				} else {
					throw new Exception("Unknown PROCESS TYPE Given in the input = " + dataIn.getStrProcessType());
				}

				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Successfully fetch the UWWorksheet data");

			} else {
				throw new Exception("Request Input String is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERRROR Message = " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL = " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
		}
		respComments.setClUWComments(respCommentsCLUW_li);
		respComments.setNbUWComments(respCommentsNBUW_li);
		respComments.setPsUWComments(respCommentsPSUW_li);
		resClaimWorksheet.setComments(respComments);

		respClients.setBeneficiaryData(respClientsBenData_li);
		respClients.setLifeInsuredData(respClientsLifeInsured_li);
		respClients.setOwnerData(respClientsOwnerData_li);
		resClaimWorksheet.setClients(respClients);

		resClaimWorksheet.setClaimHistory(respClaimHistory);
		resClaimWorksheet.setPolicyRelatedDetails(respPolRelatedDetails_li);
		resClaimWorksheet.setComponentsListData(respCompList_li);

		response.setWsResponse(wsresponse);
		response.setClaimWorksheet(resClaimWorksheet);

		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);

	}

	private void fetchComponentListData(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetComponentsListData> respCompList_li) throws DAOException {
		log.info("Retrieving the V_COMPONENTDETAILS data list with PolicyNumber = " + policyNumber);
		List<String> clientNumber_li = this.odsDao.getClientNumberList(policyNumber);
		

		if (clientNumber_li != null && clientNumber_li.size() > 0) {
			for (String clientNo : clientNumber_li) {
				List<ClaimWorksheetUIDataClaimWorksheetClientComponentList> compListOutDetails = new ArrayList<ClaimWorksheetUIDataClaimWorksheetClientComponentList>();
				ClaimWorksheetUIDataClaimWorksheetComponentsListData compListOutParent = new ClaimWorksheetUIDataClaimWorksheetComponentsListData();
				compListOutParent.setStrClientNumber(clientNo);
				List<ComponentDetailsBO> compDetails_li = this.odsDao.getComponentDetailsBO(policyNumber, clientNo);
				log.info("Successfully retrieving the ComponentDetails list with size = " + compDetails_li.size());
				log.info("Looping the compDetails_li data to set the ComponentListData");
				if (compDetails_li != null && compDetails_li.size() > 0) {
					for (ComponentDetailsBO compDetails : compDetails_li) {
						ClaimWorksheetUIDataClaimWorksheetClientComponentList compListOut = new ClaimWorksheetUIDataClaimWorksheetClientComponentList();
						compListOut.setStrCLDRider(compDetails.getRider());
						compListOut.setStrCLDComponentName(compDetails.getCOMPONENTNAME());
						compListOut.setStrCLDComponentCode(compDetails.getCOMPONENTCODE());
						compListOut.setStrCLDLifeNumber(compDetails.getLife());
						String strClientNumber = compDetails.getCLIENTNUMBER();
						compListOut.setStrCLDLifeName(this.odsDao.getLifenameFromClientDetails(strClientNumber));
						compListOut.setDblCLDSumAssured(compDetails.getSUMASSURED());
						compListOut.setDtCLDComponentRiskCommencementDate(
								Util.dateToBigDecimal(compDetails.getCOMPONENTRCD()));
						compListOut
								.setDtCLDComponentRiskCessDate(Util.dateToBigDecimal(compDetails.getCOMPRISKCESDATE()));
						compListOut
								.setDtCLDComponentPremCessDate(Util.dateToBigDecimal(compDetails.getCOMPPREMCESDATE()));
						compListOut.setStrCLDComponentStatus(compDetails.getCOMPONENTSTATUS());
						compListOut.setStrCLDComponentPremStatus(compDetails.getCOMPPREMSTATUS());
						compListOut.setStrCLDPlanCode(compDetails.getPLANCODE());
						compListOut.setStrCLDUnit(compDetails.getUNIT() == null ? "0" : compDetails.getUNIT().toString());
						compListOutDetails.add(compListOut);
					}
					compListOutParent.setClientComponentList(compListOutDetails);
				}
				respCompList_li.add(compListOutParent);
			}
			log.info("Successfully finished looping the compDetails_li");
		}

		
	}

	private void fetchPolicyRelatedDetails(String insuredClientID, String insuredClientName,
			List<ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails> respPolRelatedDetails_li) throws DAOException {
		log.info("Retrieving the V_POLICYRELATEDDATA data list with InsuredClientID = " + insuredClientID);
		List<PolicyRelatedModel> polRelated_li = this.odsDao.getPolicyRelatedData(insuredClientID);
		log.info("Successfully retrieving the V_POLICYRELATEDDATA list with size = " + polRelated_li.size());
		log.info("Looping the polRelated_li data to set the PolicyRelatedDetails Data");
		if (polRelated_li != null && polRelated_li.size() > 0) {
			for (PolicyRelatedModel polRelatedModel : polRelated_li) {
				ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails polRelatedOut = new ClaimWorksheetUIDataClaimWorksheetPolicyRelatedDetails();
				polRelatedOut.setStrPRDPolicyNumber(polRelatedModel.getPOLICYNUMBER());
				polRelatedOut.setStrPRDProductCode(polRelatedModel.getPRODUCTCODE());
				polRelatedOut.setStrPRDProductName(polRelatedModel.getPRODUCTNAME());
				polRelatedOut.setStrPRDOwnerName(polRelatedModel.getOWNERNAME());
				polRelatedOut.setStrPRDInsuredName(insuredClientName);
				polRelatedOut.setStrPRDClientRole(polRelatedModel.getCLIENTROLE());
				polRelatedOut.setStrPRDPolicyStatus(polRelatedModel.getPOLICYSTATUS());
				polRelatedOut.setDblPRDSumAssured(polRelatedModel.getSUMASSURED());
				respPolRelatedDetails_li.add(polRelatedOut);
			}
		}
		log.info("Successfully finished looping the polRelated_li");
	}

	private void fetchClientDetailsOwner(String policyNumber, String ownerCLientNo,
			List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsOwnerData_li) throws DAOException {
		log.info("Retrieving the V_CLIENTDETAILS data list with OWnerCLientNO = " + ownerCLientNo);
		List<ClientDetails> clientDetails_li = this.odsDao.getClientDetailsli(ownerCLientNo);
		log.info("Successfully retrieving the V_CLIENTDETAILS list with size = " + clientDetails_li.size());
		log.info("Getting the lifeNumber from V_COMPONENTDETAILS with polNum = " + policyNumber + " and ClientNo = "
				+ ownerCLientNo);
		String lifeNumber = this.odsDao.getLifefromComponentDetails(policyNumber, ownerCLientNo);
		log.info("Successfully got the lifenumber = " + lifeNumber);
		log.info("Getting the relationship with polNum = " + policyNumber + " and ClientNo = " + ownerCLientNo);
		String relationship = this.odsDao.getRelationshipFromBenDetails(policyNumber, ownerCLientNo);
		log.info("Getting the percentage from BeneficiaryDetails with polNum = " + policyNumber);
		String ownerPercentage = this.odsDao.getPercentageFromBenDetails(policyNumber, ownerCLientNo);
		log.info("Successfully got the percentage = " + ownerPercentage);
		log.info("Looping the Owner CLient Details data list to fill the Clients Owner Data");
		if (clientDetails_li != null && clientDetails_li.size() > 0) {
			for (ClientDetails clientDetails : clientDetails_li) {
				ClaimWorksheetUIDataClaimWorksheetClientsOwnerData clientOwner = new ClaimWorksheetUIDataClaimWorksheetClientsOwnerData();

				clientOwner.setStrCLNTLifeName(clientDetails.getLIFENAME());
				clientOwner.setStrCLNTLifeNumber(lifeNumber);
				clientOwner.setStrCLNTClientNumber(clientDetails.getCLIENTNO());
				clientOwner.setDtCLNTClientDOB(Util.dateToBigDecimal(clientDetails.getDOB()));
				clientOwner.setStrCLNTGender(clientDetails.getSEX());
				clientOwner.setStrCLNTAddress1(clientDetails.getAddress1());
				clientOwner.setStrCLNTAddress2(clientDetails.getAddress2());
				clientOwner.setStrCLNTAddress3(clientDetails.getAddress3());
				clientOwner.setStrCLNTAddress4(clientDetails.getAddress4());
				clientOwner.setStrCLNTAddress5(clientDetails.getAddress5());
				clientOwner.setStrCLNTOccupationCode(clientDetails.getOccupation_code());
				clientOwner.setStrCLNTIDType(clientDetails.getIDType());
				clientOwner.setStrCLNTIDNumber(clientDetails.getIDNumber());
				clientOwner.setStrCLNTPhoneNumber1(clientDetails.getPHONENO01());
				clientOwner.setStrCLNTPhoneNumber2(clientDetails.getPHONENO02());
				clientOwner.setStrCLNTPhoneNumber3(clientDetails.getPHONENO03());
				clientOwner.setStrCLNTNationality(clientDetails.getNationality());
				clientOwner.setStrCLNTClientStatus(clientDetails.getClientStatus());
				clientOwner.setStrCLNTRelationship(relationship);
				clientOwner.setStrCLNTBeneficiaryPercentage(ownerPercentage);
				clientOwner.setStrCLNTAbuseFlag(clientDetails.getABUSEFLAG());
				clientOwner.setStrCLNTFatcaFlag(clientDetails.getFATCAFLAG());
				respClientsOwnerData_li.add(clientOwner);

			}
		}
		log.info("Successfully finished looping the Owner clientDetails_li");
	}

	private void fetchClientDetailsBeneficiary(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsBenData_li) throws DAOException {
		log.info("Retrieving the V_CLIENTDETAILS data list with PolNum from BeneficiaryDetails = " + policyNumber);
		List<ClientDetails> clientDetails_li2 = this.odsDao.getClientDetailsli_2(policyNumber);
		log.info("Successfully retrieving the V_CLIENTDETAILS list with size = " + clientDetails_li2.size());
		log.info("Looping the clientDetails_li2 to fill the Beneficiary Clients Data");
		if (clientDetails_li2 != null && clientDetails_li2.size() > 0) {
			for (ClientDetails clientDetails : clientDetails_li2) {
				ClaimWorksheetUIDataClaimWorksheetClientsOwnerData benClient = new ClaimWorksheetUIDataClaimWorksheetClientsOwnerData();
				benClient.setStrCLNTLifeName(clientDetails.getLIFENAME());
				benClient.setStrCLNTLifeNumber(
						this.odsDao.getLifefromComponentDetails(policyNumber, clientDetails.getCLIENTNO()));
				benClient.setStrCLNTClientNumber(clientDetails.getCLIENTNO());
				benClient.setDtCLNTClientDOB(Util.dateToBigDecimal(clientDetails.getDOB()));
				benClient.setStrCLNTGender(clientDetails.getSEX());
				benClient.setStrCLNTAddress1(clientDetails.getAddress1());
				benClient.setStrCLNTAddress2(clientDetails.getAddress2());
				benClient.setStrCLNTAddress3(clientDetails.getAddress3());
				benClient.setStrCLNTAddress4(clientDetails.getAddress4());
				benClient.setStrCLNTAddress5(clientDetails.getAddress5());
				benClient.setStrCLNTOccupationCode(clientDetails.getOccupation_code());
				benClient.setStrCLNTIDType(clientDetails.getIDType());
				benClient.setStrCLNTIDNumber(clientDetails.getIDNumber());
				benClient.setStrCLNTPhoneNumber1(clientDetails.getPHONENO01());
				benClient.setStrCLNTPhoneNumber2(clientDetails.getPHONENO02());
				benClient.setStrCLNTPhoneNumber3(clientDetails.getPHONENO03());
				benClient.setStrCLNTNationality(clientDetails.getNationality());
				benClient.setStrCLNTClientStatus(clientDetails.getClientStatus());
				benClient.setStrCLNTRelationship(
						this.odsDao.getRelationshipFromBenDetails(policyNumber, clientDetails.getCLIENTNO()));
				benClient.setStrCLNTBeneficiaryPercentage(
						this.odsDao.getPercentageFromBenDetails(policyNumber, clientDetails.getCLIENTNO()));
				benClient.setStrCLNTAbuseFlag(clientDetails.getABUSEFLAG());
				benClient.setStrCLNTFatcaFlag(clientDetails.getFATCAFLAG());
				respClientsBenData_li.add(benClient);
			}
		}
		log.info("Successfully finished looping the Beneficiary clientDetails_li2");
	}

	private void fetchClientDetailsLifeInsured(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetClientsOwnerData> respClientsLifeInsured_li) throws DAOException {
		log.info("Retrieving the V_CLIENTDETAILS data list with PolNum from PolicyRelated = " + policyNumber);
		List<ClientDetails> clientDetails_li3 = this.odsDao.getClientDetailsli_3(policyNumber);
		log.info("Successfully retrieving the V_CLIENTDETAILS list with size = " + clientDetails_li3.size());
		log.info("Looping the clientDetails_li3 to fill the LifeInsured Data List");
		if (clientDetails_li3 != null && clientDetails_li3.size() > 0) {
			for (ClientDetails clientDetails : clientDetails_li3) {
				ClaimWorksheetUIDataClaimWorksheetClientsOwnerData lifeInsuredData = new ClaimWorksheetUIDataClaimWorksheetClientsOwnerData();
				lifeInsuredData.setStrCLNTLifeName(clientDetails.getLIFENAME());
				lifeInsuredData.setStrCLNTLifeNumber(
						this.odsDao.getLifefromComponentDetails(policyNumber, clientDetails.getCLIENTNO()));
				lifeInsuredData.setStrCLNTClientNumber(clientDetails.getCLIENTNO());
				lifeInsuredData.setDtCLNTClientDOB(Util.dateToBigDecimal(clientDetails.getDOB()));
				lifeInsuredData.setStrCLNTGender(clientDetails.getSEX());
				lifeInsuredData.setStrCLNTAddress1(clientDetails.getAddress1());
				lifeInsuredData.setStrCLNTAddress2(clientDetails.getAddress2());
				lifeInsuredData.setStrCLNTAddress3(clientDetails.getAddress3());
				lifeInsuredData.setStrCLNTAddress4(clientDetails.getAddress4());
				lifeInsuredData.setStrCLNTAddress5(clientDetails.getAddress5());
				lifeInsuredData.setStrCLNTOccupationCode(clientDetails.getOccupation_code());
				lifeInsuredData.setStrCLNTIDType(clientDetails.getIDType());
				lifeInsuredData.setStrCLNTIDNumber(clientDetails.getIDNumber());
				lifeInsuredData.setStrCLNTPhoneNumber1(clientDetails.getPHONENO01());
				lifeInsuredData.setStrCLNTPhoneNumber2(clientDetails.getPHONENO02());
				lifeInsuredData.setStrCLNTPhoneNumber3(clientDetails.getPHONENO03());
				lifeInsuredData.setStrCLNTNationality(clientDetails.getNationality());
				lifeInsuredData.setStrCLNTClientStatus(clientDetails.getClientStatus());
				lifeInsuredData.setStrCLNTRelationship(
						this.odsDao.getRelationshipFromBenDetails(policyNumber, clientDetails.getCLIENTNO()));
				lifeInsuredData.setStrCLNTBeneficiaryPercentage(
						this.odsDao.getPercentageFromBenDetails(policyNumber, clientDetails.getCLIENTNO()));
				lifeInsuredData.setStrCLNTAbuseFlag(clientDetails.getABUSEFLAG());
				lifeInsuredData.setStrCLNTFatcaFlag(clientDetails.getFATCAFLAG());

				respClientsLifeInsured_li.add(lifeInsuredData);
			}
		}
		log.info("Successfully finished looping the LifeInsured clientDetails_li3");
	}

	private void fetchClaimHistory(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetClaimHistory> respClaimHistory) throws DAOException {
		log.info("Retrieving the V_CLAIMHISTORY data list with PolNum = " + policyNumber);
		List<ClaimHistoryModel> claimHist_li = this.odsDao.getClaimHistoryData(policyNumber);
		log.info("Successfully retrieving the V_CLAIMHISTORY list with size = " + claimHist_li.size());
		log.info("Looping the claimHist_li to fill the ClaimHistory Data List");
		if (claimHist_li != null && claimHist_li.size() > 0) {
			for (ClaimHistoryModel claimHistoryModel : claimHist_li) {
				ClaimWorksheetUIDataClaimWorksheetClaimHistory claimHistOut = new ClaimWorksheetUIDataClaimWorksheetClaimHistory();
				claimHistOut.setStrCHLifeAsiaClaimNumber(claimHistoryModel.getLIFEASIACLIAMNUMBER());
				claimHistOut.setStrCHClaimNumber(claimHistoryModel.getPENCILCLAIMNUMBER());
				claimHistOut.setStrCHPolicynumber(claimHistoryModel.getPOLICYNUMBER());
				claimHistOut.setDblCHTotalClaimAmount(claimHistoryModel.getTOTALCLAIMAMOUNT());
				claimHistOut.setStrCHClientNumber(claimHistoryModel.getCLIENTNUMBER());
				claimHistOut.setDtCHApprovalDate(Util.dateToBigDecimal(claimHistoryModel.getAPPROVALDATE()));
				claimHistOut.setStrCHComponentCode(claimHistoryModel.getCOMPONENTCODE());

				respClaimHistory.add(claimHistOut);
			}
		}
		log.info("Successfully looping the claimHist_li");
	}

	private void fetchNBUWComments(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsNBUW_li) throws DAOException {
		log.info("Retrieving the UWComments data list with PolNum = " + policyNumber
				+ " and the Sourceof COmments = NB");
		List<UWCommentsModel> nbComments_li = this.claimDetailsDao.getUWComments(policyNumber, "NB");
		log.info("Successfully retrieving the UWCOMMENTS list with size = " + nbComments_li.size());
		log.info("Looping the nbComments_li to fill the NBUWCOMMENTS Data List");
		if (nbComments_li != null && nbComments_li.size() > 0) {
			for (UWCommentsModel uwCommentsModel : nbComments_li) {
				ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments nbComment = new ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments();
				nbComment.setStrComment(uwCommentsModel.getUWComment());
				nbComment.setStrCommentBY(uwCommentsModel.getUWCommentBy());
				nbComment.setDtCommentDate(Util.dateToBigDecimal(uwCommentsModel.getUWCommentTS()));
				nbComment.setStrExistingRecord("Y");
				respCommentsNBUW_li.add(nbComment);
			}
		}
		log.info("Successfully looping the nbComments_li");
	}

	private void fetchPSUWComments(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsPSUW_li) throws DAOException {
		log.info("Retrieving the UWComments data list with PolNum = " + policyNumber
				+ " and the Sourceof COmments = PS");
		List<UWCommentsModel> psComments_li = this.claimDetailsDao.getUWComments(policyNumber, "PS");
		log.info("Successfully retrieving the UWCOMMENTS list with size = " + psComments_li.size());
		log.info("Looping the psComments_li to fill the PSUWCOMMENTS Data List");
		if (psComments_li != null && psComments_li.size() > 0) {
			for (UWCommentsModel uwCommentsModel : psComments_li) {
				ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments psComment = new ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments();
				psComment.setStrComment(uwCommentsModel.getUWComment());
				psComment.setStrCommentBY(uwCommentsModel.getUWCommentBy());
				psComment.setDtCommentDate(Util.dateToBigDecimal(uwCommentsModel.getUWCommentTS()));
				psComment.setStrExistingRecord("Y");
				respCommentsPSUW_li.add(psComment);
			}
		}
		log.info("Successfully looping the psComments_li");
	}

	private void fetchCLUWComments(String policyNumber,
			List<ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments> respCommentsCLUW_li) throws DAOException {
		log.info("Retrieving the UWComments data list with PolNum = " + policyNumber
				+ " and the Sourceof COmments = CL");
		List<UWCommentsModel> clComments_li = this.claimDetailsDao.getUWComments(policyNumber, "CL");
		log.info("Successfully retrieving the UWCOMMENTS list with size = " + clComments_li.size());
		log.info("Looping the clComments_li to fill the CLUWCOMMENTS Data List");
		if (clComments_li != null && clComments_li.size() > 0) {
			for (UWCommentsModel uwCommentsModel : clComments_li) {
				ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments clComment = new ClaimWorksheetUIDataClaimWorksheetCommentsNBUWComments();
				clComment.setStrComment(uwCommentsModel.getUWComment());
				clComment.setStrCommentBY(uwCommentsModel.getUWCommentBy());
				clComment.setDtCommentDate(Util.dateToBigDecimal(uwCommentsModel.getUWCommentTS()));
				clComment.setStrExistingRecord("Y");
				respCommentsCLUW_li.add(clComment);
			}
		}
		log.info("Successfully looping the clComments_li");
	}

}
