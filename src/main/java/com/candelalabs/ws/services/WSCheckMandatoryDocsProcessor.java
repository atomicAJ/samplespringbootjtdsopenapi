/**
 * 
 */
package com.candelalabs.ws.services;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSCheckMandatoryDocsDataIn;
import com.candelalabs.api.model.WSCheckMandatoryDocsDataOut;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSCheckMandatoryDocsProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSCheckMandatoryDocsProcessor");

	@Autowired
	private ClaimDetailsDao claimDetailsDao;

	/**
	 * 
	 */
	public WSCheckMandatoryDocsProcessor() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		int countMandatory = 0;
		int countRequirement = 0;
		WSCheckMandatoryDocsDataOut response = new WSCheckMandatoryDocsDataOut();
		log.info("WSCheckMandatoryDocsProcessor - Service is invoked"); 
		try {
			WSCheckMandatoryDocsDataIn dataIn = exchange.getIn().getBody(WSCheckMandatoryDocsDataIn.class);
			log.debug("WSCheckMandatoryDocsProcessor - Json Object In= " + dataIn.toString());
			if (dataIn.getCLAIMNUMBER() == null || dataIn.getCLAIMNUMBER().trim() == "") {
				log.info("WSCheckMandatoryDocsProcessor - Input ClaimNumber is empty");
				throw new Exception("Input ClaimNumber is empty");
			}

			ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getCLAIMNUMBER().trim());
			if (claimData.getClaimTypeUI() == null || claimData.getClaimTypeUI().trim() == "") {
				log.info("WSCheckMandatoryDocsProcessor - Not found ClaimTypeUI in DB with claimNumber = " + dataIn.getCLAIMNUMBER());
				throw new DAOException("Not found ClaimTypeUI in DB with claimNumber = " + dataIn.getCLAIMNUMBER());
			}

			log.info("WSCheckMandatoryDocsProcessor -Fetching Count MandatoryRequirement Table");
			countMandatory = this.claimDetailsDao
					.getCountMandatoryRequirementTableByClaimTypeUI(claimData.getClaimTypeUI(), "Y");
			log.info("WSCheckMandatoryDocsProcessor - Count MandatoryRequirement Table= " + countMandatory);
			log.info("WSCheckMandatoryDocsProcessor -Fetching Count RequirementDetail Table");
			countRequirement = this.claimDetailsDao.getCountRequiremntDetailTable(claimData.getClaimTypeUI(),
					dataIn.getCLAIMNUMBER(), "Y");
			log.info("WSCheckMandatoryDocsProcessor -Count RequirementDetail Table= " + countRequirement);
			if (countMandatory > 0 || countRequirement > 0) {
				if (countMandatory == countRequirement) {
					response.setIsMatch(true);
				} else {
					response.setIsMatch(false);
				}
			} else {
				throw new CamelException("Mandatory Tables && Requirement Detail table are empty");
			}
			response.setWsProcessingStatus("1");
			response.setWsSuccessMessage(
					"Count MandatoryRequirement= " + countMandatory + ", Count RequirementDetail= " + countRequirement);
			// log.info("Creating ldapUt");
			// LDAPUtil ldapUt = new LDAPUtil(log);
			// Person p = ldapUt.getADPerson("idn_full_user1");
			// log.info("Got the personObject");
			// response.setIsMatch(true);
			// response.setMessage("Message "+p.getDisplayName()+" "+p.getCn());

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error While Processsing= " + e.getMessage());
		}
		log.info("Final Response " + response.toString());

		exchange.getOut().setBody(response);

	}

}
