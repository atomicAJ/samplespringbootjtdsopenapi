/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetFactoringHouseListWSInput;
import com.candelalabs.api.model.DataOutGetFactoringHouseListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetFactoringHouseListProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetDiagNameListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("************************************************************************************* \n"
				+ "Service is Invoked");
		DataOutGetFactoringHouseListWSResponse wsresponse = new DataOutGetFactoringHouseListWSResponse();
		try {
			DataInGetFactoringHouseListWSInput dataIn = exchange.getIn()
					.getBody(DataInGetFactoringHouseListWSInput.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
			if (dataIn.getStrCurrencyCode().trim() != "" && dataIn.getStrCurrencyCode() != null
					&& dataIn.getStrPolicyNumber().trim() != "" && dataIn.getStrPolicyNumber() != null) {
				List<String> factHouse_li = this.oDSDao.getFactoringHouseList(dataIn.getStrCurrencyCode(),
						dataIn.getStrPolicyNumber());

				if (factHouse_li != null && factHouse_li.size() > 0) {
					wsresponse.setStrFactoringHouseList(factHouse_li);
					wsresponse.setWsProcessingStatus("1");
					wsresponse.setWsSuccessMessage("Successfully retrieved the Factoring House List");

				} else {
					throw new DAOException("Factoring House is not available with curCode = "
							+ dataIn.getStrCurrencyCode() + ", and polNum = " + dataIn.getStrPolicyNumber());
				}

			} else {
				throw new CamelException("Request Input Json is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR = " + e.getMessage());
		}
		log.info("JSON RESPONSE PAYLOAD: " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);

	}

}
