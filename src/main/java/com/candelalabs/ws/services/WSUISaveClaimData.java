/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ClaimDataUIClaimDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsAdditionalRequirement;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsBenefitfundDetails;
import com.candelalabs.api.model.ClaimDataUIClaimDetailsWSResponse;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSRegisterRequirementDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.BenefitNotPaidAmount;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ODSRequirementMasterModel;
import com.candelalabs.ws.model.tables.RequirementDetailsModel;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSUISaveClaimData implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;

	@Autowired
	ProducerTemplate producerTemplate;

	private static final String dateFormat = "dd/MM/yyyy";

	private static Log log = LogFactory.getLog("WSUISaveClaimData");

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		// log.info("************************************************************************************************");
		ClaimDataUIClaimDetails dataOut = new ClaimDataUIClaimDetails();
		ClaimDataUIClaimDetailsWSResponse wsresponse = new ClaimDataUIClaimDetailsWSResponse();
		List<ClaimDataUIClaimDetailsAdditionalRequirement> output_additionalReq_li = new ArrayList<ClaimDataUIClaimDetailsAdditionalRequirement>();
		List<ClaimDataUIClaimDetailsBenefitfundDetails> output_benefitFundDetails_li = new ArrayList<ClaimDataUIClaimDetailsBenefitfundDetails>();
		try {
			ClaimDataUIClaimDetails dataIn = exchange.getIn().getBody(ClaimDataUIClaimDetails.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
			if (!Optional.ofNullable(dataIn).isPresent()) {
				throw new CamelException("Request Input Object is empty");
			}
		
			String claimTypeUI = this.claimDetailsDao.getClaimTypeUI(dataIn.getClaimNumber().trim());

			List<ClaimDataUIClaimDetailsAdditionalRequirement> additionalReq_li = dataIn.getAdditionalRequirement();
			List<ClaimDataUIClaimDetailsBenefitfundDetails> benefitFundDetails_li = dataIn.getBenefitfundDetails();
			ClaimDataNewModel claimData = new ClaimDataNewModel();
			claimData.setClaimTypeUI(claimTypeUI);
			claimData.setClaimNumber(dataIn.getClaimNumber());
			claimData.setPolicyNumber(dataIn.getClaimPolicyNumber());
			claimData.setProductCode(dataIn.getClaimProductName());
			claimData.setClaimTypeLA(dataIn.getClaimTypeLA());
			claimData.setPlanCode(dataIn.getClaimPlanCode());
			claimData.setReceivedDate(Util.objToDate(dataIn.getClaimReceivedDate(), dateFormat));
			claimData.setRegisterDate(Util.objToDate(dataIn.getClaimRegisterDate(), dateFormat));
			claimData.setInsuredClientName(dataIn.getClaimInsuredName());
			claimData.setDiagnosisCode(dataIn.getClaimDiagnoseCode());
			claimData.setDiagnosisName(dataIn.getClaimDiagnoseName());
			claimData.setAdmissionDate(Util.objToDate(dataIn.getClaimAdmissionDate(), dateFormat));
			claimData.setDischargeDate(Util.objToDate(dataIn.getClaimDischargeDate(), dateFormat));
			claimData.setProviderName(dataIn.getClaimProvider());
			claimData.setProviderNameSource(dataIn.getClaimProviderSource());
			claimData.setDoctorName(dataIn.getClaimDoctorName());
			claimData.setClaimInvoiceAmount(dataIn.getClaimInvoiceAmount());
			claimData.setClaimInvoiceAmountUI(dataIn.getClaimInvoiceAmountUI());
			claimData.setClaimCurrency(dataIn.getClaimSourcecurrency());
			claimData.setBenefitCategory(dataIn.getClaimDeathBenefitCategory());
			claimData.setPayeeName(dataIn.getClaimPayeeName());
			claimData.setFactoringHouse(dataIn.getClaimFactoringHouse());
			claimData.setBankCode(dataIn.getClaimBankCode());
			claimData.setBankAccountName(dataIn.getClaimBankAccountName());
			claimData.setBankAccountNumber(dataIn.getClaimBankAccountNumber());
			claimData.setBankType(dataIn.getClaimBankType());
			claimData.setClaimSumAssuredPercentage(dataIn.getClaimSumAssuredPercent());
			claimData.setComponentCode(dataIn.getClaimComponentName());
			claimData.setComponentSumAssured(dataIn.getClaimComponentSumAssured());
			claimData.setOSCharges(dataIn.getClaimOSCharges());
			claimData.setOtherAdjustments(dataIn.getClaimOtherAdjustment());
			claimData.setAdjustmentCode(dataIn.getClaimAdjustmentCode());
			claimData.setPolicyLoanAmount(dataIn.getClaimPolicyLoan());
			claimData.setPolicyDebtAmount(dataIn.getClaimPolicyDebt());
			claimData.setFullyClaimed(dataIn.getClaimFullyClaim());
			claimData.setClaimApprovedAmountUI(dataIn.getClaimUITotalAmount());
			claimData.setCurrentFrom(Util.objToDate(dataIn.getClaimCurrentFrom(), dateFormat));
			claimData.setStayDuration(dataIn.getClaimStayDuration());
			if (dataIn.getAutosaveflag().trim().equalsIgnoreCase("N")) {
				claimData.setUISaveFlag("Y");
			} else {
				claimData.setUISaveFlag("N");
			}

			log.info("Get claimDataDetails table with claimNo= " + dataIn.getClaimNumber());
			List<ClaimDataDetailsModel> claimDataDetails_li = new ArrayList<ClaimDataDetailsModel>();
			claimDataDetails_li = this.claimDetailsDao.getClaimDataDetailsByClaimNo(dataIn.getClaimNumber());
			log.info("Successfully retrieved claimDataDetails table with size = " + claimDataDetails_li.size());
			log.info("List benefitDetails from input is = " + benefitFundDetails_li.size());

//			if (claimTypeUI.equalsIgnoreCase("HSR") || claimTypeUI.equalsIgnoreCase("HC")) {
//				BigDecimal stayDur = new BigDecimal(0);
//				if (benefitFundDetails_li != null && benefitFundDetails_li.size() > 0) {
//					for (ClaimDataUIClaimDetailsBenefitfundDetails benDet : benefitFundDetails_li) {
//						stayDur = stayDur.add(benDet.getBenefitNoOfDays());
//					}
//					claimData.setStayDuration(stayDur);
//				}
//			}

			log.info("Get REQUIREMENTDETAILS table with claimNo= " + dataIn.getClaimNumber());
			List<RequirementDetailsModel> reqDetails_li = new ArrayList<RequirementDetailsModel>();
			reqDetails_li = this.claimDetailsDao.getRequirementDetailsList(dataIn.getClaimNumber());
			log.info("Successfully retrieved REQUIREMENTDETAILS table with size = " + reqDetails_li.size());

			log.info("Setting the inputData for WSRegisterRequirement");
			WSRegisterRequirementDataIn dataRegister = new WSRegisterRequirementDataIn();
			String reqText_li = "";
			String subCat_li = "";
			int iLoop = 0;

			for (iLoop = 0; iLoop < additionalReq_li.size(); iLoop++) {
				ClaimDataUIClaimDetailsAdditionalRequirement addReq = additionalReq_li.get(iLoop);
				reqText_li = reqText_li + addReq.getAdditionaltext();
				subCat_li = subCat_li + addReq.getSubCategory();

				if (iLoop < additionalReq_li.size() - 1) {
					reqText_li = reqText_li + "~#@ ";
					subCat_li = subCat_li + "~#@";
				}

			}

			dataRegister.setCLAIMNUMBER(dataIn.getClaimNumber());
			dataRegister.setAPPNUMBER("");
			dataRegister.setPOLICYNUMBER(dataIn.getClaimPolicyNumber());
			dataRegister.setTPACLAIMNUMBER("");
			dataRegister.setBATCHNUMBER("");
			dataRegister.setPORTALREQUESTNUMBER("");
			dataRegister.setPOSREQUESTNUMBER("");
			dataRegister.setREQTSUBCATEGORY(subCat_li);
			dataRegister.setREQDOCID("");
			dataRegister.setREQTTEXT(reqText_li);
			dataRegister.setREQTREQTS(OffsetDateTime.now());
			dataRegister.setREQTNOTIFYFLAG("11");
			dataRegister.setCUSTOMERINITIATEDREQ("");

			log.info("Invoking the RegisterRequirement Integration Service with payload = " + dataRegister.toString());
			JsonResponse responseReg = producerTemplate.requestBody("direct:WSRegisterRequirementRoute", dataRegister,
					JsonResponse.class);
			if (responseReg.getWsProcessingStatus().equalsIgnoreCase("2")) {
				throw new CamelException("Invoking WSRegisterRequirement is failing, with reason = "
						+ responseReg.getWsExceptionMessage());
			}
			;
			log.info("Successfully Invoked the RegisterRequirement Integration Service");

			log.info("Getting the listlifename");
			String listLifename = this.odsDao.getListLifeNameString(dataIn.getClaimPolicyNumber().trim());
			log.info("Got the listlifename = " + listLifename);
			log.info("Getting the docIdList");
			String docIdList = this.claimDetailsDao.getDocIDList(dataIn.getClaimNumber());
			log.info("Got the docIdList = " + docIdList);
			log.info("Getting the listSubCategoty");
			String listSubCategory = this.odsDao.getListSubcategory(docIdList);
			log.info("Got the listSubCategory = " + listSubCategory);
//			log.info("Getting the benCodeList");
//			String listBenefitName = this.claimDetailsDao.getBenefitIDList(dataIn.getClaimNumber());
//			log.info("Got the benCodeList = " + listBenefitName);

			log.info("Getting the compRiskDate");
			java.sql.Date compRiskDate = this.odsDao.getCompRiskCesDate(dataIn.getClaimPolicyNumber(),
					dataIn.getClaimComponentCode(), dataIn.getClaimInsuredClientID());
			log.info("Got the compRiskDate = " + compRiskDate);
			
//			log.info("Getting the benNotPaidAmtList");
//			List<BenefitNotPaidAmount> benNotPaidAmt_li = new ArrayList<BenefitNotPaidAmount>();
//			benNotPaidAmt_li = this.claimDetailsDao.getBenefitNotPaidAmountList(dataIn.getClaimNumber());
//			String strbenNotPaidAmt_li = Util.listToString(benNotPaidAmt_li, ";");
//			log.info("Got the benNotPaidAmtList = " + strbenNotPaidAmt_li);

			log.info("SAVING THE CLAIMDATA UI");
			log.info("Running Save");

			if (claimData.getProviderName() != null && !claimData.getProviderName().trim().equalsIgnoreCase("")) {
				log.info("Getting the ProviderCode");
				String providerCode = this.odsDao.getProviderCode(claimData.getProviderName());
				log.info("Succesfully Getting the ProviderCode = " + providerCode);
				claimData.setProviderCode(providerCode);
			}

			this.claimDetailsDao.saveClaimDataUI(dataIn.getClaimNumber(), benefitFundDetails_li, claimDataDetails_li,
					claimData, dataIn, claimTypeUI, listLifename, listSubCategory, "", compRiskDate, log,
					null);
			log.info("Successfully Running Save");

			// GET THE NEW UPDATED DATA AND RETURN TO THE UI
			ClaimDataNewModel claimDataResult = this.claimDetailsDao.getClaimDataNew(dataIn.getClaimNumber());
			// SAVE NOTIFICATION DATA

			log.info("SUCCESSFULLY SAVING THE CLAIMDATA UI");

			if (!Optional.ofNullable(claimData).isPresent()) {
				throw new DAOException("There is no record in CLAIMDATA for claimNumber = " + dataIn.getClaimNumber());
			}

			log.info("Get the new updated Data and set the Output");
			dataOut.setClaimNumber(dataIn.getClaimNumber());
			dataOut.setClaimPolicyNumber(claimDataResult.getPolicyNumber());
			dataOut.setClaimProductName(claimDataResult.getProductCode());
			dataOut.setClaimTypeLA(claimDataResult.getClaimTypeLA());
			dataOut.setClaimPlanCode(claimDataResult.getPlanCode());
			dataOut.setClaimReceivedDate(Util.dateToBigDecimal(claimDataResult.getReceivedDate()));
			dataOut.setClaimRegisterDate(Util.dateToBigDecimal(claimDataResult.getRegisterDate()));
			dataOut.setClaimInsuredName(claimDataResult.getInsuredClientName());
			dataOut.setClaimInsuredClientID(claimData.getInsuredClientID());
			dataOut.setClaimDiagnoseCode(claimDataResult.getDiagnosisCode());
			dataOut.setClaimDiagnoseName(claimDataResult.getDiagnosisName());
			dataOut.setClaimAdmissionDate(Util.dateToBigDecimal(claimDataResult.getAdmissionDate()));
			dataOut.setClaimDischargeDate(Util.dateToBigDecimal(claimDataResult.getDischargeDate()));
			dataOut.setClaimProvider(claimDataResult.getProviderName());
			dataOut.setClaimProviderSource(claimDataResult.getProviderNameSource());
			dataOut.setClaimDoctorName(claimDataResult.getDoctorName());
			dataOut.setClaimInvoiceAmount(claimDataResult.getClaimInvoiceAmount());
			dataOut.setClaimInvoiceAmountUI(claimDataResult.getClaimInvoiceAmountUI());
			dataOut.setClaimPolicyCurrency(dataIn.getClaimPolicyCurrency());
			dataOut.setClaimSourcecurrency(claimDataResult.getClaimCurrency());
			dataOut.setClaimDeathBenefitCategory(claimDataResult.getBenefitCategory());
			dataOut.setClaimPayeeName(claimDataResult.getPayeeName());
			dataOut.setClaimFactoringHouse(claimDataResult.getFactoringHouse());
			dataOut.setClaimBankCode(claimDataResult.getBankCode());
			dataOut.setClaimBankAccountName(claimDataResult.getBankAccountName());
			dataOut.setClaimBankAccountNumber(claimDataResult.getBankAccountNumber());
			dataOut.setClaimBankType(claimDataResult.getBankType());
			dataOut.setClaimSumAssuredPercent(claimDataResult.getClaimSumAssuredPercentage());
			dataOut.setClaimComponentCode(claimData.getComponentCode());
			dataOut.setClaimComponentName(claimDataResult.getComponentCode());
			dataOut.setClaimComponentSumAssured(claimDataResult.getComponentSumAssured());
			dataOut.setClaimOSCharges(claimDataResult.getOSCharges());
			dataOut.setClaimOtherAdjustment(claimDataResult.getOtherAdjustments());
			dataOut.setClaimAdjustmentCode(claimDataResult.getAdjustmentCode());
			dataOut.setClaimPolicyLoan(claimDataResult.getPolicyLoanAmount());
			dataOut.setClaimPolicyDebt(claimDataResult.getPolicyDebtAmount());
			dataOut.setClaimFullyClaim(claimData.getFullyClaimed());
			dataOut.setClaimUITotalAmount(claimDataResult.getClaimApprovedAmountUI());
			dataOut.setClaimCurrentFrom(Util.dateToBigDecimal(claimDataResult.getCurrentFrom()));
			dataOut.setAutosavetime(dataIn.getAutosavetime());
			dataOut.setClaimStayDuration(claimDataResult.getStayDuration());

			log.info("Get claimDataDetails table with claimNo= " + dataIn.getClaimNumber());
			// List<ClaimDataDetailsModel> claimDataDetails_li_result = new
			// ArrayList<ClaimDataDetailsModel>();
			claimDataDetails_li = this.claimDetailsDao.getClaimDataDetailsByClaimNo(dataIn.getClaimNumber());
			log.info("Successfully retrieved claimDataDetails table with size = " + claimDataDetails_li.size());

			if (claimDataDetails_li != null && claimDataDetails_li.size() > 0) {
				for (ClaimDataDetailsModel claimDataDetailsModel : claimDataDetails_li) {
					ClaimDataUIClaimDetailsBenefitfundDetails benFUndModel = new ClaimDataUIClaimDetailsBenefitfundDetails();
					benFUndModel.setBenefitCode(claimDataDetailsModel.getBenefitCode());
					benFUndModel.setBenefitName(claimDataDetailsModel.getBenefitName());
					benFUndModel.setBenefitnoOfDaysClaimed(claimDataDetailsModel.getBenefitDaysClaimed() != null
							? claimDataDetailsModel.getBenefitDaysClaimed().intValue() : 0);
					benFUndModel.setBenefittotalAmountClaimedAsOfNow(claimDataDetailsModel.getBenefitAmountClaimed());
					benFUndModel
							.setBenefitTotalAvailableAmountForClaim(claimDataDetailsModel.getBenefitAmountApplicable());
					benFUndModel.setBenefitdailyIncurred(claimDataDetailsModel.getBenefitAmountPerDay());
					benFUndModel.setBenefitDateFrom(Util.dateToBigDecimal(claimDataDetailsModel.getBenefitFromDate()));
					benFUndModel.setBenefitDateTo(Util.dateToBigDecimal(claimDataDetailsModel.getBenefitToDate()));
					benFUndModel.setBenefitNoOfDays(claimDataDetailsModel.getBenefitActualDays());
					benFUndModel.setBenefitAmountIncurred(claimDataDetailsModel.getBenefitInvoiceAmount());
					// benFUndModel.setBenefitAmountApproveFromTPA(claimDataDetailsModel.getBenefitApprovedAmount());
					benFUndModel.setBenefitApprovedAmount(claimDataDetailsModel.getBenefitApprovedAmount());
					benFUndModel
							.setBenefitAactualPayableByClaimUser(claimDataDetailsModel.getBenefitUserApprovedAmount());
					benFUndModel.setBenefitPercentage(claimDataDetailsModel.getBenefitPercentage());
					benFUndModel.setBenefitAnnualTotalDays(claimDataDetailsModel.getBenefitAnnualTotalDays());
					benFUndModel.setBenefitNotApprovedAmount(claimDataDetailsModel.getBenefitNotApprovedAmount());
					benFUndModel.setBenefitMaxAmountPerDay(claimDataDetailsModel.getBenefitMaxAmountPerDay());
					benFUndModel.setBenefitTotalIncurredAmount(claimDataDetailsModel.getBenefitTotalIncurredAmount());
					benFUndModel.setBenefitNoOfUnits(claimDataDetailsModel.getBenefitNoOfUnits());
					benFUndModel.setFundComponentCode(claimDataDetailsModel.getFundComponentCode());
					benFUndModel.setFundComponentName(claimDataDetailsModel.getFundComponentName());
					benFUndModel.setFundCode(claimDataDetailsModel.getFundCode());
					benFUndModel.setFundDescription(claimDataDetailsModel.getFundDescription());
					benFUndModel.setFundEstimatedAmount(claimDataDetailsModel.getFundEstimatedAmount());
					benFUndModel.setFundActualAmount(claimDataDetailsModel.getFundActualAmount());

					output_benefitFundDetails_li.add(benFUndModel);
				}
			}

			log.info("Get REQUIREMENTDETAILS table with claimNo= " + dataIn.getClaimNumber());
			reqDetails_li = this.claimDetailsDao.getRequirementDetailsList(dataIn.getClaimNumber());
			log.info("Successfully retrieved REQUIREMENTDETAILS table with size = " + reqDetails_li.size());

			if (reqDetails_li != null && reqDetails_li.size() > 0) {
				for (RequirementDetailsModel requirementDetailsModel : reqDetails_li) {
					ODSRequirementMasterModel reqMasterData = new ODSRequirementMasterModel();
					reqMasterData = this.odsDao.getRequirementMaster(requirementDetailsModel.getDocID());

					ClaimDataUIClaimDetailsAdditionalRequirement addRequirement = new ClaimDataUIClaimDetailsAdditionalRequirement();
					addRequirement.setReqtDropDownText(reqMasterData.getDROPDOWNTEXT());
					addRequirement.setSubCategory(reqMasterData.getSUBCATEGORY());
					addRequirement.setAdditionaltext(requirementDetailsModel.getRequirementText());
					addRequirement.setNotificationsent(requirementDetailsModel.getRequirementNotificationSent());

					output_additionalReq_li.add(addRequirement);
				}
			}
			if(claimData.getClaimTypeUI().equalsIgnoreCase("WAIVER") || 
					(claimData.getClaimTypeUI().equalsIgnoreCase("WOP"))){
				// UPDATE CLAIMDATA SET ClaimPayDate='<value of claimPaymentDate parameter>' 
				//WHERE CLAIMNUMBER = '<value of strClaimNumber parameter>'
				java.sql.Date claimPaymentDat =null;
				if(dataIn.getClaimPaymentDate()!=null)
				{
					claimPaymentDat= Util.objToDate(dataIn.getClaimPaymentDate(), "yyyy-MM-dd hh:mm:ss");
					
				
				 }
				log.info("claimPaymentDat is :- "+ claimPaymentDat);
				int i = claimDetailsDao.updateClaimDataClaimPayDate( claimPaymentDat,  dataIn.getClaimNumber());
			}
			wsresponse.setWsProcessingStatus("1");
			wsresponse.setWsSuccessMessage("Successfully save the claimUI Data");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}

			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR = " + e);
		}

		dataOut.setWsResponse(wsresponse);
		dataOut.setAdditionalRequirement(output_additionalReq_li);
		dataOut.setBenefitfundDetails(output_benefitFundDetails_li);
		log.info("JSON RESPONSE PAYLOAD = " + dataOut.toString());
		exchange.getOut().setBody(dataOut);

	}

}
