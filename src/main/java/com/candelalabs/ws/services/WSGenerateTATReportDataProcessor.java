package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.activiti.engine.impl.util.json.JSONArray;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.ArgValuePairs;
import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.TATReportData;
import com.candelalabs.api.model.WSGenerateTATReportDataIn;
import com.candelalabs.api.model.WSGenerateTATReportDataOut;
import com.candelalabs.api.model.WSGetProcessCaseSheetCommentsDataIn;
import com.candelalabs.api.model.WSGetProcessCaseSheetCommentsDataOut;
import com.candelalabs.api.model.WSReportOutputXLSDataIn;
import com.candelalabs.api.model.WSReportOutputXLSDataInWSInput;
import com.candelalabs.api.model.WSReportOutputXLSDataOut;
import com.candelalabs.api.model.WSUpdateDataDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.model.ClaimDataTATReport;
import com.candelalabs.ws.model.XlsColValMap;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.PolicyDetails;

@Component
public class WSGenerateTATReportDataProcessor implements Processor {

    private static final Log log = LogFactory.getLog("WSGenerateTATReportDataProcessor");

    @Autowired
    ClaimDetailsDao claimDetailsDao;

    @Autowired
    ODSDao odsDao;

    @Autowired
    ProducerTemplate producerTemplate;

    public static XlsColValMap prepGenericClassObj(int colNum, String colValue) {
        XlsColValMap xlsColValMap = new XlsColValMap();
        xlsColValMap.setColNum(colNum);
        xlsColValMap.setColValue(colValue);
        return xlsColValMap;
    }

    public static Date max(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.after(d2)) ? d1 : d2;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void process(Exchange exchange) throws Exception {
        // TODO Auto-generated method stub
        log.info("WSGenerateTATReportDataProcessor - Service is invoked");
        WSGenerateTATReportDataOut response = new WSGenerateTATReportDataOut();

        List<ClaimDataTATReport> claimDataTATReportDataList = new ArrayList<ClaimDataTATReport>();
        List<TATReportData> TATReportDataList = new ArrayList<TATReportData>();
        PolicyDetails policyDetails = new PolicyDetails();
        ClientDetails clientDetails = new ClientDetails();

        try {
            WSGenerateTATReportDataIn dataIn = (exchange.getIn().getBody(WSGenerateTATReportDataIn.class));
            log.debug("----- dataIn -----");
            log.debug(dataIn.toString());

            String pattern1 = "yyyy-MM-dd hh:mm:ss";
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

            String pattern2 = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern2);

            WSUpdateDataDataIn updateDataIn = new WSUpdateDataDataIn();
            List<ArgValuePairs> argValuePairsList = new ArrayList<ArgValuePairs>();

            if (dataIn.getWsInput().getDtFromDate() != null && dataIn.getWsInput().getDtFromDate().trim() != "" &&
                    dataIn.getWsInput().getDtToDate() != null && dataIn.getWsInput().getDtToDate().trim() != "" &&
                    dataIn.getWsInput().getStrProductCode() != null && dataIn.getWsInput().getStrProductCode() != "") {
                if (dataIn.getWsInput().getStrReportType().equalsIgnoreCase("claims")) {
                    if (dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("xls")) {
                        claimDataTATReportDataList = claimDetailsDao.getTATReportClaimData(dataIn.getWsInput().getStrReportDateType(),
                                dataIn.getWsInput().getStrProductCode(),
                                dataIn.getWsInput().getDtFromDate(),
                                dataIn.getWsInput().getDtToDate());

                        if (claimDataTATReportDataList.size() == 0) {
                            String currDtString = simpleDateFormat1.format(new Date());

                            updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
                                    + "ReportGenerated=?,ReportEndTimeStamp=? WHERE ReportRequestID=?");

                            argValuePairsList.add(prepareArgValuePairsObj("0", "string", "No Records found for the search criteria for the reports"));
                            argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
                            argValuePairsList.add(prepareArgValuePairsObj("2", "string", "0"));
                            argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
                            argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportRequestID()));

                            updateDataIn.setArgValuePairs(argValuePairsList);

                            boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

                            if (updateGenRepStatus) {
                                response.setWsProcessingStatus("2");
                                response.setWsExceptionMessage("No Records found for the search criteria for the reports");
                            } else {
                                response.setWsProcessingStatus("2");
                                response.setWsExceptionMessage("No Records found for the search criteria for the reports and Exception response form WSUpdateData service");
                            }
                        } else {
                            for (ClaimDataTATReport claimDataTATReportData : claimDataTATReportDataList) {
                                TATReportData tatReportData = new TATReportData();

                                log.debug("----- current claimDataTATReportData record -----");
                                log.debug(claimDataTATReportData.toString());

                                policyDetails = odsDao.getPolicyDetailsForReport(claimDataTATReportData.getPolicyNumber());
                                String sumAssured = odsDao.getSumAssured(claimDataTATReportData.getPolicyNumber(), claimDataTATReportData.getInsuredClientId(), claimDataTATReportData.getComponentCode());
                                clientDetails = odsDao.getClientDetails(claimDataTATReportData.getInsuredClientId());

                                WSGetProcessCaseSheetCommentsDataIn jsonDataIn = new WSGetProcessCaseSheetCommentsDataIn();
                                jsonDataIn.setCaseReferenceKey(claimDataTATReportData.getClaimNumber());
                                jsonDataIn.setTagName(dataIn.getWsInput().getStrTagName());

                                log.info("Invoking the wSGetProcessCaseSheetComments Service with payload = " + jsonDataIn.toString());
                                WSGetProcessCaseSheetCommentsDataOut dataOutPCaseSheet = producerTemplate.requestBody("direct:WSGetProcessCaseSheetComments", jsonDataIn,
                                        WSGetProcessCaseSheetCommentsDataOut.class);
                                if (dataOutPCaseSheet.getWsProcessingStatus().equalsIgnoreCase("2")) {
                                    log.info("Exception invoking wSGetProcessCaseSheetComments service");
                                    throw new CamelException("Invoking WSRegisterRequirement is failing, with reason = "
                                            + dataOutPCaseSheet.getWsExceptionMessage());
                                }

                                if (policyDetails != null) {
                                    log.debug("----- policyDetails -----");
                                    log.debug(policyDetails.toString());
                                    String claimNumber = claimDataTATReportData.getClaimNumber();
                                    int reqDocPendCount = claimDetailsDao.isAllPendingDocReceived(claimNumber, claimDataTATReportData.getClaimTypeUi(), "");
                                    Date pendCompDateCol = claimDetailsDao.getPendDocReceiveDate(claimNumber, claimDataTATReportData.getClaimTypeUi());

                                    tatReportData.setPolicyNumber(claimDataTATReportData.getPolicyNumber());
                                    tatReportData.setClaimNumber(claimDataTATReportData.getClaimNumber());
                                    tatReportData.setClaimType(claimDataTATReportData.getClaimTypeUi());
                                    tatReportData.setProductName(policyDetails.getPRODUCTNAME());
                                    tatReportData.setPolicyStatus(policyDetails.getPOLICYSTATUS());
                                    tatReportData.setRcDDate(policyDetails.getRISKCOMMDATE().toString());

                                    log.debug("----- Calculate Age of Policy -----");
                                    if (policyDetails.getRISKCOMMDATE() != null && claimDataTATReportData.getDischargeDate() != null) {
                                        LocalDate rcdDate = policyDetails.getRISKCOMMDATE().toLocalDate();
                                        LocalDate dischargeDate = claimDataTATReportData.getDischargeDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                                        log.debug("rcdDate : " + rcdDate);
                                        log.debug("dischargeDate : " + dischargeDate);

                                        Period diff = Period.between(rcdDate, dischargeDate);
                                        tatReportData.setAgeOfPolicy(diff.getYears() + " Year," + diff.getMonths() + " Month," + diff.getDays() + " Days");
                                    } else {
                                        tatReportData.setAgeOfPolicy("");
                                    }

                                    tatReportData.setPolicyOwner(policyDetails.getOWNERNAME());
                                    tatReportData.setInsuredName(claimDataTATReportData.getInsuredClientName());
                                    tatReportData.setReceiveDate(claimDataTATReportData.getReceivedDate() == null ? "" : simpleDateFormat2.format(claimDataTATReportData.getReceivedDate()).toString());
                                    tatReportData.setRegisterDate(claimDataTATReportData.getRegisterDate() == null ? "" : simpleDateFormat2.format(claimDataTATReportData.getRegisterDate()).toString());
                                    tatReportData.setDateOfDeath(claimDataTATReportData.getDischargeDate() == null ? "" : simpleDateFormat2.format(claimDataTATReportData.getDischargeDate()).toString());
                                    tatReportData.setAdmissionDate(claimDataTATReportData.getAdmissionDate() == null ? "" : simpleDateFormat2.format(claimDataTATReportData.getAdmissionDate()).toString());
                                    tatReportData.setDischargeDate(claimDataTATReportData.getDischargeDate() == null ? "" : simpleDateFormat2.format(claimDataTATReportData.getDischargeDate()).toString());
                                    tatReportData.setLengthOfStay(claimDataTATReportData.getStayDuration() == null ? "" : claimDataTATReportData.getStayDuration().toString());
                                    tatReportData.setDiagnosis(claimDataTATReportData.getDiagnosisName());
                                    tatReportData.setProviderName(claimDataTATReportData.getProviderName());
                                    tatReportData.setPayeeName(claimDataTATReportData.getPayeeName());
                                    tatReportData.setSumAssured(sumAssured);
                                    tatReportData.setCurrency(policyDetails.getCURRENCY());
                                    tatReportData.setAmountIncurred(claimDataTATReportData.getClaimInvoiceAmount() == null ? "" : claimDataTATReportData.getClaimInvoiceAmount().toString());
                                    tatReportData.setAmountApproved(claimDataTATReportData.getClaimApprovedAmountUi() == null ? "" : claimDataTATReportData.getClaimApprovedAmountUi().toString());

                                    if (claimDataTATReportData.getClaimInvoiceAmount() != null && claimDataTATReportData.getClaimApprovedAmountUi() != null) {
                                        BigDecimal amountNotApproved = claimDataTATReportData.getClaimInvoiceAmount().subtract(claimDataTATReportData.getClaimApprovedAmountUi());
                                        tatReportData.setAmountNotApproved(amountNotApproved.toString());
                                    } else {
                                        tatReportData.setAmountNotApproved("");
                                    }

                                    int invDocNotUploadCount = claimDetailsDao.getDocNotUploadCount(claimDataTATReportData.getClaimNumber(), "Wait For Investigation Report");
                                    int reqInvDocNotUploadCount = claimDetailsDao.getDocNotUploadCount(claimDataTATReportData.getClaimNumber(), "Wait For Investigation Requirement Document");
                                    int reqDocNotUploadCount = claimDetailsDao.getDocNotUploadCount(claimDataTATReportData.getClaimNumber(), "Wait For Requested Documents");

                                    if (invDocNotUploadCount > 0 || reqInvDocNotUploadCount > 0) {
                                        tatReportData.setClaimStatus("Pending Investigation");
                                    } else if (reqDocNotUploadCount > 0) {
                                        tatReportData.setClaimStatus("Pending Document");
                                    } else if (claimDataTATReportData.getClaimDecision() != null && !claimDataTATReportData.getClaimDecision().equalsIgnoreCase("")) {
                                        tatReportData.setClaimStatus(claimDataTATReportData.getClaimDecision());
                                    } else {
                                        tatReportData.setClaimStatus(claimDataTATReportData.getClaimStatus());
                                    }

                                    if (reqDocPendCount > 0) {
                                        tatReportData.setPendingCompleteDate("");
                                    } else {
                                        tatReportData.setPendingCompleteDate(pendCompDateCol == null ? "" : simpleDateFormat2.format(pendCompDateCol).toString());
                                    }

                                    tatReportData.setClaimDecisionDate(claimDataTATReportData.getClaimDecisionDate() == null ? "" : simpleDateFormat2.format(claimDataTATReportData.getClaimDecisionDate()).toString());

                                    log.debug("***** TAT Clean and TAT All Process conputation start *****");

                                    int tatCleanDays;
                                    int tatAllDays;
                                    Date pendCompDate;
                                    int invstDocUploadCount;
                                    int pendDocCount;
                                    int pendDocCountWithMandDoc;
                                    int visitedInvestCount;
                                    int clmProcsInvCount;
                                    int clmProcsInvCountnEndTimeNotNull;
                                    boolean isConditionSuccess = false;

                                    int pendingDocCount = claimDetailsDao.getPendingDocCount(claimNumber);
                                    int pendInvestCount = claimDetailsDao.getVisitedActivityCount(claimNumber, "Claim Processor Investigation", "");
                                    log.debug("ClainNumber = " + claimNumber + " pendingDocCount = " + pendingDocCount + " "
                                            + " and pendInvestCount = " + pendInvestCount + "ClaimDecision = "
                                            + "" + claimDataTATReportData.getClaimDecision());
                                    if (claimDataTATReportData.getClaimStpFlag() == 1 && pendingDocCount == 0 && pendInvestCount == 0) {
                                        log.debug("Passed condition ClaimStpFlag = 1, pendingDocCount = 0, pendInvestCount = 0");
                                        if (claimDataTATReportData.getClaimDecisionDate() != null && claimDataTATReportData.getReceivedDate() != null) {
                                            log.debug("ReceivedDate = " + claimDataTATReportData.getReceivedDate() + "and ClaimDecisionDate = " + claimDataTATReportData.getClaimDecisionDate());
                                            int diffInDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                            tatReportData.setTatAllProcess(Integer.toString(diffInDays));
                                            tatReportData.setTatClean(Integer.toString(diffInDays));
                                        } else {
                                            log.debug("Either ClaimDecisionDate or ReceivedDate is null, hence TAT Clean and TAT All Process could not be computed");
                                            tatReportData.setTatClean("");
                                            tatReportData.setTatAllProcess("");
                                        }
                                    } else if (claimDataTATReportData.getClaimStpFlag() == 0 && claimDataTATReportData.getClaimDecision() != null) {
                                        log.debug("Passed condition ClaimStpFlag = 0, ClaimDecision = " + claimDataTATReportData.getClaimDecision());

                                        if (claimDataTATReportData.getClaimDecision().equalsIgnoreCase("Approve")) {
                                            log.debug("***** Approve case *****");
                                            invstDocUploadCount = claimDetailsDao.getDocUploadCount(claimNumber, "bnbCLMDOC-00017", "");
                                            pendDocCount = claimDetailsDao.isAllPendingDocReceived(claimNumber, claimDataTATReportData.getClaimTypeUi(), "");
                                            pendDocCountWithMandDoc = claimDetailsDao.isAllPendingDocReceived(claimNumber, "", "MANDATORYDOCS");
                                            visitedInvestCount = claimDetailsDao.getVisitedActivityCount(claimNumber, "Claim Processor Investigation", "ENDTIME");
                                            log.debug("invstDocUploadCount = " + invstDocUploadCount + ", pendDocCount = " + pendDocCount + ", pendDocCountWithMandDoc = " + pendDocCountWithMandDoc +
                                                    ", visitedInvestCount = " + visitedInvestCount);
                                            if (invstDocUploadCount == 0 && pendDocCount == 0 && visitedInvestCount > 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendCompDateReptTbl(claimNumber, "Claim Processor Investigation", "");
                                                if (pendCompDate != null) {
                                                    if (claimDataTATReportData.getClaimDecisionDate() != null && claimDataTATReportData.getReceivedDate() != null) {
                                                        tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), pendCompDate);
                                                        tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                        tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                        tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                    } else {
                                                        log.debug("Either ClaimDecisionDate or ReceivedDate is null, hence TAT Clean and TAT All Process could not be computed");
                                                        tatReportData.setTatAllProcess("");
                                                        tatReportData.setTatClean("");
                                                    }
                                                } else {
                                                    log.debug("TAT Clean could not be completed as the pendCompDate1 completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }
                                            } else if (invstDocUploadCount != 0 && pendDocCount == 0 && visitedInvestCount > 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendCompDateReptTbl(claimNumber, "Claim Processor Investigation", "");
                                                Date invsDocUploadDate = claimDetailsDao.getInvestDocUploadDate(claimNumber, "bnbCLMDOC-00017");

                                                if (pendCompDate != null && invsDocUploadDate != null) {
                                                    Date maxPendCompDate = max(pendCompDate, invsDocUploadDate);

                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), maxPendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));

                                                } else {
                                                    log.debug("1 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }
                                            } else if (pendDocCount == 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendDocReceiveDate(claimNumber, claimDataTATReportData.getClaimTypeUi());

                                                if (pendCompDate != null) {
                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), pendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                } else {
                                                    log.debug("2 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatAllDays));
                                                }
                                            } else if (pendDocCountWithMandDoc > 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendDocReceiveDate(claimNumber, "INCLUDEPENDDOCS");
                                                if (pendCompDate != null) {
                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), pendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                } else {
                                                    log.debug("3 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }

                                            } else {
                                                log.debug("TAT Clean and TAT All Process conputation did not pass any of the condition, hence not computed - 1");
                                                tatReportData.setTatAllProcess("");
                                                tatReportData.setTatClean("");
                                            }
                                        } else if (claimDataTATReportData.getClaimDecision().equalsIgnoreCase("Reject")) {
                                            log.debug("***** Reject case *****");
                                            int pendDocCountNotRaised = claimDetailsDao.isAllPendingDocReceived(claimNumber, claimDataTATReportData.getClaimTypeUi(), "NODOCSREQ");
                                            pendDocCount = claimDetailsDao.isAllPendingDocReceived(claimNumber, claimDataTATReportData.getClaimTypeUi(), "");
                                            visitedInvestCount = claimDetailsDao.getVisitedActivityCount(claimNumber, "Claim Processor Investigation", "");
                                            invstDocUploadCount = claimDetailsDao.getDocUploadCount(claimNumber, "bnbCLMDOC-00017", "");
                                            int visitedInvestCountEndTime = claimDetailsDao.getVisitedActivityCount(claimNumber, "Claim Processor Investigation", "ENDTIME");
                                            log.debug("pendDocCountNotRaised = " + pendDocCountNotRaised + ", pendDocCount = " + pendDocCount + ", visitedInvestCount = " + visitedInvestCount + ", invstDocUploadCount = " + invstDocUploadCount);
                                            if (pendDocCountNotRaised == 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;

                                                tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());
                                                tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                tatReportData.setTatClean(Integer.toString(tatCleanDays));

                                            } else if (pendDocCount == 0 && visitedInvestCount == 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendDocReceiveDate(claimNumber, claimDataTATReportData.getClaimTypeUi());

                                                if (pendCompDate != null) {
                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), pendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                } else {
                                                    log.debug("4 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatAllDays));
                                                }
                                            } else if (pendDocCount != 0 && visitedInvestCount == 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendDocReceiveDate(claimNumber, claimDataTATReportData.getClaimTypeUi());

                                                if (pendCompDate != null) {
                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), pendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                } else {
                                                    log.debug("5 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }
                                            } else if (invstDocUploadCount == 0 && pendDocCount == 0 && visitedInvestCountEndTime > 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendCompDateReptTbl(claimNumber, "Claim Processor Investigation", "");

                                                if (pendCompDate != null) {
                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), pendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                } else {
                                                    log.debug("6 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }
                                            } else if (invstDocUploadCount != 0 && pendDocCount == 0 && visitedInvestCountEndTime > 0 && !isConditionSuccess) {
                                                isConditionSuccess = true;
                                                pendCompDate = claimDetailsDao.getPendCompDateReptTbl(claimNumber, "Claim Processor Investigation", "");
                                                Date invsDocUploadDate = claimDetailsDao.getInvestDocUploadDate(claimNumber, "bnbCLMDOC-00017");

                                                if (pendCompDate != null && invsDocUploadDate != null) {
                                                    Date maxPendCompDate = max(pendCompDate, invsDocUploadDate);

                                                    tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), maxPendCompDate);
                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));

                                                } else {
                                                    log.debug("7 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }
                                            } else {
                                                log.debug("Reject case, user raised document request, hence TAT all process and TAT clean not computed");
                                            }
                                        } else if (claimDataTATReportData.getClaimDecision().equalsIgnoreCase("Auto Reject")) {
                                            log.debug("***** Auto Reject case *****");
                                            tatCleanDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getClaimDecisionDate());
                                            tatAllDays = getDiffDays(claimDataTATReportData.getClaimDecisionDate(), claimDataTATReportData.getReceivedDate());

                                            tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                            tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                        } else {
                                            log.debug("***** Claim Deciaion != APPROVE && != REJECT *****");
                                            invstDocUploadCount = claimDetailsDao.getDocUploadCount(claimNumber, "bnbCLMDOC-00017", "ALLPENDCASE1");
                                            pendDocCount = claimDetailsDao.isAllPendingDocReceived(claimNumber, claimDataTATReportData.getClaimTypeUi(), "");
                                            clmProcsInvCount = claimDetailsDao.getPendCompCountReptTbl(claimNumber, "Claim Processor Investigation", "");
                                            clmProcsInvCountnEndTimeNotNull = claimDetailsDao.getPendCompCountReptTbl(claimNumber, "Claim Processor Investigation", "END TIME NOT NULL");
                                            if (invstDocUploadCount == 0 && pendDocCount == 0 && clmProcsInvCount == 0 && clmProcsInvCountnEndTimeNotNull > 0) {
                                                Date pendCompDate5 = claimDetailsDao.getPendCompDateReptTbl(claimNumber, "Claim Processor Investigation", "START TIME");
                                                if (pendCompDate5 != null) {
                                                    tatCleanDays = getDiffDays(new Date(), pendCompDate5);
                                                    tatAllDays = getDiffDays(new Date(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                                } else {
                                                    log.debug("8 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                    log.debug("Conputing TatAllProcess only");

                                                    tatAllDays = getDiffDays(new Date(), claimDataTATReportData.getReceivedDate());

                                                    tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                    tatReportData.setTatClean("");
                                                }
                                            }
                                        }

                                    } else if (claimDataTATReportData.getClaimStpFlag() == 0 && claimDataTATReportData.getClaimDecision() == null) {
                                        log.debug("***** Claim Deciaion = NULL *****");
                                        invstDocUploadCount = claimDetailsDao.getDocUploadCount(claimNumber, "bnbCLMDOC-00017", "ALLPENDCASE1");
                                        pendDocCount = claimDetailsDao.isAllPendingDocReceived(claimNumber, claimDataTATReportData.getClaimTypeUi(), "");
                                        clmProcsInvCount = claimDetailsDao.getPendCompCountReptTbl(claimNumber, "Claim Processor Investigation", "");
                                        clmProcsInvCountnEndTimeNotNull = claimDetailsDao.getPendCompCountReptTbl(claimNumber, "Claim Processor Investigation", "END TIME NOT NULL");
                                        log.debug("invstDocUploadCount = " + invstDocUploadCount + ", pendDocCount = " + pendDocCount + ", clmProcsInvCount = " + clmProcsInvCount +
                                                ", clmProcsInvCountnEndTimeNotNull = " + clmProcsInvCountnEndTimeNotNull);
                                        if (invstDocUploadCount == 0 && pendDocCount == 0 && clmProcsInvCount == 0 && clmProcsInvCountnEndTimeNotNull > 0) {
                                            log.info("In condition Running case where nothing is pending : invstDocUploadCount == 0 && pendDocCount == 0 && clmProcsInvCount == 0 && clmProcsInvCountnEndTimeNotNull > 0");
                                            Date pendCompDate5 = claimDetailsDao.getPendCompDateReptTbl(claimNumber, "Claim Processor Investigation", "START TIME");
                                            if (pendCompDate5 != null) {
                                                tatCleanDays = getDiffDays(new Date(), pendCompDate5);
                                                tatAllDays = getDiffDays(new Date(), claimDataTATReportData.getReceivedDate());

                                                tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                tatReportData.setTatClean(Integer.toString(tatCleanDays));
                                            } else {
                                                log.debug("9 - TAT Clean could not be computed as the pendCompDate / invsDocUploadDate completion date is null");
                                                log.debug("Conputing TatAllProcess only");

                                                tatAllDays = getDiffDays(new Date(), claimDataTATReportData.getReceivedDate());

                                                tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                                tatReportData.setTatClean("");
                                            }
                                        }
                                        ///This condition added for Running case where nothing is pending-- no pending doc
                                        // no pending inves,no docs pending to be uploaded as fix for jira671
                                        else if (invstDocUploadCount == 0 && pendDocCount == 0 && clmProcsInvCount == 0 && clmProcsInvCountnEndTimeNotNull == 0) {
                                            log.info("In condition Running case where nothing is pending ");
                                            tatReportData.setTatClean(""); // to be confirmed with FWD
                                            tatAllDays = getDiffDays(new Date(), claimDataTATReportData.getReceivedDate());
                                            tatCleanDays = tatAllDays;
                                            log.info("In condition Running case where nothing is pending : tatAllDays -" + tatAllDays);
                                            tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
                                            long tatAllProcess = Long.parseLong(tatReportData.getTatAllProcess());
                                            Double tatAchieve = (double) tatAllProcess;
                                            tatAchieve = (tatAchieve / 10) * 100;
                                            tatReportData.setTatAchieve(tatAchieve.toString() + "%");
                                            tatReportData.setTatClean(String.valueOf(tatCleanDays));
                                            log.info("In condition Running case where nothing is pending : tatAchieve -" + tatAchieve.toString());
                                        } else if (invstDocUploadCount == 0 && pendDocCount == 0 && clmProcsInvCount > 0 && clmProcsInvCountnEndTimeNotNull == 0) {
											log.info("In condition Running case where nothing is pending ");
											tatReportData.setTatClean(""); // to be confirmed with FWD
											tatAllDays = getDiffDays(new Date(), claimDataTATReportData.getReceivedDate());
											tatCleanDays = getDiffDays(new Date(),claimDetailsDao.getPendCompDateReptTbl(claimNumber,"Claim Processor Investigation","START TIME"));
											log.info("In condition Running case where nothing is pending : tatAllDays -" + tatAllDays);
											tatReportData.setTatAllProcess(Integer.toString(tatAllDays));
											long tatAllProcess = Long.parseLong(tatReportData.getTatAllProcess());
											Double tatAchieve = (double) tatAllProcess;
											tatAchieve = (tatAchieve / 10) * 100;
											tatReportData.setTatAchieve(tatAchieve.toString() + "%");
											tatReportData.setTatClean(String.valueOf(tatCleanDays));
											log.info("In condition Running case where nothing is pending : tatAchieve -" + tatAchieve.toString());
										} else if (invstDocUploadCount == 0 && pendDocCount > 0 && clmProcsInvCount == 0 && clmProcsInvCountnEndTimeNotNull == 0) {
                                        	Date clPendingDate = claimDetailsDao.getPendDocReceiveDate(claimNumber,"PCR PENDDAYS_MAX");
                                        	tatReportData.setTatClean(String.valueOf(getDiffDays(new Date(),clPendingDate)));
                                        	tatReportData.setTatAllProcess(String.valueOf(getDiffDays(new Date(),claimDataTATReportData.getReceivedDate())));
										}
                                    } else {
                                        log.debug("TAT Clean and TAT All Process conputation did not pass any of the condition, hence not computed - 2");
                                        tatReportData.setTatAllProcess("");
                                        tatReportData.setTatClean("");
                                    }

                                    log.debug("***** TAT Clean and TAT All Process conputation end *****");

                                    tatReportData.setTatTarget("10");

                                    if (tatReportData.getTatAllProcess() == null || tatReportData.getTatAllProcess().equalsIgnoreCase("")) {
                                        tatReportData.setTatAchieve("");
                                    } else {
                                        long tatAllProcess = Long.parseLong(tatReportData.getTatAllProcess());

                                        //long tatAchieve = tatAllProcess/10;
                                        //fix for UATJira-671 on 8NOV to correct the %age value displayed
                                        Double tatAchieve = (double) tatAllProcess;
                                        tatAchieve = (tatAchieve / 10) * 100;
                                        //tatReportData.setTatAchieve(Long.toString(tatAchieve)+"%");
                                        //fix for UATJira-671 on 8 Nov to correct the %age value displayed
                                        tatReportData.setTatAchieve(tatAchieve.toString() + "%");
                                    }

                                    tatReportData.setRemarkClaim(dataOutPCaseSheet.getComments());

                                    tatReportData.setFatcaCRS(clientDetails != null ? clientDetails.getFATCAFLAG() : "");
                                    tatReportData.setChannel(policyDetails.getCHANNEL());
                                    tatReportData.setAgenName(policyDetails.getAGENTNAME());
                                    tatReportData.setUserID(claimDataTATReportData.getClaimUser());

                                    TATReportDataList.add(tatReportData);
                                } else {
                                    log.debug("Policy Details not found for the policy number: " + claimDataTATReportData.getPolicyNumber());
                                }
                            }

                            int i = 1;
                            @SuppressWarnings("rawtypes")
                            List reportList = new ArrayList();
                            List<XlsColValMap> xlsColValMapHeaderList1 = new ArrayList<XlsColValMap>();
                            List<XlsColValMap> xlsColValMapHeaderList2 = new ArrayList<XlsColValMap>();
                            List<XlsColValMap> xlsColValMapHeaderList3 = new ArrayList<XlsColValMap>();
                            List<XlsColValMap> xlsColValMapHeaderList4 = new ArrayList<XlsColValMap>();
                            List<XlsColValMap> xlsColValMapHeaderList5 = new ArrayList<XlsColValMap>();
                            List<XlsColValMap> xlsColValMapHeaderList6 = new ArrayList<XlsColValMap>();

                            xlsColValMapHeaderList1.add(prepGenericClassObj(0, "Report Claim (Include TAT)"));
                            xlsColValMapHeaderList2.add(prepGenericClassObj(0, "Product Code : " + dataIn.getWsInput().getStrProductCode()));

                            if (dataIn.getWsInput().getStrReportDateType().equalsIgnoreCase("Receive Date")) {
                                xlsColValMapHeaderList3.add(prepGenericClassObj(0, "Receive Date : " + dataIn.getWsInput().getDtFromDate() + " to " + dataIn.getWsInput().getDtToDate()));
                                xlsColValMapHeaderList4.add(prepGenericClassObj(0, "Register Date : "));
                            }
                            if (dataIn.getWsInput().getStrReportDateType().equalsIgnoreCase("Register Date")) {
                                xlsColValMapHeaderList3.add(prepGenericClassObj(0, "Receive Date : "));
                                xlsColValMapHeaderList4.add(prepGenericClassObj(0, "Register Date : " + dataIn.getWsInput().getDtFromDate() + " to " + dataIn.getWsInput().getDtToDate()));
                            }

                            for (int j = 0; j < 35; j++) {
                                xlsColValMapHeaderList1.add(prepGenericClassObj(j + 1, ""));
                                xlsColValMapHeaderList2.add(prepGenericClassObj(j + 1, ""));
                                xlsColValMapHeaderList3.add(prepGenericClassObj(j + 1, ""));
                                xlsColValMapHeaderList4.add(prepGenericClassObj(j + 1, ""));
                            }

                            for (int k = 0; k < 36; k++) {
                                xlsColValMapHeaderList5.add(prepGenericClassObj(k, ""));
                            }

                            xlsColValMapHeaderList6.add(prepGenericClassObj(0, "No"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(1, "POLICY NO"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(2, "CLAIM NO"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(3, "CLAIM TYPE"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(4, "Product Name"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(5, "Policy Status"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(6, "RCD Date"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(7, "Age of Policy"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(8, "Policy Owner"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(9, "Insured Name"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(10, "Receive Date"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(11, "Register Date"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(12, "Date of Death"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(13, "Admission date"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(14, "Discharge Date "));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(15, "Length of Stay"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(16, "Diagnosis"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(17, "Provider Name"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(18, "Payee Name"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(19, "Sum Assured"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(20, "Currency"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(21, "Amount Incurred"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(22, "Amount Approved"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(23, "Amount not Approved"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(24, "Status claim"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(25, "Pending Complete Date"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(26, "Claim Decision Date"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(27, "TAT Clean"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(28, "TAT all process"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(29, "TAT Target"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(30, "TAT achieve"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(31, "Remark claim"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(32, "FATCA CRS"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(33, "Channel"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(34, "Agen Name"));
                            xlsColValMapHeaderList6.add(prepGenericClassObj(35, "User ID"));

                            reportList.add(xlsColValMapHeaderList1);
                            reportList.add(xlsColValMapHeaderList2);
                            reportList.add(xlsColValMapHeaderList3);
                            reportList.add(xlsColValMapHeaderList4);
                            reportList.add(xlsColValMapHeaderList5);
                            reportList.add(xlsColValMapHeaderList6);

                            for (TATReportData tatReportData : TATReportDataList) {
                                List<XlsColValMap> xlsColValMapList = new ArrayList<XlsColValMap>();

                                xlsColValMapList.add(prepGenericClassObj(0, Integer.toString(i)));
                                xlsColValMapList.add(prepGenericClassObj(1, tatReportData.getPolicyNumber() != null ?
                                        tatReportData.getPolicyNumber().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(2, tatReportData.getClaimNumber() != null ?
                                        tatReportData.getClaimNumber().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(3, tatReportData.getClaimType() != null ?
                                        tatReportData.getClaimType().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(4, tatReportData.getProductName() != null ?
                                        tatReportData.getProductName().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(5, tatReportData.getPolicyStatus() != null ?
                                        tatReportData.getPolicyStatus().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(6, tatReportData.getRcDDate() != null ?
                                        tatReportData.getRcDDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(7, tatReportData.getAgeOfPolicy() != null ?
                                        tatReportData.getAgeOfPolicy().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(8, tatReportData.getPolicyOwner() != null ?
                                        tatReportData.getPolicyOwner().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(9, tatReportData.getInsuredName() != null ?
                                        tatReportData.getInsuredName().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(10, tatReportData.getReceiveDate() != null ?
                                        tatReportData.getReceiveDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(11, tatReportData.getRegisterDate() != null ?
                                        tatReportData.getRegisterDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(12, tatReportData.getDateOfDeath() != null ?
                                        tatReportData.getDateOfDeath().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(13, tatReportData.getAdmissionDate() != null ?
                                        tatReportData.getAdmissionDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(14, tatReportData.getDischargeDate() != null ?
                                        tatReportData.getDischargeDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(15, tatReportData.getLengthOfStay() != null ?
                                        tatReportData.getLengthOfStay().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(16, tatReportData.getDiagnosis() != null ?
                                        tatReportData.getDiagnosis().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(17, tatReportData.getProviderName() != null ?
                                        tatReportData.getProviderName().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(18, tatReportData.getPayeeName() != null ?
                                        tatReportData.getPayeeName().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(19, tatReportData.getSumAssured() != null ?
                                        tatReportData.getSumAssured().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(20, tatReportData.getCurrency() != null ?
                                        tatReportData.getCurrency().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(21, tatReportData.getAmountIncurred() != null ?
                                        tatReportData.getAmountIncurred().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(22, tatReportData.getAmountApproved() != null ?
                                        tatReportData.getAmountApproved().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(23, tatReportData.getAmountNotApproved() != null ?
                                        tatReportData.getAmountNotApproved().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(24, tatReportData.getClaimStatus() != null ?
                                        tatReportData.getClaimStatus().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(25, tatReportData.getPendingCompleteDate() != null ?
                                        tatReportData.getPendingCompleteDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(26, tatReportData.getClaimDecisionDate() != null ?
                                        tatReportData.getClaimDecisionDate().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(27, tatReportData.getTatClean() != null ?
                                        tatReportData.getTatClean().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(28, tatReportData.getTatAllProcess() != null ?
                                        tatReportData.getTatAllProcess().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(29, tatReportData.getTatTarget() != null ?
                                        tatReportData.getTatTarget().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(30, tatReportData.getTatAchieve() != null ?
                                        tatReportData.getTatAchieve().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(31, tatReportData.getRemarkClaim() != null ?
                                        tatReportData.getRemarkClaim().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(32, tatReportData.getFatcaCRS() != null ?
                                        tatReportData.getFatcaCRS().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(33, tatReportData.getChannel() != null ?
                                        tatReportData.getChannel().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(34, tatReportData.getAgenName() != null ?
                                        tatReportData.getAgenName().toString() : ""));
                                xlsColValMapList.add(prepGenericClassObj(35, tatReportData.getUserID() != null ?
                                        tatReportData.getUserID().toString() : ""));

                                reportList.add(xlsColValMapList);

                                i++;
                            }

                            JSONArray jsArray = new JSONArray(reportList);

                            WSReportOutputXLSDataIn jsonDataIn = new WSReportOutputXLSDataIn();
                            WSReportOutputXLSDataInWSInput wsDataIn = new WSReportOutputXLSDataInWSInput();

                            wsDataIn.setStrTemplateFileLocation(dataIn.getWsInput().getStrTemplateFileLocation());
                            wsDataIn.setStrTemplateFileName(dataIn.getWsInput().getStrTemplateFileName());
                            wsDataIn.setStrReportFileLocation(dataIn.getWsInput().getStrReportFileLocation());
                            wsDataIn.setReportData(jsArray.toString());
                            jsonDataIn.setWsInput(wsDataIn);

                            log.info("Invoking the WSReportOutputXLSProcessor Service with payload = " + jsonDataIn.toString());

                            WSReportOutputXLSDataOut dataOutReportXLS = producerTemplate.requestBody("direct:WSReportOutputXLSProcessor", jsonDataIn,
                                    WSReportOutputXLSDataOut.class);

                            if (dataOutReportXLS.getWsResponse().getWsProcessingStatus().equalsIgnoreCase("1")) {


                                String currDtString = simpleDateFormat1.format(new Date());

                                updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
                                        + "ReportGenerated=?,ReportEndTimeStamp=?,ReportFileLocation=? WHERE ReportRequestID=?");

                                argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Report generated Successfully"));
                                argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
                                argValuePairsList.add(prepareArgValuePairsObj("2", "string", "1"));
                                argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
                                argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportFileLocation() + "\\" + dataOutReportXLS.getWsResponse().getStrFileName()));
                                argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportRequestID()));

                                updateDataIn.setArgValuePairs(argValuePairsList);

                                boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

                                if (updateGenRepStatus) {
                                    log.info("Status updated in CASEREPORTREQUEST table successfully");
                                    response.setWsProcessingStatus("1");
                                    response.setWsSuccessMessage("Report generated Successfully");
                                } else {
                                    log.error("Exception updating status in CASEREPORTREQUEST table");
                                    response.setWsProcessingStatus("2");
                                    response.setWsExceptionMessage("Report generated Successfully, but Exception response form WSUpdateData service");
                                }
                            }
                            if (dataOutReportXLS.getWsResponse().getWsProcessingStatus().equalsIgnoreCase("2")) {
                                String currDtString = simpleDateFormat1.format(new Date());

                                updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
                                        + "ReportGenerated=?,ReportEndTimeStamp=?,ReportFileLocation=? WHERE ReportRequestID=?");

                                argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Exception while report generation: " + dataOutReportXLS.getWsResponse().getWsExceptionMessage()));
                                argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
                                argValuePairsList.add(prepareArgValuePairsObj("2", "string", "0"));
                                argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
                                argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportFileLocation()));
                                argValuePairsList.add(prepareArgValuePairsObj("5", "string", dataIn.getWsInput().getStrReportRequestID()));

                                updateDataIn.setArgValuePairs(argValuePairsList);

                                boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

                                if (updateGenRepStatus) {
                                    response.setWsProcessingStatus("2");
                                    response.setWsExceptionMessage("Exception while report generation: " + dataOutReportXLS.getWsResponse().getWsExceptionMessage());
                                } else {
                                    response.setWsProcessingStatus("2");
                                    response.setWsExceptionMessage("Exception while report generation and WSUpdateData service: " + dataOutReportXLS.getWsResponse().getWsExceptionMessage());
                                }
                            }
                        }
                    }
                }
                if (dataIn.getWsInput().getStrReportType().equalsIgnoreCase("ps")) {
                    if (dataIn.getWsInput().getStrReportOutuputFormat().equalsIgnoreCase("web")) {

                    }
                }
            } else {
                String currDtString = simpleDateFormat1.format(new Date());

                updateDataIn.setStrSQLQuery("UPDATE CASEREPORTREQUEST SET ReportGenerationMessage=?,ProcessedFlag=?,"
                        + "ReportGenerated=?,ReportEndTimeStamp=? WHERE ReportRequestID=?");

                argValuePairsList.add(prepareArgValuePairsObj("0", "string", "Invalid Input to generate the report data"));
                argValuePairsList.add(prepareArgValuePairsObj("1", "string", "1"));
                argValuePairsList.add(prepareArgValuePairsObj("2", "string", "0"));
                argValuePairsList.add(prepareArgValuePairsObj("3", "", currDtString));
                argValuePairsList.add(prepareArgValuePairsObj("4", "string", dataIn.getWsInput().getStrReportRequestID()));

                updateDataIn.setArgValuePairs(argValuePairsList);

                boolean updateGenRepStatus = invokeUpdateDataService(updateDataIn);

                if (updateGenRepStatus) {
                    log.info("Status updated in CASEREPORTREQUEST table successfully");
                    response.setWsProcessingStatus("2");
                    response.setWsExceptionMessage("Invalid Input to generate the report data");
                } else {
                    log.error("Exception updating status in CASEREPORTREQUEST table");
                    response.setWsProcessingStatus("2");
                    response.setWsExceptionMessage("Invalid Input to generate the report data");
                }

            }
        } catch (Exception e) {
            log.error("WSGenerateTATReportDataProcessor Error" + e);
            for (StackTraceElement tr : e.getStackTrace()) {
                log.error("\tat " + tr);
            }
            response.setWsExceptionMessage(e.getMessage());
            response.setWsProcessingStatus("2");
            response.setWsSuccessMessage("");
        }

        log.info("Final Response " + response.toString());
        exchange.getOut().setBody(response);

    }

    public ArgValuePairs prepareArgValuePairsObj(String index, String paramType, String value) {
        ArgValuePairs argValuePairsObj = new ArgValuePairs();
        argValuePairsObj.setIndex(index);
        argValuePairsObj.setTypeParam(paramType);
        argValuePairsObj.setValue(value);
        return argValuePairsObj;
    }

    public boolean invokeUpdateDataService(WSUpdateDataDataIn updateDataIn) {
        log.info("Invoking the WSUpdateData Service with payload = " + updateDataIn.toString());

        JsonResponse wsUpdateDataResponse = producerTemplate.requestBody("direct:WSUpdateDataProcessor", updateDataIn,
                JsonResponse.class);

        if (wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("1")) {
            log.info("Status updated in CASEREPORTREQUEST table successfully");
            return true;
        }
        if (wsUpdateDataResponse.getWsProcessingStatus().equalsIgnoreCase("2")) {
            log.error("Exception updating status in CASEREPORTREQUEST table");
            return false;
        }
        return false;
    }

    public int getDiffDays(Date higherDate, Date lowerDate) {
        Calendar higherDateCalendar = Calendar.getInstance();
        Calendar lowerDateDateCalendar = Calendar.getInstance();
        higherDateCalendar.setTime(higherDate);
        lowerDateDateCalendar.setTime(lowerDate);

        DateTime higherDateTs = new DateTime(higherDateCalendar.get(higherDateCalendar.YEAR), higherDateCalendar.get(higherDateCalendar.MONTH) + 1, higherDateCalendar.getTime().getDate(), 0, 0);
        DateTime lowerDateTs = new DateTime(lowerDateDateCalendar.get(lowerDateDateCalendar.YEAR), lowerDateDateCalendar.get(lowerDateDateCalendar.MONTH) + 1, lowerDateDateCalendar.getTime().getDate(), 0, 0);
        //int diffInDays = Days.daysBetween(higherDateTs, lowerDateTs).getDays();
        // SWATI -8NOV2019-UAT Jira-671 fix:reversed the argument sequence because api takes startdate, enddate as parameter
        // so now will return +ve value
        int diffInDays = Days.daysBetween(lowerDateTs, higherDateTs).getDays();
        return diffInDays;
    }
}
