/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetPolicyDatabyPolicyNumberWSInput;
import com.candelalabs.api.model.DataOutGetPolicyDatabyPolicyNumberWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.PolicyDetails;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetPolicyDatabyPolicyNumberProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetPolicyDatabyPolicyNumberProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetPolicyDatabyPolicyNumberWSResponse wsresponse = new DataOutGetPolicyDatabyPolicyNumberWSResponse();

		try {
			DataInGetPolicyDatabyPolicyNumberWSInput dataIn = exchange.getIn()
					.getBody(DataInGetPolicyDatabyPolicyNumberWSInput.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());

			if (dataIn.getStrPolicyNumber() != null && dataIn.getStrPolicyNumber().trim() != "") {
				PolicyDetails polDetailData = new PolicyDetails();

				log.info("Getting the PolicyDetails Data with polNUm = " + dataIn.getStrPolicyNumber());
				polDetailData = this.oDSDao.getPolicyDetail(dataIn.getStrPolicyNumber());

				if (!Optional.ofNullable(polDetailData).isPresent()) {
					throw new DAOException("Not available any records in PolicyDetails table with polNum = "
							+ dataIn.getStrPolicyNumber());
				}
				log.info("Successfully Got the PolicyDetails Data with polNUm = " + dataIn.getStrPolicyNumber());
				wsresponse.setStrProductCode(polDetailData.getPRODUCTCODE());
				wsresponse.setStrProductName(polDetailData.getPRODUCTNAME());
				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Success");

			} else {
				throw new CamelException("Request Input is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("Error: " + e.getMessage());
		}

		log.info("JSONRESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);

	}

}
