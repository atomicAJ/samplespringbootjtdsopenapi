/**
 * 
 */
package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSGetProcessCaseSheetCommentsDataIn;
import com.candelalabs.api.model.WSGetProcessCaseSheetCommentsDataOut;
import com.candelalabs.utilities.model.DsProcessCaseSheetwithTs;
import com.candelalabs.ws.dao.DsConfigDAO;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
public class WSGetProcessCaseSheetCommentsProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSGetProcessCaseSheetCommentsProcessor");

	@Autowired
	DsConfigDAO dsConfigDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		WSGetProcessCaseSheetCommentsDataOut response = new WSGetProcessCaseSheetCommentsDataOut();
		try {
			WSGetProcessCaseSheetCommentsDataIn dataIn = exchange.getIn()
					.getBody(WSGetProcessCaseSheetCommentsDataIn.class);
			log.info("DATA INPUT PAYLOAD = " + dataIn.toString());
			List<DsProcessCaseSheetwithTs> pCaseSheetData_li = new ArrayList<DsProcessCaseSheetwithTs>();
			if (dataIn.getActivityName() != null
					&& dataIn.getCaseReferenceKey() != null & dataIn.getProcessName() != null
					&& dataIn.getActivityName() != ""
					&& dataIn.getTagName() != ""
					&& dataIn.getTagName() != null
					&& dataIn.getCaseReferenceKey() != "" & dataIn.getProcessName() != "") {
				//changing so that the query is more efficient
				pCaseSheetData_li = this.dsConfigDao.getCommentsByActivityAndTag(dataIn.getActivityName(),
						dataIn.getTagName(), dataIn.getCaseReferenceKey());

				if (pCaseSheetData_li != null && pCaseSheetData_li.size() > 0) {
					DsProcessCaseSheetwithTs pCaseSheetData = pCaseSheetData_li.get(0);

					response.setActivityName(pCaseSheetData.getActivityName());
					response.setCaseReferenceKey(pCaseSheetData.getCaseReferenceKey());
					response.setCasesheetId(BigDecimal.valueOf(pCaseSheetData.getCasesheetid()));
				    response.setComments(pCaseSheetData.getComments());
					//response.setComments(comments);
					response.setProcessCaseSheetInsertionTime(
							Util.objToOffSetDateTime(pCaseSheetData.getProcessCaseSheetInsertionTime()));
					response.setProcessId(pCaseSheetData.getProcessId());
					response.setProcessInstanceName(pCaseSheetData.getProcessInstanceName());
					response.setUserName(pCaseSheetData.getUserName());
				}

				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Successfull retrieved the Data");
				//Below block of code will fetch comments using tag name and case reference key for reports
			} else if(dataIn.getCaseReferenceKey() != null && dataIn.getCaseReferenceKey() != "" &&
					dataIn.getTagName() != null && dataIn.getTagName() != ""){
				log.debug("----- get comments by tag name and case reference name -----");
				String comments = this.dsConfigDao.getReportProcessCaseSheetComments(dataIn.getCaseReferenceKey(), dataIn.getTagName());
				response.setWsProcessingStatus("1");
				response.setComments(comments);
				response.setWsSuccessMessage("Successfull retrieved the Data");
			} else {
				throw new Exception("Input Data is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error " + e);
			// log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error on processing with error details = " + e);
		}
		log.info("DATA OUTPUT PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);

	}

}
