/**
 * 
 */
package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSCheckUISaveFlagDataIn;
import com.candelalabs.api.model.WSCheckUISaveFlagDataOut;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSCheckUISaveFlagProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSCheckUISaveFlagProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		WSCheckUISaveFlagDataOut response = new WSCheckUISaveFlagDataOut();
		try {
			WSCheckUISaveFlagDataIn dataIn = exchange.getIn().getBody(WSCheckUISaveFlagDataIn.class);
			log.info("DATA INPUT PAYLOAD = " + dataIn.toString());
			String claimNumber = dataIn.getWsInput().getStrClaimNumber();
			if (claimNumber != null && claimNumber.trim() != "" && claimNumber.length() > 0) {

				log.info("Get ClaimData table with claimNo= " + claimNumber);
				ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(claimNumber);
				log.info("Successfully retrieved ClaimData table");
				log.info("UISaveFlag = " + claimData.getUISaveFlag());
				response.setStrUISaveFlag(claimData.getUISaveFlag());

			} else {
				throw new Exception("ClaimNumber input is Empty");
			}

			response.setWsProcessingStatus("1");
			response.setWsSuccessMessage("Successfully retrieved the UISaveFlag");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error " + e);
			// log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}

			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("Error processing= " + e.getMessage());
		}
		log.info("JSON RESPONSE PAYLOAD = " + response.toString());
		exchange.getOut().setBody(response);

	}

}
