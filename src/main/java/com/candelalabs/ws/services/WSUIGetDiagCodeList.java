/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.List;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.candelalabs.api.model.DataInGetDiagnosisCodeListWSInput;
import com.candelalabs.api.model.DataOutGetDiagnosisCodeListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetDiagCodeList implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetDiagCodeList");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("************************************************************************************* \n"
				+ "Service is Invoked");

		DataOutGetDiagnosisCodeListWSResponse wsresponse = new DataOutGetDiagnosisCodeListWSResponse();
		try {

			DataInGetDiagnosisCodeListWSInput dataIn = exchange.getIn()
					.getBody(DataInGetDiagnosisCodeListWSInput.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());

			if (dataIn.getStrClaimTypeUI() != "" && dataIn.getStrClaimTypeUI() != null) {
				log.info("Getting the DiagName List ");
				List<String> diagList = this.oDSDao.getDiagnoseCodeList(dataIn.getStrClaimTypeUI().trim());
				log.info("Got the DiagName List with size = " + diagList.toString());

				if (diagList.size() > 0 && diagList != null) {
					wsresponse.setStrDiagnosisCodeList(diagList);
					wsresponse.setWsProcessingStatus("1");
					wsresponse.setWsSuccessMessage("Success");

				} else {
					throw new DAOException("DiagCode List is empty");
				}
			} else {
				throw new CamelException("Request Input is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("Error: " + e.getMessage());

		}
		log.info("JSONRESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);
	}

}
