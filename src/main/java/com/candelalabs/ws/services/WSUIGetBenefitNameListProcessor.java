/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetBenefitCodeNameListWSInput1;
import com.candelalabs.api.model.DataOutGetBenefitNameListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetBenefitNameListProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetBenefitNameListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetBenefitNameListWSResponse wsresponseName = new DataOutGetBenefitNameListWSResponse();
		try {
			DataInGetBenefitCodeNameListWSInput1 dataInName = exchange.getIn()
					.getBody(DataInGetBenefitCodeNameListWSInput1.class);
			log.info("DATA INPUT: "+dataInName.toString());
			List<String> benNameList = new ArrayList<String>();
			benNameList = this.oDSDao.getBenefitNameList(dataInName.getStrComponentCode1().trim(),dataInName.getStrClaimTypeUI1());
			if (benNameList.size() > 0 && benNameList != null) {
				wsresponseName.setStrBenefitNameList(benNameList);
				wsresponseName.setWsProcessingStatus("1");
				wsresponseName.setWsSuccessMessage("Successfully retrieved the Benefit Code List");
			} else {
				throw new DAOException("Benefit Code List is not available");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			wsresponseName.setWsProcessingStatus("2");
			wsresponseName.setWsExceptionMessage("ERROR DETAIL " + e.getMessage());
		}
		
		log.info("DATA OUTPUT: "+wsresponseName.toString());
		exchange.getOut().setBody(wsresponseName);

	}

}
