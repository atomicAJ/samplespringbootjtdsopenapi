/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSRegisterNotificationDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.NotificationDecisionMasterModel;
import com.candelalabs.ws.model.tables.NotificationProcessMasterModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSRegisterNotificationProcesssor implements Processor {

	private static final Log log = LogFactory.getLog("WSRegisterNotificationProcesssor");

	@Autowired
	private ClaimDetailsDao claimDetailsDao;

	@Autowired
	private WSRegisterNotificationProcesssorTransactional wSRegisterNotificationProcesssorTransactional;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("**********************************************************************************************"
				+ "Service is invoked");
		JsonResponse response = new JsonResponse();
		response.setWsExceptionMessage("");
		response.setWsProcessingStatus("");
		response.setWsSuccessMessage("");
		try {
			WSRegisterNotificationDataIn dataIn = exchange.getIn().getBody(WSRegisterNotificationDataIn.class);
			log.info("Data In Payload= " + dataIn.toString());
			log.info("Fetching NotificationProcessMaster table with decision= " + dataIn.getDECISION());

			List<NotificationProcessMasterModel> notifProcess_li = this.claimDetailsDao.getNotificationProcess(
					dataIn.getPROCESSNAME(), dataIn.getPROCESSACTIVITYNAME(), dataIn.getDECISION());
			log.info("Successfully Fetched the table with rows= " + notifProcess_li.size());
			log.info("Fetching CLAIMDATA with claimNo= " + dataIn.getCLAIMNUMBER());
			ClaimDataNewModel claimData = this.claimDetailsDao.getClaimDataNew(dataIn.getCLAIMNUMBER());

			log.info("Successfully fetched data from CLAIMDATA table");
			if (notifProcess_li != null && notifProcess_li.size() > 0) {
				int rowNum = 1;
				for (NotificationProcessMasterModel notProcMasterModel : notifProcess_li) {
					log.info("Processing ConfigurationID= "+notProcMasterModel.getConfigurationID());
					log.info("Get the data from NotificationCategoryMaster table");
					List<String> notifCateg_li = this.claimDetailsDao
							.getNotifCategoryResultset(notProcMasterModel.getConfigurationID());
					log.info("Fetched the data from the table,  with size = "+notifCateg_li.size());

					if (notifCateg_li != null && notifCateg_li.size() > 0) {
						for (String notifCateg : notifCateg_li) {
							log.info("Processing NotifCategory = "+notifCateg);
							switch (notifCateg) {
							case "SMS":
								log.info("Fetch smsTemplateId from NOTIFICATIONSMSTEMPLATEMASTER Table where ConfigID= "
										+ notProcMasterModel.getConfigurationID());
								List<Integer> smsId_li = this.claimDetailsDao
										.getSMSTemplateResultSet(notProcMasterModel.getConfigurationID());

								log.info("Successfully fetched the table, SmsTemplateIdSize = " + smsId_li.size());
								if (smsId_li == null || smsId_li.size() == 0) {
									log.info("Set Exception Message SMS Notification Category");
									setExceptionMessage(response,
											"No SMS Template ID found to generate Notification entry for CLAIMNUMBER=",
											dataIn.getCLAIMNUMBER());
								} else {
									for (Integer smsId : smsId_li) {
										log.info("Insert SmsTemplateId to NotificationTable with smsId = "+smsId);
										/*int i = this.claimDetailsDao.insertNotificationTable2("SMSTemplateID",
												dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(), dataIn.getDECISION(),
												notifCateg, smsId, "N", "N", "N", "N", "N",
												dataIn.getPROCESSCATEGORY());*/
						//below query insert statement modified as per changes in Amit's email "RE: UAT eClaim 25042019"
										int i = this.claimDetailsDao.insertNotificationTable2("SMSTemplateID",
												dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(), dataIn.getDECISION(),
												notifCateg, smsId, "N", "N", "N", "N", "N",
												dataIn.getPROCESSCATEGORY(),dataIn.getAPPLICATIONNUMBER(),
												dataIn.getPOLICYNUMBER());
										if (i == 0) {
											throw new DAOException(
													"Not Inserted to Notification Table with Notification Category: "
															+ notifCateg);
										}
										log.info("Successfully Inserted SmsTemplateId to NotificationTable");
										setSuccessfullMessage(response,
												"Notification Record created for SMS with TemplateID =" + smsId,
												dataIn.getCLAIMNUMBER());
									}
								}
								break;
							case "EMAIL":
								log.info(
										"Fetch mailTemplateId from NOTIFICATIONEMAILTEMPLATEMASTER Table where ConfigID= "
												+ notProcMasterModel.getConfigurationID());
								List<Integer> mailId_li = new ArrayList<Integer>();
								mailId_li = this.claimDetailsDao
										.getMailTemplateResultSet(notProcMasterModel.getConfigurationID(), 0);

								log.info("Successfully fetched the table, mailTemplateIdSize = " + mailId_li.size());
								if (mailId_li == null || mailId_li.size() == 0) {
									log.info("MailTemplateId is null then set the Exception Message");
									setExceptionMessage(response,
											"No Email Template ID found to generate Notification entry for CLAIMNUMBER=",
											dataIn.getCLAIMNUMBER());

								} else {
									for (Integer mailId : mailId_li) {
										log.info("Insert mailTemplateId to NotificationTable, mailId= "+mailId);
										/*int i = this.claimDetailsDao.insertNotificationTable2("EmailTemplateID",
												dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(), dataIn.getDECISION(),
												notifCateg, mailId, "N", "N", "N", "N", "N",
												dataIn.getPROCESSCATEGORY());*/
						//below query insert statement modified as per changes in Amit's email "RE: UAT eClaim 25042019"
										int i = this.claimDetailsDao.insertNotificationTable2("EmailTemplateID",
												dataIn.getCLAIMNUMBER(), dataIn.getPROCESSNAME(), dataIn.getDECISION(),
												notifCateg, mailId, "N", "N", "N", "N", "N",
												dataIn.getPROCESSCATEGORY(),dataIn.getAPPLICATIONNUMBER(),dataIn.getPOLICYNUMBER());
										if (i == 0) {
											throw new DAOException(
													"Not Inserted to Notification Table with Notification Category: "
															+ notifCateg);
										}
										log.info("Successfully Inserted mailTemplateId to NotificationTable");
										setSuccessfullMessage(response,
												"Notification Record created for Email with TemplateID =" + mailId,
												dataIn.getCLAIMNUMBER());
									}
								}
								break;
							case "LETTER":
								this.wSRegisterNotificationProcesssorTransactional.letterProcess(claimData, response,
										dataIn, notProcMasterModel, notifCateg);
								break;
							case "EMAILLETTER":
								this.wSRegisterNotificationProcesssorTransactional.emailLetterProcess(claimData,
										response, dataIn, notProcMasterModel, notifCateg);
								break;
							default:
								break;
							}
						}
					} else {
						throw new Exception("No Notificategory Result Set is found");
					}

					log.debug("ROWNUM= " + rowNum);

					rowNum = rowNum + 1;
				}

			} else {
				throw new DAOException("Not Found any Data in NOTIFICATIONPROCESSMASTER");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.info("Error Detail= " + e);
			log.info("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.info("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage(response.getWsExceptionMessage() + " AND Error: " + e.getMessage()
					+ " AND Successfully completed Operations are = " + response.getWsSuccessMessage());
		}
		log.info("Json Response: " + response.toString());
		exchange.getOut().setBody(response);
	}

	private void setExceptionMessage(JsonResponse response, String message, String claimNumber) throws Exception {
		response.setWsProcessingStatus("2");
		if (response.getWsExceptionMessage() == "") {
			response.setWsExceptionMessage(message + " " + claimNumber);
		} else {
			response.setWsExceptionMessage(response.getWsExceptionMessage() + " And " + message + " " + claimNumber);
		}
		throw new Exception(response.getWsExceptionMessage());
	}

	private void setSuccessfullMessage(JsonResponse response, String message, String claimNumber) {
		response.setWsProcessingStatus("1");
		if (response.getWsSuccessMessage() == "") {
			response.setWsSuccessMessage(message + " " + claimNumber);
		} else {
			response.setWsSuccessMessage(response.getWsSuccessMessage() + " And " + message + " " + claimNumber);
		}
	}

}
