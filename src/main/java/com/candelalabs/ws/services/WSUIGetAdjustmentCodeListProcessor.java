/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataOutGetAdjustmentCodeListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetAdjustmentCodeListProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetAdjustmentCodeListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetAdjustmentCodeListWSResponse wsresponse = new DataOutGetAdjustmentCodeListWSResponse();
		try {
			List<String> adjCode_li = new ArrayList<String>();
			adjCode_li = this.oDSDao.getAdjustmentCodeList();
			if (adjCode_li.size() <= 0) {
				throw new DAOException("Not available any records in AdjusmentCode table");
			}

			wsresponse.setStrAdjustmentCodeList(adjCode_li);
			wsresponse.setWsProcessingStatus("1");
			wsresponse.setWsSuccessMessage("Successfully retrieved the AdjustmentCode List Data");

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			log.error("ERROR DETAIL: " + e.getStackTrace());
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL " + e.getMessage());
		}

		log.info("JSON RESPONSE PAYLOAD: " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);
	}

}
