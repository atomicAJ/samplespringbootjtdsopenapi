/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetBankCodeTypebyFactoringHouseWSInput;
import com.candelalabs.api.model.DataOutGetBankCodeTypebyFactoringHouseWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.CamelException;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.BankCodeMasterModel;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetBankCodeTypeByFactHouseProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetDiagNameByCodeProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("************************************************************************************* \n"
				+ "Service is Invoked");

		DataOutGetBankCodeTypebyFactoringHouseWSResponse wsresponse = new DataOutGetBankCodeTypebyFactoringHouseWSResponse();
		try {
			DataInGetBankCodeTypebyFactoringHouseWSInput dataIn = exchange.getIn()
					.getBody(DataInGetBankCodeTypebyFactoringHouseWSInput.class);
			log.info("JSON INPUT PAYLOAD: " + dataIn.toString());
			if (dataIn.getStrFactoringHouse().trim() != "" && dataIn.getStrFactoringHouse() != null) {
				BankCodeMasterModel bankCodeData = new BankCodeMasterModel();

				bankCodeData = this.oDSDao.getBankCodeMasterByFactHouse(dataIn.getStrFactoringHouse());

				if (Optional.ofNullable(bankCodeData).isPresent()) {
					wsresponse.setStrBankCode(bankCodeData.getBANKKEY());
					wsresponse.setStrBankType(bankCodeData.getBANKTYPE());
					wsresponse.setWsProcessingStatus("1");
					wsresponse.setWsSuccessMessage("Successfully Retrieved the Bank Code data");
				} else {
					throw new DAOException(
							"BankCodeMaster data is not available with FactHouse = " + dataIn.getStrFactoringHouse());
				}

			} else {
				throw new CamelException("Request String is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR: " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR = " + e.getMessage());
		}

		log.info("JSON RESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);

	}

}
