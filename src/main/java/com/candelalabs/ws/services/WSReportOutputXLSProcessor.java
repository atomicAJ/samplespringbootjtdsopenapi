package com.candelalabs.ws.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSReportOutputXLSDataIn;
import com.candelalabs.api.model.WSReportOutputXLSDataOut;
import com.candelalabs.api.model.WSReportOutputXLSDataOutWSResponse;

@Component
public class WSReportOutputXLSProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSReportOutputXLSProcessor");

	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("WSReportOutputXLSProcessor - Service is invoked");

		WSReportOutputXLSDataIn dataIn = (exchange.getIn().getBody(WSReportOutputXLSDataIn.class));
		WSReportOutputXLSDataOut response = new WSReportOutputXLSDataOut();
		WSReportOutputXLSDataOutWSResponse wsResponse = new WSReportOutputXLSDataOutWSResponse();

		try {
			log.debug("----- dataIn -----");
			log.debug(dataIn.getWsInput().toString());

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
			LocalDateTime now = LocalDateTime.now();
			String dateTimeString = dtf.format(now).replaceAll("/","_");
			dateTimeString = dateTimeString.replaceAll(":","_");

			String templateFileLocation = dataIn.getWsInput().getStrTemplateFileLocation();
			String templateFileName = dataIn.getWsInput().getStrTemplateFileName();
			String reportFileLocation = dataIn.getWsInput().getStrReportFileLocation();
			String[] tokens = templateFileName.split("\\.(?=[^\\.]+$)");
			String reportFileName = tokens[0]+"-"+dateTimeString+"."+tokens[1];


			if(checkFileExist(templateFileLocation+"//"+templateFileName)) {

				FileInputStream inputStream = new FileInputStream(templateFileLocation+"//"+templateFileName);
				Workbook workbook = new XSSFWorkbook(inputStream);

				Sheet sheet = null;
				Object[][] bookData;
				
				sheet = workbook.getSheetAt(0);

				JSONArray jsonArr = new JSONArray(dataIn.getWsInput().getReportData());
				JSONArray colCount = jsonArr.getJSONArray(0);
				
				bookData = new Object[jsonArr.length()][colCount.length()];
				
				for (int i = 0; i < jsonArr.length(); i++){
					
					JSONArray jsonArrInner = jsonArr.getJSONArray(i);
					
					for (int j = 0; j < jsonArrInner.length(); j++){
						JSONObject jsonObj = jsonArrInner.getJSONObject(j);
						
						int colNum = Integer.parseInt(jsonObj.get("colNum").toString());
						
						bookData[i][colNum] = jsonObj.get("colValue") != null ? jsonObj.get("colValue") : "";
						
					}
				}

				int rowCount = sheet.getLastRowNum();

				for (Object[] aBook : bookData) {
					Row row = sheet.createRow(rowCount++);

					int columnCount = 0;

					Cell cell = row.createCell(columnCount);

					for (Object field : aBook) {
						cell = row.createCell(columnCount++);
						cell.setCellValue(field.toString());
					}
				}

				FileOutputStream outputStream = new FileOutputStream(reportFileLocation+"//"+reportFileName);
				workbook.write(outputStream);
				
				workbook.close();
				outputStream.close();
				inputStream.close();

				wsResponse.setWsProcessingStatus("1");
				wsResponse.setWsSuccessMessage("Report XLS file generated");
				wsResponse.setStrFileName(reportFileName);
				response.setWsResponse(wsResponse);
			} else {
				wsResponse.setWsProcessingStatus("2");
				wsResponse.setWsSuccessMessage("Template file not found!!!");
				response.setWsResponse(wsResponse);
			}
		} catch (Exception e) {
			log.error("WSReportOutputXLSProcessor Error" + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			wsResponse.setWsProcessingStatus("2");
			wsResponse.setWsSuccessMessage("Could not generate the Report XLS file - "+e.getMessage());
			response.setWsResponse(wsResponse);
		}


		log.info("Final Response " + response.toString());
		exchange.getOut().setBody(response);
	}

	public boolean checkFileExist(String filePath) {
		log.debug("In method checkFileExist, chekcing if file - " + filePath + " exist");
		File file = new File(filePath);
		if (file.exists()) {
			log.debug("File - " + filePath + " exist");
			return true;
		} else {
			log.debug("File - " + filePath + " does not exist");
			return false;
		}
	}

}
