/**
 * 
 */
package com.candelalabs.ws.services;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSManageReportEntriesDataIn;
import com.candelalabs.api.model.WSManageReportEntriesDataOut;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ReportDetailModel;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
public class WSManageReportEntriesProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSManageReportEntriesProcessor");

	@Autowired
	private ClaimDetailsDao claimDetailsDao;

	/**
	 * 
	 */
	public WSManageReportEntriesProcessor() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		WSManageReportEntriesDataOut dataOut = new WSManageReportEntriesDataOut();
		log.info("Service is invoked");
		try {

			WSManageReportEntriesDataIn dataIn = exchange.getIn().getBody(WSManageReportEntriesDataIn.class);
			log.info("Payload object: " + dataIn.toString());
			ReportDetailModel reportDetail = new ReportDetailModel(dataIn.getClaimNumber(),
					dataIn.getPolicyRequestNumber(), dataIn.getAppNumber(), dataIn.getPolicyNumber(),
					dataIn.getProcessInstanecID(), dataIn.getProcessName(), dataIn.getActivityType(),
					dataIn.getActivityName(), Util.convertToTs(dataIn.getStartTime()),
					Util.convertToTs(dataIn.getEndTime()), dataIn.getActionedUser(), dataIn.getUserDecision());
			log.info("Report Action: " + dataIn.getReportAction());
			int rowUpdated = 0;
			if (dataIn.getReportAction().equalsIgnoreCase("1")) {
				log.info("Inserting data to Report Detail Table with ClaimNumber: " + dataIn.getClaimNumber());
				rowUpdated = this.claimDetailsDao.insertReportDetailTable(reportDetail);
				// dataOut.setProcessmsg("Successfully inserted the Report
				// Detail");
				log.info("Successfully Inserting the data");
			} else if (dataIn.getReportAction().equalsIgnoreCase("2")) {
				log.info("Updating data to Report Detail Table with ClaimNumber: " + dataIn.getClaimNumber());
				rowUpdated = this.claimDetailsDao.updateReportDetailTable(reportDetail);
				// dataOut.setProcessmsg("Successfully Updated the Report
				// Detail");
				log.info("Successfully Updating the data");
			} else {
				throw new CamelException("Not Recognized Report Action= " + dataIn.getReportAction());
			}

//			if (rowUpdated == 0) {
//				throw new DAOException(
//						"No Updated Row in REPORTDETAIL Table for ClaimNumber= " + dataIn.getClaimNumber());
//			}

			dataOut.setWsProcessingStatus("1");
			dataOut.setWsSuccessMessage("Successfully updated the report entries for the current task");
			dataOut.setWsExceptionMessage("");

		} catch (Exception e) {
			// TODO: handle exception
			dataOut.setWsProcessingStatus("2");
			dataOut.setWsSuccessMessage("");
			dataOut.setWsExceptionMessage("Could not update Report entries for the current task =" + e.getMessage());
			log.info("Error Detail= " + e);
			log.info("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr :e.getStackTrace()) {
				log.info("\tat "+tr);
			}
		}

		log.info("WS Response " + dataOut.toString());

		exchange.getOut().setBody(dataOut);

	}

}
