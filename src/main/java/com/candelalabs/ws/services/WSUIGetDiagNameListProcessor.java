/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.List;

import javax.xml.ws.RespectBinding;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataInGetDiagnosisCodeListWSInput;
import com.candelalabs.api.model.DataOutGetDiagnosisNameListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetDiagNameListProcessor implements Processor {
	private static final Log log = LogFactory.getLog("WSUIGetDiagNameListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("************************************************************************************* \n"
				+ "Service is Invoked");
		DataOutGetDiagnosisNameListWSResponse wsresponse = new DataOutGetDiagnosisNameListWSResponse();

		try {
			DataInGetDiagnosisCodeListWSInput dataIn = exchange.getIn()
					.getBody(DataInGetDiagnosisCodeListWSInput.class);
			log.info("JSON INPUT PAYLOAD = " + dataIn.toString());
			if (dataIn.getStrClaimTypeUI() != "" && dataIn.getStrClaimTypeUI() != null) {
				log.info("Getting the DiagName List ");
				List<String> diagList = this.oDSDao.getDiagnoseNameList(dataIn.getStrClaimTypeUI());
				log.info("Got the DiagName List with size = " + diagList.toString());

				if (diagList.size() > 0 && diagList != null) {
					wsresponse.setStrDiagnosisNameList(diagList);
					wsresponse.setWsProcessingStatus("1");
					wsresponse.setWsSuccessMessage("Success");

				} else {
					throw new DAOException("DiagName List is empty");
				}
			} else {
				throw new CamelException("Request Input is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL = "+e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("Error: " + e.getMessage());

		}
		log.info("JSONRESPONSE PAYLOAD = " + wsresponse.toString());
		exchange.getOut().setBody(wsresponse);
	}

}
