/**
 * 
 */
package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DetCL06CompCodeDataOut;
import com.candelalabs.api.model.DetDeathCompCodeDataOut;
import com.candelalabs.api.model.DetRegDeathCompCodeDataIn;
import com.candelalabs.api.model.DetRegDeathCompCodeDataInWSInput;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSDNULDetCL06CompCodeProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Value("${CL06CompCode}")
	private String CL06CompCode;

	private static Log log = LogFactory.getLog("WSDNULDetCL06CompCodeProcessor");

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DetCL06CompCodeDataOut response = new DetCL06CompCodeDataOut();
		try {
			DetRegDeathCompCodeDataIn dataIn = exchange.getIn().getBody(DetRegDeathCompCodeDataIn.class);
			DetRegDeathCompCodeDataInWSInput wsInput = dataIn.getWsInput();
			log.info("JSON INPUT PAYLOAD = "+dataIn.toString());
			if (dataIn != null) {
				log.info("Getting the claimData table with claimNumber: " + wsInput.getClaimNumber());
				ClaimDataNewModel claimData = new ClaimDataNewModel();
				claimData = this.claimDetailsDao.getClaimDataNew(wsInput.getClaimNumber());
				log.info("Successfully Getting the claimData table with claimNumber: " + wsInput.getClaimNumber());
				
				if (claimData.getComponentCode().trim().equalsIgnoreCase(this.CL06CompCode.trim())) {
					response.setComponentCode("CL06");
					response.setDecision("CL06 CODE");
				} else {
					response.setComponentCode("CL06");
					response.setDecision("NON CL06 CODE");
				}
				
				response.setWsProcessingStatus("1");
				response.setWsSuccessMessage("Successfully Compared the data");

			} else {
				throw new Exception("Request Input is empty");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("Error " + e);
			// log.error("ERROR DETAIL: " + e.getStackTrace());
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			response.setWsProcessingStatus("2");
			response.setWsExceptionMessage("ERROR Detail = " + e.getMessage());
		}
		log.info("JSON RESPONSE PAYLOAD = "+response.toString());
		exchange.getOut().setBody(response);
	}

}
