/**
 * 
 */
package com.candelalabs.ws.services.activitirest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.candelalabs.api.model.SignalDetailsVariables;
import com.candelalabs.ws.exception.ActivitiException;
import com.candelalabs.ws.model.activitirest.ProcessInstanceVariablesModel;
import com.candelalabs.ws.model.activitirest.QueryExecutionRestModel;
import com.candelalabs.ws.util.Util;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;

/**
 * @author Triaji
 *
 */
@Service("restRequest")
@PropertySource("classpath:application.properties")
public class RestRequestImpl implements RestRequest {

	private static final Log log = LogFactory.getLog("WSSignalReceivedTaskProcessor");

	@Autowired
	private RestTemplate restTemplate;
	@Value("${activiti.bpm.url}")
	private String activityUrl;
	@Value("${activiti.bpm.cred64}")
	private String activitiCred;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.ws.services.activitirest.RestRequest#queryExecutionId(
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public Map<String, String> queryExecutionId(String processInstanceId, String activityId) throws Exception {
		// TODO Auto-generated method stub
		String address = "query//executions";
		// QueryExecutionRestModel qm = new
		// QueryExecutionRestModel(processInstanceId,
		// BoIdentifierActivityNameUtil.getActivityId(activityId));
		QueryExecutionRestModel qm = new QueryExecutionRestModel(processInstanceId, activityId);
		HttpHeaders headers = Util.setHttpPostHeaders(activitiCred);
		HttpEntity<String> entity = new HttpEntity<String>(Util.objectToJsonString(qm), headers);
		log.debug("Invoking query Execution with payload: " + Util.objectToJsonString(qm));
		ResponseEntity<String> response = this.restTemplate.exchange(this.activityUrl + address, HttpMethod.POST,
				entity, String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new ActivitiException("Error Activiti BPM return wrong status Code" + response.getStatusCode());
		}
		Map<String, String> mapS = new HashMap<>();
		mapS.put("executionId", Util.readJsonField(response.getBody(), "data"));
		mapS.put("activityId", Util.readJsonField(response.getBody(), "data", "activityId"));

		return mapS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.candelalabs.ws.services.activitirest.RestRequest#signalReceiveTask(
	 * java.lang.String)
	 */
	@Override
	public void signalReceiveTask(String executionId) throws ActivitiException {
		// TODO Auto-generated method stub
		String address = "runtime//executions//" + executionId;
		JsonObject oj = new JsonObject();
		oj.addProperty("action", "signal");
		HttpHeaders headers = Util.setHttpPostHeaders(activitiCred);
		HttpEntity<String> entity = new HttpEntity<String>(oj.toString(), headers);
		ResponseEntity<String> response = this.restTemplate.exchange(this.activityUrl + address, HttpMethod.PUT, entity,
				String.class);
		if ((response.getStatusCode() != HttpStatus.CREATED) && (response.getStatusCode() != HttpStatus.OK)
				&& (response.getStatusCode() != HttpStatus.NO_CONTENT)) {
			throw new ActivitiException("Error Activiti BPM return wrong status Code " + response.getStatusCode());
		}
	}


	@Override
	public void updateVariables(
			List<SignalDetailsVariables> liProcessInstanceVariablesModel,
			String processInstanceId) throws JsonProcessingException, ActivitiException {
		// TODO Auto-generated method stub
		String address = "runtime//process-instances//" + processInstanceId + "//variables";
		HttpHeaders headers = Util.setHttpPostHeaders(activitiCred);
		HttpEntity<String> entity = new HttpEntity<String>(Util.objectToJsonString(liProcessInstanceVariablesModel),
				headers);
		log.debug("Invoking updateVariables with payload: " + Util.objectToJsonString(liProcessInstanceVariablesModel));
		ResponseEntity<String> response = this.restTemplate.exchange(this.activityUrl + address, HttpMethod.PUT, entity,
				String.class);
		if ((response.getStatusCode() != HttpStatus.CREATED) && (response.getStatusCode() != HttpStatus.OK)) {
			throw new ActivitiException("Error Activiti BPM return wrong status Code " + response.getStatusCode());
		}
	}

}
