package com.candelalabs.ws.services.activitirest;

import java.util.List;
import java.util.Map;

import com.candelalabs.api.model.SignalDetailsVariables;
import com.candelalabs.api.model.WSRegisterRequirementDataIn;
import com.candelalabs.ws.exception.ActivitiException;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface RestRequest {
	Map<String, String> queryExecutionId(String processInstanceId, String activityId)
			throws JsonProcessingException, Exception;

	void signalReceiveTask(String executionId) throws ActivitiException;

	void updateVariables(List<SignalDetailsVariables> liProcessInstanceVariablesModel, String processInstanceId)
			throws JsonProcessingException, ActivitiException;
	
	
}
