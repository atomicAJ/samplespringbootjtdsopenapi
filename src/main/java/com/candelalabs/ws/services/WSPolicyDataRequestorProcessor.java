package com.candelalabs.ws.services;

import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.model.tables.Beneficiarydetails;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.CleintDetPolNoBased;
import com.candelalabs.ws.model.tables.ClientDetails;
import com.candelalabs.ws.model.tables.ComponentCode;
import com.candelalabs.ws.model.tables.POSDataModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.Util;
import com.candelalabs.api.model.JsonWSPolicyDataRequestorDataIn;
import com.candelalabs.api.model.JsonWSPolicyDataRequestorDataOut;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
// @Transactional
@PropertySource("classpath:application.properties")
public class WSPolicyDataRequestorProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSPolicyDataRequestorProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;

	@Autowired
	ODSDao odsDao;
	
	@Value("${15DINVESNOTIFICATION}")
	public String i15DINVESNOTIFICATION;
	
	@Value("${i30DINVESNOTIFICATION}")
	public String i30DINVESNOTIFICATION;
	
	@Value("${DOCUMENTSCOMPLETED}")
	public String DOCUMENTSCOMPLETED;
	
	/*@Value("${i30InvestigationNotificationDay}")
	public String i30InvestigationNotification;

	@Value("${i45InvestigationNotificationDay}")
	public String i45InvestigationNotification;

	@Value("${i60InvestigationNotificationDay}")
	public String i60InvestigationNotification;

	@Value("${INVES2CUSTNOTIFY}")
	public String INVES2CUSTNOTIFY;

	@Value("${INVES1CLUSERNOTIFY}")
	public String INVES1CLUSERNOTIFY;

	@Value("${INVES2CLUSERNOTIFY}")
	public String INVES2CLUSERNOTIFY;

	@Value("${DOCUMENTWAITPERIOD}")
	public String DOCUMENTWAITPERIOD;
*/
	public String strProductCode;

	public void process(Exchange exchange) throws Exception {

		log.info("WSPolicyDataRequestorProcessor - In Method - process - start of Method");

		Gson gson = new GsonBuilder().serializeNulls().create();
		// JsonWSPolicyDataRequestorDataIn jsonData = new
		// JsonWSPolicyDataRequestorDataIn();
		JsonWSPolicyDataRequestorDataOut jsonoutData = new JsonWSPolicyDataRequestorDataOut();
		try {

			// String jsonInString =
			// (exchange.getIn().getBody(WSPolicyDataRequestorRequestType.class).getWSInputObject());
			JsonWSPolicyDataRequestorDataIn jsonData = (exchange.getIn()
					.getBody(JsonWSPolicyDataRequestorDataIn.class));

			// jsonData = gson.fromJson(jsonInString,
			// JsonWSPolicyDataRequestorDataIn.class);
		
			PolicyDetails poldets = new PolicyDetails();
			ComponentCode compdata = new ComponentCode();
			ClaimDataNewModel claimNewData = new ClaimDataNewModel();
			POSDataModel posData = new POSDataModel();
			ClientDetails cltdet = new ClientDetails();
			if (null != jsonData) {
				//claimNewData = claimDetailsDao.getClaimData(jsonData.getClaimNo().toString());
				posData = claimDetailsDao.getPOSData(jsonData.getPoSRequestNumber().toString());
				
				poldets = odsDao.getPolicyDetail(jsonData.getPOLICYNUMBER().toString());

				String strInsuredClientID = posData.getInsuredClientID();
				log.info("strInsuredClientID is -" + strInsuredClientID);
				
				String strComponentCode = posData.getComponentCode();
				log.info("strComponentCode is -" + strComponentCode);
				
				String strInsuredClientName = posData.getInsuredClientName();
				log.info("strInsuredClientName is -" + strInsuredClientName);

				strProductCode = poldets.getPRODUCTCODE();
				log.info("strProductCode is -" + strProductCode);
				
				//int iLengthofStay = claimNewData.getStayDuration().intValue();
				
				String iSumAssured = odsDao.getSumAssured(jsonData.getPOLICYNUMBER().toString(), strInsuredClientID,
						strComponentCode);
				log.info("iSumAssured is -" + iSumAssured);
				
				compdata = odsDao.getComponentDetails(jsonData.getPOLICYNUMBER().toString(), strComponentCode,
						strInsuredClientID);
				
				// jsonData.getCLAIMTYPEUI()
				/*BigDecimal iClaimBillAmount = claimNewData.getClaimInvoiceAmount();
				log.info("iClaimBillAmount is -" + iClaimBillAmount);*/
				cltdet = odsDao.getClientDetails(strInsuredClientID);

				String bFlagAbuse = cltdet.getABUSEFLAG();
				log.info("bFlagAbuse is -" + bFlagAbuse);
				
				/*int iPendingClaimCount = 0;
				iPendingClaimCount = claimDetailsDao.getPendingClaimCount(jsonData.getClaimNo().toString(), "COMPLETE",
						jsonData.getPOLICYNUMBER().toString());
				log.info("iPendingClaimCount is -" + iPendingClaimCount);
				String bPendingClaims = null;
				if (iPendingClaimCount > 0)
					bPendingClaims = "YES";
				else
					bPendingClaims = "NO";*/
				
				String bFatcaFlag = cltdet.getFATCAFLAG();
				log.info("bFatcaFlag is -" + bFatcaFlag);
				/*Date dtClaimInvoiceDate = claimNewData.getReceivedDate();
				log.info("dtClaimInvoiceDate is -" + dtClaimInvoiceDate);*/
				Date dtPaidToDate = poldets.getPAIDTODATE();
				log.info("dtPaidToDate is -" + dtPaidToDate);
				
				String strPolicyStatus = poldets.getPOLICYSTATUS();
				log.info("strPolicyStatus is -" + strPolicyStatus);
				
				String strPremiumPolicyStatus = poldets.getPREMIUMSTATUSCODE();
				log.info("strPremiumPolicyStatus is -" + strPremiumPolicyStatus);
				
				String strComponentStatus = compdata.getCOMPONENTSTATUS();
				log.info("strComponentStatus is -" + strComponentStatus);
				
				String strComponentPremStatus = compdata.getCOMPPREMSTATUS();
				log.info("strComponentPremStatus is -" + strComponentPremStatus);
			//	String strPlanCode = claimNewData.getPlanCode();
				String strPlanCode =compdata.getPLANCODE(); 
				log.info("strPlanCode is -" + strPlanCode);
				log.info("strBatchNumber is -" + claimNewData.getBatchNumber());
				
				BigDecimal dblAmtAnnually = odsDao.getAMOUNTANNUALLY(strComponentCode, strPlanCode);
				if (dblAmtAnnually == null) {
					dblAmtAnnually = BigDecimal.ZERO;
				}
				log.info("dblAmtAnnually is -" + dblAmtAnnually.toString());
				BigDecimal dblTotalAmtUsed = odsDao.getTOTALAMOUNTUSED(strComponentCode,
						jsonData.getPOLICYNUMBER().toString(), strPlanCode);
				if (dblTotalAmtUsed == null) {
					dblTotalAmtUsed = BigDecimal.ZERO;
				}
				log.info("dblTotalAmtUsed is -" + dblTotalAmtUsed.toString());
				BigDecimal iBalanceLimit = dblAmtAnnually.subtract(dblTotalAmtUsed);
				log.info("iBalanceLimit is -" + iBalanceLimit.toString());

				String strPolicyCurrency = poldets.getCURRENCY();
				log.info("strPolicyCurrency is -" + strPolicyCurrency);
				
				String strCaseCurrency = posData.getClaimCurrency();
				log.info("strCaseCurrency is -" + strCaseCurrency);
				
				/*Date dtPolicyIssueDate = poldets.getPOLICYISSUEDATE();
				log.info("dtPolicyIssueDate is -" + dtPolicyIssueDate.toString());
				int iPolicyAge = 0;
				if (dtPolicyIssueDate == null) {
					iPolicyAge = 0;
				} else {
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					Date date = new Date();
					iPolicyAge = date.getMonth() - dtPolicyIssueDate.getMonth();
				}

				log.info("iPolicyAge is -" + iPolicyAge);*/

				/*Date dtDOD = claimNewData.getDischargeDate();
				log.info("dtDOD is -" + dtDOD.toString());
				//int iSAUnit = compdata.getUNIT();
				Integer iSAUnit = compdata.getUNIT();
				log.info("iSAUnit is -" + iSAUnit);
				String strCaseSurgicalBenefit = claimNewData.getSurgreryPerformed();*/

				String strClientStatus = cltdet.getClientStatus();
				log.info("strClientStatus is -" + strClientStatus);
				
				/*Date dtElapseDate = poldets.getLAPSEDATE(); // handle // handle
															// null pass empty
															// string
				if (dtElapseDate == null || dtElapseDate.equals(" ")) {
					dtElapseDate = null;
				}
				log.info("dtElapseDate is -" + dtElapseDate);
				Date dtSurrenderDate = poldets.getSURRENDERDATE(); // handle
																	// null pass
																	// empty
																	// string
				if (dtSurrenderDate == null || dtSurrenderDate.equals(" ")) {
					dtSurrenderDate = null;
				}
				log.info("dtSurrenderDate is -" + dtSurrenderDate);*/
/*
				String i60InvestigationNotificationDay = i60InvestigationNotification;
				String i45InvestigationNotificationDay = i45InvestigationNotification;
				String i30InvestigationNotificationDay = i30InvestigationNotification;

				log.info(
						"i60InvestigationNotificationDay,i45InvestigationNotificationDay,i30InvestigationNotificationDay  is -"
								+ i60InvestigationNotificationDay + " " + i45InvestigationNotificationDay + " "
								+ i30InvestigationNotificationDay);*/
				String i15DINVESNOTIFICATIONDay= i15DINVESNOTIFICATION;
				String i30DINVESNOTIFICATIONDay=i30DINVESNOTIFICATION;
				log.info(
						"i15DINVESNOTIFICATION,i30DINVESNOTIFICATION is -"
								+ i15DINVESNOTIFICATION + " "
								+ i30DINVESNOTIFICATION);
				
				/*String strClaimTypeUI = claimNewData.getClaimTypeUI();
				String strClaimTypeLA = claimNewData.getClaimTypeLA();*/
				
				String strPosInitiated = "NO";
				/*String strInProgressClaimNo = claimDetailsDao.getInProgressClaimNo(jsonData.getClaimNo(), "COMPLETE",
						jsonData.getPOLICYNUMBER());*/
				String strInProgressClaimNo = claimDetailsDao.getInProgressPOSRequestNo(jsonData.getPoSRequestNumber(), "COMPLETE",
						jsonData.getPOLICYNUMBER());
				
				log.info("strInProgressClaimNo is +" + strInProgressClaimNo);
				
			/*	String iNotifyCustomer2Investigation = INVES2CUSTNOTIFY;
				String iNotifyCLUser1Investigation = INVES1CLUSERNOTIFY;
				String iNotifyCLUser2Investigation = INVES2CLUSERNOTIFY;
				String iDocumentWaitPeriod = DOCUMENTWAITPERIOD;
				log.info("iNotifyCustomer2Investigation,iNotifyCLUser1Investigation,iNotifyCLUser2Investigation  is -"
						+ iNotifyCustomer2Investigation + " " + iNotifyCLUser1Investigation + " "
						+ iNotifyCLUser2Investigation);*/

			//	String strTPACLAIMNUMBER = claimNewData.getTPAClaimNumber();
				// String strInsuredClientID =
				// claimNewData.getInsuredClientID();
				String strOwnerClientID = posData.getOwnerClientID();

				/*if (strClaimTypeUI.equalsIgnoreCase("HS")) {
					log.info("in if strClaimTypeUI = HS");
					BigDecimal iClaimApprovedAmtTPA = claimNewData.getClaimApprovedAmountTPA();
					log.info("b4 updating ClaimApprovedAmountUI");
					claimDetailsDao.updateClaimApprovedAmountUI(iClaimApprovedAmtTPA, jsonData.getClaimNo());
					log.info("after updating ClaimApprovedAmountUI");
				}*/
			//	String strClaimNumber = jsonData.getClaimNo();
				String strPoSRequestNumbe = jsonData.getPoSRequestNumber();
				String strPolicyNumber = jsonData.getPOLICYNUMBER();

				/*BigDecimal iClaimUITotalClaimAmount = claimNewData.getClaimApprovedAmountUI();
				if (iClaimUITotalClaimAmount == null) {
					iClaimUITotalClaimAmount = BigDecimal.ZERO;
				}*/
				String strPolicyOwnerName = poldets.getOWNERNAME();
				log.info("strPolicyOwnerName is - "+ strPolicyOwnerName);
				CleintDetPolNoBased det = new CleintDetPolNoBased();
				det = null;
				log.info("b4 getCommdetails - PolicyNumber Value = " + jsonData.getPOLICYNUMBER());
				// det=odsDao.getCommdetails(jsonData.getPOLICYNUMBER());
				det = odsDao.getClientDetailsData(strOwnerClientID);
				String strPolicyOwnerAddress1 = "";
				String strPolicyOwnerAddress2 = "";
				String strPolicyOwnerAddress3 = "";
				String strPolicyOwnerAddress4 = "";
				String strPolicyOwnerAddress5 = "";
				String strPolicyOwnerEmailAddress = "";
				String strPolicyOwnerPhoneNumber = "";

				log.info("after getCommdetails");
				if (det != null) {
					//strPolicyOwnerAddress1 = det.getPolicyOwnerAddress1();
					strPolicyOwnerAddress1 = det.getADDRESS1();
					if (strPolicyOwnerAddress1 == null || strPolicyOwnerAddress1.trim().isEmpty()) {
						strPolicyOwnerAddress1 = "";
					}
					log.info("strPolicyOwnerAddress1 is -" + strPolicyOwnerAddress1);

					strPolicyOwnerAddress2 = det.getADDRESS2();
					if (strPolicyOwnerAddress2 == null || strPolicyOwnerAddress2.trim().isEmpty()) {
						strPolicyOwnerAddress2 = "";
					}
					log.info("strPolicyOwnerAddress2 is -" + strPolicyOwnerAddress2);
					strPolicyOwnerAddress3 = det.getADDRESS3();
					if (strPolicyOwnerAddress3 == null || strPolicyOwnerAddress3.trim().isEmpty()) {
						strPolicyOwnerAddress3 = "";
					}
					log.info("strPolicyOwnerAddress3 is -" + strPolicyOwnerAddress3);
					strPolicyOwnerAddress4 = det.getADDRESS4();
					if (strPolicyOwnerAddress4 == null || strPolicyOwnerAddress4.trim().isEmpty()) {
						strPolicyOwnerAddress4 = "";
					}
					log.info("strPolicyOwnerAddress4 is -" + strPolicyOwnerAddress4);
					strPolicyOwnerAddress5 =det.getADDRESS5();
					if (strPolicyOwnerAddress5 == null || strPolicyOwnerAddress5.trim().isEmpty()) {
						strPolicyOwnerAddress5 = "";
					}
					log.info("strPolicyOwnerAddress5 is -" + strPolicyOwnerAddress5);
					strPolicyOwnerEmailAddress = det.getEMAILADDRESS();
					if (strPolicyOwnerEmailAddress == null || strPolicyOwnerEmailAddress.trim().isEmpty()) {
						strPolicyOwnerEmailAddress = "";
					}
					log.info("strPolicyOwnerEmailAddress is -" + strPolicyOwnerEmailAddress);
					
					if (!(det.getPHONENO01() == null) && !(det.getPHONENO01().trim().isEmpty())) {
						strPolicyOwnerPhoneNumber = det.getPHONENO01();
						log.info("getPHONENO01 is -" + strPolicyOwnerPhoneNumber);
					}

					else if (!(det.getPHONENO02() == null) && !(det.getPHONENO02().trim().isEmpty())) {
						strPolicyOwnerPhoneNumber = det.getPHONENO02();
						log.info("getPHONENO02 is -" + strPolicyOwnerPhoneNumber);
					} else if (!(det.getPHONENO03() == null) && !(det.getPHONENO03().trim().isEmpty())) {
						strPolicyOwnerPhoneNumber = det.getPHONENO03();
						log.info("getPHONENO03 is -" + strPolicyOwnerPhoneNumber);
					}
				}
				log.info("b4 fetching Beneficiarydetails");
				Beneficiarydetails bendet = odsDao.getTopClntNameNO(jsonData.getPOLICYNUMBER()); /// handle
				log.info("after fetching Beneficiarydetails");

				String strBeneficiaryName = "";
				String strBeneficiaryClientNo = "";
				if (bendet != null) {
					strBeneficiaryName = bendet.getCLIENTNAME();
					log.info("strBeneficiaryName is -" + strBeneficiaryName);

					strBeneficiaryClientNo = bendet.getCLIENTNO();
					log.info("strBeneficiaryClientNo is -" + strBeneficiaryClientNo);
				}
				log.info("b4 fetching BenCltAddDetails");
				CleintDetPolNoBased bencltdet = new CleintDetPolNoBased();

				bencltdet = odsDao.getClientDetailsData(strBeneficiaryClientNo);
				log.info("after fetching BenCltAddDetails");
				String strBeneficiaryAddress1 = "";
				String strBeneficiaryAddress2 = "";
				String strBeneficiaryAddress3 = "";
				String strBeneficiaryAddress4 = "";
				String strBeneficiaryAddress5 = "";

				if (bencltdet != null) {
					
					if (bencltdet.getADDRESS1() == null || bencltdet.getADDRESS1().trim() == "") {
						strBeneficiaryAddress1 = "";
					} else {
						strBeneficiaryAddress1 = bencltdet.getADDRESS1().toString();
					}
					log.info("ben1 ads fetched");
					// String strBeneficiaryAddress2 =
					// odsDao.getAddress2(strBeneficiaryClientNo) ;
					
					if (bencltdet.getADDRESS2() == null || bencltdet.getADDRESS2().trim() == "") {
						strBeneficiaryAddress2 = "";
					} else {
						strBeneficiaryAddress2 = bencltdet.getADDRESS2().toString();
					}
					log.info("ben2 ads fetched");
					// String strBeneficiaryAddress3 =
					// odsDao.getAddress3(strBeneficiaryClientNo) ;
					
					if (bencltdet.getADDRESS3() == null || bencltdet.getADDRESS3().trim() == "") {
						strBeneficiaryAddress3 = "";
					} else {
						strBeneficiaryAddress3 = bencltdet.getADDRESS3().toString();
					}
					log.info("ben3 ads fetched");
					// String strBeneficiaryAddress4 =
					// odsDao.getAddress4(strBeneficiaryClientNo) ;
					
					if (bencltdet.getADDRESS4() == null || bencltdet.getADDRESS4().trim() == "") {
						strBeneficiaryAddress4 = "";
					} else {
						strBeneficiaryAddress4 = bencltdet.getADDRESS4().toString();
					}
					log.info("ben4 ads fetched");
					// String strBeneficiaryAddress5 =
					// odsDao.getAddress5(strBeneficiaryClientNo) ;
					
					if (bencltdet.getADDRESS5() == null || bencltdet.getADDRESS5().trim() == "") {
						strBeneficiaryAddress5 = "";
					} else {
						strBeneficiaryAddress5 = bencltdet.getADDRESS5().toString();
					}
					log.info("ben5 ads fetched");
				}
				// String strBeneficiaryAddress1 =
				// odsDao.getAddress1(strBeneficiaryClientNo) ;

				//Date dtClaimProcessInitiationDate = claimNewData.getProcessInitiatonTS();
			//	Timestamp dtClaimProcessInitiationDate = claimNewData.getProcessInitiatonTS();
				Timestamp dtClaimProcessInitiationDate = posData.getProcessInitiatonTS();
				log.info("dtClaimProcessInitiationDate is -" + dtClaimProcessInitiationDate);

				BigDecimal dblPolicyRegularTopup = poldets.getREGULARTOPUP();
				if (dblPolicyRegularTopup == null) {
					dblPolicyRegularTopup = BigDecimal.ZERO;
				}
				log.info("dblPolicyRegularTopup is -" + dblPolicyRegularTopup.toString());

				String strPolicyPaymentFrequency = poldets.getPAYMENTFREQ();
				log.info("strPolicyPaymentFrequency is - " + strPolicyPaymentFrequency);

				String strAgentEmailAddress = odsDao.getAgentEmailAddress(jsonData.getPOLICYNUMBER());
				log.info("strAgentEmailAddress is - " + strAgentEmailAddress);

				String strSDEmailAddress = odsDao.getSDEmailAddress(jsonData.getPOLICYNUMBER());
				log.info("strSDEmailAddress is - " + strSDEmailAddress);

				String strNSDEmailAddress = odsDao.getNSDEmailAddress(jsonData.getPOLICYNUMBER());
				log.info("strNSDEmailAddress is - " + strNSDEmailAddress);
				/// additional field added on 23rd May 2019
				String strApplicationNumber = "";
				strApplicationNumber=jsonData.getApplicationNumber();

				/*String claimTypeLAName = "";
				if (claimNewData.getClaimTypeLAName() != null) {
					claimTypeLAName = claimNewData.getClaimTypeLAName();
				}*/

				/*int iRecordCount = claimDetailsDao.countRecords(jsonData.getClaimNo());*/
				int iRecordCount = claimDetailsDao.countRecordsbasedOnPOSReqNo(jsonData.getPoSRequestNumber());
				log.info("iRecordCount is -" + iRecordCount);
				Util util = new Util();
				if (iRecordCount == 0) {
					log.info("inside if iRecordCount==0 - b4 insert");

					claimDetailsDao.insertNotificationData(util.checknull(jsonData.getPoSRequestNumber().toString()),
							util.checknull(jsonData.getPOLICYNUMBER().toString()),
							
							util.checknull(strPlanCode),
							//util.checknull(strClaimTypeLA),
							"NULL",
							util.checknull(strInsuredClientName),
							util.checknull(strInsuredClientID), util.checknull(strProductCode),
							//iClaimUITotalClaimAmount, // change to bigdecimal
							BigDecimal.ZERO,
							util.checknull(strPolicyOwnerName), util.checknull(strPolicyOwnerAddress1),
							util.checknull(strPolicyOwnerAddress2), util.checknull(strPolicyOwnerAddress3),
							util.checknull(strPolicyOwnerAddress4), util.checknull(strPolicyOwnerAddress5),
							util.checknull(strPolicyOwnerEmailAddress), util.checknull(strPolicyOwnerPhoneNumber),
							util.checknull(strBeneficiaryName), util.checknull(strBeneficiaryAddress1),
							util.checknull(strBeneficiaryAddress2), util.checknull(strBeneficiaryAddress3),
							util.checknull(strBeneficiaryAddress4), util.checknull(strBeneficiaryAddress5),
							dtClaimProcessInitiationDate, dblPolicyRegularTopup, // chnage
																					// to
																					// bigdecimal
							util.checknull(strPolicyPaymentFrequency), util.checknull(strAgentEmailAddress),
							util.checknull(strSDEmailAddress), util.checknull(strNSDEmailAddress),util.checknull(strApplicationNumber),
							//util.checknull(claimTypeLAName)
							"NULL"
							);
					log.info("inside if iRecordCount==0 - after insert");
				} 
				else {
					log.info("inside else  - b4 update");
					claimDetailsDao.updateNotificationDataFromfetchPol(util.checknull(jsonData.getPOLICYNUMBER()),
							util.checknull(strPlanCode), 
							//util.checknull(strClaimTypeLA),
							"",
							util.checknull(strInsuredClientName), util.checknull(strInsuredClientID),
							util.checknull(strProductCode), 
							//iClaimUITotalClaimAmount,
							BigDecimal.ZERO,
							util.checknull(strPolicyOwnerName), util.checknull(strPolicyOwnerAddress1),
							util.checknull(strPolicyOwnerAddress2), util.checknull(strPolicyOwnerAddress3),
							util.checknull(strPolicyOwnerAddress4), util.checknull(strPolicyOwnerAddress5),
							util.checknull(strPolicyOwnerEmailAddress), util.checknull(strPolicyOwnerPhoneNumber),
							util.checknull(strBeneficiaryName), util.checknull(strBeneficiaryAddress1),
							util.checknull(strBeneficiaryAddress2), util.checknull(strBeneficiaryAddress3),
							util.checknull(strBeneficiaryAddress4), util.checknull(strBeneficiaryAddress5),
							dtClaimProcessInitiationDate, dblPolicyRegularTopup,
							util.checknull(strPolicyPaymentFrequency), util.checknull(strAgentEmailAddress),
							util.checknull(strSDEmailAddress), util.checknull(strNSDEmailAddress),
							//util.checknull(jsonData.getClaimNo()),
							util.checknull(jsonData.getPoSRequestNumber()),
							util.checknull(strApplicationNumber),
							//util.checknull(claimTypeLAName)
							""
							);
					log.info("inside else  - after update");
				}
				//jsonoutData.setCLAIMNUMBER(jsonData.getClaimNo());
				jsonoutData.setPoSRequestNumber(jsonData.getPoSRequestNumber());
				jsonoutData.setPOLICYNUMBER(jsonData.getPOLICYNUMBER());
				jsonoutData.setProductcode(strProductCode);
				jsonoutData.setCOMPONENTCODE(strComponentCode);
			//	jsonoutData.setLENGTHOFSTAY(String.valueOf(iLengthofStay));
				jsonoutData.setLENGTHOFSTAY("");
				jsonoutData.setSUMASSURED(String.valueOf(iSumAssured));
				jsonoutData.setPLAN(strPlanCode);
				//jsonoutData.setCLAIMBILLAMOUNT(String.valueOf(iClaimBillAmount));
				jsonoutData.setCLAIMBILLAMOUNT("");
				jsonoutData.setFLAGABUSE(bFlagAbuse);
				//jsonoutData.setPENDINGCLAIMS(bPendingClaims);
				jsonoutData.setPENDINGCLAIMS("");
				jsonoutData.setFATCAFLAG(bFatcaFlag);
				//jsonoutData.setCLAIMINVOICEDATE(String.valueOf(dtClaimInvoiceDate));
				jsonoutData.setCLAIMINVOICEDATE("");
				jsonoutData.setPAIDTODATE(String.valueOf(dtPaidToDate));
				jsonoutData.setPOLICYSTATUS(strPolicyStatus);
				jsonoutData.setPREMIUMPOLICYSTATUS(strPremiumPolicyStatus);
				jsonoutData.setCOMPONENTSTATUS(strComponentStatus);
				jsonoutData.setPREMIUMCOMPONENTSTATUS(strComponentPremStatus);
				jsonoutData.setBALANCELIMIT(String.valueOf(iBalanceLimit));
				jsonoutData.setPOLICYCURRENCY(strPolicyCurrency);
				jsonoutData.setCASECURRENCY(strCaseCurrency);
				//jsonoutData.setPOLICYAGE(String.valueOf(iPolicyAge));
				jsonoutData.setPOLICYAGE("");
				/*jsonoutData.setDATEOFDEATH(String.valueOf(dtDOD));
				jsonoutData.setSAUNIT(String.valueOf(iSAUnit));
				jsonoutData.setCASESURGICALBENEFIT(strCaseSurgicalBenefit);*/
				jsonoutData.setDATEOFDEATH("");
				jsonoutData.setSAUNIT("");
				jsonoutData.setCASESURGICALBENEFIT("");
				jsonoutData.setCLIENTSTATUS(strClientStatus);
				/*jsonoutData.setELAPSEDATE(String.valueOf(dtElapseDate));
				jsonoutData.SURRENDERDATE(String.valueOf(dtSurrenderDate));*/
				jsonoutData.setELAPSEDATE("");
				jsonoutData.SURRENDERDATE("");
				jsonoutData.set30DINVESNOTIFICATION("");
				jsonoutData.set45DINVESNOTIFICATION("");
				jsonoutData.set60DINVESNOTIFICATION("");
				/*jsonoutData.setCLAIMTYPEUI(strClaimTypeUI);
				jsonoutData.setCLAIMTYPELA(strClaimTypeLA);*/
				jsonoutData.setELAPSEDATE("");
				jsonoutData.SURRENDERDATE("");
				jsonoutData.setPOSINITIATED(strPosInitiated);
				jsonoutData.setINPROGRESSCLAIMNO(strInProgressClaimNo);
				jsonoutData.setInVES1CLUSERNOTIFY("");
				jsonoutData.setInVES2CLUSERNOTIFY("");
				jsonoutData.setInVES2CUSTNOTIFY("");
				jsonoutData.DOCUMENTWAITPERIOD("");
				//jsonoutData.setTPACLAIMNUMBER(strTPACLAIMNUMBER);
				jsonoutData.setTPACLAIMNUMBER("");
				jsonoutData.setINSUREDCLIENTID(strInsuredClientID);
				jsonoutData.setOWNERCLIENTID(strOwnerClientID);
				//jsonoutData.setBatchNumber(claimNewData.getBatchNumber());
				jsonoutData.setBatchNumber(posData.getScanRequestNumber());
				jsonoutData.setPortalClaimRequestNumber(posData.getPortalRequestNumber());
				jsonoutData.setI15DINVESNOTIFICATION(i15DINVESNOTIFICATIONDay);
				jsonoutData.setI30DINVESNOTIFICATION(i15DINVESNOTIFICATIONDay);
			}

			log.info("WSPolicyDataRequestorProcessor - In Method - process - getBody");
			jsonoutData.setWsExceptionMessage("");
			jsonoutData.setWsProcessingStatus("1");
			jsonoutData.setWsSuccessMessage("Data Fetched");

			log.info("WSPolicyDataRequestorProcessor jsonoutData.setWsProcessingStatus is " + "1");
			log.info("WSPolicyDataRequestorProcessor jsonoutData.setWsExceptionMessage is null " + null);
			log.info("WSPolicyDataRequestorProcessor jsonoutData.setWsSuccessMessage is " + "Data Fetched");

		} catch (Exception e) {
			log.error(
					"WSPolicyDataRequestorProcessor - Error - In Method - process - Exception please check application logs.");
			log.error(e.fillInStackTrace());
			/*
			 * jsonoutData.setProcessmsg("Failure" + e);
			 * jsonoutData.setDecision("ERROR RESPONSE"
			 */
			jsonoutData.setWsExceptionMessage("Failure" + e);
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsSuccessMessage(null);
			log.error("WSPolicyDataRequestorProcessor jsonoutData.setWsProcessingStatus is " + "2");
			log.error("WSPolicyDataRequestorProcessor jsonoutData.setWsExceptionMessage is null " + "Failure" + e);
			log.error("WSPolicyDataRequestorProcessor jsonoutData.setWsSuccessMessage is null");

		}
		String json = gson.toJson(jsonoutData);
		log.info("JSON RESPONSE PAYLOAD = " + jsonoutData.toString());
		// WSPolicyDataRequestorResponseType requirementResponse = new
		// WSPolicyDataRequestorResponseType();
		// requirementResponse.setWSOutputObject(json);
		exchange.getOut().setBody(jsonoutData);

	}

}
