package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDULAdjClaimAmtUserDecisionDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.*;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.*;
import com.candelalabs.ws.model.bomessages.WSDULAdjClaimAmtUserDecisionMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.LaRequestTrackerModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;

@Component
@PropertySource("classpath:application.properties")
public class WSDULAdjClaimAmtUserDecisionProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao odsDao;

	@Value("${USRPRF}")
	public String USRPRF; // reading USRPRF's value from
							// "classpath:application.properties" file
	private static Log log = LogFactory.getLog("WSDULAdjClaimAmtUserDecisionProcessor");// .getLog(WSUpdateClaimStatusProcessor.class);
	// private static Log log =
	// LogFactory.getLog("WSUpdateClaimStatusProcessor");
	// below working config
	// private static final Logger log =
	// LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());
	@Autowired
	private ClaimDetailsDao lARequestTracker_Insert;

	int updateSuccess = 0;

	public void process(Exchange exchange) throws Exception {

		log.info(" WSDULAdjClaimAmtUserDecisionProcessor -- testing log");
		log.info(" WSDULAdjClaimAmtUserDecisionProcessor.class.getName() --"
				+ WSDULAdjClaimAmtUserDecisionProcessor.class.getName());
		log.info(" WSDULActualAmountProcessor.class.getName() --"
				+ WSDULAdjClaimAmtUserDecisionProcessor.class.toString());
		System.out.println("WSDULAdjClaimAmtUserDecisionProcessor -- testing console log");
		System.out.println(WSDULAdjClaimAmtUserDecisionProcessor.class.getName());
		System.out.println(WSDULAdjClaimAmtUserDecisionProcessor.class.toString());

		Gson gson = new GsonBuilder().serializeNulls().create();
		WSDULAdjClaimAmtUserDecisionDataIn jsonData = new WSDULAdjClaimAmtUserDecisionDataIn(); // for
																								// input
																								// ->
																								// jsonData
		JsonResponse jsonoutData = new JsonResponse(); // for output ->
														// jsonoutData
		try {
			jsonData = (exchange.getIn().getBody(WSDULAdjClaimAmtUserDecisionDataIn.class));
			PaddingUtil pdngutl = new PaddingUtil();
			if (null != jsonData) {
				// log.info("Get LaRequestTrackerModel table with claimNo= " +
				// jsonData.getCLAIMNO());
				// LaRequestTrackerModel laRequestTrackerData =
				// this.claimDetailsDao.getLaRequestTrackerByClaimNo(
				// jsonData.getCLAIMNO(), jsonData.getACITVITYID(),
				// FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAdjustment);
				// log.info("Successfully Got LaRequestTrackerModel table ");
				//
				// if (laRequestTrackerData.getBoidentifier() == null &&
				// laRequestTrackerData.getMsgtosend() == null
				// && laRequestTrackerData.getMsgcreatets() == null
				// && laRequestTrackerData.getFuturemesg() == null) {
				WSDULAdjClaimAmtUserDecisionMessageModel boMessage = new WSDULAdjClaimAmtUserDecisionMessageModel();
				// jsonData.PAYCLT =
				ClaimDataNewModel claimsData = new ClaimDataNewModel();
				claimsData = claimDetailsDao.getClaimDataNew(jsonData.getCLAIMNO());
				// PolicyDetails policyData = new PolicyDetails();
				PolicyDetails polDet = new PolicyDetails();
				polDet = odsDao.getPolicyDetail(claimsData.getPolicyNumber());
				log.info(" WSDULAdjClaimAmtUserDecisionProcessor -- CHDRCOY is --" + polDet.getCOMPANY());

				//polDet = odsDao.getPayeeNumber(claimsData.getPolicyNumber());
				String payee_no = polDet.getPAYORCLIENTNO();
				log.info(" WSDULAdjClaimAmtUserDecisionProcessor -- payee_no is --" + payee_no);
				log.info(" WSDULAdjClaimAmtUserDecisionProcessor -- USRPRF is --" + USRPRF);

				boMessage.setUSRPRF(this.USRPRF);
				log.debug("USRPRF = "+boMessage.getUSRPRF());
				boMessage.setCHDRCOY(polDet.getCOMPANY());
				log.debug("CHDRCOY = "+boMessage.getCHDRCOY());
				boMessage.setCHDRSEL(claimsData.getPolicyNumber());
				log.debug("CHDRSEL = "+boMessage.getCHDRSEL());
				boMessage.setCLAIMNO(jsonData.getCLAIMNO());
				log.debug("CLAIMNO = "+boMessage.getCLAIMNO());
				boMessage.setOTHERADJST(claimsData.getOtherAdjustments() == null ? 0.0
						: claimsData.getOtherAdjustments().doubleValue());
				log.debug("OTHERADJST= "+boMessage.getOTHERADJST());
				boMessage.setZDIAGCDE(claimsData.getDiagnosisCode());
				log.debug("ZDIAGCDE= "+boMessage.getZDIAGCDE());
				boMessage.setBANKACOUNT(claimsData.getBankAccountNumber());
				log.debug("BANKACCOUNT = "+boMessage.getBANKACOUNT());
				boMessage.setBANKKEY(claimsData.getBankCode());
				log.debug("BANKKEY= "+boMessage.getBANKKEY());
				boMessage.setPAYCLT(claimsData.getOwnerClientID());
				log.debug("PAYCLT= "+boMessage.getPAYCLT());
				boMessage.setFACTHOUS(Integer.valueOf(claimsData.getFactoringHouse()));
				log.debug("FACTHOUS= "+boMessage.getFACTHOUS());
				boMessage.setBANKACCDSC(claimsData.getBankAccountName());
				log.debug("BANKACCDSC= "+boMessage.getBANKACCDSC());
				boMessage.setBNKACTYP(claimsData.getBankType());
				log.debug("BNKACTYPE: "+boMessage.getBNKACTYP());
				boMessage.setDATEFROM(Util.dateToString(claimsData.getCurrentFrom(), "yyyyMMdd"));
				boMessage.setDATETO("");
				log.debug("DATEFROM= "+boMessage.getDATEFROM()+", DATETO= "+boMessage.getDATETO());

				;
				log.info(" WSDULAdjClaimAmtUserDecisionProcessor -- msgtosend is --" + boMessage.getBOMessage());
				int result = claimDetailsDao.lARequestTracker_Insert(jsonData.getCLAIMNO().toString(),
						jsonData.getACITVITYID().toString(),
						FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAdjustment, boMessage.getBOMessage(), "0");
				// dULActualAmount(jsonData,msgtosend);

				if (result == 0) {
					throw new DAOException("No Updated row for LARequestTracker Table");
				}

				// }

				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");
				/*
				 * jsonoutData.setUSRPRF(pdngutl.padRight(jsonData.getUSRPRF().
				 * toString(),9)); jsonoutData.setCHDRCOY(pdngutl.padZero
				 * (Integer.parseInt(jsonData.getCHDRCOY().toString()), 1));
				 * jsonoutData.setBOIDEN(pdngutl.padRight(jsonData.getBOIDEN().
				 * toString(),20));
				 * jsonoutData.setMSGLNG(pdngutl.padRight(jsonData.getMSGLNG().
				 * toString(), 1)); jsonoutData.setMSGCNT(pdngutl.padZero
				 * (Integer.parseInt(jsonData.getMSGCNT().toString()), 5));
				 * jsonoutData.setINDC(pdngutl.padRight(jsonData.getINDC().
				 * toString(), 1));
				 * jsonoutData.setCLAIMNO(pdngutl.padRight(jsonData.getCLAIMNO()
				 * .toString(), 11));
				 * jsonoutData.setCHDRNUM(pdngutl.padRight(jsonData.getCHDRNUM()
				 * .toString(), 8));
				 */

			} else
				throw new Exception("Input String is Empty");

			log.info("WSDULAdjClaimAmtUserDecisionProcessor - In Method - process - getBody");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR DETAIL: " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			jsonoutData.setWsProcessingStatus("2");
			jsonoutData.setWsExceptionMessage("Error " + e.getMessage());
			// jsonoutData.setSOURCEEXCEPTIONPROCESS("Null");
			// jsonoutData.setSOURCEEXCEPTIONSTEP("Null");

		}

		String json = gson.toJson(jsonoutData);
		log.info("WSDULAdjClaimAmtUserDecisionProcessor - In Method - process - jsonOut= " + json);
		exchange.getOut().setBody(jsonoutData);

	}

}
