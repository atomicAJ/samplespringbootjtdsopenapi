/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataIn;
import com.candelalabs.api.model.WSDNULAdjClaimAmtUserDecisionDataOut;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.bomessages.WSDNULAdjClaimMessageModel;
import com.candelalabs.ws.model.tables.ClaimDataDetailsModel;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;

/**
 * @author Triaji
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class WSDNULAdjClaimAmtUserDecisionProcessor implements Processor {

	private static Log log = LogFactory.getLog("WSDNULAdjClaimAmtUserDecisionProcessor");

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao oDSDao;

	@Value("${USRPRF}")
	public String USRPRF;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		log.info("Service is invoked");
		WSDNULAdjClaimAmtUserDecisionDataOut jsonObjOut = new WSDNULAdjClaimAmtUserDecisionDataOut();
		try {
			WSDNULAdjClaimAmtUserDecisionDataIn jsonObjIn = (exchange.getIn()
					.getBody(WSDNULAdjClaimAmtUserDecisionDataIn.class));
			log.info("Json Input " + jsonObjIn.toString());
			if (jsonObjIn.getACTIVITYID() != null && jsonObjIn.getBOIDEN() != null && jsonObjIn.getCHDRSEL() != null
					&& jsonObjIn.getCLAIMNO() != null && jsonObjIn.getACTIVITYID() != "" && jsonObjIn.getBOIDEN() != ""
					&& jsonObjIn.getCHDRSEL() != "" && jsonObjIn.getCLAIMNO() != "") {

				// log.info("Get LaRequestTrackerModel table with claimNo= " +
				// jsonObjIn.getCLAIMNO());
				// LaRequestTrackerModel laRequestTrackerData =
				// this.claimDetailsDao
				// .getLaRequestTrackerByClaimNo(jsonObjIn.getCLAIMNO(),
				// jsonObjIn.getACTIVITYID(),
				// FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimAdjustment);
				// log.info("Successfully Got LaRequestTrackerModel table ");
				// if (laRequestTrackerData.getBoidentifier() == null &&
				// laRequestTrackerData.getMsgtosend() == null
				// && laRequestTrackerData.getMsgcreatets() == null
				// && laRequestTrackerData.getFuturemesg() == null) {

				WSDNULAdjClaimMessageModel messageModel = new WSDNULAdjClaimMessageModel();
				log.info("Getting the claimData table");
				ClaimDataNewModel claimsData = claimDetailsDao.getClaimDataNew(jsonObjIn.getCLAIMNO());
				log.info("Successfully got the ClaimData table");
				log.info("Processing for claimNo= " + jsonObjIn.getCLAIMNO());

				PaddingUtil paddingUtil = new PaddingUtil();
				log.info("Getting the policyDetail with polNum = " + claimsData.getPolicyNumber());
				PolicyDetails polDet = new PolicyDetails();
				polDet = oDSDao.getPolicyDetail(claimsData.getPolicyNumber());
				log.info("Successfully got the PolicyDetail table");
				log.info("Get claimDataDetails table with claimNo= " + jsonObjIn.getCLAIMNO());
				List<ClaimDataDetailsModel> claimDataDetails_li = new ArrayList<ClaimDataDetailsModel>();
				claimDataDetails_li = this.claimDetailsDao.getClaimDataDetailsByClaimNo(jsonObjIn.getCLAIMNO());
				ClaimDataDetailsModel claimDataDetails = claimDataDetails_li.get(0);
				log.info("Successfully retrieved claimDataDetails table");
				if (!(Optional.ofNullable(claimDataDetails).isPresent())) {
					throw new DAOException("ClaimDataDetails is null with claimNo= " + jsonObjIn.getCLAIMNO());
				}
				;

				messageModel.setUSRPRF(this.USRPRF);
				log.debug("USRPRF = " + this.USRPRF);
				messageModel.setCHDRCOY(polDet.getCOMPANY());
				log.debug("CHDRCOY = " + messageModel.getCHDRCOY());
				messageModel.setBOIDEN(FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimAdjustment);
				log.debug("BOIDEN = "+messageModel.getBOIDEN());
				messageModel.setMSGLNG(FWDConstants.MESSAGE_LANGUAGE);
				log.debug("LANGUAGE = "+messageModel.getMSGLNG());
				messageModel.setMSGCNT(FWDConstants.BO_NonUnitLink_DeathClaimAdjustment_MESSAGE_LENGTH);
				log.debug("Message Length = "+messageModel.getMSGCNT());
				messageModel.setINDC(FWDConstants.MORE_INDICATOR);
				log.debug("More Indicator = "+messageModel.getINDC());
				messageModel.setCHDRSEL(claimsData.getPolicyNumber());
				log.debug("CHDRSEL = "+messageModel.getCHDRSEL());
				messageModel.setCLAIMNO(jsonObjIn.getCLAIMNO());
				log.debug("ClaimNo = "+messageModel.getCLAIMNO());
				messageModel.setOTHERADJST(claimsData.getOtherAdjustments() == null ? 0.0
						: claimsData.getOtherAdjustments().doubleValue()); // Pending
				log.debug("OTHERADJST = "+ messageModel.getOTHERADJST());
				messageModel.setZDIAGCDE(claimsData.getDiagnosisCode());
				log.debug("ZDIAGCDE = "+messageModel.getZDIAGCDE());
				messageModel.setBANKKEY(claimsData.getBankCode());
				log.debug("BANKKEY = "+messageModel.getBANKKEY());
				messageModel.setBANKACOUNT(claimsData.getBankAccountNumber());
				log.debug("BANK ACCOUNT = "+messageModel.getBANKACOUNT());
				messageModel.setPAYCLT(claimsData.getOwnerClientID());
				log.debug("PAYCLT = "+messageModel.getPAYCLT());
				messageModel.setFACTHOUS(claimsData.getFactoringHouse());
				log.debug("FACTHOUS = "+messageModel.getFACTHOUS());
				messageModel.setBANKACCDSC(claimsData.getBankAccountName());
				log.debug("BANKACCDSC = "+messageModel.getBANKACCDSC());
				messageModel.setBNKACTYP(claimsData.getBankType());
				log.debug("BNKACTYP =  "+messageModel.getBNKACTYP());
				messageModel.setDATEFROM(Util.dateToString(claimsData.getCurrentFrom(), "yyyMMdd"));
				messageModel.setDATETO("");
				log.debug("DATEFROM = "+messageModel.getDATEFROM() + ", DATETO = "+messageModel.getDATETO());

				log.info("Message to be sent =" + messageModel.getLifeAsiaMessage());
				log.info("Inserting the data to LAREQUESTTRACKER table");
				int result = claimDetailsDao.lARequestTracker_Insert(jsonObjIn.getCLAIMNO().toString(),
						jsonObjIn.getACTIVITYID().toString(), jsonObjIn.getBOIDEN().toString(),
						messageModel.getLifeAsiaMessage(), "0");

				if (result == 0) {
					throw new DAOException(
							"No Updated Row for LAREQUESTTRACKER table for claimNumber = " + jsonObjIn.getCLAIMNO());
				}
				log.info("Successfully inserted row to LAREQUESTTRACKER for claimNo= " + jsonObjIn.getCLAIMNO());
				// }
				jsonObjOut.setWsSuccessMessage("Successfully Processsed");
				jsonObjOut.setWsProcessingStatus("1");

			} else {
				throw new CamelException("Request String is empty");
			}

		} catch (Exception e) {
			log.error("Error at processing: " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			jsonObjOut.setWsExceptionMessage("Error " + e.getMessage());
			jsonObjOut.setWsProcessingStatus("2");
		}

		log.info("Json Response = " + jsonObjOut.toString());

		exchange.getOut().setBody(jsonObjOut);

	}

}
