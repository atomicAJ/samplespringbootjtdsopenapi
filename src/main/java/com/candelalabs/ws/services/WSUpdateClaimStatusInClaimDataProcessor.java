package com.candelalabs.ws.services;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSUpdateClaimStatusDataIn;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.POSDataModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class WSUpdateClaimStatusInClaimDataProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	private static Log log = LogFactory.getLog("WSUpdateClaimStatusInClaimDataProcessor");// .getLog(WSUpdateClaimStatusProcessor.class);

	public void process(Exchange exchange) throws Exception {

		log.info(" WSUpdateClaimStatusInClaimDataProcessor -- testing log");
		log.info(" WSUpdateClaimStatusInClaimDataProcessor.class.getName() --"
				+ WSUpdateClaimStatusInClaimDataProcessor.class.getName());
		log.info(" WSUpdateClaimStatusInClaimDataProcessor.class.getName() --"
				+ WSUpdateClaimStatusInClaimDataProcessor.class.toString());
		System.out.println("WSUpdateClaimStatusInClaimDataProcessor -- testing console log");
		System.out.println(WSUpdateClaimStatusInClaimDataProcessor.class.getName());
		System.out.println(WSUpdateClaimStatusInClaimDataProcessor.class.toString());
		Gson gson = new GsonBuilder().serializeNulls().create();

		JsonResponse jsonoutData = new JsonResponse();
		try {

			WSUpdateClaimStatusDataIn jsonData = (exchange.getIn().getBody(WSUpdateClaimStatusDataIn.class));
			log.info("WSUpdateClaimStatusInClaimDataProcessor - In Method - process - Input Payload = "
					+ jsonData.toString());
		//	ClaimDataNewModel claimData = claimDetailsDao.getClaimDataNew(jsonData.getClaimNo());
			POSDataModel posData = claimDetailsDao.getPOSData(jsonData.getPosRequestNumber());
			if (Optional.ofNullable(jsonData).isPresent())
				//claimDetailsDao.updatedClaimStatusInClaimData(jsonData,claimData.getProcessInitiatonTS());
			{	claimDetailsDao.updatedClaimStatusInPOSData(jsonData,posData.getProcessInitiatonTS());}
			else
				throw new Exception("Input JSON is empty");

			jsonoutData.setWsProcessingStatus("1");
			jsonoutData.setWsSuccessMessage("Successfully updated ClaimStatus in ClaimData table");

			log.info("WSUpdateClaimStatusInClaimDataProcessor - In Method - process - getBody");

		} catch (Exception e) {
			log.error("ERROR Detail= " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			jsonoutData.setWsExceptionMessage("ERROR =" + e.getMessage());
			jsonoutData.setWsProcessingStatus("2");

		}

		String json = gson.toJson(jsonoutData);
		log.info("JSON RESPONSE PAYLOAD = " + json);

		exchange.getOut().setBody(jsonoutData);

	}

}
