package com.candelalabs.ws.services;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.EPOSRequest;
import com.candelalabs.api.model.EPOSResponse;
import com.candelalabs.api.model.EPOSResponseWSResponse;
import com.candelalabs.epos.model.POSTransactionDetails;
import com.candelalabs.epos.resource.EPOSPencilUIService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WSPolicyServiceProcessor implements Processor
{
	private static final Log log = LogFactory.getLog("WSPolicyServiceProcessor");

	@Autowired
	EPOSPencilUIService service;

	@Override
	public void process(Exchange exchange) throws Exception
	{
		log.info("WSPolicyServiceProcessor - In Method - process - start of Method");
		EPOSRequest request = (exchange.getIn().getBody(EPOSRequest.class));
		EPOSResponseWSResponse wsResponse = new EPOSResponseWSResponse();
		EPOSResponse res = new EPOSResponse();
		try
		{
			POSTransactionDetails response = service.getTransactionData(request.getWsInput().getPolicyNumber(), request.getWsInput().getTransactionId());
			ObjectMapper mapper = new ObjectMapper();
			wsResponse.setData(mapper.writeValueAsString(response));
			wsResponse.setWsSuccessMessage("Policy service data obtained successfully");
			wsResponse.setWsProcessingStatus("1");
			res.setWsResponse(wsResponse);
		}
		catch (Exception e)
		{
			wsResponse.setWsProcessingStatus("-1");
			wsResponse.setWsExceptionMessage(e.getMessage());
			res.setWsResponse(wsResponse);
			log.error("Error getting policy service data : ", e);
		}
		log.info("JSON RESPONSE PAYLOAD = " + wsResponse.getData());
		exchange.getOut().setBody(res);
	}

}
