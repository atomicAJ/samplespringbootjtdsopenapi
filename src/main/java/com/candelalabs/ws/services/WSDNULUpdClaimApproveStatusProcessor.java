package com.candelalabs.ws.services;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonResponse;
import com.candelalabs.api.model.WSDNULUpdClaimApproveDataIn;
import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.PolicyDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.candelalabs.ws.util.PaddingUtil;
import com.candelalabs.ws.util.Util;

@Component
@PropertySource("classpath:application.properties")
public class WSDNULUpdClaimApproveStatusProcessor implements Processor {

	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao odsDao;
	private static Log log = LogFactory.getLog("WSDNULUpdClaimApproveStatusProcessor");// .getLog(WSUpdateClaimStatusProcessor.class);

	// below working config
	// private static final Logger log =
	// LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());
	/*
	 * @Autowired private ClaimDetailsDao updatedRemarksInClaimData;
	 */
	@Value("${USRPRF}")
	private String USRPRF;
	int updateSuccess = 0;

	public void process(Exchange exchange) throws Exception {

		log.info(" WSDNULUpdClaimApproveStatusProcessor -- testing log");
		log.info(" WSDNULUpdClaimApproveStatusProcessor.class.getName() --"
				+ WSDNULUpdClaimApproveStatusProcessor.class.getName());
		log.info(" WSDNULUpdClaimApproveStatusProcessor.class.getName() --"
				+ WSDNULUpdClaimApproveStatusProcessor.class.toString());
		System.out.println("WSDNULUpdClaimApproveStatusProcessor -- testing console log");
		System.out.println(WSDNULUpdClaimApproveStatusProcessor.class.getName());
		System.out.println(WSDNULUpdClaimApproveStatusProcessor.class.toString());

		Gson gson = new GsonBuilder().serializeNulls().create();
		WSDNULUpdClaimApproveDataIn jsonData = new WSDNULUpdClaimApproveDataIn(); // for
																					// input
																					// ->
																					// jsonData
		JsonResponse jsonoutData = new JsonResponse(); // for output ->
														// jsonoutData
		try {

			jsonData = (exchange.getIn().getBody(WSDNULUpdClaimApproveDataIn.class));

			PaddingUtil pdngutl = new PaddingUtil();
			if (null != jsonData) {

//				log.info("Get LaRequestTrackerModel table with claimNo= " + jsonData.getCLAIMNO());
//				LaRequestTrackerModel laRequestTrackerData = this.claimDetailsDao.getLaRequestTrackerByClaimNo(
//						jsonData.getCLAIMNO(), jsonData.getACITVITYID(),
//						FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimApproved);
//				log.info("Successfully Got LaRequestTrackerModel table ");
//
//				if (laRequestTrackerData.getBoidentifier() == null && laRequestTrackerData.getMsgtosend() == null
//						&& laRequestTrackerData.getMsgcreatets() == null
//						&& laRequestTrackerData.getFuturemesg() == null) {
					log.info("Getting the data from ClaimData table with claimNO = " + jsonData.getCLAIMNO());
					ClaimDataNewModel claimsData = new ClaimDataNewModel();
					claimsData = claimDetailsDao.getClaimDataNew(jsonData.getCLAIMNO());
					log.info("Sucessfully got the data from ClaimData table");
					// PolicyDetails policyData = new PolicyDetails();
					log.info("Getting the PolicyDetail data with polNum = " + claimsData.getPolicyNumber());
					PolicyDetails polDet = new PolicyDetails();
					polDet = odsDao.getPolicyDetail(claimsData.getPolicyNumber());
					log.info("Successfully got the PolicyDetail table");
					Date currentDate = new Date();
					log.info("Generating the BOMessage structure");
					String msgtosend = pdngutl.padRight(USRPRF, 9)
							+ pdngutl.padZero(Integer.parseInt(polDet.getCOMPANY()), 1)
							+ pdngutl.padRight(FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimApproved, 20)
							+ pdngutl.padRight(FWDConstants.MESSAGE_LANGUAGE, 1)
							+ pdngutl.padZero(Integer.parseInt(FWDConstants.BO_DEATH_CLAIM_REJECTED_MESSAGE_LENGTH), 5)
							+ pdngutl.padRight(FWDConstants.MORE_INDICATOR, 1)
							+ pdngutl.padRight(claimsData.getPolicyNumber(), 10)
							+ pdngutl.padRight(jsonData.getCLAIMNO().toString(), 11)
							+ pdngutl.padRight(Util.dateToString(currentDate, "yyyyMMdd"), 8)
							+ pdngutl.PadForDoubleWithSym(0.0, 14);

					log.info("BOMessage result = " + msgtosend);
					log.info("Inserting the LAREQUESTTRACKER data table");
					int result = claimDetailsDao.lARequestTracker_Insert(jsonData.getCLAIMNO().toString(),
							jsonData.getACITVITYID(), FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimApproved,
							msgtosend, "0");

					if (result == 0) {
						throw new DAOException("No Updated row for LARequestTracker Table");
					}
					log.info("Successfully inserted the LAREQUESTTRACKER");
				//}
				jsonoutData.setWsProcessingStatus("1");
				jsonoutData.setWsSuccessMessage("Successfully Processed");

			} else {

				throw new Exception("Input String is empty");
			}
			log.info("WSDNULUpdClaimApproveStatusProcessor - In Method - process - getBody");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR DETAIL: " + e);
			for (StackTraceElement tr : e.getStackTrace()) {
				log.error("\tat " + tr);
			}
			jsonoutData.setWsExceptionMessage("Failure" + e);
			jsonoutData.setWsProcessingStatus("2");

		}

		// String json = gson.toJson(jsonoutData);
		// // WSDNULUpdClaimApproveStatusResponseType requirementResponse = new
		// // WSDNULUpdClaimApproveStatusResponseType();
		// String requirementResponse = null;
		// // requirementResponse.setWSOutputObject(json);
		// requirementResponse = json;
		log.info("WSDNULRegClaimProcessor - In Method - process - response in String # " + jsonoutData.toString());
		exchange.getOut().setBody(jsonoutData);

	}

}
