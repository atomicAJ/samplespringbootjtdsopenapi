package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.JsonWSRegisterRequirementDataIn;
import com.candelalabs.api.model.JsonResponse;
//import org.apache.commons.logging.Log;
import com.candelalabs.ws.dao.ClaimDetailsDao;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.model.tables.ClaimDataNewModel;
import com.candelalabs.ws.model.tables.ClientDetails;
//import com.candelalabs.ws.schema.stubs.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class WSCustPendingNotificationProcessor implements Processor {
	
	@Autowired
	ClaimDetailsDao claimDetailsDao;
	@Autowired
	ODSDao odsDao;
    private static Log log = LogFactory.getLog("WSCustPendingNotificationProcessor");//.getLog(WSUpdateClaimStatusProcessor.class);
	 
	//below working config
	//private static final Logger log = LogManager.getLogger(WSUpdateClaimStatusProcessor.class.getName());

	int updateSuccess = 0;

	public void process(Exchange exchange) throws Exception {
		
		   log.info(" WSCustPendingNotificationProcessor -- testing log");
		   log.info(" WSCustPendingNotificationProcessor.class.getName() --"+ WSCustPendingNotificationProcessor.class.getName());
		   log.info(" WSCustPendingNotificationProcessor.class.getName() --"+ WSCustPendingNotificationProcessor.class.toString());
		   System.out.println("WSCustPendingNotificationProcessor -- testing console log");
		   System.out.println(WSCustPendingNotificationProcessor.class.getName());
		   System.out.println(WSCustPendingNotificationProcessor.class.toString());
		   
			Gson gson = new GsonBuilder().serializeNulls().create();
			//JsonWSRegisterRequirementDataIn jsonData = new JsonWSRegisterRequirementDataIn();   // for input -> jsonData reusing same input object class
			JsonResponse jsonoutData = new JsonResponse(); // for output -> jsonoutData
			String notificationDecision = null;
	        String pHEmailAddress = null;
	        String pHPhoneNo = null;
	        String agentemailadd = null;
			ClientDetails clt_det = new ClientDetails();
			//ClaimsData claimsData = new ClaimsData();
			ClaimDataNewModel claimsData = new ClaimDataNewModel();
	        String claimuser = null;
	        
			try {

				
				/*String jsonInString = (exchange.getIn().getBody(WSCustPendingNotificationRequestType.class).getWSInputObject());
				jsonData = gson.fromJson(jsonInString, JsonWSRegisterRequirementDataIn.class);*/ //converting json string to class object
				JsonWSRegisterRequirementDataIn jsonData = (exchange.getIn()
						.getBody(JsonWSRegisterRequirementDataIn.class));
				log.info("JSON INPUT PAYLOAD = "+jsonData);
				int ret =0;
				if (null != jsonData)
				{
					if (jsonData.getDecision()!=null){
						
						switch(jsonData.getDecision()) {
				         case "Approve" :notificationDecision ="APPROVED";
				            break;
				         case "REJECT" :notificationDecision ="REJECT";
				         break;
				         case "AUTO REJECT" :notificationDecision ="AUTO REJECT";
				         break;
				         case "REQUEST FOR ADDITIONAL DOCUMENT" :notificationDecision ="PENDING DOCUMENT";
				         break;
				         case "START INVESTIGATION" :notificationDecision ="PENDING INVESTIGATION1";
				            break;
				         case "COMPLETE" :notificationDecision ="COMPLETE INVESTIGATION";
				            break;				            
				         default :
				        	 notificationDecision = "PENDING INVESTIGATION2";
				      }
						
				}
			   List<Map<String, Object>> notificationCategory = new ArrayList<>();
			  notificationCategory = claimDetailsDao.getNotificationCategory(notificationDecision);
			  
			 // pHEmailAddress =odsDao.getpHEmailAddress(jsonData.getPolNo());
			  claimsData = claimDetailsDao.getClaimData(jsonData.getClaimNo());
			  
			  clt_det = odsDao.getClientDetails(claimsData.getInsuredClientID());
			  pHEmailAddress = clt_det.getEMAILADDRESS();
			  pHPhoneNo = clt_det.getPHONENO01();//odsDao.getpHPhoneNo(jsonData.getPolNo());
			  
			  agentemailadd = odsDao.getAgentemailadd(jsonData.getPolNo());
			  claimuser = jsonData.getLastUser();
			  
					
					ret = claimDetailsDao.insertNotification(jsonData, notificationDecision, 
							  notificationCategory, pHEmailAddress, pHPhoneNo, agentemailadd, claimuser);
					if (ret >0)
					{
						jsonoutData.setWsProcessingStatus("1");
						jsonoutData.setWsSuccessMessage("Successfully Processed");
					log.info("WSCustPendingNotificationProcessor -"+ ret + " record inserted sucessfully");
					}
					else 
					{
						jsonoutData.setWsExceptionMessage("No rows inserted in LARequestTracker");
						jsonoutData.setWsProcessingStatus("2");
						log.info("WSCustPendingNotificationProcessor no records inserted");
					}
					
				}
				else
					
					throw new Exception();

				

				log.info("WSCustPendingNotificationProcessor - In Method - process - getBody");

			} catch (Exception e) {
				e.printStackTrace();
				for(StackTraceElement tr :e.getStackTrace()) {
					log.error("\tat "+tr);
				}
				jsonoutData.setWsExceptionMessage("Failure" + e);
				jsonoutData.setWsProcessingStatus("2");
				//jsonoutData.setSOURCEEXCEPTIONPROCESS("Null");
				//jsonoutData.setSOURCEEXCEPTIONSTEP("Null");

			}

			/*String json = gson.toJson(jsonoutData);
			WSCustPendingNotificationResponseType requirementResponse = new WSCustPendingNotificationResponseType();
			requirementResponse.setWSOutputObject(json);*/
			exchange.getOut().setBody(jsonoutData);

		}
	
	}

