/**
 * 
 */
package com.candelalabs.ws.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.candelalabs.api.model.DataOutGetProviderNameListWSResponse;
import com.candelalabs.ws.dao.ODSDao;
import com.candelalabs.ws.exception.DAOException;

/**
 * @author Triaji
 *
 */
@Component
public class WSUIGetProviderNameListProcessor implements Processor {

	private static final Log log = LogFactory.getLog("WSUIGetProviderNameListProcessor");

	@Autowired
	private ODSDao oDSDao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		DataOutGetProviderNameListWSResponse wsresponse = new DataOutGetProviderNameListWSResponse();
		try {
			List<String> prodNameList = this.oDSDao.getProviderNameList();
			List<String> newProdNameList = new ArrayList<String>();
			for (String data : prodNameList) {
				newProdNameList.add(data.trim());
			}
			if (prodNameList != null && prodNameList.size() > 0) {
				wsresponse.setStrProviderNameList(newProdNameList);
				wsresponse.setWsProcessingStatus("1");
				wsresponse.setWsSuccessMessage("Successfully Retrieved the Provider Name List");
			} else {
				throw new DAOException("Provider Name List is not available");
			}

		} catch (Exception e) {
			// TODO: handle exception
			log.error("ERROR DETAIL: " + e);
			wsresponse.setWsProcessingStatus("2");
			wsresponse.setWsExceptionMessage("ERROR DETAIL " + e.getMessage());
		}
		exchange.getOut().setBody(wsresponse);

	}

}
