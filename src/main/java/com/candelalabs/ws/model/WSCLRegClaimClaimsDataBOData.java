package com.candelalabs.ws.model;

public class WSCLRegClaimClaimsDataBOData {
	

	private String ClaimNo;
	private String ChdrSel;
    private String ZdiagCode;
    private String ZmedPrv;
	private String Datefrom;
    private String ZclmRecd; 
    private String IncurDt;
    private String Dischdt;
    private String Hosben;
    private String DateFrm;
    private String DateTo;
    private String CrTable;
    private String ClamParty;
    private String ClType;
    private double ActExp;
    private double ZnoDay;
    private double TactExp;
	public String getClaimNo() {
		return ClaimNo;
	}
	
	public String getChdrSel() {
		return ChdrSel;
	}

	public void setChdrnum(String chdrSel) {
		ChdrSel = chdrSel;
	}

	public void setClaimNo(String claimNo) {
		ClaimNo = claimNo;
	}
	
	public String getZdiagCode() {
		return ZdiagCode;
	}
	public void setZdiagCode(String zdiagCode) {
		ZdiagCode = zdiagCode;
	}
	public String getZmedPrv() {
		return ZmedPrv;
	}
	public void setZmedPrv(String zmedPrv) {
		ZmedPrv = zmedPrv;
	}
	public String getDatefrom() {
		return Datefrom;
	}
	public void setDatefrom(String datefrom) {
		Datefrom = datefrom;
	}
	public String getZclmRecd() {
		return ZclmRecd;
	}
	public void setZclmRecd(String zclmRecd) {
		ZclmRecd = zclmRecd;
	}
	public String getIncurDt() {
		return IncurDt;
	}
	public void setIncurDt(String incurDt) {
		IncurDt = incurDt;
	}
	public String getDischdt() {
		return Dischdt;
	}
	public void setDischdt(String dischdt) {
		Dischdt = dischdt;
	}
	public String getHosben() {
		return Hosben;
	}
	public void setHosben(String hosben) {
		Hosben = hosben;
	}
	public String getDateFrm() {
		return DateFrm;
	}
	public void setDateFrm(String dateFrm) {
		DateFrm = dateFrm;
	}
	public String getDateTo() {
		return DateTo;
	}
	public void setDateTo(String dateTo) {
		DateTo = dateTo;
	}
	public double getActExp() {
		return ActExp;
	}
	public void setActExp(double actExp) {
		ActExp = actExp;
	}
	public double getZnoDay() {
		return ZnoDay;
	}
	public void setZnoDay(double znoDay) {
		ZnoDay = znoDay;
	}
	public double getTactExp() {
		return TactExp;
	}
	public void setTactExp(double tactExp) {
		TactExp = tactExp;
	}

	public void setChdrSel(String chdrSel) {
		ChdrSel = chdrSel;
	}

	public String getCrTable() {
		return CrTable;
	}

	public void setCrTable(String crTable) {
		CrTable = crTable;
	}

	public String getClamParty() {
		return ClamParty;
	}

	public void setClamParty(String clamParty) {
		ClamParty = clamParty;
	}

	public String getClType() {
		return ClType;
	}

	public void setClType(String clType) {
		ClType = clType;
	}
    
	
}
