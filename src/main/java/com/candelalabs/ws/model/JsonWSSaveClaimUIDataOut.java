package com.candelalabs.ws.model;

public class JsonWSSaveClaimUIDataOut {
	private String processmsg;
    private String decision;
    
	public JsonWSSaveClaimUIDataOut() {
		super();
	}
	
	public JsonWSSaveClaimUIDataOut(String processmsg, String decision) {
		super();
		this.processmsg = processmsg;
		this.decision = decision;
	}

	public String getProcessmsg() {
		return processmsg;
	}
	public void setProcessmsg(String processmsg) {
		this.processmsg = processmsg;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}

	@Override
	public String toString() {
		return "JsonWSSaveClaimUIDataOut [processmsg=" + processmsg + ", decision=" + decision + "]";
	}
	
}
