package com.candelalabs.ws.model;

public class WSDULRegClaimClaimsDataBOData {
	
	private String ClaimNo;
	private String ChdrSel;
	private String ClntNum;
	private String ZDIAGCDE;
	private String ZCATCLMBEN;
	private String EFDATE;
	
	public String getClaimNo() {
		return ClaimNo;
	}
	public void setClaimNo(String claimNo) {
		ClaimNo = claimNo;
	}
	public String getChdrSel() {
		return ChdrSel;
	}
	public void setChdrSel(String chdrSel) {
		ChdrSel = chdrSel;
	}
	public String getClntNum() {
		return ClntNum;
	}
	public void setClntNum(String clntNum) {
		ClntNum = clntNum;
	}
	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}
	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}
	public String getZCATCLMBEN() {
		return ZCATCLMBEN;
	}
	public void setZCATCLMBEN(String zCATCLMBEN) {
		ZCATCLMBEN = zCATCLMBEN;
	}
	public String getEFDATE() {
		return EFDATE;
	}
	public void setEFDATE(String eFDATE) {
		EFDATE = eFDATE;
	}
	
	
}
