package com.candelalabs.ws.model;

import java.util.Date;

public class JsonWSLARegisterClaimDataIn {

	private String claimNo;
	private String claimCancelRemark;
	private Short claimStatus;
	
	public String getClaimNo() {
		return claimNo;
	}
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	public String getClaimCancelRemark() {
		return claimCancelRemark;
	}
	public void setClaimCancelRemark(String claimCancelRemark) {
		this.claimCancelRemark = claimCancelRemark;
	}
	public Short getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(Short claimStatus) {
		this.claimStatus = claimStatus;
	}
}