/**
 * 
 */
package com.candelalabs.ws.model;

/**
 * @author Admin
 *
 */
public class HQRetrieve {

	private String proposalNo;
	private String hqeDetails;
	private boolean recordprocessed;
	private String lifeagender;
	private boolean lifebexists;
	private String lifebgender;
	private boolean juvenileflag;
	private String contactowner;
	
	/**
	 * @return the proposalNo
	 */
	public String getProposalNo() {
		return proposalNo;
	}
	/**
	 * @param proposalNo the proposalNo to set
	 */
	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}
	/**
	 * @return the hqeDetails
	 */
	public String getHqeDetails() {
		return hqeDetails;
	}
	/**
	 * @param hqeDetails the hqeDetails to set
	 */
	public void setHqeDetails(String hqeDetails) {
		this.hqeDetails = hqeDetails;
	}
	/**
	 * @return the recordprocessed
	 */
	public boolean isRecordprocessed() {
		return recordprocessed;
	}
	/**
	 * @param recordprocessed the recordprocessed to set
	 */
	public void setRecordprocessed(boolean recordprocessed) {
		this.recordprocessed = recordprocessed;
	}
	/**
	 * @return the lifeagender
	 */
	public String getLifeagender() {
		return lifeagender;
	}
	/**
	 * @param lifeagender the lifeagender to set
	 */
	public void setLifeagender(String lifeagender) {
		this.lifeagender = lifeagender;
	}
	/**
	 * @return the lifebexists
	 */
	public boolean isLifebexists() {
		return lifebexists;
	}
	/**
	 * @param lifebexists the lifebexists to set
	 */
	public void setLifebexists(boolean lifebexists) {
		this.lifebexists = lifebexists;
	}
	/**
	 * @return the lifebgender
	 */
	public String getLifebgender() {
		return lifebgender;
	}
	/**
	 * @param lifebgender the lifebgender to set
	 */
	public void setLifebgender(String lifebgender) {
		this.lifebgender = lifebgender;
	}
	/**
	 * @return the juvenileflag
	 */
	public boolean isJuvenileflag() {
		return juvenileflag;
	}
	/**
	 * @param juvenileflag the juvenileflag to set
	 */
	public void setJuvenileflag(boolean juvenileflag) {
		this.juvenileflag = juvenileflag;
	}
	/**
	 * @return the contactowner
	 */
	public String getContactowner() {
		return contactowner;
	}
	/**
	 * @param contactowner the contactowner to set
	 */
	public void setContactowner(String contactowner) {
		this.contactowner = contactowner;
	}
	
}
