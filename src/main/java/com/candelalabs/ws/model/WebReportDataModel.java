package com.candelalabs.ws.model;

public class WebReportDataModel {
	private String strColName;
	private String strColValue;
	
	public WebReportDataModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WebReportDataModel(String strColName, String strColValue) {
		super();
		this.strColName = strColName;
		this.strColValue = strColValue;
	}

	public String getStrColName() {
		return strColName;
	}

	public void setStrColName(String strColName) {
		this.strColName = strColName;
	}

	public String getStrColValue() {
		return strColValue;
	}

	public void setStrColValue(String strColValue) {
		this.strColValue = strColValue;
	}

	@Override
	public String toString() {
		return "WebReportDataModel [strColName=" + strColName + ", strColValue=" + strColValue + "]";
	}
	
}
