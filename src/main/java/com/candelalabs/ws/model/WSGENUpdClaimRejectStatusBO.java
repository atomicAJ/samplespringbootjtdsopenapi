package com.candelalabs.ws.model;

public class WSGENUpdClaimRejectStatusBO {
	
	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private Integer MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMNO;
	private String CRTABLE;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String RGPYNUM;
	
	private String CLTYPE;
	
	public WSGENUpdClaimRejectStatusBO() {
		super();
	}
	public WSGENUpdClaimRejectStatusBO(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, Integer mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMNO, String cRTABLE, String lIFE, String cOVERAGE, String rIDER,
			String rGPYNUM,String CLTYPE) {
		super();
		this.USRPRF = uSRPRF;
		this.CHDRCOY = cHDRCOY;
		this.BOIDEN = bOIDEN;
		this.MSGLNG = mSGLNG;
		this.MSGCNT = mSGCNT;
		this.INDC = iNDC;
		this.CHDRSEL = cHDRSEL;
		this.CLAIMNO = cLAIMNO;
		this.CRTABLE = cRTABLE;
		this.LIFE = lIFE;
		this.COVERAGE = cOVERAGE;
		this.RIDER = rIDER;
		this.RGPYNUM = rGPYNUM;
		this.CLTYPE  = CLTYPE;
	}
	public String getUSRPRF() {
		return USRPRF;
	}
	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}
	public String getCHDRCOY() {
		return CHDRCOY;
	}
	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}
	public String getBOIDEN() {
		return BOIDEN;
	}
	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}
	public String getMSGLNG() {
		return MSGLNG;
	}
	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}
	public Integer getMSGCNT() {
		return MSGCNT;
	}
	public void setMSGCNT(Integer mSGCNT) {
		MSGCNT = mSGCNT;
	}
	public String getINDC() {
		return INDC;
	}
	public void setINDC(String iNDC) {
		INDC = iNDC;
	}
	public String getCHDRSEL() {
		return CHDRSEL;
	}
	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}
	public String getCLAIMNO() {
		return CLAIMNO;
	}
	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}
	public String getCRTABLE() {
		return CRTABLE;
	}
	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}
	public String getLIFE() {
		return LIFE;
	}
	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}
	public String getCOVERAGE() {
		return COVERAGE;
	}
	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}
	public String getRIDER() {
		return RIDER;
	}
	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}
	public String getRGPYNUM() {
		return RGPYNUM;
	}
	public void setRGPYNUM(String rGPYNUM) {
		RGPYNUM = rGPYNUM;
	}
	public String getCLTYPE() {
		return CLTYPE;
	}
	public void setCLTYPE(String cLTYPE) {
		CLTYPE = cLTYPE;
	}
	@Override
	public String toString() {
		return "WSGENUpdClaimRejectStatusBO [USRPRF=" + USRPRF + ", CHDRCOY=" + CHDRCOY + ", BOIDEN=" + BOIDEN
				+ ", MSGLNG=" + MSGLNG + ", MSGCNT=" + MSGCNT + ", INDC=" + INDC + ", CHDRSEL=" + CHDRSEL + ", CLAIMNO="
				+ CLAIMNO + ", CRTABLE=" + CRTABLE + ", LIFE=" + LIFE + ", COVERAGE=" + COVERAGE + ", RIDER=" + RIDER
				+ ", RGPYNUM=" + RGPYNUM + ", cLTYPE=" + CLTYPE + "]";
	}
	
}
