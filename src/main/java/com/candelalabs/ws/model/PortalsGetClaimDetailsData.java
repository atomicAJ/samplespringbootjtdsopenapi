package com.candelalabs.ws.model;
import java.sql.Date;
public class PortalsGetClaimDetailsData {
 private String ClaimNumber;
 private String PortalClaimRequestNumber;
 private String PolicyNumber;
 private String ClaimTypeLA;
 private String ClaimStatus;
 private String InsuredClientName;
 private String InsuredClientID;
 private String ClaimDecision;
 private Date  RegisterDate;
 private String ProcessInstanceID;
 
 
  
public String getProcessInstanceID() {
	return ProcessInstanceID;
}
public void setProcessInstanceID(String processInstanceID) {
	ProcessInstanceID = processInstanceID;
}
public String getClaimDecision() {
	return ClaimDecision;
}
public void setClaimDecision(String claimDecision) {
	ClaimDecision = claimDecision;
}
public Date getRegisterDate() {
	return RegisterDate;
}
public void setRegisterDate(Date registerDate) {
	RegisterDate = registerDate;
}
public String getClaimNumber() {
	return ClaimNumber;
}
public void setClaimNumber(String claimNumber) {
	ClaimNumber = claimNumber;
}
public String getPortalClaimRequestNumber() {
	return PortalClaimRequestNumber;
}
public void setPortalClaimRequestNumber(String portalClaimRequestNumber) {
	PortalClaimRequestNumber = portalClaimRequestNumber;
}
public String getPolicyNumber() {
	return PolicyNumber;
}
public void setPolicyNumber(String policyNumber) {
	PolicyNumber = policyNumber;
}
public String getClaimTypeLA() {
	return ClaimTypeLA;
}
public void setClaimTypeLA(String claimTypeLA) {
	ClaimTypeLA = claimTypeLA;
}
public String getClaimStatus() {
	return ClaimStatus;
}
public void setClaimStatus(String claimStatus) {
	ClaimStatus = claimStatus;
}
public String getInsuredClientName() {
	return InsuredClientName;
}
public void setInsuredClientName(String insuredClientName) {
	InsuredClientName = insuredClientName;
}
public String getInsuredClientID() {
	return InsuredClientID;
}
public void setInsuredClientID(String insuredClientID) {
	InsuredClientID = insuredClientID;
}
/*public String getRegisterDate() {
	return RegisterDate;
}
public void setRegisterDate(String registerDate) {
	RegisterDate = registerDate;
}*/
 
 
	
	
}
