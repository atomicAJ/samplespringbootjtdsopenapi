package com.candelalabs.ws.model;

import java.util.Date;

public class PencilReportRequest {
	private String ReportRequestID;
	private String ReportName;
	private String ReportRequestUser;
	private String ReportOutputFormat;
	private String ReportRequestInputData;
	private String ReportGenerated;
	private Date ReportRequestTimeStamp;
	private String ReportType;
	private String ReportFileLocation;
	private Date ReportEndTimeStamp;
	private String ReportGenerationMessage;
	private String ProcessedFlag;

	public PencilReportRequest(String reportRequestID, String reportName, String reportRequestUser,
			String reportOutputFormat, String reportRequestInputData, String reportGenerated,
			Date reportRequestTimeStamp, String reportType, String reportFileLocation, Date reportEndTimeStamp,
			String reportGenerationMessage, String processedFlag) {
		super();
		ReportRequestID = reportRequestID;
		ReportName = reportName;
		ReportRequestUser = reportRequestUser;
		ReportOutputFormat = reportOutputFormat;
		ReportRequestInputData = reportRequestInputData;
		ReportGenerated = reportGenerated;
		ReportRequestTimeStamp = reportRequestTimeStamp;
		ReportType = reportType;
		ReportFileLocation = reportFileLocation;
		ReportEndTimeStamp = reportEndTimeStamp;
		ReportGenerationMessage = reportGenerationMessage;
		ProcessedFlag = processedFlag;
	}

	public PencilReportRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getReportRequestID() {
		return ReportRequestID;
	}

	public void setReportRequestID(String reportRequestID) {
		ReportRequestID = reportRequestID;
	}

	public String getReportName() {
		return ReportName;
	}

	public void setReportName(String reportName) {
		ReportName = reportName;
	}

	public String getReportRequestUser() {
		return ReportRequestUser;
	}

	public void setReportRequestUser(String reportRequestUser) {
		ReportRequestUser = reportRequestUser;
	}

	public String getReportOutputFormat() {
		return ReportOutputFormat;
	}

	public void setReportOutputFormat(String reportOutputFormat) {
		ReportOutputFormat = reportOutputFormat;
	}

	public String getReportRequestInputData() {
		return ReportRequestInputData;
	}

	public void setReportRequestInputData(String reportRequestInputData) {
		ReportRequestInputData = reportRequestInputData;
	}

	public String getReportGenerated() {
		return ReportGenerated;
	}

	public void setReportGenerated(String reportGenerated) {
		ReportGenerated = reportGenerated;
	}

	public Date getReportRequestTimeStamp() {
		return ReportRequestTimeStamp;
	}

	public void setReportRequestTimeStamp(Date reportRequestTimeStamp) {
		ReportRequestTimeStamp = reportRequestTimeStamp;
	}

	public String getReportFileLocation() {
		return ReportFileLocation;
	}

	public void setReportFileLocation(String reportFileLocation) {
		ReportFileLocation = reportFileLocation;
	}

	public Date getReportEndTimeStamp() {
		return ReportEndTimeStamp;
	}

	public void setReportEndTimeStamp(Date reportEndTimeStamp) {
		ReportEndTimeStamp = reportEndTimeStamp;
	}

	public String getReportGenerationMessage() {
		return ReportGenerationMessage;
	}

	public void setReportGenerationMessage(String reportGenerationMessage) {
		ReportGenerationMessage = reportGenerationMessage;
	}

	public String getReportType() {
		return ReportType;
	}

	public void setReportType(String reportType) {
		ReportType = reportType;
	}

	public String getProcessedFlag() {
		return ProcessedFlag;
	}

	public void setProcessedFlag(String processedFlag) {
		ProcessedFlag = processedFlag;
	}

	@Override
	public String toString() {
		return "PencilReportRequest [ReportRequestID=" + ReportRequestID + ", ReportName=" + ReportName
				+ ", ReportRequestUser=" + ReportRequestUser + ", ReportOutputFormat=" + ReportOutputFormat
				+ ", ReportRequestInputData=" + ReportRequestInputData + ", ReportGenerated=" + ReportGenerated
				+ ", ReportRequestTimeStamp=" + ReportRequestTimeStamp + ", ReportType=" + ReportType
				+ ", ReportFileLocation=" + ReportFileLocation + ", ReportEndTimeStamp=" + ReportEndTimeStamp
				+ ", ReportGenerationMessage=" + ReportGenerationMessage + ", ProcessedFlag=" + ProcessedFlag + "]";
	}

}
