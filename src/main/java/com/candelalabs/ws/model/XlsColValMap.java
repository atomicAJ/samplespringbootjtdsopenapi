package com.candelalabs.ws.model;

public class XlsColValMap {
	private int colNum;
	private String colValue;
	private String colValueType;
	
	public XlsColValMap() {
		super();
		// TODO Auto-generated constructor stub
	}

	public XlsColValMap(int colNum, String colValue, String colValueType) {
		super();
		this.colNum = colNum;
		this.colValue = colValue;
		this.colValueType = colValueType;
	}

	public int getColNum() {
		return colNum;
	}

	public void setColNum(int colNum) {
		this.colNum = colNum;
	}

	public String getColValue() {
		return colValue;
	}

	public void setColValue(String colValue) {
		this.colValue = colValue;
	}

	public String getColValueType() {
		return colValueType;
	}

	public void setColValueType(String colValueType) {
		this.colValueType = colValueType;
	}

	@Override
	public String toString() {
		return "XlsColValMap [colNum=" + colNum + ", colValue=" + colValue + ", colValueType=" + colValueType + "]";
	}
	
}
