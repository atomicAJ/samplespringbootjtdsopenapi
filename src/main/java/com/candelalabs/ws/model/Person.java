/**
 * 
 */
package com.candelalabs.ws.model;

/**
 * @author Triaji
 *
 */
public class Person {
	
	private String cn;
	private String displayName;
	private String mail;
	/**
	 * 
	 */
	public Person() {
		// TODO Auto-generated constructor stub
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

}
