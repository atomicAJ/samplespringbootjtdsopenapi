/**
 * 
 */
package com.candelalabs.ws.model.activitirest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Triaji
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessInstanceVariablesModel {
	
	private String name;
	private Object value;
	
	
	public ProcessInstanceVariablesModel() {
		// TODO Auto-generated constructor stub
	}
	
	public ProcessInstanceVariablesModel(String name, Object value2) {
		super();
		this.name = name;
		this.value = value2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	

}
