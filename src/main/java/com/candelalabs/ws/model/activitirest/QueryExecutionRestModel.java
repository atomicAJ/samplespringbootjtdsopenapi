/**
 * 
 */
package com.candelalabs.ws.model.activitirest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Triaji
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryExecutionRestModel {
	
	private String processInstanceId;
	private String activityId;

	/**
	 * 
	 */
	public QueryExecutionRestModel() {
		// TODO Auto-generated constructor stub
	}

	public QueryExecutionRestModel(String processInstanceId, String activityId) {
		super();
		this.processInstanceId = processInstanceId;
		this.activityId = activityId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	

}
