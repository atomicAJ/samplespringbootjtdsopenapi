package com.candelalabs.ws.model;

public class JsonWSDULActualAmountProcessorDataIn {
   
    private String CLAIMNO;
 
    private String OFCHARGE;
    private String DTEAPPR;
    private String FUTUREMSG;
    private String ACITVITYID;

    
    
    public String getDTEAPPR() {
		return DTEAPPR;
	}
	public void setDTEAPPR(String dTEAPPR) {
		DTEAPPR = dTEAPPR;
	}
	public String getFUTUREMSG() {
		return FUTUREMSG;
	}
	public void setFUTUREMSG(String fUTUREMSG) {
		FUTUREMSG = fUTUREMSG;
	}
	
    public String getOFCHARGE() {
		return OFCHARGE;
	}
	public void setOFCHARGE(String oFCHARGE) {
		OFCHARGE = oFCHARGE;
	}
	
    
    public String getACITVITYID() {
		return ACITVITYID;
	}
	public void setACITVITYID(String aCITVITYID) {
		ACITVITYID = aCITVITYID;
	}
	
	public String getCLAIMNO() {
		return CLAIMNO;
	}
	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}
	

}
