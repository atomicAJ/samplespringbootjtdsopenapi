package com.candelalabs.ws.model;

public class ClaimDataUI {

	private String claimNumber;
	private String tpaClaimNumber;
	private String contractNumber;
	private String productName;
	private String claimType;
	private String planCode;
	private String claimReceivedDate;
	private String claimRegisteredDate;
	private String insuredName;
	private String diagnoseCode;
	private String diagnoseName;
	private String admissionDate;
	private String dischargeDate;
	private String provider;
	private String doctorName;
	private Integer invoiceAmount;
	private String currency;
	private String payeeName;
	private String factoringHouse;
	private String bankOrBranchCode;
	private String bankAccountName;
	private String bankAccountNumber;
	private String bankType;
	private String currentForm;
	private String benefitCategory;
	private String policyNumber;

	public ClaimDataUI() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClaimDataUI(String claimNumber, String tpaClaimNumber, String contractNumber, String productName, String claimType,
			String planCode, String claimReceivedDate, String claimRegisteredDate, String insuredName,
			String diagnoseCode, String diagnoseName, String admissionDate, String dischargeDate, String provider,
			String doctorName, Integer invoiceAmount, String currency, String payeeName, String factoringHouse,
			String bankOrBranchCode, String bankAccountName, String bankAccountNumber, String bankType,
			String currentForm, String benefitCategory, String policyNumber) {
		super();
		this.claimNumber = claimNumber;
		this.tpaClaimNumber = tpaClaimNumber;
		this.contractNumber = contractNumber;
		this.productName = productName;
		this.claimType = claimType;
		this.planCode = planCode;
		this.claimReceivedDate = claimReceivedDate;
		this.claimRegisteredDate = claimRegisteredDate;
		this.insuredName = insuredName;
		this.diagnoseCode = diagnoseCode;
		this.diagnoseName = diagnoseName;
		this.admissionDate = admissionDate;
		this.dischargeDate = dischargeDate;
		this.provider = provider;
		this.doctorName = doctorName;
		this.invoiceAmount = invoiceAmount;
		this.currency = currency;
		this.payeeName = payeeName;
		this.factoringHouse = factoringHouse;
		this.bankOrBranchCode = bankOrBranchCode;
		this.bankAccountName = bankAccountName;
		this.bankAccountNumber = bankAccountNumber;
		this.bankType = bankType;
		this.currentForm = currentForm;
		this.benefitCategory = benefitCategory;
		this.policyNumber = policyNumber;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}
	
	public String getTpaClaimNumber() {
		return tpaClaimNumber;
	}

	public void setTpaClaimNumber(String tpaClaimNumber) {
		this.tpaClaimNumber = tpaClaimNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getClaimReceivedDate() {
		return claimReceivedDate;
	}

	public void setClaimReceivedDate(String claimReceivedDate) {
		this.claimReceivedDate = claimReceivedDate;
	}

	public String getClaimRegisteredDate() {
		return claimRegisteredDate;
	}

	public void setClaimRegisteredDate(String claimRegisteredDate) {
		this.claimRegisteredDate = claimRegisteredDate;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getDiagnoseCode() {
		return diagnoseCode;
	}

	public void setDiagnoseCode(String diagnoseCode) {
		this.diagnoseCode = diagnoseCode;
	}

	public String getDiagnoseName() {
		return diagnoseName;
	}

	public void setDiagnoseName(String diagnoseName) {
		this.diagnoseName = diagnoseName;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public Integer getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Integer invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getFactoringHouse() {
		return factoringHouse;
	}

	public void setFactoringHouse(String factoringHouse) {
		this.factoringHouse = factoringHouse;
	}

	public String getBankOrBranchCode() {
		return bankOrBranchCode;
	}

	public void setBankOrBranchCode(String bankOrBranchCode) {
		this.bankOrBranchCode = bankOrBranchCode;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getCurrentForm() {
		return currentForm;
	}

	public void setCurrentForm(String currentForm) {
		this.currentForm = currentForm;
	}

	public String getBenefitCategory() {
		return benefitCategory;
	}

	public void setBenefitCategory(String benefitCategory) {
		this.benefitCategory = benefitCategory;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	@Override
	public String toString() {
		return "ClaimDataUI [claimNumber=" + claimNumber + ", tpaClaimNumber=" + tpaClaimNumber + ", contractNumber="
				+ contractNumber + ", productName=" + productName + ", claimType=" + claimType + ", planCode="
				+ planCode + ", claimReceivedDate=" + claimReceivedDate + ", claimRegisteredDate=" + claimRegisteredDate
				+ ", insuredName=" + insuredName + ", diagnoseCode=" + diagnoseCode + ", diagnoseName=" + diagnoseName
				+ ", admissionDate=" + admissionDate + ", dischargeDate=" + dischargeDate + ", provider=" + provider
				+ ", doctorName=" + doctorName + ", invoiceAmount=" + invoiceAmount + ", currency=" + currency
				+ ", payeeName=" + payeeName + ", factoringHouse=" + factoringHouse + ", bankOrBranchCode="
				+ bankOrBranchCode + ", bankAccountName=" + bankAccountName + ", bankAccountNumber=" + bankAccountNumber
				+ ", bankType=" + bankType + ", currentForm=" + currentForm + ", benefitCategory=" + benefitCategory
				+ ", policyNumber=" + policyNumber + "]";
	}

}
