package com.candelalabs.ws.model;

public class WSDNULUpdClaimRejectStatusClaimsDataBOData {
	
	private String ClaimNo;
	private String ChdrSel;
    private String CrTable;
    private String ClamParty;

	public String getClaimNo() {
		return ClaimNo;
	}
	
	public String getChdrSel() {
		return ChdrSel;
	}

	public void setChdrSel(String chdrSel) {
		ChdrSel = chdrSel;
	}

	public void setClaimNo(String claimNo) {
		ClaimNo = claimNo;
	}

	public String getCrTable() {
		return CrTable;
	}

	public void setCrTable(String crTable) {
		CrTable = crTable;
	}

	public String getClamParty() {
		return ClamParty;
	}

	public void setClamParty(String clamParty) {
		ClamParty = clamParty;
	}
	
}
