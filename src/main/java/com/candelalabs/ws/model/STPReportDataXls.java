package com.candelalabs.ws.model;

public class STPReportDataXls {
	private String policyNumber;
	private String claimNumber;
	private String claimType;
	private String claimStatus;
	private String claimReceiveDate;
	private String claimRegisterDate;
	private String stPStatus;
	private String reasonForSTPFail;
	private String channel;
	private String claimDecision;
	
	public STPReportDataXls() {
		super();
		// TODO Auto-generated constructor stub
	}

	public STPReportDataXls(String policyNumber, String claimNumber, String claimType, String claimStatus,
			String claimReceiveDate, String claimRegisterDate, String stPStatus, String reasonForSTPFail,
			String channel, String claimDecision) {
		super();
		this.policyNumber = policyNumber;
		this.claimNumber = claimNumber;
		this.claimType = claimType;
		this.claimStatus = claimStatus;
		this.claimReceiveDate = claimReceiveDate;
		this.claimRegisterDate = claimRegisterDate;
		this.stPStatus = stPStatus;
		this.reasonForSTPFail = reasonForSTPFail;
		this.channel = channel;
		this.claimDecision = claimDecision;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	public String getClaimReceiveDate() {
		return claimReceiveDate;
	}

	public void setClaimReceiveDate(String claimReceiveDate) {
		this.claimReceiveDate = claimReceiveDate;
	}

	public String getClaimRegisterDate() {
		return claimRegisterDate;
	}

	public void setClaimRegisterDate(String claimRegisterDate) {
		this.claimRegisterDate = claimRegisterDate;
	}

	public String getStPStatus() {
		return stPStatus;
	}

	public void setStPStatus(String stPStatus) {
		this.stPStatus = stPStatus;
	}

	public String getReasonForSTPFail() {
		return reasonForSTPFail;
	}

	public void setReasonForSTPFail(String reasonForSTPFail) {
		this.reasonForSTPFail = reasonForSTPFail;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getClaimDecision() {
		return claimDecision;
	}

	public void setClaimDecision(String claimDecision) {
		this.claimDecision = claimDecision;
	}

	@Override
	public String toString() {
		return "STPReportDataXls [policyNumber=" + policyNumber + ", claimNumber=" + claimNumber + ", claimType="
				+ claimType + ", claimStatus=" + claimStatus + ", claimReceiveDate=" + claimReceiveDate
				+ ", claimRegisterDate=" + claimRegisterDate + ", stPStatus=" + stPStatus + ", reasonForSTPFail="
				+ reasonForSTPFail + ", channel=" + channel + ", claimDecision=" + claimDecision + "]";
	}

}
