/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class BenefitSummaryModel {

	private String POLICYNUMBER;
	private String BENEFITCODE;
	private BigDecimal TOTALDAYSUSED;
	private BigDecimal TOTALAMOUNTUSED;
	private BigDecimal POLICYYEAR;
	private String COMPONENTCODE;
	private String BENEFITPLAN;

	public BenefitSummaryModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPOLICYNUMBER() {
		return POLICYNUMBER;
	}

	public void setPOLICYNUMBER(String pOLICYNUMBER) {
		POLICYNUMBER = pOLICYNUMBER;
	}

	public String getBENEFITCODE() {
		return BENEFITCODE;
	}

	public void setBENEFITCODE(String bENEFITCODE) {
		BENEFITCODE = bENEFITCODE;
	}

	public BigDecimal getTOTALDAYSUSED() {
		return TOTALDAYSUSED;
	}

	public void setTOTALDAYSUSED(BigDecimal tOTALDAYSUSED) {
		TOTALDAYSUSED = tOTALDAYSUSED;
	}

	public BigDecimal getTOTALAMOUNTUSED() {
		return TOTALAMOUNTUSED;
	}

	public void setTOTALAMOUNTUSED(BigDecimal tOTALAMOUNTUSED) {
		TOTALAMOUNTUSED = tOTALAMOUNTUSED;
	}

	public BigDecimal getPOLICYYEAR() {
		return POLICYYEAR;
	}

	public void setPOLICYYEAR(BigDecimal pOLICYYEAR) {
		POLICYYEAR = pOLICYYEAR;
	}

	public String getCOMPONENTCODE() {
		return COMPONENTCODE;
	}

	public void setCOMPONENTCODE(String cOMPONENTCODE) {
		COMPONENTCODE = cOMPONENTCODE;
	}

	public String getBENEFITPLAN() {
		return BENEFITPLAN;
	}

	public void setBENEFITPLAN(String bENEFITPLAN) {
		BENEFITPLAN = bENEFITPLAN;
	}

}
