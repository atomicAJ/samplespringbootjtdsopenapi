package com.candelalabs.ws.model.tables;

public class ComponentCode {
	private String ComponentCode;
	private String POLICYNUMBER;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String CLIENTNUMBER;
	private String COMPONENTNAME;
	private String COMPONENTSTATUS;
	private String COMPPREMSTATUS;
	private String COMPONENTRCD;
	private String COMPRISKCESDATE;
	private String COMPPREMCESDATECOMPPREMCESDATE;
	private String SUMASSURED;
	private String PREMIUM;
	private String PLANCODE;
	//private int UNIT;
	
	private Integer UNIT;
	
	
	
	/*public int getUNIT() {
		return UNIT;
	}

	public void setUNIT(int uNIT) {
		UNIT = uNIT;
	}*/

	public Integer getUNIT() {
		return UNIT;
	}

	public void setUNIT(Integer uNIT) {
		UNIT = uNIT;
	}

	public String getPLANCODE() {
		return PLANCODE;
	}

	public void setPLANCODE(String pLANCODE) {
		PLANCODE = pLANCODE;
	}

	public String getPOLICYNUMBER() {
		return POLICYNUMBER;
	}

	public void setPOLICYNUMBER(String pOLICYNUMBER) {
		POLICYNUMBER = pOLICYNUMBER;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public String getCLIENTNUMBER() {
		return CLIENTNUMBER;
	}

	public void setCLIENTNUMBER(String cLIENTNUMBER) {
		CLIENTNUMBER = cLIENTNUMBER;
	}

	public String getCOMPONENTNAME() {
		return COMPONENTNAME;
	}

	public void setCOMPONENTNAME(String cOMPONENTNAME) {
		COMPONENTNAME = cOMPONENTNAME;
	}

	public String getCOMPONENTSTATUS() {
		return COMPONENTSTATUS;
	}

	public void setCOMPONENTSTATUS(String cOMPONENTSTATUS) {
		COMPONENTSTATUS = cOMPONENTSTATUS;
	}

	public String getCOMPPREMSTATUS() {
		return COMPPREMSTATUS;
	}

	public void setCOMPPREMSTATUS(String cOMPPREMSTATUS) {
		COMPPREMSTATUS = cOMPPREMSTATUS;
	}

	public String getCOMPONENTRCD() {
		return COMPONENTRCD;
	}

	public void setCOMPONENTRCD(String cOMPONENTRCD) {
		COMPONENTRCD = cOMPONENTRCD;
	}

	public String getCOMPRISKCESDATE() {
		return COMPRISKCESDATE;
	}

	public void setCOMPRISKCESDATE(String cOMPRISKCESDATE) {
		COMPRISKCESDATE = cOMPRISKCESDATE;
	}

	public String getCOMPPREMCESDATECOMPPREMCESDATE() {
		return COMPPREMCESDATECOMPPREMCESDATE;
	}

	public void setCOMPPREMCESDATECOMPPREMCESDATE(
			String cOMPPREMCESDATECOMPPREMCESDATE) {
		COMPPREMCESDATECOMPPREMCESDATE = cOMPPREMCESDATECOMPPREMCESDATE;
	}

	public String getSUMASSURED() {
		return SUMASSURED;
	}

	public void setSUMASSURED(String sUMASSURED) {
		SUMASSURED = sUMASSURED;
	}

	public String getPREMIUM() {
		return PREMIUM;
	}

	public void setPREMIUM(String pREMIUM) {
		PREMIUM = pREMIUM;
	}

	

	

	public String getComponentCode() {
		return ComponentCode;
	}

	public void setComponentCode(String componentCode) {
		ComponentCode = componentCode;
	}


}
