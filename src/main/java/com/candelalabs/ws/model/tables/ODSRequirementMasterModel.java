/**
 * 
 */
package com.candelalabs.ws.model.tables;

/**
 * @author Triaji
 *
 */
public class ODSRequirementMasterModel {
	
	private String DROPDOWNTEXT;
	private String 	CATEGORY;
	private String DOCID;
	private String SUBCATEGORY;
	private String BUSINESSPROCESS;	
	
	public ODSRequirementMasterModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getDROPDOWNTEXT() {
		return DROPDOWNTEXT;
	}
	public void setDROPDOWNTEXT(String dROPDOWNTEXT) {
		DROPDOWNTEXT = dROPDOWNTEXT;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getDOCID() {
		return DOCID;
	}
	public void setDOCID(String dOCID) {
		DOCID = dOCID;
	}
	public String getSUBCATEGORY() {
		return SUBCATEGORY;
	}
	public void setSUBCATEGORY(String sUBCATEGORY) {
		SUBCATEGORY = sUBCATEGORY;
	}
	public String getBUSINESSPROCESS() {
		return BUSINESSPROCESS;
	}
	public void setBUSINESSPROCESS(String bUSINESSPROCESS) {
		BUSINESSPROCESS = bUSINESSPROCESS;
	}
	
	

}
