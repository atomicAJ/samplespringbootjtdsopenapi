/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.sql.Timestamp;

/**
 * @author Triaji
 *
 */
public class UWCommentsModel {
	
private String	AppNumber ;
private String	PolicyNumber; 
private String	ClaimNumber ;
private String	POSRequestNumber; 
private String	UWComment ;
private String	UWCommentBy; 
private Timestamp	UWCommentTS ;
private String SourceOfComment;

public String getAppNumber() {
	return AppNumber;
}
public void setAppNumber(String appNumber) {
	AppNumber = appNumber;
}
public String getPolicyNumber() {
	return PolicyNumber;
}
public void setPolicyNumber(String policyNumber) {
	PolicyNumber = policyNumber;
}
public String getClaimNumber() {
	return ClaimNumber;
}
public void setClaimNumber(String claimNumber) {
	ClaimNumber = claimNumber;
}
public String getPOSRequestNumber() {
	return POSRequestNumber;
}
public void setPOSRequestNumber(String pOSRequestNumber) {
	POSRequestNumber = pOSRequestNumber;
}
public String getUWComment() {
	return UWComment;
}
public void setUWComment(String uWComment) {
	UWComment = uWComment;
}
public String getUWCommentBy() {
	return UWCommentBy;
}
public void setUWCommentBy(String uWCommentBy) {
	UWCommentBy = uWCommentBy;
}
public Timestamp getUWCommentTS() {
	return UWCommentTS;
}
public void setUWCommentTS(Timestamp uWCommentTS) {
	UWCommentTS = uWCommentTS;
}
public String getSourceOfComment() {
	return SourceOfComment;
}
public void setSourceOfComment(String sourceOfComment) {
	SourceOfComment = sourceOfComment;
} 

}
