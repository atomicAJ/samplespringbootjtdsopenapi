/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.sql.Date;
//\import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author Triaji New Claim Data Table Design
 *
 */
public class POSDataModel
{

	private String POSRequestNumber;
	private String PortalRequestNumber;
	private String ScanRequestNumber;
	private String PolicyNumber;
	private String TransactionId;
	private String TransactionDescription;
	private String TransactionCategory;
	private String TransactionSource;
	private String ProcessInitiatedFlag;
	private String POSDecision;
	private Date RecordCreationTS;
	private Date RequestSubmissionTS;
	private Date RecordCreationDateTime;
	private Date UnitDealingSubmissionDate;
	private Date UnitDealingReceviedDate;
	private String DuplicateFlag;
	private Timestamp ProcessInitiatonTS;
	private Timestamp ProcessEndTS;
	private String PSUser;
	private String QCUser;
	private String TransactionData;
	private String PSPayoutCreator;
	private String PSPayoutApprover;
	private String POSStatus;
	private String InsuredClientID;
	private String ComponentCode;
	private String PlanCode;
	private String ProductCode;
	private String SumAssured;
	private String InsuredClientName;
	private String ClaimCurrency;
	private String OwnerClientID;
	private String OwnerClientName;

	public Date getRecordCreationTS()
	{
		return RecordCreationTS;
	}

	public void setRecordCreationTS(Date recordCreationTS)
	{
		RecordCreationTS = recordCreationTS;
	}

	public Date getRequestSubmissionTS()
	{
		return RequestSubmissionTS;
	}

	public void setRequestSubmissionTS(Date requestSubmissionTS)
	{
		RequestSubmissionTS = requestSubmissionTS;
	}

	public Timestamp getProcessInitiatonTS()
	{
		return ProcessInitiatonTS;
	}

	public void setProcessInitiatonTS(Timestamp processInitiatonTS)
	{
		ProcessInitiatonTS = processInitiatonTS;
	}

	public String getOwnerClientID()
	{
		return OwnerClientID;
	}

	public void setOwnerClientID(String ownerClientID)
	{
		OwnerClientID = ownerClientID;
	}

	public String getOwnerClientName()
	{
		return OwnerClientName;
	}

	public void setOwnerClientName(String ownerClientName)
	{
		OwnerClientName = ownerClientName;
	}

	public String getClaimCurrency()
	{
		return ClaimCurrency;
	}

	public void setClaimCurrency(String claimCurrency)
	{
		ClaimCurrency = claimCurrency;
	}

	public String getInsuredClientName()
	{
		return InsuredClientName;
	}

	public void setInsuredClientName(String insuredClientName)
	{
		InsuredClientName = insuredClientName;
	}

	public String getPOSRequestNumber()
	{
		return POSRequestNumber;
	}

	public void setPOSRequestNumber(String pOSRequestNumber)
	{
		POSRequestNumber = pOSRequestNumber;
	}

	public String getPortalRequestNumber()
	{
		return PortalRequestNumber;
	}

	public void setPortalRequestNumber(String portalRequestNumber)
	{
		PortalRequestNumber = portalRequestNumber;
	}

	public String getScanRequestNumber()
	{
		return ScanRequestNumber;
	}

	public void setScanRequestNumber(String scanRequestNumber)
	{
		ScanRequestNumber = scanRequestNumber;
	}

	public String getPolicyNumber()
	{
		return PolicyNumber;
	}

	public void setPolicyNumber(String policyNumber)
	{
		PolicyNumber = policyNumber;
	}

	public String getTransactionId()
	{
		return TransactionId;
	}

	public void setTransactionId(String transactionId)
	{
		TransactionId = transactionId;
	}

	public String getTransactionDescription()
	{
		return TransactionDescription;
	}

	public void setTransactionDescription(String transactionDescription)
	{
		TransactionDescription = transactionDescription;
	}

	public String getTransactionCategory()
	{
		return TransactionCategory;
	}

	public void setTransactionCategory(String transactionCategory)
	{
		TransactionCategory = transactionCategory;
	}

	public String getTransactionSource()
	{
		return TransactionSource;
	}

	public void setTransactionSource(String transactionSource)
	{
		TransactionSource = transactionSource;
	}

	public String getProcessInitiatedFlag()
	{
		return ProcessInitiatedFlag;
	}

	public void setProcessInitiatedFlag(String processInitiatedFlag)
	{
		ProcessInitiatedFlag = processInitiatedFlag;
	}

	public String getPOSDecision()
	{
		return POSDecision;
	}

	public void setPOSDecision(String pOSDecision)
	{
		POSDecision = pOSDecision;
	}

	public Date getRecordCreationDateTime()
	{
		return RecordCreationDateTime;
	}

	public void setRecordCreationDateTime(Date recordCreationDateTime)
	{
		RecordCreationDateTime = recordCreationDateTime;
	}

	public Date getUnitDealingSubmissionDate()
	{
		return UnitDealingSubmissionDate;
	}

	public void setUnitDealingSubmissionDate(Date unitDealingSubmissionDate)
	{
		UnitDealingSubmissionDate = unitDealingSubmissionDate;
	}

	public Date getUnitDealingReceviedDate()
	{
		return UnitDealingReceviedDate;
	}

	public void setUnitDealingReceviedDate(Date unitDealingReceviedDate)
	{
		UnitDealingReceviedDate = unitDealingReceviedDate;
	}

	public String getDuplicateFlag()
	{
		return DuplicateFlag;
	}

	public void setDuplicateFlag(String duplicateFlag)
	{
		DuplicateFlag = duplicateFlag;
	}

	public Timestamp getProcessEndTS()
	{
		return ProcessEndTS;
	}

	public void setProcessEndTS(Timestamp processEndTS)
	{
		ProcessEndTS = processEndTS;
	}

	public String getPSUser()
	{
		return PSUser;
	}

	public void setPSUser(String pSUser)
	{
		PSUser = pSUser;
	}

	public String getQCUser()
	{
		return QCUser;
	}

	public void setQCUser(String qCUser)
	{
		QCUser = qCUser;
	}

	public String getTransactionData()
	{
		return TransactionData;
	}

	public void setTransactionData(String transactionData)
	{
		TransactionData = transactionData;
	}

	public String getPSPayoutCreator()
	{
		return PSPayoutCreator;
	}

	public void setPSPayoutCreator(String pSPayoutCreator)
	{
		PSPayoutCreator = pSPayoutCreator;
	}

	public String getPSPayoutApprover()
	{
		return PSPayoutApprover;
	}

	public void setPSPayoutApprover(String pSPayoutApprover)
	{
		PSPayoutApprover = pSPayoutApprover;
	}

	public String getPOSStatus()
	{
		return POSStatus;
	}

	public void setPOSStatus(String pOSStatus)
	{
		POSStatus = pOSStatus;
	}

	public String getInsuredClientID()
	{
		return InsuredClientID;
	}

	public void setInsuredClientID(String insuredClientID)
	{
		InsuredClientID = insuredClientID;
	}

	public String getComponentCode()
	{
		return ComponentCode;
	}

	public void setComponentCode(String componentCode)
	{
		ComponentCode = componentCode;
	}

	public String getPlanCode()
	{
		return PlanCode;
	}

	public void setPlanCode(String planCode)
	{
		PlanCode = planCode;
	}

	public String getProductCode()
	{
		return ProductCode;
	}

	public void setProductCode(String productCode)
	{
		ProductCode = productCode;
	}

	public String getSumAssured()
	{
		return SumAssured;
	}

	public void setSumAssured(String sumAssured)
	{
		SumAssured = sumAssured;
	}

}
