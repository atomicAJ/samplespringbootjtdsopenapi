/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.sql.Timestamp;

/**
 * @author Triaji
 *
 */
public class ReportDetailModel {

	private String ClaimNumber;
	private String PolicyRequestNumber;
	private String AppNumber;
	private String PolicyNumber;
	private String ProcessInstanceID;
	private String ProcessName;
	private String ActivityType;
	private String ActivityName;
	private Timestamp StartTime;
	private Timestamp EndTime;
	private String ActionedUser;
	private String UserDecision;

	/**
	 * 
	 */
	public ReportDetailModel() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param claimNumber
	 * @param policyRequestNumber
	 * @param appNumber
	 * @param policyNumber
	 * @param processInstanceID
	 * @param processName
	 * @param activityType
	 * @param activityName
	 * @param startTime
	 * @param endTime
	 * @param actionedUser
	 * @param userDecision
	 */
	public ReportDetailModel(String claimNumber, String policyRequestNumber, String appNumber, String policyNumber,
			String processInstanceID, String processName, String activityType, String activityName, Timestamp startTime,
			Timestamp endTime, String actionedUser, String userDecision) {
		super();
		ClaimNumber = claimNumber;
		PolicyRequestNumber = policyRequestNumber;
		AppNumber = appNumber;
		PolicyNumber = policyNumber;
		ProcessInstanceID = processInstanceID;
		ProcessName = processName;
		ActivityType = activityType;
		ActivityName = activityName;
		StartTime = startTime;
		EndTime = endTime;
		ActionedUser = actionedUser;
		UserDecision = userDecision;
	}

	public String getClaimNumber() {
		return ClaimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		ClaimNumber = claimNumber;
	}

	public String getPolicyRequestNumber() {
		return PolicyRequestNumber;
	}

	public void setPolicyRequestNumber(String policyRequestNumber) {
		PolicyRequestNumber = policyRequestNumber;
	}

	public String getAppNumber() {
		return AppNumber;
	}

	public void setAppNumber(String appNumber) {
		AppNumber = appNumber;
	}

	public String getPolicyNumber() {
		return PolicyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		PolicyNumber = policyNumber;
	}

	public String getProcessInstanceID() {
		return ProcessInstanceID;
	}

	public void setProcessInstanceID(String processInstanceID) {
		ProcessInstanceID = processInstanceID;
	}

	public String getProcessName() {
		return ProcessName;
	}

	public void setProcessName(String processName) {
		ProcessName = processName;
	}

	public String getActivityType() {
		return ActivityType;
	}

	public void setActivityType(String activityType) {
		ActivityType = activityType;
	}

	public String getActivityName() {
		return ActivityName;
	}

	public void setActivityName(String activityName) {
		ActivityName = activityName;
	}

	public Timestamp getStartTime() {
		return StartTime;
	}

	public void setStartTime(Timestamp startTime) {
		StartTime = startTime;
	}

	public Timestamp getEndTime() {
		return EndTime;
	}

	public void setEndTime(Timestamp endTime) {
		EndTime = endTime;
	}

	public String getActionedUser() {
		return ActionedUser;
	}

	public void setActionedUser(String actionedUser) {
		ActionedUser = actionedUser;
	}

	public String getUserDecision() {
		return UserDecision;
	}

	public void setUserDecision(String userDecision) {
		UserDecision = userDecision;
	}

}
