/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.sql.Timestamp;

/**
 * @author Triaji
 *
 */
public class RequirementDetailsModel {

	private String ClaimNumber;
	private String AppNumber;
	private String PolicyNumber;
	private String POSRequestNumber;
	private String DocID;
	private String RequirementText;
	private Timestamp RequirementRequestTS;
	private Timestamp RequirementReceiveTS;
	private String RequirementStatus;
	private String RequirementNotificationSent;

	public RequirementDetailsModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getClaimNumber() {
		return ClaimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		ClaimNumber = claimNumber;
	}

	public String getAppNumber() {
		return AppNumber;
	}

	public void setAppNumber(String appNumber) {
		AppNumber = appNumber;
	}

	public String getPolicyNumber() {
		return PolicyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		PolicyNumber = policyNumber;
	}

	public String getPOSRequestNumber() {
		return POSRequestNumber;
	}

	public void setPOSRequestNumber(String pOSRequestNumber) {
		POSRequestNumber = pOSRequestNumber;
	}

	public String getDocID() {
		return DocID;
	}

	public void setDocID(String docID) {
		DocID = docID;
	}

	public String getRequirementText() {
		return RequirementText;
	}

	public void setRequirementText(String requirementText) {
		RequirementText = requirementText;
	}

	public Timestamp getRequirementRequestTS() {
		return RequirementRequestTS;
	}

	public void setRequirementRequestTS(Timestamp requirementRequestTS) {
		RequirementRequestTS = requirementRequestTS;
	}

	public Timestamp getRequirementReceiveTS() {
		return RequirementReceiveTS;
	}

	public void setRequirementReceiveTS(Timestamp requirementReceiveTS) {
		RequirementReceiveTS = requirementReceiveTS;
	}

	public String getRequirementStatus() {
		return RequirementStatus;
	}

	public void setRequirementStatus(String requirementStatus) {
		RequirementStatus = requirementStatus;
	}

	public String getRequirementNotificationSent() {
		return RequirementNotificationSent;
	}

	public void setRequirementNotificationSent(String requirementNotificationSent) {
		RequirementNotificationSent = requirementNotificationSent;
	}

}
