/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class BenefitNotPaidAmount {
	

	private BigDecimal BenefitNotPaidAmount;
	
	public BenefitNotPaidAmount(BigDecimal benefitNotPaidAmount) {
		super();
		BenefitNotPaidAmount = benefitNotPaidAmount;
	}

	public BenefitNotPaidAmount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getBenefitNotPaidAmount() {
		return BenefitNotPaidAmount;
	}

	public void setBenefitNotPaidAmount(BigDecimal benefitNotPaidAmount) {
		BenefitNotPaidAmount = benefitNotPaidAmount;
	}
}
