package com.candelalabs.ws.model.tables;

public class HospitalMasterDetails {
	
	private String	 PROVIDERCLIENTNUMBER;
	private String PROVIDERNAME;
	
	
	public String getPROVIDERCLIENTNUMBER() {
		return PROVIDERCLIENTNUMBER;
	}
	public void setPROVIDERCLIENTNUMBER(String pROVIDERCLIENTNUMBER) {
		PROVIDERCLIENTNUMBER = pROVIDERCLIENTNUMBER;
	}
	public String getPROVIDERNAME() {
		return PROVIDERNAME;
	}
	public void setPROVIDERNAME(String pROVIDERNAME) {
		PROVIDERNAME = pROVIDERNAME;
	}
	
	
}
