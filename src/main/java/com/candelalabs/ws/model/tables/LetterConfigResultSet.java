package com.candelalabs.ws.model.tables;

public class LetterConfigResultSet {
private int LetterConfigurationID;
private int LetterTemplateID;

    public int getLetterConfigurationID() {
        return LetterConfigurationID;
    }

    public void setLetterConfigurationID(int letterConfigurationID) {
        LetterConfigurationID = letterConfigurationID;
    }

    public int getLetterTemplateID() {
        return LetterTemplateID;
    }

    public void setLetterTemplateID(int letterTemplateID) {
        LetterTemplateID = letterTemplateID;
    }
}
