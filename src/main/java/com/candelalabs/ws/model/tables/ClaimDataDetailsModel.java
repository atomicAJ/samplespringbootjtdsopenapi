/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author Triaji
 *
 */
public class ClaimDataDetailsModel {

	private String TPAClaimNumber;
	private String ClaimNumber;
	private String BenefitCode;
	private String BenefitName;
	private BigDecimal BenefitInvoiceAmount;
	private BigDecimal BenefitApprovedAmount;
	private BigDecimal BenefitUserApprovedAmount;
	private BigDecimal BenefitDaysClaimed;
	private BigDecimal BenefitAmountClaimed;
	private BigDecimal BenefitAmountApplicable;
	private BigDecimal BenefitAmountPerDay;
	private Date BenefitFromDate;
	private Date BenefitToDate;
	private BigDecimal BenefitActualDays;
	private BigDecimal BenefitPercentage;
	private Integer BenefitComponentUnit;
	private String FundComponentCode;
	private String FundComponentName;
	private String FundCode;
	private String FundDescription;
	private BigDecimal FundEstimatedAmount;
	private BigDecimal FundActualAmount;
	private BigDecimal BenefitTotalIncurredAmount;
	private BigDecimal BenefitAnnualTotalDays;
	private BigDecimal BenefitNotApprovedAmount;
	private BigDecimal BenefitMaxAmountPerDay; 
	private BigDecimal BenefitNoOfUnits;

	/**
	 * 
	 */
	public ClaimDataDetailsModel() {
		// TODO Auto-generated constructor stub
	}

	public String getTPAClaimNumber() {
		return TPAClaimNumber;
	}

	public void setTPAClaimNumber(String tPAClaimNumber) {
		TPAClaimNumber = tPAClaimNumber;
	}

	public String getClaimNumber() {
		return ClaimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		ClaimNumber = claimNumber;
	}

	public String getBenefitCode() {
		return BenefitCode;
	}

	public void setBenefitCode(String benefitCode) {
		BenefitCode = benefitCode;
	}

	public BigDecimal getBenefitInvoiceAmount() {
		return BenefitInvoiceAmount;
	}

	public void setBenefitInvoiceAmount(BigDecimal benefitInvoiceAmount) {
		BenefitInvoiceAmount = benefitInvoiceAmount;
	}

	public BigDecimal getBenefitApprovedAmount() {
		return BenefitApprovedAmount;
	}

	public void setBenefitApprovedAmount(BigDecimal benefitApprovedAmount) {
		BenefitApprovedAmount = benefitApprovedAmount;
	}

	public BigDecimal getBenefitUserApprovedAmount() {
		return BenefitUserApprovedAmount;
	}

	public void setBenefitUserApprovedAmount(BigDecimal benefitUserApprovedAmount) {
		BenefitUserApprovedAmount = benefitUserApprovedAmount;
	}

	public BigDecimal getBenefitDaysClaimed() {
		return BenefitDaysClaimed;
	}

	public void setBenefitDaysClaimed(BigDecimal benefitDaysClaimed) {
		BenefitDaysClaimed = benefitDaysClaimed;
	}

	public BigDecimal getBenefitAmountClaimed() {
		return BenefitAmountClaimed;
	}

	public void setBenefitAmountClaimed(BigDecimal benefitAmountClaimed) {
		BenefitAmountClaimed = benefitAmountClaimed;
	}

	public BigDecimal getBenefitAmountApplicable() {
		return BenefitAmountApplicable;
	}

	public void setBenefitAmountApplicable(BigDecimal benefitAmountApplicable) {
		BenefitAmountApplicable = benefitAmountApplicable;
	}

	public BigDecimal getBenefitAmountPerDay() {
		return BenefitAmountPerDay;
	}

	public void setBenefitAmountPerDay(BigDecimal benefitAmountPerDay) {
		BenefitAmountPerDay = benefitAmountPerDay;
	}

	public Date getBenefitFromDate() {
		return BenefitFromDate;
	}

	public void setBenefitFromDate(Date benefitFromDate) {
		BenefitFromDate = benefitFromDate;
	}

	public Date getBenefitToDate() {
		return BenefitToDate;
	}

	public void setBenefitToDate(Date benefitToDate) {
		BenefitToDate = benefitToDate;
	}

	public BigDecimal getBenefitActualDays() {
		return BenefitActualDays;
	}

	public void setBenefitActualDays(BigDecimal benefitActualDays) {
		BenefitActualDays = benefitActualDays;
	}

	public BigDecimal getBenefitPercentage() {
		return BenefitPercentage;
	}

	public void setBenefitPercentage(BigDecimal benefitPercentage) {
		BenefitPercentage = benefitPercentage;
	}

	public Integer getBenefitComponentUnit() {
		return BenefitComponentUnit;
	}

	public void setBenefitComponentUnit(Integer benefitComponentUnit) {
		BenefitComponentUnit = benefitComponentUnit;
	}

	public String getBenefitName() {
		return BenefitName;
	}

	public void setBenefitName(String benefitName) {
		BenefitName = benefitName;
	}

	public String getFundComponentCode() {
		return FundComponentCode;
	}

	public void setFundComponentCode(String fundComponentCode) {
		FundComponentCode = fundComponentCode;
	}

	public String getFundComponentName() {
		return FundComponentName;
	}

	public void setFundComponentName(String fundComponentName) {
		FundComponentName = fundComponentName;
	}

	public String getFundCode() {
		return FundCode;
	}

	public void setFundCode(String fundCode) {
		FundCode = fundCode;
	}

	public String getFundDescription() {
		return FundDescription;
	}

	public void setFundDescription(String fundDescription) {
		FundDescription = fundDescription;
	}

	public BigDecimal getFundEstimatedAmount() {
		return FundEstimatedAmount;
	}

	public void setFundEstimatedAmount(BigDecimal fundEstimatedAmount) {
		FundEstimatedAmount = fundEstimatedAmount;
	}

	public BigDecimal getFundActualAmount() {
		return FundActualAmount;
	}

	public void setFundActualAmount(BigDecimal fundActualAmount) {
		FundActualAmount = fundActualAmount;
	}

	public BigDecimal getBenefitTotalIncurredAmount() {
		return BenefitTotalIncurredAmount;
	}

	public void setBenefitTotalIncurredAmount(BigDecimal benefitTotalIncurredAmount) {
		BenefitTotalIncurredAmount = benefitTotalIncurredAmount;
	}

	public BigDecimal getBenefitAnnualTotalDays() {
		return BenefitAnnualTotalDays;
	}

	public void setBenefitAnnualTotalDays(BigDecimal benefitAnnualTotalDays) {
		BenefitAnnualTotalDays = benefitAnnualTotalDays;
	}

	public BigDecimal getBenefitNotApprovedAmount() {
		return BenefitNotApprovedAmount;
	}

	public void setBenefitNotApprovedAmount(BigDecimal benefitNotApprovedAmount) {
		BenefitNotApprovedAmount = benefitNotApprovedAmount;
	}

	public BigDecimal getBenefitMaxAmountPerDay() {
		return BenefitMaxAmountPerDay;
	}

	public void setBenefitMaxAmountPerDay(BigDecimal benefitMaxAmountPerDay) {
		BenefitMaxAmountPerDay = benefitMaxAmountPerDay;
	}

	public BigDecimal getBenefitNoOfUnits() {
		return BenefitNoOfUnits;
	}

	public void setBenefitNoOfUnits(BigDecimal benefitNoOfUnits) {
		BenefitNoOfUnits = benefitNoOfUnits;
	}

}
