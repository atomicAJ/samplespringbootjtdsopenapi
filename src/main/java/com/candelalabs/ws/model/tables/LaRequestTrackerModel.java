/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.sql.Timestamp;

/**
 * @author Triaji
 *
 */
public class LaRequestTrackerModel {
	
	private String claimnumber;
	private String activityid;
	private String boidentifier;
	private Timestamp msgcreatets;
	private String msgtosend;
	private String futuremesg;
	

	/**
	 * 
	 */
	public LaRequestTrackerModel() {
		// TODO Auto-generated constructor stub
	}


	public String getClaimnumber() {
		return claimnumber;
	}


	public void setClaimnumber(String claimnumber) {
		this.claimnumber = claimnumber;
	}


	public String getActivityid() {
		return activityid;
	}


	public void setActivityid(String activityid) {
		this.activityid = activityid;
	}


	public String getBoidentifier() {
		return boidentifier;
	}


	public void setBoidentifier(String boidentifier) {
		this.boidentifier = boidentifier;
	}


	public Timestamp getMsgcreatets() {
		return msgcreatets;
	}


	public void setMsgcreatets(Timestamp msgcreatets) {
		this.msgcreatets = msgcreatets;
	}


	public String getMsgtosend() {
		return msgtosend;
	}


	public void setMsgtosend(String msgtosend) {
		this.msgtosend = msgtosend;
	}


	public String getFuturemesg() {
		return futuremesg;
	}


	public void setFuturemesg(String futuremesg) {
		this.futuremesg = futuremesg;
	}

}
