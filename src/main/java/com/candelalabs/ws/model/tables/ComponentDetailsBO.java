package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;
import java.sql.Date;

public class ComponentDetailsBO {
private String Life;
private String Coverage;
private String Rider;
private BigDecimal UNIT;
private String CLIENTNUMBER;
private String COMPONENTCODE;
private String COMPONENTNAME;
private String COMPONENTSTATUS;
private String COMPPREMSTATUS;
private Date COMPPREMCESDATE;
private Date COMPONENTRCD;
private Date COMPRISKCESDATE;
private BigDecimal SUMASSURED;
private BigDecimal PREMIUM;
private String PLANCODE;

public String getLife() {
	return Life;
}

public void setLife(String life) {
	Life = life;
}

public String getCoverage() {
	return Coverage;
}

public void setCoverage(String coverage) {
	Coverage = coverage;
}

public String getRider() {
	return Rider;
}

public void setRider(String rider) {
	Rider = rider;
}

public Date getCOMPONENTRCD() {
	return COMPONENTRCD;
}

public void setCOMPONENTRCD(Date cOMPONENTRCD) {
	COMPONENTRCD = cOMPONENTRCD;
}


public String getCLIENTNUMBER() {
	return CLIENTNUMBER;
}

public void setCLIENTNUMBER(String cLIENTNUMBER) {
	CLIENTNUMBER = cLIENTNUMBER;
}

public String getCOMPONENTCODE() {
	return COMPONENTCODE;
}

public void setCOMPONENTCODE(String cOMPONENTCODE) {
	COMPONENTCODE = cOMPONENTCODE;
}

public String getCOMPONENTNAME() {
	return COMPONENTNAME;
}

public void setCOMPONENTNAME(String cOMPONENTNAME) {
	COMPONENTNAME = cOMPONENTNAME;
}

public String getCOMPONENTSTATUS() {
	return COMPONENTSTATUS;
}

public void setCOMPONENTSTATUS(String cOMPONENTSTATUS) {
	COMPONENTSTATUS = cOMPONENTSTATUS;
}

public String getCOMPPREMSTATUS() {
	return COMPPREMSTATUS;
}

public void setCOMPPREMSTATUS(String cOMPPREMSTATUS) {
	COMPPREMSTATUS = cOMPPREMSTATUS;
}

public Date getCOMPPREMCESDATE() {
	return COMPPREMCESDATE;
}

public void setCOMPPREMCESDATE(Date cOMPPREMCESDATE) {
	COMPPREMCESDATE = cOMPPREMCESDATE;
}

public BigDecimal getSUMASSURED() {
	return SUMASSURED;
}

public void setSUMASSURED(BigDecimal sUMASSURED) {
	SUMASSURED = sUMASSURED;
}

public BigDecimal getPREMIUM() {
	return PREMIUM;
}

public void setPREMIUM(BigDecimal pREMIUM) {
	PREMIUM = pREMIUM;
}

public String getPLANCODE() {
	return PLANCODE;
}

public void setPLANCODE(String pLANCODE) {
	PLANCODE = pLANCODE;
}

public Date getCOMPRISKCESDATE() {
	return COMPRISKCESDATE;
}

public void setCOMPRISKCESDATE(Date cOMPRISKCESDATE) {
	COMPRISKCESDATE = cOMPRISKCESDATE;
}

public BigDecimal getUNIT() {
	return UNIT;
}

public void setUNIT(BigDecimal uNIT) {
	UNIT = uNIT;
}

}
