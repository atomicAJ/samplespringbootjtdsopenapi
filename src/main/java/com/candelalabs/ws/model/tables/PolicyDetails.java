package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;
import java.sql.Date;

public class PolicyDetails {
	private String PAYORCLIENTNO;
	private String CURRENCY;

	// Triaji: add other fields
	private String COMPANY;
	private String OWNERCLIENTNO;
	// Swati added more fields for Fetch policy Data
	private String POLICYNUMBER;
	private String PRODUCTCODE;
	private String PRODUCTNAME;
	private String POLICYSTATUS;
	private String PREMIUMSTATUSCODE;
	private String OWNERNAME;
	//private String OWERNAME;
	private String LIFECLIENTNO;
	private Date POLICYISSUEDATE;
	private Date RISKCOMMDATE;
	private Date PAIDTODATE;
	private Date BILLTODATE;
	private String PAYMENTMETHOD;
	private String PAYMENTFREQ;
	private String AGENTID;
	private String AGENTNAME;
	private BigDecimal PREMIUMTOTAL;
	private BigDecimal BASICPREMIUM;
	private BigDecimal REGULARTOPUP;
	private Date REINSTATDATE;
	private Date LAPSEDATE;
	private Date SURRENDERDATE;
	
	// Mani: added below field for TAT report
	private String CHANNEL;

	public String getCHANNEL() {
		return CHANNEL;
	}

	public void setCHANNEL(String cHANNEL) {
		CHANNEL = cHANNEL;
	}

	public PolicyDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getOWNERNAME() {
		return OWNERNAME;
	}

	public void setOWNERNAME(String oWNERNAME) {
		OWNERNAME = oWNERNAME;
	}

	public Date getSURRENDERDATE() {
		return SURRENDERDATE;
	}

	public void setSURRENDERDATE(Date sURRENDERDATE) {
		SURRENDERDATE = sURRENDERDATE;
	}

	public Date getLAPSEDATE() {
		return LAPSEDATE;
	}

	public void setLAPSEDATE(Date lAPSEDATE) {
		LAPSEDATE = lAPSEDATE;
	}

	public String getPAYORCLIENTNO() {
		return PAYORCLIENTNO;
	}

	public void setPAYORCLIENTNO(String pAYORCLIENTNO) {
		PAYORCLIENTNO = pAYORCLIENTNO;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getCOMPANY() {
		return COMPANY;
	}

	public void setCOMPANY(String cOMPANY) {
		COMPANY = cOMPANY;
	}

	public String getOWNERCLIENTNO() {
		return OWNERCLIENTNO;
	}

	public void setOWNERCLIENTNO(String oWNERCLIENTNO) {
		OWNERCLIENTNO = oWNERCLIENTNO;
	}

	public String getPOLICYNUMBER() {
		return POLICYNUMBER;
	}

	public void setPOLICYNUMBER(String pOLICYNUMBER) {
		POLICYNUMBER = pOLICYNUMBER;
	}

	public String getPRODUCTCODE() {
		return PRODUCTCODE;
	}

	public void setPRODUCTCODE(String pRODUCTCODE) {
		PRODUCTCODE = pRODUCTCODE;
	}

	public String getPRODUCTNAME() {
		return PRODUCTNAME;
	}

	public void setPRODUCTNAME(String pRODUCTNAME) {
		PRODUCTNAME = pRODUCTNAME;
	}

	public String getPOLICYSTATUS() {
		return POLICYSTATUS;
	}

	public void setPOLICYSTATUS(String pOLICYSTATUS) {
		POLICYSTATUS = pOLICYSTATUS;
	}

	public String getPREMIUMSTATUSCODE() {
		return PREMIUMSTATUSCODE;
	}

	public void setPREMIUMSTATUSCODE(String pREMIUMSTATUSCODE) {
		PREMIUMSTATUSCODE = pREMIUMSTATUSCODE;
	}

/*	public String getOWNERNAME() {
		return OWNERNAME;
	}

	public void setOWNERNAME(String oWNERNAME) {
		OWNERNAME = oWNERNAME;
	}*/

	public String getLIFECLIENTNO() {
		return LIFECLIENTNO;
	}

	public void setLIFECLIENTNO(String lIFECLIENTNO) {
		LIFECLIENTNO = lIFECLIENTNO;
	}

	public Date getPOLICYISSUEDATE() {
		return POLICYISSUEDATE;
	}

	public void setPOLICYISSUEDATE(Date pOLICYISSUEDATE) {
		POLICYISSUEDATE = pOLICYISSUEDATE;
	}

	public Date getRISKCOMMDATE() {
		return RISKCOMMDATE;
	}

	public void setRISKCOMMDATE(Date rISKCOMMDATE) {
		RISKCOMMDATE = rISKCOMMDATE;
	}

	public Date getPAIDTODATE() {
		return PAIDTODATE;
	}

	public void setPAIDTODATE(Date pAIDTODATE) {
		PAIDTODATE = pAIDTODATE;
	}

	public Date getBILLTODATE() {
		return BILLTODATE;
	}

	public void setBILLTODATE(Date bILLTODATE) {
		BILLTODATE = bILLTODATE;
	}

	public String getPAYMENTMETHOD() {
		return PAYMENTMETHOD;
	}

	public void setPAYMENTMETHOD(String pAYMENTMETHOD) {
		PAYMENTMETHOD = pAYMENTMETHOD;
	}

	public String getPAYMENTFREQ() {
		return PAYMENTFREQ;
	}

	public void setPAYMENTFREQ(String pAYMENTFREQ) {
		PAYMENTFREQ = pAYMENTFREQ;
	}

	public String getAGENTID() {
		return AGENTID;
	}

	public void setAGENTID(String aGENTID) {
		AGENTID = aGENTID;
	}

	public String getAGENTNAME() {
		return AGENTNAME;
	}

	public void setAGENTNAME(String aGENTNAME) {
		AGENTNAME = aGENTNAME;
	}

	public BigDecimal getPREMIUMTOTAL() {
		return PREMIUMTOTAL;
	}

	public void setPREMIUMTOTAL(BigDecimal pREMIUMTOTAL) {
		PREMIUMTOTAL = pREMIUMTOTAL;
	}

	public BigDecimal getBASICPREMIUM() {
		return BASICPREMIUM;
	}

	public void setBASICPREMIUM(BigDecimal bASICPREMIUM) {
		BASICPREMIUM = bASICPREMIUM;
	}

	public BigDecimal getREGULARTOPUP() {
		return REGULARTOPUP;
	}

	public void setREGULARTOPUP(BigDecimal rEGULARTOPUP) {
		REGULARTOPUP = rEGULARTOPUP;
	}

	public Date getREINSTATDATE() {
		return REINSTATDATE;
	}

	public void setREINSTATDATE(Date rEINSTATDATE) {
		REINSTATDATE = rEINSTATDATE;
	}

	@Override
	public String toString() {
		return "PolicyDetails [PAYORCLIENTNO=" + PAYORCLIENTNO + ", CURRENCY=" + CURRENCY + ", COMPANY=" + COMPANY
				+ ", OWNERCLIENTNO=" + OWNERCLIENTNO + ", POLICYNUMBER=" + POLICYNUMBER + ", PRODUCTCODE=" + PRODUCTCODE
				+ ", PRODUCTNAME=" + PRODUCTNAME + ", POLICYSTATUS=" + POLICYSTATUS + ", PREMIUMSTATUSCODE="
				+ PREMIUMSTATUSCODE + ", OWNERNAME=" + OWNERNAME + ", LIFECLIENTNO=" + LIFECLIENTNO
				+ ", POLICYISSUEDATE=" + POLICYISSUEDATE + ", RISKCOMMDATE=" + RISKCOMMDATE + ", PAIDTODATE="
				+ PAIDTODATE + ", BILLTODATE=" + BILLTODATE + ", PAYMENTMETHOD=" + PAYMENTMETHOD + ", PAYMENTFREQ="
				+ PAYMENTFREQ + ", AGENTID=" + AGENTID + ", AGENTNAME=" + AGENTNAME + ", PREMIUMTOTAL=" + PREMIUMTOTAL
				+ ", BASICPREMIUM=" + BASICPREMIUM + ", REGULARTOPUP=" + REGULARTOPUP + ", REINSTATDATE=" + REINSTATDATE
				+ ", LAPSEDATE=" + LAPSEDATE + ", SURRENDERDATE=" + SURRENDERDATE + ", CHANNEL=" + CHANNEL + "]";
	}

}
