/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class PolicyRelatedModel {

	private String POLICYNUMBER;
	private String PRODUCTCODE;
	private String PRODUCTNAME;
	private String OWNERNAME;
	private String LIFECLIENTNO;
	private String CLIENTROLE;
	private String POLICYSTATUS;
	private BigDecimal SUMASSURED;
	
	public String getPOLICYNUMBER() {
		return POLICYNUMBER;
	}
	public void setPOLICYNUMBER(String pOLICYNUMBER) {
		POLICYNUMBER = pOLICYNUMBER;
	}
	public String getPRODUCTCODE() {
		return PRODUCTCODE;
	}
	public void setPRODUCTCODE(String pRODUCTCODE) {
		PRODUCTCODE = pRODUCTCODE;
	}
	public String getPRODUCTNAME() {
		return PRODUCTNAME;
	}
	public void setPRODUCTNAME(String pRODUCTNAME) {
		PRODUCTNAME = pRODUCTNAME;
	}
	public String getOWNERNAME() {
		return OWNERNAME;
	}
	public void setOWNERNAME(String oWNERNAME) {
		OWNERNAME = oWNERNAME;
	}
	public String getLIFECLIENTNO() {
		return LIFECLIENTNO;
	}
	public void setLIFECLIENTNO(String lIFECLIENTNO) {
		LIFECLIENTNO = lIFECLIENTNO;
	}
	public String getCLIENTROLE() {
		return CLIENTROLE;
	}
	public void setCLIENTROLE(String cLIENTROLE) {
		CLIENTROLE = cLIENTROLE;
	}
	public String getPOLICYSTATUS() {
		return POLICYSTATUS;
	}
	public void setPOLICYSTATUS(String pOLICYSTATUS) {
		POLICYSTATUS = pOLICYSTATUS;
	}
	public BigDecimal getSUMASSURED() {
		return SUMASSURED;
	}
	public void setSUMASSURED(BigDecimal sUMASSURED) {
		SUMASSURED = sUMASSURED;
	}
	@Override
	public String toString() {
		return "PolicyRelatedModel [POLICYNUMBER=" + POLICYNUMBER + ", PRODUCTCODE=" + PRODUCTCODE + ", PRODUCTNAME="
				+ PRODUCTNAME + ", OWNERNAME=" + OWNERNAME + ", LIFECLIENTNO=" + LIFECLIENTNO + ", CLIENTROLE="
				+ CLIENTROLE + ", POLICYSTATUS=" + POLICYSTATUS + ", SUMASSURED=" + SUMASSURED + "]";
	}

}
