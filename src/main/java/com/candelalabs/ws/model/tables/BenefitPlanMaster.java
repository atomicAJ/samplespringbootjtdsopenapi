/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class BenefitPlanMaster {
	private String BENEFITCODE;
	private String BENEFITNAME;
	private String BENEFITPLAN;
	private String TPABENEFITCODE;
	private String TPABENEFITDESC;
	private String COMPONENTCODE;
	private BigDecimal AMOUNTPERDAY;
	private BigDecimal AMOUNTANNUALLY;
	private BigDecimal TOTALNOOFDAYS;
	
	
	public BenefitPlanMaster() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getBENEFITCODE() {
		return BENEFITCODE;
	}
	public void setBENEFITCODE(String bENEFITCODE) {
		BENEFITCODE = bENEFITCODE;
	}
	public String getBENEFITNAME() {
		return BENEFITNAME;
	}
	public void setBENEFITNAME(String bENEFITNAME) {
		BENEFITNAME = bENEFITNAME;
	}
	public String getBENEFITPLAN() {
		return BENEFITPLAN;
	}
	public void setBENEFITPLAN(String bENEFITPLAN) {
		BENEFITPLAN = bENEFITPLAN;
	}
	public String getTPABENEFITCODE() {
		return TPABENEFITCODE;
	}
	public void setTPABENEFITCODE(String tPABENEFITCODE) {
		TPABENEFITCODE = tPABENEFITCODE;
	}
	public String getTPABENEFITDESC() {
		return TPABENEFITDESC;
	}
	public void setTPABENEFITDESC(String tPABENEFITDESC) {
		TPABENEFITDESC = tPABENEFITDESC;
	}
	public String getCOMPONENTCODE() {
		return COMPONENTCODE;
	}
	public void setCOMPONENTCODE(String cOMPONENTCODE) {
		COMPONENTCODE = cOMPONENTCODE;
	}
	public BigDecimal getAMOUNTPERDAY() {
		return AMOUNTPERDAY;
	}
	public void setAMOUNTPERDAY(BigDecimal aMOUNTPERDAY) {
		AMOUNTPERDAY = aMOUNTPERDAY;
	}
	public BigDecimal getAMOUNTANNUALLY() {
		return AMOUNTANNUALLY;
	}
	public void setAMOUNTANNUALLY(BigDecimal aMOUNTANNUALLY) {
		AMOUNTANNUALLY = aMOUNTANNUALLY;
	}
	public BigDecimal getTOTALNOOFDAYS() {
		return TOTALNOOFDAYS;
	}
	public void setTOTALNOOFDAYS(BigDecimal tOTALNOOFDAYS) {
		TOTALNOOFDAYS = tOTALNOOFDAYS;
	}

}
