/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;
import java.sql.Date;
//\import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author Triaji New Claim Data Table Design
 *
 */
public class ClaimDataNewModel {

	private String ClaimNumber;
	private String ClaimNumberLA;
	private String PolicyNumber;
	private String ProductCode;
	private String ClaimTypeLA;
	private String ClaimTypeUI;
	private String PlanCode;
	private Date ReceivedDate;
	private Date RegisterDate;
	private int ProcessInitiatedFlag;
	// private Timestamp ProcessInitiatonTS;
	private Timestamp ProcessInitiatonTS;
	private Timestamp ProcessEndTS;
	private String ProcessInitiatedRemarks;
	private String InitiateProcess;
	private String ProcessInstanceID;
	private String InsuredClientID;
	private String InsuredClientName;
	private String OwnerClientID;
	private String TPAClaimNumber;
	private String TPAClaimStatus;
	private String BatchNumber;
	private String TPAProviderCode;
	private Date AdmissionDate;
	private Date DischargeDate;
	private BigDecimal StayDuration;
	private String ComponentCode;
	private BigDecimal ComponentSumAssured;
	private String DiagnosisCode;
	private String DiagnosisName;
	private BigDecimal ClaimInvoiceAmount;
	private BigDecimal ClaimInvoiceAmountUI;
	private BigDecimal ClaimApprovedAmountTPA;
	private BigDecimal ClaimApprovedAmountLA;
	private BigDecimal ClaimApprovedAmountUI;
	private BigDecimal ClaimSumAssuredPercentage;
	private int ClaimSTPFlag;
	private String FullyClaimed;
	private String ClaimStatus;
	//private String PendingRequirementActivityID;
	private String ClaimUser;
	private String ClaimProcessor;
	private String ClaimProcessorName;
	private String ClaimApprovalHAUser;
	private String ClaimApprovalHAUserName;
	private String ClaimRejectHAUser;
	private String ClaimRejectHAUserName;
	private String ClaimUWUser;
	private String ClaimHUWUser;
	private Date ClaimDecisionDate;
	private String ClaimDecision;
	private String ProviderNameSource;
	private String ProviderCode;
	private String ProviderName;
	private String PayeeName;
	private String BankCode;
	private String BankType;
	private String BankAccountNumber;
	private String BankAccountName;
	private String InProgressClaimNumber;
	private String DoctorName;
	private String ClaimCurrency;
	private String FactoringHouse;
	private Date CurrentFrom;
	private String BenefitCategory;
	private BigDecimal OSCharges;
	private BigDecimal OtherAdjustments;
	private String AdjustmentCode;
	private BigDecimal PolicyLoanAmount;
	private BigDecimal PolicyDebtAmount;
	private String ClaimCancelRemark;
	private String SurgreryPerformed;
	private String PortalClaimRequestNumber;
	private String UISaveFlag;
	//private String PendingInvesReptActivityID;
	private Date ClaimPayDate;
	private String ClaimTypeLAName;

	/**
	 * 
	 */

	public ClaimDataNewModel() {
		// TODO Auto-generated constructor stub
	}

	public Date getClaimPayDate() {
		return ClaimPayDate;
	}

	public void setClaimPayDate(Date claimPayDate) {
		ClaimPayDate = claimPayDate;
	}

	public String getPortalClaimRequestNumber() {
		return PortalClaimRequestNumber;
	}

	public void setPortalClaimRequestNumber(String portalClaimRequestNumber) {
		PortalClaimRequestNumber = portalClaimRequestNumber;
	}

	public Timestamp getProcessInitiatonTS() {
		return ProcessInitiatonTS;
	}

	public void setProcessInitiatonTS(Timestamp processInitiatonTS) {
		ProcessInitiatonTS = processInitiatonTS;
	}

	public String getSurgreryPerformed() {
		return SurgreryPerformed;
	}

	public void setSurgreryPerformed(String surgreryPerformed) {
		SurgreryPerformed = surgreryPerformed;
	}

	public String getClaimCancelRemark() {
		return ClaimCancelRemark;
	}

	public void setClaimCancelRemark(String claimCancelRemark) {
		ClaimCancelRemark = claimCancelRemark;
	}

	public String getClaimNumber() {
		return ClaimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		ClaimNumber = claimNumber;
	}


	public String getPolicyNumber() {
		return PolicyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		PolicyNumber = policyNumber;
	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getClaimTypeLA() {
		return ClaimTypeLA;
	}

	public void setClaimTypeLA(String claimTypeLA) {
		ClaimTypeLA = claimTypeLA;
	}

	public String getClaimTypeUI() {
		return ClaimTypeUI;
	}

	public void setClaimTypeUI(String claimTypeUI) {
		ClaimTypeUI = claimTypeUI;
	}

	public String getPlanCode() {
		return PlanCode;
	}

	public void setPlanCode(String planCode) {
		PlanCode = planCode;
	}

	public Date getReceivedDate() {
		return ReceivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		ReceivedDate = receivedDate;
	}

	public Date getRegisterDate() {
		return RegisterDate;
	}

	public void setRegisterDate(Date registerDate) {
		RegisterDate = registerDate;
	}

	public int getProcessInitiatedFlag() {
		return ProcessInitiatedFlag;
	}

	public void setProcessInitiatedFlag(int processInitiatedFlag) {
		ProcessInitiatedFlag = processInitiatedFlag;
	}

	/*
	 * public Timestamp getProcessInitiatonTS() { return ProcessInitiatonTS; }
	 * 
	 * public void setProcessInitiatonTS(Timestamp processInitiatonTS) {
	 * ProcessInitiatonTS = processInitiatonTS; }
	 */

	public Timestamp getProcessEndTS() {
		return ProcessEndTS;
	}

	public void setProcessEndTS(Timestamp processEndTS) {
		ProcessEndTS = processEndTS;
	}

	public String getProcessInitiatedRemarks() {
		return ProcessInitiatedRemarks;
	}

	public void setProcessInitiatedRemarks(String processInitiatedRemarks) {
		ProcessInitiatedRemarks = processInitiatedRemarks;
	}

	public String getInitiateProcess() {
		return InitiateProcess;
	}

	public void setInitiateProcess(String initiateProcess) {
		InitiateProcess = initiateProcess;
	}

	public String getProcessInstanceID() {
		return ProcessInstanceID;
	}

	public void setProcessInstanceID(String processInstanceID) {
		ProcessInstanceID = processInstanceID;
	}

	public String getInsuredClientID() {
		return InsuredClientID;
	}

	public void setInsuredClientID(String insuredClientID) {
		InsuredClientID = insuredClientID;
	}

	public String getOwnerClientID() {
		return OwnerClientID;
	}

	public void setOwnerClientID(String ownerClientID) {
		OwnerClientID = ownerClientID;
	}

	public String getTPAClaimNumber() {
		return TPAClaimNumber;
	}

	public void setTPAClaimNumber(String tPAClaimNumber) {
		TPAClaimNumber = tPAClaimNumber;
	}

	public String getTPAClaimStatus() {
		return TPAClaimStatus;
	}

	public void setTPAClaimStatus(String tPAClaimStatus) {
		TPAClaimStatus = tPAClaimStatus;
	}

	public String getTPAProviderCode() {
		return TPAProviderCode;
	}

	public void setTPAProviderCode(String tPAProviderCode) {
		TPAProviderCode = tPAProviderCode;
	}

	public Date getAdmissionDate() {
		return AdmissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		AdmissionDate = admissionDate;
	}

	public Date getDischargeDate() {
		return DischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		DischargeDate = dischargeDate;
	}


	public String getComponentCode() {
		return ComponentCode;
	}

	public void setComponentCode(String componentCode) {
		ComponentCode = componentCode;
	}

	public String getDiagnosisCode() {
		return DiagnosisCode;
	}

	public void setDiagnosisCode(String diagnosisCode) {
		DiagnosisCode = diagnosisCode;
	}

	public BigDecimal getClaimInvoiceAmount() {
		return ClaimInvoiceAmount;
	}

	public void setClaimInvoiceAmount(BigDecimal claimInvoiceAmount) {
		ClaimInvoiceAmount = claimInvoiceAmount;
	}

	public BigDecimal getClaimApprovedAmountTPA() {
		return ClaimApprovedAmountTPA;
	}

	public void setClaimApprovedAmountTPA(BigDecimal claimApprovedAmountTPA) {
		ClaimApprovedAmountTPA = claimApprovedAmountTPA;
	}

	public BigDecimal getClaimApprovedAmountLA() {
		return ClaimApprovedAmountLA;
	}

	public void setClaimApprovedAmountLA(BigDecimal claimApprovedAmountLA) {
		ClaimApprovedAmountLA = claimApprovedAmountLA;
	}

	public BigDecimal getClaimApprovedAmountUI() {
		return ClaimApprovedAmountUI;
	}

	public void setClaimApprovedAmountUI(BigDecimal claimApprovedAmountUI) {
		ClaimApprovedAmountUI = claimApprovedAmountUI;
	}

	public int getClaimSTPFlag() {
		return ClaimSTPFlag;
	}

	public void setClaimSTPFlag(int claimSTPFlag) {
		ClaimSTPFlag = claimSTPFlag;
	}

	public String getFullyClaimed() {
		return FullyClaimed;
	}

	public void setFullyClaimed(String fullyClaimed) {
		FullyClaimed = fullyClaimed;
	}

	public String getClaimStatus() {
		return ClaimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		ClaimStatus = claimStatus;
	}



	public String getClaimProcessor() {
		return ClaimProcessor;
	}

	public void setClaimProcessor(String claimProcessor) {
		ClaimProcessor = claimProcessor;
	}

	public Date getClaimDecisionDate() {
		return ClaimDecisionDate;
	}

	public void setClaimDecisionDate(Date claimDecisionDate) {
		ClaimDecisionDate = claimDecisionDate;
	}

	public String getClaimDecision() {
		return ClaimDecision;
	}

	public void setClaimDecision(String claimDecision) {
		ClaimDecision = claimDecision;
	}

	public String getProviderCode() {
		return ProviderCode;
	}

	public void setProviderCode(String providerCode) {
		ProviderCode = providerCode;
	}

	public String getProviderName() {
		return ProviderName;
	}

	public void setProviderName(String providerName) {
		ProviderName = providerName;
	}

	public String getBankAccountNumber() {
		return BankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		BankAccountNumber = bankAccountNumber;
	}

	public String getBankAccountName() {
		return BankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		BankAccountName = bankAccountName;
	}

	public BigDecimal getComponentSumAssured() {
		return ComponentSumAssured;
	}

	public void setComponentSumAssured(BigDecimal componentSumAssured) {
		ComponentSumAssured = componentSumAssured;
	}

	public String getDiagnosisName() {
		return DiagnosisName;
	}

	public void setDiagnosisName(String diagnosisName) {
		DiagnosisName = diagnosisName;
	}

	public BigDecimal getClaimInvoiceAmountUI() {
		return ClaimInvoiceAmountUI;
	}

	public void setClaimInvoiceAmountUI(BigDecimal claimInvoiceAmountUI) {
		ClaimInvoiceAmountUI = claimInvoiceAmountUI;
	}

	public BigDecimal getClaimSumAssuredPercentage() {
		return ClaimSumAssuredPercentage;
	}

	public void setClaimSumAssuredPercentage(BigDecimal claimSumAssuredPercentage) {
		ClaimSumAssuredPercentage = claimSumAssuredPercentage;
	}

	public String getClaimUser() {
		return ClaimUser;
	}

	public void setClaimUser(String claimUser) {
		ClaimUser = claimUser;
	}

	public String getClaimProcessorName() {
		return ClaimProcessorName;
	}

	public void setClaimProcessorName(String claimProcessorName) {
		ClaimProcessorName = claimProcessorName;
	}

	public String getClaimApprovalHAUser() {
		return ClaimApprovalHAUser;
	}

	public void setClaimApprovalHAUser(String claimApprovalHAUser) {
		ClaimApprovalHAUser = claimApprovalHAUser;
	}

	public String getClaimApprovalHAUserName() {
		return ClaimApprovalHAUserName;
	}

	public void setClaimApprovalHAUserName(String claimApprovalHAUserName) {
		ClaimApprovalHAUserName = claimApprovalHAUserName;
	}

	public String getClaimRejectHAUser() {
		return ClaimRejectHAUser;
	}

	public void setClaimRejectHAUser(String claimRejectHAUser) {
		ClaimRejectHAUser = claimRejectHAUser;
	}

	public String getClaimRejectHAUserName() {
		return ClaimRejectHAUserName;
	}

	public void setClaimRejectHAUserName(String claimRejectHAUserName) {
		ClaimRejectHAUserName = claimRejectHAUserName;
	}

	public String getClaimHUWUser() {
		return ClaimHUWUser;
	}

	public void setClaimHUWUser(String claimHUWUser) {
		ClaimHUWUser = claimHUWUser;
	}

	public String getProviderNameSource() {
		return ProviderNameSource;
	}

	public void setProviderNameSource(String providerNameSource) {
		ProviderNameSource = providerNameSource;
	}

	public String getPayeeName() {
		return PayeeName;
	}

	public void setPayeeName(String payeeName) {
		PayeeName = payeeName;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getBankType() {
		return BankType;
	}

	public void setBankType(String bankType) {
		BankType = bankType;
	}

	public String getInProgressClaimNumber() {
		return InProgressClaimNumber;
	}

	public void setInProgressClaimNumber(String inProgressClaimNumber) {
		InProgressClaimNumber = inProgressClaimNumber;
	}

	public String getDoctorName() {
		return DoctorName;
	}

	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

	public String getClaimCurrency() {
		return ClaimCurrency;
	}

	public void setClaimCurrency(String claimCurrency) {
		ClaimCurrency = claimCurrency;
	}

	public String getFactoringHouse() {
		return FactoringHouse;
	}

	public void setFactoringHouse(String factoringHouse) {
		FactoringHouse = factoringHouse;
	}

	public Date getCurrentFrom() {
		return CurrentFrom;
	}

	public void setCurrentFrom(Date currentFrom) {
		CurrentFrom = currentFrom;
	}

	public String getBenefitCategory() {
		return BenefitCategory;
	}

	public void setBenefitCategory(String benefitCategory) {
		BenefitCategory = benefitCategory;
	}

	public String getInsuredClientName() {
		return InsuredClientName;
	}

	public void setInsuredClientName(String insuredClientName) {
		InsuredClientName = insuredClientName;
	}

	public BigDecimal getOSCharges() {
		return OSCharges;
	}

	public void setOSCharges(BigDecimal oSCharges) {
		OSCharges = oSCharges;
	}

	public BigDecimal getOtherAdjustments() {
		return OtherAdjustments;
	}

	public void setOtherAdjustments(BigDecimal otherAdjustments) {
		OtherAdjustments = otherAdjustments;
	}

	public String getAdjustmentCode() {
		return AdjustmentCode;
	}

	public void setAdjustmentCode(String adjustmentCode) {
		AdjustmentCode = adjustmentCode;
	}

	public BigDecimal getPolicyLoanAmount() {
		return PolicyLoanAmount;
	}

	public void setPolicyLoanAmount(BigDecimal policyLoanAmount) {
		PolicyLoanAmount = policyLoanAmount;
	}

	public BigDecimal getPolicyDebtAmount() {
		return PolicyDebtAmount;
	}

	public void setPolicyDebtAmount(BigDecimal policyDebtAmount) {
		PolicyDebtAmount = policyDebtAmount;
	}

	public String getBatchNumber() {
		return BatchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		BatchNumber = batchNumber;
	}

	public String getClaimUWUser() {
		return ClaimUWUser;
	}

	public void setClaimUWUser(String claimUWUser) {
		ClaimUWUser = claimUWUser;
	}

	public String getUISaveFlag() {
		return UISaveFlag;
	}

	public void setUISaveFlag(String uISaveFlag) {
		UISaveFlag = uISaveFlag;
	}

	public String getClaimNumberLA() {
		return ClaimNumberLA;
	}

	public void setClaimNumberLA(String claimNumberLA) {
		ClaimNumberLA = claimNumberLA;
	}



	public BigDecimal getStayDuration() {
		return StayDuration;
	}

	public void setStayDuration(BigDecimal stayDuration) {
		StayDuration = stayDuration;
	}

	public String getClaimTypeLAName() {
		return ClaimTypeLAName;
	}

	public void setClaimTypeLAName(String claimTypeLAName) {
		ClaimTypeLAName = claimTypeLAName;
	}
}
