/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class DiagnosisMasterModel {

	private String DIAGNOSISCODE;
	private String DIAGNOSISNAME;
	private String CLAIMTYPE;
	private BigDecimal CLAIMPERCENTAGE;

	public DiagnosisMasterModel() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getDIAGNOSISNAME() {
		return DIAGNOSISNAME;
	}

	public void setDIAGNOSISNAME(String dIAGNOSISNAME) {
		DIAGNOSISNAME = dIAGNOSISNAME;
	}

	public String getCLAIMTYPE() {
		return CLAIMTYPE;
	}

	public void setCLAIMTYPE(String cLAIMTYPE) {
		CLAIMTYPE = cLAIMTYPE;
	}

	public BigDecimal getCLAIMPERCENTAGE() {
		return CLAIMPERCENTAGE;
	}

	public void setCLAIMPERCENTAGE(BigDecimal cLAIMPERCENTAGE) {
		CLAIMPERCENTAGE = cLAIMPERCENTAGE;
	}


	public String getDIAGNOSISCODE() {
		return DIAGNOSISCODE;
	}


	public void setDIAGNOSISCODE(String dIAGNOSISCODE) {
		DIAGNOSISCODE = dIAGNOSISCODE;
	}

}
