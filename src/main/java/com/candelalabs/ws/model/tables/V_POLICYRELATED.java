package com.candelalabs.ws.model.tables;

public class V_POLICYRELATED {
private String	POLICYNUMBER;
private String	PRODUCTCODE;
private String PRODUCTNAME;
private String	OWNERNAME;
private String	LIFECLIENTNO;
private String	CLIENTROLE;
private String	POLICYSTATUS;
private String	SUMASSURED;
public String getPOLICYNUMBER() {
	return POLICYNUMBER;
}
public void setPOLICYNUMBER(String pOLICYNUMBER) {
	POLICYNUMBER = pOLICYNUMBER;
}
public String getPRODUCTCODE() {
	return PRODUCTCODE;
}
public void setPRODUCTCODE(String pRODUCTCODE) {
	PRODUCTCODE = pRODUCTCODE;
}
public String getPRODUCTNAME() {
	return PRODUCTNAME;
}
public void setPRODUCTNAME(String pRODUCTNAME) {
	PRODUCTNAME = pRODUCTNAME;
}
public String getOWNERNAME() {
	return OWNERNAME;
}
public void setOWNERNAME(String oWNERNAME) {
	OWNERNAME = oWNERNAME;
}
public String getLIFECLIENTNO() {
	return LIFECLIENTNO;
}
public void setLIFECLIENTNO(String lIFECLIENTNO) {
	LIFECLIENTNO = lIFECLIENTNO;
}
public String getCLIENTROLE() {
	return CLIENTROLE;
}
public void setCLIENTROLE(String cLIENTROLE) {
	CLIENTROLE = cLIENTROLE;
}
public String getPOLICYSTATUS() {
	return POLICYSTATUS;
}
public void setPOLICYSTATUS(String pOLICYSTATUS) {
	POLICYSTATUS = pOLICYSTATUS;
}
public String getSUMASSURED() {
	return SUMASSURED;
}
public void setSUMASSURED(String sUMASSURED) {
	SUMASSURED = sUMASSURED;
}



}
