/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class AuthorityMatrixModel {

	private String UserDecision;
	private String ComparisonOperation;
	private BigDecimal TotalClaimAmount;
	private String ClaimTypeUI;
	
	public String getUserDecision() {
		return UserDecision;
	}
	public void setUserDecision(String userDecision) {
		UserDecision = userDecision;
	}
	
	public BigDecimal getTotalClaimAmount() {
		return TotalClaimAmount;
	}
	public void setTotalClaimAmount(BigDecimal totalClaimAmount) {
		TotalClaimAmount = totalClaimAmount;
	}
	public String getClaimTypeUI() {
		return ClaimTypeUI;
	}
	public void setClaimTypeUI(String claimTypeUI) {
		ClaimTypeUI = claimTypeUI;
	}
	public String getComparisonOperation() {
		return ComparisonOperation;
	}
	public void setComparisonOperation(String comparisonOperation) {
		ComparisonOperation = comparisonOperation;
	}

}
