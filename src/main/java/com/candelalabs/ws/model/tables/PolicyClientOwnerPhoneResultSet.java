package com.candelalabs.ws.model.tables;

public class PolicyClientOwnerPhoneResultSet {
	private String PHONENO01;
	private String PHONENO02;
	private String PHONENO03;
	public String getPHONENO01() {
		return PHONENO01;
	}
	public void setPHONENO01(String pHONENO01) {
		PHONENO01 = pHONENO01;
	}
	public String getPHONENO02() {
		return PHONENO02;
	}
	public void setPHONENO02(String pHONENO02) {
		PHONENO02 = pHONENO02;
	}
	public String getPHONENO03() {
		return PHONENO03;
	}
	public void setPHONENO03(String pHONENO03) {
		PHONENO03 = pHONENO03;
	}
	
	
}
