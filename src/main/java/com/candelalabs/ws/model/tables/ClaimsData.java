package com.candelalabs.ws.model.tables;

import java.util.Date;

public class ClaimsData {

	private String sourcePayorId;
	private String sourceCorporateId;
	private String sourcePolicyNumber;
	private String sourceMemberID;
	private String sourceNik;
	private String sourceBranchCode;
	private String sourceCardNo;
	private String sourceMemberName;
	private String sourceClaimId;
	private String sourceClaimType;
	private Short sourceClaimStatus;
	private String sourceProviderCode;
	private String sourceAdmissionDate;
	private String sourceDischargeDate;
	private Short sourceDurationDays;
	private String sourceCoverageId;
	private String sourcePlanId;
	private Integer sourceDisabilityNo;
	private String sourceDiagnosisCode;
	private String sourceSecondaryDiagnosisCode;
	private Integer sourceAmtIncurred;
	private Integer sourceAmtApproved;
	private Integer sourceAmtNotApproved;
	private Integer sourceAmtAsoApproved;
	private Short sourceOnPlanHighPlan;
	private String sourceRemarks;
	private Integer sourceProviderExcessPaid;
	private String sourcePayorInvoiceId;
	private String sourceHospitalInvoiceDate;
	private String sourceHospitalInvoiceNo;
	private String sourceReceivedDate;
	private String sourceSubmissionDate;
	private String sourceVerifyBy;
	private String sourceBenefitCode;
	private String sourceFatca;
	private Date claimProcessDate;
	private String claimProcessId;
	private Integer processInitiated;
	private String claimNo;
	private String claimCancelRemark;
	private String stpFlag;
	private String claimDatasource;
	private String clientPECCheck;
	private String sourceCurrency;
	private String sourceDoctorName;
	private String sourceBankAccountNumber;
	private String sourcePayeeName;
	private String sourceBankName;
    private String Other_Adjustments;
    private String  Cause_Of_Death;
    private String sourceBankType;
    private String current_from;
    private String insured_client_id;
	public ClaimsData() {
		super();
	}
	
	public ClaimsData(String sourcePayorId, String sourceCorporateId, String sourcePolicyNumber, String sourceMemberID,
			String sourceNik, String sourceBranchCode, String sourceCardNo, String sourceMemberName,
			String sourceClaimId, String sourceClaimType, Short sourceClaimStatus, String sourceProviderCode,
			String sourceAdmissionDate, String sourceDischargeDate, Short sourceDurationDays, String sourceCoverageId,
			String sourcePlanId, Integer sourceDisabilityNo, String sourceDiagnosisCode,
			String sourceSecondaryDiagnosisCode, Integer sourceAmtIncurred, Integer sourceAmtApproved,
			Integer sourceAmtNotApproved, Integer sourceAmtAsoApproved, Short sourceOnPlanHighPlan,
			String sourceRemarks, Integer sourceProviderExcessPaid, String sourcePayorInvoiceId,
			String sourceHospitalInvoiceDate, String sourceHospitalInvoiceNo, String sourceReceivedDate,
			String sourceSubmissionDate, String sourceVerifyBy, String sourceBenefitCode, String sourceFatca,
			Date claimProcessDate, String claimProcessId, Integer processInitiated, String claimNo,
			String claimCancelRemark, String stpFlag, String claimDatasource, String clientPECCheck,
			String sourceCurrency, String sourceDoctorName, String sourceBankAccountNumber, String sourcePayeeName,
			String sourceBankName,String Other_Adjustments,String Cause_Of_Death,String sourceBankType, String current_from,String insured_client_id) {
		super();
		this.sourcePayorId = sourcePayorId;
		this.sourceCorporateId = sourceCorporateId;
		this.sourcePolicyNumber = sourcePolicyNumber;
		this.sourceMemberID = sourceMemberID;
		this.sourceNik = sourceNik;
		this.sourceBranchCode = sourceBranchCode;
		this.sourceCardNo = sourceCardNo;
		this.sourceMemberName = sourceMemberName;
		this.sourceClaimId = sourceClaimId;
		this.sourceClaimType = sourceClaimType;
		this.sourceClaimStatus = sourceClaimStatus;
		this.sourceProviderCode = sourceProviderCode;
		this.sourceAdmissionDate = sourceAdmissionDate;
		this.sourceDischargeDate = sourceDischargeDate;
		this.sourceDurationDays = sourceDurationDays;
		this.sourceCoverageId = sourceCoverageId;
		this.sourcePlanId = sourcePlanId;
		this.sourceDisabilityNo = sourceDisabilityNo;
		this.sourceDiagnosisCode = sourceDiagnosisCode;
		this.sourceSecondaryDiagnosisCode = sourceSecondaryDiagnosisCode;
		this.sourceAmtIncurred = sourceAmtIncurred;
		this.sourceAmtApproved = sourceAmtApproved;
		this.sourceAmtNotApproved = sourceAmtNotApproved;
		this.sourceAmtAsoApproved = sourceAmtAsoApproved;
		this.sourceOnPlanHighPlan = sourceOnPlanHighPlan;
		this.sourceRemarks = sourceRemarks;
		this.sourceProviderExcessPaid = sourceProviderExcessPaid;
		this.sourcePayorInvoiceId = sourcePayorInvoiceId;
		this.sourceHospitalInvoiceDate = sourceHospitalInvoiceDate;
		this.sourceHospitalInvoiceNo = sourceHospitalInvoiceNo;
		this.sourceReceivedDate = sourceReceivedDate;
		this.sourceSubmissionDate = sourceSubmissionDate;
		this.sourceVerifyBy = sourceVerifyBy;
		this.sourceBenefitCode = sourceBenefitCode;
		this.sourceFatca = sourceFatca;
		this.claimProcessDate = claimProcessDate;
		this.claimProcessId = claimProcessId;
		this.processInitiated = processInitiated;
		this.claimNo = claimNo;
		this.claimCancelRemark = claimCancelRemark;
		this.stpFlag = stpFlag;
		this.claimDatasource = claimDatasource;
		this.clientPECCheck = clientPECCheck;
		this.sourceCurrency = sourceCurrency;
		this.sourceDoctorName = sourceDoctorName;
		this.sourceBankAccountNumber = sourceBankAccountNumber;
		this.sourcePayeeName = sourcePayeeName;
		this.sourceBankName = sourceBankName;
		this.Other_Adjustments= Other_Adjustments;
		this.Cause_Of_Death= Cause_Of_Death;
		this.sourceBankType = sourceBankType;
		this.current_from = current_from;
		this.insured_client_id = insured_client_id;
	}



	public String getSourcePayorId() {
		return sourcePayorId;
	}

	public void setSourcePayorId(String sourcePayorId) {
		this.sourcePayorId = sourcePayorId;
	}

	public String getSourceCorporateId() {
		return sourceCorporateId;
	}

	public void setSourceCorporateId(String sourceCorporateId) {
		this.sourceCorporateId = sourceCorporateId;
	}

	public String getSourcePolicyNumber() {
		return sourcePolicyNumber;
	}

	public void setSourcePolicyNumber(String sourcePolicyNumber) {
		this.sourcePolicyNumber = sourcePolicyNumber;
	}

	public String getSourceMemberID() {
		return sourceMemberID;
	}

	public void setSourceMemberID(String sourceMemberID) {
		this.sourceMemberID = sourceMemberID;
	}

	public String getSourceNik() {
		return sourceNik;
	}

	public void setSourceNik(String sourceNik) {
		this.sourceNik = sourceNik;
	}

	public String getSourceBranchCode() {
		return sourceBranchCode;
	}

	public void setSourceBranchCode(String sourceBranchCode) {
		this.sourceBranchCode = sourceBranchCode;
	}

	public String getSourceCardNo() {
		return sourceCardNo;
	}

	public void setSourceCardNo(String sourceCardNo) {
		this.sourceCardNo = sourceCardNo;
	}

	public String getSourceMemberName() {
		return sourceMemberName;
	}

	public void setSourceMemberName(String sourceMemberName) {
		this.sourceMemberName = sourceMemberName;
	}

	public String getSourceClaimId() {
		return sourceClaimId;
	}

	public void setSourceClaimId(String sourceClaimId) {
		this.sourceClaimId = sourceClaimId;
	}

	public String getSourceClaimType() {
		return sourceClaimType;
	}

	public void setSourceClaimType(String sourceClaimType) {
		this.sourceClaimType = sourceClaimType;
	}

	public Short getSourceClaimStatus() {
		return sourceClaimStatus;
	}

	public void setSourceClaimStatus(Short sourceClaimStatus) {
		this.sourceClaimStatus = sourceClaimStatus;
	}

	public String getSourceProviderCode() {
		return sourceProviderCode;
	}

	public void setSourceProviderCode(String sourceProviderCode) {
		this.sourceProviderCode = sourceProviderCode;
	}

	public String getSourceAdmissionDate() {
		return sourceAdmissionDate;
	}

	public void setSourceAdmissionDate(String sourceAdmissionDate) {
		this.sourceAdmissionDate = sourceAdmissionDate;
	}

	public String getSourceDischargeDate() {
		return sourceDischargeDate;
	}

	public void setSourceDischargeDate(String sourceDischargeDate) {
		this.sourceDischargeDate = sourceDischargeDate;
	}

	public Short getSourceDurationDays() {
		return sourceDurationDays;
	}

	public void setSourceDurationDays(Short sourceDurationDays) {
		this.sourceDurationDays = sourceDurationDays;
	}

	public String getSourceCoverageId() {
		return sourceCoverageId;
	}

	public void setSourceCoverageId(String sourceCoverageId) {
		this.sourceCoverageId = sourceCoverageId;
	}

	public String getSourcePlanId() {
		return sourcePlanId;
	}

	public void setSourcePlanId(String sourcePlanId) {
		this.sourcePlanId = sourcePlanId;
	}

	public Integer getSourceDisabilityNo() {
		return sourceDisabilityNo;
	}

	public void setSourceDisabilityNo(Integer sourceDisabilityNo) {
		this.sourceDisabilityNo = sourceDisabilityNo;
	}

	public String getSourceDiagnosisCode() {
		return sourceDiagnosisCode;
	}

	public void setSourceDiagnosisCode(String sourceDiagnosisCode) {
		this.sourceDiagnosisCode = sourceDiagnosisCode;
	}

	public String getSourceSecondaryDiagnosisCode() {
		return sourceSecondaryDiagnosisCode;
	}

	public void setSourceSecondaryDiagnosisCode(String sourceSecondaryDiagnosisCode) {
		this.sourceSecondaryDiagnosisCode = sourceSecondaryDiagnosisCode;
	}

	public Integer getSourceAmtIncurred() {
		return sourceAmtIncurred;
	}

	public void setSourceAmtIncurred(Integer sourceAmtIncurred) {
		this.sourceAmtIncurred = sourceAmtIncurred;
	}

	public Integer getSourceAmtApproved() {
		return sourceAmtApproved;
	}

	public void setSourceAmtApproved(Integer sourceAmtApproved) {
		this.sourceAmtApproved = sourceAmtApproved;
	}

	public Integer getSourceAmtNotApproved() {
		return sourceAmtNotApproved;
	}

	public void setSourceAmtNotApproved(Integer sourceAmtNotApproved) {
		this.sourceAmtNotApproved = sourceAmtNotApproved;
	}

	public Integer getSourceAmtAsoApproved() {
		return sourceAmtAsoApproved;
	}

	public void setSourceAmtAsoApproved(Integer sourceAmtAsoApproved) {
		this.sourceAmtAsoApproved = sourceAmtAsoApproved;
	}

	public Short getSourceOnPlanHighPlan() {
		return sourceOnPlanHighPlan;
	}

	public void setSourceOnPlanHighPlan(Short sourceOnPlanHighPlan) {
		this.sourceOnPlanHighPlan = sourceOnPlanHighPlan;
	}

	public String getSourceRemarks() {
		return sourceRemarks;
	}

	public void setSourceRemarks(String sourceRemarks) {
		this.sourceRemarks = sourceRemarks;
	}

	public Integer getSourceProviderExcessPaid() {
		return sourceProviderExcessPaid;
	}

	public void setSourceProviderExcessPaid(Integer sourceProviderExcessPaid) {
		this.sourceProviderExcessPaid = sourceProviderExcessPaid;
	}

	public String getSourcePayorInvoiceId() {
		return sourcePayorInvoiceId;
	}

	public void setSourcePayorInvoiceId(String sourcePayorInvoiceId) {
		this.sourcePayorInvoiceId = sourcePayorInvoiceId;
	}

	public String getSourceHospitalInvoiceDate() {
		return sourceHospitalInvoiceDate;
	}

	public void setSourceHospitalInvoiceDate(String sourceHospitalInvoiceDate) {
		this.sourceHospitalInvoiceDate = sourceHospitalInvoiceDate;
	}

	public String getSourceHospitalInvoiceNo() {
		return sourceHospitalInvoiceNo;
	}

	public void setSourceHospitalInvoiceNo(String sourceHospitalInvoiceNo) {
		this.sourceHospitalInvoiceNo = sourceHospitalInvoiceNo;
	}

	public String getSourceReceivedDate() {
		return sourceReceivedDate;
	}

	public void setSourceReceivedDate(String sourceReceivedDate) {
		this.sourceReceivedDate = sourceReceivedDate;
	}

	public String getSourceSubmissionDate() {
		return sourceSubmissionDate;
	}

	public void setSourceSubmissionDate(String sourceSubmissionDate) {
		this.sourceSubmissionDate = sourceSubmissionDate;
	}

	public String getSourceVerifyBy() {
		return sourceVerifyBy;
	}

	public void setSourceVerifyBy(String sourceVerifyBy) {
		this.sourceVerifyBy = sourceVerifyBy;
	}

	public String getSourceBenefitCode() {
		return sourceBenefitCode;
	}

	public void setSourceBenefitCode(String sourceBenefitCode) {
		this.sourceBenefitCode = sourceBenefitCode;
	}

	public String getSourceFatca() {
		return sourceFatca;
	}

	public void setSourceFatca(String sourceFatca) {
		this.sourceFatca = sourceFatca;
	}

	public Date getClaimProcessDate() {
		return claimProcessDate;
	}

	public void setClaimProcessDate(Date claimProcessDate) {
		this.claimProcessDate = claimProcessDate;
	}

	public String getClaimProcessId() {
		return claimProcessId;
	}

	public void setClaimProcessId(String claimProcessId) {
		this.claimProcessId = claimProcessId;
	}

	public Integer getProcessInitiated() {
		return processInitiated;
	}

	public void setProcessInitiated(Integer processInitiated) {
		this.processInitiated = processInitiated;
	}

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getClaimCancelRemark() {
		return claimCancelRemark;
	}

	public void setClaimCancelRemark(String claimCancelRemark) {
		this.claimCancelRemark = claimCancelRemark;
	}

	public String getStpFlag() {
		return stpFlag;
	}

	public void setStpFlag(String stpFlag) {
		this.stpFlag = stpFlag;
	}

	public String getClaimDatasource() {
		return claimDatasource;
	}

	public void setClaimDatasource(String claimDatasource) {
		this.claimDatasource = claimDatasource;
	}

	public String getClientPECCheck() {
		return clientPECCheck;
	}

	public void setClientPECCheck(String clientPECCheck) {
		this.clientPECCheck = clientPECCheck;
	}

	public String getSourceCurrency() {
		return sourceCurrency;
	}

	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}

	public String getSourceDoctorName() {
		return sourceDoctorName;
	}

	public void setSourceDoctorName(String sourceDoctorName) {
		this.sourceDoctorName = sourceDoctorName;
	}

	public String getSourceBankAccountNumber() {
		return sourceBankAccountNumber;
	}

	public void setSourceBankAccountNumber(String sourceBankAccountNumber) {
		this.sourceBankAccountNumber = sourceBankAccountNumber;
	}

	public String getSourcePayeeName() {
		return sourcePayeeName;
	}

	public void setSourcePayeeName(String sourcePayeeName) {
		this.sourcePayeeName = sourcePayeeName;
	}

	public String getSourceBankName() {
		return sourceBankName;
	}

	public void setSourceBankName(String sourceBankName) {
		this.sourceBankName = sourceBankName;
	}
	
	public String getOther_Adjustments() {
		return Other_Adjustments;
	}

	public void setOther_Adjustments(String Other_Adjustments) {
		this.Other_Adjustments = Other_Adjustments;
	}
	
	public String getCause_Of_Death() {
		return Cause_Of_Death;
	}

	public void setCause_Of_Death(String Cause_Of_Death) {
		this.Cause_Of_Death = Cause_Of_Death;
	}
	
	public String getsourceBankType() {
		return sourceBankType;
	}

	public void setsourceBankType(String sourceBankType) {
		this.sourceBankType = sourceBankType;
	}
	
	public String getcurrent_from() {
		return current_from;
	}

	public void setcurrent_from(String current_from) {
		this.current_from= current_from;
	}
	
	public String getinsured_client_id() {
		return insured_client_id;
	}

	public void setinsured_client_id(String insured_client_id) {
		this.insured_client_id= insured_client_id;
	}
	@Override
	public String toString() {
		return "ClaimsData [sourcePayorId=" + sourcePayorId + ", sourceCorporateId=" + sourceCorporateId
				+ ", sourcePolicyNumber=" + sourcePolicyNumber + ", sourceMemberID=" + sourceMemberID + ", sourceNik="
				+ sourceNik + ", sourceBranchCode=" + sourceBranchCode + ", sourceCardNo=" + sourceCardNo
				+ ", sourceMemberName=" + sourceMemberName + ", sourceClaimId=" + sourceClaimId + ", sourceClaimType="
				+ sourceClaimType + ", sourceClaimStatus=" + sourceClaimStatus + ", sourceProviderCode="
				+ sourceProviderCode + ", sourceAdmissionDate=" + sourceAdmissionDate + ", sourceDischargeDate="
				+ sourceDischargeDate + ", sourceDurationDays=" + sourceDurationDays + ", sourceCoverageId="
				+ sourceCoverageId + ", sourcePlanId=" + sourcePlanId + ", sourceDisabilityNo=" + sourceDisabilityNo
				+ ", sourceDiagnosisCode=" + sourceDiagnosisCode + ", sourceSecondaryDiagnosisCode="
				+ sourceSecondaryDiagnosisCode + ", sourceAmtIncurred=" + sourceAmtIncurred + ", sourceAmtApproved="
				+ sourceAmtApproved + ", sourceAmtNotApproved=" + sourceAmtNotApproved + ", sourceAmtAsoApproved="
				+ sourceAmtAsoApproved + ", sourceOnPlanHighPlan=" + sourceOnPlanHighPlan + ", sourceRemarks="
				+ sourceRemarks + ", sourceProviderExcessPaid=" + sourceProviderExcessPaid + ", sourcePayorInvoiceId="
				+ sourcePayorInvoiceId + ", sourceHospitalInvoiceDate=" + sourceHospitalInvoiceDate
				+ ", sourceHospitalInvoiceNo=" + sourceHospitalInvoiceNo + ", sourceReceivedDate=" + sourceReceivedDate
				+ ", sourceSubmissionDate=" + sourceSubmissionDate + ", sourceVerifyBy=" + sourceVerifyBy
				+ ", sourceBenefitCode=" + sourceBenefitCode + ", sourceFatca=" + sourceFatca + ", claimProcessDate="
				+ claimProcessDate + ", claimProcessId=" + claimProcessId + ", processInitiated=" + processInitiated
				+ ", claimNo=" + claimNo + ", claimCancelRemark=" + claimCancelRemark + ", stpFlag=" + stpFlag
				+ ", claimDatasource=" + claimDatasource + ", clientPECCheck=" + clientPECCheck
				+ ", sourceCurrency=" + sourceCurrency + ", sourceDoctorName=" + sourceDoctorName + ", sourceBankAccountNumber=" + sourceBankAccountNumber
				+ ", sourcePayeeName=" + sourcePayeeName 
				+",Cause_Of_Death="+Cause_Of_Death
				+",sourceBankType="+sourceBankType
				+",current_from="+current_from
				+",insured_client_id="+insured_client_id
				+ ",Other_Adjustments=" + Other_Adjustments +", sourceBankName=" + sourceBankName + "]";
	}
	public ClaimsData get(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

}
