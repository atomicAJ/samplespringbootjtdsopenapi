package com.candelalabs.ws.model.tables;

public class BenCltAddDetails {
  private String Address1;
  private String Address2;
  private String Address3;
  private String Address4;
  private String Address5;
public String getAddress1() {
	return Address1;
}
public void setAddress1(String address1) {
	Address1 = address1;
}
public String getAddress2() {
	return Address2;
}
public void setAddress2(String address2) {
	Address2 = address2;
}
public String getAddress3() {
	return Address3;
}
public void setAddress3(String address3) {
	Address3 = address3;
}
public String getAddress4() {
	return Address4;
}
public void setAddress4(String address4) {
	Address4 = address4;
}
public String getAddress5() {
	return Address5;
}
public void setAddress5(String address5) {
	Address5 = address5;
}
  
  
}
