/**
 * 
 */
package com.candelalabs.ws.model.tables;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author Triaji
 *
 */
public class ClaimHistoryModel {

	private String LIFEASIACLIAMNUMBER;
	private String PENCILCLAIMNUMBER;
	private String POLICYNUMBER;
	private BigDecimal TOTALCLAIMAMOUNT;
	private String CLIENTNUMBER;
	private Date APPROVALDATE;
	private String COMPONENTCODE;

	public String getLIFEASIACLIAMNUMBER() {
		return LIFEASIACLIAMNUMBER;
	}

	public void setLIFEASIACLIAMNUMBER(String lIFEASIACLIAMNUMBER) {
		LIFEASIACLIAMNUMBER = lIFEASIACLIAMNUMBER;
	}

	public String getPENCILCLAIMNUMBER() {
		return PENCILCLAIMNUMBER;
	}

	public void setPENCILCLAIMNUMBER(String pENCILCLAIMNUMBER) {
		PENCILCLAIMNUMBER = pENCILCLAIMNUMBER;
	}

	public String getPOLICYNUMBER() {
		return POLICYNUMBER;
	}

	public void setPOLICYNUMBER(String pOLICYNUMBER) {
		POLICYNUMBER = pOLICYNUMBER;
	}

	public BigDecimal getTOTALCLAIMAMOUNT() {
		return TOTALCLAIMAMOUNT;
	}

	public void setTOTALCLAIMAMOUNT(BigDecimal tOTALCLAIMAMOUNT) {
		TOTALCLAIMAMOUNT = tOTALCLAIMAMOUNT;
	}

	public String getCLIENTNUMBER() {
		return CLIENTNUMBER;
	}

	public void setCLIENTNUMBER(String cLIENTNUMBER) {
		CLIENTNUMBER = cLIENTNUMBER;
	}

	public Date getAPPROVALDATE() {
		return APPROVALDATE;
	}

	public void setAPPROVALDATE(Date aPPROVALDATE) {
		APPROVALDATE = aPPROVALDATE;
	}

	public String getCOMPONENTCODE() {
		return COMPONENTCODE;
	}

	public void setCOMPONENTCODE(String cOMPONENTCODE) {
		COMPONENTCODE = cOMPONENTCODE;
	}

}
