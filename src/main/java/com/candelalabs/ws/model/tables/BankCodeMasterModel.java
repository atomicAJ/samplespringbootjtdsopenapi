/**
 * 
 */
package com.candelalabs.ws.model.tables;

/**
 * @author Triaji
 *
 */
public class BankCodeMasterModel {

	private String BANKKEY;
	private String BANKTYPE;
	private String FACTORINGHOUSE;
	private String COMPANY;
	private String CURRENCY;

	public BankCodeMasterModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getBANKTYPE() {
		return BANKTYPE;
	}

	public void setBANKTYPE(String bANKTYPE) {
		BANKTYPE = bANKTYPE;
	}

	public String getFACTORINGHOUSE() {
		return FACTORINGHOUSE;
	}

	public void setFACTORINGHOUSE(String fACTORINGHOUSE) {
		FACTORINGHOUSE = fACTORINGHOUSE;
	}

	public String getCOMPANY() {
		return COMPANY;
	}

	public void setCOMPANY(String cOMPANY) {
		COMPANY = cOMPANY;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

}
