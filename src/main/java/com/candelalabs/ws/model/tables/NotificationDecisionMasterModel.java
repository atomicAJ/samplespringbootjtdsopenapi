/**
 * 
 */
package com.candelalabs.ws.model.tables;

/**
 * @author Triaji
 *
 */
public class NotificationDecisionMasterModel {
	
	private String NotificationDecision;
	private String NotificationCategory;

	/**
	 * 
	 */
	public NotificationDecisionMasterModel() {
		// TODO Auto-generated constructor stub
	}

	public String getNotificationDecision() {
		return NotificationDecision;
	}

	public void setNotificationDecision(String notificationDecision) {
		NotificationDecision = notificationDecision;
	}

	public String getNotificationCategory() {
		return NotificationCategory;
	}

	public void setNotificationCategory(String notificationCategory) {
		NotificationCategory = notificationCategory;
	}
	

}
