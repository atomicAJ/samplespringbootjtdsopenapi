package com.candelalabs.ws.model.tables;

import java.sql.Date;

public class ClientDetails {
	private String CLIENTNO;
	private String LIFENAME;
	private Date DOB;
	private String SEX;
	private String Address1;
	private String Address2;
	private String Address3;
	private String Address4;
	private String Address5;
	private String OCCUPATIONCODE;
	private String IDType;
	private String IDNumber;
	private String PHONENO01;
	private String PHONENO02;
	private String PHONENO03;
	private String Nationality;
	private String ClientStatus;
	private String EMAILADDRESS;
	private String ABUSEFLAG;
	private String FATCAFLAG;

	public String getABUSEFLAG() {
		return ABUSEFLAG;
	}

	public void setABUSEFLAG(String aBUSEFLAG) {
		ABUSEFLAG = aBUSEFLAG;
	}

	public String getFATCAFLAG() {
		return FATCAFLAG;
	}

	public void setFATCAFLAG(String fATCAFLAG) {
		FATCAFLAG = fATCAFLAG;
	}

	public String getCLIENTNO() {
		return CLIENTNO;
	}

	public void setCLIENTNO(String cLIENTNO) {
		CLIENTNO = cLIENTNO;
	}

	public String getLIFENAME() {
		return LIFENAME;
	}

	public void setLIFENAME(String lIFENAME) {
		LIFENAME = lIFENAME;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
	}

	public String getSEX() {
		return SEX;
	}

	public void setSEX(String sEX) {
		SEX = sEX;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public void setAddress3(String address3) {
		Address3 = address3;
	}

	public String getAddress4() {
		return Address4;
	}

	public void setAddress4(String address4) {
		Address4 = address4;
	}

	public String getAddress5() {
		return Address5;
	}

	public void setAddress5(String address5) {
		Address5 = address5;
	}

	public String getOccupation_code() {
		return OCCUPATIONCODE;
	}

	public void setOccupation_code(String occupation_code) {
		OCCUPATIONCODE = occupation_code;
	}

	public String getIDType() {
		return IDType;
	}

	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDNumber() {
		return IDNumber;
	}

	public void setIDNumber(String iDNumber) {
		IDNumber = iDNumber;
	}

	public String getPHONENO01() {
		return PHONENO01;
	}

	public void setPHONENO01(String pHONENO01) {
		PHONENO01 = pHONENO01;
	}

	public String getPHONENO02() {
		return PHONENO02;
	}

	public void setPHONENO02(String pHONENO02) {
		PHONENO02 = pHONENO02;
	}

	public String getPHONENO03() {
		return PHONENO03;
	}

	public void setPHONENO03(String pHONENO03) {
		PHONENO03 = pHONENO03;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	public String getClientStatus() {
		return ClientStatus;
	}

	public void setClientStatus(String clientStatus) {
		ClientStatus = clientStatus;
	}

	public String getEMAILADDRESS() {
		return EMAILADDRESS;
	}

	public void setEMAILADDRESS(String eMAILADDRESS) {
		EMAILADDRESS = eMAILADDRESS;
	}

}
