package com.candelalabs.ws.model.tables;

public class CleintDetPolNoBased {
	private String ADDRESS1 ;
	private String ADDRESS2;
	private String ADDRESS3;
	private String ADDRESS4;
	private String ADDRESS5;
	private String EMAILADDRESS;
	private String PHONENO01;
	private String PHONENO02;
	private String PHONENO03;

	public CleintDetPolNoBased() {
	}

	public CleintDetPolNoBased(String ADDRESS1, String ADDRESS2, String ADDRESS3, String ADDRESS4, String ADDRESS5, String EMAILADDRESS, String PHONENO01, String PHONENO02, String PHONENO03) {
		this.ADDRESS1 = ADDRESS1;
		this.ADDRESS2 = ADDRESS2;
		this.ADDRESS3 = ADDRESS3;
		this.ADDRESS4 = ADDRESS4;
		this.ADDRESS5 = ADDRESS5;
		this.EMAILADDRESS = EMAILADDRESS;
		this.PHONENO01 = PHONENO01;
		this.PHONENO02 = PHONENO02;
		this.PHONENO03 = PHONENO03;
	}

	public String getADDRESS1() {
		return ADDRESS1;
	}
	public void setADDRESS1(String aDDRESS1) {
		ADDRESS1 = aDDRESS1;
	}
	public String getADDRESS2() {
		return ADDRESS2;
	}
	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}
	public String getADDRESS3() {
		return ADDRESS3;
	}
	public void setADDRESS3(String aDDRESS3) {
		ADDRESS3 = aDDRESS3;
	}
	public String getADDRESS4() {
		return ADDRESS4;
	}
	public void setADDRESS4(String aDDRESS4) {
		ADDRESS4 = aDDRESS4;
	}
	public String getADDRESS5() {
		return ADDRESS5;
	}
	public void setADDRESS5(String aDDRESS5) {
		ADDRESS5 = aDDRESS5;
	}
	public String getEMAILADDRESS() {
		return EMAILADDRESS;
	}
	public void setEMAILADDRESS(String eMAILADDRESS) {
		EMAILADDRESS = eMAILADDRESS;
	}
	public String getPHONENO01() {
		return PHONENO01;
	}
	public void setPHONENO01(String pHONENO01) {
		PHONENO01 = pHONENO01;
	}
	public String getPHONENO02() {
		return PHONENO02;
	}
	public void setPHONENO02(String pHONENO02) {
		PHONENO02 = pHONENO02;
	}
	public String getPHONENO03() {
		return PHONENO03;
	}
	public void setPHONENO03(String pHONENO03) {
		PHONENO03 = pHONENO03;
	}
	
	
	
	
	/*private String PolicyOwnerAddress1 ;
	private String PolicyOwnerAddress2;
	private String PolicyOwnerAddress3;
	private String PolicyOwnerAddress4;
	private String PolicyOwnerAddress5;
	private String PolicyOwnerEmailAddress;
	private String PHONENO01;
	private String PHONENO02;
	private String PHONENO03;
	
	public String getPolicyOwnerAddress1() {
		return PolicyOwnerAddress1;
	}
	public void setPolicyOwnerAddress1(String policyOwnerAddress1) {
		PolicyOwnerAddress1 = policyOwnerAddress1;
	}
	public String getPolicyOwnerAddress2() {
		return PolicyOwnerAddress2;
	}
	public void setPolicyOwnerAddress2(String policyOwnerAddress2) {
		PolicyOwnerAddress2 = policyOwnerAddress2;
	}
	public String getPolicyOwnerAddress3() {
		return PolicyOwnerAddress3;
	}
	public void setPolicyOwnerAddress3(String policyOwnerAddress3) {
		PolicyOwnerAddress3 = policyOwnerAddress3;
	}
	public String getPolicyOwnerAddress4() {
		return PolicyOwnerAddress4;
	}
	public void setPolicyOwnerAddress4(String policyOwnerAddress4) {
		PolicyOwnerAddress4 = policyOwnerAddress4;
	}
	public String getPolicyOwnerAddress5() {
		return PolicyOwnerAddress5;
	}
	public void setPolicyOwnerAddress5(String policyOwnerAddress5) {
		PolicyOwnerAddress5 = policyOwnerAddress5;
	}
	public String getPolicyOwnerEmailAddress() {
		return PolicyOwnerEmailAddress;
	}
	public void setPolicyOwnerEmailAddress(String policyOwnerEmailAddress) {
		PolicyOwnerEmailAddress = policyOwnerEmailAddress;
	}
	public String getPHONENO01() {
		return PHONENO01;
	}
	public void setPHONENO01(String pHONENO01) {
		PHONENO01 = pHONENO01;
	}
	public String getPHONENO02() {
		return PHONENO02;
	}
	public void setPHONENO02(String pHONENO02) {
		PHONENO02 = pHONENO02;
	}
	public String getPHONENO03() {
		return PHONENO03;
	}
	public void setPHONENO03(String pHONENO03) {
		PHONENO03 = pHONENO03;
	}*/
	
	
}
