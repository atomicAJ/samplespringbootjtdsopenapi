/**
 * 
 */
package com.candelalabs.ws.model.tables;

/**
 * @author Admin
 *
 */
public class NotificationProcessMasterModel {
	
	private Integer ConfigurationID;
	private String NotificationProcessName;
	private String NotificationActivityName;
	private String NotificationDecision;
	
	
	
	public NotificationProcessMasterModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NotificationProcessMasterModel(Integer configurationID, String notificationProcessName,
			String notificationActivityName, String notificationDecision) {
		super();
		ConfigurationID = configurationID;
		NotificationProcessName = notificationProcessName;
		NotificationActivityName = notificationActivityName;
		NotificationDecision = notificationDecision;
	}
	public Integer getConfigurationID() {
		return ConfigurationID;
	}
	public void setConfigurationID(Integer configurationID) {
		ConfigurationID = configurationID;
	}
	public String getNotificationProcessName() {
		return NotificationProcessName;
	}
	public void setNotificationProcessName(String notificationProcessName) {
		NotificationProcessName = notificationProcessName;
	}
	public String getNotificationActivityName() {
		return NotificationActivityName;
	}
	public void setNotificationActivityName(String notificationActivityName) {
		NotificationActivityName = notificationActivityName;
	}
	public String getNotificationDecision() {
		return NotificationDecision;
	}
	public void setNotificationDecision(String notificationDecision) {
		NotificationDecision = notificationDecision;
	}
	
	

}
