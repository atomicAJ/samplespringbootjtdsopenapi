package com.candelalabs.ws.model;

import java.util.Date;

public class RequirementsDetails {
	private String docID;
	private Date requirementRequestTS;
	private Date requirementReceiveTS;
	private String requirementText;
	
	public RequirementsDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequirementsDetails(String docID, Date requirementRequestTS, Date requirementReceiveTS,
			String requirementText) {
		super();
		this.docID = docID;
		this.requirementRequestTS = requirementRequestTS;
		this.requirementReceiveTS = requirementReceiveTS;
		this.requirementText = requirementText;
	}

	public String getDocID() {
		return docID;
	}

	public void setDocID(String docID) {
		this.docID = docID;
	}

	public Date getRequirementRequestTS() {
		return requirementRequestTS;
	}

	public void setRequirementRequestTS(Date requirementRequestTS) {
		this.requirementRequestTS = requirementRequestTS;
	}

	public Date getRequirementReceiveTS() {
		return requirementReceiveTS;
	}

	public void setRequirementReceiveTS(Date requirementReceiveTS) {
		this.requirementReceiveTS = requirementReceiveTS;
	}

	public String getRequirementText() {
		return requirementText;
	}

	public void setRequirementText(String requirementText) {
		this.requirementText = requirementText;
	}

	@Override
	public String toString() {
		return "RequirementsDetails [docID=" + docID + ", requirementRequestTS=" + requirementRequestTS
				+ ", requirementReceiveTS=" + requirementReceiveTS + ", requirementText=" + requirementText + "]";
	}

}
