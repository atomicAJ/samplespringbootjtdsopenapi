package com.candelalabs.ws.model;

public class BENEFICIARYLIST {
 private String CLIENTNO;
 private String CLIENTNAME;
 private String RELATIONSHIP;
 private String PERCENTAGE;
public String getCLIENTNO() {
	return CLIENTNO;
}
public void setCLIENTNO(String cLIENTNO) {
	CLIENTNO = cLIENTNO;
}
public String getCLIENTNAME() {
	return CLIENTNAME;
}
public void setCLIENTNAME(String cLIENTNAME) {
	CLIENTNAME = cLIENTNAME;
}
public String getRELATIONSHIP() {
	return RELATIONSHIP;
}
public void setRELATIONSHIP(String rELATIONSHIP) {
	RELATIONSHIP = rELATIONSHIP;
}
public String getPERCENTAGE() {
	return PERCENTAGE;
}
public void setPERCENTAGE(String pERCENTAGE) {
	PERCENTAGE = pERCENTAGE;
}
 
}
