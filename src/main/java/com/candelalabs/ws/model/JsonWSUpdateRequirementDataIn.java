package com.candelalabs.ws.model;

import java.util.Date;

public class JsonWSUpdateRequirementDataIn {
	private String claimNo;
	private String requirementStatus;
	private String requirementReceiveTS;

	public String getrequirementReceiveTS() {
		return requirementReceiveTS;
	}

	public void setrequirementReceiveTS(String requirementReceiveTS) {
		this.requirementReceiveTS = requirementReceiveTS;
	}

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getrequirementStatus() {
		return requirementStatus;
	}

	public void setrequirementStatus(String requirementStatus) {
		this.requirementStatus = requirementStatus;
	}

}
