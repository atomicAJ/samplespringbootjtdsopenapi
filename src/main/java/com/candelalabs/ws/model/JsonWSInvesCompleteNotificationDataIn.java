package com.candelalabs.ws.model;

import java.util.Date;

public class JsonWSInvesCompleteNotificationDataIn {

		private String claimNo;
		//private String appNo;
		private String polNo;
		private String docId;
		private String requirementText;
		private String processName;
	    private String decision;
	    private String lastUser;
	    private String RequirementStatus;
	
   public String getPolNo() {
		return polNo;
	}

	public void setPolNo(String polNo) {
		this.polNo = polNo;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getRequirementText() {
		return requirementText;
	}

	public void setRequirementText(String requirementText) {
		this.requirementText = requirementText;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}

	public String getRequirementStatus() {
		return RequirementStatus;
	}

	public void setRequirementStatus(String requirementStatus) {
		RequirementStatus = requirementStatus;
	}


	
	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	

}
