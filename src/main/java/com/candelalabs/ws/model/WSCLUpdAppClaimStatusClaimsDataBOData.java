package com.candelalabs.ws.model;

public class WSCLUpdAppClaimStatusClaimsDataBOData {
	
	private String ClaimNo;
	private String Chdrnum;
	private String RgpyNum;
	private String ChdrSel;
	private String CrTable;
	private String ClamParty;
	
	public String getClaimNo() {
		return ClaimNo;
	}
	
	public String getChdrnum() {
		return Chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		Chdrnum = chdrnum;
	}

	public void setClaimNo(String claimNo) {
		ClaimNo = claimNo;
	}

	public String getRgpyNum() {
		return RgpyNum;
	}

	public void setRgpyNum(String rgpyNum) {
		RgpyNum = rgpyNum;
	}

	public String getChdrSel() {
		return ChdrSel;
	}

	public void setChdrSel(String chdrSel) {
		ChdrSel = chdrSel;
	}

	public String getCrTable() {
		return CrTable;
	}

	public void setCrTable(String crTable) {
		CrTable = crTable;
	}

	public String getClamParty() {
		return ClamParty;
	}

	public void setClamParty(String clamParty) {
		ClamParty = clamParty;
	}
	
	
}
