package com.candelalabs.ws.model;

public class ReqDocList {
	private String DocID;
	private String PolicyNumber;
	private String RequirementStatus;
	public String getDocID() {
		return DocID;
	}
	public void setDocID(String docID) {
		DocID = docID;
	}
	public String getPolicyNumber() {
		return PolicyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		PolicyNumber = policyNumber;
	}
	public String getRequirementStatus() {
		return RequirementStatus;
	}
	public void setRequirementStatus(String requirementStatus) {
		RequirementStatus = requirementStatus;
	}
	
	
}
