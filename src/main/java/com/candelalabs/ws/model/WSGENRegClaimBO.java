package com.candelalabs.ws.model;

public class WSGENRegClaimBO {
	
	private String USRPRF;
	private Integer CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private Integer MSGCNT;
	private String INDC;
	private String CLAIMNO;
	private String CHDRSEL;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String CRTABLE;
	private String CLAMPARTY;
	private String CLTYPE;
	private String ZDIAGCDE;
	private String ZMEDPRV;
	private String ZDOCTOR;
	private String CLAIMCUR;
	private String PAYCLT;
	private String BANKKEY;
	private String FACTHOUS;
	private String BANKACOUNT;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String DATEFROM;
	private String ZCLMRECD;
	private String INCURDT;
	private String DISCHDT;
	private String HOSBEN;
	private String DATEFRM;
	private String DATETO;
	private Double ACTEXP;
	private Double ZNODAY;
	private Double TACTEXP;
	private Double PRCNT;
	private Double PYMT;
	private String CURRCODE;
	private String LIFENUM;
	private String CLAIMEVD;
	private String ACDBEN;
	private Double BENPRCNT;
	private String ZREGPCOD;
	
	public WSGENRegClaimBO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WSGENRegClaimBO(String uSRPRF, Integer cHDRCOY, String bOIDEN, String mSGLNG, Integer mSGCNT, String iNDC,
			String cLAIMNO, String cHDRSEL, String lIFE, String cOVERAGE, String rIDER, String cRTABLE,
			String cLAMPARTY, String cLTYPE, String zDIAGCDE, String zMEDPRV, String zDOCTOR, String cLAIMCUR,
			String pAYCLT, String bANKKEY, String fACTHOUS, String bANKACOUNT, String bANKACCDSC, String bNKACTYP,
			String dATEFROM, String zCLMRECD, String iNCURDT, String dISCHDT, String hOSBEN, String dATEFRM,
			String dATETO, Double aCTEXP, Double zNODAY, Double tACTEXP, Double pRCNT, Double pYMT, String cURRCODE,
			String lIFENUM, String cLAIMEVD, String aCDBEN, Double bENPRCNT, String zREGPCOD) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		BOIDEN = bOIDEN;
		MSGLNG = mSGLNG;
		MSGCNT = mSGCNT;
		INDC = iNDC;
		CLAIMNO = cLAIMNO;
		CHDRSEL = cHDRSEL;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
		CRTABLE = cRTABLE;
		CLAMPARTY = cLAMPARTY;
		CLTYPE = cLTYPE;
		ZDIAGCDE = zDIAGCDE;
		ZMEDPRV = zMEDPRV;
		ZDOCTOR = zDOCTOR;
		CLAIMCUR = cLAIMCUR;
		PAYCLT = pAYCLT;
		BANKKEY = bANKKEY;
		FACTHOUS = fACTHOUS;
		BANKACOUNT = bANKACOUNT;
		BANKACCDSC = bANKACCDSC;
		BNKACTYP = bNKACTYP;
		DATEFROM = dATEFROM;
		ZCLMRECD = zCLMRECD;
		INCURDT = iNCURDT;
		DISCHDT = dISCHDT;
		HOSBEN = hOSBEN;
		DATEFRM = dATEFRM;
		DATETO = dATETO;
		ACTEXP = aCTEXP;
		ZNODAY = zNODAY;
		TACTEXP = tACTEXP;
		PRCNT = pRCNT;
		PYMT = pYMT;
		CURRCODE = cURRCODE;
		LIFENUM = lIFENUM;
		CLAIMEVD = cLAIMEVD;
		ACDBEN = aCDBEN;
		BENPRCNT = bENPRCNT;
		ZREGPCOD = zREGPCOD;
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public Integer getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(Integer cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public Integer getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(Integer mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getCLAMPARTY() {
		return CLAMPARTY;
	}

	public void setCLAMPARTY(String cLAMPARTY) {
		CLAMPARTY = cLAMPARTY;
	}

	public String getCLTYPE() {
		return CLTYPE;
	}

	public void setCLTYPE(String cLTYPE) {
		CLTYPE = cLTYPE;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getZMEDPRV() {
		return ZMEDPRV;
	}

	public void setZMEDPRV(String zMEDPRV) {
		ZMEDPRV = zMEDPRV;
	}

	public String getZDOCTOR() {
		return ZDOCTOR;
	}

	public void setZDOCTOR(String zDOCTOR) {
		ZDOCTOR = zDOCTOR;
	}

	public String getCLAIMCUR() {
		return CLAIMCUR;
	}

	public void setCLAIMCUR(String cLAIMCUR) {
		CLAIMCUR = cLAIMCUR;
	}

	public String getPAYCLT() {
		return PAYCLT;
	}

	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getFACTHOUS() {
		return FACTHOUS;
	}

	public void setFACTHOUS(String fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}

	public String getBANKACOUNT() {
		return BANKACOUNT;
	}

	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}

	public String getBANKACCDSC() {
		return BANKACCDSC;
	}

	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}

	public String getBNKACTYP() {
		return BNKACTYP;
	}

	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}

	public String getDATEFROM() {
		return DATEFROM;
	}

	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}

	public String getZCLMRECD() {
		return ZCLMRECD;
	}

	public void setZCLMRECD(String zCLMRECD) {
		ZCLMRECD = zCLMRECD;
	}

	public String getINCURDT() {
		return INCURDT;
	}

	public void setINCURDT(String iNCURDT) {
		INCURDT = iNCURDT;
	}

	public String getDISCHDT() {
		return DISCHDT;
	}

	public void setDISCHDT(String dISCHDT) {
		DISCHDT = dISCHDT;
	}

	public String getHOSBEN() {
		return HOSBEN;
	}

	public void setHOSBEN(String hOSBEN) {
		HOSBEN = hOSBEN;
	}

	public String getDATEFRM() {
		return DATEFRM;
	}

	public void setDATEFRM(String dATEFRM) {
		DATEFRM = dATEFRM;
	}

	public String getDATETO() {
		return DATETO;
	}

	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}

	public Double getACTEXP() {
		return ACTEXP;
	}

	public void setACTEXP(Double aCTEXP) {
		ACTEXP = aCTEXP;
	}

	public Double getZNODAY() {
		return ZNODAY;
	}

	public void setZNODAY(Double zNODAY) {
		ZNODAY = zNODAY;
	}

	public Double getTACTEXP() {
		return TACTEXP;
	}

	public void setTACTEXP(Double tACTEXP) {
		TACTEXP = tACTEXP;
	}

	public Double getPRCNT() {
		return PRCNT;
	}

	public void setPRCNT(Double pRCNT) {
		PRCNT = pRCNT;
	}

	public Double getPYMT() {
		return PYMT;
	}

	public void setPYMT(Double pYMT) {
		PYMT = pYMT;
	}

	public String getCURRCODE() {
		return CURRCODE;
	}

	public void setCURRCODE(String cURRCODE) {
		CURRCODE = cURRCODE;
	}

	public String getLIFENUM() {
		return LIFENUM;
	}

	public void setLIFENUM(String lIFENUM) {
		LIFENUM = lIFENUM;
	}

	public String getCLAIMEVD() {
		return CLAIMEVD;
	}

	public void setCLAIMEVD(String cLAIMEVD) {
		CLAIMEVD = cLAIMEVD;
	}

	public String getACDBEN() {
		return ACDBEN;
	}

	public void setACDBEN(String aCDBEN) {
		ACDBEN = aCDBEN;
	}

	public Double getBENPRCNT() {
		return BENPRCNT;
	}

	public void setBENPRCNT(Double bENPRCNT) {
		BENPRCNT = bENPRCNT;
	}

	public String getZREGPCOD() {
		return ZREGPCOD;
	}

	public void setZREGPCOD(String zREGPCOD) {
		ZREGPCOD = zREGPCOD;
	}

	@Override
	public String toString() {
		return "WSGENRegClaimBO [USRPRF=" + USRPRF + ", CHDRCOY=" + CHDRCOY + ", BOIDEN=" + BOIDEN + ", MSGLNG="
				+ MSGLNG + ", MSGCNT=" + MSGCNT + ", INDC=" + INDC + ", CLAIMNO=" + CLAIMNO + ", CHDRSEL=" + CHDRSEL
				+ ", LIFE=" + LIFE + ", COVERAGE=" + COVERAGE + ", RIDER=" + RIDER + ", CRTABLE=" + CRTABLE
				+ ", CLAMPARTY=" + CLAMPARTY + ", CLTYPE=" + CLTYPE + ", ZDIAGCDE=" + ZDIAGCDE + ", ZMEDPRV=" + ZMEDPRV
				+ ", ZDOCTOR=" + ZDOCTOR + ", CLAIMCUR=" + CLAIMCUR + ", PAYCLT=" + PAYCLT + ", BANKKEY=" + BANKKEY
				+ ", FACTHOUS=" + FACTHOUS + ", BANKACOUNT=" + BANKACOUNT + ", BANKACCDSC=" + BANKACCDSC + ", BNKACTYP="
				+ BNKACTYP + ", DATEFROM=" + DATEFROM + ", ZCLMRECD=" + ZCLMRECD + ", INCURDT=" + INCURDT + ", DISCHDT="
				+ DISCHDT + ", HOSBEN=" + HOSBEN + ", DATEFRM=" + DATEFRM + ", DATETO=" + DATETO + ", ACTEXP=" + ACTEXP
				+ ", ZNODAY=" + ZNODAY + ", TACTEXP=" + TACTEXP + ", PRCNT=" + PRCNT + ", PYMT=" + PYMT + ", CURRCODE="
				+ CURRCODE + ", LIFENUM=" + LIFENUM + ", CLAIMEVD=" + CLAIMEVD + ", ACDBEN=" + ACDBEN + ", BENPRCNT="
				+ BENPRCNT + ", ZREGPCOD=" + ZREGPCOD + "]";
	}
	
	}
