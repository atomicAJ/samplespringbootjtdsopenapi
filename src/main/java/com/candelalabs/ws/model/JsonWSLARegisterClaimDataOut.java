package com.candelalabs.ws.model;

import com.candelalabs.ws.model.tables.ClaimsData;

public class JsonWSLARegisterClaimDataOut {
	private String processmsg;
    private String decision;
    private ClaimsData claimsData;
    
	public String getProcessmsg() {
		return processmsg;
	}
	public void setProcessmsg(String processmsg) {
		this.processmsg = processmsg;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public ClaimsData getClaimsData() {
		return claimsData;
	}
	public void setClaimsData(ClaimsData claimsData) {
		this.claimsData = claimsData;
	}
	
}
