package com.candelalabs.ws.model;
public class WSDNULRegClaimClaimsDetailsBOData {


	private Double OtherAdjst;
	private String ReasonCD;
	private String CauseOfDth;
	private String DtOfDeath;

	public Double getOtherAdjst() {
		return OtherAdjst;
	}
	public void setOtherAdjst(Double othrAdjst) {
		OtherAdjst = othrAdjst;
	}
	public String getReasonCD() {
		return ReasonCD;
	}
	public void setReasonCD(String reasonCD) {
		ReasonCD = reasonCD;
	}
	public String getCauseOfDth() {
		return CauseOfDth;
	}
	public void setCauseOfDth(String causeOfDth) {
		CauseOfDth = causeOfDth;
	}
	public String getDtOfDeath() {
		return DtOfDeath;
	}
	public void setDtOfDeath(String dtOfDeath) {
		DtOfDeath = dtOfDeath;
	} 
	
}
