package com.candelalabs.ws.model;

public class ClaimRecord {
	private String ClaimNumber;
	 private String PolicyNumber;
	 private String StartTime;
	 private String ActivityName;
	   private String EndTime;
	   private String ActionedUser;
       private String UserDecision;
	         
	         
	         
	      
	         
			public String getClaimNumber() {
				return ClaimNumber;
			}
			public void setClaimNumber(String claimNumber) {
				ClaimNumber = claimNumber;
			}
			
			public String getPolicyNumber() {
				return PolicyNumber;
			}
			public void setPolicyNumber(String policyNumber) {
				PolicyNumber = policyNumber;
			}
			
			
			public String getActivityName() {
				return ActivityName;
			}
			public void setActivityName(String activityName) {
				ActivityName = activityName;
			}
			public String getStartTime() {
				return StartTime;
			}
			public void setStartTime(String startTime) {
				StartTime = startTime;
			}
			public String getEndTime() {
				return EndTime;
			}
			public void setEndTime(String endTime) {
				EndTime = endTime;
			}
			public String getActionedUser() {
				return ActionedUser;
			}
			public void setActionedUser(String actionedUser) {
				ActionedUser = actionedUser;
			}
			public String getUserDecision() {
				return UserDecision;
			}
			public void setUserDecision(String userDecision) {
				UserDecision = userDecision;
			}
	  
}
