package com.candelalabs.ws.model;

import java.util.List;

public class PendingCaseReport {
	private String policyNumber;
	private String claimNumber;
	private String claimType;
	private String productName;
	private String policyStatus;
	private String channel;
	private String agentName;
	private String salesOfficeName;
	private String salesOfficeAddress;
	private String userId;
	private String policyOwner;
	private String insuredName;
	private String receiveDate;
	private String registerDate;
	private String admissionDate;
	private String dischargeDate;
	private String dateOfDeath;
	private String amountIncurred;
	private String pendingDays;
	private String fupEntryDate;
	private String pendingType;
	private List<String> fupDescription;
	
	public PendingCaseReport() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PendingCaseReport(String policyNumber, String claimNumber, String claimType, String productName,
			String policyStatus, String channel, String agentName, String salesOfficeName, String salesOfficeAddress,
			String userId, String policyOwner, String insuredName, String receiveDate, String registerDate,
			String admissionDate, String dischargeDate, String dateOfDeath, String amountIncurred, String pendingDays,
			String fupEntryDate, String pendingType, List<String> fupDescription) {
		super();
		this.policyNumber = policyNumber;
		this.claimNumber = claimNumber;
		this.claimType = claimType;
		this.productName = productName;
		this.policyStatus = policyStatus;
		this.channel = channel;
		this.agentName = agentName;
		this.salesOfficeName = salesOfficeName;
		this.salesOfficeAddress = salesOfficeAddress;
		this.userId = userId;
		this.policyOwner = policyOwner;
		this.insuredName = insuredName;
		this.receiveDate = receiveDate;
		this.registerDate = registerDate;
		this.admissionDate = admissionDate;
		this.dischargeDate = dischargeDate;
		this.dateOfDeath = dateOfDeath;
		this.amountIncurred = amountIncurred;
		this.pendingDays = pendingDays;
		this.fupEntryDate = fupEntryDate;
		this.pendingType = pendingType;
		this.fupDescription = fupDescription;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getSalesOfficeName() {
		return salesOfficeName;
	}

	public void setSalesOfficeName(String salesOfficeName) {
		this.salesOfficeName = salesOfficeName;
	}

	public String getSalesOfficeAddress() {
		return salesOfficeAddress;
	}

	public void setSalesOfficeAddress(String salesOfficeAddress) {
		this.salesOfficeAddress = salesOfficeAddress;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPolicyOwner() {
		return policyOwner;
	}

	public void setPolicyOwner(String policyOwner) {
		this.policyOwner = policyOwner;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public String getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(String dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}

	public String getAmountIncurred() {
		return amountIncurred;
	}

	public void setAmountIncurred(String amountIncurred) {
		this.amountIncurred = amountIncurred;
	}

	public String getPendingDays() {
		return pendingDays;
	}

	public void setPendingDays(String pendingDays) {
		this.pendingDays = pendingDays;
	}

	public String getFupEntryDate() {
		return fupEntryDate;
	}

	public void setFupEntryDate(String fupEntryDate) {
		this.fupEntryDate = fupEntryDate;
	}

	public String getPendingType() {
		return pendingType;
	}

	public void setPendingType(String pendingType) {
		this.pendingType = pendingType;
	}

	public List<String> getFupDescription() {
		return fupDescription;
	}

	public void setFupDescription(List<String> fupDescription) {
		this.fupDescription = fupDescription;
	}

	@Override
	public String toString() {
		return "PendingCaseReportList [policyNumber=" + policyNumber + ", claimNumber=" + claimNumber + ", claimType="
				+ claimType + ", productName=" + productName + ", policyStatus=" + policyStatus + ", channel=" + channel
				+ ", agentName=" + agentName + ", salesOfficeName=" + salesOfficeName + ", salesOfficeAddress="
				+ salesOfficeAddress + ", userId=" + userId + ", policyOwner=" + policyOwner + ", insuredName="
				+ insuredName + ", receiveDate=" + receiveDate + ", registerDate=" + registerDate + ", admissionDate="
				+ admissionDate + ", dischargeDate=" + dischargeDate + ", dateOfDeath=" + dateOfDeath
				+ ", amountIncurred=" + amountIncurred + ", pendingDays=" + pendingDays + ", fupEntryDate="
				+ fupEntryDate + ", pendingType=" + pendingType + ", fupDescription=" + fupDescription + "]";
	}
	
}
