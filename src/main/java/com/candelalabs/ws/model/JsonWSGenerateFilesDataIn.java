package com.candelalabs.ws.model;

public class JsonWSGenerateFilesDataIn {
    private String claimNo;
    private String generateFile;
    
	public String getClaimNo() {
		return claimNo;
	}
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	public String getGenerateFile() {
		return generateFile;
	}
	public void setGenerateFile(String generateFile) {
		this.generateFile = generateFile;
	}
    

}
