package com.candelalabs.ws.model;

public class WSDULRegClaimDataIn {
	private String CLAIMNO;
	private String ACTIVITYID;
	private String BOIDENTIFIER;
	public String getCLAIMNO() {
		return CLAIMNO;
	}
	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}
	public String getACTIVITYID() {
		return ACTIVITYID;
	}
	public void setACTIVITYID(String aCTIVITYID) {
		ACTIVITYID = aCTIVITYID;
	}
	public String getBOIDENTIFIER() {
		return BOIDENTIFIER;
	}
	public void setBOIDENTIFIER(String bOIDENTIFIER) {
		BOIDENTIFIER = bOIDENTIFIER;
	}
	
	
}
