package com.candelalabs.ws.model;

import java.math.BigDecimal;
import java.util.Date;

public class PendingCaseClaimData {
	private String PolicyNumber;
	private String ClaimNumber;
	private String ClaimTypeUI;
	private String InsuredClientName;
	private Date Receiveddate;
	private Date Registerdate;
	private Date Admissiondate;
	private Date Dischargedate;
	private BigDecimal ClaimInvoiceAmount;
	
	public PendingCaseClaimData(String policyNumber, String claimNumber, String claimTypeUI, String insuredClientName,
			Date receiveddate, Date registerdate, Date admissiondate, Date dischargedate,
			BigDecimal claimInvoiceAmount) {
		super();
		PolicyNumber = policyNumber;
		ClaimNumber = claimNumber;
		ClaimTypeUI = claimTypeUI;
		InsuredClientName = insuredClientName;
		Receiveddate = receiveddate;
		Registerdate = registerdate;
		Admissiondate = admissiondate;
		Dischargedate = dischargedate;
		ClaimInvoiceAmount = claimInvoiceAmount;
	}

	public PendingCaseClaimData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPolicyNumber() {
		return PolicyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		PolicyNumber = policyNumber;
	}

	public String getClaimNumber() {
		return ClaimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		ClaimNumber = claimNumber;
	}

	public String getClaimTypeUI() {
		return ClaimTypeUI;
	}

	public void setClaimTypeUI(String claimTypeUI) {
		ClaimTypeUI = claimTypeUI;
	}

	public String getInsuredClientName() {
		return InsuredClientName;
	}

	public void setInsuredClientName(String insuredClientName) {
		InsuredClientName = insuredClientName;
	}

	public Date getReceiveddate() {
		return Receiveddate;
	}

	public void setReceiveddate(Date receiveddate) {
		Receiveddate = receiveddate;
	}

	public Date getRegisterdate() {
		return Registerdate;
	}

	public void setRegisterdate(Date registerdate) {
		Registerdate = registerdate;
	}

	public Date getAdmissiondate() {
		return Admissiondate;
	}

	public void setAdmissiondate(Date admissiondate) {
		Admissiondate = admissiondate;
	}

	public Date getDischargedate() {
		return Dischargedate;
	}

	public void setDischargedate(Date dischargedate) {
		Dischargedate = dischargedate;
	}

	public BigDecimal getClaimInvoiceAmount() {
		return ClaimInvoiceAmount;
	}

	public void setClaimInvoiceAmount(BigDecimal claimInvoiceAmount) {
		ClaimInvoiceAmount = claimInvoiceAmount;
	}

	@Override
	public String toString() {
		return "PendingCaseClaimData [PolicyNumber=" + PolicyNumber + ", ClaimNumber=" + ClaimNumber + ", ClaimTypeUI="
				+ ClaimTypeUI + ", InsuredClientName=" + InsuredClientName + ", Receiveddate=" + Receiveddate
				+ ", Registerdate=" + Registerdate + ", Admissiondate=" + Admissiondate + ", Dischargedate="
				+ Dischargedate + ", ClaimInvoiceAmount=" + ClaimInvoiceAmount + "]";
	}
	
}
