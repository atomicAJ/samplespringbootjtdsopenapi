package com.candelalabs.ws.model;

public class WSCLRegClaimBnfcDetailsBOData {
    private Integer Chdrcoy;
    private String ClaimCur;
    private Integer PayClt;
	private String BankAcount;
    private String BankAccDsc;
    
	public Integer getChdrcoy() {
		return Chdrcoy;
	}
	public void setChdrcoy(Integer chdrcoy) {
		Chdrcoy = chdrcoy;
	}
	


	public String getClaimCur() {
		return ClaimCur;
	}
	public void setClaimCur(String claimCur) {
		ClaimCur = claimCur;
	}
	public Integer getPayClt() {
		return PayClt;
	}
	public void setPayClt(Integer payClt) {
		PayClt = payClt;
	}
	public String getBankAcount() {
		return BankAcount;
	}
	public void setBankAcount(String bankAcount) {
		BankAcount = bankAcount;
	}
	public String getBankAccDsc() {
		return BankAccDsc;
	}
	public void setBankAccDsc(String bankAccDsc) {
		BankAccDsc = bankAccDsc;
	}
    
    
}
