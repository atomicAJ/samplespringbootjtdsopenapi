package com.candelalabs.ws.model;

public class JsonWSUpdateRequirementDataOut {
	private String processmsg;
    private String decision;
	public String getProcessmsg() {
		return processmsg;
	}
	public void setProcessmsg(String processmsg) {
		this.processmsg = processmsg;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
}
