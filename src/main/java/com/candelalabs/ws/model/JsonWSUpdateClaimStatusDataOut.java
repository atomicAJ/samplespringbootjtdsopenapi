package com.candelalabs.ws.model;

import com.fasterxml.jackson.core.JsonParser;

public class JsonWSUpdateClaimStatusDataOut {
	private String processmsg;
    private String decision;
    
	public String getProcessmsg() {
		return processmsg;
	}
	public void setProcessmsg(String processmsg) {
		this.processmsg = processmsg;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
}
