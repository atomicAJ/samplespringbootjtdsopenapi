package com.candelalabs.ws.model;

public class JsonWSDULRegClaimDataIn {

	private String USRPRF;
	private int CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private int MSGCNT;
	private String INDC;
	private String CLAIMNO;
	private int CHDRNUM;
	private String CLNTNUM;
	private String ZCATCLMBEN;
	private String CAUSEOFDTH;
	private int OTHERADJST;
	private String EFDATE;
	private String DTOFDEATH;
	private String ZCLMRECD;

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public int getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(int cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public int getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(int mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public int getCHDRNUM() {
		return CHDRNUM;
	}

	public void setCHDRNUM(int cHDRNUM) {
		CHDRNUM = cHDRNUM;
	}

	public String getCLNTNUM() {
		return CLNTNUM;
	}

	public void setCLNTNUM(String cLNTNUM) {
		CLNTNUM = cLNTNUM;
	}

	public String getZCATCLMBEN() {
		return ZCATCLMBEN;
	}

	public void setZCATCLMBEN(String zCATCLMBEN) {
		ZCATCLMBEN = zCATCLMBEN;
	}

	public String getCAUSEOFDTH() {
		return CAUSEOFDTH;
	}

	public void setCAUSEOFDTH(String cAUSEOFDTH) {
		CAUSEOFDTH = cAUSEOFDTH;
	}

	public int getOTHERADJST() {
		return OTHERADJST;
	}

	public void setOTHERADJST(int oTHERADJST) {
		OTHERADJST = oTHERADJST;
	}

	public String getEFDATE() {
		return EFDATE;
	}

	public void setEFDATE(String eFDATE) {
		EFDATE = eFDATE;
	}

	public String getDTOFDEATH() {
		return DTOFDEATH;
	}

	public void setDTOFDEATH(String dTOFDEATH) {
		DTOFDEATH = dTOFDEATH;
	}

	public String getZCLMRECD() {
		return ZCLMRECD;
	}

	public void setZCLMRECD(String zCLMRECD) {
		ZCLMRECD = zCLMRECD;
	}

}
