package com.candelalabs.ws.model;
public class WSDNULUpdClaimRejectStatusClaimsDetailsBOData {


	private Double OfCharge;
	private String CancelDate;
	public Double getOfCharge() {
		return OfCharge;
	}
	public void setOfCharge(Double ofCharge) {
		OfCharge = ofCharge;
	}
	public String getCancelDate() {
		return CancelDate;
	}
	public void setCancelDate(String cancelDate) {
		CancelDate = cancelDate;
	}
    
	
}
