package com.candelalabs.ws.model;

import java.math.BigDecimal;
import java.util.Date;

public class ClaimDataTATReport {
	private String claimNumber;
	private String claimNumberLa;
	private String policyNumber;
	private String productCode;
	private String claimTypeLa;
	private String claimTypeUi;
	private String planCode;
	private Date receivedDate;
	private Date registerDate;
	private int processInitiatedFlag;
	private Date processInitiatonTs;
	private Date processEndTs;
	private String processInitiatedRemarks;
	private String initiateProcess;
	private String processInstanceId;
	private String insuredClientId;
	private String insuredClientName;
	private String ownerClientId;
	private String tpaClaimNumber;
	private String batchNumber;
	private Date admissionDate;
	private Date dischargeDate;
	private BigDecimal stayDuration;
	private String componentCode;
	private String componentSumAssured;
	private String diagnosisCode;
	private String diagnosisName;
	private BigDecimal claimInvoiceAmount;
	private BigDecimal claimInvoiceAmountUi;
	private BigDecimal claimApprovedAmountTpa;
	private BigDecimal claimApprovedAmountLa;
	private BigDecimal claimApprovedAmountUi;
	private BigDecimal claimSumAssuredPercentage;
	private int claimStpFlag;
	private String fullyClaimed;
	private String claimStatus;
	private String pendingRequirementActivityId;
	private String claimUser;
	private String claimProcessor;
	private String claimProcessorName;
	private String claimApprovalHaUser;
	private String claimApprovalHaUserName;
	private String claimRejectHaUser;
	private String claimRejectHaUserName;
	private String claimUwUser;
	private String claimHuwUser;
	private Date claimDecisionDate;
	private String claimDecision;
	private String providerNameSource;
	private String providerName;
	private String providerCode;
	private String payeeName;
	private String bankCode;
	private String bankType;
	private String bankAccountNumber;
	private String bankAccountName;
	private String inProgressClaimNumber;
	private String doctorName;
	private String claimCurrency;
	private String factoringHouse;
	private Date currentFrom;
	private String benefitCategory;
	private BigDecimal osCharges;
	private BigDecimal otherAdjustments;
	private String adjustmentCode;
	private String policyLoanAmount;
	private String policyDebtAmount;
	private String surgeryPerformed;
	private String claimCancelRemark;
	private String portalClaimRequestNumber;
	private String uiSaveFlag;
	
	//PENCILRULEDETAILS fields for TAT report
	private String ProcessType;
	private String ProcessKeyReferenceNumber;
	private String RuleType;
	private String RuleDescription;
	private String RuleStatus;
	private String RuleReason;
	private Date RuleProcessingStartTimeStamp;
	private Date RuleProcessingEndTimeStamp;

	//Below variables are for validation
	private boolean validRecord;
	private String exceptionMessage;

	public ClaimDataTATReport() {
		super();
	}

	public ClaimDataTATReport(String claimNumber, String claimNumberLa, String policyNumber, String productCode,
			String claimTypeLa, String claimTypeUi, String planCode, Date receivedDate, Date registerDate,
			int processInitiatedFlag, Date processInitiatonTs, Date processEndTs, String processInitiatedRemarks,
			String initiateProcess, String processInstanceId, String insuredClientId, String insuredClientName,
			String ownerClientId, String tpaClaimNumber, String batchNumber, Date admissionDate, Date dischargeDate,
			BigDecimal stayDuration, String componentCode, String componentSumAssured, String diagnosisCode,
			String diagnosisName, BigDecimal claimInvoiceAmount, BigDecimal claimInvoiceAmountUi,
			BigDecimal claimApprovedAmountTpa, BigDecimal claimApprovedAmountLa, BigDecimal claimApprovedAmountUi,
			BigDecimal claimSumAssuredPercentage, int claimStpFlag, String fullyClaimed, String claimStatus,
			String pendingRequirementActivityId, String claimUser, String claimProcessor, String claimProcessorName,
			String claimApprovalHaUser, String claimApprovalHaUserName, String claimRejectHaUser,
			String claimRejectHaUserName, String claimUwUser, String claimHuwUser, Date claimDecisionDate,
			String claimDecision, String providerNameSource, String providerName, String providerCode, String payeeName,
			String bankCode, String bankType, String bankAccountNumber, String bankAccountName,
			String inProgressClaimNumber, String doctorName, String claimCurrency, String factoringHouse,
			Date currentFrom, String benefitCategory, BigDecimal osCharges, BigDecimal otherAdjustments,
			String adjustmentCode, String policyLoanAmount, String policyDebtAmount, String surgeryPerformed,
			String claimCancelRemark, String portalClaimRequestNumber, String uiSaveFlag, String processType,
			String processKeyReferenceNumber, String ruleType, String ruleDescription, String ruleStatus,
			String ruleReason, Date ruleProcessingStartTimeStamp, Date ruleProcessingEndTimeStamp, boolean validRecord,
			String exceptionMessage) {
		super();
		this.claimNumber = claimNumber;
		this.claimNumberLa = claimNumberLa;
		this.policyNumber = policyNumber;
		this.productCode = productCode;
		this.claimTypeLa = claimTypeLa;
		this.claimTypeUi = claimTypeUi;
		this.planCode = planCode;
		this.receivedDate = receivedDate;
		this.registerDate = registerDate;
		this.processInitiatedFlag = processInitiatedFlag;
		this.processInitiatonTs = processInitiatonTs;
		this.processEndTs = processEndTs;
		this.processInitiatedRemarks = processInitiatedRemarks;
		this.initiateProcess = initiateProcess;
		this.processInstanceId = processInstanceId;
		this.insuredClientId = insuredClientId;
		this.insuredClientName = insuredClientName;
		this.ownerClientId = ownerClientId;
		this.tpaClaimNumber = tpaClaimNumber;
		this.batchNumber = batchNumber;
		this.admissionDate = admissionDate;
		this.dischargeDate = dischargeDate;
		this.stayDuration = stayDuration;
		this.componentCode = componentCode;
		this.componentSumAssured = componentSumAssured;
		this.diagnosisCode = diagnosisCode;
		this.diagnosisName = diagnosisName;
		this.claimInvoiceAmount = claimInvoiceAmount;
		this.claimInvoiceAmountUi = claimInvoiceAmountUi;
		this.claimApprovedAmountTpa = claimApprovedAmountTpa;
		this.claimApprovedAmountLa = claimApprovedAmountLa;
		this.claimApprovedAmountUi = claimApprovedAmountUi;
		this.claimSumAssuredPercentage = claimSumAssuredPercentage;
		this.claimStpFlag = claimStpFlag;
		this.fullyClaimed = fullyClaimed;
		this.claimStatus = claimStatus;
		this.pendingRequirementActivityId = pendingRequirementActivityId;
		this.claimUser = claimUser;
		this.claimProcessor = claimProcessor;
		this.claimProcessorName = claimProcessorName;
		this.claimApprovalHaUser = claimApprovalHaUser;
		this.claimApprovalHaUserName = claimApprovalHaUserName;
		this.claimRejectHaUser = claimRejectHaUser;
		this.claimRejectHaUserName = claimRejectHaUserName;
		this.claimUwUser = claimUwUser;
		this.claimHuwUser = claimHuwUser;
		this.claimDecisionDate = claimDecisionDate;
		this.claimDecision = claimDecision;
		this.providerNameSource = providerNameSource;
		this.providerName = providerName;
		this.providerCode = providerCode;
		this.payeeName = payeeName;
		this.bankCode = bankCode;
		this.bankType = bankType;
		this.bankAccountNumber = bankAccountNumber;
		this.bankAccountName = bankAccountName;
		this.inProgressClaimNumber = inProgressClaimNumber;
		this.doctorName = doctorName;
		this.claimCurrency = claimCurrency;
		this.factoringHouse = factoringHouse;
		this.currentFrom = currentFrom;
		this.benefitCategory = benefitCategory;
		this.osCharges = osCharges;
		this.otherAdjustments = otherAdjustments;
		this.adjustmentCode = adjustmentCode;
		this.policyLoanAmount = policyLoanAmount;
		this.policyDebtAmount = policyDebtAmount;
		this.surgeryPerformed = surgeryPerformed;
		this.claimCancelRemark = claimCancelRemark;
		this.portalClaimRequestNumber = portalClaimRequestNumber;
		this.uiSaveFlag = uiSaveFlag;
		ProcessType = processType;
		ProcessKeyReferenceNumber = processKeyReferenceNumber;
		RuleType = ruleType;
		RuleDescription = ruleDescription;
		RuleStatus = ruleStatus;
		RuleReason = ruleReason;
		RuleProcessingStartTimeStamp = ruleProcessingStartTimeStamp;
		RuleProcessingEndTimeStamp = ruleProcessingEndTimeStamp;
		this.validRecord = validRecord;
		this.exceptionMessage = exceptionMessage;
	}



	public String getClaimNumber() {
		return claimNumber;
	}

	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}

	public String getClaimNumberLa() {
		return claimNumberLa;
	}

	public void setClaimNumberLa(String claimNumberLa) {
		this.claimNumberLa = claimNumberLa;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getClaimTypeLa() {
		return claimTypeLa;
	}

	public void setClaimTypeLa(String claimTypeLa) {
		this.claimTypeLa = claimTypeLa;
	}

	public String getClaimTypeUi() {
		return claimTypeUi;
	}

	public void setClaimTypeUi(String claimTypeUi) {
		this.claimTypeUi = claimTypeUi;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public int getProcessInitiatedFlag() {
		return processInitiatedFlag;
	}

	public void setProcessInitiatedFlag(int processInitiatedFlag) {
		this.processInitiatedFlag = processInitiatedFlag;
	}

	public Date getProcessInitiatonTs() {
		return processInitiatonTs;
	}

	public void setProcessInitiatonTs(Date processInitiatonTs) {
		this.processInitiatonTs = processInitiatonTs;
	}

	public Date getProcessEndTs() {
		return processEndTs;
	}

	public void setProcessEndTs(Date processEndTs) {
		this.processEndTs = processEndTs;
	}

	public String getProcessInitiatedRemarks() {
		return processInitiatedRemarks;
	}

	public void setProcessInitiatedRemarks(String processInitiatedRemarks) {
		this.processInitiatedRemarks = processInitiatedRemarks;
	}

	public String getInitiateProcess() {
		return initiateProcess;
	}

	public void setInitiateProcess(String initiateProcess) {
		this.initiateProcess = initiateProcess;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getInsuredClientId() {
		return insuredClientId;
	}

	public void setInsuredClientId(String insuredClientId) {
		this.insuredClientId = insuredClientId;
	}

	public String getInsuredClientName() {
		return insuredClientName;
	}

	public void setInsuredClientName(String insuredClientName) {
		this.insuredClientName = insuredClientName;
	}

	public String getOwnerClientId() {
		return ownerClientId;
	}

	public void setOwnerClientId(String ownerClientId) {
		this.ownerClientId = ownerClientId;
	}

	public String getTpaClaimNumber() {
		return tpaClaimNumber;
	}

	public void setTpaClaimNumber(String tpaClaimNumber) {
		this.tpaClaimNumber = tpaClaimNumber;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public BigDecimal getStayDuration() {
		return stayDuration;
	}

	public void setStayDuration(BigDecimal stayDuration) {
		this.stayDuration = stayDuration;
	}

	public String getComponentCode() {
		return componentCode;
	}

	public void setComponentCode(String componentCode) {
		this.componentCode = componentCode;
	}

	public String getComponentSumAssured() {
		return componentSumAssured;
	}

	public void setComponentSumAssured(String componentSumAssured) {
		this.componentSumAssured = componentSumAssured;
	}

	public String getDiagnosisCode() {
		return diagnosisCode;
	}

	public void setDiagnosisCode(String diagnosisCode) {
		this.diagnosisCode = diagnosisCode;
	}

	public String getDiagnosisName() {
		return diagnosisName;
	}

	public void setDiagnosisName(String diagnosisName) {
		this.diagnosisName = diagnosisName;
	}

	public BigDecimal getClaimInvoiceAmount() {
		return claimInvoiceAmount;
	}

	public void setClaimInvoiceAmount(BigDecimal claimInvoiceAmount) {
		this.claimInvoiceAmount = claimInvoiceAmount;
	}

	public BigDecimal getClaimInvoiceAmountUi() {
		return claimInvoiceAmountUi;
	}

	public void setClaimInvoiceAmountUi(BigDecimal claimInvoiceAmountUi) {
		this.claimInvoiceAmountUi = claimInvoiceAmountUi;
	}

	public BigDecimal getClaimApprovedAmountTpa() {
		return claimApprovedAmountTpa;
	}

	public void setClaimApprovedAmountTpa(BigDecimal claimApprovedAmountTpa) {
		this.claimApprovedAmountTpa = claimApprovedAmountTpa;
	}

	public BigDecimal getClaimApprovedAmountLa() {
		return claimApprovedAmountLa;
	}

	public void setClaimApprovedAmountLa(BigDecimal claimApprovedAmountLa) {
		this.claimApprovedAmountLa = claimApprovedAmountLa;
	}

	public BigDecimal getClaimApprovedAmountUi() {
		return claimApprovedAmountUi;
	}

	public void setClaimApprovedAmountUi(BigDecimal claimApprovedAmountUi) {
		this.claimApprovedAmountUi = claimApprovedAmountUi;
	}

	public BigDecimal getClaimSumAssuredPercentage() {
		return claimSumAssuredPercentage;
	}

	public void setClaimSumAssuredPercentage(BigDecimal claimSumAssuredPercentage) {
		this.claimSumAssuredPercentage = claimSumAssuredPercentage;
	}

	public int getClaimStpFlag() {
		return claimStpFlag;
	}

	public void setClaimStpFlag(int claimStpFlag) {
		this.claimStpFlag = claimStpFlag;
	}

	public String getFullyClaimed() {
		return fullyClaimed;
	}

	public void setFullyClaimed(String fullyClaimed) {
		this.fullyClaimed = fullyClaimed;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	public String getPendingRequirementActivityId() {
		return pendingRequirementActivityId;
	}

	public void setPendingRequirementActivityId(String pendingRequirementActivityId) {
		this.pendingRequirementActivityId = pendingRequirementActivityId;
	}

	public String getClaimUser() {
		return claimUser;
	}

	public void setClaimUser(String claimUser) {
		this.claimUser = claimUser;
	}

	public String getClaimProcessor() {
		return claimProcessor;
	}

	public void setClaimProcessor(String claimProcessor) {
		this.claimProcessor = claimProcessor;
	}

	public String getClaimProcessorName() {
		return claimProcessorName;
	}

	public void setClaimProcessorName(String claimProcessorName) {
		this.claimProcessorName = claimProcessorName;
	}

	public String getClaimApprovalHaUser() {
		return claimApprovalHaUser;
	}

	public void setClaimApprovalHaUser(String claimApprovalHaUser) {
		this.claimApprovalHaUser = claimApprovalHaUser;
	}

	public String getClaimApprovalHaUserName() {
		return claimApprovalHaUserName;
	}

	public void setClaimApprovalHaUserName(String claimApprovalHaUserName) {
		this.claimApprovalHaUserName = claimApprovalHaUserName;
	}

	public String getClaimRejectHaUser() {
		return claimRejectHaUser;
	}

	public void setClaimRejectHaUser(String claimRejectHaUser) {
		this.claimRejectHaUser = claimRejectHaUser;
	}

	public String getClaimRejectHaUserName() {
		return claimRejectHaUserName;
	}

	public void setClaimRejectHaUserName(String claimRejectHaUserName) {
		this.claimRejectHaUserName = claimRejectHaUserName;
	}

	public String getClaimUwUser() {
		return claimUwUser;
	}

	public void setClaimUwUser(String claimUwUser) {
		this.claimUwUser = claimUwUser;
	}

	public String getClaimHuwUser() {
		return claimHuwUser;
	}

	public void setClaimHuwUser(String claimHuwUser) {
		this.claimHuwUser = claimHuwUser;
	}

	public Date getClaimDecisionDate() {
		return claimDecisionDate;
	}

	public void setClaimDecisionDate(Date claimDecisionDate) {
		this.claimDecisionDate = claimDecisionDate;
	}

	public String getClaimDecision() {
		return claimDecision;
	}

	public void setClaimDecision(String claimDecision) {
		this.claimDecision = claimDecision;
	}

	public String getProviderNameSource() {
		return providerNameSource;
	}

	public void setProviderNameSource(String providerNameSource) {
		this.providerNameSource = providerNameSource;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getInProgressClaimNumber() {
		return inProgressClaimNumber;
	}

	public void setInProgressClaimNumber(String inProgressClaimNumber) {
		this.inProgressClaimNumber = inProgressClaimNumber;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getClaimCurrency() {
		return claimCurrency;
	}

	public void setClaimCurrency(String claimCurrency) {
		this.claimCurrency = claimCurrency;
	}

	public String getFactoringHouse() {
		return factoringHouse;
	}

	public void setFactoringHouse(String factoringHouse) {
		this.factoringHouse = factoringHouse;
	}

	public Date getCurrentFrom() {
		return currentFrom;
	}

	public void setCurrentFrom(Date currentFrom) {
		this.currentFrom = currentFrom;
	}

	public String getBenefitCategory() {
		return benefitCategory;
	}

	public void setBenefitCategory(String benefitCategory) {
		this.benefitCategory = benefitCategory;
	}

	public BigDecimal getOsCharges() {
		return osCharges;
	}

	public void setOsCharges(BigDecimal osCharges) {
		this.osCharges = osCharges;
	}

	public BigDecimal getOtherAdjustments() {
		return otherAdjustments;
	}

	public void setOtherAdjustments(BigDecimal otherAdjustments) {
		this.otherAdjustments = otherAdjustments;
	}

	public String getAdjustmentCode() {
		return adjustmentCode;
	}

	public void setAdjustmentCode(String adjustmentCode) {
		this.adjustmentCode = adjustmentCode;
	}

	public String getPolicyLoanAmount() {
		return policyLoanAmount;
	}

	public void setPolicyLoanAmount(String policyLoanAmount) {
		this.policyLoanAmount = policyLoanAmount;
	}

	public String getPolicyDebtAmount() {
		return policyDebtAmount;
	}

	public void setPolicyDebtAmount(String policyDebtAmount) {
		this.policyDebtAmount = policyDebtAmount;
	}

	public String getSurgeryPerformed() {
		return surgeryPerformed;
	}

	public void setSurgeryPerformed(String surgeryPerformed) {
		this.surgeryPerformed = surgeryPerformed;
	}

	public String getClaimCancelRemark() {
		return claimCancelRemark;
	}

	public void setClaimCancelRemark(String claimCancelRemark) {
		this.claimCancelRemark = claimCancelRemark;
	}

	public String getPortalClaimRequestNumber() {
		return portalClaimRequestNumber;
	}

	public void setPortalClaimRequestNumber(String portalClaimRequestNumber) {
		this.portalClaimRequestNumber = portalClaimRequestNumber;
	}

	public String getUiSaveFlag() {
		return uiSaveFlag;
	}

	public void setUiSaveFlag(String uiSaveFlag) {
		this.uiSaveFlag = uiSaveFlag;
	}

	public boolean isValidRecord() {
		return validRecord;
	}

	public void setValidRecord(boolean validRecord) {
		this.validRecord = validRecord;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getProcessType() {
		return ProcessType;
	}

	public void setProcessType(String processType) {
		ProcessType = processType;
	}

	public String getProcessKeyReferenceNumber() {
		return ProcessKeyReferenceNumber;
	}

	public void setProcessKeyReferenceNumber(String processKeyReferenceNumber) {
		ProcessKeyReferenceNumber = processKeyReferenceNumber;
	}

	public String getRuleType() {
		return RuleType;
	}

	public void setRuleType(String ruleType) {
		RuleType = ruleType;
	}

	public String getRuleDescription() {
		return RuleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		RuleDescription = ruleDescription;
	}

	public String getRuleStatus() {
		return RuleStatus;
	}

	public void setRuleStatus(String ruleStatus) {
		RuleStatus = ruleStatus;
	}

	public String getRuleReason() {
		return RuleReason;
	}

	public void setRuleReason(String ruleReason) {
		RuleReason = ruleReason;
	}

	public Date getRuleProcessingStartTimeStamp() {
		return RuleProcessingStartTimeStamp;
	}

	public void setRuleProcessingStartTimeStamp(Date ruleProcessingStartTimeStamp) {
		RuleProcessingStartTimeStamp = ruleProcessingStartTimeStamp;
	}

	public Date getRuleProcessingEndTimeStamp() {
		return RuleProcessingEndTimeStamp;
	}

	public void setRuleProcessingEndTimeStamp(Date ruleProcessingEndTimeStamp) {
		RuleProcessingEndTimeStamp = ruleProcessingEndTimeStamp;
	}

	@Override
	public String toString() {
		return "ClaimDataTATReport [claimNumber=" + claimNumber + ", claimNumberLa=" + claimNumberLa + ", policyNumber="
				+ policyNumber + ", productCode=" + productCode + ", claimTypeLa=" + claimTypeLa + ", claimTypeUi="
				+ claimTypeUi + ", planCode=" + planCode + ", receivedDate=" + receivedDate + ", registerDate="
				+ registerDate + ", processInitiatedFlag=" + processInitiatedFlag + ", processInitiatonTs="
				+ processInitiatonTs + ", processEndTs=" + processEndTs + ", processInitiatedRemarks="
				+ processInitiatedRemarks + ", initiateProcess=" + initiateProcess + ", processInstanceId="
				+ processInstanceId + ", insuredClientId=" + insuredClientId + ", insuredClientName="
				+ insuredClientName + ", ownerClientId=" + ownerClientId + ", tpaClaimNumber=" + tpaClaimNumber
				+ ", batchNumber=" + batchNumber + ", admissionDate=" + admissionDate + ", dischargeDate="
				+ dischargeDate + ", stayDuration=" + stayDuration + ", componentCode=" + componentCode
				+ ", componentSumAssured=" + componentSumAssured + ", diagnosisCode=" + diagnosisCode
				+ ", diagnosisName=" + diagnosisName + ", claimInvoiceAmount=" + claimInvoiceAmount
				+ ", claimInvoiceAmountUi=" + claimInvoiceAmountUi + ", claimApprovedAmountTpa="
				+ claimApprovedAmountTpa + ", claimApprovedAmountLa=" + claimApprovedAmountLa
				+ ", claimApprovedAmountUi=" + claimApprovedAmountUi + ", claimSumAssuredPercentage="
				+ claimSumAssuredPercentage + ", claimStpFlag=" + claimStpFlag + ", fullyClaimed=" + fullyClaimed
				+ ", claimStatus=" + claimStatus + ", pendingRequirementActivityId=" + pendingRequirementActivityId
				+ ", claimUser=" + claimUser + ", claimProcessor=" + claimProcessor + ", claimProcessorName="
				+ claimProcessorName + ", claimApprovalHaUser=" + claimApprovalHaUser + ", claimApprovalHaUserName="
				+ claimApprovalHaUserName + ", claimRejectHaUser=" + claimRejectHaUser + ", claimRejectHaUserName="
				+ claimRejectHaUserName + ", claimUwUser=" + claimUwUser + ", claimHuwUser=" + claimHuwUser
				+ ", claimDecisionDate=" + claimDecisionDate + ", claimDecision=" + claimDecision
				+ ", providerNameSource=" + providerNameSource + ", providerName=" + providerName + ", providerCode="
				+ providerCode + ", payeeName=" + payeeName + ", bankCode=" + bankCode + ", bankType=" + bankType
				+ ", bankAccountNumber=" + bankAccountNumber + ", bankAccountName=" + bankAccountName
				+ ", inProgressClaimNumber=" + inProgressClaimNumber + ", doctorName=" + doctorName + ", claimCurrency="
				+ claimCurrency + ", factoringHouse=" + factoringHouse + ", currentFrom=" + currentFrom
				+ ", benefitCategory=" + benefitCategory + ", osCharges=" + osCharges + ", otherAdjustments="
				+ otherAdjustments + ", adjustmentCode=" + adjustmentCode + ", policyLoanAmount=" + policyLoanAmount
				+ ", policyDebtAmount=" + policyDebtAmount + ", surgeryPerformed=" + surgeryPerformed
				+ ", claimCancelRemark=" + claimCancelRemark + ", portalClaimRequestNumber=" + portalClaimRequestNumber
				+ ", uiSaveFlag=" + uiSaveFlag + ", ProcessType=" + ProcessType + ", ProcessKeyReferenceNumber="
				+ ProcessKeyReferenceNumber + ", RuleType=" + RuleType + ", RuleDescription=" + RuleDescription
				+ ", RuleStatus=" + RuleStatus + ", RuleReason=" + RuleReason + ", RuleProcessingStartTimeStamp="
				+ RuleProcessingStartTimeStamp + ", RuleProcessingEndTimeStamp=" + RuleProcessingEndTimeStamp
				+ ", validRecord=" + validRecord + ", exceptionMessage=" + exceptionMessage + "]";
	}
}
