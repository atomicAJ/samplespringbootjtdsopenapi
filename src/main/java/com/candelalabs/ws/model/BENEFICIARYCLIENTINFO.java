package com.candelalabs.ws.model;

import java.sql.Date;

public class BENEFICIARYCLIENTINFO {
 private String IDNUMBER;
 private String SEX;
 private Date DOB;
public String getIDNUMBER() {
	return IDNUMBER;
}
public void setIDNUMBER(String iDNUMBER) {
	IDNUMBER = iDNUMBER;
}
public String getSEX() {
	return SEX;
}
public void setSEX(String sEX) {
	SEX = sEX;
}
public Date getDOB() {
	return DOB;
}
public void setDOB(Date dOB) {
	DOB = dOB;
}
 
 
 
}
