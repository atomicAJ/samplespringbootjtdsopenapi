package com.candelalabs.ws.model;

public class ReportDetails {
	private String ActionedUser;

	public ReportDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReportDetails(String actionedUser) {
		super();
		ActionedUser = actionedUser;
	}

	public String getActionedUser() {
		return ActionedUser;
	}

	public void setActionedUser(String actionedUser) {
		ActionedUser = actionedUser;
	}

	@Override
	public String toString() {
		return "ReportDetails [ActionedUser=" + ActionedUser + "]";
	}

}
