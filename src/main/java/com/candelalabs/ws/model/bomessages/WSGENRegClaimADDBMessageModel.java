/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.util.List;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSGENRegClaimADDBMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMEVD;
	private String CLAIMNO;
	private String CLTYPE;
	private String INCURDT;
	private String PAYCLT;
	private Double PRCNT;
	private Double PYMT;
	private String ZCLMRECD;
	private String ZDIAGCDE;
	private String BANKACOUNT;
	private String BANKKEY;
	private String DISCHDT;
	private String ZDOCTOR;
	private String ZMEDPRV;
	private String LIFENUM;
	private String CRTABLE;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String FACTHOUS;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String CURRCODE;
	private List<WSGENRegClaimADDBRecurringMessageModel> wSGenRecLaimADDB_li;

	/**
	 * 
	 */
	public WSGENRegClaimADDBMessageModel() {
		// TODO Auto-generated constructor stub
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Generic_RegisterAddb;
		this.MSGLNG = FWDConstants.MESSAGE_LANGUAGE;
		this.MSGCNT = "00636";
		this.INDC = FWDConstants.MORE_INDICATOR;

	}

	public WSGENRegClaimADDBMessageModel(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, String mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMEVD, String cLAIMNO, String cLTYPE, String iNCURDT, String pAYCLT,
			Double pRCNT, Double pYMT, String zCLMRECD, String zDIAGCDE, String bANKACOUNT, String bANKKEY,
			String dISCHDT, String zDOCTOR, String zMEDPRV, String lIFENUM, String cRTABLE, String lIFE,
			String cOVERAGE, String rIDER, String fACTHOUS, String bANKACCDSC, String bNKACTYP, String cURRCODE,
			List<WSGENRegClaimADDBRecurringMessageModel> wSGenRecLaimADDB_li) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Generic_RegisterAddb;
		this.MSGLNG = FWDConstants.MESSAGE_LANGUAGE;
		this.MSGCNT = "00636";
		this.INDC = FWDConstants.MORE_INDICATOR;
		CHDRSEL = cHDRSEL;
		CLAIMEVD = cLAIMEVD;
		CLAIMNO = cLAIMNO;
		CLTYPE = cLTYPE;
		INCURDT = iNCURDT;
		PAYCLT = pAYCLT;
		PRCNT = pRCNT;
		PYMT = pYMT;
		ZCLMRECD = zCLMRECD;
		ZDIAGCDE = zDIAGCDE;
		BANKACOUNT = bANKACOUNT;
		BANKKEY = bANKKEY;
		DISCHDT = dISCHDT;
		ZDOCTOR = zDOCTOR;
		ZMEDPRV = zMEDPRV;
		LIFENUM = lIFENUM;
		CRTABLE = cRTABLE;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
		FACTHOUS = fACTHOUS;
		BANKACCDSC = bANKACCDSC;
		BNKACTYP = bNKACTYP;
		CURRCODE = cURRCODE;
		this.wSGenRecLaimADDB_li = wSGenRecLaimADDB_li;
	}

	public String getBOMessage() {
		PaddingUtil pdngutl = new PaddingUtil();

		String boMessage = pdngutl.padRight(this.USRPRF, 9) +

				pdngutl.padRight(this.CHDRCOY, 1) +

				pdngutl.padRight(this.BOIDEN, 20) +

				pdngutl.padRight(this.MSGLNG, 1) +

				pdngutl.padRight(this.MSGCNT, 5) +

				pdngutl.padRight(this.INDC, 1) +

				pdngutl.padRight(this.CHDRSEL, 10) +

				pdngutl.padRight("", 18) +

				pdngutl.padRight(this.CLAIMNO, 11) +

				pdngutl.padRight(this.CLTYPE, 2) +

				pdngutl.padRight(this.INCURDT, 8) +

				pdngutl.padRight(this.PAYCLT, 8) +

				pdngutl.PadForDouble(this.PRCNT, 5) +

				pdngutl.PadForDoubleWithSym(this.PYMT, 18) +

				pdngutl.padRight(this.ZCLMRECD, 8) +

				pdngutl.padRight(this.ZDIAGCDE, 8) +

				pdngutl.padRight(this.BANKACOUNT, 20) +

				pdngutl.padRight(this.BANKKEY, 10) +

				pdngutl.padRight(this.DISCHDT, 8) +

				pdngutl.padRight(this.ZDOCTOR, 8) +

				pdngutl.padRight(this.ZMEDPRV, 10) +

				pdngutl.padRight(this.LIFENUM, 8) +

				pdngutl.padRight(this.CRTABLE, 4) +

				pdngutl.padZero(Integer.valueOf(this.LIFE), 2) +

				pdngutl.padZero(Integer.valueOf(this.COVERAGE), 2) +

				pdngutl.padZero(Integer.valueOf(this.RIDER), 2) +

				pdngutl.padRight(this.FACTHOUS, 2) +

				pdngutl.padRight(this.BANKACCDSC, 30) +

				pdngutl.padRight(this.BNKACTYP, 4) +

				pdngutl.padRight(this.CURRCODE, 3);

		for (WSGENRegClaimADDBRecurringMessageModel wsgenRegClaimADDBRecurringMessageModel : this.wSGenRecLaimADDB_li) {
			boMessage = boMessage + wsgenRegClaimADDBRecurringMessageModel.getBOMessage();
		}

		return boMessage;
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMEVD() {
		return CLAIMEVD;
	}

	public void setCLAIMEVD(String cLAIMEVD) {
		CLAIMEVD = cLAIMEVD;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCLTYPE() {
		return CLTYPE;
	}

	public void setCLTYPE(String cLTYPE) {
		CLTYPE = cLTYPE;
	}

	public String getINCURDT() {
		return INCURDT;
	}

	public void setINCURDT(String iNCURDT) {
		INCURDT = iNCURDT;
	}

	public String getPAYCLT() {
		return PAYCLT;
	}

	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}

	public Double getPRCNT() {
		return PRCNT;
	}

	public void setPRCNT(Double pRCNT) {
		PRCNT = pRCNT;
	}

	public Double getPYMT() {
		return PYMT;
	}

	public void setPYMT(Double pYMT) {
		PYMT = pYMT;
	}

	public String getZCLMRECD() {
		return ZCLMRECD;
	}

	public void setZCLMRECD(String zCLMRECD) {
		ZCLMRECD = zCLMRECD;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getBANKACOUNT() {
		return BANKACOUNT;
	}

	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getDISCHDT() {
		return DISCHDT;
	}

	public void setDISCHDT(String dISCHDT) {
		DISCHDT = dISCHDT;
	}

	public String getZDOCTOR() {
		return ZDOCTOR;
	}

	public void setZDOCTOR(String zDOCTOR) {
		ZDOCTOR = zDOCTOR;
	}

	public String getZMEDPRV() {
		return ZMEDPRV;
	}

	public void setZMEDPRV(String zMEDPRV) {
		ZMEDPRV = zMEDPRV;
	}

	public String getLIFENUM() {
		return LIFENUM;
	}

	public void setLIFENUM(String lIFENUM) {
		LIFENUM = lIFENUM;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public String getFACTHOUS() {
		return FACTHOUS;
	}

	public void setFACTHOUS(String fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}

	public String getBANKACCDSC() {
		return BANKACCDSC;
	}

	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}

	public String getBNKACTYP() {
		return BNKACTYP;
	}

	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}

	public String getCURRCODE() {
		return CURRCODE;
	}

	public void setCURRCODE(String cURRCODE) {
		CURRCODE = cURRCODE;
	}

	public List<WSGENRegClaimADDBRecurringMessageModel> getwSGenRecLaimADDB_li() {
		return wSGenRecLaimADDB_li;
	}

	public void setwSGenRecLaimADDB_li(List<WSGENRegClaimADDBRecurringMessageModel> wSGenRecLaimADDB_li) {
		this.wSGenRecLaimADDB_li = wSGenRecLaimADDB_li;
	}

}
