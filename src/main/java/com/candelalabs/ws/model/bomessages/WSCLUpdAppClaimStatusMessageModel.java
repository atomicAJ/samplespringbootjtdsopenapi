/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSCLUpdAppClaimStatusMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMNO;
	private String CRTABLE;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String RGPYNUM;

	/**
	 * 
	 */
	public WSCLUpdAppClaimStatusMessageModel() {
		// TODO Auto-generated constructor stub
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Cashless_ApprovedHS;

	}

	public WSCLUpdAppClaimStatusMessageModel(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, String mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMNO, String cRTABLE, String lIFE, String cOVERAGE, String rIDER,
			String rGPYNUM) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		BOIDEN = FWDConstants.BO_IDENTIFIER_Cashless_ApprovedHS;
		MSGLNG = mSGLNG;
		MSGCNT = mSGCNT;
		INDC = iNDC;
		CHDRSEL = cHDRSEL;
		CLAIMNO = cLAIMNO;
		CRTABLE = cRTABLE;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
		RGPYNUM = rGPYNUM;
	}

	public String getBOMeesage() {
		PaddingUtil paddingUtil = new PaddingUtil();

		return paddingUtil.padRight(this.USRPRF, 9) +

				paddingUtil.padRight(this.CHDRCOY, 1) +

				paddingUtil.padRight(FWDConstants.BO_IDENTIFIER_Cashless_ApprovedHS, 20) +

				FWDConstants.MESSAGE_LANGUAGE +

				FWDConstants.BO_APPROVED_HS_MESSAGE_LENGTH +

				FWDConstants.MORE_INDICATOR +

				paddingUtil.padRight(this.CHDRSEL, 10) +
				// need to modify
				paddingUtil.padRight(this.CLAIMNO, 11) +

				paddingUtil.padRight(this.CRTABLE, 4) +

				paddingUtil.padZero(Integer.valueOf(this.LIFE), 2) +

				paddingUtil.padZero(Integer.valueOf(this.COVERAGE), 2) +

				paddingUtil.padZero(Integer.valueOf(this.RIDER), 2) +

				paddingUtil.padRight(this.RGPYNUM, 5);

	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public String getRGPYNUM() {
		return RGPYNUM;
	}

	public void setRGPYNUM(String rGPYNUM) {
		RGPYNUM = rGPYNUM;
	}

}
