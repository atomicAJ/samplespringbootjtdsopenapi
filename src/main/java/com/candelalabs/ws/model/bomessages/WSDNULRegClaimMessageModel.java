/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.math.BigDecimal;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSDNULRegClaimMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String DTOFDEATH;
	private String ZCATCLMBEN;
	private String CLAIMNO;
	private BigDecimal OTHERADJST;
	private String REASONCD;
	private String ZDIAGCDE;
	private String CLNTNUM;

	/**
	 * 
	 */
	public WSDNULRegClaimMessageModel() {
		// TODO Auto-generated constructor stub
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimRegister;
	}

	public String getBOMessage() {
		PaddingUtil paddingUtil = new PaddingUtil();

		return paddingUtil.padRight(this.USRPRF, 9) +

				paddingUtil.padRight(this.CHDRCOY, 1) +

				paddingUtil.padRight(this.BOIDEN, 20) +

				FWDConstants.MESSAGE_LANGUAGE +

				FWDConstants.BO_DEATH_CLAIM_REGISTER_MESSAGE_LENGTH +

				FWDConstants.MORE_INDICATOR +

				paddingUtil.padRight(this.CHDRSEL, 10) +

				paddingUtil.padRight(this.DTOFDEATH, 8).replace("-", "") +

				paddingUtil.padRight(this.ZCATCLMBEN, 1) +

				paddingUtil.padRight(this.CLAIMNO, 11) +

				paddingUtil.PadForDoubleWithSym(FWDConstants.BO_DEATH_UNIT_LINK_CLAIM_REGISTER_Other_Adjustments, 18) +

				paddingUtil.padRight("", 4) +

				paddingUtil.padRight(this.ZDIAGCDE, 8) +

				paddingUtil.padRight(this.CLNTNUM, 8);

	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getDTOFDEATH() {
		return DTOFDEATH;
	}

	public void setDTOFDEATH(String dTOFDEATH) {
		DTOFDEATH = dTOFDEATH;
	}

	public String getZCATCLMBEN() {
		return ZCATCLMBEN;
	}

	public void setZCATCLMBEN(String zCATCLMBEN) {
		ZCATCLMBEN = zCATCLMBEN;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public BigDecimal getOTHERADJST() {
		return OTHERADJST;
	}

	public void setOTHERADJST(BigDecimal oTHERADJST) {
		OTHERADJST = oTHERADJST;
	}

	public String getREASONCD() {
		return REASONCD;
	}

	public void setREASONCD(String rEASONCD) {
		REASONCD = rEASONCD;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getCLNTNUM() {
		return CLNTNUM;
	}

	public void setCLNTNUM(String cLNTNUM) {
		CLNTNUM = cLNTNUM;
	}

}
