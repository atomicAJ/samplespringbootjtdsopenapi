/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.math.BigDecimal;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSGENRegClaimWPMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMCUR;
	private String CLAIMNO;
	private String CLTYPE;
	private String PAYDATE;
	private String INCURDT;
	private Double PRCNT;
	private BigDecimal PYMT;
	private String ZCLMRECD;
	private String ZDIAGCDE;
	private String ZREGPCOD;
	private String CRTABLE;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;

	public WSGENRegClaimWPMessageModel() {
		super();
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Generic_RegisterClaimWP;
		this.MSGLNG = FWDConstants.MESSAGE_LANGUAGE;
		this.MSGCNT = "00133";
		this.INDC = FWDConstants.MORE_INDICATOR;
		// TODO Auto-generated constructor stub
	}

	public WSGENRegClaimWPMessageModel(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, String mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMCUR, String cLAIMNO, String cLTYPE, String iNCURDT, Double pRCNT,
			BigDecimal pYMT, String zCLMRECD, String zDIAGCDE, String zREGPCOD, String cRTABLE, String lIFE,
			String cOVERAGE, String rIDER) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		BOIDEN = FWDConstants.BO_IDENTIFIER_Generic_RegisterClaimWP;
		MSGLNG = FWDConstants.MESSAGE_LANGUAGE;
		MSGCNT = "00133";
		INDC = FWDConstants.MORE_INDICATOR;
		CHDRSEL = cHDRSEL;
		CLAIMCUR = cLAIMCUR;
		CLAIMNO = cLAIMNO;
		CLTYPE = cLTYPE;
		INCURDT = iNCURDT;
		PRCNT = pRCNT;
		PYMT = pYMT;
		ZCLMRECD = zCLMRECD;
		ZDIAGCDE = zDIAGCDE;
		ZREGPCOD = zREGPCOD;
		CRTABLE = cRTABLE;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
	}

	public String getBoMessage() {
		PaddingUtil pdngutl = new PaddingUtil();

		return pdngutl.padRight(this.USRPRF, 9) +

				pdngutl.padRight(this.CHDRCOY, 1) +

				pdngutl.padRight(this.BOIDEN, 20) +

				pdngutl.padRight(this.MSGLNG, 1) +

				pdngutl.padRight(this.MSGCNT, 5) +

				pdngutl.padRight(this.INDC, 1) +

				pdngutl.padRight(this.CHDRSEL, 10) +

				pdngutl.padRight(this.CLAIMCUR, 3) +

				pdngutl.padRight(this.CLAIMNO, 11) +

				pdngutl.padRight(this.CLTYPE, 2) +

				pdngutl.padRight(this.PAYDATE, 8) +

				pdngutl.padRight(this.INCURDT, 8) +

				pdngutl.PadForDouble(this.PRCNT, 5) +

				pdngutl.PadForDoubleWithSym(this.PYMT.doubleValue(), 18) + 

				pdngutl.padRight(this.ZCLMRECD, 8) +

				pdngutl.padRight(this.ZDIAGCDE, 8) +

				pdngutl.padRight(this.ZREGPCOD, 5) + 

				pdngutl.padRight(this.CRTABLE, 4) +

				pdngutl.padZero(Integer.valueOf(this.LIFE), 2) +

				pdngutl.padZero(Integer.valueOf(this.COVERAGE), 2) +

				pdngutl.padZero(Integer.valueOf(this.RIDER), 2);
	}

	public String getPAYDATE() {
		return PAYDATE;
	}

	public void setPAYDATE(String PAYDATE) {
		this.PAYDATE = PAYDATE;
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMCUR() {
		return CLAIMCUR;
	}

	public void setCLAIMCUR(String cLAIMCUR) {
		CLAIMCUR = cLAIMCUR;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCLTYPE() {
		return CLTYPE;
	}

	public void setCLTYPE(String cLTYPE) {
		CLTYPE = cLTYPE;
	}

	public String getINCURDT() {
		return INCURDT;
	}

	public void setINCURDT(String iNCURDT) {
		INCURDT = iNCURDT;
	}

	public Double getPRCNT() {
		return PRCNT;
	}

	public void setPRCNT(Double pRCNT) {
		PRCNT = pRCNT;
	}

	public BigDecimal getPYMT() {
		return PYMT;
	}

	public void setPYMT(BigDecimal pYMT) {
		PYMT = pYMT;
	}

	public String getZCLMRECD() {
		return ZCLMRECD;
	}

	public void setZCLMRECD(String zCLMRECD) {
		ZCLMRECD = zCLMRECD;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getZREGPCOD() {
		return ZREGPCOD;
	}

	public void setZREGPCOD(String zREGPCOD) {
		ZREGPCOD = zREGPCOD;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

}
