/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSGENRegClaimADDBRecurringMessageModel {

	private String DATEFROM;
	private String DATETO;
	private String ACDBEN;
	private Double BENPRCNT;

	/**
	 * 
	 */
	public WSGENRegClaimADDBRecurringMessageModel() {
		// TODO Auto-generated constructor stub
	}

	public WSGENRegClaimADDBRecurringMessageModel(String dATEFROM, String dATETO, String aCDBEN, Double bENPRCNT) {
		super();
		DATEFROM = dATEFROM;
		DATETO = dATETO;
		ACDBEN = aCDBEN;
		BENPRCNT = bENPRCNT;
	}

	public String getBOMessage() {
		PaddingUtil util = new PaddingUtil();

		return util.padRightWith9AsDefault(this.DATEFROM, 8) + util.padRightWith9AsDefault(this.DATETO, 8)
				+ util.padRight(this.ACDBEN, 5) + util.PadForDouble(this.BENPRCNT, 5);

	}

	public String getDATEFROM() {
		return DATEFROM;
	}

	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}

	public String getDATETO() {
		return DATETO;
	}

	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}

	public String getACDBEN() {
		return ACDBEN;
	}

	public void setACDBEN(String aCDBEN) {
		ACDBEN = aCDBEN;
	}

	public Double getBENPRCNT() {
		return BENPRCNT;
	}

	public void setBENPRCNT(Double bENPRCNT) {
		BENPRCNT = bENPRCNT;
	}

}
