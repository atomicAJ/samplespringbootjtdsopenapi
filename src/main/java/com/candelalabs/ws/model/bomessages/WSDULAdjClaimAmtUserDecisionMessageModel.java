/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.candelalabs.ws.FWDConstants;

import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSDULAdjClaimAmtUserDecisionMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMNO;
	private Double OTHERADJST;
	private String ZDIAGCDE;
	private String BANKACOUNT;
	private String BANKKEY;
	private String PAYCLT;
	private int FACTHOUS;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String DATEFROM;
	private String DATETO;
	
	private static Log log = LogFactory.getLog("WSDULAdjClaimAmtUserDecisionProcessor");

	/**
	 * 
	 */
	public WSDULAdjClaimAmtUserDecisionMessageModel() {
		// TODO Auto-generated constructor stub
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAdjustment;
	}

	public String getBOMessage() {
		PaddingUtil pdngutl = new PaddingUtil();
		log.info("COMPANY ="+this.CHDRCOY);
		return pdngutl.padRight(this.USRPRF, 9) 
				+ pdngutl.padRight(this.CHDRCOY, 1)

				+ pdngutl.padRight(FWDConstants.BO_IDENTIFIER_UnitLink_DeathClaimActualAdjustment, 20)

				+ pdngutl.padRight(FWDConstants.MESSAGE_LANGUAGE, 1)

				+ pdngutl.padZero(Integer.parseInt(FWDConstants.BO_UnitLink_DeathClaimActualAdjustment_Message_Length),
						5)

				+ pdngutl.padRight(FWDConstants.MORE_INDICATOR, 1)

				+ pdngutl.padRight(this.CHDRSEL, 10)

				+ pdngutl.padRight(this.CLAIMNO, 11)

				+ pdngutl.PadForDoubleWithSym(this.OTHERADJST, 18) 

				+ pdngutl.padRight(this.ZDIAGCDE, 8) 

				+ pdngutl.padRight(this.BANKACOUNT, 20) 

				+ pdngutl.padRight(this.BANKKEY, 10)

				+ pdngutl.padRight(this.PAYCLT, 8)

				+ pdngutl.padZero(this.FACTHOUS, 2)

				+ pdngutl.padRight(this.BANKACCDSC, 30)

				+ pdngutl.padRight(this.BNKACTYP, 4)

				+ pdngutl.padRight(this.DATEFROM, 8)

				+ pdngutl.padRight(this.DATETO, 8);

	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}


	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public Double getOTHERADJST() {
		return OTHERADJST;
	}

	public void setOTHERADJST(Double oTHERADJST) {
		OTHERADJST = oTHERADJST;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getBANKACOUNT() {
		return BANKACOUNT;
	}

	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getPAYCLT() {
		return PAYCLT;
	}

	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}

	public int getFACTHOUS() {
		return FACTHOUS;
	}

	public void setFACTHOUS(int fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}

	public String getBANKACCDSC() {
		return BANKACCDSC;
	}

	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}

	public String getBNKACTYP() {
		return BNKACTYP;
	}

	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}

	public String getDATEFROM() {
		return DATEFROM;
	}

	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}

	public String getDATETO() {
		return DATETO;
	}

	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

}
