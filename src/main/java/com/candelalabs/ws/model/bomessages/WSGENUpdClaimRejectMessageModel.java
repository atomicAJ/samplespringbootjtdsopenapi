/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSGENUpdClaimRejectMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMNO;
	private String CRTABLE;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String RGPYNUM;

	/**
	 * 
	 */
	public WSGENUpdClaimRejectMessageModel() {
		// TODO Auto-generated constructor stub
	}

	public WSGENUpdClaimRejectMessageModel(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, String mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMNO, String cRTABLE, String lIFE, String cOVERAGE, String rIDER,
			String rGPYNUM) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		BOIDEN = bOIDEN;
		MSGLNG = mSGLNG;
		MSGCNT = mSGCNT;
		INDC = iNDC;
		CHDRSEL = cHDRSEL;
		CLAIMNO = cLAIMNO;
		CRTABLE = cRTABLE;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
		RGPYNUM = rGPYNUM;
	}

	public String getBOMessage() {
		PaddingUtil padUtil = new PaddingUtil();

		return padUtil.padRight(this.USRPRF, 9)

				+ padUtil.padRight(this.CHDRCOY, 1)

				+ padUtil.padRight(this.BOIDEN, 20)

				+ padUtil.padRight(FWDConstants.MESSAGE_LANGUAGE, 1)

				+ padUtil.padZero(73, 5)

				+ padUtil.padRight(FWDConstants.MORE_INDICATOR, 1)

				+ padUtil.padRight(this.CHDRSEL, 10)

				+ padUtil.padRight(this.CLAIMNO, 11)

				+ padUtil.padRight(this.CRTABLE, 4)

				+ padUtil.padRight(this.LIFE, 2)

				+ padUtil.padRight(this.COVERAGE, 2)

				+ padUtil.padRight(this.RIDER, 2)

				+ padUtil.padRight(this.RGPYNUM, 5)

		;
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public String getRGPYNUM() {
		return RGPYNUM;
	}

	public void setRGPYNUM(String rGPYNUM) {
		RGPYNUM = rGPYNUM;
	}

}
