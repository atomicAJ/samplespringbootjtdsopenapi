/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.math.BigDecimal;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSDNULUpdClaimRejectStatusMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CANCELDATE;
	private String CLAIMNO;
	private BigDecimal OFCHARGE;

	/**
	 * 
	 */
	public WSDNULUpdClaimRejectStatusMessageModel() {
		// TODO Auto-generated constructor stub
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_NonUnitLink_DeathClaimRejected;
	}

	public String getBoMessage() {
		PaddingUtil paddingUtil = new PaddingUtil();

		return paddingUtil.padRight(USRPRF, 9) +

				paddingUtil.padRight(this.CHDRCOY, 1) +

				paddingUtil.padRight(this.BOIDEN, 20) +

				FWDConstants.MESSAGE_LANGUAGE +

				FWDConstants.BO_DEATH_CLAIM_REJECTED_MESSAGE_LENGTH +

				FWDConstants.MORE_INDICATOR +

				paddingUtil.padRight(this.CHDRSEL, 10) +

				paddingUtil.padRight(this.CANCELDATE, 8).replace("-", "") +

				paddingUtil.padRight(this.CLAIMNO, 11) +

				paddingUtil.PadForDoubleWithSym(FWDConstants.BO_DEATH_CLAIM_REJECTED_Office_Charges, 14);
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCANCELDATE() {
		return CANCELDATE;
	}

	public void setCANCELDATE(String cANCELDATE) {
		CANCELDATE = cANCELDATE;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public BigDecimal getOFCHARGE() {
		return OFCHARGE;
	}

	public void setOFCHARGE(BigDecimal oFCHARGE) {
		OFCHARGE = oFCHARGE;
	}

}
