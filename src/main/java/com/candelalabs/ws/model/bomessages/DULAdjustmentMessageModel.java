/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.math.BigDecimal;

/**
 * @author Triaji
 *
 */
public class DULAdjustmentMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMNO;
	private BigDecimal OTHERADJST;
	private String ZDIAGCDE;
	private String BANKACOUNT;
	private String BANKKEY;
	private String PAYCLT;
	private String FACTHOUS;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String DATEFROM;
	private String DATETO;

	/**
	 * 
	 */
	public DULAdjustmentMessageModel() {
		// TODO Auto-generated constructor stub
	}
	
	

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public BigDecimal getOTHERADJST() {
		return OTHERADJST;
	}

	public void setOTHERADJST(BigDecimal oTHERADJST) {
		OTHERADJST = oTHERADJST;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getBANKACOUNT() {
		return BANKACOUNT;
	}

	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getPAYCLT() {
		return PAYCLT;
	}

	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}

	public String getFACTHOUS() {
		return FACTHOUS;
	}

	public void setFACTHOUS(String fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}

	public String getBANKACCDSC() {
		return BANKACCDSC;
	}

	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}

	public String getBNKACTYP() {
		return BNKACTYP;
	}

	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}

	public String getDATEFROM() {
		return DATEFROM;
	}

	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}

	public String getDATETO() {
		return DATETO;
	}

	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}
	
}
