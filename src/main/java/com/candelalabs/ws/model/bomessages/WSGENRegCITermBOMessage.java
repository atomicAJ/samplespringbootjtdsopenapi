/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.math.BigDecimal;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSGENRegCITermBOMessage {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMCUR;
	private String CLAIMNO;
	private String CLTYPE;
	private String INCURDT;
	private String PAYCLT;
	private Double PRCNT;
	private BigDecimal PYMT;
	private String ZCLMRECD;
	private String ZDIAGCDE;
	private String ZREGPCOD;
	private String BANKACOUNT;
	private String BANKKEY;
	private String CRTABLE;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private int FACTHOUS;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String CURRCODE;
	private String DATEFROM;
	private String DATETO;

	public WSGENRegCITermBOMessage() {
		super();
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Generic_RegisterClaimCITerm;
		this.MSGLNG = FWDConstants.MESSAGE_LANGUAGE;
		this.MSGCNT = "00218";
		this.INDC = FWDConstants.MORE_INDICATOR;
		// TODO Auto-generated constructor stub
	}

	public WSGENRegCITermBOMessage(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, String mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMCUR, String cLAIMNO, String cLTYPE, String iNCURDT, String pAYCLT,
			Double pRCNT, BigDecimal pYMT, String zCLMRECD, String zDIAGCDE, String zREGPCOD, String bANKACOUNT,
			String bANKKEY, String cRTABLE, String lIFE, String cOVERAGE, String rIDER, int fACTHOUS, String bANKACCDSC,
			String bNKACTYP, String cURRCODE, String dATEFROM, String dATETO) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Generic_RegisterClaimCITerm;
		this.MSGLNG = FWDConstants.MESSAGE_LANGUAGE;
		this.MSGCNT = "00218";
		this.INDC = FWDConstants.MORE_INDICATOR;
		CHDRSEL = cHDRSEL;
		CLAIMCUR = cLAIMCUR;
		CLAIMNO = cLAIMNO;
		CLTYPE = cLTYPE;
		INCURDT = iNCURDT;
		PAYCLT = pAYCLT;
		PRCNT = pRCNT;
		PYMT = pYMT;
		ZCLMRECD = zCLMRECD;
		ZDIAGCDE = zDIAGCDE;
		ZREGPCOD = zREGPCOD;
		BANKACOUNT = bANKACOUNT;
		BANKKEY = bANKKEY;
		CRTABLE = cRTABLE;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
		FACTHOUS = fACTHOUS;
		BANKACCDSC = bANKACCDSC;
		BNKACTYP = bNKACTYP;
		CURRCODE = cURRCODE;
		DATEFROM = dATEFROM;
		DATETO = dATETO;
	}

	public String getBoMessage() {
		PaddingUtil util = new PaddingUtil();

		return util.padRight(this.USRPRF, 9)

				+ util.padRight(this.CHDRCOY, 1)

				+ util.padRight(this.BOIDEN, 20)

				+ util.padRight(this.MSGLNG, 1)

				+ this.MSGCNT

				+ this.INDC

				+ util.padRight(this.CHDRSEL, 10)

				+ util.padRight(this.CLAIMCUR, 3)

				+ util.padRight(this.CLAIMNO, 11)

				+ util.padRight(this.CLTYPE, 2)

				+ util.padRight(this.INCURDT, 8)

				+ util.padRight(this.PAYCLT, 8)

				+ util.PadForDouble(this.PRCNT, 5)

				+ util.PadForDoubleWithSym(this.PYMT.doubleValue(), 18)

				+ util.padRight(this.ZCLMRECD, 8)

				+ util.padRight(this.ZDIAGCDE, 8)

				+ util.padRight(this.ZREGPCOD, 5)

				+ util.padRight(this.BANKACOUNT, 20)

				+ util.padRight(this.BANKKEY, 10)

				+ util.padRight(this.CRTABLE, 4)

				+ util.padZero(Integer.valueOf(this.LIFE), 2)

				+ util.padZero(Integer.valueOf(this.COVERAGE), 2)

				+ util.padZero(Integer.valueOf(this.RIDER), 2)

				+ util.padZero(this.FACTHOUS, 2)

				+ util.padRight(this.BANKACCDSC, 30)

				+ util.padRight(this.BNKACTYP, 4)

				+ util.padRight(this.CURRCODE, 3)

				+ util.padRight(this.DATEFROM, 8)

				+ util.padRight(this.DATETO, 8);
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getCLAIMCUR() {
		return CLAIMCUR;
	}

	public void setCLAIMCUR(String cLAIMCUR) {
		CLAIMCUR = cLAIMCUR;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCLTYPE() {
		return CLTYPE;
	}

	public void setCLTYPE(String cLTYPE) {
		CLTYPE = cLTYPE;
	}

	public String getINCURDT() {
		return INCURDT;
	}

	public void setINCURDT(String iNCURDT) {
		INCURDT = iNCURDT;
	}

	public String getPAYCLT() {
		return PAYCLT;
	}

	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}

	public Double getPRCNT() {
		return PRCNT;
	}

	public void setPRCNT(Double pRCNT) {
		PRCNT = pRCNT;
	}

	public BigDecimal getPYMT() {
		return PYMT;
	}

	public void setPYMT(BigDecimal pYMT) {
		PYMT = pYMT;
	}

	public String getZCLMRECD() {
		return ZCLMRECD;
	}

	public void setZCLMRECD(String zCLMRECD) {
		ZCLMRECD = zCLMRECD;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getZREGPCOD() {
		return ZREGPCOD;
	}

	public void setZREGPCOD(String zREGPCOD) {
		ZREGPCOD = zREGPCOD;
	}

	public String getBANKACOUNT() {
		return BANKACOUNT;
	}

	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public int getFACTHOUS() {
		return FACTHOUS;
	}

	public void setFACTHOUS(int fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}

	public String getBANKACCDSC() {
		return BANKACCDSC;
	}

	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}

	public String getBNKACTYP() {
		return BNKACTYP;
	}

	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}

	public String getCURRCODE() {
		return CURRCODE;
	}

	public void setCURRCODE(String cURRCODE) {
		CURRCODE = cURRCODE;
	}

	public String getDATEFROM() {
		return DATEFROM;
	}

	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}

	public String getDATETO() {
		return DATETO;
	}

	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}

}
