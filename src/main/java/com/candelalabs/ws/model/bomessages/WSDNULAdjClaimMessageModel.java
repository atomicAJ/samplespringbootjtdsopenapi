/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSDNULAdjClaimMessageModel {
	
	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private int MSGCNT;
	private String INDC;
	private String CHDRSEL;
	private String CLAIMNO;
	private Double OTHERADJST;
	private String ZDIAGCDE;
	private String PAYCLT;
	private String BANKKEY;
	private String FACTHOUS;
	private String BANKACOUNT;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String DATEFROM;
	private String DATETO;


	/**
	 * 
	 */
	public WSDNULAdjClaimMessageModel() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param uSRPRF
	 * @param cHDRCOY
	 * @param bOIDEN
	 * @param mSGLNG
	 * @param mSGCNT
	 * @param iNDC
	 * @param cHDRSEL
	 * @param cLAIMNO
	 * @param oTHERADJST
	 * @param zDIAGCDE
	 * @param pAYCLT
	 * @param bANKKEY
	 * @param fACTHOUS
	 * @param bANKACOUNT
	 * @param bANKACCDSC
	 * @param bNKACTYP
	 * @param dATEFROM
	 * @param dATETO
	 */
	public WSDNULAdjClaimMessageModel(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, int mSGCNT,
			String iNDC, String cHDRSEL, String cLAIMNO, Double oTHERADJST, String zDIAGCDE, String pAYCLT,
			String bANKKEY, String fACTHOUS, String bANKACOUNT, String bANKACCDSC, String bNKACTYP, String dATEFROM) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		BOIDEN = bOIDEN;
		MSGLNG = mSGLNG;
		MSGCNT = mSGCNT;
		INDC = iNDC;
		CHDRSEL = cHDRSEL;
		CLAIMNO = cLAIMNO;
		OTHERADJST = oTHERADJST;
		ZDIAGCDE = zDIAGCDE;
		PAYCLT = pAYCLT;
		BANKKEY = bANKKEY;
		FACTHOUS = fACTHOUS;
		BANKACOUNT = bANKACOUNT;
		BANKACCDSC = bANKACCDSC;
		BNKACTYP = bNKACTYP;
		DATEFROM = dATEFROM;
	}


	public String getUSRPRF() {
		return USRPRF;
	}


	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}


	public String getCHDRCOY() {
		return CHDRCOY;
	}


	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}


	public String getBOIDEN() {
		return BOIDEN;
	}


	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}


	public String getMSGLNG() {
		return MSGLNG;
	}


	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}


	public int getMSGCNT() {
		return MSGCNT;
	}


	public void setMSGCNT(int mSGCNT) {
		MSGCNT = mSGCNT;
	}


	public String getINDC() {
		return INDC;
	}


	public void setINDC(String iNDC) {
		INDC = iNDC;
	}


	public String getCHDRSEL() {
		return CHDRSEL;
	}


	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}


	public String getCLAIMNO() {
		return CLAIMNO;
	}


	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}


	public Double getOTHERADJST() {
		return OTHERADJST;
	}


	public void setOTHERADJST(Double oTHERADJST) {
		OTHERADJST = oTHERADJST;
	}


	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}


	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}


	public String getPAYCLT() {
		return PAYCLT;
	}


	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}


	public String getBANKKEY() {
		return BANKKEY;
	}


	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}


	public String getFACTHOUS() {
		return FACTHOUS;
	}


	public void setFACTHOUS(String fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}


	public String getBANKACOUNT() {
		return BANKACOUNT;
	}


	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}


	public String getBANKACCDSC() {
		return BANKACCDSC;
	}


	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}


	public String getBNKACTYP() {
		return BNKACTYP;
	}


	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}


	public String getDATEFROM() {
		return DATEFROM;
	}


	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}


	public String getDATETO() {
		return DATETO;
	}


	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}


	public String getLifeAsiaMessage() {
		PaddingUtil pdngutl = new PaddingUtil();
		String msgtosend = pdngutl.padRight(USRPRF, 9)
				+ pdngutl.padRight(CHDRCOY, 1)
				+ pdngutl.padRight(BOIDEN, 20) 
				+ pdngutl.padRight(MSGLNG, 1)
				+ pdngutl.padZero(Integer.valueOf(MSGCNT), 5)
				+ pdngutl.padRight(INDC, 1) 
				+ pdngutl.padRight(CHDRSEL, 10)
				+ pdngutl.padRight(CLAIMNO, 11)
				+ pdngutl.PadForDoubleWithSym(OTHERADJST, 18)
				+ pdngutl.padRight(ZDIAGCDE, 8)
				+ pdngutl.padRight(BANKACOUNT, 20)
				+ pdngutl.padRight(BANKKEY, 10)
				+ pdngutl.padRight(PAYCLT, 8)				 
				+ pdngutl.padRight(FACTHOUS, 2)				
				+ pdngutl.padRight(BANKACCDSC,30)
				+ pdngutl.padRight(BNKACTYP, 4) 
				+ pdngutl.padRight(DATEFROM, 8)
				+ pdngutl.padRight(DATETO, 8);
		
		return msgtosend;
	}

}
