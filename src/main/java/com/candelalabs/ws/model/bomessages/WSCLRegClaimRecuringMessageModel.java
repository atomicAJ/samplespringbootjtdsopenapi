/**
 * 
 */
package com.candelalabs.ws.model.bomessages;

import java.math.BigDecimal;

import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSCLRegClaimRecuringMessageModel {

	private String HOSBEN;
	private String DATEFRM;
	private String DATETO;
	private BigDecimal ACTEXP;
	private BigDecimal ZNODAY;
	private BigDecimal TACTEXP;
	private BigDecimal GCNETPY;

	/**
	 * 
	 */
	public WSCLRegClaimRecuringMessageModel() {
		// TODO Auto-generated constructor stub
	}

	public WSCLRegClaimRecuringMessageModel(String hOSBEN, String dATEFRM, String dATETO, BigDecimal aCTEXP,
			BigDecimal zNODAY, BigDecimal tACTEXP, BigDecimal gCNETPY) {
		super();
		HOSBEN = hOSBEN;
		DATEFRM = dATEFRM;
		DATETO = dATETO;
		ACTEXP = aCTEXP;
		ZNODAY = zNODAY;
		TACTEXP = tACTEXP;
		GCNETPY = gCNETPY;
	}

	public String getBoMessage() {
		PaddingUtil paddingUtil = new PaddingUtil();
		// need to modify
		return paddingUtil.padRight(this.HOSBEN, 5) +
		// need to modify
				((paddingUtil.padRight(this.DATEFRM, 8))).replace("-", "") +
				// need to modify
				(paddingUtil.padRight(this.DATETO, 8)).replace("-", "") +
				// need to modify
				paddingUtil.PadForDoubleWithSym(this.ACTEXP.doubleValue(), 14) +
				// need to modify
				paddingUtil.PadForDouble(this.ZNODAY.doubleValue(), 5) +

				paddingUtil.PadForDoubleWithSym(this.TACTEXP.doubleValue(), 13) +

				paddingUtil.PadForDoubleWithSym(this.GCNETPY.doubleValue(), 13);

	}

	public String getHOSBEN() {
		return HOSBEN;
	}

	public void setHOSBEN(String hOSBEN) {
		HOSBEN = hOSBEN;
	}

	public String getDATEFRM() {
		return DATEFRM;
	}

	public void setDATEFRM(String dATEFRM) {
		DATEFRM = dATEFRM;
	}

	public String getDATETO() {
		return DATETO;
	}

	public void setDATETO(String dATETO) {
		DATETO = dATETO;
	}

	public BigDecimal getACTEXP() {
		return ACTEXP;
	}

	public void setACTEXP(BigDecimal aCTEXP) {
		ACTEXP = aCTEXP;
	}

	public BigDecimal getZNODAY() {
		return ZNODAY;
	}

	public void setZNODAY(BigDecimal zNODAY) {
		ZNODAY = zNODAY;
	}

	public BigDecimal getTACTEXP() {
		return TACTEXP;
	}

	public void setTACTEXP(BigDecimal tACTEXP) {
		TACTEXP = tACTEXP;
	}

	public BigDecimal getGCNETPY() {
		return GCNETPY;
	}

	public void setGCNETPY(BigDecimal gCNETPY) {
		GCNETPY = gCNETPY;
	}

}
