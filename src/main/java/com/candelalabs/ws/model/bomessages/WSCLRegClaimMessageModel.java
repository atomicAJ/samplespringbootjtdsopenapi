/**
 * 
 */
package com.candelalabs.ws.model.bomessages;


import java.util.List;

import com.candelalabs.ws.FWDConstants;
import com.candelalabs.ws.util.PaddingUtil;

/**
 * @author Triaji
 *
 */
public class WSCLRegClaimMessageModel {

	private String USRPRF;
	private String CHDRCOY;
	private String BOIDEN;
	private String MSGLNG;
	private String MSGCNT;
	private String INDC;
	private String CLAIMNO;
	private String CHDRSEL;
	private String LIFE;
	private String COVERAGE;
	private String RIDER;
	private String CRTABLE;
	private String CLAMPARTY;
	private String CLTYPE;
	private String ZDIAGCDE;
	private String ZMEDPRV;
	private String ZDOCTOR;
	private String CLAIMCUR;
	private String PAYCLT;
	private String BANKKEY;
	private String FACTHOUS;
	private String BANKACOUNT;
	private String BANKACCDSC;
	private String BNKACTYP;
	private String DATEFROM;
	private String ZCLMRECD;
	private String INCURDT;
	private String DISCHDT;
	private List<WSCLRegClaimRecuringMessageModel> wSCLRegClaimRecuring_li;

	/**
	 * 
	 */
	public WSCLRegClaimMessageModel() {
		// TODO Auto-generated constructor stub
		this.BOIDEN = FWDConstants.BO_IDENTIFIER_Cashless_RegisterRegularClaimHS;

	}

	public WSCLRegClaimMessageModel(String uSRPRF, String cHDRCOY, String bOIDEN, String mSGLNG, String mSGCNT,
			String iNDC, String cLAIMNO, String cHDRSEL, String lIFE, String cOVERAGE, String rIDER, String cRTABLE,
			String cLAMPARTY, String cLTYPE, String zDIAGCDE, String zMEDPRV, String zDOCTOR, String cLAIMCUR,
			String pAYCLT, String bANKKEY, String fACTHOUS, String bANKACOUNT, String bANKACCDSC, String bNKACTYP,
			String dATEFROM, String zCLMRECD, String iNCURDT, String dISCHDT,
			List<WSCLRegClaimRecuringMessageModel> wSCLRegClaimRecuring_li) {
		super();
		USRPRF = uSRPRF;
		CHDRCOY = cHDRCOY;
		BOIDEN = FWDConstants.BO_IDENTIFIER_Cashless_RegisterRegularClaimHS;
		MSGLNG = mSGLNG;
		MSGCNT = mSGCNT;
		INDC = iNDC;
		CLAIMNO = cLAIMNO;
		CHDRSEL = cHDRSEL;
		LIFE = lIFE;
		COVERAGE = cOVERAGE;
		RIDER = rIDER;
		CRTABLE = cRTABLE;
		CLAMPARTY = cLAMPARTY;
		CLTYPE = cLTYPE;
		ZDIAGCDE = zDIAGCDE;
		ZMEDPRV = zMEDPRV;
		ZDOCTOR = zDOCTOR;
		CLAIMCUR = cLAIMCUR;
		PAYCLT = pAYCLT;
		BANKKEY = bANKKEY;
		FACTHOUS = fACTHOUS;
		BANKACOUNT = bANKACOUNT;
		BANKACCDSC = bANKACCDSC;
		BNKACTYP = bNKACTYP;
		DATEFROM = dATEFROM;
		ZCLMRECD = zCLMRECD;
		INCURDT = iNCURDT;
		DISCHDT = dISCHDT;
		this.wSCLRegClaimRecuring_li = wSCLRegClaimRecuring_li;
	}

	public String getBOMessage() {
		PaddingUtil paddingUtil = new PaddingUtil();

		String boMessage = paddingUtil.padRight(this.USRPRF, 9) +

				paddingUtil.padRight(this.CHDRCOY, 1) +

				paddingUtil.padRight(FWDConstants.BO_IDENTIFIER_Cashless_RegisterRegularClaimHS, 20) +

				FWDConstants.MESSAGE_LANGUAGE +

				FWDConstants.BO_REGISTER_REGULAR_CLAIM_HS_MESSAGE_LENGTH +

				FWDConstants.MORE_INDICATOR +

				paddingUtil.padRight(this.CLAIMNO, 11) +

				paddingUtil.padRight(this.CHDRSEL, 10) +

				paddingUtil.padZero(Integer.valueOf(this.LIFE), 2) +

				paddingUtil.padZero(Integer.valueOf(this.COVERAGE), 2) +

				paddingUtil.padZero(Integer.valueOf(this.RIDER), 2) +

				paddingUtil.padRight(this.CRTABLE, 4) +

				paddingUtil.padRight(this.CLAMPARTY, 8) +

				paddingUtil.padRight(this.CLTYPE, 2) +

				paddingUtil.padRight(this.ZDIAGCDE, 8) +

				paddingUtil.padRight(this.ZMEDPRV, 10) +

				paddingUtil.padRight("", 8) +

				paddingUtil.padRight(this.CLAIMCUR, 3) +

				paddingUtil.padRight(this.PAYCLT, 8) +

				paddingUtil.padRight(this.BANKKEY, 10) +

				paddingUtil.padRight(this.FACTHOUS, 2) +
				// need to modify
				paddingUtil.padRight(this.BANKACOUNT, 20) +

				// need to modify
				paddingUtil.padRight(this.BANKACCDSC, 30) +

				paddingUtil.padRight(this.BNKACTYP, 4) +

				(paddingUtil.padRight(FWDConstants.DATEFROM, 8)).replace("-", "") +

				(paddingUtil.padRight(this.ZCLMRECD, 8)).replace("-", "") +

				(paddingUtil.padRight(this.INCURDT, 8)).replace("-", "") +

				(paddingUtil.padRight(this.DISCHDT, 8)).replace("-", "");

		for (WSCLRegClaimRecuringMessageModel wsclRegClaimRecuringMessageModel : wSCLRegClaimRecuring_li) {
			boMessage = boMessage + wsclRegClaimRecuringMessageModel.getBoMessage();
		}

		return boMessage;
	}

	public String getUSRPRF() {
		return USRPRF;
	}

	public void setUSRPRF(String uSRPRF) {
		USRPRF = uSRPRF;
	}

	public String getCHDRCOY() {
		return CHDRCOY;
	}

	public void setCHDRCOY(String cHDRCOY) {
		CHDRCOY = cHDRCOY;
	}

	public String getBOIDEN() {
		return BOIDEN;
	}

	public void setBOIDEN(String bOIDEN) {
		BOIDEN = bOIDEN;
	}

	public String getMSGLNG() {
		return MSGLNG;
	}

	public void setMSGLNG(String mSGLNG) {
		MSGLNG = mSGLNG;
	}

	public String getMSGCNT() {
		return MSGCNT;
	}

	public void setMSGCNT(String mSGCNT) {
		MSGCNT = mSGCNT;
	}

	public String getINDC() {
		return INDC;
	}

	public void setINDC(String iNDC) {
		INDC = iNDC;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCHDRSEL() {
		return CHDRSEL;
	}

	public void setCHDRSEL(String cHDRSEL) {
		CHDRSEL = cHDRSEL;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

	public String getCOVERAGE() {
		return COVERAGE;
	}

	public void setCOVERAGE(String cOVERAGE) {
		COVERAGE = cOVERAGE;
	}

	public String getRIDER() {
		return RIDER;
	}

	public void setRIDER(String rIDER) {
		RIDER = rIDER;
	}

	public String getCRTABLE() {
		return CRTABLE;
	}

	public void setCRTABLE(String cRTABLE) {
		CRTABLE = cRTABLE;
	}

	public String getCLAMPARTY() {
		return CLAMPARTY;
	}

	public void setCLAMPARTY(String cLAMPARTY) {
		CLAMPARTY = cLAMPARTY;
	}

	public String getCLTYPE() {
		return CLTYPE;
	}

	public void setCLTYPE(String cLTYPE) {
		CLTYPE = cLTYPE;
	}

	public String getZDIAGCDE() {
		return ZDIAGCDE;
	}

	public void setZDIAGCDE(String zDIAGCDE) {
		ZDIAGCDE = zDIAGCDE;
	}

	public String getZMEDPRV() {
		return ZMEDPRV;
	}

	public void setZMEDPRV(String zMEDPRV) {
		ZMEDPRV = zMEDPRV;
	}

	public String getZDOCTOR() {
		return ZDOCTOR;
	}

	public void setZDOCTOR(String zDOCTOR) {
		ZDOCTOR = zDOCTOR;
	}

	public String getCLAIMCUR() {
		return CLAIMCUR;
	}

	public void setCLAIMCUR(String cLAIMCUR) {
		CLAIMCUR = cLAIMCUR;
	}

	public String getPAYCLT() {
		return PAYCLT;
	}

	public void setPAYCLT(String pAYCLT) {
		PAYCLT = pAYCLT;
	}

	public String getBANKKEY() {
		return BANKKEY;
	}

	public void setBANKKEY(String bANKKEY) {
		BANKKEY = bANKKEY;
	}

	public String getFACTHOUS() {
		return FACTHOUS;
	}

	public void setFACTHOUS(String fACTHOUS) {
		FACTHOUS = fACTHOUS;
	}

	public String getBANKACOUNT() {
		return BANKACOUNT;
	}

	public void setBANKACOUNT(String bANKACOUNT) {
		BANKACOUNT = bANKACOUNT;
	}

	public String getBANKACCDSC() {
		return BANKACCDSC;
	}

	public void setBANKACCDSC(String bANKACCDSC) {
		BANKACCDSC = bANKACCDSC;
	}

	public String getBNKACTYP() {
		return BNKACTYP;
	}

	public void setBNKACTYP(String bNKACTYP) {
		BNKACTYP = bNKACTYP;
	}

	public String getDATEFROM() {
		return DATEFROM;
	}

	public void setDATEFROM(String dATEFROM) {
		DATEFROM = dATEFROM;
	}

	public String getZCLMRECD() {
		return ZCLMRECD;
	}

	public void setZCLMRECD(String zCLMRECD) {
		ZCLMRECD = zCLMRECD;
	}

	public String getINCURDT() {
		return INCURDT;
	}

	public void setINCURDT(String iNCURDT) {
		INCURDT = iNCURDT;
	}

	public String getDISCHDT() {
		return DISCHDT;
	}

	public void setDISCHDT(String dISCHDT) {
		DISCHDT = dISCHDT;
	}

	public List<WSCLRegClaimRecuringMessageModel> getwSCLRegClaimRecuring_li() {
		return wSCLRegClaimRecuring_li;
	}

	public void setwSCLRegClaimRecuring_li(List<WSCLRegClaimRecuringMessageModel> wSCLRegClaimRecuring_li) {
		this.wSCLRegClaimRecuring_li = wSCLRegClaimRecuring_li;
	}

}
