package com.candelalabs.ws.model;

public class WSDULRegClaimClaimsDetailsBOData {
	
	private String CauseOfDth;
	private double OtherAdjst;
	private String DtOfDeath;
	
	public String getCauseOfDth() {
		return CauseOfDth;
	}
	public void setCauseOfDth(String causeOfDth) {
		CauseOfDth = causeOfDth;
	}
	public double getOtherAdjst() {
		return OtherAdjst;
	}
	public void setOtherAdjst(double otherAdjst) {
		OtherAdjst = otherAdjst;
	}
	public String getDtOfDeath() {
		return DtOfDeath;
	}
	public void setDtOfDeath(String dtOfDeath) {
		DtOfDeath = dtOfDeath;
	}
	
	
	
}
