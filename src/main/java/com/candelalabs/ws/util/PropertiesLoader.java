/**
 * 
 */
package com.candelalabs.ws.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Triaji
 *
 */
public class PropertiesLoader {

	/**
	 * 
	 */
	public PropertiesLoader() {
		// TODO Auto-generated constructor stub
	}
	
	 public static Properties loadProperties(String resourceFileName) throws IOException {
	        Properties configuration = new Properties();
	        InputStream inputStream = PropertiesLoader.class
	          .getClassLoader()
	          .getResourceAsStream(resourceFileName);
	        configuration.load(inputStream);
	        inputStream.close();
	        return configuration;
	    }
	 
	 public static String getURLfromProp() throws IOException {
		 Properties prop = PropertiesLoader.loadProperties("application.properties");
		 return prop.getProperty("IntegrationService.Rest.BasePath");
	 }
	 
	 public static String getProp(String key) throws IOException {
		 Properties prop = PropertiesLoader.loadProperties("application.properties");
		 return prop.getProperty(key);
	 }
	 
//	 public static void main(String arg[]) {
//		 try {
//			
//			System.out.println(getURLfromProp());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 
//	 }

}
