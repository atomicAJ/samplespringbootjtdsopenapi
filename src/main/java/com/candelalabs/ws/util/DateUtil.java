package com.candelalabs.ws.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {
	private static final String DATE_OUTPUT_FORMAT = "dd/MM/yyyy";
	private static final String DATE_INPUT_FORMAT = "dd-MM-yyyy";

	private DateUtil() {
	}

	/**
	 * @param ts in sql timstamp
	 * @return date in string format
	 */
	public static String formatDate(Timestamp ts) {
		return (null != ts) ? new SimpleDateFormat(DATE_OUTPUT_FORMAT).format(ts) : null;
	}

	/**
	 * @param dt sql Date 
	 * @return date in string format
	 */
	public static String formatDate(Date dt) {
		return null != dt ? new SimpleDateFormat(DATE_OUTPUT_FORMAT).format(dt) : null;
	}
	
	/**
	 * @param strDt in strig
	 * @return date in string format
	 * @throws ParseException
	 */
	public static String formatDate(String strDt){
		if(null == strDt || strDt.isEmpty())
			return null;
		try{
			java.util.Date parse = new SimpleDateFormat(DATE_INPUT_FORMAT).parse(strDt);
			return null != parse ? new SimpleDateFormat(DATE_OUTPUT_FORMAT).format(parse) : null;
		}catch(ParseException pse)
		{
			return null;
		}
	}
}
