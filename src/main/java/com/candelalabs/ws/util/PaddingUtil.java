package com.candelalabs.ws.util;

import java.text.DecimalFormat;
import java.util.Optional;

/**
 * @author Swati Reusable Padding functions
 *
 */
public class PaddingUtil {

	public static void main(String args[]) {

		PaddingUtil util = new PaddingUtil();
		System.out.println(util.PadForDoubleWithSym(0.0, 14));

	}

	public String padZero(Integer no, int noOfZeros) {
		if (Optional.ofNullable(no).isPresent()) {
			return String.format("%0" + noOfZeros + "d", no);
		} else
			return String.format("%0" + noOfZeros + "d", 0);
	}

	public String padForLong(Long no, int noOfZeros) {
		if (Optional.ofNullable(no).isPresent()) {
			return String.format("%0" + noOfZeros + "d", no);
		} else
			return String.format("%0" + noOfZeros + "d", 0);
	}

	public String PadForDouble(Double no, int noOfZeros) {
		if (no != null) {
			if (Optional.ofNullable(no).isPresent()) {
				DecimalFormat decimalFormat = new DecimalFormat(".##");
				String fractionstr = decimalFormat.format(no).split("\\.")[1];
				fractionstr = fractionstr.length() < 2 ? fractionstr + "0" : fractionstr;
				return padForLong(no.longValue(), noOfZeros - 2) + fractionstr;
			} else {
				return String.format("%0" + noOfZeros + "d", 0);
			}
		} else {
			return String.format("%0" + noOfZeros + "d", 0);
		}
	}

	public String PadForDoubleWithSym(Double no, int noOfZeros) {

		if (no != null) {
			if (Optional.ofNullable(no).isPresent()) {
				DecimalFormat decimalFormat = new DecimalFormat(".##");
				String fractionstr = decimalFormat.format(no).split("\\.")[1];
				fractionstr = fractionstr.length() < 2 ? fractionstr + "0" : fractionstr;
				String str = no >= 0 ? "+" : "-";
				return padForLong(Math.abs(no.longValue()), noOfZeros - 3) + fractionstr + str;
			} else {
				return String.format("%0" + Integer.valueOf(noOfZeros - 1) + "d", 0) + "+";
			}
		} else {
			return String.format("%0" + Integer.valueOf(noOfZeros - 1) + "d", 0) + "+";
		}
	}

	public String padLeft(String s, int n) {
		String empty = "";
		if (null != s)
			return String.format("%1$" + n + "s", s);
		else
			return String.format("%1$" + n + "s", empty);
	}

	public String padRight(String s, int n) {
		String empty = "";
		if (null != s)
			// return String.format("%1$-" + n + "s", s);
			return String.format("%-" + n + "." + n + "s", s);
		else
			// return String.format("%1$-" + n + "s", empty);
			return String.format("%-" + n + "." + n + "s", empty);
	}
	
	public String padRightWith9AsDefault(String s, int n) {
		String empty = "";
		if (null != s && !s.trim().equalsIgnoreCase(""))
			// return String.format("%1$-" + n + "s", s);
			return String.format("%-" + n + "." + n + "s", s);
		else
			for (int i = 0; i<n;i++) {
				empty = empty +"9";
			}
		return empty;
	}

	public String BankKey(int cc, String cur) {
		String BANKKEY = "";
		if (null != cur) {
			if (cc == 2 && cur.contentEquals("IDR")) {
				BANKKEY = "BCA003";
			} else if (cc == 2 && cur.contentEquals("USD")) {
				BANKKEY = "BCA004";
			} else if (cc == 5 && cur.contentEquals("IDR")) {
				BANKKEY = "MAN005";
			}
		}

		return BANKKEY;
	}

	public int FactoringHouse(int cc, String curm) {
		int FACTHOUS = 0;
		if (null != curm) {
			if (cc == 2 && curm.contentEquals("IDR")) {
				FACTHOUS = 15;
			} else if (cc == 2 && curm.contentEquals("USD")) {
				FACTHOUS = 16;
			} else if (cc == 5 && curm.contentEquals("IDR")) {
				FACTHOUS = 63;
			}
		}
		return FACTHOUS;
	}

	public String Type(String bankkey) {
		String BNKACTYP = "";
		if (null != bankkey) {
			if (bankkey.contentEquals("BCA003") || bankkey.contentEquals("BCA004")) {
				BNKACTYP = "BCAC";
			} else if (bankkey.contentEquals("MAN005")) {
				BNKACTYP = "MDDD";
			}
		}

		return BNKACTYP;
	}
	
	

}
