/**
 * 
 */
package com.candelalabs.ws.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;

import org.apache.commons.text.StringEscapeUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.candelalabs.ws.model.tables.BenefitNotPaidAmount;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * @author Triaji Reusable generic functions
 *
 */
public class Util {
	public static String objectToJsonString(Object obj) throws JsonProcessingException {
		ObjectMapper om = new ObjectMapper();

		return om.writerWithDefaultPrettyPrinter().writeValueAsString(obj);

	}

	public static <T> Object jsonnStringtoObject(String jsonString, Class<T> cls)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();

		return om.readValue(jsonString, cls);
	}

	public static String readJsonField(String jsons, String name) throws Exception {
		String statusValue = "";
		if (jsons != null && jsons.length() > 0) {

			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(jsons);
			JsonNode statusNode = jsonNode.get(name);
			if (statusNode.isArray()) {
				ArrayNode ar = (ArrayNode) statusNode;
				if (ar.size() == 1) {
					statusValue = ar.get(0).get("id").asText();
				} else if (ar.size() > 1) {
					throw new Exception("Data has got more than one");
				} else {
					throw new Exception("Execution Data is Empty");
				}
			} else {
				statusValue = statusNode.asText();
			}
			;
			return statusValue;
		}
		return statusValue;
	}

	public static String readJsonField(String jsons, String name, String fieldInArray) throws Exception {
		String statusValue = "";
		if (jsons != null && jsons.length() > 0) {

			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(jsons);
			JsonNode statusNode = jsonNode.get(name);
			if (statusNode.isArray()) {
				ArrayNode ar = (ArrayNode) statusNode;
				if (ar.size() == 1) {
					statusValue = ar.get(0).get(fieldInArray).asText();
				} else if (ar.size() > 1) {
					throw new Exception("Data has got more than one");
				} else {
					throw new Exception("Execution Data is Empty");
				}
			} else {
				statusValue = statusNode.asText();
			}
			;
			return statusValue;
		}
		return statusValue;
	}

	public static HttpHeaders setHttpPostHeaders(String cred64) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", cred64);
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		return headers;
	}

	public static Timestamp convertToTs(Object obj) throws ParseException {
		Timestamp ts = null;
		if (obj != null) {
			if (obj instanceof OffsetDateTime) {
				OffsetDateTime dateTime = (OffsetDateTime) obj;
				// ts =
				// Timestamp.valueOf(dateTime.toLocalDateTime().atZone(ZoneId.systemDefault()));
				ts = Timestamp.from(dateTime.atZoneSameInstant(ZoneId.systemDefault()).toInstant());
			} else if (obj instanceof String) {
				java.sql.Date objDate = objToDate(obj, "dd/MM/yyyy");
				ts = new Timestamp(objDate.getTime());
			}
		}
		return ts;

	}

	public static String dateToString(Object obj, String format) {
		String dateString = "";
		if (obj != null) {
			if (obj instanceof LocalDate) {
				LocalDate tgl = (LocalDate) obj;
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
				dateString = tgl.format(formatter);
			} else if (obj instanceof Timestamp) {
				Timestamp ts = (Timestamp) obj;
				java.util.Date tgl = new Date();
				tgl.setTime(ts.getTime());
				dateString = new SimpleDateFormat(format).format(tgl);
			} else {
				SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(format);
				dateString = DATE_FORMAT.format(obj);
			}
		}

		return dateString;
	}

	public static Long dateToLong(Object obj) {
		Long dateLong = new Long(0);
		if (obj != null) {
			// if (obj instanceof LocalDate) {
			// LocalDate tgl = (LocalDate) obj;
			// DateTimeFormatter formatter =
			// DateTimeFormatter.ofPattern(format);
			// tgl.
			// dateLong = tgl.format(formatter);
			// } else
			if (obj instanceof Timestamp) {
				Timestamp ts = (Timestamp) obj;
				dateLong = ts.getTime();
			} else if (obj instanceof java.sql.Date) {
				java.sql.Date tgl = (java.sql.Date) obj;
				dateLong = tgl.getTime();
			} else if (obj instanceof java.util.Date) {
				java.util.Date tgl = (Date) obj;
				dateLong = tgl.getTime();
			}
		}

		return dateLong;
	}

	public static BigDecimal dateToBigDecimal(Object obj) {
		Long dateLong = new Long(0);
		if (obj != null) {
			// if (obj instanceof LocalDate) {
			// LocalDate tgl = (LocalDate) obj;
			// DateTimeFormatter formatter =
			// DateTimeFormatter.ofPattern(format);
			// tgl.
			// dateLong = tgl.format(formatter);
			// } else
			if (obj instanceof Timestamp) {
				Timestamp ts = (Timestamp) obj;
				dateLong = ts.getTime();
			} else if (obj instanceof java.sql.Date) {
				java.sql.Date tgl = (java.sql.Date) obj;
				dateLong = tgl.getTime();
			} else if (obj instanceof java.util.Date) {
				java.util.Date tgl = (Date) obj;
				dateLong = tgl.getTime();
			}
			return new BigDecimal(dateLong);
		} else {
			return null;
		}

	}

	public static java.sql.Date objToDate(Object obj, String format) throws ParseException {
		if (obj != null) {
			if (obj instanceof String) {
				String dateString = (String) obj;
				if (dateString != null && dateString != "") {
					SimpleDateFormat sdf1 = new SimpleDateFormat(format);
					java.util.Date date = sdf1.parse(dateString);
					java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
					return sqlStartDate;
				} else {
					return null;
				}
			} else if (obj instanceof BigDecimal) {
				BigDecimal dateDec = (BigDecimal) obj;
				Long dateLong = dateDec.longValue();
				java.sql.Date tgl = new java.sql.Date(dateLong);
				return tgl;
			} else if (obj instanceof Long) {
				Long dateLong = (Long) obj;
				java.sql.Date tgl = new java.sql.Date(dateLong);
				return tgl;
			} else if (obj instanceof LocalDate) {
				LocalDate dt = (LocalDate) obj;
				java.sql.Date tgl = java.sql.Date.valueOf(dt);
				return tgl;
			}  else if (obj instanceof  LocalDateTime) {
				LocalDateTime dt = (LocalDateTime) obj;
				java.util.Date tgl = Date.from(dt.atZone(ZoneId.systemDefault()).toInstant());
				java.sql.Date sqltgl = new java.sql.Date(tgl.getTime());
				return sqltgl;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static OffsetDateTime objToOffSetDateTime(Object obj) {
		OffsetDateTime dt = null;
		if (obj != null) {
			if (obj instanceof Timestamp) {
				Timestamp ts = (Timestamp) obj;
				dt = OffsetDateTime.ofInstant(Instant.ofEpochMilli(ts.getTime()), ZoneId.systemDefault());
			} else if (obj instanceof java.util.Date) {
				java.util.Date ts = (Date) obj;
				dt = OffsetDateTime.ofInstant(Instant.ofEpochMilli(ts.getTime()), ZoneId.systemDefault());
			}
		}

		return dt;
	}

	public static java.sql.Date addDays(Date date, int days) throws Exception {
		/*Calendar c = Calendar.getInstance();
		c.setTime(date);
		int addedDays = 0;
		while (addedDays < days) {
			c.add(Calendar.DATE, 1);
			if (!(c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7)) {
				addedDays = addedDays + 1;
			}
		}
		return new java.sql.Date(c.getTimeInMillis());*/
		LocalDateTime localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDate = localDate.plusDays(days);
		return  objToDate(localDate,null);
	}

	public static java.sql.Date addDays(java.sql.Date date, int days) throws Exception {
		/*Calendar c = Calendar.getInstance();
		c.setTime(date);
		int addedDays = 0;
		while (addedDays < days) {
			c.add(Calendar.DATE, 1);
			if (!(c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7)) {
				addedDays = addedDays + 1;
			}
		}
		return new java.sql.Date(c.getTimeInMillis());
		*/
		LocalDate localDate = date.toLocalDate();
		localDate = localDate.plusDays(days);
		return  objToDate(localDate,null);

	}

	public static String listToString(Object obj_lis, String separator) {
		List<Object> obj_li = (List<Object>) obj_lis;
		String temp = "";
		int i = 0;
		if (obj_li != null && obj_li.size() > 0) {
			for (Object object : obj_li) {
				if (object instanceof BigDecimal) {
					BigDecimal bg = (BigDecimal) object;
					temp = temp + bg.toString() + separator;
				} else if (object instanceof BenefitNotPaidAmount) {
					BenefitNotPaidAmount ba = (BenefitNotPaidAmount) object;
					BigDecimal bg = ba.getBenefitNotPaidAmount();
					if (bg != null) {
						temp = temp + bg.toString() + separator;
					}
				}
			}
		}

		return temp;
	}

	public static int getDiffYears(java.sql.Date first, java.sql.Date last) {
		Calendar a = getCalendar(first);
		Calendar b = getCalendar(last);
		int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
		if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH)
				|| (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
			diff--;
		}
		return diff;
	}

	public static Calendar getCalendar(java.sql.Date date) {
		Calendar cal = Calendar.getInstance(Locale.US);
		cal.setTime(date);
		return cal;
	}

	public static void main(String[] args) throws ParseException {

		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(System.currentTimeMillis()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date tgl = sdf.parse("2019-10-23");
		try {
			System.out.println(addDays(tgl,3));
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public String checknull(String strinput) {
		if (strinput == null || strinput.trim().equalsIgnoreCase("null")) {
			strinput = "";
		}

		return strinput;
	}
}
