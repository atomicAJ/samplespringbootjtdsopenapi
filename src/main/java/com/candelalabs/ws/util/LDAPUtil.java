/**
 * 
 */
package com.candelalabs.ws.util;

import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import javax.naming.directory.SearchControls;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import com.candelalabs.ws.model.Person;

/**
 * @author Triaji
 *
 */

public class LDAPUtil {

	private String domainBase;

	private SearchControls searchCtls;

	private LdapTemplate ldapTemplate;

	public LDAPUtil() throws Exception {
		// TODO Auto-generated constructor stub
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(PropertiesLoader.getProp("LDAP.URL"));
		contextSource.setBase(PropertiesLoader.getProp("LDAP.DOMAINBASE"));
		contextSource.setUserDn(PropertiesLoader.getProp("LDAP.USERNAME"));
		contextSource.setPassword(PropertiesLoader.getProp("LDAP.PASSWORD"));
		contextSource.afterPropertiesSet();
		this.ldapTemplate = new LdapTemplate(contextSource);
		this.ldapTemplate.afterPropertiesSet();
		// Create the search controls
		this.searchCtls = new SearchControls();

		// Specify the attributes to return
		String returnedAtts[] = { "cn", "givenName", "displayName", "userPrincipalName", "mail", "mobile" };
		searchCtls.setReturningAttributes(returnedAtts);

		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
		this.domainBase = PropertiesLoader.getProp("LDAP.DOMAINBASE");

	}

	public Person getADPerson(String cn) throws NamingException {
		String searchfilter = "(cn=" + cn + ")";
		List<Person> people = this.ldapTemplate.search("", searchfilter, this.searchCtls, new PersonAttributesMapper());
		// "+people.get(0).getDisplayName()+" ");

		return people.get(0);
	}

	private class PersonAttributesMapper implements AttributesMapper<Person> {
		public Person mapFromAttributes(Attributes attrs) throws NamingException {
			Person person = new Person();
			// person.setFullName((String)attrs.get("cn").get());
			try {
				person.setDisplayName((String) attrs.get("displayName").get());
				person.setCn((String) attrs.get("cn").get());
				Attribute mail = attrs.get("mail");
				if (mail != null) {
					person.setMail((String) mail.get());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				person.setCn("");
				person.setDisplayName("");
				person.setMail("");
			}
			return person;
		}
	}

}